/*******************************************************************************                            
** Change History                              
*******************************************************************************                            
** Date        Author          Description                              
*******************************************************************************        
** 30/08/2017  Poongodi		Created
** 28/09/2017  Poongodi		Opening and Closing Issue fixed
 ** 04/10/2017 Poongodi		 Costprice formula 
** 13/10/2017 Poongodi	 Opening stock and closing stock removed
** 31/10/2017 Poongodi	 Opening stock and closing stock Added
** 05/12/2017 Poongodi	 Distinct added in opening and closing section
*******************************************************************************/ 
CREATE PROC dbo.Usp_ControlReport(@InstanceId  VARCHAR(36),@AccountId CHAR(36), @StartDate datetime,@EndDate datetime )
as
begin

Declare @OpeningStock Decimal (20,2) ,@ClosingStock decimal (20,2),@AdjStock Decimal (20,2),
@AdjQty bigint, @OPenQty bigint,@ClosingImportQty bigint, @ClosingImportValue decimal(20,2) ,@ClosingQty bigint ,@TempOpeningValue decimal (20,2) ,@TempOpeningQty bigint  ,
@TempClosing bigint, @TempClosingValue  decimal (20,2) 
 --select  @InstanceId=N'2dd2c9b1-33a4-488d-b68a-4ad603141ff1',@AccountId=N'67067991-0e68-4779-825e-8e1d724cd68b',@StartDate=N'01-Jul-2017',@EndDate=N'31-Jul-2017'
 --select * from instance  (nolock)where accountid ='67067991-0e68-4779-825e-8e1d724cd68b' and id ='5d9a5071-3db9-44e0-9921-c226f34c3616'
 Declare @IsDisplayStock int =0 
 if exists (select * from ClosingStock (nolock) where AccountId = @AccountId AND InstanceId = @InstanceId and (TransDate < @StartDate OR TransDate <= @EndDate))
 set @IsDisplayStock =1

 Create  table #Control  (TranType varchar(20), TranCount bigint, PaymentMode varchar(25), TransTotal Decimal(18,2), [Basic] decimal(18,2),
 TaxAmount decimal(18,2), DiscountValue  decimal(18,2),[RoundOff]  decimal(18,2),GroupSum int, DisplayType int default 0)
 declare @Paymenttype table (payid int identity(1,1),Paymenttype varchar(10))
 insert into @Paymenttype
 values(' Cash'),('Card'),('Credit'),('Cheque')
 insert into #Control(PaymentMode,DisplayType) 
 Values ('Sales',3)
 
 /*Sales*/
insert into #Control(TranType,TranCount,PaymentMode,TransTotal,[Basic],TaxAmount,DiscountValue,[RoundOff]) 
Select [Type],[SalesCount],p.paymenttype,[TransTotal],[Basic],[TaxAmount],[DiscountValue],[RoundOff] 
from (
	Select 
	'Sales' [Type], 
	Count(DISTINCT Id) [SalesCount], 
	paymenttype [PaymentType], 
	sum(transtotal) [TransTotal], 
	sum([Basic]) [Basic], 
	sum(taxamount) [TaxAmount], 
	sum(discountvalue) [DiscountValue],
	sum(roundoff) [RoundOff]  
	from (
		SELECT   
		S.Id   ,
		case isnull(CashType,'') when 'Full' then isnull(PaymentType,'') else isnull(CashType,'') end [PaymentType],
		Convert(date,S.invoicedate) invoicedate,
		--round((case right(cast(sum(a.itemval - a.discount) as decimal(18,2)), 2) when 50 then round(sum(a.itemval - a.discount)+0.5, 0) else round(sum(a.itemval - a.discount), 0) end), 2) [TransTotal],
		S.SaleAmount AS [TransTotal],
		--Round(SUM(a.Discount), 2) as DiscountValue,
		ISNULL(S.TotalDiscountValue,0) as DiscountValue,
		--Round(sum ((a.itemval - a.discount ) *100/(100+ISNULL(SI.GstTotal, 0))),2) [Basic],
		(S.SalesItemAmount-ISNULL(S.TotalDiscountValue,0))-S.GstAmount AS [Basic],
		--Round(sum((a.itemval - a.discount ) - ((a.itemval - a.discount ) *100/(100+ISNULL(SI.GstTotal, 0)))),2) [TaxAmount],
		S.GstAmount AS [TaxAmount],
		--round((case right(cast(sum(a.itemval - a.discount) as decimal(18,2))  ,2) when  50 then round( sum(a.itemval - a.discount)+0.5 ,0) else round( sum(a.itemval - a.discount) ,0) end )- round(sum(a.itemval - a.discount) ,2) ,2) [RoundOff]
		S.RoundoffSaleAmount AS [RoundOff]
		FROM Sales S WITH(NOLOCK) 
		INNER JOIN (Select * from SalesItem WITH(NOLOCK) where AccountId = @AccountId AND InstanceId = @InstanceId) SI  on S.id= SI.salesid
		INNER JOIN (Select * from ProductStock WITH(NOLOCK) where AccountId = @AccountId AND InstanceId = @InstanceId) PS  on PS.Id=SI.productstockid
		--cross apply (Select (SI.Quantity) * (isNull(SI.SellingPrice, ISNULL(PS.SellingPrice,0))) [ItemVal],	((SI.Quantity) * ISNULL(SI.SellingPrice, ISNULL(PS.SellingPrice,0)) * case when ISNULL(s.Discount,0) > 0 then s.Discount else ISNULL(si.Discount,0) end / 100) [Discount]) as A
		WHERE S.AccountId = @AccountId and S.InstanceId = @InstanceId
		AND Convert(date,S.invoicedate) BETWEEN @StartDate AND @EndDate
		and s.cancelstatus is null
		group by isnull(CashType,''),isnull(PaymentType,''),s.id, Convert(date,S.invoicedate),S.SaleAmount,ISNULL(S.TotalDiscountValue,0),
		S.SalesItemAmount,S.GstAmount,S.RoundoffSaleAmount  
	) C group by paymenttype 
) a right join @Paymenttype p on ltrim(p.Paymenttype) = a.Paymenttype
order by p.paymenttype

/*Sales Summary*/
Insert into  #Control(TranType, TranCount,PaymentMode,TransTotal,[Basic],TaxAmount,DiscountValue,[RoundOff],GroupSum,DisplayType )
select 'Sales', sum(isnull(TranCount,0)),'Total(a)', sum(isnull(TransTotal,0)),sum(isnull([Basic],0)),sum(isnull(TaxAmount,0)),sum(isnull(DiscountValue,0)),sum(isnull([RoundOff],0)),1 ,1
from #Control
insert into #Control(PaymentMode,DisplayType) 
 Values ('Return',3)

/*Sales Return*/
insert into #Control(TranType, TranCount,PaymentMode,TransTotal,[Basic],TaxAmount,DiscountValue,[RoundOff])
SELECT [Type],ReturnCount,p.paymenttype,[TransTotal],[Basic],[TaxAmount],[DiscountValue],[RoundOff]  
from (
	SELECT 
	'SalesReturn' [Type], 
	count(distinct id) ReturnCount,  
	paymenttype, 
	--Sum(Round(transtotal, 0)) *(-1) [TransTotal], 
	--Sum(Round([basic], 2)) * (-1) [Basic], 
	--Sum([taxamount]) *(-1) [TaxAmount], 
	--Sum(discountvalue) * (-1) [DiscountValue],
	--Sum(Round(transtotal, 0) - Round(transtotal, 2)) *(-1) [RoundOff] 
	sum(transtotal)*(-1) [TransTotal], 
	sum([Basic])*(-1) [Basic], 
	sum(taxamount)*(-1) [TaxAmount], 
	sum(discountvalue)*(-1) [DiscountValue],
	sum(roundoff)*(-1) [RoundOff] 
	FROM (
		SELECT 
		CONVERT(DATE, SR.Returndate) InvoiceDate, 
		SR.id, 
		Count( SR.id) ReturnCount, 
		CASE Isnull(S.cashtype, '') WHEN '' THEN CASE Isnull(SP.customerpaymenttype, 'Cash') WHEN 'Cash' THEN Isnull(SP.customerpaymenttype, 'Cash') ELSE Isnull(SP.customerpaymenttype, '') END WHEN 'Full' THEN Isnull(paymenttype, '') ELSE Isnull(cashtype, '') END paymenttype,
		--Round(Sum(a.itemval - a.discount), 2) [TransTotal], 
		SR.NetAmount AS [TransTotal],
		--Round(Sum(a.discount), 2) AS DiscountValue, 
		ISNULL(SR.TotalDiscountValue,0) AS DiscountValue,
		--Round(Sum ((a.itemval - a.discount) * 100 / ( 100 + ( CASE Isnull(ps.gsttotal, 0) WHEN 0 THEN Isnull(sri.gsttotal, 0 ) ELSE Isnull(ps.gsttotal, 0) END ) )), 2) [Basic],
		(SR.ReturnItemAmount-ISNULL(SR.TotalDiscountValue,0))-SR.GstAmount AS [Basic],
		--Round(Sum((a.itemval - a.discount) - ((a.itemval - a.discount) * 100 / ( 100 + ( CASE Isnull(ps.gsttotal, 0) WHEN 0 THEN Isnull(sri.gsttotal, 0 ) ELSE Isnull(ps.gsttotal, 0) END ) ) )), 2) [TaxAmount] 
		SR.GstAmount AS [TaxAmount],
		SR.RoundoffNetAmount AS [RoundOff]
		FROM salesreturn SR WITH(nolock) 
		INNER JOIN (SELECT * FROM salesreturnitem (nolock) WHERE AccountId = @AccountId AND instanceid = @InstanceId AND Isnull(isdeleted, 0) != 1) SRI ON SRI.salesreturnid = SR.id 
		INNER JOIN (SELECT * FROM productstock (nolock) WHERE AccountId = @AccountId AND instanceid = @InstanceId) PS ON PS.id = SRI.productstockid 
		LEFT JOIN (SELECT * FROM patient (nolock) WHERE accountid = @accountid) SP ON SP.id = Sr.patientid 
		LEFT JOIN (SELECT * FROM sales (nolock) WHERE AccountId = @AccountId AND instanceid = @InstanceId) S ON SR.salesid = S.id 
		--CROSS apply (SELECT (SRI.quantity * Isnull(SRI.mrpsellingprice, Isnull(PS.sellingprice, 0))) [ItemVal], ((SRI.quantity) * Isnull(SRI.mrpsellingprice, Isnull(PS.sellingprice, 0)) * CASE Isnull(s.discount, 0) WHEN 0 THEN Isnull(sri.discount, 0) else s.discount END / 100) [Discount]) A 
		WHERE SR.accountid = @AccountId AND SR.InstanceId = @InstanceId
		AND CONVERT(DATE, SR.Returndate) BETWEEN @StartDate AND @EndDate
		AND Isnull(sr.canceltype, 0) != 2 
		and (sri.IsDeleted is null or sri.IsDeleted != 1)
		and isnull(s.cancelstatus,0) = 0
		GROUP BY CONVERT(DATE, SR.Returndate),SR.id,S.cashtype,SP.customerpaymenttype,s.paymenttype,
		SR.NetAmount,SR.ReturnItemAmount,ISNULL(SR.TotalDiscountValue,0),SR.GstAmount,SR.RoundoffNetAmount 
	) C group by paymenttype 
) A 
right join @Paymenttype p on ltrim(p.Paymenttype) = a.Paymenttype
order by p.paymenttype
 
/*Sales Return Summary*/
Insert into  #Control(TranType, TranCount,PaymentMode,TransTotal,[Basic],TaxAmount,DiscountValue,[RoundOff],GroupSum,DisplayType )
select 'SalesReturn', sum(isnull(TranCount,0)),'Total(b)', sum(isnull(TransTotal,0)),sum(isnull([Basic],0)),sum(isnull(TaxAmount,0)),sum(isnull(DiscountValue,0)),sum(isnull([RoundOff],0)),1 ,1
from #Control where trantype ='SalesReturn'

/*Transfer Out*/
 insert into #Control(TranType, TranCount,PaymentMode,TransTotal,[Basic],TaxAmount,DiscountValue,[RoundOff],GroupSum,DisplayType  )
 SElect   'TransferOut' [Type], count(distinct st.Id) [Count], 'Tr. Out (c)' [PaymentType],sum(A.itemval- A.itemval* isnull(sti.gsttotal,0)/100)  [Basic],
  sum(A.itemval* isnull(sti.gsttotal,0)/100) [TaxAmount],round(sum(A.itemval),0) [TransTotal],0 [DiscountValue] ,
  round(sum(A.itemval),0) - round(sum(A.itemval),2) [RoundOff],1,0
    from StockTransferItem sti Inner join 
	(select * from StockTransfer where InstanceId =@InstanceId  and TransferDate between @startdate and @EndDate  and transferstatus in (1,3)) st on st.id =sti.transferid 
 cross apply (Select Quantity * PurchasePrice [ItemVal] ) A
 where sti.InstanceId =@InstanceId

 Insert into  #Control(TranType, TranCount,PaymentMode,TransTotal,[Basic],TaxAmount,DiscountValue,[RoundOff],GroupSum,DisplayType )
select 'Sales', sum(isnull(TranCount,0)),'Total(a-b+c)', sum(isnull(TransTotal,0)),sum(isnull([Basic],0)),sum(isnull(TaxAmount,0)),sum(isnull(DiscountValue,0)),sum(isnull([RoundOff],0)),1 ,2
from #Control where GroupSum =1

   Insert into  #Control( TranCount,PaymentMode,DisplayType )
Values (NULL,'',7), (NULL,'',7)

insert into #Control(PaymentMode,DisplayType) 
 Values ('Purchase',3)

/*Purchase*/
insert into #Control(TranType, TranCount,PaymentMode,TransTotal,[Basic],TaxAmount,DiscountValue,[RoundOff] )
Select   [Type],  [Count],p.PaymentType,   [TransTotal],
	      [Basic],
	     [TaxAmount], [DiscountValue],  [RoundOff] 
	   from(
	   Select   'Purchase' [Type],count(1) [Count],PaymentType, sum(round(ReportTotal,0)) [TransTotal],
	   sum(round(ReportTotal -[TaxValue],2))  [Basic],
	   sum(round([TaxValue],2)) [TaxAmount],sum(round(discount,2)) [DiscountValue],sum(round(ReportTotal,0) -round(ReportTotal,2)) [RoundOff] 
	   from(
	   SELECT  sum(pocal.discount) [discount], vendorpurchase.id,VendorPurchase.PaymentType,
		sum(  Round( (Isnull(pocal.Poval, 0)) * 
       (case when VendorPurchase.TaxRefNo = 1 then ISNULL(ProductStock.GstTotal,isnull(VendorPurchaseItem.GstTotal,0))					
	  else Isnull(productstock.VAT, 0) end) / 100, 2) ) [TaxValue],    
         sum(    ( ( Isnull(vendorpurchaseitem.packageqty, 0) - Isnull(vendorpurchaseitem.freeqty, 0) ) * 
               vendorpurchaseitem.packagepurchaseprice ) - ( ( ( Isnull( vendorpurchaseitem.packageqty, 0) - 
			   Isnull( vendorpurchaseitem.freeqty, 0) ) * vendorpurchaseitem.packagepurchaseprice ) * 
             Isnull(vendorpurchaseitem.discount, 0) / 100 ) + ( ( ( ( Isnull( vendorpurchaseitem.packageqty, 0) - Isnull(vendorpurchaseitem.freeqty, 0) ) * 
             vendorpurchaseitem.packagepurchaseprice ) - ( ( ( Isnull(vendorpurchaseitem.packageqty, 0) - 
		  Isnull(vendorpurchaseitem.freeqty, 0) ) * vendorpurchaseitem.packagepurchaseprice ) * Isnull(vendorpurchaseitem.discount, 0) / 100 ) ) * ( 
 		   (case  VendorPurchase.TaxRefNo  when 1 then ISNULL(ProductStock.GstTotal,isnull(VendorPurchaseItem.GstTotal,0)) else Isnull(productstock.vat, 0) end) / 100)		  ) )
		    ReportTotal
	 	  FROM   vendorpurchaseitem (nolock)
		  INNER JOIN (select * from vendorpurchase  (nolock) where cast(CreatedAt as date ) BETWEEN @StartDate AND @EndDate  
		  and instanceid =@instanceid   and isnull(CancelStatus ,0)= 0) vendorpurchase 
				  ON vendorpurchase.id = vendorpurchaseitem.vendorpurchaseid 
		  Left JOIN (select * from vendor (nolock) where accountid =@AccountId) vendor
				  ON vendor.id = vendorpurchase.vendorid 
		  INNER JOIN (select * from productstock (nolock) where instanceid =@instanceid )productstock
				  ON productstock.id = vendorpurchaseitem.productstockid 
		  Left JOIN (select * from product  where accountid  =@AccountId)product
				  ON product.id = productstock.productid 
		CRoss apply (select  Isnull(vendorpurchaseitem.packagepurchaseprice, 0) * ( 
							 Isnull ( vendorpurchaseitem.packageqty, 0) - Isnull ( vendorpurchaseitem.freeqty, 0) ) - ( 
							 Isnull( vendorpurchaseitem.packagepurchaseprice, 0) * ( Isnull ( vendorpurchaseitem.packageqty, 0) - 
							 Isnull ( vendorpurchaseitem.freeqty, 0) ) * Isnull ( vendorpurchaseitem.discount, 0) / 100 )  [Poval],
							( Isnull( vendorpurchaseitem.packagepurchaseprice, 0) * ( Isnull ( vendorpurchaseitem.packageqty, 0) - 
							Isnull ( vendorpurchaseitem.freeqty, 0) ) * Isnull ( vendorpurchaseitem.discount, 0) / 100 )  [Discount]) as pocal
		  WHERE   vendorpurchaseitem.accountid = @AccountId 
		  AND vendorpurchaseitem.instanceid = @InstanceId  
		  and (vendorpurchaseitem.Status is null or vendorpurchaseitem.Status = 1)
 group by VendorPurchase.PaymentType,vendorpurchase.id )c 
group by paymenttype  )  
 A right join @Paymenttype p on ltrim(p.Paymenttype) = a.PaymentType
 order by p.paymenttype
 
/*Purchase Summary*/
Insert into  #Control(TranType, TranCount,PaymentMode,TransTotal,[Basic],TaxAmount,DiscountValue,[RoundOff],GroupSum,DisplayType )
select 'Purchase', sum(isnull(TranCount,0)),'Total(d)', sum(isnull(TransTotal,0)),sum(isnull([Basic],0)),sum(isnull(TaxAmount,0)),sum(isnull(DiscountValue,0)),sum(isnull([RoundOff],0)),2 ,1
from #Control where trantype ='Purchase'

insert into #Control(PaymentMode,DisplayType) 
 Values ('Return',3)

 /*Vendor Return*/
 insert into #Control(TranType, TranCount,PaymentMode,TransTotal,[Basic],TaxAmount,DiscountValue,[RoundOff] )
  Select   [Type], [Count],p.PaymentType,  [TransTotal],
	    [Basic],[TaxAmount], 0 [DiscountValue],
	   0 [RoundOff] 
	   from (Select  'Purchase Return' [Type],count(1) [Count],PaymentType, sum([TransTotal]) * (-1) [TransTotal],
	   sum(round([TransTotal] - Tax,2))* (-1)  [Basic],
	   sum(round(Tax,2)) * (-1)  [TaxAmount],  0 [DiscountValue], 0 [RoundOff] 
	   from (
	   SELECT  sum(a.itemval)  [TransTotal],
	  sum( a.itemval -(  a.itemval *  100 / (100+ ISNULL(VendorReturnItem.GstTotal,productstock.GstTotal) ) ) ) [Tax],
		 vendorreturn.id,isnull(VendorPurchase.PaymentType, isnull(vendor.PaymentType,'Cash')) [PaymentType]
	 FROM   vendorreturnitem(nolock)  INNER JOIN 
	 (Select * from vendorreturn(nolock)   where instanceid =@InstanceId and returndate BETWEEN @StartDate AND @EndDate ) vendorreturn ON vendorreturn.id = vendorreturnitem.vendorreturnid LEFT JOIN 
	 (Select * from vendorpurchase(nolock)  where instanceid =@InstanceId ) vendorpurchase ON vendorpurchase.id = vendorreturn.vendorpurchaseid INNER JOIN 
	 (select * from vendor(nolock)  where accountid =@accountid ) Vendor ON vendor.id = vendorreturn.vendorid INNER JOIN 
	 (Select * from productstock(nolock)   where instanceid =@instanceid ) productstock ON productstock.id = vendorreturnitem.productstockid LEFT JOIN 
	 (Select * from vendorpurchaseitem(nolock)  where instanceid =@instanceid) vendorpurchaseitem ON vendorpurchaseitem.productstockid = productstock.id 
					 and VendorPurchaseItem.VendorPurchaseId = VendorReturn.VendorPurchaseId INNER JOIN 
	(Select * from product(nolock)   where accountid =@AccountId) product ON product.id = productstock.productid 
	  cross apply (select  isnull(vendorreturnitem.quantity,0) * isnull(VendorReturnItem.ReturnPurchasePrice,VendorPurchaseItem.PurchasePrice) [ItemVal]) A
      WHERE    vendorreturnitem.accountid = @AccountId 
             AND vendorreturnitem.instanceid = @InstanceId 
      group  BY vendorreturn.id ,vendor.PaymentType,VendorPurchase.PaymentType) c group by paymenttype 
	  ) A  right join @Paymenttype p on ltrim(p.Paymenttype) = a.PaymentType
 order by p.paymenttype
  

/*Purchase Return Summary*/
Insert into  #Control(TranType, TranCount,PaymentMode,TransTotal,[Basic],TaxAmount,DiscountValue,[RoundOff],GroupSum,DisplayType )
select 'Purchase Return' , sum(isnull(TranCount,0)),'Total(e)', sum(isnull(TransTotal,0)),sum(isnull([Basic],0)),sum(isnull(TaxAmount,0)),sum(isnull(DiscountValue,0)),sum(isnull([RoundOff],0)),2 ,1
from #Control where trantype ='Purchase Return' 

Update #Control set    TransTotal = TransTotal *(-1) ,[Basic] = [Basic] *(-1), TaxAmount = TaxAmount *(-1),DiscountValue = DiscountValue*(-1),
[RoundOff] = [RoundOff]* (-1)  where trantype in('Purchase Return' ,'SalesReturn')

 /*TransferIn*/
 insert into #Control(TranType, TranCount,PaymentMode,TransTotal,[Basic],TaxAmount,DiscountValue,[RoundOff] ,GroupSum,DisplayType )
  SElect  'TransferIn' [Type], count(distinct st.Id) [Count], 'Tr.In(f)' [PaymentType],
   sum(A.itemval- A.itemval* isnull(sti.gsttotal,0)/100)  [Basic],
   sum(A.itemval* isnull(sti.gsttotal,0)/100) [TaxAmount],round(sum(A.itemval),0) [TransTotal],0 [DiscountValue] ,
    round(sum(A.itemval),0) - round(sum(A.itemval),2) [RoundOff],2,0
   from StockTransferItem sti Inner join (select * from StockTransfer where ToInstanceId =@InstanceId
 and TransferDate between @startdate and @EndDate  and transferstatus =3) st on st.id =sti.transferid 
 cross apply (Select Quantity * PurchasePrice [ItemVal] ) A
 where sti.ToInstanceId =@InstanceId
  
   Insert into  #Control(TranType, TranCount,PaymentMode,TransTotal,[Basic],TaxAmount,DiscountValue,[RoundOff],GroupSum,DisplayType )
select 'Purchase', sum(isnull(TranCount,0)),'Total(d-e+f)', sum(isnull(TransTotal,0)),sum(isnull([Basic],0)),sum(isnull(TaxAmount,0)),sum(isnull(DiscountValue,0)),sum(isnull([RoundOff],0)),1 ,2
from #Control where GroupSum =2
  
  Update #Control set Trancount = 0 where isnull(TransTotal,0)  = 0

   Insert into  #Control( TranCount,PaymentMode ,DisplayType)
Values (NULL,'',7),(0,'Other Debits',9),(0,'Other Credits',9) ,(NULL,'',7)

 /*Opening STock Value*/
 	select @OpenQty   = sum(isnull(importqty,0)) ,@OpeningStock =sum(isnull(importqty,0)* 
	( (
	 CASE ISNULL(PackageSize,0) WHEN 0 THEN	ISNULL(PurchasePrice,0)  
	   ELSE  case ISNULL(PackagePurchasePrice,0)  when 0 then ISNULL(PurchasePrice,0) 
	    else   (ISNULL(PackagePurchasePrice,0)/ISNULL(PackageSize,0)  ) + ((ISNULL(PackagePurchasePrice,0)/ISNULL(PackageSize,0) )* (ISNULL(GstTotal,0)  /100) ) END  end ))) from productstock where instanceid = @InstanceId and isnull(StockImport,0) =1  
	   and cast(createdat as date) <= @StartDate  and Isnull(newopenedstock, 0)  =0  and accountid =@accountid and instanceid = @InstanceId  
 
/*SELECT  
/*@OpenQty = sum(isnull(temp.quantity ,0) + isnull(dc.quantity ,0) + isnull(qty.importqty,0) +isnull( qty.newstockqty,0) +isnull(vp.quantity,0)+isnull(sr.quantity,0)-isnull(si.quantity,0)-isnull(vr.quantity,0)-isnull(st1.quantity,0)+isnull(st.quantity,0)+isnull(ad.adjustedstock,0)+isnull(ad1.adjustedstock,0)-isnull(consumption.quantity,0)),
@OpeningStock = 
 sum( cast(isnull(temp.quantity ,0) + isnull(dc.quantity ,0) + isnull(qty.importqty,0) +isnull( qty.newstockqty,0) +isnull(vp.quantity,0)+isnull(sr.quantity,0)-isnull(si.quantity,0)-isnull(vr.quantity,0)-isnull(st1.quantity,0)+isnull(st.quantity,0)+isnull(ad.adjustedstock,0)+isnull(ad1.adjustedstock,0)-isnull(consumption.quantity,0) as decimal) * isnull (ps.purchaseprice,0) ) ,*/
 @OpenQty = isnull(@OpenQty,0) + sum(isnull(temp.quantity ,0) + isnull(dc.quantity ,0) + isnull(qty.importqty,0) +isnull( qty.newstockqty,0) +isnull(vp.quantity,0)+isnull(sr.quantity,0)-isnull(si.quantity,0)-isnull(vr.quantity,0)-isnull(st1.quantity,0)+isnull(st.quantity,0)+isnull(ad.adjustedstock,0)+isnull(ad1.adjustedstock,0)-isnull(consumption.quantity,0)),
@OpeningStock = isnull(@OpeningStock,0) +
sum( cast(isnull(temp.quantity ,0) + isnull(dc.quantity ,0) + isnull(qty.importqty,0) +isnull( qty.newstockqty,0) +isnull(vp.quantity,0)+isnull(sr.quantity,0)-isnull(si.quantity,0)-isnull(vr.quantity,0)-isnull(st1.quantity,0)+isnull(st.quantity,0)+isnull(ad.adjustedstock,0)+isnull(ad1.adjustedstock,0)-isnull(consumption.quantity,0) as decimal) * isnull (ps.purchaseprice,0) ) ,
 @TempOpeningQty = sum(isnull(temp.quantity ,0)),
 @TempOpeningValue= sum(isnull(temp.quantity ,0) * isnull (PurchasePrice,0) ) 
 FROM
(select * from productstock (nolock) where instanceid = @InstanceId   ) ps LEFT OUTER JOIN 
 (select sum(quantity) quantity, productstockid from salesitem (nolock) where instanceid = @InstanceId and cast(updatedat as date) < @StartDate  group by productstockid) si ON si.productstockid = ps.id LEFT OUTER JOIN 
(select sum(quantity) quantity, productstockid from salesreturnitem (nolock) where instanceid = @InstanceId  and cast(updatedat as date) < @StartDate group by productstockid) sr ON sr.productstockid = ps.id LEFT OUTER JOIN 
(select sum(quantity) quantity, productstockid from vendorpurchaseitem (nolock) where instanceid = @InstanceId and cast(updatedat as date) < @StartDate  group by productstockid) vp ON vp.productstockid = ps.id LEFT OUTER JOIN 
(select sum(quantity) quantity, productstockid from vendorreturnitem (nolock) where instanceid = @InstanceId and cast(updatedat as date) < @StartDate  group by productstockid) vr ON vr.productstockid = ps.id LEFT OUTER JOIN 
(select sum(quantity) quantity, productstockid from stocktransferitem (nolock) s1 INNER JOIN stocktransfer (nolock) s2 ON s2.id = s1.transferid where transferstatus in(1,3) AND s1.instanceid = @InstanceId  and cast(s1.updatedat as date) < @StartDate  group by s1.productstockid) st1 ON st1.productstockid = ps.id LEFT OUTER JOIN 
(select sum(quantity) quantity, toproductstockid from stocktransferitem (nolock) s1 INNER JOIN stocktransfer (nolock) s2 ON s2.id = s1.transferid where transferstatus = 3 AND s1.toinstanceid = @InstanceId  and cast(s1.updatedat as date) < @StartDate  group by s1.toproductstockid) st ON st.toproductstockid = ps.id LEFT OUTER JOIN 
(select sum(abs(adjustedstock)) adjustedstock, productstockid from stockadjustment (nolock) where instanceid = @InstanceId  and cast(updatedat as date) < @StartDate and adjustedstock<0  group by productstockid) ad ON ad.productstockid = ps.id LEFT OUTER JOIN 
(select sum(adjustedstock) adjustedstock, productstockid from stockadjustment (nolock) where instanceid = @InstanceId  and cast(updatedat as date) < @StartDate  and adjustedstock>0 group by productstockid) ad1 ON ad.productstockid = ps.id LEFT OUTER JOIN 
(select sum(quantity) quantity, productstockid from dcvendorpurchaseitem (nolock) where instanceid = @InstanceId    AND Isnull(ispurchased, 0) = 0  and cast(updatedat as date) < @StartDate  group by productstockid) dc ON dc.productstockid = ps.id LEFT OUTER JOIN  
(select sum(quantity) quantity, productstockid from tempvendorpurchaseitem (nolock) where instanceid = @InstanceId  AND isactive = 1  and isnull(isdeleted,0) =0  and cast(updatedat as date) < @StartDate  group by productstockid) temp ON temp.productstockid = ps.id LEFT OUTER JOIN  
(select sum(Isnull(newstockqty, 0)) newstockqty, 0 importqty, id as productstockid from productstock (nolock) where instanceid = @InstanceId  AND Isnull(newopenedstock, 0) = 1   and cast(updatedat as date) < @StartDate    group by id) [qty] ON [qty].productstockid = ps.id LEFT OUTER JOIN  
(select sum(Consumption) quantity, productstockid from SelfConsumption (nolock) where instanceid = @InstanceId   and cast(updatedat as date) < @StartDate  group by productstockid) consumption ON consumption.productstockid = ps.id  
 */

 SElect  @OpenQty = isnull(@OpenQty,0) + sum(isnull(temp ,0) + isnull(dc ,0) + isnull(imp,0) +isnull( new,0) +isnull(vp,0)+isnull(sr,0)-isnull(si,0)-isnull(vr,0)-isnull(st1,0)+isnull(st,0)+isnull(ad,0)+isnull(ad1,0)-isnull(consumption,0)),
 @OpeningStock = isnull(@OpeningStock,0)   + sum( CAST(isnull(temp ,0) + isnull(dc ,0) + isnull(imp,0) +isnull( new,0) +isnull(vp,0)+isnull(sr,0)-isnull(si,0)-isnull(vr,0)-isnull(st1,0)+isnull(st,0)+isnull(ad,0)+isnull(ad1,0)-isnull(consumption,0) AS DECIMAL)   * isnull (PUR,0)) ,
/*   SUM(isnull(temp ,0) ) temp, SUM(isnull(dc ,0) ) dc, SUM(isnull(imp,0) ) imp,SUM(isnull( new,0) ) new,SUM(isnull(vp,0)) vp,
 SUM(isnull(sr,0)) sr ,SUM(isnull(si,0)) si,SUM(isnull(vr,0)) vr,SUM(isnull(st1,0)) st1,SUM(isnull(st,0)) st,SUM(isnull(ad,0)) ad,SUM(isnull(ad1,0)) ad1,SUM(isnull(consumption,0)) consumption*/
  @TempOpeningQty = sum(isnull(temp ,0)),
 @TempOpeningValue= sum(isnull(temp ,0) * isnull (PUR,0) ) 
  FROM(
 Select ps.id, max(isnull(ps.purPRICE,0)) PUR,
     max(isnull(temp.quantity ,0) ) temp, max(isnull(dc.quantity ,0) ) dc, max(isnull(qty.importqty,0) ) imp,max(isnull( qty.newstockqty,0) ) new,max(isnull(vp.quantity,0)) vp,
	 max(isnull(sr.quantity,0)) sr ,max(isnull(si.quantity,0)) si,
	 max(isnull(vr.quantity,0)) vr,max(isnull(st1.quantity,0)) st1,max(isnull(st.quantity,0)) st,max(isnull(ad.adjustedstock,0)) ad,max(isnull(ad1.adjustedstock,0)) ad1,
	 max(isnull(consumption.quantity,0)) consumption
 FROM
(select  ( (
	 CASE ISNULL(PackageSize,0) WHEN 0 THEN	ISNULL(PurchasePrice,0)  
	   ELSE  case ISNULL(PackagePurchasePrice,0)  when 0 then ISNULL(PurchasePrice,0) 
	    else   (ISNULL(PackagePurchasePrice,0)/ISNULL(PackageSize,0)  ) + ((ISNULL(PackagePurchasePrice,0)/ISNULL(PackageSize,0) )* (ISNULL(GstTotal,0)  /100) ) END  end ))  purPRICE ,* from productstock (nolock) where instanceid = @InstanceId   ) ps LEFT OUTER JOIN 
 (select sum(quantity) quantity, productstockid from salesitem (nolock) where instanceid = @InstanceId and cast(updatedat as date) < @StartDate  group by productstockid) si ON si.productstockid = ps.id LEFT OUTER JOIN 
(select sum(quantity) quantity, productstockid from salesreturnitem (nolock) where instanceid = @InstanceId  and cast(updatedat as date) < @StartDate group by productstockid) sr ON sr.productstockid = ps.id LEFT OUTER JOIN 
(select sum(quantity) quantity, productstockid from vendorpurchaseitem (nolock) where instanceid = @InstanceId and cast(updatedat as date) < @StartDate  group by productstockid) vp ON vp.productstockid = ps.id LEFT OUTER JOIN 
(select sum(quantity) quantity, productstockid from vendorreturnitem (nolock) where instanceid = @InstanceId and cast(updatedat as date) < @StartDate  group by productstockid) vr ON vr.productstockid = ps.id LEFT OUTER JOIN 
(select sum(quantity) quantity, productstockid from stocktransferitem (nolock) s1 INNER JOIN stocktransfer (nolock) s2 ON s2.id = s1.transferid where transferstatus in(1,3) AND s1.instanceid = @InstanceId  and cast(s1.updatedat as date) < @StartDate  group by s1.productstockid) st1 ON st1.productstockid = ps.id LEFT OUTER JOIN 
(select sum(quantity) quantity, toproductstockid from stocktransferitem (nolock) s1 INNER JOIN stocktransfer (nolock) s2 ON s2.id = s1.transferid where transferstatus = 3 AND s1.toinstanceid = @InstanceId  and cast(s1.updatedat as date) < @StartDate  group by s1.toproductstockid) st ON st.toproductstockid = ps.id LEFT OUTER JOIN 
(select sum((adjustedstock)) adjustedstock, productstockid from stockadjustment (nolock) where instanceid = @InstanceId  and cast(updatedat as date) < @StartDate and adjustedstock<0  group by productstockid) ad ON ad.productstockid = ps.id LEFT OUTER JOIN 
(select sum(adjustedstock) adjustedstock, productstockid from stockadjustment (nolock) where instanceid = @InstanceId  and cast(updatedat as date) < @StartDate  and adjustedstock>0 group by productstockid) ad1 ON ad1.productstockid = ps.id LEFT OUTER JOIN 
(select sum(quantity) quantity, productstockid from dcvendorpurchaseitem (nolock) where instanceid = @InstanceId    AND Isnull(ispurchased, 0) = 0  and cast(updatedat as date) < @StartDate  group by productstockid) dc ON dc.productstockid = ps.id LEFT OUTER JOIN  
(select sum(quantity) quantity, productstockid from tempvendorpurchaseitem (nolock) where instanceid = @InstanceId  AND isactive = 1  and isnull(isdeleted,0) =0  and cast(updatedat as date) < @StartDate  group by productstockid) temp ON temp.productstockid = ps.id LEFT OUTER JOIN  
(select sum(Isnull(newstockqty, 0)) newstockqty, 0 importqty, id as productstockid from productstock (nolock) where instanceid = @InstanceId  AND Isnull(newopenedstock, 0) = 1   and cast(updatedat as date)  < @StartDate   group by id) [qty] ON [qty].productstockid = ps.id LEFT OUTER JOIN  

(select sum(Consumption) quantity, 0 importqty, productstockid from SelfConsumption (nolock) where instanceid = @InstanceId   and cast(updatedat as date) < @StartDate  group by productstockid) consumption ON consumption.productstockid = ps.id  
 
 group by ps.id) AS ONE

 /*Closing Stock Value */
 select @ClosingImportQty   = sum(isnull(importqty,0)) ,@ClosingImportValue =sum(isnull(importqty,0)*( (
	 CASE ISNULL(PackageSize,0) WHEN 0 THEN	ISNULL(PurchasePrice,0)  
	   ELSE  case ISNULL(PackagePurchasePrice,0)  when 0 then ISNULL(PurchasePrice,0) 
	    else   (ISNULL(PackagePurchasePrice,0)/ISNULL(PackageSize,0)  ) + ((ISNULL(PackagePurchasePrice,0)/ISNULL(PackageSize,0) )* (ISNULL(GstTotal,0)  /100) ) END  end ))) from productstock where instanceid = @InstanceId and isnull(StockImport,0) =1 
   and cast(createdat as date)  Between @StartDate and @EndDate and  Isnull(newopenedstock, 0)  =0  and accountid =@accountid and instanceid = @InstanceId  
 
 /*	    
SELECT  
@ClosingQty =isnull(@OpenQty,0)+ isnull(@ClosingImportQty,0) + sum(isnull(temp.quantity ,0) + isnull(dc.quantity ,0) + isnull(qty.importqty,0) +isnull( qty.newstockqty,0) +isnull(vp.quantity,0)+isnull(sr.quantity,0)-isnull(si.quantity,0)-isnull(vr.quantity,0)-isnull(st1.quantity,0)+isnull(st.quantity,0)+isnull(ad.adjustedstock,0)+isnull(ad1.adjustedstock,0)-isnull(consumption.quantity,0)),
@ClosingStock = isnull(@OpeningStock,0) +isnull(@ClosingImportValue,0) +
 sum( cast(isnull(temp.quantity ,0) + isnull(dc.quantity ,0) + isnull(qty.importqty,0) +isnull( qty.newstockqty,0) +isnull(vp.quantity,0)+isnull(sr.quantity,0)-isnull(si.quantity,0)-isnull(vr.quantity,0)-isnull(st1.quantity,0)+isnull(st.quantity,0)+isnull(ad.adjustedstock,0)+isnull(ad1.adjustedstock,0)-isnull(consumption.quantity,0) as decimal) * isnull (ps.purchaseprice,0) ) ,
 @TempClosing =isnull(@TempOpeningQty,0) + sum(isnull(temp.quantity ,0)),
 @TempClosingValue= isnull(@TempOpeningValue,0) + sum(isnull(temp.quantity ,0) * isnull (PurchasePrice,0) ) 
 FROM
(select * from productstock (nolock) where instanceid = @InstanceId ) ps LEFT OUTER JOIN 
(select sum(quantity) quantity, productstockid from salesitem (nolock) where instanceid = @InstanceId and cast(updatedat as date) Between @StartDate and @EndDate  group by productstockid) si ON si.productstockid = ps.id LEFT OUTER JOIN 
(select sum(quantity) quantity, productstockid from salesreturnitem (nolock) where instanceid = @InstanceId  and cast(updatedat as date) Between @StartDate and @EndDate group by productstockid) sr ON sr.productstockid = ps.id LEFT OUTER JOIN 
(select sum(quantity) quantity, productstockid from vendorpurchaseitem (nolock) where instanceid = @InstanceId and cast(updatedat as date) Between @StartDate and @EndDate  group by productstockid) vp ON vp.productstockid = ps.id LEFT OUTER JOIN 
(select sum(quantity) quantity, productstockid from vendorreturnitem (nolock) where instanceid = @InstanceId and cast(updatedat as date) Between @StartDate and @EndDate  group by productstockid) vr ON vr.productstockid = ps.id LEFT OUTER JOIN 
(select sum(quantity) quantity, productstockid from stocktransferitem (nolock) s1 INNER JOIN stocktransfer (nolock) s2 ON s2.id = s1.transferid where transferstatus in(1,3) AND s1.instanceid = @InstanceId  and cast(s1.updatedat as date) Between @StartDate and @EndDate  group by s1.productstockid) st1 ON st1.productstockid = ps.id LEFT OUTER JOIN 
(select sum(quantity) quantity, toproductstockid from stocktransferitem (nolock) s1 INNER JOIN stocktransfer (nolock) s2 ON s2.id = s1.transferid where transferstatus = 3 AND s1.toinstanceid = @InstanceId  and cast(s1.updatedat as date) Between @StartDate and @EndDate  group by s1.toproductstockid) st ON st.toproductstockid = ps.id LEFT OUTER JOIN 
(select sum(abs(adjustedstock)) adjustedstock, productstockid from stockadjustment (nolock) where instanceid = @InstanceId  and cast(updatedat as date) Between @StartDate and @EndDate and adjustedstock<0  group by productstockid) ad ON ad.productstockid = ps.id LEFT OUTER JOIN 
(select sum(adjustedstock) adjustedstock, productstockid from stockadjustment (nolock) where instanceid = @InstanceId  and cast(updatedat as date) Between @StartDate and @EndDate  and adjustedstock>0 group by productstockid) ad1 ON ad.productstockid = ps.id LEFT OUTER JOIN 
(select sum(quantity) quantity, productstockid from dcvendorpurchaseitem (nolock) where instanceid = @InstanceId    AND Isnull(ispurchased, 0) = 0  and cast(updatedat as date) Between @StartDate and @EndDate  group by productstockid) dc ON dc.productstockid = ps.id LEFT OUTER JOIN  
(select sum(quantity) quantity, productstockid from tempvendorpurchaseitem (nolock) where instanceid = @InstanceId  AND isactive = 1  and isnull(isdeleted,0) =0 and cast(updatedat as date) Between @StartDate and @EndDate  group by productstockid) temp ON temp.productstockid = ps.id LEFT OUTER JOIN  
(select sum(Isnull(newstockqty, 0)) newstockqty, 0 importqty, id as productstockid from productstock (nolock) where instanceid = @InstanceId  AND Isnull(newopenedstock, 0) = 1   and cast(updatedat as date) Between @StartDate and @EndDate  group by id) [qty] ON [qty].productstockid = ps.id LEFT OUTER JOIN  
(select sum(Consumption) quantity, productstockid from SelfConsumption (nolock) where instanceid = @InstanceId   and cast(updatedat as date) Between @StartDate and @EndDate  group by productstockid) consumption ON consumption.productstockid = ps.id 
 */
 
	 SElect  @ClosingQty = isnull(@OpenQty,0)+ isnull(@ClosingImportQty,0) + sum(isnull(temp ,0) + isnull(dc ,0) + isnull(imp,0) +isnull( new,0) +isnull(vp,0)+isnull(sr,0)-isnull(si,0)-isnull(vr,0)-isnull(st1,0)+isnull(st,0)+isnull(ad,0)+isnull(ad1,0)-isnull(consumption,0)),
 @ClosingStock = isnull(@OpeningStock,0) +isnull(@ClosingImportValue,0) + sum( CAST(isnull(temp ,0) + isnull(dc ,0) + isnull(imp,0) +isnull( new,0) +isnull(vp,0)+isnull(sr,0)-isnull(si,0)-isnull(vr,0)-isnull(st1,0)+isnull(st,0)+isnull(ad,0)+isnull(ad1,0)-isnull(consumption,0) AS DECIMAL)   * isnull (PUR,0)) ,
   @TempClosing =isnull(@TempOpeningQty,0) + sum(isnull(temp ,0)),
 @TempClosingValue= isnull(@TempOpeningValue,0) + sum(isnull(temp ,0) * isnull (pur,0) )  
 
  /*  SUM(isnull(temp ,0) ) temp, SUM(isnull(dc ,0) ) dc, SUM(isnull(imp,0) ) imp,SUM(isnull( new,0) ) new,SUM(isnull(vp,0)) vp,
 SUM(isnull(sr,0)) sr ,SUM(isnull(si,0)) si,SUM(isnull(vr,0)) vr,SUM(isnull(st1,0)) st1,SUM(isnull(st,0)) st,SUM(isnull(ad,0)) ad,SUM(isnull(ad1,0)) ad1,SUM(isnull(consumption,0)) consumption  
 SUM(isnull(temp ,0) ) temp, SUM(isnull(dc ,0) ) dc, SUM(isnull(imp,0) ) imp,SUM(isnull( new,0) ) new,SUM(isnull(vp,0)) vp,
 SUM(isnull(sr,0)) sr ,SUM(isnull(si,0)) si,SUM(isnull(vr,0)) vr,SUM(isnull(st1,0)) st1,SUM(isnull(st,0)) st,SUM(isnull(ad,0)) ad,SUM(isnull(ad1,0)) ad1,SUM(isnull(consumption,0)) consumption
 
 */ FROM(
 Select ps.id, max(isnull(ps.purPRICE,0)) PUR,
     max(isnull(temp.quantity ,0) ) temp, max(isnull(dc.quantity ,0) ) dc, max(isnull(qty.importqty,0) ) imp,max(isnull( qty.newstockqty,0) ) new,max(isnull(vp.quantity,0)) vp,
	 max(isnull(sr.quantity,0)) sr ,max(isnull(si.quantity,0)) si,
	 max(isnull(vr.quantity,0)) vr,max(isnull(st1.quantity,0)) st1,max(isnull(st.quantity,0)) st,max(isnull(ad.adjustedstock,0)) ad,max(isnull(ad1.adjustedstock,0)) ad1,
	 max(isnull(consumption.quantity,0)) consumption
 

 FROM
(select ( (
	 CASE ISNULL(PackageSize,0) WHEN 0 THEN	ISNULL(PurchasePrice,0)  
	   ELSE  case ISNULL(PackagePurchasePrice,0)  when 0 then ISNULL(PurchasePrice,0) 
	    else   (ISNULL(PackagePurchasePrice,0)/ISNULL(PackageSize,0)  ) + ((ISNULL(PackagePurchasePrice,0)/ISNULL(PackageSize,0) )* (ISNULL(GstTotal,0)  /100) ) END  end ))   purPRICE ,* from productstock (nolock) where instanceid = @InstanceId ) ps LEFT OUTER JOIN 
(select sum(quantity) quantity, productstockid from salesitem (nolock) where instanceid = @InstanceId and cast(updatedat as date) Between @StartDate and @EndDate  group by productstockid) si ON si.productstockid = ps.id LEFT OUTER JOIN 
(select sum(quantity) quantity, productstockid from salesreturnitem (nolock) where instanceid = @InstanceId  and cast(updatedat as date) Between @StartDate and @EndDate group by productstockid) sr ON sr.productstockid = ps.id LEFT OUTER JOIN 
(select sum(quantity) quantity, productstockid from vendorpurchaseitem (nolock) where instanceid = @InstanceId and cast(updatedat as date) Between @StartDate and @EndDate  group by productstockid) vp ON vp.productstockid = ps.id LEFT OUTER JOIN 
(select sum(quantity) quantity, productstockid from vendorreturnitem (nolock) where instanceid = @InstanceId and cast(updatedat as date) Between @StartDate and @EndDate  group by productstockid) vr ON vr.productstockid = ps.id LEFT OUTER JOIN 
(select sum(quantity) quantity, productstockid from stocktransferitem (nolock) s1 INNER JOIN stocktransfer (nolock) s2 ON s2.id = s1.transferid where transferstatus in(1,3) AND s1.instanceid = @InstanceId  and cast(s1.updatedat as date) Between @StartDate and @EndDate  group by s1.productstockid) st1 ON st1.productstockid = ps.id LEFT OUTER JOIN 
(select sum(quantity) quantity, toproductstockid from stocktransferitem (nolock) s1 INNER JOIN stocktransfer (nolock) s2 ON s2.id = s1.transferid where transferstatus = 3 AND s1.toinstanceid = @InstanceId  and cast(s1.updatedat as date) Between @StartDate and @EndDate  group by s1.toproductstockid) st ON st.toproductstockid = ps.id LEFT OUTER JOIN 
(select sum((adjustedstock)) adjustedstock, productstockid from stockadjustment (nolock) where instanceid = @InstanceId  and cast(updatedat as date) Between @StartDate and @EndDate and adjustedstock<0  group by productstockid) ad ON ad.productstockid = ps.id LEFT OUTER JOIN 
(select sum(adjustedstock) adjustedstock, productstockid from stockadjustment (nolock) where instanceid = @InstanceId  and cast(updatedat as date) Between @StartDate and @EndDate  and adjustedstock>0 group by productstockid) ad1 ON ad1.productstockid = ps.id LEFT OUTER JOIN 
(select sum(quantity) quantity, productstockid from dcvendorpurchaseitem (nolock) where instanceid = @InstanceId    AND Isnull(ispurchased, 0) = 0  and cast(updatedat as date) Between @StartDate and @EndDate  group by productstockid) dc ON dc.productstockid = ps.id LEFT OUTER JOIN  
(select sum(quantity) quantity, productstockid from tempvendorpurchaseitem (nolock) where instanceid = @InstanceId  AND isactive = 1  and isnull(isdeleted,0) =0 and cast(updatedat as date) Between @StartDate and @EndDate  group by productstockid) temp ON temp.productstockid = ps.id LEFT OUTER JOIN  
(select sum(Isnull(newstockqty, 0)) newstockqty, 0 importqty, id as productstockid from productstock (nolock) where instanceid = @InstanceId  AND Isnull(newopenedstock, 0) = 1   and cast(updatedat as date) Between @StartDate and @EndDate  group by id) [qty] ON [qty].productstockid = ps.id LEFT OUTER JOIN  

(select sum(Consumption) quantity, productstockid from SelfConsumption (nolock) where instanceid = @InstanceId   and cast(updatedat as date) Between @StartDate and @EndDate  group by productstockid) consumption ON consumption.productstockid = ps.id 
 
 group by ps.id) AS ONE
/*Adjustment */

Select @AdjQty = sum(ad.Qty ),  @AdjStock = sum(isnull(ad.Qty ,0) * isnull(ps.purchaseprice,0)) from 
(select sum(adjustedstock) Qty, productstockid from stockadjustment (nolock) where instanceid = @InstanceId  and cast(updatedat as date) between @StartDate and @EndDate group by productstockid) ad  
Inner join (Select * from productstock (nolock)  where instanceid =@instanceid ) ps on ps.id = ad.productstockid 

/*Temp closing*/
Select @TempClosing = isnull(@TempOpeningQty,0) + sum(temp.Qty ),  @TempClosingValue = isnull(@TempOpeningValue,0)+ sum(isnull(temp.Qty ,0) * isnull(ps.purchaseprice,0)) from 
(select sum(quantity) Qty, productstockid from tempvendorpurchaseitem (nolock) where instanceid = @InstanceId  and cast(updatedat as date) between @StartDate and @EndDate AND isactive = 1   and isnull(isdeleted,0) =0 group by productstockid) temp  
Inner join (Select * from productstock (nolock)  where instanceid =@instanceid ) ps on ps.id = temp.productstockid 

--SElect @OpenQty  OpenQty, @OpeningStock OpeningStock,@ClosingQty ClosingQty,
--@ClosingStock ClosingStock,@AdjQty AdjQty,@AdjStock AdjStock,@TempOpeningQty TempOpeningQty, @TempOpeningValue TempOpeningValue,@TempClosing TempClosing  ,@TempClosingValue TempClosingValue
/*Closing and Opening stock*/
--select  @ClosingStock=  sum(isnull(Stockvalue,0)), @ClosingQty= sum(isnull(Closingstock,0))  from 
-- (Select productstockid, max(Transdate) [TransDate] from closingstock (nolock) where instanceid =@Instanceid and Transdate <=@EndDate 
-- and closingstock>=0
-- group by Productstockid ) ts inner join 
--  (Select *    from closingstock (nolock) where instanceid =@Instanceid and Transdate <=@EndDate ) cs  on cs.Productstockid= ts.Productstockid
-- inner join  (Select *    from productstock (nolock) where instanceid =@Instanceid  ) ps  on ps.id= ts.Productstockid
-- inner join  (Select *    from product  (nolock) where AccountId =@AccountId  ) p   on p.Id= ps.ProductId
--  and cs.transdate = ts.transdate
 

--select  @OpeningStock=  sum(isnull(Stockvalue,0)), @OPenQty= sum(isnull(Closingstock,0))  from 
-- (Select productstockid, max(Transdate) [TransDate] from closingstock (nolock) where instanceid =@Instanceid and Transdate < @StartDate 
--  and closingstock >=0
-- group by Productstockid ) ts inner join 
--  (Select *    from closingstock (nolock) where instanceid =@Instanceid and Transdate < @StartDate ) cs  on cs.Productstockid= ts.Productstockid
--   inner join  (Select *    from productstock (nolock) where instanceid =@Instanceid  ) ps  on ps.id= ts.Productstockid
-- inner join  (Select *    from product  (nolock) where AccountId =@AccountId  ) p   on p.Id= ps.ProductId
--  and cs.transdate = ts.transdate
 
  select    @OpeningStock=  sum(isnull(a.stockval,0)), @OPenQty=   sum(isnull(a.stock ,0))  from
(
select  cs.productstockid , cs.instanceid,  max(isnull(Stockvalue,0)) stockval,  max(isnull(Closingstock,0)) stock  from 
 (Select productstockid, max(Transdate) [TransDate] from closingstock (nolock) where accountid =@AccountId  and instanceid =@InstanceId  and Transdate < @StartDate
 group by Productstockid ) ts inner join 
  (Select *    from closingstock (nolock) where accountid =@AccountId and Transdate < @StartDate and instanceid =@InstanceId) cs  on cs.Productstockid= ts.Productstockid
  and cs.transdate = ts.transdate 
  --inner join (select * from productstock(nolock) where  accountid =@AccountId  and instanceid =@InstanceId ) ps on ps.id = cs.productstockid 
  group by  cs.productstockid , cs.instanceid
 ) a 
   select    @ClosingStock=  sum(isnull(a.stockval,0)), @ClosingQty=   sum(isnull(a.stock ,0))  from
(
select  cs.productstockid , cs.instanceid,  max(isnull(Stockvalue,0)) stockval,  max(isnull(Closingstock,0)) stock  from 
 (Select productstockid, max(Transdate) [TransDate] from closingstock (nolock) where accountid =@AccountId  and instanceid =@InstanceId  and Transdate <= @EndDate
 group by Productstockid ) ts inner join 
  (Select *    from closingstock (nolock) where accountid =@AccountId and Transdate <= @EndDate  and instanceid =@InstanceId) cs  on cs.Productstockid= ts.Productstockid
  and cs.transdate = ts.transdate 
  --inner join (select * from productstock(nolock) where  accountid =@AccountId  and instanceid =@InstanceId ) ps on ps.id = cs.productstockid 
  group by  cs.productstockid , cs.instanceid
 ) a 
 

insert into #Control(PaymentMode,DisplayType) 
 Values ('Accounts',3),
	('Transaction',4)

/*Sales cancel*/
insert into #Control(TranType, TranCount,PaymentMode,TransTotal,[Basic],TaxAmount,DiscountValue,[RoundOff],displaytype )
Select  
	'SalesCancel' [Type], 
	Count(DISTINCT Id) [SalesCancelCount], 
	'Sales' [PaymentType], 
	sum(transtotal) [TransTotal],  
	sum([Basic]) [Basic],
	sum(taxamount) [TaxAmount], 
	sum(discountvalue) [DiscountValue],
	sum(roundoff) [RoundOff],
	5 AS displaytype
	from (
		SELECT   
		S.Id,
		case isnull(CashType,'') when 'Full' then isnull(PaymentType,'') else isnull(CashType,'') end [PaymentType],
		Convert(date,S.invoicedate) invoicedate,
		--round((case right(cast(sum(a.itemval - a.discount) as decimal(18,2)) ,2) when 50 then round(sum(a.itemval - a.discount)+0.5 ,0) else round(sum(a.itemval - a.discount) ,0) end) ,2) [TransTotal],
		S.SaleAmount AS [TransTotal],
		--Round(SUM(a.Discount) ,2) as DiscountValue,
		ISNULL(S.TotalDiscountValue,0) AS DiscountValue,
		--Round(sum((a.itemval - a.discount) *100/(100+ISNULL(SI.GstTotal, 0))),2) [Basic],
		(S.SalesItemAmount-ISNULL(S.TotalDiscountValue,0))-S.GstAmount AS [Basic],
		--Round(sum((a.itemval - a.discount) - ((a.itemval - a.discount ) *100/(100+ISNULL(SI.GstTotal, 0)))),2) [TaxAmount] ,
		S.GstAmount AS [TaxAmount],
		--round((case right(cast(sum(a.itemval - a.discount) as decimal(18,2)) ,2) when  50 then round(sum(a.itemval - a.discount)+0.5 ,0) else round(sum(a.itemval - a.discount) ,0) end)- round(sum(a.itemval - a.discount) ,2) ,2) [RoundOff]
		S.RoundoffSaleAmount AS [RoundOff]
		FROM Sales S WITH(NOLOCK) INNER JOIN (Select * from SalesItem WITH(NOLOCK) where AccountId = @AccountId AND InstanceId = @InstanceId) SI  on S.id= SI.salesid
		INNER JOIN (Select * from ProductStock WITH(NOLOCK) where AccountId = @AccountId AND InstanceId = @InstanceId) PS on PS.Id=SI.productstockid
		--cross apply (Select (SI.Quantity) * (isNull(SI.SellingPrice, ISNULL(PS.SellingPrice,0))) [ItemVal], ((SI.Quantity) * ISNULL(SI.SellingPrice, ISNULL(PS.SellingPrice,0)) * case when ISNULL(s.Discount,0) > 0 then s.Discount else ISNULL(si.Discount,0) end / 100) [Discount] ) as A
		WHERE S.AccountId = @AccountId and S.InstanceId = @InstanceId
		AND Convert(date,S.invoicedate) BETWEEN @StartDate AND @EndDate
		and s.cancelstatus is not null
		group by isnull(CashType,'') ,isnull(PaymentType,''),s.id, Convert(date,S.invoicedate),
		S.SaleAmount,S.SalesItemAmount,ISNULL(S.TotalDiscountValue,0),S.GstAmount,S.RoundoffSaleAmount
	) a 

/*Purchase Cancel*/
insert into #Control(TranType, TranCount,PaymentMode,TransTotal,[Basic],TaxAmount,DiscountValue,[RoundOff] ,displaytype )
	   Select  'PurchaseCancel' [Type], count(1) [PurchasecancelCount],'Purchase', sum(round(ReportTotal,0)) [TransTotal],
	   sum(round(ReportTotal -[TaxValue],2))  [Basic],
	   sum(round([TaxValue],2)) [TaxAmount],sum(round(discount,2)) [DiscountValue],
	   sum(round(ReportTotal,0) -round(ReportTotal,2)) [RoundOff] ,5
	   from(
	   SELECT  sum(pocal.discount) [discount], vendorpurchase.id,VendorPurchase.PaymentType,
		sum(  Round( (Isnull(pocal.Poval, 0)) * 
       (case when VendorPurchase.TaxRefNo = 1 then ISNULL(ProductStock.GstTotal,isnull(VendorPurchaseItem.GstTotal,0))					
	  else Isnull(productstock.VAT, 0) end) / 100, 2) ) [TaxValue],    
         sum(    ( ( Isnull(vendorpurchaseitem.packageqty, 0) - Isnull(vendorpurchaseitem.freeqty, 0) ) * 
               vendorpurchaseitem.packagepurchaseprice ) - ( ( ( Isnull( vendorpurchaseitem.packageqty, 0) - 
			   Isnull( vendorpurchaseitem.freeqty, 0) ) * vendorpurchaseitem.packagepurchaseprice ) * 
             Isnull(vendorpurchaseitem.discount, 0) / 100 ) + ( ( ( ( Isnull( vendorpurchaseitem.packageqty, 0) - Isnull(vendorpurchaseitem.freeqty, 0) ) * 
             vendorpurchaseitem.packagepurchaseprice ) - ( ( ( Isnull(vendorpurchaseitem.packageqty, 0) - 
		  Isnull(vendorpurchaseitem.freeqty, 0) ) * vendorpurchaseitem.packagepurchaseprice ) * Isnull(vendorpurchaseitem.discount, 0) / 100 ) ) * ( 
 		   (case when VendorPurchase.TaxRefNo = 1 then ISNULL(ProductStock.GstTotal,isnull(VendorPurchaseItem.GstTotal,0)) else Isnull(productstock.vat, 0) end) / 100)		  ) )
		    ReportTotal
	 	  FROM   vendorpurchaseitem (nolock)
		  INNER JOIN (select * from vendorpurchase  (nolock) where cast(CreatedAt as date ) BETWEEN @StartDate AND @EndDate  
		  and instanceid =@instanceid   and isnull(CancelStatus ,0) != 0) vendorpurchase 
				  ON vendorpurchase.id = vendorpurchaseitem.vendorpurchaseid 
		  INNER JOIN (select * from vendor (nolock) where accountid =@AccountId) vendor
				  ON vendor.id = vendorpurchase.vendorid 
		  INNER JOIN (select * from productstock (nolock) where instanceid =@instanceid )productstock
				  ON productstock.id = vendorpurchaseitem.productstockid 
		  Left JOIN (select * from product  where accountid  =@AccountId)product
				  ON product.id = productstock.productid 
		CRoss apply (select  Isnull(vendorpurchaseitem.packagepurchaseprice, 0) * ( 
							 Isnull ( vendorpurchaseitem.packageqty, 0) - Isnull ( vendorpurchaseitem.freeqty, 0) ) - ( 
							 Isnull( vendorpurchaseitem.packagepurchaseprice, 0) * ( Isnull ( vendorpurchaseitem.packageqty, 0) - 
							 Isnull ( vendorpurchaseitem.freeqty, 0) ) * Isnull ( vendorpurchaseitem.discount, 0) / 100 )  [Poval],
							( Isnull( vendorpurchaseitem.packagepurchaseprice, 0) * ( Isnull ( vendorpurchaseitem.packageqty, 0) - 
							Isnull ( vendorpurchaseitem.freeqty, 0) ) * Isnull ( vendorpurchaseitem.discount, 0) / 100 )  [Discount]) as pocal
		  WHERE   vendorpurchaseitem.accountid = @AccountId 
		  AND vendorpurchaseitem.instanceid = @InstanceId  
		  and (vendorpurchaseitem.Status is null or vendorpurchaseitem.Status = 1)
 group by VendorPurchase.PaymentType,vendorpurchase.id )
 A 
 
--insert into #Control(PaymentMode,DisplayType) 
-- Values ('Accounts',3),
--	('Transaction',4)

   Insert into  #Control( TranCount,PaymentMode ,DisplayType)
Values (NULL,'',7),(NULL,'',6)

  if (@IsDisplayStock =1)
  begin
   insert into #Control(  paymentmode, trancount,transtotal,DisplayType)
	Values('Opening Stock',  @OpenQty   , @OpeningStock,5  ),
	  ('Closing Stock', @ClosingQty ,@ClosingStock,5) 
  end 
 insert into #Control(  paymentmode, trancount,transtotal,DisplayType)
 Values 
	  ('Stock Adjustment',@AdjQty,@AdjStock,5),
	    ('Opening Temp Stock ',@TempOpeningQty,@TempOpeningValue,5),
		('Closing Temp Stock ',@TempClosing,@TempClosingValue,5) 
--Values('Opening Stock',  @OpenQty   , @OpeningStock,5  ),
--	  ('Closing Stock', @ClosingQty ,@ClosingStock,5),
--	  ('Stock Adjustment',@AdjQty,@AdjStock,5),
--	    ('Opening Temp Stock ',@TempOpeningQty,@TempOpeningValue,5),
--		('Closing Temp Stock ',@TempClosing,@TempClosingValue,5) 
 
Select * from #Control
 
end