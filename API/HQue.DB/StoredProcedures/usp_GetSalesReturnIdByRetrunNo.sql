﻿
CREATE PROC usp_GetSalesReturnIdByRetrunNo(@Type INT, @TypeValue varchar(20), @Value varchar(5), @accountid char(36), @instanceid char(36))
AS
BEGIN

IF @Type = 1
BEGIN
select Id from SalesReturn 
WHERE ReturnNo = @Value AND AccountId = @accountid and InstanceId=@instanceid 
order by ReturnDate desc
END

ELSE
BEGIN
select Id from SalesReturn 
WHERE InvoiceSeries = @TypeValue AND ReturnNo = @Value AND AccountId = @accountid and InstanceId=@instanceid  
order by ReturnDate desc
END

END