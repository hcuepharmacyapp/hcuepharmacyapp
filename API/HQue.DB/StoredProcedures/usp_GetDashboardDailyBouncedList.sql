 -- Exec usp_GetDashboardDailyBouncedList '18204879-99ff-4efd-b076-f85b4a0da0a3'
 CREATE PROCEDURE [dbo].[usp_GetDashboardDailyBouncedList] (@AccountId varchar(36))
 AS
 BEGIN
   SET NOCOUNT ON
  
	SELECT Branch,SUM(BouncedQty) AS BouncedQty FROM (
	select Instance.Name AS BRANCH,MissedOrder.Quantity as BouncedQty
	from Account
	--INNER JOIN HQueUser on HQueUser.Accountid=Account.id
	INNER JOIN Instance on Instance.AccountId = Account.id -- AND HQueUser.InstanceId is NULL AND HQueUser.Userid!=''
	INNER JOIN MissedOrder on  MissedOrder.AccountId = Account.id AND MissedOrder.InstanceId=INSTANCE.ID  
	INNER JOIN Product on Product.id = MissedOrder.ProductId --AND Product.InstanceId=INSTANCE.ID 
	WHERE Account.id =@AccountId  and Convert(date,MissedOrder.CreatedAt)  = dateadd(day,datediff(day,1,GETDATE()),0) 
	group by Instance.Name,MissedOrder.Quantity
	UNION
	select Instance.Name AS Branch,'0.00' as BouncedQty
	from Account
	--INNER JOIN HQueUser on HQueUser.Accountid=Account.id
	INNER JOIN Instance on Instance.AccountId = Account.id --AND HQueUser.InstanceId is NULL AND HQueUser.Userid!=''           
	WHERE Account.id =@AccountId --and HQueUser.UserId='saravanan@goclinix.com'
	group by Instance.Name 
	) AS SS GROUP BY SS.BRANCH

 END
  



