/*******************************************************************************                            
** Change History                              
*******************************************************************************                            
** Date        Author          Description                              
*******************************************************************************        
**29/11/18     Sumathi 		SP Created to capture the last synced Id from offline
*******************************************************************************/
CREATE proc [dbo].[usp_Reverse_SyncData_Seed_Insert](@AccountId CHAR(36),@InstanceId  CHAR(36),@SeedIndex INT,@CreatedAt DATETIME)
AS

BEGIN
	Insert into Reverse_SyncData_Seed (AccountId,InstanceId,SeedIndex,CreatedAt)
		Values (@AccountId,@InstanceId,@SeedIndex,@CreatedAt)

	SELECT 'success' 
END