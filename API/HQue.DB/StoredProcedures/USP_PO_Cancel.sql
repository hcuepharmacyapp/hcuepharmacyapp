 
/*                               
******************************************************************************                            
** File: [usp_po_cancel]   
** Name: [usp_po_cancel]                               
** Description: To Update PO Cancel details
**   
** This template can be customized:                               
**                                
** Called by:                                
**                                
**  Parameters:                               
**  Input                Output                               
**  ----------              -----------                               
**   
** Author:Poongodi R   
** Created Date: 28/02/2017   
**   
*******************************************************************************                            
** Change History                               
*******************************************************************************                            
** Date        Author          Description                               
*******************************************************************************         
 ** 04/05/2017 Poongodi R	  Vendor Purchase Item Deleted Status Included
**	11/02/2017 Poongodi R	  Tempstock validation moved in first 
*******************************************************************************/ 
CREATE PROC dbo.usp_po_cancel (@InstanceId CHAR(36), 
@AccountId                                CHAR(36), 
@PurchaseId                               CHAR(36), 
@Updatedby                                varCHAR(300) ) 
AS 
  BEGIN 
  --declare @Userid char (36)
 
 IF EXISTS 
    ( 
               SELECT     * 
               FROM       VendorPurchaseItem VPI 
               
               WHERE      VPI.instanceid =@InstanceId 
               AND        VPI.accountid =@AccountId 
               AND        VPI.VendorPurchaseId  =@PurchaseId
			   and isnull(fromTempId ,'') <>''
               ) 
    BEGIN 
      SELECT 6 POStatus 
      RETURN 
    END 
    IF (EXISTS 
    ( 
        select vpi.ProductStockId     from VendorPurchaseItem  vpi inner join ProductStock ps on ps.id = vpi.ProductStockId 
where vpi.VendorPurchaseId =@PurchaseId  and isnull(vpi.Status ,0)Not in (2)
group  by vpi.ProductStockId having  sum(stock -Quantity) < 0))
  
    BEGIN 
      SELECT 2 POStatus 
      RETURN 
    END 
  
    IF EXISTS 
    ( 
           SELECT * 
           FROM   vendorreturn 
           WHERE  vendorpurchaseid = @PurchaseId)  
   BEGIN 
    SELECT 1 POStatus 
    RETURN 
  END
    
    IF EXISTS 
    ( 
               SELECT     * 
               FROM       VendorPurchaseItem VPI 
               
               WHERE      VPI.instanceid =@InstanceId 
               AND        VPI.accountid =@AccountId 
               AND        VPI.VendorPurchaseId  =@PurchaseId
			   and isnull(fromDcId ,'') <>''
               ) 
    BEGIN 
      SELECT 5 POStatus 
      RETURN 
    END 
 
    IF EXISTS 
    ( 
               SELECT     * 
               FROM       Payment  P 
               
               WHERE      P.instanceid =@InstanceId 
               AND        P.accountid =@AccountId 
               AND        P.VendorPurchaseId  =@PurchaseId
			   and isnull(p.Debit ,0) >0
               ) 
    BEGIN 
      SELECT 3 POStatus 
      RETURN 
    END 
 
  /*Get user id from user table based on the loginid*/
    --select @Userid =  id from HQueUser where userid =@Updatedby and isnull([Password] ,'')<>''
  /*Update Product Stock*/
  UPDATE productstock 
  SET        stock = stock - vpi.quantity, 
             updatedby =@Updatedby, 
             updatedat = Getdate() 
  FROM       productstock PS 
  INNER JOIN vendorpurchaseitem VPI 
  ON         VPI.productstockid =Ps.id 
  AND        VPI.vendorpurchaseid =@PurchaseId
   and isnull(vpi.Status ,0)Not in (2)
    /*Update Vendor Purchase*/
  UPDATE vendorpurchase 
  SET    cancelstatus =1, 
         updatedby =@Updatedby, 
         updatedat = Getdate() 
  WHERE  id =@PurchaseId
  /*Update dc  table where product coming from DC
  update DCVendorPurchaseItem set isActive = 1 from DCVendorPurchaseItem DC inner join VendorPurchaseItem VPI on vpi.fromDcId =  dc.id 
  where vpi.VendorPurchaseId = @PurchaseId */
    /*Update Temp vendor Purchase item  table where product coming from Temp stock
  update TempVendorPurchaseItem set isActive = 1 from TempVendorPurchaseItem TVP inner join VendorPurchaseItem VPI on vpi.fromTempId =  TVP.id 
  where vpi.VendorPurchaseId = @PurchaseId */

  SELECT 0 POStatus 
end