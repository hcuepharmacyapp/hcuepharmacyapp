/*******************************************************************************                            
** Change History                               
*******************************************************************************                            
** Date        Author          Description                               
*******************************************************************************         
** 26/08/2017 Poongodi R	   Created
** 07/09/2017 Poongodi R	   Package purchase price length increased
*******************************************************************************/ 
Create Proc dbo.Usp_Inventory_StockUpdate
(
@Id char(36),@AccountId char(36),@InstanceId char(36),
@BatchNo varchar(100),@ExpireDate date,@Igst decimal(9,2),@Cgst decimal(9,2),
@Sgst decimal(9,2),
@GstTotal decimal(9,2) ,
@SellingPrice decimal(18,6),
@MRP decimal(18,6),
@PackageSize decimal(9,2),
@PackagePurchasePrice decimal(18,6),
@PurchasePrice decimal(18,6),
@Hsncode  varchar(50),
@UpdatedAt datetime,
@UpdatedBy char(36) 
)
as 
begin
SET DEADLOCK_PRIORITY LOW
UPDATE ProductStock SET BatchNo = @BatchNo,ExpireDate = @ExpireDate,Igst = @Igst,Cgst = @Cgst,Sgst = @Sgst,GstTotal = @GstTotal
,SellingPrice = @SellingPrice,MRP = @MRP,PackageSize = @PackageSize,PackagePurchasePrice = @PackagePurchasePrice,PurchasePrice = @PurchasePrice,UpdatedAt = @UpdatedAt ,  HsnCode =@Hsncode
WHERE ProductStock.Id  =  @Id AND ProductStock.AccountId  =  @AccountId AND ProductStock.InstanceId  =  @InstanceId
select 'success'
end 

