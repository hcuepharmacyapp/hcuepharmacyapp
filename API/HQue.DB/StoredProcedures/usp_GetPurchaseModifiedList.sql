/** Change History                               
*******************************************************************************                            
** Date        Author          Description                               
*******************************************************************************         
** 22/06/2017   Violet			Prefix Added 
** 07/07/2017   Sarubala		GST Calculation Added 
** 13/09/2017   Lawrence		All branch condition Added 
******************************************************************************* 
--exec usp_GetPurchaseModifiedList 'c503ca6e-f418-4807-a847-b6886378cf0b','3e2ec066-1551-4527-be53-5aa3c5b7fb7d','0','','grnNo','','1','100','2017-03-10'
--'2017-07-01', '2017-07-01', '2017-09-14'
--exec usp_GetPurchaseModifiedList 'c503ca6e-f418-4807-a847-b6886378cf0b','3e2ec066-1551-4527-be53-5aa3c5b7fb7d','0','','grnNo','','44','44','2017-03-10'
--exec usp_GetPurchaseModifiedList 'c503ca6e-f418-4807-a847-b6886378cf0b','3e2ec066-1551-4527-be53-5aa3c5b7fb7d','0','','grnNo','','44','44','2017-03-10'
*/ 
CREATE PROCEDURE [dbo].[usp_GetPurchaseModifiedList](@AccountId varchar(36),@InstanceId varchar(36),@PageNo int,@PageSize int,@SearchColName varchar(50), @SearchOption varchar(50),
 @SearchValue varchar(50), @fromDate varchar(15),@Todate varchar(15))
 AS
 BEGIN
   SET NOCOUNT ON

  Declare @isDetail bit = 0

  Declare @BtchNo varchar(150) = '',  @invoiceNo varchar(150) = '', @grNo varchar(150) = '',@vendorName varchar(150) = '',@From_ExpiryDt date = null, @To_ExpiryDt date = null, 
  @billDate date = null, @product varchar(150) = '',@expiry date = null,@email varchar(150) = '',@mobile varchar(150) = '',@From_BillDt date = null, @To_BillDt date = null,
  @From_GRNDate date = null, @To_GRNDate date = null,@From_ModifiedDate date = null, @To_ModifiedDate date = null


	if (@SearchColName ='batchNo')
	select  @BtchNo = isnull(@SearchValue,'')

	else  if (@SearchColName ='invoiceNo')
	select  @invoiceNo =   isnull(@SearchValue,'') 
  
	else   if (@SearchColName ='grnNo')
	select  @grNo =  isnull(@SearchValue,'') 

	else   if (@SearchColName ='vendorName')
	select  @vendorName =   isnull(@SearchValue,'') 
	else   if (@SearchColName ='email')
	select  @email =   isnull(@SearchValue,'') 
	else   if (@SearchColName ='mobile')
	select  @mobile =   isnull(@SearchValue,'') 
	else if (@SearchColName ='product')
		
	select @product = isnull(@SearchValue,'')

	else if (@SearchColName ='expiry')

	select @From_ExpiryDt =  @fromDate ,@To_ExpiryDt = @Todate 

	else if (@SearchColName ='billDate')

	select @From_BillDt =  @fromDate ,@To_BillDt = @Todate

	else if (@SearchColName ='createddate')

	select @From_GRNDate =  @fromDate ,@To_GRNDate = @Todate

	else if (@SearchColName ='modifiedDate')

	select @From_ModifiedDate =  @fromDate ,@To_ModifiedDate = @Todate

IF (@SearchColName ='product')
BEGIN

SELECT COUNT(1) OVER() PurchaseItemAuditCount,VP.Id,VP.InvoiceNo,VP.InvoiceDate,VP.Discount,VP.GoodsRcvNo as [GRNNo],VP.PaymentType,VP.ChequeNo,VP.ChequeDate,VP.CreditNoOfDays,ISNULL(VP.Prefix,'')+ISNULL(VP.BillSeries,'') BillSeries,
CAST(isnull(vp.TaxRefNo,0) as int) TaxRefNo,
VP.Credit,VP.CreatedAt[GRNDate],VP.NoteAmount,VP.NoteType,Vendor.Id [VendorId] ,Vendor.Name [VendorName] ,Vendor.Email,
Vendor.Mobile ,Vendor.EnableCST,VPIA.ProductStockId,VPIA.PackageSize,
(isnull(VPIA.PackageQty,0)-isnull(VPIA.FreeQty,0)) AS PackageQty,
--((isnull(VPIA.PackagePurchasePrice,0)*(isnull(VPIA.PackageQty,0)-isnull(VPIA.FreeQty,0)))+((isnull(VPIA.PackagePurchasePrice,0)*(isnull(VPIA.PackageQty,0)-isnull(VPIA.FreeQty,0)))*((case when VP.TaxRefNo = 1 then isnull(PS.GstTotal,0) else isnull(PS.VAT,0) end)/100)))/(isnull(VPIA.PackageQty,0)-isnull(VPIA.FreeQty,0))

VPIA.PurchasePrice*VPIA.PackageSize
 AS PackagePurchasePrice,
VPIA.PackageSellingPrice,
VPIA.VendorPurchaseId,
VPIA.Quantity,
VPIA.PurchasePrice AS PurchasePrice,
VPIA.FreeQty,VPIA.Discount,VPIA.CreatedAt[ModifiedDate],VPIA.Action,
PS.SellingPrice ,PS.VAT,PS.CST,ps.GstTotal,ps.Cgst, ps.Sgst,ps.Igst,
PS.ExpireDate AS [ExpireDate],PS.BatchNo,Product.Name AS [ProductName],HQueUser.Name AS [UpdatedBy],
substring(convert(varchar(20), VPIA.CreatedAt, 9), 13, 5) + ' ' + substring(convert(varchar(30), VPIA.CreatedAt, 9), 25, 2) AS ModifiedTime
, Inst.Name AS InstanceName,ISNULL(VPIA.MarkupPerc,0) AS MarkupPerc,ISNULL(VPIA.SchemeDiscountPerc,0) AS SchemeDiscountPerc
FROM VendorPurchase VP
INNER JOIN VendorPurchaseItemAudit VPIA (NOLOCK) ON VPIA.VendorPurchaseId=VP.Id
INNER JOIN ProductStock PS (NOLOCK) ON PS.Id = VPIA.ProductStockId 
INNER JOIN Product (NOLOCK) ON Product.Id = PS.ProductId 
LEFT JOIN Vendor ON Vendor.Id = VP.VendorId  
INNER JOIN HQueUser (NOLOCK) ON HQueUser.Id = VPIA.UpdatedBy  
INNER JOIN Instance Inst on (Inst.AccountId = @AccountId and Inst.Id=ISNULL(@InstanceId,VP.InstanceId))
WHERE VP.Accountid=@AccountId and VP.InstanceId=ISNULL(@InstanceId,VP.InstanceId)
and isnull(Vendor.Name ,'')  like isnull(@vendorName,'') +'%'
and isnull(Vendor.Mobile ,'') like isnull(@mobile,'') +'%'
and isnull( Vendor.Email,'')  like isnull(@email,'') +'%'
and isnull(vp.BillSeries,'')+isnull(VP.GoodsRcvNo,'')  like '%'+isnull(@grNo,'') +'%'
and isnull(VP.InvoiceNo,'') like isnull(@invoiceNo,'')+'%'
and cast(VP.InvoiceDate as date )between isnull(@From_BillDt, InvoiceDate) and isnull(@To_BillDt, InvoiceDate)
and PS.ProductId =@product
and isnull(PS.BatchNo ,'') like isnull(@BtchNo,'') +'%'
and cast(PS.ExpireDate  as date )between isnull(@From_ExpiryDt, PS.ExpireDate) and isnull(@To_ExpiryDt, PS.ExpireDate)
and cast(VP.Createdat as date ) between isnull(@From_GRNDate, VP.Createdat) and isnull(@To_GRNDate, VP.Createdat)
and cast(VPIA.Createdat as date ) between isnull(@From_ModifiedDate, VPIA.Createdat) and isnull(@To_ModifiedDate, VPIA.Createdat) and VPIA.PackageQty>0
and isnull(VP.CancelStatus ,0) =0
and VPIA.Action != 'I'
ORDER BY VPIA.Createdat  desc 

END

ELSE
BEGIN

SELECT COUNT(1) OVER() PurchaseItemAuditCount,VP.Id,VP.InvoiceNo,VP.InvoiceDate,VP.Discount,VP.GoodsRcvNo as [GRNNo],VP.PaymentType,VP.ChequeNo,VP.ChequeDate,VP.CreditNoOfDays,ISNULL(VP.Prefix,'')+ISNULL(VP.BillSeries,'') BillSeries,
CAST(isnull(vp.TaxRefNo,0) as int) TaxRefNo,
VP.Credit,VP.CreatedAt[GRNDate],VP.NoteAmount,VP.NoteType,Vendor.Id [VendorId] ,Vendor.Name [VendorName] ,Vendor.Email,
Vendor.Mobile ,Vendor.EnableCST,VPIA.ProductStockId,VPIA.PackageSize,
(isnull(VPIA.PackageQty,0)-isnull(VPIA.FreeQty,0)) AS PackageQty,
--((isnull(VPIA.PackagePurchasePrice,0)*(isnull(VPIA.PackageQty,0)-isnull(VPIA.FreeQty,0)))+((isnull(VPIA.PackagePurchasePrice,0)*(isnull(VPIA.PackageQty,0)-isnull(VPIA.FreeQty,0)))*((case when VP.TaxRefNo = 1 then isnull(PS.GstTotal,0) else isnull(PS.VAT,0) end)/100)))/(isnull(VPIA.PackageQty,0)-isnull(VPIA.FreeQty,0))
VPIA.PurchasePrice*VPIA.PackageSize
 AS PackagePurchasePrice,
VPIA.PackageSellingPrice,VPIA.VendorPurchaseId,VPIA.Quantity,
 VPIA.PurchasePrice AS PurchasePrice,
VPIA.FreeQty,VPIA.Discount,VPIA.CreatedAt[ModifiedDate],VPIA.Action,
PS.SellingPrice ,PS.VAT,PS.CST,ps.GstTotal,ps.Cgst, ps.Sgst,ps.Igst,
PS.ExpireDate AS [ExpireDate],PS.BatchNo,Product.Name AS [ProductName],HQueUser.Name AS [UpdatedBy],
substring(convert(varchar(20), VPIA.CreatedAt, 9), 13, 5) + ' ' + substring(convert(varchar(30), VPIA.CreatedAt, 9), 25, 2) AS ModifiedTime 
, Inst.Name AS InstanceName,ISNULL(VPIA.MarkupPerc,0) AS MarkupPerc,ISNULL(VPIA.SchemeDiscountPerc,0) AS SchemeDiscountPerc
FROM 
VendorPurchase VP
INNER JOIN VendorPurchaseItemAudit VPIA (NOLOCK) ON VPIA.VendorPurchaseId=VP.Id
INNER JOIN ProductStock PS (NOLOCK) ON PS.Id = VPIA.ProductStockId 
INNER JOIN Product (NOLOCK) ON Product.Id = PS.ProductId 
LEFT JOIN Vendor ON Vendor.Id = VP.VendorId  
INNER JOIN HQueUser (NOLOCK) ON HQueUser.Id = VPIA.UpdatedBy  
INNER JOIN Instance Inst on (Inst.AccountId = @AccountId and Inst.Id=ISNULL(@InstanceId,VP.InstanceId))
WHERE VP.Accountid=@AccountId and VP.InstanceId=ISNULL(@InstanceId,VP.InstanceId)
and isnull(Vendor.Name ,'')  like isnull(@vendorName,'') +'%'
and isnull(Vendor.Mobile ,'') like isnull(@mobile,'') +'%'
and isnull( Vendor.Email,'')  like isnull(@email,'') +'%'
and isnull(vp.BillSeries,'')+isnull(VP.GoodsRcvNo,'')  like '%'+isnull(@grNo,'') +'%'
and isnull(VP.InvoiceNo,'') like isnull(@invoiceNo,'')+'%'
and cast(VP.InvoiceDate as date )between isnull(@From_BillDt, InvoiceDate) and isnull(@To_BillDt, InvoiceDate)
--and PS.ProductId like isnull(@product, '')+'%'
and isnull(PS.BatchNo ,'') like isnull(@BtchNo,'') +'%'
and cast(PS.ExpireDate  as date )between isnull(@From_ExpiryDt, PS.ExpireDate) and isnull(@To_ExpiryDt, PS.ExpireDate)
and cast(VP.Createdat as date ) between isnull(@From_GRNDate, VP.Createdat) and isnull(@To_GRNDate, VP.Createdat)
and cast(VPIA.Createdat as date ) between isnull(@From_ModifiedDate, VPIA.Createdat) and isnull(@To_ModifiedDate, VPIA.Createdat) and VPIA.PackageQty>0
and isnull(VP.CancelStatus ,0) =0
and VPIA.Action != 'I'
ORDER BY VPIA.Createdat  desc --OFFSET @PageNo ROWS FETCH NEXT @PageSize ROWS ONLY

END

END