  --Usage : -- usp_GetDistributorLowProductStockList '11e9fe4e-6dc8-46ec-bd7b-637670508a4b','kamalammedicalcorp@gmail.com'--,'4edd19c2-3fe3-4ba2-8083-83a06ccd6c88'
  CREATE PROCEDURE [dbo].[usp_GetDistributorLowProductStockList](@InstanceId varchar(36),@EmailId varchar(100))
 AS
 BEGIN
   SET NOCOUNT ON
     SELECT P.Name AS Product, PS.Stock AS Stock 
	 FROM Vendor V WITH(NOLOCK) 
    INNER JOIN Instance I WITH(NOLOCK) ON I.Id = V.InstanceId
    INNER JOIN VendorPurchase VP WITH(NOLOCK) ON V.Id = VP.VendorId
    INNER JOIN VendorPurchaseItem VPI WITH(NOLOCK) ON VPI.VendorPurchaseId = VP.Id
    INNER JOIN ProductStock PS WITH(NOLOCK) ON VPI.ProductStockId = PS.Id
    INNER JOIN Product P WITH(NOLOCK) ON P.Id = PS.ProductId
    WHERE V.Email=@EmailId AND V.InstanceId=@InstanceId
    GROUP BY P.Name, PS.Stock
    HAVING ABS(SUM(PS.Stock)) < 50
END
 

