
/*                              
******************************************************************************                            
**  
** Author:Poongodi R  
** Created Date: 30/06/2017
**  
*******************************************************************************                            
** Change History                              
*******************************************************************************                            
** Date        Author          Description                              
*******************************************************************************        
 
*******************************************************************************/ 
Create Proc dbo.USP_ClosingStock_B4GST
as
begin 
If  not exists (select * from sys.objects where type ='u' and name ='ProductStock_B4GST')
begin
CREATE TABLE [dbo].[ProductStock_B4GST]
(
	[Id] CHAR(36) NOT NULL,  
	ProductStockId char(36) NULL,
	[AccountId] CHAR(36) NOT NULL ,
	[InstanceId] CHAR(36) NOT NULL ,
	[ProductId] CHAR(36) NOT NULL, 
	[VendorId] CHAR(36) NULL, 
    [BatchNo] VARCHAR(100) NOT NULL, 
    [ExpireDate] DATE NOT NULL, 
    [VAT] DECIMAL(9, 2) NOT NULL, 
	[TaxType] VARCHAR(10) NULL,
    [SellingPrice] DECIMAL(18, 6) NOT NULL, 
	[MRP] DECIMAL(18, 6) DEFAULT 0, 
	[PurchaseBarcode]	VARCHAR(100) NULL,
	[Stock]	DECIMAL(18,2) NOT NULL,
	[PackageSize] DECIMAL(10,2),
	[PackagePurchasePrice] Decimal(18,6) DEFAULT 0,
    [PurchasePrice] DECIMAL(18, 6) NULL ,
	[OfflineStatus] BIT NULL DEFAULT 0,
	[CreatedAt] DATETIME ,    
    [UpdatedAt] DATETIME  ,  
    [CreatedBy] CHAR(36)  ,
    [UpdatedBy] CHAR(36) ,
    [CST] DECIMAL(18, 2) NULL DEFAULT 0, 
    [IsMovingStock] BIT NULL DEFAULT 0, 
    [ReOrderQty] DECIMAL(18, 2) NULL, 
    [Status] INT NULL,
	[IsMovingStockExpire] BIT NULL DEFAULT 0, 
	[NewOpenedStock] BIT NULL DEFAULT 0,      
    [NewStockInvoiceNo] VARCHAR(50) NULL, 
    [NewStockQty] DECIMAL(18, 2) NULL, 
    [StockImport] BIT NULL DEFAULT 0,
	ImportQty DECIMAL(18, 2) NULL, 
	[Eancode] VARCHAR(50) NULL, 
	[HsnCode] VARCHAR(50) NULL,
	[Igst] DECIMAL(9, 2) NULL, 
	[Cgst] DECIMAL(9, 2) NULL, 
	[Sgst] DECIMAL(9, 2) NULL, 
	[GstTotal] DECIMAL(9, 2) NULL, 
    CONSTRAINT [PK_ProductStock_B4GST] PRIMARY KEY ([Id]) ,
)
Insert into ProductStock_B4GST(
				Id,
				ProductStockId,
				AccountId,
				InstanceId,
				ProductId,
				VendorId,
				BatchNo,
				ExpireDate,
				VAT,
				TaxType,
				SellingPrice,
				MRP,
				PurchaseBarcode,
				Stock,
				PackageSize,
				PackagePurchasePrice,
				PurchasePrice,
				OfflineStatus,
				CreatedAt,
				UpdatedAt,
				CreatedBy,
				UpdatedBy,
				CST,
				IsMovingStock,
				ReOrderQty,
				Status,
				IsMovingStockExpire,
				NewOpenedStock,
				NewStockInvoiceNo,
				NewStockQty,
				StockImport,
				ImportQty,
				Eancode,
				HsnCode,
				Igst,
				Cgst,
				Sgst,
				GstTotal)
		select 
				NEWID(),
				Id,
				AccountId,
				InstanceId,
				ProductId,
				VendorId,
				BatchNo,
				ExpireDate,
				VAT,
				TaxType,
				SellingPrice,
				MRP,
				PurchaseBarcode,
				Stock,
				PackageSize,
				PackagePurchasePrice,
				PurchasePrice,
				OfflineStatus,
				CreatedAt,
				UpdatedAt,
				CreatedBy,
				UpdatedBy,
				CST,
				IsMovingStock,
				ReOrderQty,
				Status,
				IsMovingStockExpire,
				NewOpenedStock,
				NewStockInvoiceNo,
				NewStockQty,
				StockImport,
				ImportQty,
				Eancode,
				HsnCode,
				Igst,
				Cgst,
				Sgst,
				GstTotal 
		from ProductStock
	end
end 