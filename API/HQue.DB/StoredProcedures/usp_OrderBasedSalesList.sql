 
 CREATE PROCEDURE [dbo].[usp_OrderBasedSalesList](@InstanceId varchar(36),@AccountId varchar(36),@PreviousDays integer)
 AS
 BEGIN
 SET NOCOUNT ON  
  
  declare @type varchar(30),@daysToAdd integer,@days integer,@startdate date,@enddate date

set @days = @PreviousDays-1
if @days <= 7
	begin
		set @type='Days'
		set @daysToAdd = 1
	end
else if @days <=30
	begin
		set @type='Week'
		set @daysToAdd = 7
	end
else
	begin
		set @type='Month'
		set @daysToAdd = 30
	end

	set @startdate=DATEADD(d,-(@days),getdate())
	set @enddate=GETDATE()
	

select isnull(A.Name,'') ProductName,A.Id,sum(A.Day1) Day1,sum(A.Day2) Day2,sum(A.Day3) Day3,sum(A.Day4) Day4,sum(A.Day5) Day5,sum(A.Day6) Day6,sum(A.Day7) Day7
,sum(A.Day8) Day8,sum(A.Day9) Day9,sum(A.Day10) Day10,isnull(A.Stock,0) Stock,isnull(A.VendorName,'') VendorName,ISNULL(A.VendorId,'') VendorId from
(select p.Name,p.Id,
case when cast(sa.InvoiceDate as date) between @startdate and dateadd(d,((1*@daysToAdd)-1),@startdate) then si.Quantity else 0 end Day1
,case when cast(sa.InvoiceDate as date) between dateadd(d,(1*@daysToAdd),@startdate) and dateadd(d,((2*@daysToAdd)-1),@startdate) then si.Quantity else 0 end Day2
,case when cast(sa.InvoiceDate as date) between dateadd(d,(2*@daysToAdd),@startdate) and dateadd(d,((3*@daysToAdd)-1),@startdate) then si.Quantity else 0 end Day3
,case when cast(sa.InvoiceDate as date) between dateadd(d,(3*@daysToAdd),@startdate) and dateadd(d,((4*@daysToAdd)-1),@startdate) then si.Quantity else 0 end Day4
,case when cast(sa.InvoiceDate as date) between dateadd(d,(4*@daysToAdd),@startdate) and dateadd(d,((5*@daysToAdd)-1),@startdate) then si.Quantity else 0 end Day5
,case when cast(sa.InvoiceDate as date) between dateadd(d,(5*@daysToAdd),@startdate) and dateadd(d,((6*@daysToAdd)-1),@startdate) then si.Quantity else 0 end Day6
,case when cast(sa.InvoiceDate as date) between dateadd(d,(6*@daysToAdd),@startdate) and dateadd(d,((7*@daysToAdd)-1),@startdate) then si.Quantity else 0 end 
Day7
,case when cast(sa.InvoiceDate as date) between dateadd(d,(7*@daysToAdd),@startdate) and dateadd(d,((8*@daysToAdd)-1),@startdate) then si.Quantity else 0 end 
Day8
,case when cast(sa.InvoiceDate as date) between dateadd(d,(8*@daysToAdd),@startdate) and dateadd(d,((9*@daysToAdd)-1),@startdate) then si.Quantity else 0 end Day9
,case when cast(sa.InvoiceDate as date) between dateadd(d,(9*@daysToAdd),@startdate) and dateadd(d,((10*@daysToAdd)-1),@startdate) then si.Quantity else 0 end Day10
,ps.Stock,v.Name as VendorName,v.Id as VendorId
from Sales sa 
join SalesItem si on si.SalesId=sa.Id
INNER join ProductStock ps on ps.Id=si.ProductStockId
INNER join Product p on p.Id=ps.ProductId
INNER join Vendor v on v.Id=ps.VendorId
where sa.AccountId=@AccountId and sa.InstanceId=@InstanceId
and cast(sa.InvoiceDate as date) between @startdate and @enddate 
group by p.Name,p.Id,sa.InvoiceDate,si.Quantity,ps.Stock,v.Name,v.Id)A
group by A.Name,A.Id,A.Stock,A.VendorName,A.VendorId
order by A.Name

END
 