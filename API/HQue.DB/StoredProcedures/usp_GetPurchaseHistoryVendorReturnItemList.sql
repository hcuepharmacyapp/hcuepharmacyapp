﻿ 

/*                               
******************************************************************************                            
** File: [usp_GetPurchaseHistoryReturnItemList]   
** Name: [usp_GetPurchaseHistoryReturnItemList]                               
** Description: To Get Purchase Audit details  
**   
** This template can be customized:                               
**                                
** Called by:                                
**                                
**  Parameters:                               
**  Input                Output                               
**  ----------              -----------                               
**   
** Author:Gavaskar S   
** Created Date: 04/09/2017   
**   
*******************************************************************************                            
** Change History                               
*******************************************************************************                            
** Date        Author          Description                               
*******************************************************************************         
**	07/12/2017  Settu			GstTotal refferred from returnItem table
*******************************************************************************/

 -- exec usp_GetPurchaseHistoryVendorReturnItemList 'd5692d1f-13bf-43f1-8f67-a6ce4e6b3ca1'
 CREATE PROCEDURE [dbo].[usp_GetPurchaseHistoryVendorReturnItemList](@VendorReturnId varchar(max), @Product varchar(50))
 AS
 BEGIN
   SET NOCOUNT ON

SELECT VendorReturnItem.ProductStockId,VendorReturnItem.Quantity,VendorReturnItem.VendorReturnId [VendorReturnId],VendorReturnItem.ReturnPurchasePrice,ProductStock.SellingPrice, ProductStock.MRP,
ProductStock.VAT ,ProductStock.CST ,ProductStock.BatchNo ,ProductStock.ExpireDate AS [ExpireDate],
-- GST related fields 
VendorReturnItem.Igst,VendorReturnItem.Cgst,VendorReturnItem.Sgst,VendorReturnItem.GstTotal,
VendorPurchaseItem.PackageQty ,VendorPurchaseItem.FreeQty ,VendorPurchaseItem.Discount ,
Product.Name AS [ProductName],HQueUser.Name AS [CreatedBy] ,VendorReturnItem.ReturnedTotal,VendorReturnItem.Discount as [VendorReturnItemDiscount],
ProductStock.PackageSize,ProductStock.PackagePurchasePrice,ProductStock.PurchasePrice,VendorReturnItem.IsDeleted,VendorReturnItem.GstAmount
FROM VendorReturnItem 
INNER JOIN VendorReturn ON VendorReturn.Id = VendorReturnItem.VendorReturnId and VendorReturn.IsAlongWithPurchase=1
INNER JOIN VendorPurchase ON VendorPurchase.Id = VendorReturn.VendorPurchaseId 
INNER JOIN ProductStock ON ProductStock.Id = VendorReturnItem.ProductStockId 
left JOIN VendorPurchaseItem ON VendorPurchaseItem.ProductStockId = VendorReturnItem.ProductStockId
and  VendorPurchaseItem.VendorPurchaseId = VendorPurchase.Id
INNER JOIN HQueUser ON HQueUser.Id = VendorReturnItem.CreatedBy
INNER JOIN Product ON Product.Id = ProductStock.ProductId where ProductStock.ProductId  like isnull(@product, '')+'%' and
VendorReturnItem.VendorReturnId in (select a.id from dbo.udf_Split(@VendorReturnId ) a) ORDER BY 1 
      
 END
           
 


