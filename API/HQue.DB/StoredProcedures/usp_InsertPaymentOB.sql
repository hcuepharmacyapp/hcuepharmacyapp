﻿Create PROCEDURE [dbo].[usp_InsertPaymentOB]
	(@InstanceId varchar(36),
	@AccountId varchar(36),
	@VendorId char(36),
	@credit NUMERIC(18,2),
	@offlineStatus bit,
	@createdBy CHAR(36),
	@id CHAR(36),
	@updatedDate Datetime)
AS
 BEGIN
   SET NOCOUNT ON 
			Declare @status varchar(10);
			Set @status = '0'
		
			IF (@credit > 0)
			Begin
			IF Not Exists(Select 1 from Payment where VendorId = @VendorId AND Credit > 0 AND VendorPurchaseId is null And 
				AccountId = @AccountId and InstanceId = @InstanceId)
			Begin

			INSERT INTO Payment
			(Id,AccountID,InstanceID,TransactionDate, VendorId, Debit,Credit, paymenttype, offlinestatus,createdat,updatedat,createdby,updatedby) Values
			(@id, @AccountId, @InstanceId, @updatedDate, @VendorId, 0, @credit,'Update',  @offlineStatus,@updatedDate,@updatedDate,
			@createdBy,@createdBy)

			Set @status = '1'
			end
			end
			Select @status as status



			
END

