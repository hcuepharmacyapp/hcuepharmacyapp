/*                               
******************************************************************************                            
** File: [usp_GetSalesReturnLoyaltyPointSettings]   
** Name: [usp_GetSalesReturnLoyaltyPointSettings]                               
**   
**                                
** Called by:                                
**                                
**  Parameters:                               
**  Input                Output                               
**  ----------              -----------                               
**   
** Author:Sarubala V   
** Created Date: 20/06/2018  
**   
*******************************************************************************                            
** Change History                               
*******************************************************************************                            
** Date        Author          Description                               
*******************************************************************************         
*******************************************************************************/ 
 
 -- exec usp_GetSalesLoyaltyPointSettings '956d7fe7-d39b-4a9c-91b6-552fb7d1481e','87c13de7-eda0-4567-8439-8bb078f71059','e46a39f5-0325-47fe-9d71-1bdc6606df88'
 CREATE  PROCEDURE [dbo].[usp_GetSalesReturnLoyaltyPointSettings](@AccountId varchar(36),@LoyaltyId varchar(36),@SalesReturnId varchar(36))
 AS
 BEGIN

SELECT LoyaltyPointSettings.Id,SalesReturn.Id as SalesId,LoyaltyPointSettings.LoyaltySaleValue,LoyaltyPointSettings.LoyaltyPoint,
LoyaltyPointSettings.StartDate,LoyaltyPointSettings.EndDate,LoyaltyPointSettings.MinimumSalesAmt,LoyaltyPointSettings.RedeemPoint,
LoyaltyPointSettings.RedeemValue,LoyaltyPointSettings.LoyaltyOption,LoyaltyPointSettings.IsMinimumSale,LoyaltyPointSettings.IsEndDate,
LoyaltyPointSettings.MaximumRedeemPoint, LoyaltyPointSettings.IsActive, LoyaltyPointSettings.IsMaximumRedeem 
FROM LoyaltyPointSettings  (nolock)  
INNER JOIN SalesReturn (nolock) ON SalesReturn.LoyaltyId = LoyaltyPointSettings.Id  
WHERE LoyaltyPointSettings.AccountId  =  @AccountId AND SalesReturn.LoyaltyId  =  @LoyaltyId
 AND SalesReturn.Id  =  @SalesReturnId
           
END
