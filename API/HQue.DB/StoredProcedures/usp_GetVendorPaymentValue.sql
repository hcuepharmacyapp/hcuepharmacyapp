﻿/*******************************************************************************                            
** Change History                              
*******************************************************************************                            
** Date        Author          Description                              
*******************************************************************************        
**04/10/17     Poongodi R	Updatedat removed 
*******************************************************************************/
Create PROCEDURE [dbo].[usp_GetVendorPaymentValue](
@AccountId VARCHAR(36), 
@InstanceId VARCHAR(36), 
@fromdate datetime, 
@todate datetime, 
@UserId VARCHAR(36),
@offlineStatus bit)
 AS
 BEGIN
   SET NOCOUNT ON
   
	select  sum(debit) ReturnValue from Payment
	WHERE PaymentMode = 'Cash' And 
	 convert(datetime, createdat) between (@fromdate) and (@todate)
		And AccountId=@AccountId and InstanceId=@InstanceId and CreatedBy=@UserId
		and offlineStatus = @offlineStatus


END


 