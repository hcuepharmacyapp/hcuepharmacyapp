/*******************************************************************************                            
** Change History                              
*******************************************************************************                            
** Date        Author          Description                              
*******************************************************************************        
** 30/08/2017  Poongodi		Created
** 04/092017  Sarubala		Added order by filter
*******************************************************************************/ 
Create PROC dbo.Usp_gstr1_sales_medi(@Accountid  CHAR(36), 
                               @InstanceId VARCHAR(36), 
                               @Startdate  DATE, 
                               @EndDate    DATE, 
                               @GstinNo    VARCHAR(50), 
                               @FilterType VARCHAR(10),
							   @IsSummary bit=0)   
AS 
  BEGIN 
  print 1
      create TABLE #Instance   
        ( 
           id CHAR(36) primary key
        ) 

      IF ( @FilterType = 'branch' ) 
        BEGIN 
            INSERT INTO #Instance 
            SELECT @Instanceid 
			set @GstinNo = nULL
        END 
      ELSE 
        BEGIN 
            INSERT INTO #Instance 
            SELECT id 
            FROM   instance (nolock)
            WHERE  gstinno = @GstinNo 
        END 
		Select SalesId, max(BranchName) [BranchName], max([BranchGSTin]) [BranchGSTin], max([InvoiceNumber]) [InvoiceNumber],
		 ([InvoiceDate] ) [InvoiceDate] ,max([RecipientName]) [RecipientName],max([RecipientGSTin]) [RecipientGSTin],
		max([PartyType]) [PartyType],[Code],[HSN],[ProductName],
		sum(isnull([Quantity],0) ) [Quantity], sum([InvoiceValue]) [InvoiceValue], max([PlaceOfSupply]) [PlaceOfSupply],
		     'Goods'                              [IsGoods], 
             'No'                                 [IsReverseCharge], max([SupplyType]) [SupplyType],[GSTTotal], sum([DiscountValue]) [DiscountValue],
			 sum([ValueWithoutTax]) [ValueWithoutTax],sum([CGSTAmount]) [CGSTAmount], sum([SGSTAmount]) [SGSTAmount],
			 0.00                                 [IGSTAmount], 
             0.00                                 [CESS], 
			 sum([TaxAmount]) [TaxAmount],[BatchNo],[ExipryDate] from (
      SELECT i.NAME + ' - ' + i.area              AS BranchName, 
             i.gstinno                            [BranchGSTin], 
             Ltrim(Isnull(s.prefix, '') 
                   + Isnull(s.invoiceseries, '')) AS InvoiceSeries, 
             S.invoiceno, 
             Ltrim(Isnull(s.prefix, '')+Isnull(s.invoiceseries, '')) 
             + Isnull(S.invoiceno, '')            [InvoiceNumber], 
             S.invoicedate                        [InvoiceDate], 
             Isnull(S.NAME, '')                   AS [RecipientName], 
			 ltrim(isnull(pat.GsTin,'') ) [RecipientGSTin],
             case ltrim(isnull(pat.GsTin,'')) when '' then 'UnRegistered' else 'Registered' end [PartyType], 
             case @IsSummary when 1 then '' else Isnull(p.code, '')   end                [Code], 
             case @IsSummary when 1 then '' else Isnull(P.hsncode, '') end               [HSN], 
             case @IsSummary when 1 then '' else Isnull(P.NAME, 'Not Available') end   AS [ProductName], 
             SI.quantity                          [Quantity], 
             SIP.finalvalue  - (Cast(( SIP.finalvalue * Isnull(SIp.discount, 0) / 100 ) AS 
                  DECIMAL (18, 2)) )                     [InvoiceValue], 
             left(ltrim(isnull(pat.GsTin,'')),2)  [PlaceOfSupply], 
        
             CONVERT(DECIMAL(18, 2), Isnull(SI.sellingprice, PS.sellingprice) 
                                     * SI.quantity) 
                                                  AS SellingPrice, 
             CASE Isnull(s.taxrefno, 0) 
               WHEN 1 THEN 
                 CASE Isnull(gst.gsttotal, 0) 
                   WHEN 0 THEN 'Zero Rated Supplies' 
                   ELSE 'Regular GST Supply' 
                 END 
             END                                  [SupplyType], 
             CASE Isnull(s.taxrefno, 0) 
               WHEN 1 THEN case isnull(ps.gsttotal,0) when 0 then isnull(si.gsttotal,0) else  isnull(ps.gsttotal,0) end 
               ELSE PS.vat 
             END                                  [GSTTotal], 
             Isnull(SI.discount, 0)               Discount, 
             Cast(( SIP.finalvalue * Isnull(SIp.discount, 0) / 100 ) AS 
                  DECIMAL (18, 2)) 
                                                  [DiscountValue], 
 
				 round(SIP.finalvalue ,2) - round(tax.taxvalue,2) - (Cast(( round(SIP.finalvalue ,2)* Isnull(SIp.discount, 0) / 100 ) AS 
                  DECIMAL (18, 2)) )           [ValueWithoutTax], 
             Round(tax.taxvalue / 2, 3)           [CGSTAmount], 
             Round(tax.taxvalue / 2, 3)           [SGSTAmount], 
        
             Round(tax.taxvalue, 2)               [TaxAmount], 
             case @IsSummary when 1 then '' else  PS.batchno  end                         [BatchNo], 
              case @IsSummary when 1 then NULL else PS.expiredate  end                      [ExipryDate] ,
			 s.id [SalesId]
      FROM   sales S WITH(nolock) 
			inner join #Instance I1 on I1.id = S.instanceid
             INNER JOIN (SELECT SI.* 
                         FROM   salesitem (nolock) SI inner join #Instance I on I.id = si.instanceid
                          ) SI 
                     ON S.id = SI.salesid 
             INNER JOIN (SELECT SI.*  
                         FROM   productstock (nolock) SI inner join #Instance I on I.id = si.instanceid
                        ) PS 
                     ON PS.id = SI.productstockid 
			LEFT JOIN (SELECT * FROM PATIENT WHERE  accountid = @Accountid) pat 
			on pat.id = S.PatientId
             LEFT JOIN (SELECT * 
                        FROM   product (nolock) 
                        WHERE  accountid = @Accountid) p 
                    ON p.id = ps.productid 
             INNER JOIN (SELECT SI.*  
                         FROM   instance (nolock) SI inner join #Instance I on I.id = si.id
                         ) i 
                     ON i.id = ps.instanceid 
					 cross apply (select  Isnull(ps.gsttotal,  Isnull(SI.gsttotal, 0))    gsttotal) gst
             CROSS apply(SELECT CONVERT(DECIMAL(18, 2), 
                                Isnull(SI.sellingprice, PS.sellingprice) 
                                           * SI.quantity) 
                                                            AS FinalValue, 
                                (  case when ISNULL(s.Discount,0) > 0 then s.Discount else ISNULL(si.Discount,0) end ) discount) SIP 
             CROSS apply (SELECT 
                            Cast(( SIP.finalvalue - ( SIP.finalvalue * 
                                                      Isnull(SIp.discount, 0) / 
                                                      100 ) 
                                 ) 
                            - 
                                 ( SIP.finalvalue 
                            - ( SIP.finalvalue * Isnull(SIp.discount, 0) / 100 ) 
                                 ) 
                                                          * ( 
                                                          100 
                            / 
                            ( 100 
                            + 
                            Isnull(gst.gsttotal,0) ) ) AS 
                                 DECIMAL (18, 3)) 
                            [TaxValue]) AS Tax 
      WHERE   
               S.accountid = @Accountid 
             AND S.cancelstatus IS NULL 
             AND CONVERT(DATE, S.invoicedate) BETWEEN @Startdate AND @EndDate 
             AND i.gstinno = Isnull(@GstinNo, i.gstinno)  ) one 
			 group by BranchName,InvoiceDate,InvoiceNumber,SalesId,ProductName,GSTTotal,[BatchNo],[ExipryDate],[Code],[HSN]

			 --order by  max(BranchName), max([InvoiceDate] ) desc,max([InvoiceNumber]) desc  
  END 