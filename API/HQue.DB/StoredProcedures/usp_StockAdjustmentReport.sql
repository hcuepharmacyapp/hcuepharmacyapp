 
 --Usage : -- usp_StockAdjustmentReport '013513b1-ea8c-4ea8-9fed-054b260ee197','18204879-99ff-4efd-b076-f85b4a0da0a3','01 Nov 2016','30 Nov 2016','013513b1-ea8c-4ea8-9fed-054b260ery97','mat35',null,null,null
  
  /*******************************************************************************                            
** Change History                               
*******************************************************************************                            
** Date        Author          Description                               
*******************************************************************************         
 ** 19/06/2007	Poongodi R		Date filter added 
*******************************************************************************/
 Create PROCEDURE [dbo].[usp_StockAdjustmentReport](@InstanceId varchar(36),@AccountId varchar(36),@StartDate datetime,@EndDate datetime,
 @ProdId varchar(36),@Batch varchar(100),@SearchOn varchar(30),@Value decimal,@Operator varchar(5))
 AS
 BEGIN
 SET NOCOUNT ON  
  
  select @ProdId  = isnull(@ProdId ,''),@StartDate = ISNULL(@StartDate,''),@EndDate = ISNULL(@EndDate,''), @Batch = ISNULL(@Batch,'') 
 ,@Operator=ISNULL(@Operator,'')

  if(@Operator is null or @Operator = '')
  begin
 
	select sa.Id,isnull(p.Name,'') Name,ps.BatchNo,ps.ExpireDate,
	case when ps.CST>0 then ps.cst 
	else case isnull(sa.TaxRefNo,0)  when 1 
	then ps.GstTotal else ps.VAT end
	end VAT,
	isnull(ps.PurchasePrice,0) PurchasePrice,isnull(ps.Stock,0) Stock,isnull(sa.AdjustedStock,0) AdjustedStock,isnull(hu.Name,'') AdjustedName,sa.CreatedAt,
	case when sa.AdjustedStock >0 then ISNULL(ps.Stock,0) - sa.AdjustedStock else ISNULL(ps.Stock,0) + ABS(sa.AdjustedStock) end 'StockBeforeAdjust',
	(ABS(sa.AdjustedStock)*ISNULL(ps.PurchasePrice,0)) Price,
	(ABS(sa.AdjustedStock)*ISNULL(ps.PurchasePrice,0)*case when ps.CST>0 then ps.cst 
	else case isnull(sa.TaxRefNo,0)  when 1 
	then ps.GstTotal else ps.VAT end 
	end /100) VatPrice,
	--cast((ABS(sa.AdjustedStock)*ISNULL(ps.PurchasePrice,0))+(ABS(sa.AdjustedStock)*ISNULL(ps.PurchasePrice,0)*case when ps.CST>0 then ps.cst else ps.VAT end /100) as decimal)			Total
	cast((ABS(sa.AdjustedStock)*ISNULL(ps.PurchasePrice,0)) as decimal)			Total,ISNULL(ps.GstTotal,ps.VAT) AS GSTVAT 
	from ProductStock ps join 
	StockAdjustment sa on ps.Id=sa.ProductStockId
	join Product P on p.Id=ps.ProductId
	join HQueUser hu on hu.Id=sa.CreatedBy
	where sa.AccountId=@AccountId and sa.InstanceId=@InstanceId
	and ((((convert(varchar,sa.CreatedAt,106)) >= @StartDate) or @StartDate='') and (((convert(varchar,sa.CreatedAt,106)) <= @EndDate) or @EndDate=''))
	and (ps.ProductId=@ProdId or @ProdId='')
	and (ps.BatchNo=@Batch or @Batch='')
	order by sa.AdjustedStock,sa.CreatedAt desc

  end

  else
  begin
	declare @fromValue decimal = null, @toValue decimal = null,@fromstock decimal= null, @tostock decimal= null, @fromprice decimal= null,
	 @toprice decimal = null,@fromadstock decimal = null, @toadstock decimal = null
	 

		if(@Operator = '=')
		 begin
			select @fromValue = isnull(@Value,0.00),@toValue = isnull(@Value,0.00)
		 end
		else if(@Operator = '<')
		 begin
			select @fromValue = NULL ,@toValue = isnull(@Value,0.00)-1
		 end
		else if(@Operator = '>')
		 begin
			select @fromValue = isnull(@Value,0.00)+1 ,@toValue = null
		 end

		 if (lower (@SearchOn)  ='stock')
			begin
				select @fromstock = @fromValue , @tostock =@toValue
			end
		else if (lower (@SearchOn)  ='price')
			begin
				select @fromprice = @fromValue , @toprice =@toValue
			end
		else if (lower (@SearchOn)  ='adjuststock')
			begin
				select @fromadstock = @fromValue , @toadstock =@toValue
			end		  
 
	--select @fromadstock,@toadstock,@fromValue,@toValue,@fromstock,@tostock,@fromprice,@toprice,@SearchOn
	select sa.Id,isnull(p.Name,'') Name,ps.BatchNo,ps.ExpireDate,
	case when ps.CST>0 then ps.cst 
	else case isnull(sa.TaxRefNo,0) when 1 
	then ps.GstTotal else ps.VAT end
	end VAT,
	isnull(ps.PurchasePrice,0) PurchasePrice,isnull(ps.Stock,0) Stock,isnull(sa.AdjustedStock,0) AdjustedStock,isnull(hu.Name,'') AdjustedName,sa.CreatedAt,
	case when sa.AdjustedStock >0 then ISNULL(ps.Stock,0) - sa.AdjustedStock else ISNULL(ps.Stock,0) + ABS(sa.AdjustedStock) end 'StockBeforeAdjust',
	(ABS(sa.AdjustedStock)*ISNULL(ps.PurchasePrice,0)) Price,
	(ABS(sa.AdjustedStock)*ISNULL(ps.PurchasePrice,0)*case when ps.CST>0 then ps.cst 
	else case isnull(sa.TaxRefNo,0) when 1 
	then ps.GstTotal else ps.VAT end
	end /100) VatPrice,
	--(ABS(sa.AdjustedStock)*ISNULL(ps.PurchasePrice,0))+(ABS(sa.AdjustedStock)*ISNULL(ps.PurchasePrice,0)*isnull(ps.CST,ps.VAT) /100) 
	A.Total ,ISNULL(ps.GstTotal,ps.VAT) AS GSTVAT 
	from ProductStock ps join 
	StockAdjustment sa on ps.Id=sa.ProductStockId
	join Product P on p.Id=ps.ProductId
	join HQueUser hu on hu.Id=sa.CreatedBy
	--cross apply(select cast((ABS(sa.AdjustedStock)*ISNULL(ps.PurchasePrice,0))-(ABS(sa.AdjustedStock)*ISNULL(ps.PurchasePrice,0)*isnull(ps.CST,ps.VAT) /100)  as decimal) ) as A(Total)
	cross apply(select cast((ABS(sa.AdjustedStock)*ISNULL(ps.PurchasePrice,0))as decimal) ) as A(Total)
	where sa.AccountId=@AccountId and sa.InstanceId=@InstanceId 	
	and sa.AdjustedStock  between isnull(  @fromadstock,sa.AdjustedStock) and isnull(@toadstock,sa.AdjustedStock)
	and ps.Stock between isnull(@fromstock,ps.Stock ) and isnull(@tostock,ps.Stock)
	and A.Total between ISNULL(@fromprice,A.Total) and ISNULL(@toprice,A.Total)
		and ((((convert(varchar,sa.CreatedAt,106)) >= @StartDate) or @StartDate='') and (((convert(varchar,sa.CreatedAt,106)) <= @EndDate) or @EndDate=''))
	order by sa.AdjustedStock,sa.CreatedAt desc

	 
  end

END
 