﻿ -- SP Create GSTR4 TaxRateWise TurnOver
/** Date        Author          Description                              
*******************************************************************************        
 ** 25/11/2017	Lawrence		Created for GSTR4 Reports
*******************************************************************************/
-- usp_gstr4TaxRateWiseTurnOver 'c503ca6e-f418-4807-a847-b6886378cf0b', '3e2ec066-1551-4527-be53-5aa3c5b7fb7d', 'GSTIN3453453454', '01-Nov-2017', '25-Nov-2017'
CREATE PROC usp_gstr4TaxRateWiseTurnOver(@AccountId char(36), @InstanceId char(36), @GstinNo VARCHAR(50), @fromDate date, @toDate date)
AS
Begin

if @GstinNo = ''
	set @GstinNo = NULL
if @InstanceId = ''
	set @InstanceId = NULL

DECLARE @temp1FromDate AS DATE = '01-Apr-2017'
DECLARE @temp1ToDate AS DATE = '30-Jun-2017'
DECLARE @temp1FinYearAmount DECIMAL(18, 6)

DECLARE @temp2FromDate AS DATE = '01-Apr-2016'
DECLARE @temp2ToDate AS DATE = '31-Mar-2017'
DECLARE @temp2FinYearAmount DECIMAL(18, 6)

--! Apr - Jun data
--- --------------
SELECT @temp1FinYearAmount=(SUM(InvoiceAmount)-SUM(ReturnInvoiceAmount))
FROM (
	SELECT 
	ISNULL(SI.VAT,0) GstTotal,		
	--SUM(CONVERT(decimal(18,2), (SI.Quantity) * (ISNULL(SI.SellingPrice, ISNULL(PS.SellingPrice,0)) * (1 - (case when ISNULL(s.Discount,0) > 0 then s.Discount else ISNULL(si.Discount,0) end / 100))))) As InvoiceAmount, 
	SUM(S.SaleAmount) As InvoiceAmount,
	0 as ReturnInvoiceAmount,
	--SUM((SI.Quantity) * (isNull(SI.SellingPrice, ISNULL(PS.SellingPrice,0)) * (1- (case when ISNULL(s.Discount,0) > 0 then s.Discount else ISNULL(si.Discount,0) end / 100))) / (100 + (IsNull(PS.VAT,ISNULL(SI.VAT,0)))) * IsNull(PS.VAT,ISNULL(SI.VAT,0)) ) as TaxAmount, 
	SUM(S.VatAmount) as TaxAmount, 
	0 as ReturnTaxAmount	
	FROM Sales S WITH(NOLOCK)
	INNER JOIN Instance I WITH(NOLOCK) On I.Id = S.InstanceId
	INNER JOIN SalesItem SI  WITH(NOLOCK) on S.id= SI.salesid
	INNER JOIN ProductStock PS WITH(NOLOCK) on PS.Id=SI.productstockid
	WHERE S.AccountId = @AccountId AND S.InstanceId = ISNULL(@InstanceId,S.InstanceId) 
	AND Convert(date,S.invoicedate) BETWEEN @temp1FromDate AND @temp1ToDate
	AND I.GsTinNo = ISNULL(@GstinNo,I.GsTinNo) and S.Cancelstatus is NULL	
	GROUP BY ISNULL(SI.VAT,0)

	UNION ALL

	SELECT 
	isNull(ps.VAT,0) AS GstTotal,
	0 as InvoiceAmount, 
	--SUM(CONVERT(decimal(18,2), (SRI.Quantity) * (ISNULL(SRI.MrpSellingPrice, ISNULL(PS.SellingPrice,0)) * (1 - ((ISNULL(s.Discount,0)+ISNULL(SRI.Discount,0)) / 100))))) As ReturnInvoiceAmount,
	SUM(SR.NetAmount) As ReturnInvoiceAmount,
	0 as TaxAmount, 
	--SUM((SRI.Quantity) * (isNull(SRI.MrpSellingPrice, ISNULL(PS.SellingPrice,0)) * (1- (ISNULL(s.Discount,0)+ISNULL(SRI.Discount,0)) / 100)) / (100 + (isNull(ps.VAT,0))) * isNull(ps.VAT,0) ) as ReturnTaxAmount	
	SUM(SRI.TotalAmount / (100 + (isNull(ps.VAT,0))) * isNull(ps.VAT,0) ) as ReturnTaxAmount	
	FROM SalesReturn SR WITH(NOLOCK)
	INNER JOIN Instance I WITH(NOLOCK) On I.Id = SR.InstanceId
	INNER JOIN SalesReturnItem SRI WITH(NOLOCK) ON SRI.SalesReturnId = SR.Id
	INNER JOIN ProductStock PS WITH(NOLOCK) on PS.Id=SRI.ProductStockId
	LEFT JOIN Sales S WITH(NOLOCK) On SR.SalesId = S.Id
	WHERE SR.AccountId = @AccountId AND SR.InstanceId = ISNULL(@InstanceId,SR.InstanceId) 
	AND Convert(date,SR.returndate) BETWEEN @temp1FromDate AND @temp1ToDate
	AND I.GsTinNo = ISNULL(@GstinNo,I.GsTinNo)
	AND S.Cancelstatus is NULL
	and ISNULL(sri.IsDeleted,0) != 1
	and ISNULL(sr.CancelType,0) != 2	
	GROUP BY isNull(ps.VAT,0)
) A

--! Fin year data
--- -------------
SELECT @temp2FinYearAmount=(SUM(InvoiceAmount)-SUM(ReturnInvoiceAmount))
FROM (
	SELECT 
	ISNULL(SI.VAT,0) GstTotal,		
	--SUM(CONVERT(decimal(18,2), (SI.Quantity) * (ISNULL(SI.SellingPrice, ISNULL(PS.SellingPrice,0)) * (1 - (case when ISNULL(s.Discount,0) > 0 then s.Discount else ISNULL(si.Discount,0) end / 100))))) As InvoiceAmount, 
	SUM(S.SaleAmount) As InvoiceAmount, 
	0 as ReturnInvoiceAmount,
	--SUM((SI.Quantity) * (isNull(SI.SellingPrice, ISNULL(PS.SellingPrice,0)) * (1- (case when ISNULL(s.Discount,0) > 0 then s.Discount else ISNULL(si.Discount,0) end / 100))) / (100 + (IsNull(PS.VAT,ISNULL(SI.VAT,0)))) * IsNull(PS.VAT,ISNULL(SI.VAT,0)) ) as TaxAmount, 
	SUM(S.VatAmount) as TaxAmount, 
	0 as ReturnTaxAmount	
	FROM Sales S WITH(NOLOCK)
	INNER JOIN Instance I WITH(NOLOCK) On I.Id = S.InstanceId
	INNER JOIN SalesItem SI  WITH(NOLOCK) on S.id= SI.salesid
	INNER JOIN ProductStock PS WITH(NOLOCK) on PS.Id=SI.productstockid
	WHERE S.AccountId = @AccountId AND S.InstanceId = ISNULL(@InstanceId,S.InstanceId) 
	AND Convert(date,S.invoicedate) BETWEEN @temp2FromDate AND @temp2ToDate
	AND I.GsTinNo = ISNULL(@GstinNo,I.GsTinNo) and S.Cancelstatus is NULL	
	GROUP BY ISNULL(SI.VAT,0)

	UNION ALL

	SELECT 
	isNull(ps.VAT,0) AS GstTotal, 
	0 as InvoiceAmount, 
	--SUM(CONVERT(decimal(18,2), (SRI.Quantity) * (ISNULL(SRI.MrpSellingPrice, ISNULL(PS.SellingPrice,0)) * (1 - ((ISNULL(s.Discount,0)+ISNULL(SRI.Discount,0)) / 100))))) As ReturnInvoiceAmount, 
	SUM(SR.NetAmount) As ReturnInvoiceAmount,
	0 as TaxAmount, 
	--SUM((SRI.Quantity) * (isNull(SRI.MrpSellingPrice, ISNULL(PS.SellingPrice,0)) * (1- (ISNULL(s.Discount,0)+ISNULL(SRI.Discount,0)) / 100)) / (100 + (isNull(ps.VAT,0))) * isNull(ps.VAT,0) ) as ReturnTaxAmount
	SUM(SRI.TotalAmount / (100 + (isNull(ps.VAT,0))) * isNull(ps.VAT,0) ) as ReturnTaxAmount	
	FROM SalesReturn SR WITH(NOLOCK)
	INNER JOIN Instance I WITH(NOLOCK) On I.Id = SR.InstanceId
	INNER JOIN SalesReturnItem SRI WITH(NOLOCK) ON SRI.SalesReturnId = SR.Id
	INNER JOIN ProductStock PS  WITH(NOLOCK) on PS.Id=SRI.ProductStockId
	LEFT JOIN Sales S WITH(NOLOCK) On SR.SalesId = S.Id
	WHERE SR.AccountId = @AccountId AND SR.InstanceId = ISNULL(@InstanceId,SR.InstanceId) 
	AND Convert(date,SR.returndate) BETWEEN @temp2FromDate AND @temp2ToDate
	AND I.GsTinNo = ISNULL(@GstinNo,I.GsTinNo)
	AND S.Cancelstatus is NULL
	and ISNULL(sri.IsDeleted,0) != 1
	and ISNULL(sr.CancelType,0) != 2	
	GROUP BY isNull(ps.VAT,0)
) A

--! Tax wise data
--- -------------
CREATE TABLE #temp2(GstTotal decimal(18,2), InvoiceAmount decimal(18,2), CGSTTaxAmount decimal(18,2), SGSTTaxAmount decimal(18,2))

INSERT INTO #temp2
SELECT GstTotal, (SUM(InvoiceAmount)-SUM(ReturnInvoiceAmount)) AS InvoiceAmount, (SUM(TaxAmount)-SUM(ReturnTaxAmount))/2 AS CGSTTaxAmount,(SUM(TaxAmount)-SUM(ReturnTaxAmount))/2 AS SGSTTaxAmount
FROM (
	SELECT 
	ISNULL(SI.GstTotal,0) GstTotal, 
	--SUM(CONVERT(decimal(18,2), (SI.Quantity) * (ISNULL(SI.SellingPrice, ISNULL(PS.SellingPrice,0)) * (1 - (case when ISNULL(s.Discount,0) > 0 then s.Discount else ISNULL(si.Discount,0) end / 100))))) As InvoiceAmount, 
	SUM(SI.TotalAmount) As InvoiceAmount, 
	0 as ReturnInvoiceAmount, 
	--SUM((SI.Quantity) * (isNull(SI.SellingPrice, ISNULL(PS.SellingPrice,0)) * (1- (case when ISNULL(s.Discount,0) > 0 then s.Discount else ISNULL(si.Discount,0) end / 100))) / (100 + (IsNull(PS.GstTotal,ISNULL(SI.GstTotal,0)))) * IsNull(PS.GstTotal,ISNULL(SI.GstTotal,0)) ) as TaxAmount, 
	SUM(SI.GstAmount) as TaxAmount, 
	0 as ReturnTaxAmount	
	FROM Sales S WITH(NOLOCK)
	INNER JOIN Instance I WITH(NOLOCK) On I.Id = S.InstanceId
	INNER JOIN SalesItem SI  WITH(NOLOCK) on S.id= SI.salesid
	INNER JOIN ProductStock PS WITH(NOLOCK) on PS.Id=SI.productstockid
	WHERE S.AccountId = @AccountId AND S.InstanceId = ISNULL(@InstanceId,S.InstanceId) 
	AND Convert(date,S.invoicedate) BETWEEN @fromDate AND @toDate
	AND I.GsTinNo = ISNULL(@GstinNo,I.GsTinNo) and S.Cancelstatus is NULL	
	GROUP BY ISNULL(SI.GstTotal,0)

	UNION ALL

	SELECT 
	--case when isNull(ps.GstTotal,0) !=0 then ps.GstTotal else isNull(sri.GstTotal, 0) end GstTotal, 
	isNull(sri.GstTotal, 0) AS GstTotal,
	0 as InvoiceAmount, 
	--SUM(CONVERT(decimal(18,2), (SRI.Quantity) * (ISNULL(SRI.MrpSellingPrice, ISNULL(PS.SellingPrice,0)) * (1 - ((ISNULL(s.Discount,0)+ISNULL(SRI.Discount,0)) / 100))))) As ReturnInvoiceAmount, 
	SUM(SRI.TotalAmount) As ReturnInvoiceAmount, 
	0 as TaxAmount, 
	--SUM((SRI.Quantity) * (isNull(SRI.MrpSellingPrice, ISNULL(PS.SellingPrice,0)) * (1- (ISNULL(s.Discount,0)+ISNULL(SRI.Discount,0)) / 100)) / (100 + (case when isNull(ps.GstTotal,0) !=0 then ps.GstTotal else isNull(sri.GstTotal, 0) end)) * case when isNull(ps.GstTotal,0) !=0 then ps.GstTotal else isNull(sri.GstTotal, 0) end ) as ReturnTaxAmount
	SUM(SRI.GstAmount) AS ReturnTaxAmount
	FROM SalesReturn SR WITH(NOLOCK)
	INNER JOIN Instance I WITH(NOLOCK) On I.Id = SR.InstanceId
	INNER JOIN SalesReturnItem SRI WITH(NOLOCK) ON SRI.SalesReturnId = SR.Id
	INNER JOIN ProductStock PS  WITH(NOLOCK) on PS.Id=SRI.ProductStockId
	LEFT JOIN Sales S WITH(NOLOCK) On SR.SalesId = S.Id
	WHERE SR.AccountId = @AccountId AND SR.InstanceId = ISNULL(@InstanceId,SR.InstanceId) 
	AND Convert(date,SR.returndate) BETWEEN @fromDate AND @toDate
	AND I.GsTinNo = ISNULL(@GstinNo,I.GsTinNo)
	AND S.Cancelstatus is NULL
	and ISNULL(sri.IsDeleted,0) != 1
	and ISNULL(sr.CancelType,0) != 2	
	GROUP BY isNull(sri.GstTotal, 0)
) A
GROUP BY GstTotal
ORDER BY GstTotal

--! Final data
SELECT CAST(@temp1FinYearAmount AS DECIMAL(18, 2)) AS InvAmountNoTaxNoDiscount, NULL GstTotal, CAST(@temp2FinYearAmount AS DECIMAL(18, 6)) AS InvoiceAmountNoTaxNoDiscount, NULL AS CGSTTaxAmt, NULL AS SGSTTaxAmt 
UNION ALL
SELECT 0 InvAmountNoTaxNoDiscount, T2.GstTotal, ISNULL(T2.InvoiceAmount,0) InvoiceAmountNoTaxNoDiscount, ISNULL(T2.CGSTTaxAmount,0) AS CGSTTaxAmount, ISNULL(T2.SGSTTaxAmount,0) AS SGSTTaxAmount FROM #temp2 T2

End