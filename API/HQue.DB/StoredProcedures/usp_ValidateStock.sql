﻿---- Usage
--DECLARE @stockValues VARCHAR(MAX) = '000012a9-e5f1-4ca2-9e87-e61e5ddb03a6~10,0000520f-d8a8-4978-bfd7-5f76953de527~1'
--EXEC usp_ValidateStock 'af1f88a4-3ff0-48b9-b18a-c7497c6cd4a9','b1edce81-dfce-4f75-ba69-d8d7af26cbd2',@stockValues
/*

*/
CREATE PROCEDURE [dbo].[usp_ValidateStock](@AccountId CHAR(36), @InstanceId CHAR(36), @StockIds VARCHAR(MAX))
AS
BEGIN
	DECLARE @xml XML 
	SET @xml = CAST('<ProductStock><ProductStockId>'+REPLACE(REPLACE(REPLACE(ISNULL(@StockIds,''),'~','</ProductStockId><RequiredStock>'),'''',''),',','</RequiredStock></ProductStock><ProductStock><ProductStockId>')+'</RequiredStock></ProductStock>' AS XML)

	SELECT P.Name,PS.ProductId,ProductStockId,RequiredStock,PS.Stock,PS.BatchNo FROM 
	(
		SELECT Tab.Col.value('ProductStockId[1]', 'CHAR(36)') AS ProductStockId,
		Tab.Col.value('RequiredStock[1]', 'DECIMAL(18,2)') AS RequiredStock
		FROM @xml.nodes('ProductStock') AS Tab(Col)
	) R
	INNER JOIN (SELECT * FROM ProductStock(NOLOCK) WHERE AccountId = @AccountId AND InstanceId = @InstanceId) PS ON PS.Id = R.ProductStockId
	INNER JOIN (SELECT * FROM Product(NOLOCK) WHERE AccountId = @AccountId) P ON P.Id = PS.ProductId
	WHERE PS.AccountId = @AccountId AND PS.InstanceId = @InstanceId
	AND ISNULL(PS.Stock,0) < ISNULL(R.RequiredStock,0)
END