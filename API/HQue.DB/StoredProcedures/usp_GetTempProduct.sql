  --Usage : -- usp_GetTempProduct '85d3fdc9-1426-4d3e-8a96-16c78088eaa8','756efa5e-66e1-46ff-a3b2-3f282d746d93','v',''
  --Usage : -- usp_GetTempProduct '85d3fdc9-1426-4d3e-8a96-16c78088eaa8','756efa5e-66e1-46ff-a3b2-3f282d746d93','','9'

CREATE PROCEDURE [dbo].[usp_GetTempProduct](@InstanceId varchar(36),@AccountId varchar(36))
AS
 BEGIN
   SET NOCOUNT ON
    Select p.Name,p.Id from TempVendorPurchaseItem T Inner join ProductStock ps ON T.ProductStockId=ps.id 
 Inner join Product p ON p.id=ps.ProductId
 Where T.InstanceId=@InstanceId  AND T.AccountId  =@AccountId

END