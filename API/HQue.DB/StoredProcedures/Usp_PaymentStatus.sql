﻿/**   
*******************************************************************************                            
** Change History                               
*******************************************************************************                            
** Date        Author          Description                               
*******************************************************************************         
** 17/08/2017   Lawrence       Payment info for vendor
** 17/08/2017   Lawrence       Opening Balance Issue fixed
*******************************************************************************/ 
--USP_PaymentStatus 'c503ca6e-f418-4807-a847-b6886378cf0b', '3e2ec066-1551-4527-be53-5aa3c5b7fb7d', '58fd033d-d401-474a-8e89-ef649535e57e'
CREATE PROC Usp_PaymentStatus(@AccountId char(63), @InstanceId char(63), @VendorId char(36))
AS
Begin

DECLARE @VpId char(63)
select @VpId=vp.Id from VendorPurchase vp 
where vp.AccountId=@AccountId and vp.InstanceId=@InstanceId and vp.VendorId=@VendorId


select isnull(sum(Debit),0) as Debit, isnull(sum(Credit),0) as Credit  from (
select 
SUM(Isnull(p.Debit,0)) AS Debit,
(cast(CAST(dbo.CreditCalculate(@VpId) as Numeric(20,0)) as numeric(20,2)) + (isnull((select (BalanceAmount) from vendor where Id=@VendorId),0))) AS Credit
from Payment p
join VendorPurchase vp on vp.AccountId=p.AccountId and vp.InstanceId=p.InstanceId and vp.VendorId=@VendorId	--p.VendorId 
--and vp.PaymentType = 'Credit'  
where p.VendorId=@VendorId and p.AccountId=@AccountId and p.InstanceId=@InstanceId
HAVING cast(CAST(dbo.CreditCalculate(@VpId) as Numeric(20,0)) as numeric(20,2)) - SUM(p.Debit) + (isnull((select BalanceAmount from vendor where Id=@VendorId),0)) > 0

UNION

select  Isnull(sum(debit),0) Debit, Isnull(sum(Credit),0) Credit from payment 
Where vendorid = @VendorId
and accountid = @AccountId
and instanceid = @InstanceId
and vendorpurchaseid is null 
Having (Sum(Credit)-sum(debit)) > 0

) as A1
/*
select @VpId=vp.Id from VendorPurchase vp 
where vp.AccountId=@AccountId and vp.InstanceId=@InstanceId and vp.VendorId=@VendorId
print @VpId
select --COUNT(vp.Id) as Transcation, 
SUM(Isnull(p.Debit,0)) AS Debit,
--sum(ROUND(p.Debit,0,0)) Debit,  
Isnull(dbo.CreditCalculate(@VpId),0)
--sum(ROUND(p.Credit,0,0)) 
as Credit 
from Payment p
join VendorPurchase vp on vp.AccountId=p.AccountId and vp.InstanceId=p.InstanceId and vp.VendorId=@VendorId	--p.VendorId 
--and vp.PaymentType = 'Credit'  
where p.VendorId=@VendorId and p.AccountId=@AccountId and p.InstanceId=@InstanceId
HAVING 
cast(CAST(dbo.CreditCalculate(@VpId) as Numeric(20,0)) as numeric(20,2)) - SUM(p.Debit) > 0 

select COUNT(vp.Id) as Transcation, sum(ROUND(p.Debit,0,0)) Debit, sum(ROUND(p.Credit,0,0)) Credit from Payment p
join VendorPurchase vp on vp.AccountId=p.AccountId and vp.InstanceId=p.InstanceId and vp.VendorId=@VendorId	--p.VendorId 
and vp.PaymentType = 'Credit'  
where p.VendorId=@VendorId and p.AccountId=@AccountId and p.InstanceId=@InstanceId
HAVING cast(CAST(dbo.CreditCalculate(@VpId) as Numeric(20,0)) as numeric(20,2)) - SUM(p.Debit) > 0 
*/
End