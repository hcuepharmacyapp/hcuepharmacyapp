﻿CREATE PROCEDURE [dbo].[usp_UpdateProductCodes](@AccountId CHAR(36), @ProductIds VARCHAR(MAX))
AS
BEGIN
	UPDATE Product set Product.Code = NULL WHERE AccountId = @AccountId AND ISNUMERIC(Code) != 1 

	UPDATE Product set Product.Code = p.Code
	FROM Product JOIN 
	(
		SELECT gp.Id,(SELECT ISNULL(MAX(CAST(ISNULL(Code,0) AS INT)),0) FROM Product WHERE AccountId = gp.AccountId)+ROW_NUMBER() OVER (PARTITION BY gp.AccountId ORDER BY gp.CreatedAt ASC) as Code
		FROM Product gp WHERE gp.AccountId = @AccountId AND gp.Code IS NULL
	) p on p.Id = Product.Id WHERE Product.AccountId = @AccountId AND Product.Code IS NULL

	DECLARE @XmlList XML
	SET @XmlList = CAST(('<A>'+REPLACE(@ProductIds,',','</A><A>')+'</A>') AS XML)

	SELECT Id,Code FROM Product JOIN 
	(SELECT A.value('.', 'VARCHAR(36)') AS ProductId FROM @XmlList.nodes('A') AS FN(A)) P ON P.ProductId = Product.Id

END