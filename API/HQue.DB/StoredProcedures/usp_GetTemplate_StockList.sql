﻿ /*                               
******************************************************************************                            
** File: [usp_GetTemplate_StockList]   
** Name: [usp_GetTemplate_StockList]                               
** Description: 
**   
** This template can be customized:                               
**                                
** Called by:                                
**                                
**  Parameters:                               
**  Input                Output                               
**  ----------              -----------                               
**   
** Author:Gavaskar S   
** Created Date: 15/10/2017   
**   
*******************************************************************************                            
** Change History                               
*******************************************************************************                            
** Date        Author          Description                               
*******************************************************************************         
  
*******************************************************************************/
 
 -- usp_GetTemplate_StockList 'c503ca6e-f418-4807-a847-b6886378cf0b','3e2ec066-1551-4527-be53-5aa3c5b7fb7d','eb4acda3-1f7a-417b-bb78-cee87af6b6c1','OrderEstimate'
 CREATE PROCEDURE [dbo].[usp_GetTemplate_StockList](@AccountId varchar(36),@InstanceId varchar(36),@TemplateId varchar(36),@Types varchar(36))
 AS
 BEGIN
 SET NOCOUNT ON  
  
declare @getProductId  table(productid char(36),stockid char(36), BatchNo varchar(100), expiredate date , Stock decimal(18,2))
declare @getProductId1  table(assignedQty decimal(18,2),qty decimal(18,2),  productid char(36),stockid char(36), BatchNo varchar(100), expiredate date , stock decimal(18,2))
declare @getProductId2  table(assignedQty decimal(18,2),qty decimal(18,2),  productid char(36),stockid char(36), BatchNo varchar(100), expiredate date , stock decimal(18,2))
DECLARE @ProductId varchar(36), @qty decimal(18,2), @assignedQty decimal(18,2),@count int =0, @stock decimal(18,2),@stockid varchar(36)
 
IF (@Types ='Template')
BEGIN

insert into @getProductId

select  soe.ProductId ,ps.id, ps.BatchNo,ps.ExpireDate,  PS.Stock as Stock from 
(select * from SalesTemplateItem (nolock) where  TemplateId =@TemplateId ) soe left join 
(Select * from productstock(nolock) where instanceid =@InstanceId and cast(Expiredate as date)> cast(getdate() as date) and stock>0 and isnull(status,0) =0)ps  on ps.productid  = soe.productid  order by ps.ExpireDate asc

DECLARE  sstProductId CURSOR for select productid, quantity from SalesTemplateItem (nolock) where  TemplateId =@Templateid 

open sstProductId
FETCH NEXT
FROM  sstProductId INTO @ProductId,@qty 
 while (@@FETCH_STATUS  =0)
 begin

		dECLARE  sstProductId1 CURSOR for select stockid,stock  from  @getProductId where   productid =@ProductId 
		open sstProductId1
		FETCH NEXT
		FROM  sstProductId1 INTO @stockid,@stock 
			 while (@@FETCH_STATUS  =0)
			 begin
				print 'qty:'
				print @qty
				print 'stock:'
				print @stock
				if(@qty>0)
				begin
					 if (@stock >=@qty)
					 begin
						set @assignedQty = @qty
						 
						end 
					else 
					begin
						set @assignedQty = @stock
					end 
			    insert into @getProductId1
				Select  @assignedQty,@qty,*  from  @getProductId where  stockid =@stockid 

				if (@stock >=@qty)
				begin
					set @qty = 0
				end
				else
				begin
					set @qty = @qty- @stock
				end
			   end
			   FETCH NEXT FROM  sstProductId1 INTO @stockid,@stock 
			 end 
			  close sstProductId1
 deallocate  sstProductId1
	 if(@qty>0)
	 begin
		insert into @getProductId1
				Select @qty,@qty, @productId, '', '', '', 0
	end
		FETCH NEXT
FROM sstProductId INTO @ProductId,@qty 
 end 
 close sstProductId
 deallocate  sstProductId

 print 'Out of Cursor:'
 print @qty

insert into @getProductId2
select * from @getProductId1 
union all
select  Quantity as assignedQty,0 as qty,productid,'' as stockid,'' as BatchNo,'' as expiredate,0 as stock from SalesTemplateItem (nolock) where TemplateId =@TemplateId and ProductId in(select productid from @getProductId where productid not in (select productid from @getProductId1))

select  P.Name as Name,ISNULL(PI.RackNo,'') as RackNo,ISNULL(PI.BoxNo,'') as BoxNo,P1.assignedQty as OrderQty, * from @getProductId2 P1
LEFT join (select * from ProductStock (nolock) where InstanceId =@InstanceId  and isnull(status,0) =0  ) ps1  on ps1.id = P1.stockid 
INNER join (select * from product (nolock) where accountid =@Accountid) p on p.id = P1.ProductId
LEFT JOIN (select productid, max(isnull(rackno,'')) [Rackno],  max(isnull(BoxNo,'')) [BoxNo]  from ProductInstance (nolock) where accountid =@Accountid and InstanceId =@InstanceId 
group by productid ) PI ON PI.ProductId = P.Id 
inner join salestemplateITem (nolock) ST on ST.ProductId = P1.productid
where ST.TemplateId=@TemplateId and (ST.IsDeleted =0 or ST.IsDeleted is null) and (p.Status in (0,1) or p.Status is null) and P1.assignedQty > 0  ORDER BY P.Name ASC



END

ELSE IF (@Types ='Order')
BEGIN

insert into @getProductId

select  soe.ProductId ,ps.id, ps.BatchNo,ps.ExpireDate, ps.Stock as Stock from (select * from 
SalesOrderEstimateitem (nolock) where  SalesOrderEstimateId =@TemplateId ) soe left join 
(Select * from productstock(nolock) where instanceid =@InstanceId and cast( Expiredate as date)> cast(getdate() as date )  and Stock > 0  and isnull(status,0) =0 )ps  on ps.productid  = soe.productid  order by ps.ExpireDate asc

DECLARE  sstProductId CURSOR for select productid, (quantity-ISNULL(ReceivedQty,0)) as quantity from SalesOrderEstimateitem (nolock) where  SalesOrderEstimateId =@Templateid 

open sstProductId
FETCH NEXT
FROM  sstProductId INTO @ProductId,@qty 
 while (@@FETCH_STATUS  =0)
 begin

		dECLARE  sstProductId1 CURSOR for select stockid,stock  from  @getProductId where   productid =@ProductId 
		open sstProductId1
		FETCH NEXT
		FROM  sstProductId1 INTO @stockid,@stock 
			 while (@@FETCH_STATUS  =0)
			 begin
				print 'qty:'
				print @qty
				print 'stock:'
				print @stock
				if(@qty>0)
				begin
					 if (@stock >=@qty)
					 begin
						set @assignedQty = @qty
						 
						end 
					else 
					begin
						set @assignedQty = @stock
					end 
			    insert into @getProductId1
				Select  @assignedQty,@qty,*  from  @getProductId where  stockid =@stockid 

				if (@stock >=@qty)
				begin
					set @qty = 0
				end
				else
				begin
					set @qty = @qty- @stock
				end
			   end
			   FETCH NEXT FROM  sstProductId1 INTO @stockid,@stock 
			 end 
			  close sstProductId1
 deallocate  sstProductId1
	 if(@qty>0)
	 begin
		insert into @getProductId1
				Select @qty,@qty, @productId, '', '', '', 0
	end
		FETCH NEXT
FROM sstProductId INTO @ProductId,@qty 
 end 
 close sstProductId
 deallocate  sstProductId

 print 'Out of Cursor:'
 print @qty

insert into @getProductId2
select  * from @getProductId1 
union all
select  Quantity as assignedQty,0 as qty,productid,'' as stockid,'' as BatchNo,'' as expiredate,0 as stock from SalesOrderEstimateitem (nolock) 
where  SalesOrderEstimateId =@TemplateId and  ProductId in(select productid from @getProductId 
where productid not in (select productid from @getProductId1))

select  P.Name as Name,ISNULL(PI.RackNo,'') as RackNo,ISNULL(PI.BoxNo,'') as BoxNo,Cust.Id as PatientId,Cust.Name as PatientName,Cust.Mobile as PatientMobile,

P1.assignedQty as OrderQty,ST.Discount,  * from @getProductId2 P1
LEFT join (select * from ProductStock (nolock) where InstanceId =@InstanceId  and isnull(status,0) =0  ) ps1  on ps1.id = P1.stockid 
INNER join (select * from product (nolock) where accountid =@Accountid) p on p.id = P1.ProductId
LEFT JOIN (select productid, max(isnull(rackno,'')) [Rackno],  max(isnull(BoxNo,'')) [BoxNo]  from ProductInstance (nolock) where accountid =@Accountid and InstanceId =@InstanceId 
group by productid ) PI ON PI.ProductId = P.Id 
inner join SalesOrderEstimateitem (nolock) ST on ST.ProductId = P1.productid
inner join (select * from SalesOrderEstimate (nolock) ) SOE on SOE.id = ST.SalesOrderEstimateId
LEFT join (select * from Patient (nolock) ) Cust on Cust.id = SOE.PatientId
where ST.SalesOrderEstimateId=@TemplateId and (ST.IsDeleted =0 or ST.IsDeleted is null) and (ST.IsOrder =0 or ST.IsOrder is null) and (p.Status in (0,1) or p.Status is null)
and P1.assignedQty > 0  ORDER BY P.Name ASC

END

ELSE IF (@Types ='Estimate')
BEGIN

--insert into @getProductId

--select  (soe.ProductId) as ProductId  ,ps.id, ps.BatchNo,ps.ExpireDate, ps.Stock as Stock from (select * from 
--SalesOrderEstimateitem (nolock) where  SalesOrderEstimateId =@TemplateId ) soe left join 
--(Select * from productstock(nolock) where instanceid =@InstanceId and cast( Expiredate as date)> cast(getdate() as date )  and Stock > 0 and isnull(status,0) =0 )ps  on ps.productid  = soe.productid  
--group by soe.ProductId ,ps.id, ps.BatchNo,ps.ExpireDate, ps.Stock order by ps.ExpireDate asc

--DECLARE  sstProductId CURSOR for select  min(productid) as productid, sum(quantity) as quantity from SalesOrderEstimateitem (nolock) where  SalesOrderEstimateId =@Templateid 
--group by productid--,quantity
--open sstProductId
--FETCH NEXT
--FROM  sstProductId INTO @ProductId,@qty 
-- while (@@FETCH_STATUS  =0)
-- begin
--		dECLARE  sstProductId1 CURSOR for select stockid,Stock  from  @getProductId where   productid =@ProductId 
--		open sstProductId1
--		FETCH NEXT
--		FROM  sstProductId1 INTO @stockid,@stock 
--			 while (@@FETCH_STATUS  =0)
--			 begin
--				print 'qty:'
--				print @qty
--				print 'stock:'
--				print @stock
--				if(@qty>0)
--				begin
--					 if (@stock >=@qty)
--					 begin
--						set @assignedQty = @qty
						 
--						end 
--					else 
--					begin
--						set @assignedQty = @stock
--					end 
--			    insert into @getProductId1
--				Select  @assignedQty,@qty,*  from  @getProductId where  stockid =@stockid 

--				if (@stock >=@qty)
--				begin
--					set @qty = 0
--				end
--				else
--				begin
--					set @qty = @qty- @stock
--				end
--			   end
--			   FETCH NEXT FROM  sstProductId1 INTO @stockid,@stock 
--			 end 
--			  close sstProductId1
-- deallocate  sstProductId1
--	 if(@qty>0)
--	 begin
--		insert into @getProductId1
--				Select @qty,@qty, @productId, '', '', '', 0
--	end
--		FETCH NEXT
--FROM sstProductId INTO @ProductId,@qty 
-- end 
-- close sstProductId
-- deallocate  sstProductId

-- print 'Out of Cursor:'
-- print @qty

--insert into @getProductId2
--select  * from @getProductId1 
--union all
--select  Quantity as assignedQty,0 as qty,productid,'' as stockid,'' as BatchNo,'' as expiredate,0 as stock from SalesOrderEstimateitem (nolock)
--where  SalesOrderEstimateId =@TemplateId and  ProductId in(select productid from @getProductId where productid not in (select productid from @getProductId1))

--select  P.Name as Name,ISNULL(PI.RackNo,'') as RackNo,ISNULL(PI.BoxNo,'') as BoxNo,Cust.Id as PatientId,Cust.Name as PatientName,Cust.Mobile as PatientMobile,
--P1.assignedQty as OrderQty,ST.Discount,  * from @getProductId2 P1
--LEFT join (select * from ProductStock (nolock) where InstanceId =@InstanceId  and isnull(status,0) =0  ) ps1  on ps1.id = P1.stockid 
--INNER join (select * from product (nolock) where accountid =@Accountid) p on p.id = P1.ProductId
--LEFT JOIN (select productid, max(isnull(rackno,'')) [Rackno],  max(isnull(BoxNo,'')) [BoxNo]  from ProductInstance (nolock) where accountid =@Accountid and InstanceId =@InstanceId 
--group by productid ) PI ON PI.ProductId = P.Id 
--inner join (select productid,SalesOrderEstimateId,IsDeleted,IsOrder,Discount from SalesOrderEstimateitem (nolock) group by productid,SalesOrderEstimateId,IsDeleted,IsOrder,Discount)  ST on ST.ProductId = P1.productid
--inner join (select * from SalesOrderEstimate (nolock)) SOE on SOE.id = ST.SalesOrderEstimateId
--LEFT join (select * from Patient (nolock) ) Cust on Cust.id = SOE.PatientId
--where ST.SalesOrderEstimateId=@TemplateId and (ST.IsDeleted =0 or ST.IsDeleted is null) and (ST.IsOrder =0 or ST.IsOrder is null) and (p.Status in (0,1) or p.Status is null)
--and P1.assignedQty > 0  ORDER BY P.Name ASC

select  P.Name as Name,ISNULL(PI.RackNo,'') as RackNo,ISNULL(PI.BoxNo,'') as BoxNo,Cust.Id as PatientId,Cust.Name as PatientName,Cust.Mobile as PatientMobile,
P1.Quantity as OrderQty,P1.Discount,ps1.id as stockid,  * from (SELECT * FROM salesorderestimateitem WHERE salesorderestimateId = @TemplateId AND
(IsDeleted =0 or IsDeleted is null) and (IsOrder =0 or IsOrder is null)) P1
INNER join (select * from ProductStock (nolock) where Accountid = @Accountid AND InstanceId = @InstanceId and isnull(status,0) = 0) ps1 on ps1.id = P1.ProductStockid 
INNER join (select * from product (nolock) where Accountid = @Accountid) p on p.id = P1.ProductId
LEFT JOIN (select productid, max(isnull(rackno,'')) [Rackno],  max(isnull(BoxNo,'')) [BoxNo]  from ProductInstance (nolock) where accountid =@Accountid and InstanceId =@InstanceId 
group by productid ) PI ON PI.ProductId = P.Id 
--inner join (select productid,SalesOrderEstimateId,IsDeleted,IsOrder,Discount from SalesOrderEstimateitem (nolock) group by productid,SalesOrderEstimateId,IsDeleted,IsOrder,Discount)  ST on ST.ProductId = P1.productid
inner join (select * from SalesOrderEstimate (nolock)) SOE on SOE.id = P1.SalesOrderEstimateId
INNER join (select * from Patient (nolock) ) Cust on Cust.id = SOE.PatientId
where (p.Status in (0,1) or p.Status is null)
--and ps1.stock > 0  
ORDER BY P.Name ASC

END

END