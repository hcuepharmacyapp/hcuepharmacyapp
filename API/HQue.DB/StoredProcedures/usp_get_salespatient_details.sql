﻿/*                               
******************************************************************************                            
 --[usp_get_salespatient_details] 'c503ca6e-f418-4807-a847-b6886378cf0b','508ddc99-7147-47be-94a3-113e0bbdb81f','dep',0
**   
*******************************************************************************                            
** Change History                               
*******************************************************************************                            
** Date        Author          Description                               
*******************************************************************************    
 27-05-2017     Violet        Patient detils
*******************************************************************************/ 
CREATE PROC [dbo].[usp_get_salespatient_details] ( 
@AccountId  NVARCHAR(36), 
@InstanceId NVARCHAR(36), 
@Name       VARCHAR(50), 
@Dept       INT 
 ) 
AS 
  BEGIN 
   
	--When dept 1
	IF @Dept = 1
	BEGIN
		SELECT Patient.Id,Patient.AccountId,Patient.InstanceId,Patient.Code,Patient.Name,Patient.ShortName,Patient.Mobile,
		Patient.Email,Patient.DOB,Patient.Age,Patient.Gender,Patient.Address,Patient.Area,Patient.City,Patient.State,
		Patient.Pincode,Patient.OfflineStatus,Patient.CreatedAt,Patient.UpdatedAt,Patient.CreatedBy,Patient.UpdatedBy,
		Patient.EmpID,Patient.Discount,Patient.IsSync,Patient.CustomerPaymentType,Patient.PatientType FROM Patient Inner join
		Sales on Patient.id = Sales.PatientId
		WHERE Patient.Name  Like  @Name+'%' AND Patient.AccountId  =  @AccountId and (PatientType =1)
		union
		SELECT Patient.Id,Patient.AccountId,Patient.InstanceId,Patient.Code,Patient.Name,Patient.ShortName,Patient.Mobile,
		Patient.Email,Patient.DOB,Patient.Age,Patient.Gender,Patient.Address,Patient.Area,Patient.City,Patient.State,
		Patient.Pincode,Patient.OfflineStatus,Patient.CreatedAt,Patient.UpdatedAt,Patient.CreatedBy,Patient.UpdatedBy,
		Patient.EmpID,Patient.Discount,Patient.IsSync,Patient.CustomerPaymentType,Patient.PatientType FROM Patient Inner join
		Sales on Patient.id = Sales.PatientId
		WHERE Patient.Name  Like  @Name+'%' AND Patient.AccountId  =  @AccountId AND Patient.InstanceId  =  @InstanceId 
		and (PatientType =2)
	END

--When dept 0
IF @Dept = 0
	BEGIN
		SELECT Patient.Id,Patient.AccountId,Patient.InstanceId,Patient.Code,Patient.Name,Patient.ShortName,Patient.Mobile,
		Patient.Email,Patient.DOB,Patient.Age,Patient.Gender,Patient.Address,Patient.Area,Patient.City,Patient.State,
		Patient.Pincode,Patient.OfflineStatus,Patient.CreatedAt,Patient.UpdatedAt,Patient.CreatedBy,Patient.UpdatedBy,
		Patient.EmpID,Patient.Discount,Patient.IsSync,Patient.CustomerPaymentType,Patient.PatientType FROM Patient Inner join
		Sales on Patient.id = Sales.PatientId
		WHERE Patient.Name  Like  @Name+'%' AND Patient.AccountId  =  @AccountId --AND Patient.InstanceId  =  @InstanceId 
		and (PatientType <> 2 or PatientType is null)
	END
  END