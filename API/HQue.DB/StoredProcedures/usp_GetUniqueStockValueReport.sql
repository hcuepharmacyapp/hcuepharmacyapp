/*******************************************************************************                            
** Change History                               
*******************************************************************************                            
** Date        Author          Description                               
*******************************************************************************         
** 11/10/2017	nandhini		gst column added
*******************************************************************************/

 CREATE PROCEDURE [dbo].[usp_GetUniqueStockValueReport](@InstanceId varchar(36),@AccountId varchar(36),@ReportNo varchar(50))
 AS
 BEGIN
 SET NOCOUNT ON  
  
select isnull(Category,'') Category,ProductCount,isnull(TotalPurchasePrice,0) TotalPurchasePrice,isnull(TotalMrp,0) TotalMrp, isnull(Profit,0) Profit, isnull(Vat0PurchasePrice,0) Vat0PurchasePrice,isnull(Vat5PurchasePrice,0) Vat5PurchasePrice,isnull(Vat145PurchasePrice,0) Vat145PurchasePrice,
isnull(VatOtherPurchasePrice,0) VatOtherPurchasePrice,isnull(Gst0PurchasePrice,0) Gst0PurchasePrice,isnull(Gst5PurchasePrice,0) Gst5PurchasePrice,isnull(Gst12PurchasePrice,0) Gst12PurchasePrice,isnull(Gst18PurchasePrice,0) Gst18PurchasePrice,isnull(Gst28PurchasePrice,0) Gst28PurchasePrice,isnull(GstOtherPurchasePrice,0) GstOtherPurchasePrice from StockValueReport 
where AccountId=@AccountId and InstanceId=@InstanceId
and ReportNo=@ReportNo
order by Category

  

END

 