
/*                             
******************************************************************************                            
** File: [usp_GetSalesList] 
** Name: [usp_GetSalesList]                             
** Description: To Get Sales   details 
** 
** This template can be customized:                             
**                              
** Called by:                              
**                              
**  Parameters:                             
**  Input                Output                             
**  ----------              -----------                             
** 
** Author: 
** Created Date:  
** 
*******************************************************************************                            
** Change History                             
*******************************************************************************                            
** Date        Author          Description                             
*******************************************************************************       
** 03/02/2017 Poongodi R		Isnull added   
** 17/02/2017  Poongodi R		Alias name given by invoice date
** 17/05/2017  Poongodi R		Sales Name added
** 21/06/2017   Violet			Prefix Addded
** 13/09/2017   Lawrence		All branch condition Added 
** 13/09/2017	Poongodi R		Instance Name added
** 14/09/2017	Poongodi		Int changed to Bigint
** 30/11/2018	Sumathi			IANULL Added for SaleAmount, RoundoffSaleAmount
*******************************************************************************/ 


 --Usage : -- usp_GetSalesList '013513b1-ea8c-4ea8-9fed-054b260ee197','18204879-99ff-4efd-b076-f85b4a0da0a3','2016-11-01 00:00:00','2016-11-30 23:59:59'
 CREATE PROCEDURE [dbo].[Usp_getsaleslist](@InstanceId varchar(36),@AccountId varchar(36),@StartDate datetime,@EndDate datetime, @SearchOption varchar(50),
 @SearchValue varchar(50), @FromInvoice bigint, @ToInvoice bigint)
 AS
 BEGIN


 declare @InvoiceSeries varchar(100) = ''

if (@SearchOption ='1')

select  @InvoiceSeries = '' 

else  if (@SearchOption ='2')

select  @InvoiceSeries = isnull(@SearchValue,'') 
else  if (@SearchOption ='4')

select  @InvoiceSeries = isnull(@SearchValue,'') 

else  if (@SearchOption ='3')

select  @InvoiceSeries = 'MAN'

if(@FromInvoice=0  OR @FromInvoice='')
select  @FromInvoice =null

if(@ToInvoice=0 OR @ToInvoice='')
select  @ToInvoice =null




SET NOCOUNT ON
 
IF (@SearchOption ='1')
BEGIN

SELECT max( S.InvoiceDate) InvoiceDate,S.InvoiceNo,ltrim(isnull(s.prefix,'')+isnull(s.InvoiceSeries, '')) as InvoiceSeries,
max(S.Name) [Name],
--CONVERT(decimal(18,2), SUM(ISNULL(SI.SellingPrice, PS.SellingPrice ) *SI.quantity)) As InvoiceAmount,
S.SalesItemAmount As InvoiceAmount,
ISNULL(S.SaleAmount,0) SaleAmount,
--CONVERT(decimal(18,2), 
--CASE  IsNull(S.Discount,0) WHEN 0 THEN isNull(S.DiscountValue,0) ELSE sum(SI.Quantity * ISNULL(SI.SellingPrice, PS.SellingPrice ) * isnull(S.Discount,0) / 100) END )as Discount,
isnull(S.TotalDiscountValue,0) as TotalDiscountValue,
--CONVERT(decimal(18,2), 
--sum(SI.Quantity * ISNULL(SI.SellingPrice, PS.SellingPrice ) * isnull(SI.Discount,0) / 100)) as DiscountSum ,
S.salesType,ST.name as SalesTypeName	 , Ins.Name [Branch],
ISNULL(S.RoundoffSaleAmount,0) RoundoffSaleAmount,
S.IsRoundOff 
FROM Sales S   (NOLOCK)
Inner join (Select * from instance where accountid= @AccountId ) Ins on Ins.id = s.InstanceId

INNER JOIN SalesItem SI (NOLOCK)    on S.id= SI.salesid
--INNER JOIN ProductStock PS  (NOLOCK)   on PS.Id=SI.productstockid
LEFT JOIN SalesType ST (NOLOCK) on ST.Id = S.salesType and ST.AccountId = S.AccountId and ST.InstanceId = S.InstanceId    
WHERE S.AccountId = @AccountId AND S.InstanceId = ISNULL(@InstanceId,S.InstanceId) 
AND S.invoicedate BETWEEN @StartDate AND @EndDate 
AND isnull(S.InvoiceSeries,'') =''
AND Isnull(S.InvoiceNo,'') BETWEEN isnull(@FromInvoice,Isnull(S.InvoiceNo,'')) AND isnull(@ToInvoice,Isnull(S.InvoiceNo,''))
AND S.Cancelstatus is NULL
GROUP BY S.invoicedate,S.invoiceno,S.InvoiceSeries,S.Prefix,S.CreatedAt,S.salesType,ST.Name , Ins.Name,S.SaleAmount,S.RoundoffSaleAmount,
S.IsRoundOff,S.SalesItemAmount,S.TotalDiscountValue
ORDER BY  cast (InvoiceNo as bigint) desc

END

ELSE IF (@SearchOption ='2')
BEGIN

SELECT max( S.InvoiceDate) InvoiceDate,S.InvoiceNo,ltrim(isnull(s.prefix,'')+isnull(s.InvoiceSeries, '')) as InvoiceSeries,
max(S.Name) [Name],
--CONVERT(decimal(18,2), SUM(ISNULL(SI.SellingPrice, PS.SellingPrice ) *SI.quantity)) As InvoiceAmount,
S.SalesItemAmount As InvoiceAmount,
ISNULL(S.SaleAmount,0) SaleAmount,
--CONVERT(decimal(18,2), CASE IsNull(S.Discount,0 ) WHEN 0 THEN isNull(S.DiscountValue,0) ELSE sum(SI.Quantity * ISNULL(SI.SellingPrice, PS.SellingPrice )
-- * isnull(S.Discount,0) / 100) END )as Discount,
isnull(S.TotalDiscountValue,0) as TotalDiscountValue,
--CONVERT(decimal(18,2), sum(SI.Quantity * ISNULL(SI.SellingPrice, PS.SellingPrice ) * isnull(SI.Discount,0) / 100)) as DiscountSum ,
S.salesType,ST.name as SalesTypeName , Ins.Name [Branch],
ISNULL(S.RoundoffSaleAmount,0) RoundoffSaleAmount,
S.IsRoundOff 	
FROM Sales S  (NOLOCK)
Inner join (Select * from instance(NOLOCK) where accountid= @AccountId ) Ins on Ins.id = s.InstanceId
LEFT JOIN InvoiceSeriesItem ISI (NOLOCK) ON ISI.SeriesName=S.InvoiceSeries and S.InstanceId = ISI.InstanceId AND S.AccountId = ISI.AccountId
INNER JOIN SalesItem SI (NOLOCK)    on S.id= SI.salesid
--INNER JOIN ProductStock PS (NOLOCK)    on PS.Id=SI.productstockid
LEFT JOIN SalesType ST (NOLOCK) on ST.Id = S.salesType and ST.AccountId = S.AccountId and ST.InstanceId = S.InstanceId    
WHERE S.AccountId = @AccountId AND S.InstanceId = ISNULL(@InstanceId,S.InstanceId) 
AND S.invoicedate BETWEEN @StartDate AND @EndDate 
AND Isnull(S.InvoiceNo,'') BETWEEN isnull(@FromInvoice,Isnull(S.InvoiceNo,'')) AND isnull(@ToInvoice,Isnull(S.InvoiceNo,''))
AND isnull(S.InvoiceSeries,'')+isnull( S.InvoiceNo,'') like isnull(@InvoiceSeries,'')+'%' 
AND isnull(S.InvoiceSeries,'') !='' AND isnull(S.InvoiceSeries,'') !='MAN'
AND S.Cancelstatus is NULL  and ISI.InvoiceseriesType=2
GROUP BY S.invoicedate,S.invoiceno,S.InvoiceSeries,S.Prefix,S.CreatedAt,S.salesType,ST.Name , Ins.Name,S.SaleAmount,S.RoundoffSaleAmount,
S.IsRoundOff,S.SalesItemAmount,S.TotalDiscountValue
ORDER BY  cast (InvoiceNo as bigint)  desc

END

ELSE IF (@SearchOption ='4')
BEGIN

SELECT max( S.InvoiceDate) InvoiceDate,S.InvoiceNo,ltrim(isnull(s.prefix,'')+isnull(s.InvoiceSeries, '')) as InvoiceSeries,
max(S.Name) [Name],
--CONVERT(decimal(18,2), SUM(ISNULL(SI.SellingPrice, PS.SellingPrice ) *SI.quantity)) As InvoiceAmount,
S.SalesItemAmount As InvoiceAmount,
ISNULL(S.SaleAmount,0) SaleAmount,
--CONVERT(decimal(18,2), CASE IsNull(S.Discount,0) WHEN 0 THEN isNull(S.DiscountValue,0) ELSE sum(SI.Quantity * ISNULL(SI.SellingPrice, PS.SellingPrice ) * 
--isnull(S.Discount,0) / 100) END ) as Discount,
isnull(S.TotalDiscountValue,0) as TotalDiscountValue,
--CONVERT(decimal(18,2), sum(SI.Quantity * ISNULL(SI.SellingPrice, PS.SellingPrice ) * isnull(SI.Discount,0) / 100) )as DiscountSum ,
S.salesType,ST.name as SalesTypeName	 , Ins.Name [Branch],
ISNULL(S.RoundoffSaleAmount,0) RoundoffSaleAmount,
S.IsRoundOff 
FROM Sales S   (NOLOCK)
Inner join (Select * from instance(NOLOCK) where accountid= @AccountId ) Ins on Ins.id = s.InstanceId
LEFT JOIN InvoiceSeriesItem ISI (NOLOCK) ON ISI.SeriesName=S.InvoiceSeries and S.InstanceId = ISI.InstanceId AND S.AccountId = ISI.AccountId 
INNER JOIN SalesItem SI (NOLOCK)    on S.id= SI.salesid
--INNER JOIN ProductStock PS (NOLOCK)    on PS.Id=SI.productstockid
LEFT JOIN SalesType ST (NOLOCK) on ST.Id = S.salesType and ST.AccountId = S.AccountId and ST.InstanceId = S.InstanceId    
WHERE S.AccountId = @AccountId AND S.InstanceId = ISNULL(@InstanceId,S.InstanceId) 
AND S.invoicedate BETWEEN @StartDate AND @EndDate 
AND Isnull(S.InvoiceNo,'') BETWEEN isnull(@FromInvoice,Isnull(S.InvoiceNo,'')) AND isnull(@ToInvoice,Isnull(S.InvoiceNo,''))
AND isnull(S.InvoiceSeries,'')+isnull( S.InvoiceNo,'') like isnull(@InvoiceSeries,'')+'%' 
AND isnull(S.InvoiceSeries,'') !='' AND isnull(S.InvoiceSeries,'') !='MAN'
AND S.Cancelstatus is NULL and ISI.InvoiceseriesType=4
GROUP BY S.invoicedate,S.invoiceno,S.InvoiceSeries,S.Prefix,S.CreatedAt,S.salesType,ST.Name , Ins.Name,S.SaleAmount,S.RoundoffSaleAmount,
S.IsRoundOff,S.SalesItemAmount,S.TotalDiscountValue 
ORDER BY  cast (InvoiceNo as bigint) desc

END

ELSE 
BEGIN

SELECT max( S.InvoiceDate) InvoiceDate,S.InvoiceNo,ltrim(isnull(s.prefix,'')+isnull(s.InvoiceSeries, '')) as InvoiceSeries,
max(S.Name) [Name],
--CONVERT(decimal(18,2), SUM(ISNULL(SI.SellingPrice, PS.SellingPrice ) *SI.quantity)) As InvoiceAmount,
S.SalesItemAmount As InvoiceAmount,
ISNULL(S.SaleAmount,0) SaleAmount,
--CONVERT(decimal(18,2), CASE IsNull(S.Discount,0) WHEN  0 THEN isNull(S.DiscountValue,0) ELSE sum(SI.Quantity * 
--ISNULL(SI.SellingPrice, PS.SellingPrice ) * isnull(S.Discount,0) / 100)  END ) as Discount,
isnull(S.TotalDiscountValue,0) as TotalDiscountValue,
--CONVERT(decimal(18,2), sum(SI.Quantity * ISNULL(SI.SellingPrice, PS.SellingPrice ) * isnull(SI.Discount,0) / 100)) as DiscountSum ,
S.salesType,ST.name as SalesTypeName , Ins.Name [Branch],
ISNULL(S.RoundoffSaleAmount,0) RoundoffSaleAmount,
S.IsRoundOff 	
FROM Sales S  (NOLOCK)
Inner join (Select * from instance(NOLOCK) where accountid= @AccountId ) Ins on Ins.id = s.InstanceId

INNER JOIN SalesItem SI (NOLOCK)    on S.id= SI.salesid
--INNER JOIN ProductStock PS (NOLOCK)    on PS.Id=SI.productstockid
LEFT JOIN SalesType ST (NOLOCK) on ST.Id = S.salesType and ST.AccountId = S.AccountId and ST.InstanceId = S.InstanceId
    
WHERE S.AccountId = @AccountId AND S.InstanceId = ISNULL(@InstanceId,S.InstanceId) 
AND S.invoicedate BETWEEN @StartDate AND @EndDate 
AND Isnull(S.InvoiceNo,'') BETWEEN isnull(@FromInvoice,Isnull(S.InvoiceNo,'')) AND isnull(@ToInvoice,Isnull(S.InvoiceNo,''))
AND isnull(S.InvoiceSeries,'')+isnull( S.InvoiceNo,'') like isnull(@InvoiceSeries,'')+'%'
AND S.Cancelstatus is NULL
GROUP BY S.invoicedate,S.invoiceno,S.InvoiceSeries,S.Prefix,S.CreatedAt,S.salesType,ST.Name , Ins.Name,S.SaleAmount,S.RoundoffSaleAmount,
S.IsRoundOff,S.SalesItemAmount,S.TotalDiscountValue 
ORDER BY  cast (InvoiceNo as bigint) desc

END

  
END