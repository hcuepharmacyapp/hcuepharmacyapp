
/*                             
******************************************************************************                            
** File: [usp_tallyReturnSaleReport] 
** Name: [TallySaleReturnReport]                             
** Description: To Get Sales   details 
** 
** This template can be customized:                             
**                              
** Called by:                              
**                              
**  Parameters:                             
**  Input                Output                             
**  ----------              -----------                             
** 
** Author: 
** Created Date:  
** 
*******************************************************************************                            
** Change History                             
*******************************************************************************                            
** Date        Author          Description                             
*******************************************************************************       
** 04/07/2018 Bikas		Columns added and some columns removed.   
*******************************************************************************/ 
create PROCEDURE [dbo].[TallySaleReturnReport](
@InstanceId varchar(36),@AccountId varchar(36),@StartDate Datetime,@EndDate Datetime )
 AS
 BEGIN
 if (@AccountId = '6137e806-7a11-43ab-9f33-23ad4e3d71a6')
BEGIN
select sa.InvoiceDate,(isnull(sr.Prefix,'') + isnull(sr.InvoiceSeries,'') + sr.ReturnNo) InvoiceNo,a.CustomerPaymentType,sa.Name CustomerName,a.GsTin,ps.HsnCode,p.NAME SaleProductName,sri.Cgst ,sri.Sgst,sri.Igst,sri.MrpSellingPrice,sri.Quantity,sri.GstTotal,
sri.GstAmount/2 AS CGSTAmount,(sri.GstAmount/2) AS SGSTAmount,sri.DiscountAmount AS DiscountValue,sri.GstAmount,sa.RoundoffSaleAmount AS RoundOff,sri.TotalAmount,V.Name	,'22' As StateId,V.VendorType ,'1001' As salereturncode,Case  WHEN V.VendorType = 1 THEN 'R'
When V.VendorType = 2 THEN 'UR'
When V.VendorType = 3 THEN 'C'
END as VendorTypeDesc
from SalesReturn sr with(nolock) INNER JOIN 
SalesReturnItem sri with(nolock) on sr.Id=sri.salesreturnid
  LEFT JOIN sales sa (nolock)
                     ON sa.id = sr.salesid 
   INNER JOIN productstock ps (nolock)
                     ON ps.id = sri.productstockid 
             INNER JOIN product p (nolock)
                     ON p.id = ps.productid 
					 INNER JOIN Vendor V (nolock) on ps.VendorId = V.Id
   left join CustomerPayment as cp WITH(NOLOCK) on  cp.SalesId = sa.Id
   inner Join Patient a On a.id  = sa.PatientId
where sr.AccountId=@AccountId and sr.InstanceId=@InstanceId and sr.CreatedAt between @StartDate and @EndDate
order by InvoiceDate ,sr.CreatedAt
END
else 
BEGIN
select sa.InvoiceDate,(isnull(sr.Prefix,'') + isnull(sr.InvoiceSeries,'') + sr.ReturnNo) InvoiceNo,a.CustomerPaymentType,sa.Name CustomerName,a.GsTin,ps.HsnCode,p.NAME SaleProductName,sri.Cgst ,sri.Sgst,sri.Igst,sri.MrpSellingPrice,sri.Quantity,sri.GstTotal,
sri.GstAmount/2 AS CGSTAmount,(sri.GstAmount/2) AS SGSTAmount,sri.DiscountAmount AS DiscountValue,sri.GstAmount,sa.RoundoffSaleAmount AS RoundOff,sri.TotalAmount,V.Name	,V.StateId,V.VendorType ,'1001' As salereturncode,Case  WHEN V.VendorType = 1 THEN 'R'
When V.VendorType = 2 THEN 'UR'
When V.VendorType = 3 THEN 'C'
END as VendorTypeDesc
from SalesReturn sr with(nolock) INNER JOIN 
SalesReturnItem sri with(nolock) on sr.Id=sri.salesreturnid
  LEFT JOIN sales sa (nolock)
                     ON sa.id = sr.salesid 
   INNER JOIN productstock ps (nolock)
                     ON ps.id = sri.productstockid 
             INNER JOIN product p (nolock)
                     ON p.id = ps.productid 
					 INNER JOIN Vendor V (nolock) on ps.VendorId = V.Id
   left join CustomerPayment as cp WITH(NOLOCK) on  cp.SalesId = sa.Id
   LEFT Join Patient a On a.id  = sa.PatientId
where sr.AccountId=@AccountId and sr.InstanceId=@InstanceId and sr.CreatedAt between @StartDate and @EndDate

order by InvoiceDate ,sr.CreatedAt
END
END
