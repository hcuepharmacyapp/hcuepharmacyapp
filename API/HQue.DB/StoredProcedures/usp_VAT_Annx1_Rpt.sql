 
/*                              
******************************************************************************                            
** File: [usp_VAT_Annx1_Rpt]  
** Name: [usp_VAT_Annx1_Rpt]                              
** Description: To Get Purchase details  
**  
** This template can be customized:                              
**                               
** Called by:                               
**                               
**  Parameters:                              
**  Input                Output                              
**  ----------              -----------                              
**  
** Author:Poongodi R  
** Created Date: 27/01/2017  
**  
*******************************************************************************                            
** Change History                              
*******************************************************************************                            
** Date        Author          Description                              
*******************************************************************************        
** 02/02/20017  POongodi    @vatPer Param added    
** 03/02/2017  Poongodi    Vat Calculation corrected (Changed from Purchaseprice to Packageprice)
 **08/03/2017  Poongodi     Po cancel status validation added 
 ** 28/03/2017	Poongodi		Taxtype validation added
 ** 30/03/2017  Poongodi			Invoice date changed to created at
 ** 30/06/2017  Poongodi			TaxRefNovalidation added
*******************************************************************************/ 
Create PROC dbo.Usp_vat_annx1_rpt(@AccountId  CHAR(36), 
                                  @InstanceId CHAR(36), 
                                  @StartDate  DATETIME, 
                                  @EndDate    DATETIME, 
                                  @vatPer     INT) 
AS 
  BEGIN 
      DECLARE @Vat INT =0 

      /* 
      Commodity code hard coded based on VAT % 
      VAT    Commoditycode 
      5      2044 
      14.5    301 
      0 or other 752 
       
      Category 
      Purchare - R for all vat % 
       
         
      */ 
	    declare @GstDate date ='2017-06-30'
      SELECT [vendor.name]                                [VendorName], 
             [vendor.tinno]                               [VendorTinNo], 
             CASE Isnull([productstock.vat], 0) 
               WHEN 5 THEN '2044' 
               WHEN 14.5 THEN '301' 
               ELSE '752' 
             END                                          [Commoditycode], 
             [vendorpurchase.invoiceno]                   [InvoiceNo], 
             [vendorpurchase.invoicedate]                 [InvoiceDate], 
             Sum(Isnull([vendorpurchaseitem.povalue], 0) + ( 
                 Isnull([vendorpurchaseitem.povalue], 0) * 
                 Isnull([productstock.vat], 0) / 
                 100 
                   ))                                     InvoiceValue, 
             Sum(Isnull([vendorpurchaseitem.povalue], 0)) POValue, 
             [productstock.vat]                           [VAT], 
             Round(Sum(Isnull([vendorpurchaseitem.povalue], 0)) * 
                   Isnull([productstock.vat], 0) / 
                   100, 2)                                [VatValue], 
             'R'                                          [Category] 
      FROM   (SELECT vendorpurchaseitem.productstockid, 
                     vendorpurchaseitem.quantity, 
                     vendorpurchaseitem.purchaseprice, 
                     vendorpurchaseitem.packagepurchaseprice, 
                     vendorpurchaseitem.packageqty, 
                     vendorpurchaseitem.packagesellingprice, 
                     vendorpurchaseitem.freeqty, 
                     vendorpurchaseitem.discount, 
                     vendorpurchase.id          AS [VendorPurchase.Id], 
                     vendorpurchase.invoiceno   AS [VendorPurchase.InvoiceNo], 
                     vendorpurchase.invoicedate AS [VendorPurchase.InvoiceDate], 
                     vendorpurchase.discount    AS [VendorPurchase.Discount], 
                     vendor.id                  AS [Vendor.Id], 
                     vendor.NAME                AS [Vendor.Name], 
                     vendor.tinno               AS [Vendor.TinNo], 
                     productstock.sellingprice  AS [ProductStock.SellingPrice], 
                     productstock.vat           AS [ProductStock.VAT], 
                     Isnull(vendorpurchaseitem.packagepurchaseprice, 0) * ( 
                     Isnull 
                     ( 
                     vendorpurchaseitem.packageqty, 0) 
                     - 
                                          Isnull 
                     ( 
                     vendorpurchaseitem.freeqty, 0) ) - ( 
                     Isnull( 
                     vendorpurchaseitem.packagepurchaseprice, 0) * 
     ( 
                     Isnull 
                     ( 
                     vendorpurchaseitem.packageqty, 0) 
                     - 
                                          Isnull 
                     ( 
                     vendorpurchaseitem.freeqty, 0) ) * 
      Isnull 
      ( 
             vendorpurchaseitem.discount, 0) / 
      100 ) 
      [VendorPurchaseItem.POValue] 
      FROM   vendorpurchaseitem 
      INNER JOIN vendorpurchase 
      ON vendorpurchase.id = vendorpurchaseitem.vendorpurchaseid 
      INNER JOIN vendor 
      ON vendor.id = vendorpurchase.vendorid 
      INNER JOIN productstock 
      ON productstock.id = vendorpurchaseitem.productstockid 
      INNER JOIN product 
      ON product.id = productstock.productid 
      WHERE cast( vendorpurchase.createdat as date)BETWEEN @StartDate AND @EndDate 
	  and isnull(vendorpurchase.TaxRefNo,0) =0
	   and CONVERT(DATE, vendorpurchase.createdat) <= @GstDate
	  and isnull(Vendor.TaxType,'Tax') in ( 'Tax','')
	   and (vendorpurchaseitem.Status is null or vendorpurchaseitem.Status = 1)
	  and isnull(VendorPurchase.CancelStatus ,0)= 0
      AND vendorpurchase.accountid = @AccountId 
      AND vendorpurchase.instanceid = @InstanceId 
      AND ( ( ( productstock.vat = 0 ) 
      AND ( @vatPer = 0 ) ) 
      OR ( ( productstock.vat > 0 ) 
      AND ( @vatPer = 1 ) ) ))AS one 
      GROUP  BY [vendor.name], 
                [vendor.tinno], 
                [vendorpurchase.invoiceno], 
                [vendorpurchase.invoicedate], 
                [productstock.vat] 
      ORDER  BY [vendorpurchase.invoicedate],
				[vendor.name]
                
  END 