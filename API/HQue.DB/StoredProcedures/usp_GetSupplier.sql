﻿/*******************************************************************************                            
** Change History                               
*******************************************************************************                            
** Date        Author          Description                               
*******************************************************************************         
** 27/04/2017	Poongodi R	  Vendor InstanceId mpping removed and Instanceid map to Vendor purchase
*******************************************************************************/
create PROCEDURE [dbo].[usp_GetSupplier](@InstanceId varchar(36),@AccountId varchar(36),@Name varchar(50),@Mobile varchar(20))
AS
 BEGIN
   SET NOCOUNT ON
   /*
   declare @condition nvarchar(max)	
	Declare @qry nvarchar(max)
	SET @condition=''

	if(@Name is not null AND @Name!='')
	begin
		set @condition=@condition+' AND Name like'+''''+@Name +'%'''
	end	

	if(@Mobile is not null AND @Mobile!='')
	begin
		set @condition=@condition+' AND Mobile like'+''''+@Mobile +'%'''
	end

	SET @qry='SELECT Name,Mobile,Id FROM Vendor WHERE InstanceId  = ''' +@InstanceId +''''+' AND AccountId  ='''+  @AccountId +''''+@condition
	*/
	select @Name = isnull(@Name,'') , @Mobile = isnull(@Mobile,'')
	select Name,Mobile,Id FROM Vendor WHERE AccountId  =   @AccountId  and name like @Name +'%' AND isnull(Mobile,'')  like @Mobile +'%'
	--print @qry
 

END