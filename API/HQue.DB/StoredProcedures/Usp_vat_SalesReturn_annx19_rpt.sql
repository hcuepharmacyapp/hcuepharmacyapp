 

/*                             
******************************************************************************                            
** File: [Usp_vat_SalesReturn_annx19_rpt] 
** Name: [Usp_vat_SalesReturn_annx19_rpt]                             
** Description: To Get Sales and Return details 
** 
** This template can be customized:                             
**                              
** Called by:                              
**                              
**  Parameters:                             
**  Input                Output                             
**  ----------              -----------                             
** 
** Author:Poongodi R 
** Created Date: 11/02/2017 
** 
*******************************************************************************                            
** Change History                             
*******************************************************************************                            
** Date        Author          Description                             
*******************************************************************************       
** 13/02/2017	Poongodi		Invoiceseries col added along with invoicenumber 
** 31/05/2017	Poongodi		Sales  mapping changed from inner to left 
** 13/06/2017   Violet			Cancelstatus handled 
** 19/06/2017	Poongodi		Cancel Blocked in Return
** 22/06/2017   Violet			Prefix Added
** 30/06/2017  Poongodi			TaxRefNovalidation added
*******************************************************************************/ 
CREATE PROC [dbo].[Usp_vat_SalesReturn_annx19_rpt](@AccountId  CHAR(36), 
                                  @InstanceId CHAR(36), 
                                  @StartDate  DATETIME, 
                                  @EndDate    DATETIME) 
AS 
  BEGIN 
 

	  /*
	  Commodity code hard coded based on VAT %
	  VAT		Commoditycode
	  5			2044
	  14.5		301
	  0 or other 752
	  
	  Category
	  Purchare - R for all vat %

	  	1141213551
	  */
	  	  declare @GstDate date ='2017-06-30'
	  	Select isnull(Name,'-') [BuyerName] ,'-' BuyerTin,  isnull(Invoicedate , ReturnDate) [Invoicedate],  isnull(InvoiceNo, '') [InvoiceNo],
					 CASE Isnull(vat, 0) 
               WHEN 5 THEN '2044' 
               WHEN 14.5 THEN '301' 
               ELSE '752' 
             END						                      [Commoditycode],
			Sum(isnull(RInvoiceAmount,0) -  isnull(RDiscountVal,0)) - ( round((Sum(isnull(RInvoiceAmount,0) -  isnull(RDiscountVal,0)) /(100+ Isnull(Vat, 0))*100 )   * 
                   Isnull(Vat, 0) / 
                   100, 2) ) [ReturnValue],
			Isnull(vat, 0) VAT,
			      round((Sum(isnull(RInvoiceAmount,0) -  isnull(RDiscountVal,0)) /(100+ Isnull(Vat, 0))*100 )   * 
                   Isnull(Vat, 0) / 
                   100, 2)                                [RVatValue], ReturnDate [ReturnDate], ReturnNo [ReturnNo],
				   Sum(isnull(InvoiceAmount,0) -  isnull(DiscountVal,0)) - ( round((Sum(isnull(InvoiceAmount,0) -  isnull(DiscountVal,0)) /(100+ Isnull(Vat, 0))*100 )   * 
                   Isnull(Vat, 0) / 
                   100, 2) ) [SalesValue],
			
			      round((Sum(isnull(InvoiceAmount,0) -  isnull(DiscountVal,0)) /(100+ Isnull(Vat, 0))*100 )   * 
                   Isnull(Vat, 0) / 
                   100, 2)                                [VatValue], 

				      Category from (
				 
					 
			 
					select SalesReturn.ReturnDate [ReturnDate] , ltrim(Isnull(SalesReturn.prefix,'')+' '+SalesReturn.ReturnNo) [ReturnNo], sales.InvoiceDate AS InvoiceDate,  Sales.InvoiceNo as InvoiceNo,0 as  salesDiscount,
					isnull(Sales.Name,isnull(cust.name,''))Name ,SalesReturnItem.quantity,
		 
					SalesReturnItem.Discount [SalesReturnItem.Discount],
					 CancelStatus [CancelStatus],IsDeleted [IsDeleted],
					ps.VAT, p.Name as ProductName,  'R' Category ,
convert(decimal(18,2),(CASE isnull( SalesReturnItem.MrpSellingPrice,0) WHEN    0 then ps.SellingPrice  else SalesReturnItem.MrpSellingPrice   END) * SalesReturnItem.quantity) As RInvoiceAmount
                    , Sales.Discount as Discount 
                    ,(SalesReturnItem.Quantity *
					(CASE isnull( SalesReturnItem.MrpSellingPrice,0) WHEN    0 then ps.SellingPrice  else SalesReturnItem.MrpSellingPrice   END) * isnull(SalesReturnItem.Discount,0) / 100) + 
					(isnull(SalesReturnItem.MrpSellingPrice  ,ps.SellingPrice )* SalesReturnItem.quantity * isnull(sales.Discount,0) /100) as RDiscountVal
                    ,convert(decimal(18,2), isnull( sales.InvoiceAmount,0)) As InvoiceAmount
				  ,isnull( sales.DiscountVal,0)  as DiscountVal

					from SalesReturn SalesReturn 
                    join SalesReturnItem SalesReturnItem on SalesReturn.id= SalesReturnItem.SalesReturnId
					Left join Patient cust on SalesReturn.PatientId = cust.id 
                    join productstock ps on ps.id=SalesReturnItem.productstockid
                    inner join Product p on p.Id=ps.ProductId
					  Left join (Select sales.id,max( Sales.Discount) [Discount], max(isnull(Sales.Cancelstatus,0)) [CancelStatus], max(isnull(Sales.Name,'')) [Name], Max(isnull(sales.Prefix ,'')+isnull(sales.InvoiceSeries ,'') + Sales.InvoiceNo) [InvoiceNo], max(sales.invoicedate) [invoicedate],
					 sum((  isnull( salesitem.SellingPrice,ps.SellingPrice)  ) * salesitem.quantity) As InvoiceAmount
				  ,sum( (salesitem.Quantity *
					(CASE isnull( salesitem.SellingPrice,0) WHEN    0 then ps.SellingPrice  else salesitem.SellingPrice   END) * isnull(salesitem.Discount,0) / 100) +
					(isnull(salesitem.SellingPrice  ,ps.SellingPrice )* salesitem.quantity * isnull(sales.Discount,0) /100) ) as DiscountVal  from Sales 
					 inner join salesitem salesitem on sales.id= salesitem.salesid
					 join productstock ps on ps.id=salesitem.productstockid where 	--isnull(sales.cancelstatus ,0) =0 
					  
					  sales.InstanceId = @InstanceId AND sales.AccountId =@AccountId  --and isnull(ps.VAT,0)>0 
					  group by  sales.id) Sales on sales.id = salesreturn.salesid 
                    WHERE SalesReturn.InstanceId = @InstanceId AND SalesReturn.AccountId =@AccountId and isnull(ps.VAT,0)>0
					AND Convert(date,SalesReturn.CreatedAt) BETWEEN @StartDate AND @EndDate
				 AND isnull(sales.cancelstatus ,0) =0 
				 and SalesReturnItem.quantity>0
				 and isnull(SalesReturn.TaxRefNo,0) =0
				  and CONVERT(DATE, SalesReturn.createdat) <=@GstDate
                   ) as one   where isnull(CancelStatus,0) = 0 and(IsDeleted is null or IsDeleted != 1) group by [Name] ,  Category, Invoicedate, InvoiceNo,Isnull(vat, 0),ReturnNo,ReturnDate
                    ORDER BY   vat,Invoiceno   
					 
					END 