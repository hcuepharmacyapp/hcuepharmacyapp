﻿/*******************************************************************************                            
** Change History                              
*******************************************************************************                            
** Date        Author          Description                              
*******************************************************************************        
**13/7/18/10/17   bIKAS		NEWLY CREATED FOR CUSTOMERRECEIPT
*******************************************************************************/
CREATE PROCEDURE dbo.usp_GetCustomerReceiptItems(@InstanceId NVARCHAR(36), @AccountId NVARCHAR(36), @PatientId NVARCHAR(36))
AS
BEGIN	
	SELECT 
	s.Id,
	s.InvoiceNo,
	isnull(s.prefix,'') + s.InvoiceSeries [InvoiceSeries],
	s.InvoiceDate,		  	
	s.SaleAmount AS InvoiceValue,		                       
	cp.PaymentType,
	cp.CreatedAt,
	cp.Remarks,
	cp.Debit,   
	cp.Credit,
	cp.CustomerId,
	P.Status
	from               
	SalesItem as si WITH(NOLOCK) inner join Sales as s WITH(NOLOCK) on si.SalesId = s.Id
	inner join ProductStock as ps WITH(NOLOCK) on si.ProductStockId  = ps.Id
	inner join CustomerPayment as cp WITH(NOLOCK) on  cp.SalesId = s.Id
	left join Patient p WITH(NOLOCK) on p.Id = @PatientId
	where s.AccountId = @AccountId and s.InstanceId = @InstanceId  and s.PatientId = @PatientId and s.cancelstatus IS NULL
	group by  s.Id,s.InvoiceNo, isnull(s.prefix,'') +s.InvoiceSeries,s.InvoiceDate, s.SaleAmount, cp.customerID,cp.PaymentType,cp.CreatedAt,cp.Remarks,cp.Credit,cp.Debit,P.Status
	Union Select null as id, null as invoiceNo, 'OB' as invoiceSeries, (select Max(Transactiondate) from CustomerPayment WITH(NOLOCK) where customerId = @PatientId and credit > 0),  
	(select credit from CustomerPayment WITH(NOLOCK) where customerId = @PatientId and credit > 0), cs.paymentType, cs.createdat,  cs.Remarks,
	cs.Debit, cs.Credit, cs.customerId , '' Status from CustomerPayment cs WITH(NOLOCK) where cs.AccountId = @AccountId and cs.InstanceId = @InstanceId 
	and cs.CustomerId = @PatientId
    
END


