/*                             
******************************************************************************                            
** File: [usp_TallypurchaseReturnReport] 
** Name: [TallypurchaseReturnReport]                             
** Description: To Get Purchase  details 
** 
** This template can be customized:                             
**                              
** Called by:                              
**                              
**  Parameters:                             
**  Input                Output                             
**  ----------              -----------                             
** 
** Author: 
** Created Date:  
** 
*******************************************************************************                            
** Change History                             
*******************************************************************************                            
** Date        Author          Description                             
*******************************************************************************       
** 04/07/2018 Bikas		Columns added and some columns removed.   
*******************************************************************************/ 
Create PROCEDURE [dbo].[TallypurchaseReturnReport](@InstanceId varchar(36),@AccountId varchar(36),@StartDate datetime,@EndDate datetime )
 AS
 BEGIN
 if (@AccountId = '6137e806-7a11-43ab-9f33-23ad4e3d71a6')
BEGIN
 select vr.ReturnDate,(isnull(vr.Prefix,'') + vr.ReturnNo) ReturnNo,vendor.Name,product.Name AS ProductName,vendor.GsTinNo,productstock.HsnCode,vendor.PaymentType,vri.Cgst ,vri.Sgst,vri.Igst,vri.ReturnPurchasePrice,vri.Quantity,vri.GstAmount/2 AS CGSTAmount,(vri.GstAmount/2) AS SGSTAmount,vpi.GstValue GstAmount,vpi.GstTotal,vri.DiscountValue,vr.RoundOffValue AS RoundOff, isnull(vri.quantity,0) * isnull(vri.ReturnPurchasePrice,vpi.PurchasePrice) [ReturnTotal],Product.Code,'1001' As purchasereturnCode,'22' As StateId,Case  WHEN vendor.VendorType = 1 THEN 'R'
When vendor.VendorType = 2 THEN 'UR'
When vendor.VendorType = 3 THEN 'C'
END as VendorTypeDesc
from VendorReturn vr with(nolock) inner join
VendorReturnItem vri with(nolock) on vr.Id=vri.VendorReturnId
  LEFT JOIN vendorpurchase vp WITH(NOLOCK) 
                     ON vp.id = vr.vendorpurchaseid 
 INNER JOIN vendor (nolock) 
				  ON vendor.id = vr.vendorid 
 INNER JOIN productstock (nolock) 
				  ON productstock.id = vri.productstockid 
				   Left JOIN product (nolock) 
				  ON product.id = productstock.productid 
 LEFT JOIN vendorpurchaseitem vpi WITH(NOLOCK)
                  ON vpi.productstockid = productstock.id 
				   and vpi.VendorPurchaseId = vr.VendorPurchaseId
where vr.AccountId=@AccountId and vr.InstanceId=@InstanceId and vr.CreatedAt between @StartDate and @EndDate 
order by vr.ReturnDate ,vr.CreatedAt desc
END 
else
BEGIN
select vr.ReturnDate,(isnull(vr.Prefix,'') + vr.ReturnNo) ReturnNo,vendor.Name,product.Name AS ProductName,vendor.GsTinNo,productstock.HsnCode,vendor.PaymentType,vri.Cgst ,vri.Sgst,vri.Igst,vri.ReturnPurchasePrice,vri.Quantity,vri.GstAmount/2 AS CGSTAmount,(vri.GstAmount/2) AS SGSTAmount,vpi.GstValue GstAmount,vpi.GstTotal,vri.DiscountValue,vr.RoundOffValue AS RoundOff, isnull(vri.quantity,0) * isnull(vri.ReturnPurchasePrice,vpi.PurchasePrice) [ReturnTotal],Product.Code,'' As purchasereturnCode,vendor.StateId,Case  WHEN vendor.VendorType = 1 THEN 'R'
When vendor.VendorType = 2 THEN 'UR'
When vendor.VendorType = 3 THEN 'C'
END as VendorTypeDesc
from VendorReturn vr with(nolock) inner join
VendorReturnItem vri with(nolock) on vr.Id=vri.VendorReturnId
  LEFT JOIN vendorpurchase vp WITH(NOLOCK) 
                     ON vp.id = vr.vendorpurchaseid 
 INNER JOIN vendor (nolock) 
				  ON vendor.id = vr.vendorid 
 INNER JOIN productstock (nolock) 
				  ON productstock.id = vri.productstockid 
				   Left JOIN product (nolock) 
				  ON product.id = productstock.productid 
 LEFT JOIN vendorpurchaseitem vpi WITH(NOLOCK)
                  ON vpi.productstockid = productstock.id 
				   and vpi.VendorPurchaseId = vr.VendorPurchaseId
where vr.AccountId=@AccountId and vr.InstanceId=@InstanceId and vr.CreatedAt between @StartDate and @EndDate 
order by vr.ReturnDate ,vr.CreatedAt desc
END
 END

