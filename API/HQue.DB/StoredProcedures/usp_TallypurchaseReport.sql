
/*                             
******************************************************************************                            
** File: [usp_TallypurchaseReport] 
** Name: [TallypurchaseReport]                             
** Description: To Get Sales   details 
** 
** This template can be customized:                             
**                              
** Called by:                              
**                              
**  Parameters:                             
**  Input                Output                             
**  ----------              -----------                             
** 
** Author: 
** Created Date:  
** 
*******************************************************************************                            
** Change History                             
*******************************************************************************                            
** Date        Author          Description                             
*******************************************************************************       
** 04/07/2018 Bikas		Columns added and some columns removed.   
*******************************************************************************/ 
Create PROCEDURE [dbo].[TallypurchaseReport](@InstanceId varchar(36),@AccountId varchar(36),@StartDate datetime,@EndDate datetime )
 AS
BEGIN
 if (@AccountId = '6137e806-7a11-43ab-9f33-23ad4e3d71a6')
 BEGIN
 select vp.InvoiceDate,(isnull(vp.Prefix,'') + vp.InvoiceNo) InvoiceNo,vendor.PaymentType,vendor.Name,product.Name AS ProductName,vendor.GsTinNo,productstock.HsnCode,vpi.Cgst ,vpi.Sgst,vpi.Igst,vpi.PackagePurchasePrice,vpi.Quantity,vpi.GstValue/2 AS CGSTAmount,(vpi.GstValue/2) AS SGSTAmount,vpi.GstValue GstAmount,vpi.GstTotal,vpi.DiscountValue,vp.RoundOffValue AS RoundOff,vp.NetValue,'22' As StateId,vendor.VendorType ,Product.Code,'1001' As purchaseCode,Case  WHEN vendor.VendorType = 1 THEN 'R'
When vendor.VendorType = 2 THEN 'UR'
When vendor.VendorType = 3 THEN 'C'
END as VendorTypeDesc
from VendorPurchase vp with(nolock) inner join
VendorPurchaseItem vpi with(nolock) on vp.Id=vpi.VendorPurchaseId
 INNER JOIN vendor (nolock) 
				  ON vendor.id = vp.vendorid 
 INNER JOIN productstock (nolock) 
				  ON productstock.id = vpi.productstockid 
				   Left JOIN product (nolock) 
				  ON product.id = productstock.productid 
where vp.AccountId=@AccountId and vp.InstanceId=@InstanceId and vp.CreatedAt between @StartDate and @EndDate and isnull(vp.Cancelstatus,0)= 0 
order by vp.InvoiceDate ,vp.CreatedAt desc
 END
 else 
 BEGIN
  select vp.InvoiceDate,(isnull(vp.Prefix,'') + vp.InvoiceNo) InvoiceNo,vendor.PaymentType,vendor.Name,product.Name AS ProductName,vendor.GsTinNo,productstock.HsnCode,vpi.Cgst ,vpi.Sgst,vpi.Igst,vpi.PackagePurchasePrice,vpi.Quantity,vpi.GstValue/2 AS CGSTAmount,(vpi.GstValue/2) AS SGSTAmount,vpi.GstValue GstAmount,vpi.GstTotal,vpi.DiscountValue,vp.RoundOffValue AS RoundOff,vp.NetValue,vendor.StateId,vendor.VendorType ,Product.Code,'' As purchaseCode,Case  WHEN vendor.VendorType = 1 THEN 'R'
When vendor.VendorType = 2 THEN 'UR'
When vendor.VendorType = 3 THEN 'C'
END as VendorTypeDesc
from VendorPurchase vp with(nolock) inner join
VendorPurchaseItem vpi with(nolock) on vp.Id=vpi.VendorPurchaseId
 INNER JOIN vendor (nolock) 
				  ON vendor.id = vp.vendorid 
 INNER JOIN productstock (nolock) 
				  ON productstock.id = vpi.productstockid 
				   Left JOIN product (nolock) 
				  ON product.id = productstock.productid 
where vp.AccountId=@AccountId and vp.InstanceId=@InstanceId and vp.CreatedAt between @StartDate and @EndDate and isnull(vp.Cancelstatus,0)= 0 
order by vp.InvoiceDate ,vp.CreatedAt desc
 END
 END