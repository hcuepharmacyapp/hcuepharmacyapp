  /*******************************************************************************                            
** Change History                               
*******************************************************************************                            
** Date        Author          Description                               
*******************************************************************************         
 **09/05/17     Poongodi R		Query optimized (address the Performance Issue)
 **24/05/17     Mani			Low Stock Formula modified
*******************************************************************************/ 
 Create PROCEDURE [dbo].[usp_GetDashboardTopLowStock](@AccountId VARCHAR(36), @InstanceId VARCHAR(36) = '')
 AS
 BEGIN
   SET NOCOUNT ON

   IF @InstanceId != ''
   BEGIN
		
select top 10 p.id, p.Name, CEILING(sum(osi.Quantity)/30) AvgSold, sum(isNull(ps.Stock, 0)) as Stock from productstock ps 
Join Product P on P.id = ps.productId And ps.AccountId = P.AccountId
inner join  
(
select ps.productId, sum(si.Quantity) Quantity FROM salesitem (nolock) si 
Join productStock ps On ps.id = si.ProductStockId
WHERE Convert(date, si.CreatedAt) between dateadd(day,datediff(day,0,GETDATE()),-30) and dateadd(day,datediff(day,0,GETDATE()),0)
	AND si.AccountId = @AccountId AND si.InstanceId  =  @InstanceId 
	Group By ps.productId
) osi on p.id = osi.productId
where ps.AccountId = @AccountId AND ps.InstanceId  =  @InstanceId
Group By p.id, p.Name
having  sum(isnull( ps.Stock ,0))  < CEILING(sum(osi.Quantity)/30) And sum(isnull( ps.Stock ,0)) > 0
ORDER BY sum(isNull(ps.Stock, 0)) DESC

	END
	ELSE
	BEGIN
		
select top 10 p.id, p.Name, CEILING(sum(osi.Quantity)/30) AvgSold, sum(isNull(ps.Stock, 0)) as Stock from productstock ps 
Join Product P on P.id = ps.productId And ps.AccountId = P.AccountId
inner join  
(
select ps.productId, sum(si.Quantity) Quantity FROM salesitem (nolock) si 
Join productStock ps On ps.id = si.ProductStockId
WHERE Convert(date, si.CreatedAt) between dateadd(day,datediff(day,0,GETDATE()),-30) and dateadd(day,datediff(day,0,GETDATE()),0)
	AND si.AccountId = @AccountId 
	Group By ps.productId
) osi on p.id = osi.productId
Group By p.id, p.Name
having  sum(isnull( ps.Stock ,0))  < CEILING(sum(osi.Quantity)/30) And sum(isnull( ps.Stock ,0)) > 0
ORDER BY sum(isNull(ps.Stock, 0)) DESC

	END
END
 