-- usp_GetPOMarginList null, 'c503ca6e-f418-4807-a847-b6886378cf0b', '01-Sep-2017', '10-Sep-2017', 0 
-- usp_GetPOMarginList '3e2ec066-1551-4527-be53-5aa3c5b7fb7d', 'c503ca6e-f418-4807-a847-b6886378cf0b', '01-Sep-2017', '10-Sep-2017', 0 
/*                              
******************************************************************************                            
** File: [usp_GetPOMarginList]  
** Name: [usp_GetPOMarginList]                              
** Description: To Purchase Order Details 
**  
** This template can be customized:                              
**                               
** Called by:                               
**                               
**  Parameters:                              
**  Input                Output                              
**  ----------              -----------                              
**  
** Author: Poongodi R   
** Created Date:  20/02/2017 
**  
*******************************************************************************                            
** Change History                              
*******************************************************************************                            
** Date        Author          Description                              
*******************************************************************************        
  **08/03/2017  Poongodi     Po cancel status validation added 
 **04/05/2017  Poongodi      Date filter changed from Invoice date to Created date
  **05/07/2017  Sarubala     GST calculation added 
  **23/08/2017  Lawrence	 Added for 'Invoice Date Wise' filter added
  **11/09/2017  Poongodi	Divideby zero error fixed
*******************************************************************************/ 
--Usage : --usp_GetPOMarginList  @StartDate='01-Jan-2017',@EndDate=N'06-Feb-2017',@AccountId=N'18204879-99ff-4efd-b076-f85b4a0da0a3',@InstanceId=N'013513b1-ea8c-4ea8-9fed-054b260ee197'
CREATE PROCEDURE [dbo].[Usp_getpomarginlist](@InstanceId VARCHAR(36), 
                                             @AccountId  VARCHAR(36), 
                                             @StartDate  DATETIME, 
                                             @EndDate    DATETIME,
											 @IsInvoiceDate BIT) 
AS 
  BEGIN 
      SET nocount ON 
	  	Declare @InvStartDt date,
			@InvEndDt date,
			@GRNStartDt date,
			@GRNEndDt date

if (@IsInvoiceDate =1)
	begin
	Select @InvStartDt = @StartDate,@InvEndDt =@EndDate, @GRNStartDt = NULL, @GRNEndDt = NULL
	end
else
	begin
	Select @InvStartDt = NULL,@InvEndDt =NULL, @GRNStartDt = @StartDate, @GRNEndDt = @EndDate
	end

      SELECT vendorpurchaseitem.productstockid, 
			 CAST(isnull(VendorPurchase.TaxRefNo,0) as int) TaxRefNo,
             productstock.productid, 
             vendorpurchaseitem.quantity, 
             vendorpurchaseitem.purchaseprice, 
             vendorpurchaseitem.packagepurchaseprice 
             [PackagePurchasePrice], 
             vendorpurchaseitem.packageqty, 
             vendorpurchaseitem.packagesellingprice 
             [PackageSellingPrice], 
             Isnull(vendorpurchaseitem.packageqty, 0) - Isnull( 
             vendorpurchaseitem.freeqty, 0) 
                          [ActualQty], 
             /*  Round( (Isnull(vendorpurchaseitem.packagepurchaseprice, 0) * (  
                    Isnull  
                    (  
                    vendorpurchaseitem.packageqty, 0)  
                    -  
                                         Isnull  
                    (  
                    vendorpurchaseitem.freeqty, 0) )  *  
                  (Isnull(productstock.vat, 0) /  
                  100 ))  ,2)                 */ 
            
			/* Round(( Isnull(pocal.poval, 0) ) * (case when VendorPurchase.TaxRefNo = 1 then ISNULL
			  (VendorPurchaseItem.GstTotal,isnull(ProductStock.GstTotal,0)) else Isnull(productstock.VAT, 0)	end) / 100, 2) [VatValue]  
			 (ProductStock.GstTotal,isnull(VendorPurchaseItem.GstTotal,0)) else Isnull(productstock.VAT, 0)	end) / 100, 2) [VatValue], */

			 (case when VendorPurchase.TaxRefNo = 1 then ISNULL(VendorPurchaseItem.GstValue,0)				
			else Isnull(VendorPurchaseItem.VatValue, 0) end) [VatValue],   

             /* Round(Isnull(PoCal.totalpo, 0), 2) 
             [ReportTotal], 
			 */
			 vendorpurchaseitem.PurchasePrice * vendorpurchaseitem.Quantity as [ReportTotal],
             vendorpurchaseitem.freeqty [FreeQty], 
             vendorpurchaseitem.FreeQtyTaxValue [FreeQtyTaxValue],
			 vendorpurchaseitem.discount [Discount] , 
			 vendorpurchaseitem.SchemeDiscountPerc [SchDiscount], 
			 vendorpurchaseitem.MarkupPerc [MarkupDiscount], 
			 vendorpurchaseitem.FreeQtyTaxValue [FreeQtyTaxValue],
             ((Isnull(vendorpurchaseitem.packageqty, 0) - Isnull(vendorpurchaseitem.freeqty, 0) ) * Isnull(vendorpurchaseitem.packagesellingprice, 0)) [MRPValue], 
			 /* --Round(Isnull(PoCal.mrpvalue, 0), 2) [MRPValue], 

             --Round(Isnull(PoCal.mrpvalue, 0) - Isnull(PoCal.totalpo, 0), 2) [MarginAmount], 
             --Round(Isnull(PoCal.mrpvalue, 0) 
			 */
			 Round(Isnull(((Isnull(vendorpurchaseitem.packageqty, 0) - Isnull(vendorpurchaseitem.freeqty, 0) ) * Isnull(vendorpurchaseitem.packagesellingprice, 0)), 0) 
			 - Isnull(vendorpurchaseitem.PurchasePrice * vendorpurchaseitem.Quantity, 0), 2) [MarginAmount], 
             /*Round(( Isnull(PoCal.mrpvalue, 0) / CASE Isnull(PoCal.totalpo, 0) 
                                                   WHEN 0 THEN 1 
                                                   ELSE Isnull(PoCal.totalpo, 0) 
                                                 END * 100 ) - 100, 2) [MarginPer], 
			--(((Isnull(PoCal.mrpvalue, 0) - Round(Isnull(PoCal.totalpo, 0), 2)) / case Isnull(PoCal.mrpvalue, 0)  when 0 then 1 else  Isnull(PoCal.mrpvalue, 1) end ) * 100) [MarginPer],
			*/
			(((Isnull(( Isnull(vendorpurchaseitem.packageqty, 0) - 
             Isnull(vendorpurchaseitem.freeqty, 0) ) * Isnull(vendorpurchaseitem.packagesellingprice, 0), 0) 
			 - Round(Isnull(vendorpurchaseitem.PurchasePrice * vendorpurchaseitem.Quantity, 0), 2)) 
			 / case Isnull(( Isnull(vendorpurchaseitem.packageqty, 0)
			 - Isnull(vendorpurchaseitem.freeqty, 0) ) * Isnull(vendorpurchaseitem.packagesellingprice, 0), 0)
			 when 0 then 1 
			 else  Isnull((Isnull(vendorpurchaseitem.packageqty, 0) - Isnull(vendorpurchaseitem.freeqty, 0))
			 * 
			 Isnull(vendorpurchaseitem.packagesellingprice, 0), 1) end ) * 100) [MarginPer],
              vendorpurchase.id                                              AS 
             [VendorPurchaseId], 
             vendorpurchase.invoiceno                                       AS 
             [VendorPONo], 
             vendorpurchase.invoicedate                                     AS 
             [InvoiceDate], 
             vendorpurchase.discount                                        AS 
             [PODiscount], 
			 VendorPurchase.TaxRefNo										AS
			 [TaxRefNo],
             vendor.id                                                      AS 
             [Vendor.Id], 
             vendor.NAME                                                    AS 
             [VendorName], 
             vendor.mobile                                                  AS 
             [VendorMobile], 
             vendor.email                                                   AS 
             [VendorEmail], 
             vendor.address                                                 AS 
             [VendorAddress], 
             vendor.tinno                                                   AS 
             [VendorTinNo], 
             productstock.sellingprice                                      AS 
             [ProductStockSellingPrice], 
             productstock.vat                                               AS 
             [VAT], 
             productstock.cst                                               AS 
             [CST], 
			/* ISNULL(VendorPurchaseItem.GstTotal,isnull(ProductStock.GstTotal,0))      AS 
             [GSTTotal], 
			 ISNULL(VendorPurchaseItem.Cgst,isnull(ProductStock.Cgst,0))             AS 
             [Cgst], 
			 ISNULL(VendorPurchaseItem.Sgst,isnull(ProductStock.Sgst,0))             AS 
             [Sgst], 
			 ISNULL(VendorPurchaseItem.Igst,isnull(ProductStock.Igst,0))			 AS 
             [Igst], */
			 ISNULL(ProductStock.GstTotal,isnull(VendorPurchaseItem.GstTotal,0))      AS 
             [GSTTotal], 
			 ISNULL(ProductStock.Cgst,isnull(VendorPurchaseItem.Cgst,0))             AS 
             [Cgst], 
			 ISNULL(ProductStock.Sgst,isnull(VendorPurchaseItem.Sgst,0))             AS 
             [Sgst], 
			 ISNULL(ProductStock.Igst,isnull(VendorPurchaseItem.Igst,0))			 AS 
             [Igst], 
             product.NAME                                                   AS [ProductName] 
			 , Inst.Name													AS InstanceName 
      FROM   vendorpurchaseitem 
             INNER JOIN vendorpurchase 
                     ON vendorpurchase.id = vendorpurchaseitem.vendorpurchaseid 
             INNER JOIN vendor 
                     ON vendor.id = vendorpurchase.vendorid 
             INNER JOIN productstock 
                     ON productstock.id = vendorpurchaseitem.productstockid 
             INNER JOIN product 
                     ON product.id = productstock.productid 
             /*
			 CROSS apply (SELECT ( ( 
                                   Isnull( 
                         vendorpurchaseitem.packageqty, 0) 
                                   - 
                                             Isnull( 
                                 vendorpurchaseitem.freeqty, 0 
                                                                        ) 
                                                                        ) * 
                                 vendorpurchaseitem.packagepurchaseprice ) - 
                                                            ( 
                                 ( ( 
                                 Isnull( 
                                 vendorpurchaseitem.packageqty, 0) 
                                 - 
                                                      Isnull( 
                                 vendorpurchaseitem.freeqty, 0) ) * 
                                 vendorpurchaseitem.packagepurchaseprice ) * 
                                 Isnull 
                                 ( 
                                 vendorpurchaseitem.discount, 0 
                                 ) / 
                                 100 ) + ( ( ( ( Isnull( 
                                             vendorpurchaseitem.packageqty, 
                                                 0) - 
             Isnull(vendorpurchaseitem.freeqty, 0) ) * 
             vendorpurchaseitem.packagepurchaseprice ) - ( 
             ( ( 
             Isnull(vendorpurchaseitem.packageqty, 0) - 
                 Isnull(vendorpurchaseitem.freeqty, 0) 
             ) * 
               vendorpurchaseitem.packagepurchaseprice 
             ) * 
             Isnull( 
               vendorpurchaseitem.discount, 0) / 100 ) 
             ) * 
			 --  ( (case when VendorPurchase.TaxRefNo = 1 then  ISNULL(VendorPurchaseItem.GstTotal,isnull(ProductStock.GstTotal,0))		
			 			 ( (case when VendorPurchase.TaxRefNo = 1 then  ISNULL(ProductStock.GstTotal,isnull(VendorPurchaseItem.GstTotal,0))				
			 else Isnull(productstock.vat, 0) end) / 100 ) ) TotalPo,
			  
             ( Isnull(vendorpurchaseitem.packageqty, 0) - 
             Isnull(vendorpurchaseitem.freeqty, 0) ) * Isnull( 
             vendorpurchaseitem.packagesellingprice, 0) [MrpValue], 

             Isnull(vendorpurchaseitem.packagepurchaseprice, 0) * 
             ( Isnull (vendorpurchaseitem.packageqty, 0) - 
             Isnull ( 
             vendorpurchaseitem.freeqty, 0) ) - ( Isnull( 
             vendorpurchaseitem.packagepurchaseprice, 0) * 
             ( 
             Isnull (vendorpurchaseitem.packageqty, 0) 
             - 
                                    Isnull ( 
             vendorpurchaseitem.freeqty, 0) ) * Isnull ( 
             vendorpurchaseitem.discount, 
                  0) / 
                  100 )         [Poval]) AS PoCal 
				  */
			 INNER JOIN Instance Inst on Inst.AccountId = @AccountId
			 and (Inst.AccountId = @AccountId and Inst.Id = ISNULL(@InstanceId,vendorpurchase.InstanceId))
      		  WHERE  cast(vendorpurchase.CreatedAt as date) BETWEEN isnull(@GRNStartDt,cast(vendorpurchase.CreatedAt as date)) AND isnull(@GRNEndDt,cast(vendorpurchase.CreatedAt as date))
			 AND cast(vendorpurchase.InvoiceDate as date)  BETWEEN isnull(@InvStartDt,cast(vendorpurchase.InvoiceDate as date) ) AND isnull(@InvEndDt,cast(vendorpurchase.InvoiceDate as date))
             AND vendorpurchase.accountid = @AccountId 
             AND vendorpurchase.instanceid = isnull(@InstanceId ,vendorpurchase.InstanceId)
			 and isnull(VendorPurchase.CancelStatus ,0)= 0
			 and (vendorpurchaseitem.Status is null or vendorpurchaseitem.Status = 1)
      ORDER  BY vendorpurchase.createdat DESC 
  END