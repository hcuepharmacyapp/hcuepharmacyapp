create proc [dbo].[usp_Account_Update](@Accountid Varchar(36),
@IsBillNumberReset bit,
@UpdatedAt datetime,
@UpdatedBy Varchar(36) )
as
begin
update Account set IsBillNumberReset = @IsBillNumberReset , UpdatedAt = @UpdatedAt , UpdatedBy = @UpdatedBy 
where id =@Accountid

select * from FinYearMaster where AccountId=@Accountid

end



