﻿

CREATE PROCEDURE [dbo].[usp_GetTransferStockItems](@FromInstanceId varchar(36), @ToInstanceId varchar(36),@TransferDate datetime,
@TransferNo varchar(50),@TransferStatus varchar(50),@Filter varchar(36),@PageNo int, @PageSize int
)
 AS
 BEGIN


if (isnull(@Filter,'') ='') 
set @Filter = NULL

if (isnull(@TransferStatus,'') ='') 
set @TransferStatus = NULL

if (isNull(@TransferNo,'')= '')
set @TransferNo = Null

if (isNull(@FromInstanceId,'')= '')
set @FromInstanceId = Null

if (isNull(@ToInstanceId,'')= '')
set @ToInstanceId = Null

if (isNull(@TransferDate,'')= '')
set @TransferDate = Null

SELECT StockTransfer.Id,StockTransfer.AccountId,StockTransfer.InstanceId,StockTransfer.FromInstanceId,StockTransfer.ToInstanceId,StockTransfer.TransferNo,StockTransfer.TransferDate,StockTransfer.FileName,StockTransfer.Comments,StockTransfer.TransferStatus,StockTransfer.OfflineStatus,StockTransfer.CreatedAt,StockTransfer.UpdatedAt,StockTransfer.CreatedBy,StockTransfer.UpdatedBy,Instance.Id AS [Instance.Id],Instance.AccountId AS [Instance.AccountId],Instance.Name AS [InstanceName],Instance.Mobile AS [InstanceMobile]
,ToInstance.Id AS [ToInstance.Id],ToInstance.AccountId AS
[ToInstance.AccountId],ToInstance.Name AS [ToInstanceName],ToInstance.RegistrationDate AS 
[ToInstance.RegistrationDate],
HQueUser.Id AS [HQueUser.Id],HQueUser.AccountId AS [HQueUser.AccountId],HQueUser.InstanceId AS [HQueUser.InstanceId],HQueUser.UserId AS [HQueUser.UserId],HQueUser.Password AS [HQueUser.Password],HQueUser.UserType AS [HQueUser.UserType],HQueUser.Status AS [HQueUser.Status],HQueUser.Name AS [HQueUserName]
FROM StockTransfer 
INNER JOIN Instance ON Instance.Id = StockTransfer.FromInstanceId 
INNER JOIN Instance AS ToInstance ON ToInstance.Id = StockTransfer.ToInstanceId 
INNER JOIN HQueUser ON HQueUser.Id = StockTransfer.CreatedBy 
 WHERE isnull(StockTransfer.TransferNo,'')  = isnull(@TransferNo, StockTransfer.TransferNo)   
 AND (isnull(StockTransfer.ToInstanceId,'') = isNull(@Filter,StockTransfer.ToInstanceId) 
 OR isnull(StockTransfer.FromInstanceId,'') = isNull(@Filter,StockTransfer.FromInstanceId) )
 AND isnull(cast(StockTransfer.TransferDate as date),'') = isNull(cast(@TransferDate as date),StockTransfer.TransferDate)
 
 ORDER BY StockTransfer.CreatedAt desc OFFSET @PageNo ROWS FETCH NEXT @PageSize ROWS ONLY
 
 END
