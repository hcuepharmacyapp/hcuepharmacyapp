 

/*                               
******************************************************************************                            
** File: [usp_GetPurchaseHistoryReturnList]   
** Name: [usp_GetPurchaseHistoryReturnList]                               
** Description: To Get Purchase Audit details  
**   
** This template can be customized:                               
**                                
** Called by:                                
**                                
**  Parameters:                               
**  Input                Output                               
**  ----------              -----------                               
**   
** Author:Gavaskar S   
** Created Date: 28/02/2017   
**   
*******************************************************************************                            
** Change History                               
*******************************************************************************                            
** Date        Author          Description                               
*******************************************************************************         
 **15/06/2007	Poongodi R		Prefix added
*******************************************************************************/

 -- exec usp_GetPurchaseHistoryReturnList '8adafeb8-ebed-454b-90c7-2c65a7a94f06'
 CREATE PROCEDURE [dbo].[usp_GetPurchaseHistoryReturnList](@VendorPurchaseId varchar(max), @Product varchar(50))
 AS
 BEGIN
   SET NOCOUNT ON

SELECT VendorReturn.Id,isnull(VendorReturn.Prefix,'')+ VendorReturn.ReturnNo [ReturnNo],VendorReturn.ReturnDate,VendorReturn.Reason,VendorReturn.CreatedAt,VendorPurchase.Id AS [VendorPurchaseId],
VendorPurchase.VendorId ,VendorPurchase.InvoiceNo ,VendorPurchase.GoodsRcvNo,HQueUser.Name AS [CreatedBy] 
FROM VendorReturn INNER JOIN VendorPurchase  ON VendorPurchase.Id = VendorReturn.VendorPurchaseId 
INNER JOIN HQueUser ON HQueUser.Id = VendorReturn.CreatedBy  
WHERE (VendorReturn.IsAlongWithPurchase is null or VendorReturn.IsAlongWithPurchase=0)  and VendorReturn.VendorPurchaseId  in (select a.id from dbo.udf_Split(@VendorPurchaseId ) a) ORDER BY 1
      
 END
           
