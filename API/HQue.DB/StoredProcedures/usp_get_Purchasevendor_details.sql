/*                               
******************************************************************************                            
 --[usp_get_Purchasevendor_details] 
**   
*******************************************************************************                            
** Change History                               
*******************************************************************************                            
** Date        Author          Description                               
*******************************************************************************    
 25-09-2017     Nandhini        Vendor details
*******************************************************************************/ 
CREATE PROC [dbo].[usp_get_Purchasevendor_details] ( 
@AccountId  NVARCHAR(36), 
@InstanceId NVARCHAR(36), 
@Name       VARCHAR(50) 

 ) 
 AS
 BEGIN
 SET NOCOUNT ON  
   if @Name = ''
  select @Name = null
		
		select vendor.Id,Vendor.name,vendor.Mobile,vendor.InstanceId,vendor.AccountId from Vendor(nolock)	
		  where 
		 vendor.AccountId=@AccountId 
   and Vendor.name like isnull(@Name,vendor.Name) +'%'
   group by vendor.Id,Vendor.name,vendor.Mobile,vendor.InstanceId,vendor.AccountId
	END