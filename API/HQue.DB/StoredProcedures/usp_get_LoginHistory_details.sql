/*                               
******************************************************************************                            
 --[usp_get_LoginHistory_details] 
**   
*******************************************************************************                            
** Change History                               
*******************************************************************************                            
** Date        Author          Description                               
*******************************************************************************    
 15-11-2017     Nandhini        LoginHistory
*******************************************************************************/ 
create PROC [dbo].[usp_get_LoginHistory_details] ( 
@AccountId  NVARCHAR(36), 
@InstanceId NVARCHAR(36),
@LoginUserId NVARCHAR(36),
@LoggedInDate datetime
 ) 
 AS
 BEGIN
 SET NOCOUNT ON  

  declare @Logoutdate datetime,
		 @UserMailid varchar(150) ,
		  @Userid varchar(150) ,
		  @BranchName varchar(150) ,
		  @ContactEmail varchar(150) ,
		    @UserName varchar(150) ,
			@date1 date,
			@logdate datetime,
			 @Useridnew varchar(150) ,
			@logtime datetime,
			@count int =0 

			select @date1 = (select cast(max(LoggedInDate) as date) from LoginHistory where cast(LoggedInDate as date) != cast(@LoggedInDate as date) and InstanceId=@InstanceId and AccountId=@AccountId)
			select @logdate = max(loggedindate) from LoginHistory where InstanceId=@InstanceId and AccountId=@AccountId
			select @BranchName = name from Instance where id = @InstanceId and AccountId=@AccountId
			select @UserName = name,@ContactEmail = Email from Account where id=@AccountId
			select @Useridnew=@LoginUserId


 
		select  @Logoutdate= LoggedOutDate,@UserMailid = LogOutUserId,@count  =1
		from 
		LoginHistory		
		 where 
		 LoginHistory.AccountId=@AccountId and 
		 LoginHistory.InstanceId=@InstanceId
		 and cast(LoginHistory.LoggedInDate as date) = cast(@date1 as date)
		 and isnull(IsSentsms,0) =1
		 if (isnull(@UserMailid,'')  ='' and isnull(@BranchName,'') <>'' and @count > 0 )
		 begin 
			 Select top 1 @Logoutdate= updatedat,@Userid = updatedby from productstock(nolock) where instanceid =@InstanceId  and cast(updatedat as date )  =@date1   order by updatedat desc  
			 select  @UserMailid = userid from hqueuser(nolock) where id =@Userid and accountid =@AccountId
		 end 
		select @Logoutdate LoggedOutDate, @UserMailid LogOutUserId ,ISNULL(@BranchName,'') BranchName,@ContactEmail ContactEmail,@UserName UserName, @logdate LoggedInDate, 'Logout: '+ isnull(@UserMailid,'-') + '&nbsp;&nbsp;&nbsp;&nbsp;  Login:  ' + @Useridnew    LogInUserId
	END