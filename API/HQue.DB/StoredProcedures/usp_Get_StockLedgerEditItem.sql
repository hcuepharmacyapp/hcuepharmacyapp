﻿CREATE Proc dbo.usp_Get_StockLedgerEditItem(@AccountId char(36),@InstanceId char(36), @ProductId varchar(max))
As
BEGIN


SELECT ProductStock.Id as stockid,ProductStock.BatchNo,ProductStock.ExpireDate,ProductStock.SellingPrice,ProductStock.VAT,ProductStock.GstTotal,
ProductStock.Stock,ProductStock.VendorId,ProductStock.MRP,Stock as ActualStock,Product.Name AS ProductName,ProductStock.productId FROM ProductStock  (nolock)
INNER JOIN Product (nolock) ON Product.Id = ProductStock.ProductId  WHERE  Product.Id  in (select a.id from dbo.udf_Split(@ProductId ) a)  AND ProductStock.AccountId  =  @AccountId AND 
ProductStock.InstanceId  =  @InstanceId
ORDER BY stock desc

END


