﻿/**   
*******************************************************************************                            
** Date        Author          Description                               
*******************************************************************************         
 ** 21/06/2017 Poongodi			Prefix added
 ** 13/10/2017 Poongodi			Validation changed from tin to GSTin number 
 ** 24/11/2018 Bikas			Adding Free Qty 
*******************************************************************************/ 
Create PROCEDURE [usp_vendorBasedOrder](@InstanceId varchar(36), @chkPO bit, @AccountId varchar(36), @VendorId varchar(36), @ToInstance varchar(36))
 AS
 BEGIN
 SET NOCOUNT ON  

 if(@chkPO = 0)
 BEGIN
 -- Below statement commented temporarily by settu on 25-05-2017 since stock update not handle in indent transfer
 --if((select count(id) from TransferSettings where AccountId=@AccountId and isnull(TransferZeroStock,0)=1)>0)
 if(@chkPO = 0)
 Begin
	select distinct vi.AccountId, vi.InstanceId, vi.VendorOrderId, vi.ProductId, vi.Quantity, isnull(vi.VAT,0) as VAT
	,isnull(vi.igst,0) as igst, isnull(vi.cgst,0) as cgst,isnull(vi.sgst,0) as sgst, isnull(vi.gstTotal,0) as gstTotal
	, vi.PackageSize, vi.PackageQty, vi.PurchasePrice, vi.CreatedAt, vi.ExpireDate, vi.SellingPrice,  pdt.Name , 
	isnull(vo.Prefix,'')+ vo.OrderId [OrderId], ins.GsTinNo TinNo, (select name from Instance

	 where id = vo.InstanceId)	 as BranchName, vi.id as VendorOrderItemId, isnull(trans.TransferQtyType,1) as TransferQtyType
	from VendorOrderItem as vi
	inner join VendorOrder as vo On vo.id = vi.VendorOrderId
	inner join Product as pdt On pdt.Id = vi.ProductId
	inner join Vendor as vn On vn.Id = vo.VendorId
	inner join Instance as ins On ins.GsTinNo = vn.GsTinNo
	left join   TransferSettings as trans On trans.AccountId = @AccountId And trans.InstanceId = @InstanceId
	--inner join Instance as ins On ins.Id = vi.InstanceId
	where vo.AccountId = @AccountId and isnull(IsDeleted,0) = 0 and vi.VendorPurchaseId is NULL and vi.StockTransferId is NULL 
	and vi.InstanceId = @ToInstance
	order by vi.CreatedAt desc
End
else
Begin
	select distinct vi.AccountId, vi.InstanceId, vi.VendorOrderId, vi.ProductId, vi.Quantity, isnull(vi.VAT,0) as VAT
	,isnull(vi.igst,0) as igst, isnull(vi.cgst,0) as cgst,isnull(vi.sgst,0) as sgst, isnull(vi.gstTotal,0) as gstTotal
	, vi.PackageSize, vi.PackageQty, vi.PurchasePrice, vi.CreatedAt, vi.ExpireDate, vi.SellingPrice,  pdt.Name , 
	isnull(vo.Prefix,'')+ vo.OrderId [OrderId], ins.GsTinNo TinNo, (select name from Instance

	 where id = vo.InstanceId)	 as BranchName, vi.id as VendorOrderItemId, isnull(trans.TransferQtyType,1) as TransferQtyType
	from VendorOrderItem as vi
	inner join VendorOrder as vo On vo.id = vi.VendorOrderId
	inner join Product as pdt On pdt.Id = vi.ProductId
	inner join Vendor as vn On vn.Id = vo.VendorId
	inner join Instance as ins On ins.GsTinNo = vn.GsTinNo
	left join   TransferSettings as trans On trans.AccountId = @AccountId And trans.InstanceId = @InstanceId
	--inner join Instance as ins On ins.Id = vi.InstanceId
	where vo.AccountId = @AccountId and isnull(IsDeleted,0) = 0 and vi.VendorPurchaseId is NULL and vi.StockTransferId is NULL 
	and vi.InstanceId = @ToInstance	
	and vi.ProductId in(select ProductId from ProductStock where AccountId=@AccountId
	and InstanceId=@InstanceId and isnull(stock,0)>0 group by ProductId)
	order by vi.CreatedAt desc
End
 END
 else
 BEGIN
		select distinct vi.AccountId, vi.InstanceId, vi.VendorOrderId, vi.ProductId, vi.Quantity, isnull(vi.VAT,0) as VAT
		,isnull(vi.igst,0) as igst, isnull(vi.cgst,0) as cgst,isnull(vi.sgst,0) as sgst, isnull(vi.gstTotal,0) as gstTotal
		, vi.PackageSize, vi.PackageQty,vi.FreeQty, vi.PurchasePrice, vi.CreatedAt, pdt.Name ,
	isnull(vo.Prefix,'')+ vo.OrderId [OrderId], ins.GsTinNo TinNo, vi.ExpireDate, vi.SellingPrice, vi.id as VendorOrderItemId 
	from VendorOrderItem as vi
	inner join VendorOrder as vo On vo.id = vi.VendorOrderId
	inner join Product as pdt On pdt.Id = vi.ProductId
	inner join Vendor as vn On vn.Id = vo.VendorId
	--inner join Instance as ins On ins.TinNo <> vn.TinNo  
	inner join Instance as ins On ins.Id = vi.InstanceId -- and ins.TinNo <> vn.TinNo  
	where vo.VendorId = @VendorId and vo.InstanceId = @InstanceId and vo.AccountId = @AccountId and isnull(IsDeleted,0) = 0 and vi.VendorPurchaseId is NULL and vi.StockTransferId is NULL
	order by vi.CreatedAt desc

 END
 

END