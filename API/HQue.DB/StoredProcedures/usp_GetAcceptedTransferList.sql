﻿/*******************************************************************************                            
** Change History                               
*******************************************************************************                            
** Date        Author          Description                               
*******************************************************************************         
** 16/11/2017	Settu	   Created
*******************************************************************************/
CREATE PROCEDURE [dbo].[usp_GetAcceptedTransferList](@AccountId CHAR(36), @InstanceId CHAR(36), @TransferNo VARCHAR(15))
AS
BEGIN
	SET NOCOUNT ON

	SELECT ProductStock.Id,
	ProductStock.BatchNo,  
	ProductStock.ExpireDate , 
	ProductStock.SellingPrice,
	ISNULL(ProductStock.MRP, ProductStock.SellingPrice) AS MRP,
	ProductStock.PurchaseBarcode,
	ProductStock.Stock,
	ProductStock.CreatedAt,

	Product.Id AS [ProductId],
	Product.Code AS [ProductCode],
	Product.Name AS [ProductName]

	FROM (SELECT TOP 1 * FROM StockTransfer ST1 
	WHERE ST1.AccountId = @AccountId AND ST1.ToInstanceId = @InstanceId AND ST1.TransferStatus = 3
	AND ISNULL(ST1.Prefix,'')+ST1.TransferNo = @TransferNo ORDER BY CreatedAt DESC) st2	
	INNER JOIN StockTransferItem STI WITH(NOLOCK) ON STI.TransferId = ST2.Id 	 
	INNER JOIN (SELECT * FROM ProductStock WITH(NOLOCK) WHERE AccountId = @AccountId AND InstanceId = @InstanceId) ProductStock ON ProductStock.Id = STI.ToProductStockId
	INNER JOIN (SELECT * FROM Product WITH(NOLOCK) WHERE AccountId = @AccountId ) Product ON Product.Id = ProductStock.ProductId 
	 
	SET NOCOUNT OFF    

END