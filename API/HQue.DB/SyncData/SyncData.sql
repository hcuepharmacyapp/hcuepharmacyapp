﻿	CREATE TABLE [dbo].[SyncData]
(
	[Id] INT NOT NULL Identity,
	[AccountId] CHAR(36) NOT NULL,
	[InstanceId] CHAR(36) NOT NULL, 
	[Action] CHAR(1) NOT NULL,
    [Data] VARCHAR(MAX) NOT NULL, 
    [CreatedAt] DATETIME NOT NULL DEFAULT getdate(),
	CONSTRAINT [PK_SyncData] PRIMARY KEY ([Id]) 
)
