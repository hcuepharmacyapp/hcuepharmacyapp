﻿CREATE TABLE [dbo].[SyncData_Reverse_Pending]
(
	[Id] INT NOT NULL Identity,
	[AccountId] CHAR(36) NOT NULL,
	[InstanceId] CHAR(36) NOT NULL, 
	[Action] CHAR(1) NOT NULL,
    [Data] VARCHAR(MAX) NOT NULL, 
    [CreatedAt] DATETIME NOT NULL DEFAULT getdate(),
	CONSTRAINT [PK_SyncDataReversePending] PRIMARY KEY ([Id]) 
)
