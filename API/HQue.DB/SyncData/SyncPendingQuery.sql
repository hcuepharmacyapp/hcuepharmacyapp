﻿CREATE TABLE [dbo].[SyncPendingQuery]
(
	[Id] CHAR(36) NOT NULL,
	[AccountId] CHAR(36) NOT NULL,
	[InstanceId] CHAR(36) NOT NULL, 
    [SyncQuery] VARCHAR(MAX), 
    [CreatedAt] DATETIME NOT NULL DEFAULT GETDATE(),
	CONSTRAINT [PK_SyncPendingQuery] PRIMARY KEY ([Id]), 
)
