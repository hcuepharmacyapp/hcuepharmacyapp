﻿CREATE TABLE [dbo].[SyncData_Backup](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[AccountId] [char](36) NOT NULL,
	[InstanceId] [char](36) NOT NULL,
	[Action] [char](1) NOT NULL,
	[Data] [varchar](max) NOT NULL,
	[CreatedAt] [datetime] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]