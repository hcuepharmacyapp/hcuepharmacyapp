﻿CREATE TABLE [dbo].[syncdata_Offline_Error](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[AccountId] [char](36) NULL,
	[InstanceId] [char](36) NULL,
	[Action] [char](1) NOT NULL,
	[Data] [varchar](max) NOT NULL,
	[CreatedAt] [datetime] NOT NULL CONSTRAINT [df_Createdat1]  DEFAULT (getdate()),
	[ErrorMessage] [nvarchar](max) NULL
)