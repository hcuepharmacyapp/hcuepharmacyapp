CREATE TABLE [dbo].[Reverse_SyncData_Seed]
(
	[AccountId] CHAR(36) NOT NULL, 
    [InstanceId] CHAR(36) NOT NULL, 
    [SeedIndex] INT NOT NULL, 
    [CreatedAt] DATETIME NOT NULL
)
