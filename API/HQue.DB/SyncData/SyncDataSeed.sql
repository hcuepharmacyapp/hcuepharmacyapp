﻿CREATE TABLE [dbo].[SyncDataSeed]
(
	[AccountId] CHAR(36) NOT NULL PRIMARY KEY, 
    [InstanceId] CHAR(36) NOT NULL, 
    [SeedIndex] INT NOT NULL, 
    [LastSynedAt] DATETIME NOT NULL
)
