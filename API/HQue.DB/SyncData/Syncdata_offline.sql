﻿
CREATE TABLE [dbo].[SyncData_Offline](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[AccountId] [char](36) NULL,
	[InstanceId] [char](36) NULL,
	[Action] [char](1) NOT NULL,
	[Data] [varchar](max) NOT NULL,
	[CreatedAt] [datetime] NOT NULL CONSTRAINT [df_Createdat]  DEFAULT (getdate())
)
