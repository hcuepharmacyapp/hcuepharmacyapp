/****** Object:  UserDefinedFunction [dbo].[GetSalesInvoiceNo]    Script Date: 24-11-2017 11.22.07 AM ******/

Create FUNCTION [dbo].[GetSalesInvoiceNo]
(@Accountid char(36),
@InstanceId char(36),
@Billdate date,
@InvoiceSeries varchar(10),
@Prefix varchar(2),
@BillNo Numeric(10,0)) Returns Numeric(10,0)
As
Begin
	Declare @BillReset bit, @FinyearId char(36), @sBillNo Numeric(10,0)
	Set @Prefix = IsNull(@Prefix, '')

	Select @BillReset = IsBillNumberReset from Account where id = @Accountid
	Select @FinyearId = id from FinYearMaster where Accountid = @Accountid AND cast(@Billdate as date) between FinYearStartDate and FinYearEndDate 
	select @FinyearId = isnull(@FinyearId,'')
	
	If @InvoiceSeries!='MAN'
	Begin
		If IsNull(@BillReset, 0) =1
			Begin
			SELECT @sBillNo =  ISNull(MAX(ISNULL(CONVERT(INT, InvoiceNo) , 0)),0)    FROM Sales WHERE AccountId = @Accountid AND InstanceId = @InstanceId and isnull(FinyearId,'') = @FinyearId 
			and isnull(Prefix ,'') = isNull(@Prefix, '') AND isnull(InvoiceSeries,'') = isnull(@InvoiceSeries,'')
			End
		Else
			Begin
			SELECT @sBillNo =  ISNull(MAX(ISNULL(CONVERT(INT, InvoiceNo) , 0)),0)    FROM Sales WHERE AccountId = @Accountid AND InstanceId = @InstanceId
			and isnull(Prefix ,'') = isNull(@Prefix, '') AND isnull(InvoiceSeries,'') = isnull(@InvoiceSeries,'')
			End
		Set @sBillNo = isnull(@sBillNo,0) + 1
	End
	Else
	Begin
		Set @sBillNo = @BillNo
	End

	Return @sBillNo

END