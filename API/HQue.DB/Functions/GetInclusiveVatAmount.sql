﻿ 
CREATE FUNCTION GetInclusiveVatAmount
(
@Value DECIMAL(18,2)
,@Vat DECIMAL(18,2)
)
RETURNS DECIMAL(18,2)
BEGIN
RETURN @Value -  (@Value * 100)/(@Vat + 100)
END