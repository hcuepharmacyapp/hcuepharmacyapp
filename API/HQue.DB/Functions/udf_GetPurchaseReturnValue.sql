
Create function dbo.udf_GetPurchaseReturnValue(@AccountId varchar(36),  @Instanceid varchar(36), @FromDate date,@ToDate date)
returns Numeric(20,2)
as
begin
Declare @PRValue Numeric(20,2)
     SELECT @PRValue = 
                Round(Convert(decimal(18,2),sum(((isnull(VendorReturnItem.ReturnPurchasePrice,VendorPurchaseItem.PurchasePrice) * VendorReturnItem.Quantity)))),0)  
                FROM VendorReturnItem
INNER JOIN VendorReturn ON VendorReturn.Id = VendorReturnItem.VendorReturnId  
INNER JOIN VendorPurchase ON VendorPurchase.Id = VendorReturn.VendorPurchaseId
INNER JOIN VendorPurchaseItem ON VendorPurchaseItem.ProductStockId = VendorReturnItem.ProductStockId
and VendorPurchaseItem.VendorPurchaseId = VendorReturn.VendorPurchaseId
                where  VendorPurchase.InstanceId = isnull(@InstanceId, VendorPurchase.InstanceId) and VendorPurchase.AccountId =@AccountId
					and isnull(VendorPurchase.CancelStatus ,0) =0
                AND Convert(date,VendorReturn.CreatedAt)   BETWEEN @FromDate AND @ToDate  
				and (VendorPurchaseItem.Status is null or VendorPurchaseItem.Status = 1)
           
return @PRValue
end 