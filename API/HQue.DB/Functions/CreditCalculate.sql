﻿create function [dbo].[CreditCalculate](@vendorPurchaseId varchar(50))
returns float
as
begin
declare @credit float;

--set @credit = (select sum(((vpi.PackagePurchasePrice * (vpi.PackageQty - isnull(vpi.FreeQty,0)))-(vpi.PackagePurchasePrice * (vpi.PackageQty - isnull(vpi.FreeQty,0)) * vpi.Discount/100)) +
--(((vpi.PackagePurchasePrice * (vpi.PackageQty - isnull(vpi.FreeQty,0)))-(vpi.PackagePurchasePrice * (vpi.PackageQty - isnull(vpi.FreeQty,0)) * vpi.Discount/100))*(case when vp.taxrefno=1 then ps.gsttotal else (case when ps.cst > 0 then ps.cst else ps.VAT end) end)/100))
--from VendorPurchaseItem(nolock) vpi join VendorPurchase(nolock) vp on vpi.VendorPurchaseId = vp.Id and (vpi.Status is null or vpi.Status = 1)
--join ProductStock(nolock) ps on ps.Id = vpi.ProductStockId
--where vp.Id=@vendorPurchaseId);

set @credit = (select sum(vpi.PurchasePrice*vpi.Quantity)
from VendorPurchaseItem(nolock) vpi join VendorPurchase(nolock) vp on vpi.VendorPurchaseId = vp.Id and (vpi.Status is null or vpi.Status = 1)
join ProductStock(nolock) ps on ps.Id = vpi.ProductStockId
where vp.Id=@vendorPurchaseId);

declare @notetype varchar(20);
set @notetype = (select NoteType from VendorPurchase(nolock) where Id=@vendorPurchaseId);
-- Added by Gavaskar 12-09-2017 Return Amount Start
declare @returnamount varchar(20);
set @returnamount = (select  isnull(ReturnAmount,0) from VendorPurchase(nolock) where Id=@vendorPurchaseId);
-- Added by Gavaskar 12-09-2017 Return Amount End
declare @amount decimal;
if(@notetype is not null)
begin
	set @amount = (select isnull(NoteAmount,0) from VendorPurchase(nolock) where Id=@vendorPurchaseId);
	if(@notetype = 'credit')
	set @credit = @credit - @amount -@returnamount;
	else
	set @credit = @credit + @amount -@returnamount;
end

return @credit;
end