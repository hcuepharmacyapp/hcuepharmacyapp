
Create function dbo.Udf_GetTransStock(@InstanceId char(36) ,@FromDate date,@ToDate date) -- @TransDate Date,
returns @Outuput table (Productid char(36), InQty int,  OutQty int,AdjQty int)
as
begin
 
		
		  --select  @Instanceid   ='18B4664B-E03C-4CA4-BD23-5E741BBC0C15',@FromDate = '2017-07-06',@ToDate='2017-07-07'
		  insert into @Outuput (Productid,InQty,OutQty,AdjQty)
SELECT   Productid, sum (inqty) , sum(outqty), sum(AdjQty)    FROM
(
SELECT id [ProductStockId], 'IN' trans, sum(importqty) inQty, 0 outQty,0 AdjQty FROM productstock 
	WHERE instanceid = @Instanceid   AND stockimport = 1 
	and cast(CreatedAt as date ) between  @FromDate and @ToDate  group by id
UNION ALL	
SELECT ProductStockId, 'IN' trans, sum(quantity) inQty, 0 outQty ,0 AdjQty FROM vendorpurchaseitem vp 
	 inner join vendorpurchase v on v.id = vp.VendorPurchaseId  and isnull(v.cancelstatus,0) = 0 
	 WHERE vp.instanceid = @Instanceid   and isnull(vp.status,0) =0
		and cast(vp.Updatedat as date ) between  @FromDate and @ToDate
		 group by ProductStockId
UNION ALL
SELECT ProductStockId, 'IN' trans, 0 inQty,  sum(quantity) outQty,0 AdjQty FROM VendorReturnItem vp
	  inner join vendorreturn v on v.id = vp.VendorReturnId   
			 WHERE vp.instanceid = @Instanceid 
			 and cast(vp.Updatedat as date ) between  @FromDate and @ToDate
			 group by ProductStockId
UNION ALL
SELECT ProductStockId, 'OUT' trans, 0 inQty, sum(Quantity) outQty,0 AdjQty FROM salesitem si 
	 inner join sales s on s.id = si.SalesId  and isnull(s.cancelstatus,0) = 0 
	 
	  WHERE si.instanceid = @Instanceid  
	  and cast(si.Updatedat as date ) between  @FromDate and @ToDate 
	   group by ProductStockId
UNION ALL
SELECT ProductStockId, 'IN' trans, sum(Quantity) inQty,  0 outQty ,0 AdjQty FROM SalesReturnItem si  
	inner join SalesReturn s on s.id = si.SalesReturnId    
	 
	WHERE si.instanceid = @Instanceid   and isnull(si.IsDeleted,0)  = 0  
	 and cast(si.Updatedat as date ) between  @FromDate and @ToDate
	 group by ProductStockId
UNION ALL
SELECT ProductStockId, 'OUT' trans, 0 inQty, sum(Quantity) outQty,0 AdjQty FROM StockTransferItem si  
inner join StockTransfer s on s.id = si.TransferId and isnull(s.TransferStatus,0) in (1,3)
 
 WHERE si.instanceid = @Instanceid    
  and cast(si.Updatedat as date ) between  @FromDate and @ToDate
   group by ProductStockId
UNION ALL
SELECT si.toProductStockId, 'In' trans, sum(Quantity) inQty, 0  outQty,0 AdjQty FROM StockTransferItem si  
inner join StockTransfer s on s.id = si.TransferId and isnull(s.TransferStatus,0) in (3)
 
 WHERE si.toinstanceid = @Instanceid 
  and cast(si.Updatedat as date )  between  @FromDate and @ToDate
   group by si.toProductStockId
UNION ALL
SELECT ProductStockId, 'OUT' trans, 0 inQty  ,0 outQty, sum(abs(AdjustedStock)) AdjQty FROM StockAdjustment  s  
 
 WHERE s.instanceid = @Instanceid  
  and cast(s.Updatedat as date ) between  @FromDate and @ToDate
 group by ProductStockId
 
) a  Inner join Productstock Ps on Ps.id = a.ProductStockId and ps.InstanceId =@InstanceId   group by productid

 
 return
 end 