/**   
*******************************************************************************                            
** Change History                               
*******************************************************************************                            
** Date        Author          Description                               
*******************************************************************************         
** 01/11/2017	Poongodi		Created
*******************************************************************************/ 
create function dbo.Udf_GetClosingStock(@InstanceId varchar(36),  @AccountId  VARCHAR(36) ,@TransDate date) -- @TransDate Date,
returns @outputable table (Productid char(36), ClosingQty decimal(18,2))
as
begin
 
		 Insert into @outputable
		 
  select  a.ProductId [Product], cast( sum( a.stock ) as decimal (18,2)) from
(
select  cs.productstockid, Productid, cs.instanceid,  max(isnull(Stockvalue,0)) stockval,  max(isnull(Closingstock,0)) stock  from 
 (Select productstockid, max(Transdate) [TransDate] from closingstock (nolock) where accountid =@AccountId  and instanceid =@InstanceId  and Transdate <= @TransDate
 group by Productstockid ) ts inner join 
  (Select *    from closingstock (nolock) where accountid =@AccountId and Transdate <= @TransDate and instanceid =@InstanceId) cs  on cs.Productstockid= ts.Productstockid
  and cs.transdate = ts.transdate 
  inner join (select * from productstock(nolock) where  accountid =@AccountId  and instanceid =@InstanceId ) ps on ps.id = cs.productstockid 
  group by  cs.productstockid, Productid, cs.instanceid
 ) a  
  group by   a.ProductId
 
return  
end 