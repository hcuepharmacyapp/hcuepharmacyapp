/**   
*******************************************************************************                            
** Change History                               
*******************************************************************************                            
** Date        Author          Description                               
*******************************************************************************         
** 25/07/2017	Poongodi		 Optimized  
** 06/09/2017	Poongodi		 Import Qty taken from productstock based on created date
*******************************************************************************/ 
create function dbo.Udf_GetOpeningStock(@InstanceId char(36),  @AccountId  VARCHAR(36) ,@TransDate date) -- @TransDate Date,
returns @outputable table (Productid char(36), ClosingQty bigint)
as
begin
 
		 Insert into @outputable
		 
  select  p.id [Product],  sum( a.stock ) from
(
select  cs.productstockid, Productid, cs.instanceid,  max(isnull(Stockvalue,0)) stockval,  max(isnull(Closingstock,0)) stock  from 
 (Select productstockid, max(Transdate) [TransDate] from closingstock (nolock) where accountid =@AccountId  and instanceid =@InstanceId  and Transdate < @TransDate
 group by Productstockid ) ts inner join 
  (Select *    from closingstock (nolock) where accountid =@AccountId and Transdate < @TransDate and instanceid =@InstanceId) cs  on cs.Productstockid= ts.Productstockid
  and cs.transdate = ts.transdate 
  inner join (select * from productstock(nolock) where  accountid =@AccountId  and instanceid =@InstanceId ) ps on ps.id = cs.productstockid 
  group by  cs.productstockid, Productid, cs.instanceid
 ) a inner join (select * from product (nolock) where  accountid =@AccountId  ) p on p.id = a.productid
  group by   p.id
	/*	 SELECT t.productid [ProductId] ,
case when sum(isnull(actual,0)) <0  then 0 else sum(isnull(actual,0)) end [ClosingQty] 
 
 FROM (
SELECT ps.id,ps.BatchNo,ps.ExpireDate,ps.productid,imp.importqty,qty.newstockqty, 
 ps.stock, si.quantity si, sr.quantity sr, vp.quantity vp, vr.quantity vr, st.quantity st, st1.quantity st1,
 ad.adjustedstock adjOut,
  ad1.adjustedstock adjin,
 dc.quantity dc ,
 temp.quantity temp,
  isnull(temp.quantity ,0) + isnull(dc.quantity ,0) + isnull(imp.importqty,0) +isnull( qty.newstockqty,0) +isnull(vp.quantity,0)+isnull(sr.quantity,0)+isnull(ad1.adjustedstock,0) [TotalInQty],
   isnull(si.quantity,0)-isnull(vr.quantity,0)+isnull(st1.quantity,0)+isnull(st.quantity,0)+isnull(ad.adjustedstock,0) [TotalOutQty] , 
  isnull(temp.quantity ,0) + isnull(dc.quantity ,0) + isnull(imp.importqty,0) +isnull( qty.newstockqty,0) +isnull(vp.quantity,0)+isnull(sr.quantity,0)-isnull(si.quantity,0)-isnull(vr.quantity,0)-isnull(st1.quantity,0)+isnull(st.quantity,0)+isnull(ad.adjustedstock,0)+isnull(ad1.adjustedstock,0)-isnull(consumption.quantity,0) actual FROM
(select * from productstock (nolock) where instanceid = @InstanceId and cast(updatedat as date) < @TransDate ) ps LEFT OUTER JOIN 
(select sum(quantity) quantity, productstockid from salesitem (nolock) where instanceid = @InstanceId and cast(updatedat as date) < @TransDate  group by productstockid) si ON si.productstockid = ps.id LEFT OUTER JOIN 
(select sum(quantity) quantity, productstockid from salesreturnitem (nolock) where instanceid = @InstanceId  and cast(updatedat as date) < @TransDate group by productstockid) sr ON sr.productstockid = ps.id LEFT OUTER JOIN 
(select sum(quantity) quantity, productstockid from vendorpurchaseitem (nolock) where instanceid = @InstanceId and cast(updatedat as date) < @TransDate  group by productstockid) vp ON vp.productstockid = ps.id LEFT OUTER JOIN 
(select sum(quantity) quantity, productstockid from vendorreturnitem (nolock) where instanceid = @InstanceId and cast(updatedat as date) < @TransDate  group by productstockid) vr ON vr.productstockid = ps.id LEFT OUTER JOIN 
(select sum(quantity) quantity, productstockid from stocktransferitem (nolock) s1 INNER JOIN stocktransfer (nolock) s2 ON s2.id = s1.transferid where transferstatus in(1,3) AND s1.instanceid = @InstanceId  and cast(s1.updatedat as date) < @TransDate  group by s1.productstockid) st1 ON st1.productstockid = ps.id LEFT OUTER JOIN 
(select sum(quantity) quantity, toproductstockid from stocktransferitem (nolock) s1 INNER JOIN stocktransfer (nolock) s2 ON s2.id = s1.transferid where transferstatus = 3 AND s1.toinstanceid = @InstanceId  and cast(s1.updatedat as date) < @TransDate  group by s1.toproductstockid) st ON st.toproductstockid = ps.id LEFT OUTER JOIN 
(select sum(abs(adjustedstock)) adjustedstock, productstockid from stockadjustment (nolock) where instanceid = @InstanceId  and cast(updatedat as date) < @TransDate and adjustedstock<0  group by productstockid) ad ON ad.productstockid = ps.id LEFT OUTER JOIN 
(select sum(adjustedstock) adjustedstock, productstockid from stockadjustment (nolock) where instanceid = @InstanceId  and cast(updatedat as date) < @TransDate  and adjustedstock>0 group by productstockid) ad1 ON ad.productstockid = ps.id LEFT OUTER JOIN 
(select sum(quantity) quantity, productstockid from dcvendorpurchaseitem (nolock) where instanceid = @InstanceId    AND Isnull(ispurchased, 0) = 0  and cast(updatedat as date) < @TransDate  group by productstockid) dc ON dc.productstockid = ps.id LEFT OUTER JOIN  
(select sum(quantity) quantity, productstockid from tempvendorpurchaseitem (nolock) where instanceid = @InstanceId  AND isactive = 1   and cast(updatedat as date) < @TransDate  group by productstockid) temp ON temp.productstockid = ps.id LEFT OUTER JOIN  
(select sum(Consumption) quantity, productstockid from SelfConsumption (nolock) where instanceid = @InstanceId   and cast(updatedat as date) < @TransDate  group by productstockid) consumption ON consumption.productstockid = ps.id  LEFT JOIN
(select sum(importqty) importqty, id productstockid from productstock (nolock) where instanceid = @InstanceId   and cast(Createdat as date) < @TransDate and isnull(StockImport,0) =1 and  Isnull(newopenedstock, 0)  = 0   group by id) imp ON imp.productstockid = ps.id  
cross apply (select case  Isnull(newopenedstock, 0) when 1   then Isnull(newstockqty, 0) else 0 end  newstockqty) [qty]
) t  inner join (select * from product where accountid =@accountid) p on p.id = t.productid
group by t.productid 
 */
return  
end 