Create function dbo.udf_GetSaleValue(@AccountId varchar(36),  @Instanceid varchar(36), @FromDate date,@ToDate date)
returns Numeric(20,2)
as
begin
Declare @SaleValue Numeric(20,2)
 select  @SaleValue = sum(Amount)   from (
                select  
                 round(Convert(decimal(18,2),(sum(isnull(salesitem.SellingPrice ,productstock.SellingPrice )
				* salesitem.Quantity - 
	( isnull(salesitem.SellingPrice ,productstock.SellingPrice ) * salesitem.Quantity * sales.Discount / 100))-sum(salesitem.Quantity * 
	 isnull(salesitem.SellingPrice ,productstock.SellingPrice )  * isnull(salesitem.Discount,0) / 100))),0)  As Amount 
               
                from sales inner join salesitem on sales.id = salesitem.salesid inner join productstock on salesitem.productstockid = productstock.id
              
                WHERE     sales.InstanceId = isnull(@InstanceId, sales.InstanceId) and sales.AccountId =@AccountId
                AND Convert(date,sales.CreatedAt)BETWEEN @FromDate AND @ToDate    and isnull(sales.Cancelstatus,0) =0
               
				group by sales.id 
				) a  

return @SaleValue
end