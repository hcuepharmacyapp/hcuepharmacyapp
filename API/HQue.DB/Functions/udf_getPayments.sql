/*******************************************************************************                            
** Change History                              
*******************************************************************************                            
** Date        Author          Description                              
*******************************************************************************        
** 25/10/2017	Sarubala		Function created
********************************************************************************/ 

create function dbo.udf_getPayments(@AccountId varchar(36),@InstanceId varchar(36), @Fromdate date, @Todate date)
returns @Output table (SalesId varchar(36),[cash] decimal(18,6),[card] decimal(18,6),cheque decimal(18,6),credit decimal(18,6),eWallet decimal(18,6),subPayment varchar(36))
as

begin

with sales1 as 
(select sp.SalesId,sp.Amount,dv.DisplayText,sdv1.DisplayText subDomainText
from sales s left join (select * from  SalesPayment WITH(NOLOCK) where AccountId = @AccountId AND Instanceid = @InstanceId )sp on s.id=sp.SalesId
join DomainValues dv WITH(NOLOCK) on dv.DomainValue=sp.PaymentInd
left join DomainValues sdv1 WITH(NOLOCK) on sdv1.DomainValue = sp.SubPaymentInd
where s.AccountId=@AccountId and s.InstanceId=@InstanceId 
and CONVERT(date,s.InvoiceDate) between @Fromdate and @Todate and s.PaymentType='Multiple' and isnull(sp.isActive,0) = 1)
insert into @Output 
select max(SalesId) SalesId,max(isnull([cash],0)) [cash],max(isnull([card],0)) [card],max(isnull([cheque],0)) [cheque],max(isnull([credit],0)) [credit],max(isnull([ewallet],0)) [ewallet], max(isnull(subDomainText,'')) subPayment from sales1 
pivot (max(amount) for DisplayText in ([cash],[card],[cheque],[credit],[ewallet])) P
group by SalesId

return

end
