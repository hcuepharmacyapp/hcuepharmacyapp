Create function dbo.udf_GetPurchaseValue(@AccountId varchar(36),  @Instanceid varchar(36), @FromDate date,@ToDate date)
returns Numeric(20,2)
as
begin
Declare @PoValue Numeric(20,2)
    SELECT   @PoValue=
             Sum(Isnull([vendorpurchaseitem.povalue], 0) + ( 
                 Isnull([vendorpurchaseitem.povalue], 0) * 
                 Isnull([productstock.vat], 0) / 
                 100 
                   ))                                   
             
      FROM   (SELECT vendorpurchaseitem.productstockid, 
                      
                  
                     productstock.vat           AS [ProductStock.VAT], 
                    sum( Isnull(vendorpurchaseitem.packagepurchaseprice, 0) * ( 
                     Isnull 
                     ( 
                     vendorpurchaseitem.packageqty, 0) 
                     - 
                                          Isnull 
                     ( 
                     vendorpurchaseitem.freeqty, 0) ) - ( 
                     Isnull( 
                     vendorpurchaseitem.packagepurchaseprice, 0) * 
     ( 
                     Isnull 
                     ( 
                     vendorpurchaseitem.packageqty, 0) 
                     - 
                                          Isnull 
                     ( 
                     vendorpurchaseitem.freeqty, 0) ) * 
      Isnull 
      ( 
             vendorpurchaseitem.discount, 0) / 
      100 ) )
      [VendorPurchaseItem.POValue] 
      FROM   vendorpurchaseitem 
      INNER JOIN vendorpurchase 
      ON vendorpurchase.id = vendorpurchaseitem.vendorpurchaseid 
    
      INNER JOIN productstock 
      ON productstock.id = vendorpurchaseitem.productstockid 
   
      WHERE cast( vendorpurchase.createdat as date)BETWEEN @FromDate AND @ToDate 
  
	   and (vendorpurchaseitem.Status is null or vendorpurchaseitem.Status = 1)
	  and isnull(VendorPurchase.CancelStatus ,0)= 0
      AND vendorpurchase.accountid = @AccountId 
and   vendorpurchase.InstanceId = isnull(@InstanceId, vendorpurchase.InstanceId)
      GROUP  BY  
               vendorpurchaseitem.productstockid, productstock.vat 
      )AS one 

return @PoValue
end 