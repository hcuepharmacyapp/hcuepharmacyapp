﻿ 
CREATE FUNCTION [dbo].[udf_Split] (@string NVARCHAR(MAX))
RETURNS @Output TABLE (id NVARCHAR(MAX))
AS 
BEGIN
  DECLARE @separator NCHAR(1)
  SET @separator=','
  DECLARE @position int
  SET @position = 1
  SET @string = @string + @separator
  WHILE charindex(@separator,@string,@position) <> 0
     BEGIN
        INSERT into @Output
        SELECT substring(@string, @position, charindex(@separator,@string,@position) - @position)
        SET @position = charindex(@separator,@string,@position) + 1
     END
    RETURN 
END
 