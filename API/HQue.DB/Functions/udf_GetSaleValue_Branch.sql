Create function dbo.udf_GetSaleValue_Branch (@AccountId varchar(36),  @Instanceid varchar(36), @FromDate date,@ToDate date)
returns @Outputtable table (BranchId Varchar(36), BranchName varchar(150), SaleValue Numeric(20,2))
as
begin
 Insert into @Outputtable (BranchId, BranchName,SaleValue)

 select   InstanceId, Name, sum(Amount)   from (
                select  Sales.InstanceId,
                 round(Convert(decimal(18,2),(sum(isnull(salesitem.SellingPrice ,productstock.SellingPrice )
				* salesitem.Quantity - 
	( isnull(salesitem.SellingPrice ,productstock.SellingPrice ) * salesitem.Quantity * sales.Discount / 100))-sum(salesitem.Quantity * 
	 isnull(salesitem.SellingPrice ,productstock.SellingPrice )  * isnull(salesitem.Discount,0) / 100))),0)  As Amount 
               
                from sales inner join salesitem on sales.id = salesitem.salesid inner join productstock on salesitem.productstockid = productstock.id
              
                WHERE     sales.InstanceId = isnull(@InstanceId, sales.InstanceId) and sales.AccountId =@AccountId
                AND Convert(date,sales.CreatedAt)BETWEEN @FromDate AND @ToDate    and isnull(sales.Cancelstatus,0) =0
               
				group by sales.id , Sales.InstanceId
				) a  Inner join instance I on I.id = a.InstanceId
				Group by InstanceId,Name
Return  
end