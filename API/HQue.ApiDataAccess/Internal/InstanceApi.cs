﻿using System.Collections.Generic;
using System.Threading.Tasks;
using HQue.Contract.Infrastructure.Setup;
using HQue.ServiceConnector.Connector;
using Utilities.Helpers;

namespace HQue.ApiDataAccess.Internal
{
    public class InstanceApi : BaseApi
    {
        public InstanceApi(ConfigHelper configHelper) : base(configHelper)
        {

        }

        public async Task<List<Instance>> GetInstance(string accountId)
        {
            var connector = new RestConnector();
            return await connector.GetAsyc<List<Instance>>($"{ConfigHelper.InternalApi.Instance}?accountid={accountId}");
        }
    }
}
