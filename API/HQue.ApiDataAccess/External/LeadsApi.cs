﻿using HQue.ServiceConnector.Connector;
using System;
using System.Threading.Tasks;
using HQue.Contract.External.Leads;
using Utilities.Helpers;
using System.IO;
using Newtonsoft.Json;
using System.Text;

namespace HQue.ApiDataAccess.External
{
    public class LeadsApi : BaseApi
    {

        public LeadsApi(ConfigHelper configHelper) : base(configHelper)
        {

        }

        public async Task<SearchResponse> GetLeadsExternal(Int64 instanceId, long leadId, string leadStatus)
        {
            var rc = new RestConnector();
            var extData = new SearchRequest
            {
                Status = leadStatus,
                StartDate = CustomDateTime.IST.AddDays(-2).ToExternalFormat(),
                PageSize = 10,
                PageNumber = 1,
                PatientCaseID = leadId,
                PharmaID = instanceId,
                EndDate = CustomDateTime.IST.AddDays(2).ToExternalFormat()
            };
            AddAuthHeader(rc, Platform);

            string name = string.Format("patientAPI.txt");
            FileInfo info = new FileInfo(name);
            /*As suggested by Martin Log writting commented by Poongodion 6/10/2017*/
            /*using (StreamWriter writer = info.AppendText())
              {
                  string jsonStr = JsonConvert.SerializeObject(extData);
                  var msg = string.Format("{0} : InstanceID : {1} | LeadID : {2} \r\n Request : \r\n", DateTime.Now.ToString(), instanceId, leadId);
                  writer.Write(msg);
                  writer.Write(jsonStr);
              }*/


            var result = await rc.PostAsyc<SearchResponse>(await AddAuthParameter(ConfigHelper.ExternalApi.UrlGetPharmaLeads,Platform), extData);

            //MAR: we need to retry incase we get service call failure.

            return result;
        }
        public async Task UpdateLeadsExternal(string leadStatus, Int64 patientCaseId, string rowId)
        {
            var rc = new RestConnector();
            var extData = new UpdateRequest
            {
                Status = leadStatus,
                USRType = Constant.UserType,
                RowID = rowId,
                PatientCaseID = patientCaseId,
                USRId = 38
            };
            AddAuthHeader(rc, Platform);
            var result =
                await
                    rc.PostAsyc<UpdateResponse[]>(
                        await AddAuthParameter(ConfigHelper.ExternalApi.UrlUpdatePatientCasePrescriptionStatus, Platform), extData);
        }

        public async Task updatePharmaLeads(string leadStatus, Int64 patientCaseId, string rowId, Int32 ExternalPharmaId)
        {
            var rc = new RestConnector();
            var extData = new UpdateRequest
            {
                Status = leadStatus,
                USRType = Constant.UserType,
                RowID = rowId,
                PatientCaseID = patientCaseId,
                USRId = ExternalPharmaId
            };
            AddAuthHeader(rc, Platform);
            var result =
                await
                    rc.PostAsyc<UpdateResponse[]>(
                        await AddAuthParameter(ConfigHelper.ExternalApi.UrlUpdatePharmaLeadsStatus, Platform), extData);
        }
    }
}
