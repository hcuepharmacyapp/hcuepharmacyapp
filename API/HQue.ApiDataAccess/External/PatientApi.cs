﻿using HQue.Contract.External.Common;
using HQue.Contract.External.Patient;
using HQue.Contract.Infrastructure.Master;
using HQue.ServiceConnector.Connector;
using System.Collections.Generic;
using System.Threading.Tasks;
using HQue.DataAccess.Master;
using System;
using Utilities.Helpers;
using Newtonsoft.Json;
using System.Linq;

namespace HQue.ApiDataAccess.External
{
    public class PatientApi : BaseApi
    {

        public PatientApi(ConfigHelper configHelper) : base(configHelper)
        {

        }

        public async Task<Patient> PatientSaveExternal(Patient data)
        {
            var sc = new RestConnector();
            string dDob = null;

            if (data.DOB.HasValue)
                dDob = data.DOB.Value.ToString("yyyy-MM-dd");

            if (data.Address == null)
            {
                data.Address = "N/A";
            }
            if (data.Email == null)
            {
                //data.Email = "N/A";
                data.Email = null;
            }
            if(data.Discount == null)
            {
                data.Discount = 0;
            }

            ExtPatient patientDetails = new ExtPatient
            {
                FirstName = data.Name,
                FullName = data.Name,
                Gender = data.Gender,
                TermsAccepted = "Y",
                DOB = dDob,
                Age = data.Age,
                MobileID = long.Parse(data.Mobile),
                Discount = data.Discount
            };
            Phone[] phone =
            {
                new Phone
                {
                    PhCntryCD = 91,
                    PhStateCD = 9962,
                    PhAreaCD = 233866,
                    PhNumber = long.Parse(data.Mobile),
                    PhType = "M",
                    PrimaryIND = "Y"
                }
            };
            Address[] address =
            {
                new Address
                {
                    Address1 = data.Address,
                    Address2 = data.Address,
                    AddressType = "H",
                    CityTown = "MAS",
                    Country = "IND",
                    DistrictRegion = "NewChennai",
                    Location = "345",
                    Latitude = "5",
                    Longitude = "6",
                    PrimaryIND = "Y",
                    State = "TN",
                    Street = "New Metro Street"
                }
            };
            Email[] email = { new Email { EmailID = data.Email, EmailIDType = "P", PrimaryIND = "Y" } };
            if (data.Email == null)
            {
                email =null;
            }

            var extData = new AddRequest
            {
                USRType = Constant.PharmaUserType,
                patientDetails = patientDetails,
                USRId = long.Parse(data.Mobile),
                patientPhone = phone,
                patientAddress = address,
                patientEmail = email
            };

            AddAuthHeader(sc, Patient);
            var uri = await AddAuthParameter(ConfigHelper.ExternalApi.UrlAddPatient, Patient);
            var result = await sc.PostAsyc<AddResponse>(uri, extData);

            data.Id = result.Patient[0].PatientID?.ToString();
            return data;
        }

        public async Task<Patient> UpdatePatientExternal(Patient data)
        {
            var sc = new RestConnector();
            string dDob = null;

            if (data.DOB.HasValue)
                dDob = data.DOB.Value.ToString("yyyy-MM-dd");

            if (data.Address == null)
            {
                data.Address = "N/A";
            }
            if (data.Email == null)
            {
                //data.Email = "N/A";
                data.Email = null;
            }
            if(data.Discount == null)
            {
                data.Discount = 0;
            }

            ExtPatient patientDetails = new ExtPatient
            {
                FirstName = data.Name,
                FullName = data.Name,
                Gender = data.Gender,
                TermsAccepted = "Y",
                DOB = dDob,
                Age = data.Age,
                MobileID = long.Parse(data.Mobile),
              //  PatientID = Convert.ToInt64(data.Id),
                EmpID = data.EmpID,
                Discount = data.Discount
            };
            Phone[] phone =
            {
                new Phone
                {
                    PhCntryCD = 91,
                    PhStateCD = 9962,
                    PhAreaCD = 233866,
                    PhNumber = long.Parse(data.Mobile),
                    PhType = "M",
                    PrimaryIND = "Y"
                }
            };
            Address[] address =
            {
                new Address
                {
                    Address1 = data.Address,
                    Address2 = data.Address,
                    AddressType = "H",
                    CityTown = "MAS",
                    Country = "IND",
                    DistrictRegion = "NewChennai",
                    Location = "345",
                    Latitude = "5",
                    Longitude = "6",
                    PrimaryIND = "Y",
                    State = "TN",
                    Street = "New Metro Street"
                }
            };

            Email[] email = { new Email { EmailID = data.Email, EmailIDType = "P", PrimaryIND = "Y" } };
            if (data.Email == null)
            {
                email = null;
            }

            var extData = new AddRequest
            {
                USRType = Constant.UserType,
                patientDetails = patientDetails,
                USRId = long.Parse(data.Mobile),
                patientPhone = phone,
                patientAddress = address,
                patientEmail = email
            };

            var sz = JsonConvert.SerializeObject(extData);

            AddAuthHeader(sc, Patient);
            var uri = await AddAuthParameter(ConfigHelper.ExternalApi.UrlUpdatePatient, Patient);
            var result = await sc.PostAsyc<AddResponse>(uri, extData);
            return data;
        }

        public async Task<IEnumerable<Patient>> PatientListExternal(Patient data)
        {
            var sc = new RestConnector();
            var extData = new SearchRequest {PhoneNumber = data.Mobile};
            AddAuthHeader(sc, Patient);
            var uri = await AddAuthParameter(ConfigHelper.ExternalApi.UrlGetPatient, Patient);
            var result = await sc.PostAsyc<SearchResponse>(uri, extData);
            return PatientDataAccess.GetPatientList(result.rows);
        }
        public async Task<IEnumerable<Patient>> PatientSearchListExternal(string data)
        {
            var sc = new RestConnector();
            var extData = new SearchPatientRequest { FirstName = data };
            AddAuthHeader(sc, Patient);
            var uri = await AddAuthParameter(ConfigHelper.ExternalApi.UrlGetPatient, Patient);
            var result = await sc.PostAsyc<SearchResponse>(uri, extData);
            var plist =  PatientDataAccess.GetPatientList(result.rows);
            plist = plist.OrderBy(x => x.Name).ToList();

            // return PatientDataAccess.GetPatientList(result.rows);
            return plist;
        }
    }
}
