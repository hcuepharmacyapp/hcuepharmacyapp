﻿using HQue.Contract.External.Common;
using HQue.Contract.Infrastructure.Setup;
using HQue.ServiceConnector.Connector;
using System.Threading.Tasks;
using HQue.Contract.External.Pharma;
using static System.Int64;
using Utilities.Helpers;

namespace HQue.ApiDataAccess.External
{
    public class PharmaApi : BaseApi
    {

        public PharmaApi(ConfigHelper configHelper) : base(configHelper)
        {
            
        }

        public async Task<ExtPharma> SavePharmaExternal(Instance data)
        {
            var rc = new RestConnector();
            //string phone = "";
            //if (!string.IsNullOrEmpty(data.ContactMobile))
            //    phone = data.ContactMobile;
            //else
            //    phone = data.Phone;

            AddRequest extData;


            //if (!string.IsNullOrEmpty(data.ContactMobile))
            //{

            //    extData = new AddRequest
            //    {
            //        USRType = Constant.UserType,
            //        MobileNumber = Parse(data.ContactMobile),
            //        WebSite = data.Website,
            //        Address1 = data.Address,
            //        USRId = 0,
            //        PharmaName = data.Name,
            //        TINNumber = data.TinNo,
            //        ContactName = data.ContactName,
            //    };
            //}
            //else
            //{
            //    extData = new AddRequest
            //    {
            //        USRType = Constant.UserType,
            //        WebSite = data.Website,
            //        Address1 = data.Address,
            //        USRId = 0,
            //        PharmaName = data.Name,
            //        TINNumber = data.TinNo,
            //        ContactName = data.ContactName,
            //    };
            //}

            if (string.IsNullOrEmpty(data.ExternalId))
            {

                extData = new AddRequest
                {
                    USRType = Constant.UserType,
                    MobileNumber = Parse(data.ContactMobile),
                    PharmaExternalId = data.ExternalId,
                    WebSite = data.Website,
                    Address1 = data.Address,
                    USRId = 0,
                    PharmaName = data.Name,
                    TINNumber = data.TinNo,
                    ContactName = data.ContactName,
                };
            }
            else
            {
                extData = new AddRequest
                {
                    USRType = Constant.UserType,
                    MobileNumber = Parse(data.ContactMobile),
                    WebSite = data.Website,
                    Address1 = data.Address,
                    USRId = 0,
                    PharmaName = data.Name,
                    TINNumber = data.TinNo,
                    ContactName = data.ContactName,
                };
            }

            AddAuthHeader(rc, Platform);
            var uri = await AddAuthParameter(ConfigHelper.ExternalApi.UrlAddUpdatePharma, Platform);
            var result = await rc.PostAsyc<AddResponse>(uri, extData);
            return result.PharmaDetails;
        }
    }
}
