﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using HQue.ServiceConnector.Connector;
using Utilities.Helpers;

namespace HQue.ApiDataAccess
{
    public class BaseApi
    {
        protected ConfigHelper ConfigHelper;
        public const int Patient = 1;
        public const int Platform = 2;

        private const string CloudFrontSignature = "CloudFront-Signature";
        private const string CloudFrontKeyPairId = "CloudFront-Key-Pair-Id";
        private const string CloudFrontPolicy = "CloudFront-Policy";

        public BaseApi(ConfigHelper configHelper)
        {
            ConfigHelper = configHelper;
        }

        protected async Task<string> AddAuthParameter(string url, int authType)
        {
            if (!ConfigHelper.AppConfig.ApiAuth)
                return url;

            if (authType == Patient)
            {
                var con = new RestConnector();
                AddAuthHeader(con, authType);
                var header = await con.GetHeaderAsyc(ConfigHelper.ExternalApi.UrlGetPatientCookie);
                return  GetCookies(header,url);
            }
            if (authType == Platform)
            {
                var con = new RestConnector();
                AddAuthHeader(con, authType);
                var header = await con.GetHeaderAsyc(ConfigHelper.ExternalApi.UrlGetPlatformCookie);
                return GetCookies(header, url);
            }

            throw new Exception($"Invalid option {authType}");
        }

        public void AddAuthHeader(RestConnector connector, int authType)
        {
            if (!ConfigHelper.AppConfig.ApiAuth)
                return;

            if (authType == Patient)
            {
                connector.AddHeaders("Authorization", ConfigHelper.AppConfig.PatientAuthToken);
            }
            if (authType == Platform)
            {
                connector.AddHeaders("Authorization", ConfigHelper.AppConfig.PlatformAuthToken);
            }
        }

        private string GetCookies(HttpResponseHeaders header, string sourceUrl)
        {
            IEnumerable<string> signature;
            header.TryGetValues(CloudFrontSignature, out signature);

            IEnumerable<string> keyPairId;
            header.TryGetValues(CloudFrontKeyPairId, out keyPairId);

            IEnumerable<string> policy;
            header.TryGetValues(CloudFrontPolicy, out policy);

            return $"{sourceUrl}?Signature={signature.First()}&Key-Pair-Id={keyPairId.First()}&Policy={policy.First()}";
        }
    }
}