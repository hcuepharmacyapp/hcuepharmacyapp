﻿namespace HQue.Contract.External.Common
{
    public class ExtLeadsPrescription
    {
        public string Medicine { get; set; }
        public int Quantity { get; set; }
        public string PharmaWrkFlowStatusID { get; set; }
        public long PatientCaseID { get; set; }
        public string Dosage1 { get; set; }
        public string Dosage2 { get; set; }
        public string Dosage4 { get; set; }
        public string NumberofDays { get; set; }
        public string BeforeAfter { get; set; }
        public string MedicineType { get; set; }
    }
}
