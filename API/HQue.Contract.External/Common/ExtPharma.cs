﻿using System;

namespace HQue.Contract.External.Common
{
    public class ExtPharma
    {
        public string EmailAddress { get; set; }
        public Int64 MobileNumber { get; set; }
        public string WebSite { get; set; }
        public string PharmaName { get; set; }
        public string TINNumber { get; set; }
        public Int64 PharmaID { get; set; }
        public string ContactName { get; set; }
        public string Password { get; set; }
        public string Prospect { get; set; }
        public string TermsAccepted { get; set; }
    }
}
