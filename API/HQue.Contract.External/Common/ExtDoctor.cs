﻿using System;

namespace HQue.Contract.External.Common
{
    public class ExtDoctor
    {
        public string FullName { get; set; }
        public Int64 DoctorID { get; set; }
        public string Gender { get; set; }
        public int Exp { get; set; }
        public decimal Fees { get; set; }
        public int AddressID { get; set; }
        public string Address { get; set; }
        public string EmailID { get; set; }
        public string PhoneCountryCD { get; set; }
        public int PhoneNumber { get; set; }
        public string Distance { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public string ProfileImage { get; set; }
        //public string Qualification { get; set; }
        public string SpecialityID { get; set; }
        //public List<string> SpecialityCD { get; set; }
    }
}
