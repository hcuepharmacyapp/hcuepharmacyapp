﻿namespace HQue.Contract.External.Common
{
    public class PatientAddress
    {
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string PrimaryIND { get; set; }
        public string Latitude { get; set; }
        public string DistrictRegion { get; set; }
        public string Longitude { get; set; }
        public int PinCode { get; set; }
        public string State { get; set; }
        public string Street { get; set; }
        public string Country { get; set; }
        public string AddressType { get; set; }
        public string CityTown { get; set; }
        public string Location { get; set; }
    }
}
