﻿namespace HQue.Contract.External.Common
{
    public class Email
    {
        public string EmailID { get; set; }
        public string PrimaryIND { get; set; }
        public string EmailIDType { get; set; }
        public string ValidIND { get; set; }
    }
}
