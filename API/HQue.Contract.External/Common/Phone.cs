﻿using System;

namespace HQue.Contract.External.Common
{
    public class Phone
    {
        public string PhType { get; set; }
        public string PrimaryIND { get; set; }
        public int PhAreaCD { get; set; }
        public int PhCntryCD { get; set; }
        public Int64 PhNumber { get; set; }
        public int PhStateCD { get; set; }
    }
}
