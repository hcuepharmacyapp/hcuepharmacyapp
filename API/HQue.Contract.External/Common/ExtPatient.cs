﻿using System;

namespace HQue.Contract.External.Common
{
    public class ExtPatient
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string FullName { get; set; }
        public long? PatientID { get; set; }
        public int AadhaarID { get; set; }
        public string Gender { get; set; }
        public string DOB { get; set; }
        public decimal? Age { get; set; }
        public string BloodGroup { get; set; }
        public string FamilyHdIND { get; set; }
        public Int64 FamilyHdID { get; set; }
        public string TermsAccepted { get; set; }
        public Int64 MobileID { get; set; }
        public string PhNumber { get; set; }
        public string EmailID { get; set; }
        public string EmpID { get; set; }
        public decimal? Discount { get; set; }

    }
}
