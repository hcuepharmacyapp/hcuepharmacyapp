﻿namespace HQue.Contract.External.Common
{
    public class MobileVerifySearch
    {
        public string phone { get; set; }
        
    }

    public class OTPVerifySearch : MobileVerifySearch
    {
        public string code { get; set; }
    }

    public class MobileVerifyResponse
    {
        public dynamic error { get; set; }
        public bool verified { get; set; }
    }
}
