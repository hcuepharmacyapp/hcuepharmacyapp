﻿using System;

namespace HQue.Contract.External.Pharma
{
    public class AddRequest
    {
        public string USRType { get; set; }
        public Int64 MobileNumber { get; set; }
        public string LoginID { get; set; }
        public string WebSite { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public int USRId { get; set; }
        public string PharmaName { get; set; }
        public string TINNumber { get; set; }
        //public int PharmaID { get; set; }
        public string ContactName { get; set; }
        public string Password { get; set; }

        public string PharmaExternalId { get; set; }
    }
}
