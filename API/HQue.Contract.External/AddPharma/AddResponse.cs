using HQue.Contract.External.Common;

namespace HQue.Contract.External.Pharma
{
    public class AddResponse
    {
        public ExtPharma PharmaDetails { get; set; }
    }
}