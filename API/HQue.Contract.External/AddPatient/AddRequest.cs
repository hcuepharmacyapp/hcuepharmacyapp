﻿using System;
using HQue.Contract.External.Common;

namespace HQue.Contract.External.Patient
{
    public class AddRequest
    {
        public ExtPatient patientDetails { get; set; }
        public Phone[] patientPhone { get; set; }
        public string USRType { get; set; }
        public Int64 USRId { get; set; }

        public Address[] patientAddress { get; set; }

        public Email[] patientEmail { get; set; }
    }
}
