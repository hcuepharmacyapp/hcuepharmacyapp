using HQue.Contract.External.Common;

namespace HQue.Contract.External.Patient
{
    public class AddResponse
    {
        public ExtPatient[] Patient { get; set; }
        public Address[] PatientAddress { get; set; }
        public Email[] PatientEmail { get; set; }
        public Phone[] PatientPhone { get; set; }
    }
}