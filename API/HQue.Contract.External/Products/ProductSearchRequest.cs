﻿using System;
using System.Collections.Generic;

namespace HQue.Contract.External
{
    public class ProductSearchRequest : BaseSearch
    {
        public ProductSearchRequest()
        {
            TemplateDetails = new List<TemplateDetail>();
        }
        public Int64 TemplateId { get; set; }
        public string TemplateName { get; set; }
        public Int64 DoctorID { get; set; }
        public Int64 ExternalID { get; set; }

        public List<TemplateDetail> TemplateDetails { get; set; }
        
    }

    public class TemplateDetail
    {
        public Int64 RowID { get; set; }
        public string Diagnostic { get; set; }
        public string MedicineType { get; set; }
        public string Medicine { get; set; }
        public decimal Quantity { get; set; }
        public decimal MRP { get; set; }
        public string Manufacturer { get; set; }
        public string MedicineGenericName { get; set; }
        public string MedicineContainer { get; set; }
        public string Dosage1 { get; set; }
        public string Dosage2 { get; set; }
        public string Dosage3 { get; set; }
        public string Dosage4 { get; set; }
        public Int64 NumberofDays { get; set; }
        public bool BeforeAfter { get; set; }
        public string PharmaWrkFlowStatusID { get; set; }
        public Int64 TemplateID { get; set; }
        public string TemplateName { get; set; }
    }
}
