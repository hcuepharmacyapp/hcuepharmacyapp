﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace HQue.Contract.External
{
    /// <summary>
    /// Contract for External API - Products search results
    /// </summary>
    public class ProductStockResult
    {
        [JsonProperty(PropertyName = "Medicine")]
        public string   Medicine { get; set; }

        [JsonProperty(PropertyName = "GeneralName")]
        public string   GeneralName { get; set; }

        [JsonProperty(PropertyName = "Strength")]
        public string   Strength { get; set; }

        [JsonProperty(PropertyName = "MedicineType")]
        public string   MedicineType { get; set; }

        [JsonProperty(PropertyName = "Manufacturer")]
        public string   Manufacturer { get; set; }

        [JsonProperty(PropertyName = "Container")]
        public string   Container { get; set;  }

        [JsonProperty(PropertyName = "Stock")]
        public decimal  Stock { get; set; }

        [JsonProperty(PropertyName = "MRP")]
        public decimal  MRP { get; set; }

        //public string BatchNo { get; set; }
        //public DateTime ExpireDate { get; set; }
    }
}
