﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HQue.Contract.External.Sync
{
    public class ExportDataRequest
    {
        public string AccountID { get; set;  }
        public string InstanceID { get; set; }
        public int SeedIndex { get; set; }
    }
}
