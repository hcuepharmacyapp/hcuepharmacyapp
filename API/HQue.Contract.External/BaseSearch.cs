namespace HQue.Contract.External
{
    public class BaseSearch
    {
        public BaseSearch()
        {
            PageSize = 10;
            PageNumber = 1;
            Sort = "";
            //Sort = "asc";         
        }

        public int PageSize { get; set; }
        public int PageNumber { get; set; }
        public string Sort { get; set; }       
    }
}