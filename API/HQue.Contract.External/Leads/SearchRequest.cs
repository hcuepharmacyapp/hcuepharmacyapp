﻿using System;

namespace HQue.Contract.External.Leads
{
    public class SearchRequest : BaseSearch
    {
        public string Status { get; set; }
        public Int64 PatientCaseID { get; set; }
        public Int64 PharmaID { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
    }
}
