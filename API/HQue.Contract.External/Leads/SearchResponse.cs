namespace HQue.Contract.External.Leads
{
    public class SearchResponse
    {
        public SearchResultRows[] Rows { get; set; }

        public int count { get; set; }
    }
}