﻿using System;

namespace HQue.Contract.External.Leads
{
    public class UpdateRequest : BaseSearch
    {
        public string Status { get; set; }
        public string USRType { get; set; }
        public string RowID { get; set; }
        public Int64 PatientCaseID { get; set; }
        public int USRId { get; set; }
    }
}
