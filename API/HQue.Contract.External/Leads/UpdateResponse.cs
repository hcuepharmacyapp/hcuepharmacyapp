namespace HQue.Contract.External.Leads
{
    public class UpdateResponse
    {
        public int RowID { get; set; }
        //public int PharmaWrkFlowStatusID { get; set; }
        public int PatientCaseID { get; set; }
    }
}