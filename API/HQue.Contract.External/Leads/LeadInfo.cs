﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HQue.Contract.External.Leads
{
    public class LeadInfo
    {
        public string InstanceId { get; set; }
        public string LeadId { get; set; }
        public string PatientName { get; set; }
        public string Mobile { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
        public string Pincode { get; set; }
        public decimal Latitude { get; set; }
        public decimal Longitude { get; set; }
        public string DeliveryMode { get; set; }
    }
}
