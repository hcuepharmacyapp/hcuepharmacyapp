﻿using HQue.Contract.External.Common;

namespace HQue.Contract.External.Leads
{
    public class SearchResultRows
    {
        public PatientDetails PatientDetails { get; set; }
        public DoctorDetails DoctorDetails { get; set; }
        public ExtLeadsPrescription[] PatientPrescription { get; set; }
        public string requestSubmitDate { get; set; }  
    }
    public class PatientDetails
    {
        public ExtPatient Patient { get; set; }
        public Address[] PatientAddress { get; set; }
        public Phone[] PatientPhone { get; set; }
    }
    public class DoctorDetails
    {
        public ExtDoctor Doctor { get; set; }
    }
}
