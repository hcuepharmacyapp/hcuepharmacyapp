namespace HQue.Contract.External.Patient
{
    public class SearchResponse
    {
        public SearchResultRows[] rows { get; set; }
        public int count { get; set; }
    }
}