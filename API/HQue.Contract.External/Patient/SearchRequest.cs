﻿namespace HQue.Contract.External.Patient
{
    public class SearchRequest : BaseSearch
    {
        public string PhoneNumber { get; set; }
    }
    public class SearchPatientRequest : BaseSearch
    {
        public string FirstName { get; set; }
    }
}
