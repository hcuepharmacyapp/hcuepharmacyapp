﻿namespace HQue.Contract.External.Doctor
{
    public class SearchRequest : BaseSearch
    {
        public string DoctorName { get; set; }
        //public string DayCD { get; set; }
        //public string SpecialityID { get; set; }
        //public string Gender { get; set; }
        public int Count { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public int Radius { get; set; }
    }
}
