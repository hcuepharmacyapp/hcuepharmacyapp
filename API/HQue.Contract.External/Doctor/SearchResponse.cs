namespace HQue.Contract.External.Doctor
{
    public class SearchResponse
    {
        public SearchResultRows[] rows { get; set; }
        public int count { get; set; }
    }
}