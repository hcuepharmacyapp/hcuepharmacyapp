﻿using HQue.Contract.External.Common;

namespace HQue.Contract.External.Doctor
{
    public class SearchResultRows
    {
        public ExtDoctor[] Doctor { get; set; }
    }
}
