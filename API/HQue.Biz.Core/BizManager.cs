﻿using System;
using System.Security.Claims;
using System.Threading.Tasks;
using HQue.Contract.Base;
using HQue.DataAccess;
using Microsoft.AspNetCore.Http;
using Utilities.Helpers;

namespace HQue.Biz.Core
{
    public class BizManager
    {
        protected readonly BaseDataAccess DataAccess;
        private ClaimsPrincipal _user;

        public BizManager(BaseDataAccess dataAccess)
        {
            DataAccess = dataAccess;
        }

        public BizManager(BaseDataAccess dataAccess, IHttpContextAccessor httpContextAccessor)
        {
            DataAccess = dataAccess;
            _user = httpContextAccessor.HttpContext.User;
        }

        public async Task<bool> ConnectionTest()
        {
            return await DataAccess.ConnectionTest();
        }

        protected virtual async Task<T> Save<T>(T data) where T : IContract
        {
            return await DataAccess.Save(data);
        }

        protected void SetMetaData(IContract from, IContract to)
        {
            to.AccountId = from.AccountId;
            to.InstanceId = from.InstanceId;
            to.CreatedBy = from.CreatedBy;
            to.UpdatedBy = from.UpdatedBy;
            to.OfflineStatus = from.OfflineStatus;
            if (from.WriteExecutionQuery == true)
            {
                var executionQuery = from.GetExecutionQuery();
                if (executionQuery != null)
                {
                    to.SetExecutionQuery(executionQuery, from.WriteExecutionQuery);
                }
            }
        }

        protected void SetMetaData(IContract data)
        {
            if(_user == null)
                throw new Exception("User context not set, use the overloaded constructor that accepts user context");

            data.AccountId = _user.AccountId();
            data.InstanceId = _user.InstanceId();
            data.CreatedBy = _user.Identity.Id();
            data.UpdatedBy = _user.Identity.Id();
            data.OfflineStatus = Convert.ToBoolean(_user.OfflineStatus());
        }

        public Task<bool> GetFeatureAccess(int GroupId)
        {
            return DataAccess.GetFeatureAccess(GroupId);
        }
    }
}
