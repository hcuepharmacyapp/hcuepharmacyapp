﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using HQue.Contract.Infrastructure.Master;
using HQue.DataAccess.Master;
using System.Linq;
using HQue.Biz.Core.Master;

namespace HQue.Biz.Core.ETL
{
    public class ImportCustomer
    {
        private readonly PatientDataAccess _patientDataAccess;       
        private readonly PatientManager _patientManager;

        private string _instanceId = string.Empty;
        private string _accountId = string.Empty;
        private string _userId;

        public ImportCustomer(PatientDataAccess patientDataAccess, PatientManager patientManager)
        {
            _patientDataAccess = patientDataAccess;
            _patientManager = patientManager;
        }

        public async Task<List<int>> ImportData(Stream stream, string instanceId, string accountId, string userId)
        {
            _instanceId = instanceId;
            _accountId = accountId;
            _userId = userId;
            try
            {
                var tupleStocks = await Task.Run(() => GetCustomers(stream));
                var patient = tupleStocks.Item1;
                var missedStocks = tupleStocks.Item2;
                if (missedStocks.Count == 0)
                {
                    var import = await ImportData(patient);
                }
                return missedStocks;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private Tuple<List<Patient>, List<int>> GetCustomers(Stream stream)
        {
            var isFirstRow = true;
            var patient = new List<Patient>();
            var missedItems = new List<int>();

            var sr = new StreamReader(stream);
            int i = 0;
            while (!sr.EndOfStream)
            {
                var item = sr.ReadLine();
                var split = item.Split(',');
               
                if (isFirstRow)
                {
                    isFirstRow = false;
                    i++;
                    continue;
                }

                var ps = new Patient
                {
                    Name = split[0],
                    Mobile = split[1],
                    Email = split[2],
                    DOB = Convert.ToDateTime(split[3]),
                    Gender = split[4],
                    Age = 0,
                    Address = split[5],
                    Area = split[6],
                    City = split[7],
                    State = split[8],
                    Pincode = split[9],
                    EmpID = split[10],
                    IsSync=1,
                    Discount = Convert.ToDecimal(split[11])
                };
                i++;
                if (ps.Name!=null || ps.Mobile !=null) //Newly modified to validate the Cost is less than MRP
                {
                    patient.Add(ps);
                }
                else
                {
                    missedItems.Add(i);
                }
            }

            //return productStock;
            return new Tuple<List<Patient>, List<int>>(patient, missedItems);
        }

        private async Task<bool> ImportData(IEnumerable<Patient> patient)
        {
            var prod = patient.GroupBy(p => new { p.Name, p.Mobile, p.Email, p.DOB, p.Gender, p.Address, p.Area, p.City, p.State, p.Pincode, p.EmpID })
                                       .Select(p => p.FirstOrDefault());

            foreach (var patientItem in prod)
            {                
                await SavePatient(patientItem);              
            }
            return true;
        }

        private async Task<bool> SavePatient(Patient patient)
        {            
            patient.InstanceId = _instanceId;
            patient.AccountId = _accountId;
            patient.CreatedBy = _userId;
            patient.UpdatedBy = _userId;
            await _patientDataAccess.Save(patient);
            return true;
        }      

    }
}
