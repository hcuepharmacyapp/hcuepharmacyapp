﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using HQue.Contract.Infrastructure.Inventory;
using HQue.Contract.Infrastructure.Master;
using HQue.DataAccess.Inventory;
using HQue.DataAccess.Master;
using Utilities.Logger;
using HQue.Biz.Core.Accounts;
using System.Linq;
using HQue.DataAccess.ETL;
namespace HQue.Biz.Core.ETL
{

    public class Import
    {



        private readonly ProductDataAccess _productDataAccess;
        private readonly ProductStockDataAccess _productStockDataAccess;
        private readonly VendorPurchaseDataAccess _vendorPurchaseDataAccess;
    
        private readonly VendorDataAccess _vendorDataAccess;
        private readonly PaymentManager _paymentManager;
        private readonly ETLDataAccess _etlDataAccess;
        private string _instanceId = string.Empty;
        private string _accountId = string.Empty;
        private string _userId;

        public Import(ProductDataAccess productDataAccess, ProductStockDataAccess productStockDataAccess, VendorPurchaseDataAccess vendorPurchaseDataAccess, VendorDataAccess vendorDataAccess, PaymentManager paymentManager, ETLDataAccess etlDataAccess )
        {
            _productDataAccess = productDataAccess;
            _productStockDataAccess = productStockDataAccess;
            _vendorPurchaseDataAccess = vendorPurchaseDataAccess;
            _vendorDataAccess = vendorDataAccess;
            _paymentManager = paymentManager;
            _etlDataAccess = etlDataAccess;
       
        }
        public async Task<List<int>> ImportData(Stream stream, string instanceId, string accountId, string userId)
        {
            _instanceId = instanceId;
            _accountId = accountId;
            _userId = userId;
            try
            {
                var tupleStocks = await Task.Run(() => GetProductStock(stream));
                var productStocks = tupleStocks.Item1;
                var missedStocks = tupleStocks.Item2;
                if (missedStocks.Count == 0)
                {
                    var import = await ImportData(productStocks, _instanceId, _accountId);
                }
                return missedStocks;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<bool> ImportStockOnline(Stream stream, string instanceId, string accountId, string userId, int isCloud, string fileName)
        {
            _instanceId = instanceId;
            _accountId = accountId;
            _userId = userId;
            try
            {

                var tupleStocks = await Task.Run(() => GetProductStock(stream));
                var productStock = tupleStocks.Item1;

                await _productStockDataAccess.ImportStockOnline(productStock, instanceId, accountId, userId, isCloud, fileName);

                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task ImportBuyData(Stream stream, string instanceId, string accountId, string userId)
        {
            _instanceId = instanceId;
            _accountId = accountId;
            _userId = userId;
            var vendorPurchase = await Task.Run(() => GetVendorPurchaseItem(stream));
            await SaveVendorPurchase(vendorPurchase);
            await ImportBuyData(vendorPurchase);
            await _paymentManager.SaveVendorPurchasePayment(vendorPurchase);
        }

        public async Task ImportHistoricalPurchases(string fileName, string instanceId, string accountId, string userId)
        {
            await _etlDataAccess.ImportHistoricalPurchases(accountId, accountId, fileName, 1, userId);
        }

        public async Task ImportSalesPurchases(string fileName, string instanceId, string accountId, string userId)
        {
            await _etlDataAccess.ImportHistoricalSales(accountId, instanceId, fileName, 1, userId);
        }

        public async Task ImportVendorMaster(string fileName, string instanceId, string accountId, string userId)
        {
            await _etlDataAccess.ImportVendorMaster(accountId, instanceId, fileName, 1, userId);
        }

        public async Task ImportProductMaster(string fileName, string instanceId, string accountId, string userId)
        {
            await _etlDataAccess.ImportProductMaster(accountId, instanceId, fileName, 1);
        }
        /// <summary>
        /// Offline Product sync to Online
        /// </summary>
        /// <param name="fileName"></param>
        /// <param name="accountId"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public async Task SyncProductToOnline(string fileName, string accountId, string userId)
        {
            await _etlDataAccess.SyncProductToOnline(accountId, fileName, 1, userId);
        }
        /// <summary>
        /// Bulk Import Method added by Poongodi on 02/08/2017
        /// </summary>
        /// <param name="fileName"></param>
        /// <param name="instanceId"></param>
        /// <param name="accountId"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public async Task ImportProductStock(string fileName, string instanceId, string accountId, string userId)
        {
            await _etlDataAccess.ImportProductStock(accountId, instanceId, fileName, 1, userId);
        }

        private Tuple<List<ProductStock>, List<int>> GetProductStock(Stream stream)
        {

            var isFirstRow = true;
            var productStock = new List<ProductStock>();
            var missedItems = new List<int>();
            var ProductStockItem = new ProductStock();
            string errorMessage = "";
            decimal result;
            DateTime result1;
            decimal priceAfterTax = 0;
            decimal FreeQty = 0;
            var sr = new StreamReader(stream);
            int i = 0;

            while (!sr.EndOfStream)
            {
                var item = sr.ReadLine();
                if (sr.EndOfStream && isFirstRow)
                {
                    errorMessage = "Please fill product details in Import file";
                    throw new Exception(errorMessage);
                }
                var split = item.Split(',');              
               //added by nandhini for stock template header validation 07/08/2017
                if (isFirstRow)
                {
                    if (split[Convert.ToInt32(fields.ProductName)]!= fields.ProductName.ToString())
                    {
                          errorMessage = "Invalid ProductName Position ";
                        throw new Exception(errorMessage);
                    }
                    if (split[Convert.ToInt32(fields.HsnCode)] !=fields.HsnCode.ToString())
                    {
                        errorMessage = "Invalid HsnCode Position ";
                        throw new Exception(errorMessage);
                    }
                     if(split[Convert.ToInt32(fields.GstTotal)] != fields.GstTotal.ToString())
                    {
                        errorMessage = "Invalid GstTotal Position ";
                        throw new Exception(errorMessage);
                    }
                    if (split[Convert.ToInt32(fields.ExpireDate)] != "ExpireDate(dd-MMM-yyyy)")
                    {
                        errorMessage = "Invalid ExpireDate Position ";
                        throw new Exception(errorMessage);
                    }

                    if (split[Convert.ToInt32(fields.BatchNo)] != fields.BatchNo.ToString())
                    {
                        errorMessage = "Invalid BatchNo Position ";
                        throw new Exception(errorMessage);
                    }
                    
                       if(split[Convert.ToInt32(fields.PackageSize)] != "Units/Strip")
                    {
                        errorMessage = "Invalid Units/Strip Position ";
                        throw new Exception(errorMessage);
                    }
                    if (split[Convert.ToInt32(fields.PackagePurchasePrice)] != "Price/Strip")
                    {
                        errorMessage = "Invalid Price/Strip Position ";
                        throw new Exception(errorMessage);
                    }
                    if (split[Convert.ToInt32(fields.SellingPrice)] != "MRP/Strip")
                    {
                        errorMessage = "Invalid MRP/Strip Position ";
                        throw new Exception(errorMessage);
                    }
                    if (split[Convert.ToInt32(fields.Eancode)] != "Barcode")
                    {
                        errorMessage = "Invalid Barcode Position ";
                        throw new Exception(errorMessage);
                    }
                    if (split[Convert.ToInt32(fields.Stock)] != "Stock")
                    {
                        errorMessage = "Invalid Stock Position ";
                        throw new Exception(errorMessage);

                    }
                    if (split[Convert.ToInt32(fields.FreeQty)] != "FreeQty")

                    {
                        errorMessage = "Invalid FreeQty Position ";
                        throw new Exception(errorMessage);
                    }
                    if (split[Convert.ToInt32(fields.Name)] != "Vendor Name")

                    {
                        errorMessage = "Invalid VendorName Position ";
                        throw new Exception(errorMessage);
                    }
                    if (split[Convert.ToInt32(fields.Category)] != fields.Category.ToString())

                    {
                        errorMessage = "Invalid Category Position ";    
                        throw new Exception(errorMessage);
                    }
                    if (split[Convert.ToInt32(fields.GenericName)] != "Generic Name")

                    {
                        errorMessage = "Invalid GenericName Position ";
                        throw new Exception(errorMessage);
                    }
                    if (split[Convert.ToInt32(fields.Schedule)] != fields.Schedule.ToString())

                    {
                        errorMessage = "Invalid Schedule Position ";
                        throw new Exception(errorMessage);
                    }
                    if (split[Convert.ToInt32(fields.Type)] != fields.Type.ToString())

                    {
                        errorMessage = "Invalid Type Position ";
                        throw new Exception(errorMessage);
                    }
                    if (split[Convert.ToInt32(fields.Manufacturer)] != fields.Manufacturer.ToString())

                    {
                        errorMessage = "Invalid Manufacturer Position ";
                        throw new Exception(errorMessage);
                    }
                    if (split[Convert.ToInt32(fields.RackNo)] != fields.RackNo.ToString())

                    {
                        errorMessage = "Invalid RackNo Position ";
                        throw new Exception(errorMessage);
                    }
                    if (split[Convert.ToInt32(fields.BoxNo)] != fields.BoxNo.ToString())

                    {
                        errorMessage = "Invalid BoxNo Position ";
                        throw new Exception(errorMessage);
                    }
                    
                    isFirstRow = false;
                    i++;
                    continue;
                }
                //end
                //add by nandhini 4/7/17
                if (split[Convert.ToInt32(fields.ProductName)] != "")
                {
                    ProductStockItem.productName = Convert.ToString(split[Convert.ToInt32(fields.ProductName)]);
                }

                    if (split[Convert.ToInt32(fields.ProductName)] == "")
                {
                    errorMessage = "ProductName is Mandatory " + " at Row# " + Convert.ToString(i + 1);
                    throw new Exception(errorMessage);
                }
                if (split[Convert.ToInt32(fields.HsnCode)] == "")
                {
                    errorMessage = "Please fill the HsnCode to import file  for the Product " + Convert.ToString(split[Convert.ToInt32(fields.ProductName)]) + " and  Row# " + Convert.ToString(i + 1);
                    throw new Exception(errorMessage);

                }
                if (split[Convert.ToInt32(fields.HsnCode)] != "")
                {
                    if (decimal.TryParse(split[Convert.ToInt32(fields.HsnCode)], out result))
                    {
                        ProductStockItem.HsnCode = Convert.ToString(split[Convert.ToInt32(fields.HsnCode)]);
                    }
                }
                

                //end
                if (split[Convert.ToInt32(fields.GstTotal)] != "")
                {
                    if (decimal.TryParse(split[Convert.ToInt32(fields.GstTotal)], out result))
                    {
                        ProductStockItem.GstTotal = Convert.ToDecimal(split[Convert.ToInt32(fields.GstTotal)]);
                    }
                    else
                    {
                        errorMessage = "Invalid GST for the Product " + Convert.ToString(split[Convert.ToInt32(fields.ProductName)]) + " and  Row# " + Convert.ToString(i + 1);
                        throw new Exception(errorMessage);
                    }
                }
                else
                {
                    errorMessage = "GST is Mandatory for the Product " + Convert.ToString(split[Convert.ToInt32(fields.ProductName)]) + " and  Row# " + Convert.ToString(i + 1);
                    throw new Exception(errorMessage);
                }
                if (split[Convert.ToInt32(fields.ExpireDate)] != "")
                {
                    if (DateTime.TryParse(split[Convert.ToInt32(fields.ExpireDate)], out result1))
                    {
                        ProductStockItem.ExpireDate = Convert.ToDateTime(split[Convert.ToInt32(fields.ExpireDate)]);
                    }
                    else
                    {
                        errorMessage = "Invalid ExpireDate for the Product " + Convert.ToString(split[Convert.ToInt32(fields.ProductName)]) + " at  Row# " + Convert.ToString(i + 1) + "  and Check The Date Format";
                        throw new Exception(errorMessage);

                    }

                }
                if (split[Convert.ToInt32(fields.ExpireDate)] == "")
                {
                    errorMessage = "Expire Date is Mandatory for the Product " + Convert.ToString(split[Convert.ToInt32(fields.ProductName)]) + " and  Row# " + Convert.ToString(i + 1);
                    throw new Exception(errorMessage);
                }

                if (split[Convert.ToInt32(fields.BatchNo)] != "")
                {
                    ProductStockItem.BatchNo = Convert.ToString(split[Convert.ToInt32(fields.BatchNo)]);
                }
                else
                {
                    errorMessage = "Batch Number is Mandatory for the Product " + Convert.ToString(split[Convert.ToInt32(fields.ProductName)]) + " and  Row# " + Convert.ToString(i + 1);
                    throw new Exception(errorMessage);
                }

                if (split[Convert.ToInt32(fields.PackageSize)] != "")
                {
                    if (decimal.TryParse(split[Convert.ToInt32(fields.PackageSize)], out result))
                    {
                        ProductStockItem.PackageSize = Convert.ToDecimal(split[Convert.ToInt32(fields.PackageSize)]);
                    }
                    else
                    {
                        errorMessage = "Invalid Units/Strip Number for the Product " + Convert.ToString(split[Convert.ToInt32(fields.ProductName)]) + " and  Row# " + Convert.ToString(i + 1);
                        throw new Exception(errorMessage);
                    }
                }
                else
                {
                    errorMessage = "Units/Strip is Mandatory for the Product " + Convert.ToString(split[Convert.ToInt32(fields.ProductName)]) + " and  Row# " + Convert.ToString(i + 1);
                    throw new Exception(errorMessage);
                }
                if (ProductStockItem.PackageSize == 0)
                {
                    errorMessage = "Units/Strip should be greaterthan 0 for the Product " + Convert.ToString(split[Convert.ToInt32(fields.ProductName)]) + " and  Row# " + Convert.ToString(i + 1);
                    throw new Exception(errorMessage);
                }

                if (split[Convert.ToInt32(fields.PackagePurchasePrice)] != "")
                {
                    if (decimal.TryParse(split[Convert.ToInt32(fields.PackagePurchasePrice)], out result))
                    {
                        ProductStockItem.PackagePurchasePrice = Convert.ToDecimal(split[Convert.ToInt32(fields.PackagePurchasePrice)]);
                    }

                    else
                    {
                        errorMessage = "Invalid Price/Strip Number for the Product " + Convert.ToString(split[Convert.ToInt32(fields.ProductName)]) + " and  Row# " + Convert.ToString(i + 1);
                        throw new Exception(errorMessage);
                    }
                }
                else
                {
                    errorMessage = "Price/Strip is Mandatory " + Convert.ToString(split[Convert.ToInt32(fields.ProductName)]) + " and  Row# " + Convert.ToString(i + 1);
                    throw new Exception(errorMessage);
                }
                if (ProductStockItem.PackagePurchasePrice == 0)
                {
                    errorMessage = "Price/Strip should be greaterthan 0 for the Product  " + Convert.ToString(split[Convert.ToInt32(fields.ProductName)]) + " and  Row# " + Convert.ToString(i + 1);
                    throw new Exception(errorMessage);
                }
                if (split[Convert.ToInt32(fields.SellingPrice)] != "")
                {
                    if (decimal.TryParse(split[Convert.ToInt32(fields.SellingPrice)], out result))
                    {
                        ProductStockItem.SellingPrice = Convert.ToDecimal(split[Convert.ToInt32(fields.SellingPrice)]);
                        //if (ProductStockItem.PackagePurchasePrice > (ProductStockItem.SellingPrice + (ProductStockItem.GstTotal / 100)))
                            if (ProductStockItem.PackagePurchasePrice + (ProductStockItem.PackagePurchasePrice * ProductStockItem.GstTotal / 100) > ProductStockItem.SellingPrice)

                              
                            {
                            errorMessage = "MRP Price must be greater than Price + GST" + " and  Row# " + Convert.ToString(i + 1);
                            throw new Exception(errorMessage);
                        }
                    }
                    else
                    {
                        errorMessage = "Invalid MRP/Strip Number for the Product " + Convert.ToString(split[Convert.ToInt32(fields.ProductName)]) + " and  Row# " + Convert.ToString(i + 1);
                        throw new Exception(errorMessage);
                    }
                }
                else
                {
                    errorMessage = "MRP/Strip is Mandatory for the Product " + Convert.ToString(split[Convert.ToInt32(fields.ProductName)]) + " and  Row# " + Convert.ToString(i + 1);
                    throw new Exception(errorMessage);
                }
                if (ProductStockItem.SellingPrice == 0)
                {

                    errorMessage = "MRP/Strip should be greaterthan 0 for the Product " + Convert.ToString(split[Convert.ToInt32(fields.ProductName)]) + " and  Row# " + Convert.ToString(i + 1);
                    throw new Exception(errorMessage);
                }
                if (split[Convert.ToInt32(fields.Eancode)] == "")
                {
                    errorMessage = "Please fill the Barcode to import file  for the Product " + Convert.ToString(split[Convert.ToInt32(fields.ProductName)]) + " and  Row# " + Convert.ToString(i + 1);
                    throw new Exception(errorMessage);

                }


                if (split[Convert.ToInt32(fields.Stock)] != "")
                {
                    if (decimal.TryParse(split[Convert.ToInt32(fields.Stock)], out result))
                    {
                        ProductStockItem.Stock = Convert.ToDecimal(split[Convert.ToInt32(fields.Stock)]);
                    }
                    else
                    {
                        errorMessage = "Invalid Stock Number for the Product " + Convert.ToString(split[Convert.ToInt32(fields.ProductName)]) + " and  Row# " + Convert.ToString(i + 1);
                        throw new Exception(errorMessage);
                    }
                }
                else
                {
                    errorMessage = "Stock is Mandatory for the Product " + Convert.ToString(split[Convert.ToInt32(fields.ProductName)]) + " and  Row# " + Convert.ToString(i + 1);
                    throw new Exception(errorMessage);
                }

                if (split[Convert.ToInt32(fields.FreeQty)] != "")
                {
                    if (decimal.TryParse(split[Convert.ToInt32(fields.FreeQty)], out result))
                    {
                        FreeQty = Convert.ToDecimal(split[Convert.ToInt32(fields.FreeQty)]);
                    }

                }
                if (split[Convert.ToInt32(fields.FreeQty)] == "")
                {
                    errorMessage = "Please fill the FreeQty to import file  for the Product " + Convert.ToString(split[Convert.ToInt32(fields.ProductName)]) + " and  Row# " + Convert.ToString(i + 1);
                    throw new Exception(errorMessage);
                }
                if (split[Convert.ToInt32(fields.Name)] == "")
                {
                    errorMessage = "Vendor Name is Mandatory for the Product " + Convert.ToString(split[Convert.ToInt32(fields.ProductName)]) + " and  Row# " + Convert.ToString(i + 1);
                    throw new Exception(errorMessage);

                }
                if (split[Convert.ToInt32(fields.Category)] == "")
                {
                    errorMessage = "Please fill the Category to import file  for the Product " + Convert.ToString(split[Convert.ToInt32(fields.ProductName)]) + " and  Row# " + Convert.ToString(i + 1);
                    throw new Exception(errorMessage);
                }

                if (split[Convert.ToInt32(fields.GenericName)] == "")
                {
                    errorMessage = "Please fill the GenericName to import file  for the Product " + Convert.ToString(split[Convert.ToInt32(fields.ProductName)]) + " and  Row# " + Convert.ToString(i + 1);
                    throw new Exception(errorMessage);
                }
                if (split[Convert.ToInt32(fields.Schedule)] == "")
                {
                    errorMessage = "Please fill the Schedule to import file  for the Product " + Convert.ToString(split[Convert.ToInt32(fields.ProductName)]) + " and  Row# " + Convert.ToString(i + 1);
                    throw new Exception(errorMessage);
                }
                if (split[Convert.ToInt32(fields.Type)] == "")
                {
                    errorMessage = "Please fill the Type to import file  for the Product " + Convert.ToString(split[Convert.ToInt32(fields.ProductName)]) + " and  Row# " + Convert.ToString(i + 1);
                    throw new Exception(errorMessage);

                }
                if (split[Convert.ToInt32(fields.Manufacturer)] == "")
                {
                    errorMessage = "Please fill the Manufacturer to import file  for the Product " + Convert.ToString(split[Convert.ToInt32(fields.ProductName)]) + " and  Row# " + Convert.ToString(i + 1);
                    throw new Exception(errorMessage);
                }
                if (split[Convert.ToInt32(fields.RackNo)] == "")
                {
                    errorMessage = "Please fill the RackNo to import file  for the Product " + Convert.ToString(split[Convert.ToInt32(fields.ProductName)]) + " and  Row# " + Convert.ToString(i + 1);
                    throw new Exception(errorMessage);
                }
                if (split[Convert.ToInt32(fields.BoxNo)] == "")
                {
                    errorMessage = "Please fill the BoxNo to import file  for the Product " + Convert.ToString(split[Convert.ToInt32(fields.ProductName)]) + " and  Row# " + Convert.ToString(i + 1);
                    throw new Exception(errorMessage);
                }
                if (ProductStockItem.VAT == null)
                    ProductStockItem.VAT = 0;
                ProductStockItem.PurchasePrice = (((ProductStockItem.PackagePurchasePrice / ProductStockItem.PackageSize) * ProductStockItem.GstTotal) / 100) + (ProductStockItem.PackagePurchasePrice / ProductStockItem.PackageSize);
                //ProductStockItem.PurchasePrice = (priceAfterTax / ProductStockItem.packageQty) / ProductStockItem.PackageSize;
                ProductStockItem.PurchasePrice = Math.Round((decimal)ProductStockItem.PurchasePrice, 5);
                ProductStockItem.SellingPrice = Math.Round(Convert.ToDecimal(ProductStockItem.SellingPrice / ProductStockItem.PackageSize), 5);

                var ps = new ProductStock
                {
                    Product =
                    {
                        Name = split[Convert.ToInt32(fields.ProductName)].Trim(),
                        Category = split[Convert.ToInt32(fields.Category)].Trim(),
                        GenericName = split[Convert.ToInt32(fields.GenericName)].Trim(),
                        Schedule = split[Convert.ToInt32(fields.Schedule)].Trim(),
                        Type = split[Convert.ToInt32(fields.Type)].Trim(),
                        //CommodityCode = split[Convert.ToInt32(fields.CommodityCode)].Trim(),
                        Manufacturer = split[Convert.ToInt32(fields.Manufacturer)].Trim(),
                        RackNo  = split.Length > Convert.ToInt32(fields.RackNo) ? split[Convert.ToInt32(fields.RackNo)].Trim() : "", //Updated by Settu to include boxno
                        BoxNo  = split.Length > Convert.ToInt32(fields.BoxNo) ? split[Convert.ToInt32(fields.BoxNo)].Trim() : "",
                   HsnCode = Convert.ToString(split[Convert.ToInt32(fields.HsnCode)]),
                   GstTotal = Convert.ToDecimal(split[Convert.ToInt32(fields.GstTotal)].Trim()),
                    Igst = Convert.ToDecimal(split[Convert.ToInt32(fields.GstTotal)].Trim()),
                    Cgst = Convert.ToDecimal(ProductStockItem.GstTotal / 2),
                    Sgst = Convert.ToDecimal(ProductStockItem.GstTotal / 2),
                    },
                    Vendor =
                    {
                        Code = "Vendor000",
                        Name =split[Convert.ToInt32(fields.Name)].Trim(),
                        AllowViewStock=true,
                        Status=1
                    },
                    VAT = ProductStockItem.VAT,
                    ExpireDate = Convert.ToDateTime(split[Convert.ToInt32(fields.ExpireDate)]),
                    BatchNo = ProductStockItem.BatchNo,
                    //SellingPrice = Convert.ToDecimal(split[4].Trim()),
                    Eancode = split[Convert.ToInt32(fields.Eancode)].Trim(),
                    Stock = ProductStockItem.Stock,
                    ImportQty = ProductStockItem.Stock,
                    // CST = Convert.ToDecimal(split[9].Trim()),
                    HsnCode = Convert.ToString(split[Convert.ToInt32(fields.HsnCode)]),
                    GstTotal = Convert.ToDecimal(split[Convert.ToInt32(fields.GstTotal)].Trim()),
                    Igst = Convert.ToDecimal(split[Convert.ToInt32(fields.GstTotal)].Trim()),
                    Cgst = Convert.ToDecimal(ProductStockItem.GstTotal / 2),
                    Sgst = Convert.ToDecimal(ProductStockItem.GstTotal / 2),
                    //split[8] - FreeQty is not already handled in this code
                    PackageSize = ProductStockItem.PackageSize,
                    PackagePurchasePrice = ProductStockItem.PackagePurchasePrice,
                    PurchasePrice = ProductStockItem.PurchasePrice,
                    SellingPrice = ProductStockItem.SellingPrice,
                    MRP = ProductStockItem.SellingPrice,
                    //added by nandhini for importdate
                    ImportDate = DateTime.UtcNow.Date,
                };
                i++;
                if (ps.PackagePurchasePrice <= Convert.ToDecimal(split[Convert.ToInt32(fields.SellingPrice)])) //Newly modified to validate the Cost is less than MRP
                {
                    productStock.Add(ps);
                }
                else
                {
                    missedItems.Add(i);
                }
            }
            //return productStock;
            return new Tuple<List<ProductStock>, List<int>>(productStock, missedItems);
        }


        private async Task<bool> ImportData(IEnumerable<ProductStock> productStocks, string instanceId, string accountId)
        {
            var prod = productStocks.GroupBy(p => new { p.Product.Name, p.VAT, p.ExpireDate, p.BatchNo, p.MRP, p.Stock })
                                       .Select(p => p.FirstOrDefault());
            int i = 1221;
            foreach (var productStock in prod)
            {
                i++;
                if (i == 1250)
                {
                    int j = 1;
                }
                if (i == 1285)
                {
                    int j = 1;
                }
                if (i == 1306)
                {
                    int j = 1;
                }
                //by San
                productStock.Product.AccountId = accountId;
                productStock.Product.InstanceId = instanceId;
                var uniqueCheck = await _productDataAccess.CheckUniqueProduct(productStock.Product);
                productStock.Vendor.InstanceId = instanceId;
                productStock.Vendor.Mobile1checked = false;
                productStock.Vendor.Mobile2checked = false;
                productStock.Vendor.AccountId = accountId; //Added by Poongodi to validate Accountwise vendor on 02/06/17
                var vendorList = await _vendorDataAccess.CheckUniqueVendor(productStock.Vendor);
                Product product;
                if (uniqueCheck == true)
                {
                    product = await SaveProduct(productStock.Product);
                }
                else
                {
                    product = await _productDataAccess.GetImportProductById(productStock.Product);
                    /*Modified by Poongodi on 09/06/2017*/
                    /*Added by Settu To update rack/box number to the instance table*/
                    //if (productStock.Product.RackNo != null && productStock.Product.RackNo.Trim() != "" ||
                    //    productStock.Product.BoxNo != null && productStock.Product.BoxNo.Trim() != "")
                    //{
                    //    productStock.Product.UpdatedBy = _userId;
                    //    productStock.Product.Id = product.Id;
                    //    await _productDataAccess.UpdateProductRackNo(productStock.Product);
                    //}
                    productStock.Product.UpdatedBy = _userId;
                    productStock.Product.Id = product.Id;
                    await _productDataAccess.UpdateProductRackNo(productStock.Product);
                }
                Vendor vendor;
                if (vendorList.Count == 0)
                {
                    productStock.Vendor.Name = productStock.Vendor.Name.ToString();
                    vendor = await SaveVendor(productStock.Vendor);
                }
                else
                {
                    vendor = vendorList.First();
                }
                await SaveProductStock(productStock, product, vendor);
                //if (uniqueCheck == true)
                //{
                //    var product = await SaveProduct(productStock.Product);
                //    await SaveProductStock(productStock, product);
                //}
                //else
                //{
                //    var product = await _productDataAccess.GetByProductId(productStock.Product);
                //    await SaveProductStock(productStock, product);
                //}
            }
            return true;
        }
        private async Task<bool> SaveProductStock(ProductStock productStock, Product product, Vendor vendor)
        {
            productStock.ProductId = product.Id;
            productStock.VendorId = vendor.Id;
            productStock.InstanceId = _instanceId;
            productStock.AccountId = _accountId;
            productStock.CreatedBy = _userId;
            productStock.UpdatedBy = _userId;
            productStock.StockImport = true;
            await _productStockDataAccess.Save(productStock);
            return true;
        }
        private async Task<Product> SaveProduct(Product product)
        {
            product.AccountId = _accountId;
            product.InstanceId = _instanceId;
            product.CreatedBy = _userId;
            product.UpdatedBy = _userId;
            ProductMaster pm = new ProductMaster();
            //product.Name = product.Name;
            product.ProductInstance.RackNo = product.RackNo;
            //var count = await _productDataAccess.TotalProductCount(product);
            product.Name = product.Name.ToUpper();
            //if (product.Name.Length >= 2)
            //{
            //    var getChar = product.Name.Substring(0, 2);
            //    product.Code = (getChar.ToUpper() + (count + 1)).ToString();
            //}
            //else
            //{
            //    product.Code = product.Name.ToUpper();
            //}
            pm.Name = product.Name;
            var getProductMasterId = await _productDataAccess.GetProductMasterById(pm);
            if (!string.IsNullOrEmpty(getProductMasterId.Id))
                product.ProductMasterID = getProductMasterId.Id;
            var obj = await _productDataAccess.SaveImportProduct(product);
            await _productDataAccess.SaveProductInstance(obj);
            return obj;
        }
        private async Task<Vendor> SaveVendor(Vendor vendor)
        {
            vendor.AccountId = _accountId;
            vendor.InstanceId = _instanceId;
            vendor.CreatedBy = _userId;
            vendor.UpdatedBy = _userId;
            var obj = await _vendorDataAccess.Save(vendor);
            return obj;
        }
        /* public async Task ImportBuyData(Stream stream, string instanceId, string accountId, string userId)
         {
             _instanceId = instanceId;
             _accountId = accountId;
             _userId = userId;
             var vendorPurchase = await Task.Run(() => GetVendorPurchaseItem(stream));
             await SaveVendorPurchase(vendorPurchase);
             await ImportBuyData(vendorPurchase);
             await _paymentManager.SaveVendorPurchasePayment(vendorPurchase);
         }
         */
        private async Task<VendorPurchase> GetVendorPurchaseItem(Stream stream)
        {
            var isFirstRow = true;
            var isSecondRow = true;
            var isThirdRow = true;
            var vendorPurchase = new VendorPurchase();
            var vendorPurchaseItemList = new List<VendorPurchaseItem>();
            string errorMessage = "";
            var sr = new StreamReader(stream);
            while (!sr.EndOfStream)
            {
                var item = sr.ReadLine();
                var split = item.Split(',');
                //[0]ProductName,[1]VAT,[2]ExpireDate(dd/mm/yyyy),[3]BatchNo,[4]MRP,[5]Barcode,[6]Stock
                if (isFirstRow)
                {
                    isFirstRow = false;
                    continue;
                }
                // Vendor Purchase data
                if (isSecondRow)
                {
                    //check if vendor name exists
                    Vendor vendor = new Vendor();
                    vendor.Name = split[0];
                    vendor = await _vendorDataAccess.GetVendorId(vendor);
                    try
                    {
                        if (vendor == null)
                        {
                            errorMessage = "Vendor name doesn't exist";
                            throw new ArgumentException(errorMessage);
                        }
                        if (split[1] == "")
                        {
                            errorMessage = "Invoice Number is Mandatory";
                            throw new ArgumentException(errorMessage);
                        }
                        if (split[4] == "")
                        {
                            errorMessage = "Amount is Mandatory";
                            throw new ArgumentException(errorMessage);
                        }
                        vendorPurchase.VendorId = vendor.Id;
                        vendorPurchase.InvoiceNo = split[1];
                        vendorPurchase.InvoiceDate = Convert.ToDateTime(split[2]);
                        if (split[3] != "")
                        {
                            vendorPurchase.Discount = Convert.ToDecimal(split[3]);
                        }
                        vendorPurchase.Credit = Convert.ToDecimal(split[4]);
                        // vp = await SaveVendorPurchase(vendorPurchase);
                        isSecondRow = false;
                        continue;
                    }
                    catch (Exception ex)
                    {
                        LogAndException.WriteToLogger((errorMessage == "" ? ex.Message : errorMessage), ErrorLevel.Error);
                        throw;
                    }
                }
                // Vendor Purchase Item data
                if (isThirdRow)
                {
                    isThirdRow = false;
                    continue;
                }
                //check product exists
                Product product = new Product();
                product.Name = split[0];
                product = await _productDataAccess.GetByProductId(product);
                try
                {
                    if (product.Id == null)
                    {
                        product.AccountId = _accountId;
                        product.InstanceId = _instanceId;
                        product.CreatedBy = _userId;
                        product.UpdatedBy = _userId;
                        product = await _productDataAccess.Save(product);
                    }
                    if (split[1] == "")
                    {
                        errorMessage = "Batch Number is Mandatory";
                        throw new ArgumentException(errorMessage);
                    }
                    var vendorPurchaseItem = new VendorPurchaseItem();
                    vendorPurchaseItem.AccountId = _accountId;
                    vendorPurchaseItem.InstanceId = _instanceId;
                    vendorPurchaseItem.CreatedBy = _userId;
                    vendorPurchaseItem.UpdatedBy = _userId;
                    //vendorPurchaseItem.VendorPurchaseId = vp.Id;
                    vendorPurchaseItem.ProductStock = new ProductStock();
                    vendorPurchaseItem.ProductStock.AccountId = _accountId;
                    vendorPurchaseItem.ProductStock.InstanceId = _instanceId;
                    vendorPurchaseItem.ProductStock.CreatedBy = _userId;
                    vendorPurchaseItem.ProductStock.UpdatedBy = _userId;
                    vendorPurchaseItem.ProductStock.VendorId = vendorPurchase.VendorId;
                    vendorPurchaseItem.ProductStock.ProductId = product.Id;
                    vendorPurchaseItem.ProductStock.BatchNo = split[1];
                    vendorPurchaseItem.ProductStock.ExpireDate = Convert.ToDateTime(split[2]);
                    decimal result;

                    if (decimal.TryParse(split[3], out result))
                    {
                        vendorPurchaseItem.ProductStock.VAT = Convert.ToDecimal(split[3]);
                    }
                    else
                    {
                        errorMessage = "Vat is Mandatory";
                        throw new ArgumentException(errorMessage);
                    }

                    ////add by nandhini for purchase import 07/07/17
                    //if (decimal.TryParse(split[3], out result))
                    //{
                    //    vendorPurchaseItem.ProductStock.GstTotal = Convert.ToDecimal(split[3]);
                    //}
                    //else
                    //{
                    //    errorMessage = "GST is Mandatory";
                    //    throw new ArgumentException(errorMessage);
                    //}
                    //if (decimal.TryParse(split[10], out result))
                    //{
                    //    vendorPurchaseItem.ProductStock.HsnCode = Convert.ToString(split[10]);
                    //}

                    ////end
                    if (decimal.TryParse(split[4], out result))
                    {
                        vendorPurchaseItem.PackageSize = Convert.ToDecimal(split[4]);
                    }
                    else
                    {
                        errorMessage = "Units/Strip is Mandatory";
                        throw new ArgumentException(errorMessage);
                    }
                    if (decimal.TryParse(split[5], out result))
                    {
                        vendorPurchaseItem.PackageQty = Convert.ToDecimal(split[5]);
                    }
                    else
                    {
                        errorMessage = "No.Strips is Mandatory";
                        throw new ArgumentException(errorMessage);
                    }
                    if (decimal.TryParse(split[6], out result))
                    {
                        vendorPurchaseItem.PackagePurchasePrice = Convert.ToDecimal(split[6]);
                    }
                    else
                    {
                        errorMessage = "Price/Strip is Mandatory";
                        throw new ArgumentException(errorMessage);
                    }
                    if (decimal.TryParse(split[7], out result))
                    {
                        vendorPurchaseItem.PackageSellingPrice = Convert.ToDecimal(split[7]);
                    }
                    else
                    {
                        errorMessage = "MRP/Strip is Mandatory";
                        throw new ArgumentException(errorMessage);
                    }
                    vendorPurchaseItemList.Add(vendorPurchaseItem);
                }
                catch (Exception ex)
                {
                    LogAndException.WriteToLogger((errorMessage == "" ? ex.Message : errorMessage), ErrorLevel.Error);
                    throw;
                }
            }
            vendorPurchase.VendorPurchaseItem = vendorPurchaseItemList;
            return vendorPurchase;
        }


        private async Task<VendorPurchase> SaveVendorPurchase(VendorPurchase vendorPurchase)
        {
            vendorPurchase.AccountId = _accountId;
            vendorPurchase.InstanceId = _instanceId;
            vendorPurchase.CreatedBy = _userId;
            vendorPurchase.UpdatedBy = _userId;
            var obj = await _vendorPurchaseDataAccess.Save(vendorPurchase);
            return obj;
        }
        private async Task<bool> ImportBuyData(VendorPurchase vendorPurchase)
        {
            var obj = await _vendorPurchaseDataAccess.Save(GetEnumerable(vendorPurchase));
            return true;
        }
        private IEnumerable<VendorPurchaseItem> GetEnumerable(VendorPurchase vendorPurchase)
        {
            foreach (var item in vendorPurchase.VendorPurchaseItem)
            {
                item.VendorPurchaseId = vendorPurchase.Id;
                yield return item;
            }

        }
        /// <summary>
        /// To Get Missed SalesITem Details added by Poongodi on 22/09/2017
        /// </summary>
        /// <param name="sAccountid"></param>
        /// <param name="sinstanceId"></param>
        /// <param name="sType"></param>
        /// <returns></returns>
        public async Task<MissedDetail> GetMissedItem(string sAccountid, string sinstanceId, string sType)
        {
           return  await _etlDataAccess.GetMissedSalesItem(sAccountid, sinstanceId,sType);
              
        }
        /// <summary>
        /// Forcefully write missed files to syncqueue added by Poongodi on 22/09/2017
        /// </summary>
        /// <param name="itemList"></param>
        /// <returns></returns>
        public async Task<bool> WriteMissedItemToSyncQue(List<MissedItemList> itemList,string sType)
        {
            foreach (var item in itemList)
            { 
                  await _etlDataAccess.WriteMissedItemToSyncQue(item, sType);
            }
            return true;
        }
    }
    //added by nandhini for stock import 05/07.2017
    public enum fields
    {

        ProductName,
        HsnCode,
        GstTotal,
        ExpireDate,
        BatchNo,
        PackageSize,
        PackagePurchasePrice,
        SellingPrice,
        Eancode,
        Stock,
        FreeQty,
        Name,
        Category,
        GenericName,
        Schedule,
        Type,
        Manufacturer,
        RackNo,
        BoxNo
    };
    //end
}
