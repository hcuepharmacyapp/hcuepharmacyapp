﻿using System.Collections.Generic;
using System.Threading.Tasks;
using HQue.Biz.Core.Extension;
using HQue.Contract.Infrastructure.Master;
using HQue.DataAccess.Master;
using HQue.Contract.Infrastructure.Inventory;
using Microsoft.AspNetCore.Http;
using HQue.Contract.Infrastructure;
using System.Linq;
using HQue.DataAccess.DbModel;

namespace HQue.Biz.Core.Master
{
    public class DoctorManager : BizManager
    {
        public DoctorManager(DoctorDataAccess dataAccess, IHttpContextAccessor httpContextAccessor) : base(dataAccess,httpContextAccessor)
        {

        }

        private DoctorDataAccess DoctorDataAccess => (DoctorDataAccess) DataAccess;

        private async Task<Doctor> Save(Doctor model)
        {
            return await DoctorDataAccess.Save(model);
        }

        public async Task<string> SaveDoctor(Doctor doctor)
        {
            var nvalue ="";
            SetMetaData(doctor);
            if (!await DoctorExists(doctor))
            {
                if (doctor.WriteExecutionQuery)
                {
                    DataAccess.SetInsertMetaData(doctor, DoctorTable.Table);
                    DataAccess.WriteInsertExecutionQuery(doctor, DoctorTable.Table);
                    nvalue = doctor.Id;
                }
                else
                {
                    var aa = await Save(doctor);
                    nvalue = aa.Id;
                }
            }
            return nvalue;
        }


        //public async Task<Doctor> SaveDoctor(Doctor doctor)
        //{
        //    SetMetaData(doctor);
        //    if (!await DoctorExists(doctor))
        //    {

        //        var aa = await DoctorDataAccess.Save(doctor);
        //        return aa;
        //    }
        //}


        private async Task<bool> DoctorExists(Doctor doctor)
        {
            var doctors = await DoctorDataAccess.CheckUniqueDoctor(doctor);
            return doctors.Count != 0;
        }

        //public Task<IEnumerable<Doctor>> GetDoctorExternal(Doctor data)
        //{
        //    return _doctorDataAccess.GetDoctorExternal(data);
        //}

        //public IEnumerable<Doctor> DoctorList(Doctor data)
        //{
        //    return _doctorDataAccess.DoctorList(data);
        //}

        public Task<IEnumerable<Doctor>> DoctorListExternal(Doctor data)
        {
            return DoctorDataAccess.DoctorListExternal(data);
        }

        public Task<IEnumerable<Doctor>> DoctorSaveExternal(Doctor data)
        {
            return DoctorDataAccess.DoctorListExternal(data);
        }

        public Task<List<Doctor>> DoctorList(string doctorName)
        {
            return DoctorDataAccess.DoctorList(doctorName);
        }

        
        public Task<List<Doctor>> LocalDoctorList(string doctorName,string accountid,string instanceid)
        {
            return DoctorDataAccess.LocalDoctorList(doctorName,accountid,instanceid);
        }

        public Task<List<Doctor>> DoctorListProfile(string doctorName, string accountid)
        {
            return DoctorDataAccess.DoctorListProfile(doctorName, accountid);
        }

        public async Task<PagerContract<Doctor>> GetDoctorList(Doctor doc)
        {

            var pager = new PagerContract<Doctor>
            {
                List = await DoctorDataAccess.GetDoctorList(doc)
            };

            if (pager.List == null || pager.List.Count() <= 0)
            {
                pager.NoOfRows = 1;
            }
            else
                pager.NoOfRows = pager.List.FirstOrDefault().DoctorCount;


            return pager;
            //return await DoctorDataAccess.GetDoctorList(doc);
        }

        public Task<Doctor> CreateDoctor(Doctor doc1)
        {
            return DoctorDataAccess.CreateDoctor(doc1);
        }

        public async Task<Doctor> GetDoctorById(string id)
        {
            return await DoctorDataAccess.GetDoctorById(id);
        }

        public async Task<Doctor> UpdateDoctor(Doctor doc)
        {
            return await DoctorDataAccess.UpdateDoctor(doc);
        }

        public async Task<Doctor> SaveDefaultDoctor(Doctor doc)
        {
            return await DoctorDataAccess.SaveDefaultDoctor(doc);
        }

        public async Task<Doctor> GetDefaultDoctor(string AccId, string InsId)
        {
            return await DoctorDataAccess.GetDefaultDoctor(AccId, InsId);
        }

        public async Task<Doctor> ResetDefaultDoctor(Doctor doc)
        {
            return await DoctorDataAccess.ResetDefaultDoctor(doc);
        }

        public Task<List<Doctor>> LocalShortDoctorList(string ShortDoctorName, string accountid, string instanceid)
        {
            return DoctorDataAccess.LocalShortDoctorList(ShortDoctorName, accountid, instanceid);
        }

       

    }
}
