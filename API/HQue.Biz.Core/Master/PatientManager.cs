﻿using System.Collections.Generic;
using System.Threading.Tasks;
using HQue.Contract.Infrastructure.Master;
using HQue.DataAccess.Master;
using HQue.ApiDataAccess.External;
using HQue.Communication.Sms;
using Utilities.Helpers;
using System;

namespace HQue.Biz.Core.Master
{
    public class PatientManager
    {
        protected readonly PatientDataAccess _patientDataAccess;
        private readonly PatientApi _patientApi;
        private readonly SmsSender _smsSender;
        private readonly ConfigHelper _configHelper;
        private ConfigHelper configHelper;

        public PatientManager(PatientDataAccess patientDataAccess, PatientApi patientApi, SmsSender smsSender)
        {
            _patientDataAccess = patientDataAccess;
            _patientApi = patientApi;
            _smsSender = smsSender;
            _configHelper = configHelper;
        }


        //saving a customer code Newly Added by DURGA on 18-04-2017 
        public virtual async Task<Patient> PatientSave(Patient model)
        {


            model.LocationType = Convert.ToInt32(Enum.Parse(typeof(LocationType), model.LocationTypeName));
            Patient data;
            if (model.PatientType == 1)
            {
                 data = await _patientDataAccess.PatientSave(model);
            }
            else
            {
                 data = await _patientDataAccess.DepartmentPatientSave(model);
            }
            await _patientDataAccess.SaveOpeningBalanceCustomerPayment(data);
            return model;
        }

        public async Task<string> autoSavepatient(Patient model)
        {
            return await _patientDataAccess.AutoSave(model);
        }


        //Updating a customer code Newly Added by DURGA on 19-04-2017 
        public async Task<Patient> PatientUpdate(Patient model)
        {
            Patient data;
            if (model.PatientType == 1)
            {
                var temp = model.Id;
                data = await _patientDataAccess.PatientUpdate(model);
                model.Id = temp;
            }
            else
            {
                data = await _patientDataAccess.DepartmentPatientUpdate(model);
            }
            await _patientDataAccess.UpdateOpeningBalanceCustomerayment(data);
            return model;
        }
        public async Task<decimal?> GetCustomerDiscount(Patient customer)
        {
            return await _patientDataAccess.GetCustomerDiscount(customer);
        }

        public async Task<IEnumerable<Patient>> PatientList(Patient data, string Accountid, string Instanceid)
        {
            var empId = data.EmpID;
            if (!string.IsNullOrEmpty(data.EmpID))
            {

                if (string.IsNullOrEmpty(data.Mobile))
                {
                    var mobi = _patientDataAccess.PatientMobile(data, Accountid, Instanceid);
                    if (!string.IsNullOrEmpty(mobi))
                        data.Mobile = mobi;
                }
                //var mobi = _patientDataAccess.PatientMobile(data,Accountid,Instanceid);
                //if (!string.IsNullOrEmpty(mobi))
                //    data.Mobile = mobi;
            }
            //Added for Check the patient have sale or not
            data.strCheckSales = _patientDataAccess.ChkPatientInSales(data, Accountid, Instanceid);

            var plist = await _patientApi.PatientListExternal(data);


            data.InstanceId = Instanceid; data.AccountId = Accountid;
            var Locallist = await _patientDataAccess.GetExternalLocalPatientList(data);



            foreach (var p in plist)
            {
                p.ispatientLocal = false;
                foreach (var l in Locallist)
                {
                    if (p.Name == l.Name && p.Mobile == l.Mobile)
                    {
                        p.EmpID = l.EmpID;
                        p.Id = l.Id;
                        p.ispatientLocal = true;
                        p.PatientType = l.PatientType;
                        p.CustomerPaymentType = l.CustomerPaymentType;
                    }
                }

            }

            return plist;
        }
        public async Task<IEnumerable<Patient>> PatientOfflineList(Patient data, string AccountId, string InstanceId, int department)
        {
            var empId = data.EmpID;

            var plist = await _patientDataAccess.GetOfflinePatientList(data, department);



            //foreach (var p in plist)
            //    p.EmpID = empId;
            return plist;
        }

        //public virtual Task<IEnumerable<Patient>> PatientList(Patient data)
        //{
        //    return _patientApi.PatientListExternal(data);
        //}

        public void SendSMSToCustomer(Patient patient)
        {
            _smsSender.Send(patient.Mobile, 6, new object[] { patient.Name });
        }

        public virtual Task<IEnumerable<Patient>> PatientSearchList(string data)
        {
            var plist = _patientApi.PatientSearchListExternal(data);
            return plist;
        }

        public virtual Task<IEnumerable<Patient>> PatientSearchListLocal(string data, string accountid, string instanceid, int department, int isActivet)
        {
            var plist = _patientDataAccess.GetPatientListLocal(data, accountid, instanceid, department, isActivet);
            return plist;
        }
        public virtual Task<IEnumerable<Patient>> PatientSearchSalesListLocal(string data, string accountid, string instanceid, int department)
        {
            var plist = _patientDataAccess.GetPatientSalesListLocal(data, accountid, instanceid, department);
            return plist;
        }

        public virtual Task<IEnumerable<Patient>> PatientSearchMobile(string data, string accountid, string instanceid, int department)
        {
            var plist = _patientDataAccess.GetPatientMobileList(data, accountid, instanceid, department);
            return plist;
        }

        //PatientSearchMobile
        public virtual Task<IEnumerable<Patient>> PatientSearchListLocalId(string data, string accountid, string instanceid)
        {
            var plist = _patientDataAccess.GetPatientListLocalId(data, accountid, instanceid);
            return plist;
        }

        public virtual Task<IEnumerable<Patient>> PatientSearchShortListLocal(string data, string accountid, string instanceid)
        {
            var plist = _patientDataAccess.PatientSearchShortListLocal(data, accountid, instanceid);
            return plist;
        }

        public async Task<bool> CustPaymentStatus(string accountId, string instanceId, string CustName, string CustMobile)
        {
            return await _patientDataAccess.CustPaymentStatus(accountId, instanceId, CustName, CustMobile);
        }
    }
}
