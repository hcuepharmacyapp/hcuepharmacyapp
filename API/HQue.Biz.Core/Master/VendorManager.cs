﻿using System.Collections.Generic;
using HQue.Contract.Infrastructure.Master;
using HQue.DataAccess.Master;
using HQue.Contract.Infrastructure;
using System;
using System.Threading.Tasks;
using HQue.Biz.Core.UserAccount;

namespace HQue.Biz.Core.Master
{
    public class VendorManager : BizManager
    {
        private readonly VendorDataAccess _vendorDataAccess;
        private readonly UserManager _userManager;

        public VendorManager(VendorDataAccess vendorDataAccess,UserManager userManager)
            : base(vendorDataAccess)
        {
            _vendorDataAccess = vendorDataAccess;
            _userManager = userManager;
        }

        //public async Task<string> Save(Vendor model)
        //{
        //    var exString = await ValidateVendor(model);
        //    if (exString != "") return exString;
        //    model.Status = true;
        //    model.AllowViewStock = model.AllowViewStock ?? false;

        //    var result = await _vendorDataAccess.Save(model);
        //    // Newly Added Gavaskar 22-10-2016 Start

        //    _userManager.SendInviteCommunicationVendor(model);

        //    // Newly Added Gavaskar 22-10-2016 End
        //    if (!result.AllowViewStock.HasValue || !result.AllowViewStock.Value)
        //    {
        //        //return result;
        //        return "";
        //    }

        //   return await _userManager.SaveVendorUser(model);
        //   //return result;
        //}

        public async Task<string> Save(Vendor model)
        {
            var exString = await ValidateVendor(model);
            if (exString != "") return exString; //If Vendor already exists return the custom error string

            //model.Status = (model.Status == false) ? false : true; //model.Status = true;
            model.AllowViewStock = model.AllowViewStock ?? false;

            // If Allow View Stock is checked, then Save the Vendor User
            if (model.AllowViewStock.HasValue && model.AllowViewStock.Value)
            {
                var userResult = await _userManager.SaveVendorUser(model);
                if (userResult != "") return userResult;
            }
            if (string.IsNullOrEmpty(model.PaymentType))
            {
                model.PaymentType = null;
            }

            bool isSendSms = await GetVendorCreateSmsSetting(model.InstanceId); //Added by Sarubala on 17-11-17

            var result = await _vendorDataAccess.Save(model);
            // Newly Added Gavaskar 22-10-2016 Start
            await _vendorDataAccess.SaveOpeningBalancePayment(result);

            if (isSendSms == true) //Modified by Sarubala on 17-11-17
            {
                _userManager.SendInviteCommunicationVendor(model);
            }
            

            return "";
            
           //return result;
        }

        public async Task<Vendor> Update(Vendor model)
        {
            var isSendSms = GetVendorCreateSmsSetting(model.InstanceId); //Added by Sarubala on 17-11-17
            // Newly Added Gavaskar 22-10-2016 Start
            Vendor result  = null;
            if (Convert.ToBoolean(isSendSms.Result) == true) //Modified by Sarubala on 17-11-17
            {
                _userManager.SendInviteCommunicationVendorUpdate(model);
            }
                

            // Newly Added Gavaskar 22-10-2016 End

            if (!string.IsNullOrEmpty(model.Name) && !string.IsNullOrEmpty(model.DrugLicenseNo))
            {
                if (model.DrugLicenseNo != model.tempDrugLicenseNo)
                {
                    var vendors = await _vendorDataAccess.CheckUniqueVendorByDrugLicenseNo(model);
                    if ((vendors.Count != 0) && (vendors[0].DrugLicenseNo == model.tempDrugLicenseNo) && (vendors.Count > 0))
                    {
                        return null;
                    }
                    else if (vendors.Count > 0)
                    {
                        return null;
                    }
                    result =  await _vendorDataAccess.Update(model);
                }
                else
                {
                    result =  await _vendorDataAccess.Update(model);
                }
                await _vendorDataAccess.UpdateOpeningBalancePayment(result);
            }
            // return null;
            return result;
        }

        public async Task<IEnumerable<Vendor>> VendorList(Vendor vendor)
        {
            return await _vendorDataAccess.VendorList(vendor);
        }
        public async Task<int> CheckOBPaymentPaid(string accountId, string instanceId, string customerid)
        {
            var res = await _vendorDataAccess.CheckOBPaymentPaid(accountId, instanceId, customerid);
            return res;
        }
        public async Task<PagerContract<Vendor>> ListPager(Vendor data)
        {
            var pager = new PagerContract<Vendor>
            {
                NoOfRows = await _vendorDataAccess.Count(data),
                List = await _vendorDataAccess.List(data)
            };

            return pager;
        }

        public async Task<IEnumerable<Vendor>> VendorDataList(Vendor vendor)
        {
            return await _vendorDataAccess.VendorDataList(vendor);
        }
        public Task <List<AlternateVendorProduct>> CheckExisitingProductList(string vendorId)
        {
            return _vendorDataAccess.CheckExisitingProductList(vendorId);
        }
        
        public async Task<Vendor> GetById(Vendor vendor)
        {
            return await _vendorDataAccess.GetById(vendor);
        }

        public async Task<bool> PaymentStatus(string accountId, string instanceId, string vendorId)
        {
            return await _vendorDataAccess.PaymentStatus(accountId, instanceId, vendorId);
        }

        public Task<List<State>> GetState(string stateName)
        {
            return _vendorDataAccess.GetState(stateName);
        }

        private async Task<string> ValidateVendor(Vendor vendor)
        {
            if (string.IsNullOrEmpty (vendor.Id )&& string.IsNullOrEmpty(vendor.Email) && string.IsNullOrEmpty(vendor.Mobile))
            {
                if (!string.IsNullOrEmpty(vendor.DrugLicenseNo))
                {
                    var vendors = await _vendorDataAccess.CheckUniqueVendorByDrugLicenseNo(vendor);
                    if (vendors.Count > 0)
                    {
                        return "Drug License No already exists";
                    }
                    return "";
                }
            }
            if (!string.IsNullOrEmpty(vendor.Name) && !string.IsNullOrEmpty(vendor.DrugLicenseNo))
            {
                var vendors = await _vendorDataAccess.CheckUniqueVendorByDrugLicenseNo(vendor);
                if (vendors.Count > 0)
                {
                    return "Drug License No already exists";
                }
                return "";
            }
            else
            {
                var vendors = await _vendorDataAccess.CheckUniqueVendor(vendor);
                if (vendors.Count > 0)
                {
                    //throw new Exception($" Record already exists");
                    return "Record already exists";
                }
                return "";
            }
        }

        //Added By Gavaskar Vendor Bulk Update  14-08-2017 Start
        public async Task<List<Vendor>> BulkProductUpdate(List<Vendor> vendors, Vendor userData)
        {
            foreach (var vendor in vendors)
            {
                vendor.AccountId = userData.AccountId;
               // vendor.InstanceId = userData.InstanceId;
                vendor.CreatedBy = userData.CreatedBy;
                vendor.UpdatedBy = userData.UpdatedBy;
                await UpdateVendorData(vendor);
            }
            return vendors;
        }

        public async Task<Vendor> UpdateVendorData(Vendor model)
        {
            var vendor = await _vendorDataAccess.Update(model);
            return vendor;
        }
        //Added By Gavaskar Vendor Bulk Update  14-08-2017 End

        public async Task<VendorPurchaseFormula> SavePurchaseFormula(VendorPurchaseFormula model)
        {
            return await _vendorDataAccess.SavePurchaseFormula(model);
        }

        public async Task<List<VendorPurchaseFormula>> GetPurchaseFormulaList(string AccountId, string formulaName)
        {
            return await _vendorDataAccess.GetPurchaseFormulaList(AccountId, formulaName);
        }

        //Added by Sarubala on 17-11-17
        public async Task<bool> GetVendorCreateSmsSetting(string InstanceId)
        {
            return await _vendorDataAccess.GetVendorCreateSmsSetting(InstanceId);
        }
    }
}
