﻿using HQue.Contract.Infrastructure;
using HQue.Contract.Infrastructure.Inventory;
using HQue.Contract.Infrastructure.Master;
using HQue.DataAccess.Master;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using HQue.DataAccess.Inventory;
using HQue.Contract.Base;
using HQue.ServiceConnector.Connector;
using Utilities.Helpers;

namespace HQue.Biz.Core.Master
{
    public class ProductManager
    {
        private readonly ProductDataAccess _productDataAccess;
        private readonly VendorPurchaseDataAccess _vendorPurchaseDataAccess;
        private readonly ConfigHelper _configHelper;
        public ProductManager(ProductDataAccess productDataAccess, VendorPurchaseDataAccess vendorPurchaseDataAccess, ConfigHelper configHelper)
        {
            _productDataAccess = productDataAccess;
            _vendorPurchaseDataAccess = vendorPurchaseDataAccess;
            _configHelper = configHelper;
        }
        public async Task<Product> Save(Product model)
        {
            await ValidateProduct(model);
            //Product model1 = new Product();
            //model1.AccountId = model.AccountId;
            //model1.InstanceId = model.InstanceId;
            //var count=await TotalProductCount(model1);
            model.Name = model.Name.ToUpper();
            //var getChar = model.Name.Substring(0, 2);
            //model.Code=(getChar.ToUpper()+ (count.NoOfRows + 1)).ToString();
            model.ProductMasterID = model.Id;
            var product = await _productDataAccess.Save(model);

            //GST related changes by Mani on 22-Jun-2017
            //Taking Global product master based on ProductMasterID
            ProductMaster globalProduct = new ProductMaster();
            if (product != null)
            {
                if (!string.IsNullOrEmpty(product.ProductMasterID))
                {
                    globalProduct = await _productDataAccess.GetProductByMasterId(product.ProductMasterID);
                }
            }

            var isGSTGiven = false;
            var isHsn = false;
            var isIgst = false;
            var isCgst = false;
            var isSgst = false;
            var isGstTotal = false;

            if (string.IsNullOrEmpty(globalProduct.HsnCode) && !string.IsNullOrEmpty(product.HsnCode))
            {
                isGSTGiven = true;
                isHsn = true;
                globalProduct.HsnCode = product.HsnCode;
            }
            if (globalProduct.Igst == null && product.Igst != null)
            {
                isGSTGiven = true;
                isIgst = true;
                globalProduct.Igst = product.Igst;
            }
            if (globalProduct.Cgst == null && product.Cgst != null)
            {
                isGSTGiven = true;
                isCgst = true;
                globalProduct.Cgst = product.Cgst;
            }
            if (globalProduct.Sgst == null && product.Sgst != null)
            {
                isGSTGiven = true;
                isSgst = true;
                globalProduct.Sgst = product.Sgst;
            }
            if (globalProduct.GstTotal == null && product.GstTotal != null)
            {
                isGSTGiven = true;
                isGstTotal = true;
                globalProduct.GstTotal = product.GstTotal;
            }

            if (isGSTGiven)
            {
                //globalProduct.UpdatedBy = 
                var productMaster = await _productDataAccess.UpdateHcueProductMasterGST(globalProduct, isHsn, isIgst, isCgst, isSgst, isGstTotal);
            }
            ///*Product Instance section Added by Poongodi on 14/03/2017*/
            //ProductInstance model2 = new ProductInstance();
            //model2.AccountId = model.AccountId;
            //model2.InstanceId = model.InstanceId;
            //model2.ProductId = product.Id;
            //model2.CreatedBy = model.CreatedBy;
            //model2.UpdatedBy = model.UpdatedBy;
            //var productinstance = await _productDataAccess.SaveProductInstance(model2);
            return product;
        }

        //New function added by Manivannan on 10-Oct-2017
        public async Task<int> GetGstNullAvailable(string accountId, string instanceId)
        {
            return await _productDataAccess.GetGstNullAvailable(accountId, instanceId);
        }
        public async Task<Product> UpdateProductData(Product model)
        {
            //await ValidateProduct(model);
            var product = await _productDataAccess.Update(model);
            /*Product Instance section Added by Poongodi on 14/03/2017*/
            ProductInstance model2 = new ProductInstance();
            model2.AccountId = model.AccountId;
            model2.InstanceId = model.InstanceId;
            model2.ProductId = product.Id;
            model2.RackNo = model.RackNo;
            model2.ReOrderLevel  = model.ReOrderLevel;
            model2.ReOrderQty = model.ReOrderQty;
            model2.UpdatedBy = model.UpdatedBy;
            model2.ReOrderModified = model.ReOrderLevel != null && model.ReOrderLevel != 0 || model.ReOrderQty != null && model.ReOrderQty != 0 ? true : false;
            var productinstance = await _productDataAccess.UpdateProductInstance(model2,"");
            return product;
        }
        //newly added by nandhini
        public async Task<Product> getGenericDetails(Product product)
        {
            return await _productDataAccess.getGenericDetails(product);
        }
      
        //newly added by nandhini
        //Purchase History Product
        public Task<List<Product>> InstanceList(string productName, string InstanceId)
        {
            return _productDataAccess.InstanceList(productName, InstanceId);
        }
        //Purchase Return Product 
        public Task<List<Product>> ReturnPurchaseList(string productName, string InstanceId)
        {
            return _productDataAccess.ReturnPurchaseList(productName, InstanceId);
        }
        //Sales History List
        public Task<List<Product>> SalesHistoryList(string productName, string InstanceId)
        {
            return _productDataAccess.SalesHistoryList(productName, InstanceId);
        }
        //added by nandhini for sales return list product search
        public Task<List<Product>> SalesReturnProductList(string productName, string InstanceId)
        {
            return _productDataAccess.SalesReturnProductList(productName, InstanceId);
        }
        public async Task<List<MasterProduct>> List(string productName,string Accountid,string InstanceId)
        {
            return  await _productDataAccess.List(productName,Accountid,InstanceId);
        }

        public async Task<List<MasterProduct>> AcountWiseProductList(string productName, string Accountid, string InstanceId)
        {
            return await _productDataAccess.AccountWiseList(productName, Accountid, InstanceId);
        }

        public async Task<List<MasterProduct>> NonHiddenProductList(string productName, string Accountid, string InstanceId)
        {
            return await _productDataAccess.NonHiddenProductList(productName, Accountid, InstanceId);
        }
        // newly added Violet Raj
        public async Task<List<MasterProduct>> StockProductList(string productName, string Accountid, string InstanceId)
        {
            return await _productDataAccess.StockProductList(productName, Accountid, InstanceId);
        }
        // Newly Added Gavaskar 22-02-2017 Start
        public async Task<List<MasterProduct>> InventoryUpdateProductList(string productName, string Accountid, string InstanceId)
        {
            return await _productDataAccess.InventoryUpdateProductList(productName, Accountid, InstanceId);
        }
        // Newly Added Gavaskar 22-02-2017 End
        public Task<List<MasterProduct>> LocalProductList(string productName, string Accountid, string InstanceId)
        {
            return _productDataAccess.LocalProductList(productName, Accountid, InstanceId);
        }
        public Task<List<ProductMaster>> GetProductMaster(string productName)
        {
            return _productDataAccess.GetProductMaster(productName);
        }
        public Task<List<Product>> productNameList(string productName)
        {
            return _productDataAccess.GetproductList(productName);
        }
        public Task<List<Product>> GenericList(string genericName)
        {
            return _productDataAccess.GetGenericList(genericName);
        }
        public Task<List<ProductStock>> GenericSearchList(string genericName)
        {
            return _productDataAccess.GetGenericSearchList(genericName);
        }
        public Task<List<Product>> ManufacturerList(string manufacturercName)
        {
            return _productDataAccess.GetManufacturerList(manufacturercName);
        }
        public Task<List<Product>> ScheduledCategoryList(string scheduledCatName)
        {
            return _productDataAccess.GetScheduledCategoryList(scheduledCatName);
        }
        public Task<List<Product>> TypeList(string typeName)
        {
            return _productDataAccess.GetTypeList(typeName);
        }
        public Task<List<Product>> CategoryList(string categoryName)
        {
            return _productDataAccess.GetCategoryList(categoryName);
        }
        /// <summary>
        /// To Get Subcategory added by Arun on 26/09/2017
        /// </summary>
        /// <param name="categoryName"></param>
        /// <returns></returns>
        public Task<List<BaseDomainValues>> SubCategoryList(string categoryName)
        {
            return _productDataAccess.SubCategoryList(categoryName);
        }
       
        public Task<List<Product>> CommodityCodeList(string commodityName)
        {
            return _productDataAccess.GetCommodityCodeList(commodityName);
        }
        public Task<List<Product>> KindNameList(string kindName)
        {
            return _productDataAccess.GetKindNameList(kindName);
        }
        public async Task<PagerContract<Product>> ListPager(Product data)
        {
            var pager = new PagerContract<Product>
            {
                NoOfRows = await _productDataAccess.Count(data),
                List = await _productDataAccess.List(data)
            };
            return pager;
        }
        //public async Task<PagerContract<Product>> ListProductPager(Product data)
        //{
        //    var pager = new PagerContract<Product>
        //    {
        //        NoOfRows = await _productDataAccess.ProductCount(data),                
        //        List = await _productDataAccess.ProductList(data)
        //    };
        //    return pager;
        //}
        public async Task<IEnumerable<Product>> ListProductPager(Product data)
        {
            return await _productDataAccess.ProductList(data);
        }
		 public async Task<PagerContract<Product>> ListingPager(string instanceId, DateTime from, DateTime to)
        {
            var pager = new PagerContract<Product>
            {
                NoOfRows = await _productDataAccess.Count(instanceId,from, to),
                List = await _productDataAccess.Listdata(instanceId,from, to)
            };
            return pager;
        }
        public async Task<PagerContract<Product>> Export(Product data)
        {
            var pager = new PagerContract<Product>
            {
                NoOfRows = await _productDataAccess.Count(data),
                List = await _productDataAccess.ExportList(data)
            };
            return pager;
        }
        private async Task<bool> ValidateProduct(Product data)
        {
            var newProduct = await _productDataAccess.CheckUniqueProduct(data);
            if (!newProduct)
                throw new Exception($"Product already exists");
            return true;
        }
        public async Task<PagerContract<Product>> ProductCount(Product data)
        {
            var productCount = new PagerContract<Product>
            {
                NoOfRows = await _productDataAccess.Count(data),               
            };
            return productCount;
        }
        public async Task<PagerContract<Product>> TotalProductCount(Product data)
        {
            var productCount = new PagerContract<Product>
            {
                NoOfRows = await _productDataAccess.TotalProductCount(data),
            };
            return productCount;
        }
        public async Task<PagerContract<Product>> MasterListPager(string accountid, string instanceid, Product data)
        {
            if (data.IsGstNullSearch)
            {
                var pager = new PagerContract<Product>
                {
                    NoOfRows = await _productDataAccess.GstNullMasterCount(accountid, instanceid, data),
                    List = await _productDataAccess.GstNullMasterList(accountid, instanceid, data)
                };

                return pager;
            }
            else
            {
                var pager = new PagerContract<Product>
                {
                    NoOfRows = await _productDataAccess.MasterCount(accountid, instanceid, data),
                    List = await _productDataAccess.MasterList(accountid, instanceid, data)
                };

                return pager;
            }
            
        }
        public async Task<PagerContract<Product>> EntireMasterListPager(string accountid, string instanceid, Product data)
        {
            var pager = new PagerContract<Product>
            {
                NoOfRows = await _productDataAccess.MasterCount(accountid, instanceid, data),
                List = await _productDataAccess.EntireMasterList(accountid, instanceid, data)
            };
            return pager;
        }
        public async Task<IEnumerable<DraftVendorPurchaseItem>> SaveProduct(IEnumerable<DraftVendorPurchaseItem> itemList)
        {
            try
            {
                foreach (var draftvendorPurchaseItem in itemList)
                {
                    if (string.IsNullOrEmpty(draftvendorPurchaseItem.ProductStock.Product.ProductMasterID))
                    {
                        continue;
                    }
                    var product1 = new Product
                    {
                        Name = draftvendorPurchaseItem.ProductStock.Product.Name,
                        ProductMasterID = draftvendorPurchaseItem.ProductStock.Product.ProductMasterID,
                        AccountId = draftvendorPurchaseItem.AccountId
                    };
                    var getExistData = await _productDataAccess.GetExistProductMasterById(product1);
                    if (getExistData.Id != null)
                    {
                        draftvendorPurchaseItem.ProductStock.ProductId = getExistData.Id;
                        await _vendorPurchaseDataAccess.UpdateProductId(draftvendorPurchaseItem.ProductStock.ProductId, draftvendorPurchaseItem.Id);
                        continue;
                    }
                        ProductMaster productmaster = await _productDataAccess.GetProductByMasterId(draftvendorPurchaseItem.ProductStock.ProductId);
                        Product product = new Product();
                        //int NoOfRows = await _productDataAccess.TotalProductCount(product);
                        //var getChar = draftvendorPurchaseItem.ProductStock.Product.Name.Substring(0, 2);
                        //string Code = (getChar.ToUpper() + (NoOfRows + 1)).ToString();
                        product.AccountId = draftvendorPurchaseItem.AccountId;
                        product.InstanceId = draftvendorPurchaseItem.InstanceId;
                        //product.Code = Code;
                        product.Name = draftvendorPurchaseItem.ProductStock.Product.Name;
                        product.Manufacturer = productmaster.Manufacturer;
                        product.KindName = productmaster.KindName;
                        product.StrengthName = productmaster.StrengthName;
                        product.Type = productmaster.Type;
                        product.Schedule = productmaster.Schedule;
                        product.Category = productmaster.Category;
                        product.GenericName = productmaster.GenericName;
                        product.CommodityCode = productmaster.CommodityCode;
                        product.Packing = productmaster.Packing;
                        product.PackageSize = productmaster.PackageSize;
                        product.VAT = productmaster.VAT;
                        product.Price = productmaster.Price;
                        product.Status = productmaster.Status;
                        product.RackNo = productmaster.RackNo;
                        product.ProductMasterID = productmaster.Id;
                        product.CreatedBy = draftvendorPurchaseItem.CreatedBy;
                        product.UpdatedBy = draftvendorPurchaseItem.UpdatedBy;
                    var productdata = await _productDataAccess.Save(product);
                    draftvendorPurchaseItem.ProductId = productdata.Id;
                    await _vendorPurchaseDataAccess.UpdateProductId(draftvendorPurchaseItem.ProductId, draftvendorPurchaseItem.Id);
                }
                return itemList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<IEnumerable<ProductMaster>> HcueProductMasterListPager(ProductMaster data)
        {
            return await _productDataAccess.HcueProductMasterList(data);

        }
        public async Task<ProductMaster> UpdateHcueProductMaster(ProductMaster data)
        {
            return await _productDataAccess.UpdateHcueProductMaster(data);
        }
        public Task<List<ProductMaster>> GetProductMasterGeneric(string genericName)
        {
            return _productDataAccess.GetProductMasterGeneric(genericName);
        }
        public Task<ProductMaster> ProductMasterUpdateCount()
        {
            return _productDataAccess.ProductMasterUpdateCount();
        }

        public Task<bool> ReorderMinMaxBulkUpdate(ProductInstance data, DateTime fromDate, DateTime toDate, int minReorderFactor, int maxReorderFactor)
        {
            return _productDataAccess.ReorderMinMaxBulkUpdate(data, fromDate, toDate, minReorderFactor, maxReorderFactor);
        }

        public async Task<List<Product>> BulkProductUpdate(List<Product> products,Product userData)
        {            
            foreach (var product in products)
            {
                if (product.IsEdit == true)
                {
                    product.AccountId=userData.AccountId;
                    product.InstanceId = userData.InstanceId;
                    product.CreatedBy = userData.CreatedBy;
                    product.UpdatedBy = userData.UpdatedBy;                    
                    await UpdateProductData(product);
                }
            }
                //return await _productDataAccess.BulkProductUpdate(productStock);
            return products;
        }
        public Task<string> ExportProductMaster(string accountId)
        {
            return _productDataAccess.ExportProductMaster(accountId);
            //return null;
        }

        public async Task<List<Product>> GetProductCodes(List<Product> data)
        {
            return await _productDataAccess.GetProductCodes(data);
        }

        public async Task<List<Product>> GetOnlineProductCodes(List<Product> data)
        {
            var sc = new RestConnector();
            var result = await sc.PostAsyc<List<Product>>(_configHelper.InternalApi.GetProductCodesUrl, data);
            result = await UpdateProductCodesToLocal(result);
            return result;
        }

        public async Task<List<Product>> UpdateProductCodesToLocal(List<Product> data)
        {
            return await _productDataAccess.UpdateProductCodesToLocal(data);
        }
        public async Task<int> CheckProductExist(string AccountId)
        {
            return await _productDataAccess.CheckProductExist(AccountId);
        }

        public async Task<Tuple<bool,ProductStock>> GetDataCorrectionProductStocks(ProductStock data)
        {
            return await _productDataAccess.GetDataCorrectionProductStocks(data);
        }
        public async Task<Tuple<bool, ProductStock>> InsertDataCorrectionOpeningStocks(ProductStock data)
        {
            return await _productDataAccess.InsertDataCorrectionOpeningStocks(data);
        }
        public async Task<Tuple<bool, StockAdjustment>> InsertDataCorrectionStockAdjustments(StockAdjustment data)
        {
            return await _productDataAccess.InsertDataCorrectionStockAdjustments(data);
        }
        public async Task<Tuple<bool, TempVendorPurchaseItem>> InsertDataCorrectionTempStocks(TempVendorPurchaseItem data)
        {
            return await _productDataAccess.InsertDataCorrectionTempStocks(data);
        }
        public async Task<Tuple<bool, ProductStock>> UpdateProductStockToOnline(ProductStock data)
        {
            return await _productDataAccess.UpdateProductStockToOnline(data);
        }
        public async Task<string> GetReverseSyncOfflineToOnlineCompare(string SeedIndex)
        {
            return await _productDataAccess.GetReverseSyncOfflineToOnlineCompare(SeedIndex);
        }
    }
}
