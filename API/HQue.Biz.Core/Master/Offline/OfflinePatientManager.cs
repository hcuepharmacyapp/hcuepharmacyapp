﻿using HQue.ApiDataAccess.External;
using HQue.Communication.Sms;
using HQue.DataAccess.Master;
using System.Collections.Generic;
using System.Threading.Tasks;
using HQue.Contract.Infrastructure.Master;

namespace HQue.Biz.Core.Master.Offline
{
    public class OfflinePatientManager : PatientManager
    {
        public OfflinePatientManager(PatientDataAccess patientDataAccess, PatientApi patientApi, SmsSender smsSender) : base(patientDataAccess, patientApi, smsSender)
        {

        }

        public async new Task<IEnumerable<Patient>> PatientList(Patient data)
        {
            return await _patientDataAccess.PatientList(data);
        }

        //public override async Task<Patient> Save(Patient model)
        //{
        //    return await _patientDataAccess.Save(model);
        //}
    }
}
