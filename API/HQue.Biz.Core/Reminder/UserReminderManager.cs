﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using HQue.Contract.Infrastructure;
using HQue.Contract.Infrastructure.Estimates;
using HQue.Contract.Infrastructure.Inventory;
using HQue.Contract.Infrastructure.Reminder;
using HQue.DataAccess.Reminder;
using Microsoft.AspNetCore.Http;
using Utilities.Helpers;
using Utilities.Helpers.Interface;

namespace HQue.Biz.Core.Reminder
{
    public class UserReminderManager : BizManager
    {
        private readonly ICacheManager _cacheManager;
        private readonly ClaimsPrincipal _user;
        private string OpenPersonalReminderCountToken => $"{_user.Identity.Id()}_{_user.InstanceId()}_PER_OPENREMINDERCOUNTTOKEN";
        private string OpenCustomerReminderCountToken => $"{_user.InstanceId()}_CUS_OPENREMINDERCOUNTTOKEN";
        private new UserReminderDataAccess DataAccess => base.DataAccess as UserReminderDataAccess;
        public UserReminderManager(UserReminderDataAccess dataAccess, IHttpContextAccessor httpContextAccessor, ICacheManager  cacheManager) : base(dataAccess)
        {
            _cacheManager = cacheManager;
            _user = httpContextAccessor.HttpContext.User;
        }

        #region ReinderList

        public async Task<List<UserReminder>> PersonalReminderList(bool isUpcoming)
        {
            return isUpcoming ? PersonalUpcomingReminderList() : await PersonalMissedReminderList();
        }

        public async Task<List<CustomerReminder>> CustomerReminderList(bool isUpcoming, CustomerReminder customerReminder)
        {
            customerReminder.AccountId = _user.AccountId();
            customerReminder.InstanceId = _user.InstanceId();
            return isUpcoming ? await CustomerUpcomingReminderList(customerReminder) : await CustomerMissedReminderList(customerReminder);
        }

        public async Task<PagerContract<UserReminder>> PersonalReminderList()
        {
            var data = new UserReminder
            {
                HqueUserId = _user.Identity.Id(),
                AccountId = _user.AccountId(),
                InstanceId = _user.InstanceId()
            };
            var pager = new PagerContract<UserReminder>
            {
                NoOfRows = await DataAccess.GetReminderCount(data),
                List = await GetUserReminder(data)
            };

            return pager;
        }

        private async Task<List<UserReminder>> GetUserReminder(UserReminder data)
        {
            var reminder = await DataAccess.GetActivePersonalReminder(data);

            return reminder.Select(r =>
                {
                    var x = r;
                    x.ReminderDate = r.StartDate.Value;
                    return x;
                }
            ).ToList();
        }

        private List<UserReminder> PersonalUpcomingReminderList()
        {
            var data = new UserReminder
            {
                HqueUserId = _user.Identity.Id(),
                AccountId = _user.AccountId(),
                InstanceId = _user.InstanceId()
            };
            var reminderTask = DataAccess.GetActivePersonalReminder(data);
            var reminderRemarksTask = DataAccess.GetRecentlyReminderRemark(data.HqueUserId, data.AccountId, data.InstanceId);

            Task.WaitAll(reminderTask, reminderRemarksTask);

            var reminder = reminderTask.Result;
            var reminderRemarks = reminderRemarksTask.Result;


            var listManager = new UserReminderListManager(reminder, reminderRemarks);
            var list = listManager.GetList();
            return list;
        }

        private async Task<List<UserReminder>> PersonalMissedReminderList()
        {
            var reminder = await DataAccess.GetActivePersonalReminder(new UserReminder
            {
                HqueUserId = _user.Identity.Id(),
                AccountId = _user.AccountId(),
                InstanceId = _user.InstanceId()
            });
            var listManager = new UserReminderListManager(reminder);
            var list = listManager.GetMissedList().ToArray();

            var previousRemarks = await DataAccess.GetReminderRemarks(list);
            return listManager.GetMissedList(list, previousRemarks);
        }


        private async Task<List<CustomerReminder>> CustomerUpcomingReminderList(CustomerReminder customerReminder)
        {
            customerReminder.AccountId = _user.AccountId();
            customerReminder.InstanceId = _user.InstanceId();
            var reminderTask = DataAccess.GetActiveCustomerReminder(customerReminder);
            var reminderRemarksTask = DataAccess.GetRecentlyCustomerReminderRemark(customerReminder.AccountId, customerReminder.InstanceId);

            Task.WaitAll(reminderTask, reminderRemarksTask);

            var reminder = reminderTask.Result;
            var reminderRemarks = reminderRemarksTask.Result;


            var listManager = new CustomerReminderListManager(reminder, reminderRemarks);
            var list = listManager.GetList();

            var reminderItem = await DataAccess.GetReminderItem(list);

            var customerReminderWithItem = listManager.GetList(list, reminderItem);

            return customerReminderWithItem;
        }

        private async Task<List<CustomerReminder>> CustomerMissedReminderList(CustomerReminder customerReminder)
        {
            var reminder = await DataAccess.GetActiveCustomerReminder(customerReminder);
            var listManager = new CustomerReminderListManager(reminder);
            var list = listManager.GetMissedList().ToArray();

            var previousRemarks = await DataAccess.GetReminderRemarks(list);
            var missedReminder = listManager.GetMissedList(list, previousRemarks);

            var reminderItem = await DataAccess.GetReminderItem(missedReminder);

            var customerReminderWithItem = listManager.GetList(missedReminder, reminderItem);
            customerReminderWithItem.ForEach(x => { x.MissedType = 1; });

            var missedOrders = await DataAccess.GetMissedOrders(customerReminder);

            if (!string.IsNullOrEmpty(customerReminder.CustomerName))
                missedOrders = missedOrders.Where(x => x.CustomerName == customerReminder.CustomerName).ToList();
            if (!string.IsNullOrEmpty(customerReminder.ReminderPhone))
                missedOrders = missedOrders.Where(x => x.ReminderPhone == customerReminder.ReminderPhone).ToList();
            if (!string.IsNullOrEmpty(customerReminder.DoctorName))
                missedOrders = missedOrders.Where(x => x.DoctorName == customerReminder.DoctorName).ToList();

            customerReminderWithItem.AddRange(missedOrders);
            
            return customerReminderWithItem.OrderByDescending(x => x.ReminderDate).Select(x => x).ToList();
        }


        #endregion


        public Task<bool> CancelReminder(string id)
        {
            ClearCache();
            return DataAccess.CancelReminder(id);
        }

        public Task<bool> CancelReminder(UserReminder userReminder)
        {
            ClearCache();
            return DataAccess.CancelReminder(userReminder);
        }

        public Task<bool> SaveRemark(ReminderRemark data)
        {
            data.RemarksDate = CustomDateTime.IST;
            ClearCache();
            return DataAccess.SaveRemark(data);
        }

        public async Task<CustomerReminder> CreateCustomerReminder(CustomerReminder customerReminder)
        {
            var reminderManager = new CustomerReminderCreateManager(DataAccess);
            _cacheManager.Remove(customerReminder.ReminderType == UserReminder.Customer ? OpenCustomerReminderCountToken : OpenPersonalReminderCountToken);
            return await reminderManager.CreateReminder(customerReminder);
        }

        public async Task<bool> CancelSalesReminder(string referenceId)
        {
            var reminder = await DataAccess.List(new UserReminder {ReferenceId = referenceId});
            foreach (var userReminder in reminder)
            {
                await CancelReminder(userReminder.Id);
            }
            return true;
        }

        public async Task<bool> CancelSalesReminder(Sales data)
        {
            string referenceId = data.Id;
            var reminder = await DataAccess.List(new UserReminder { ReferenceId = referenceId });
            foreach (var userReminder in reminder)
            {
                userReminder.SetExecutionQuery(data.GetExecutionQuery(), data.WriteExecutionQuery);
                await CancelReminder(userReminder);
            }
            return true;
        }

        public async Task SetSalesReminder(IReminder sales)
        {
            var reminderManager = new CustomerReminderCreateManager(DataAccess);
            await reminderManager.CreateReminder(sales);
            _cacheManager.Remove(OpenCustomerReminderCountToken);
        }

        public async Task SetEstimateReminder(Estimate sales)
        {
            var reminderManager = new CustomerReminderCreateManager(DataAccess);
            await reminderManager.CreateReminder(sales);
            _cacheManager.Remove(OpenCustomerReminderCountToken);
        }

        public async Task<int> OpenReminderCount()
        {
            var total = 0;
            total += GetPersonalReminderCount();
            total += await GetCustomerReminderCount();
            return total;
        }

        public async Task<PagerContract<CustomerReminder>> CustomerReminderList(CustomerReminder customerReminder)
        {
            //customerReminder.AccountId = _user.AccountId();
            //customerReminder.InstanceId = _user.InstanceId();
            
            var pager = new PagerContract<CustomerReminder>
            {
                NoOfRows = await (DataAccess.GetReminderCount(customerReminder)) + (await DataAccess.GetMissedOrderCount(customerReminder)),
                List = await CustomerReminder(customerReminder)
            };

            return pager;

        }

        private async Task<List<CustomerReminder>> CustomerReminder(CustomerReminder customerReminder)
        {
            var reminder = await DataAccess.GetActiveCustomerReminder(customerReminder);
            var reminderItem = await DataAccess.GetReminderItem(reminder);
            var reminderFull = reminder.Select(x =>
            {
                var r = x;
                r.ReminderDate = r.StartDate.Value;
                r.ReminderProduct = reminderItem.Where(ri => ri.UserReminderId == r.Id).ToList();
                return r;
            }).ToList();
            reminderFull.ForEach(x => { x.MissedType = 1; });

            var missedOrders = await DataAccess.GetMissedOrders(customerReminder);

            if (!string.IsNullOrEmpty(customerReminder.CustomerName))
                missedOrders = missedOrders.Where(x => x.CustomerName == customerReminder.CustomerName).ToList();
            if (!string.IsNullOrEmpty(customerReminder.ReminderPhone))
                missedOrders = missedOrders.Where(x => x.ReminderPhone == customerReminder.ReminderPhone).ToList();
            if (!string.IsNullOrEmpty(customerReminder.DoctorName))
                missedOrders = missedOrders.Where(x => x.DoctorName == customerReminder.DoctorName).ToList();

            reminderFull.AddRange(missedOrders);

            return reminderFull.OrderByDescending(x => x.ReminderDate).Select(x => x).ToList();
            
        }

        private int GetPersonalReminderCount()
        {
            var cacheCount = _cacheManager.Get<int?>(OpenPersonalReminderCountToken);
            if (cacheCount.HasValue)
                return cacheCount.Value;

            var count = PersonalUpcomingReminderList().Count;
            _cacheManager.Set(OpenPersonalReminderCountToken, count);
            return count;
        }

        private async Task<int> GetCustomerReminderCount()
        {
            var cacheCount = _cacheManager.Get<int?>(OpenCustomerReminderCountToken);
            if (cacheCount.HasValue)
                return cacheCount.Value;

            var count = (await CustomerUpcomingReminderList(new CustomerReminder())).Count;
            _cacheManager.Set(OpenCustomerReminderCountToken, count);
            return count;
        }

        private void ClearCache()
        {
            _cacheManager.Remove(OpenPersonalReminderCountToken);
            _cacheManager.Remove(OpenCustomerReminderCountToken);
        }
    }
}
