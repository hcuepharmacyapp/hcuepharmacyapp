using System;
using System.Collections.Generic;
using System.Linq;
using HQue.Contract.Infrastructure.Reminder;
using Utilities.Helpers;

namespace HQue.Biz.Core.Reminder
{
    public class UserReminderListManager
    {
        private readonly List<UserReminder> _userReminders;
        private readonly List<ReminderRemark> _reminderRemarks;
        protected readonly List<UserReminder> List;


        protected UserReminderListManager()
        {
            
        }

        protected UserReminderListManager(List<ReminderRemark> reminderRemarks)
        {
            List = new List<UserReminder>();
            _reminderRemarks = reminderRemarks;
        }

        public UserReminderListManager(List<UserReminder> userReminders)
        {
            List = new List<UserReminder>();
            _userReminders = userReminders;
        }

        public UserReminderListManager(List<UserReminder> userReminders, List<ReminderRemark> reminderRemarks) : this(reminderRemarks)
        {
            _userReminders = userReminders;
        }

        protected void AddReminder(UserReminder userReminder, DateTime validationDate)
        {
            foreach (var nextReminderDate in userReminder.GetNextReminderDates().GroupBy( x=> x.Value))
            {
                var reminderFrequency = nextReminderDate.Aggregate(UserReminderType.None, (current, keyValuePair) => current | keyValuePair.Key);

                var reminderDate = nextReminderDate.Key;

                if (validationDate.Date < userReminder.StartDate.Value.Date.AddDays(-3))
                    return;

                if (reminderDate.Date < validationDate.Date || reminderDate.Date > validationDate.AddDays(3).Date)
                    return;

                if (_reminderRemarks.Any(
                r =>
                    r.UserReminderId == userReminder.Id &&
                    r.RemarksFor.Value.Date == reminderDate.Date))
                    return;

                userReminder.ReminderFrequency = (int) reminderFrequency;

                AddToList(userReminder, reminderDate);
            }
        }

        public List<UserReminder> GetList()
        {
            foreach (var userReminder in _userReminders)
            {
                AddReminder(userReminder,CustomDateTime.IST);
            }

            return List.OrderBy(r => r.ReminderDate).ToList();
        }

        protected virtual void AddToList(UserReminder reminder, DateTime date)
        {
            List.Add(reminder.Clone(date));
        }

        internal List<UserReminder> GetMissedList(IEnumerable<UserReminder> reminders, List<ReminderRemark> previousRemarks)
        {
            return
                reminders.Where(customerReminder => !previousRemarks
                        .Any(x => x.UserReminderId == customerReminder.Id && x.RemarksFor.Value.Date >= customerReminder.ReminderDate.Date))
                    .ToList();
        }

        public IEnumerable<UserReminder> GetMissedList()
        {
            var list = new List<UserReminder>();
            foreach (var customerReminder in _userReminders)
            {
                var date = customerReminder.GetPreviousReminderDate();
                if (date.Date < customerReminder.StartDate.Value.Date || date >= CustomDateTime.IST.Date)
                    continue;

                customerReminder.ReminderDate = date.Date;
                list.Add(customerReminder);
            }
            return list;
        }
    }
}