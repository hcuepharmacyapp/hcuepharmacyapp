using System;
using System.Collections.Generic;
using HQue.Contract.Infrastructure.Reminder;
using Utilities.Helpers;

namespace HQue.Biz.Core.Reminder
{
    public static class ReminderHelper
    {

        public static DateTime GetPreviousReminderDate(this UserReminder customerReminder)
        {
            if (customerReminder.UserReminderFrequencyType.HasFlag(UserReminderType.Daily))
                return CustomDateTime.IST.AddDays(-1);
            else if (customerReminder.UserReminderFrequencyType.HasFlag(UserReminderType.Weekly))
                return customerReminder.GetNextReminderDate().AddDays(-7);
            else if (customerReminder.UserReminderFrequencyType.HasFlag(UserReminderType.Monthly))
                return customerReminder.GetNextReminderDate().AddMonths(-1);
            else if (customerReminder.UserReminderFrequencyType.HasFlag(UserReminderType.Yearly))
                return customerReminder.GetNextReminderDate().AddYears(-1);
            else //if (customerReminder.UserReminderFrequencyType.HasFlag(UserReminderType.Once))
                return customerReminder.GetNextReminderDate();

            //throw new Exception($"invalid frequency {customerReminder.ReminderFrequency}");
        }

        public static IEnumerable<KeyValuePair<UserReminderType, DateTime>> GetNextReminderDates(this UserReminder reminder)
        {
            if (reminder.UserReminderFrequencyType.HasFlag(UserReminderType.Once))
                yield return new KeyValuePair<UserReminderType, DateTime>(UserReminderType.Once, reminder.StartDate.Value);

            if (reminder.UserReminderFrequencyType.HasFlag(UserReminderType.Daily))
                yield return new KeyValuePair<UserReminderType, DateTime>(UserReminderType.Daily, CustomDateTime.IST.Date);

            if (reminder.UserReminderFrequencyType.HasFlag(UserReminderType.Weekly))
                yield return new KeyValuePair<UserReminderType, DateTime>(UserReminderType.Weekly, GetNextWeekday(CustomDateTime.IST, reminder.StartDate.Value.DayOfWeek).Date);

            if (reminder.UserReminderFrequencyType.HasFlag(UserReminderType.Monthly))
                yield return new KeyValuePair<UserReminderType, DateTime>(UserReminderType.Monthly, NextReminderMonth(reminder).Date);

            if (reminder.UserReminderFrequencyType.HasFlag(UserReminderType.Yearly))
                yield return new KeyValuePair<UserReminderType, DateTime>(UserReminderType.Yearly, GetNextYear(reminder).Date);
        }

        private static DateTime GetNextReminderDate(this UserReminder reminder)
        {
            if (reminder.UserReminderFrequencyType.HasFlag(UserReminderType.Once))
                return reminder.StartDate.Value;

            if (reminder.UserReminderFrequencyType.HasFlag(UserReminderType.Daily))
                return CustomDateTime.IST;

            if (reminder.UserReminderFrequencyType.HasFlag(UserReminderType.Weekly))
                return GetNextWeekday(CustomDateTime.IST,reminder.StartDate.Value.DayOfWeek);

            if (reminder.UserReminderFrequencyType.HasFlag(UserReminderType.Monthly))
                return NextReminderMonth(reminder);

            if (reminder.UserReminderFrequencyType.HasFlag(UserReminderType.Yearly))
                return GetNextYear(reminder);

            return CustomDateTime.IST;
        }

        private static DateTime NextReminderMonth(UserReminder reminder)
        {
            var lastDayOfMonth = new DateTime(CustomDateTime.IST.Year, CustomDateTime.IST.Month, 1).AddMonths(1).AddDays(-1).Day;
            var reminderDay = reminder.StartDate.Value.Day > lastDayOfMonth
                ? lastDayOfMonth
                : reminder.StartDate.Value.Day;

            if (CustomDateTime.IST.Day > reminder.StartDate.Value.Day)
            {
                var nextMonth = new DateTime(CustomDateTime.IST.Year, CustomDateTime.IST.Month, 1).AddMonths(1);
                return new DateTime(nextMonth.Year, nextMonth.Month, reminderDay).Date;
            }
            return new DateTime(CustomDateTime.IST.Year, CustomDateTime.IST.Month, reminderDay).Date;
        }

        private static DateTime GetNextYear(UserReminder reminder)
        {
            var lastDayOfMonth = new DateTime(CustomDateTime.IST.Year, CustomDateTime.IST.Month, 1).AddMonths(1).AddDays(-1).Day;
            var reminderDay = reminder.StartDate.Value.Day > lastDayOfMonth
                ? lastDayOfMonth
                : reminder.StartDate.Value.Day;

            if (CustomDateTime.IST.DayOfYear > reminder.StartDate.Value.DayOfYear)
            {
                return new DateTime(CustomDateTime.IST.Year, reminder.StartDate.Value.Month, reminderDay).AddYears(1).Date;
            }
            return new DateTime(CustomDateTime.IST.Year, reminder.StartDate.Value.Month, reminderDay).Date;
        }

        private static DateTime GetNextWeekday(DateTime start, DayOfWeek day)
        {
            int daysToAdd = ((int)day - (int)start.DayOfWeek + 7) % 7;
            return start.AddDays(daysToAdd).Date;
        }
    }
}