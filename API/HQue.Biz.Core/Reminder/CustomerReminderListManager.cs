using System;
using System.Collections.Generic;
using System.Linq;
using HQue.Contract.Infrastructure.Reminder;
using Utilities.Helpers;

namespace HQue.Biz.Core.Reminder
{
    internal class CustomerReminderListManager : UserReminderListManager
    {
        private readonly List<CustomerReminder> _customerReminders;

        public CustomerReminderListManager(List<CustomerReminder> userReminders, List<ReminderRemark> reminderRemarks) : base(reminderRemarks)
        {
            _customerReminders = userReminders;
        }

        public CustomerReminderListManager(List<CustomerReminder> reminder)
        {
            _customerReminders = reminder;
        }

        public new List<CustomerReminder> GetList()
        {
            foreach (var userReminder in _customerReminders)
            {
                AddReminder(userReminder,CustomDateTime.IST);
            }

            return List.OrderBy(r => r.ReminderDate).Select(x => x as CustomerReminder).ToList();
        }

        protected override void AddToList(UserReminder reminder, DateTime date)
        {
            List.Add(((CustomerReminder)reminder).Clone(date));
        }

        internal List<CustomerReminder> GetList(List<CustomerReminder> list, List<ReminderProduct> reminderItem)
        {
            foreach (var item in list)
            {
                var reminder = reminderItem.Where(x => x.UserReminderId == item.Id 
                && ((UserReminderType) item.ReminderFrequency).HasFlag((UserReminderType) x.ReminderFrequency.Value));
                item.ReminderProduct = reminder;
            }
            return list;
        }

        public new IEnumerable<CustomerReminder> GetMissedList()
        {
            var list = new List<CustomerReminder>();
            foreach (var customerReminder in _customerReminders)
            {
                var date = customerReminder.GetPreviousReminderDate();
                if (date.Date < customerReminder.StartDate.Value.Date || date >= CustomDateTime.IST.Date)
                    continue;
                customerReminder.ReminderDate = date.Date;
                list.Add(customerReminder);
            }
            return list;
        }

        

        internal List<CustomerReminder> GetMissedList(IEnumerable<CustomerReminder> customerReminders, List<ReminderRemark> previousRemarks)
        {
            var missedReminder = new List<CustomerReminder>();
            foreach (var customerReminder in customerReminders)
            {
                if(!previousRemarks.Any(x => x.UserReminderId == customerReminder.Id && x.RemarksFor.Value.Date >= customerReminder.ReminderDate.Date))
                    missedReminder.Add(customerReminder);
            }
            return missedReminder;
        }
    }
}