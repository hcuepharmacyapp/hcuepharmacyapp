﻿using HQue.DataAccess.Reminder;
using System.Threading.Tasks;
using HQue.Contract.Infrastructure.Reminder;
using Utilities.Helpers;
using System.Linq;
using System;
using System.Collections.Generic;
using HQue.DataAccess.DbModel;

namespace HQue.Biz.Core.Reminder
{
    internal class CustomerReminderCreateManager : BizManager
    {
        private new UserReminderDataAccess DataAccess => base.DataAccess as UserReminderDataAccess;

        internal CustomerReminderCreateManager(UserReminderDataAccess userReminderDataAccess) : base(userReminderDataAccess)
        {
            
        }

        internal async Task CreateReminder(IReminder sales)
        {
            var salesItemGroup = sales.ReminderItems.Where(x => x.ReminderFrequency != (int) UserReminderType.None && x.ReminderFrequency != null).GroupBy(x => x.ReminderDate.ToFormat());

            foreach (var salesItems in salesItemGroup)
            {
                var reminderType = GetReminderType(salesItems);
                var userReminder = await CreateReminder(sales, reminderType, salesItems.FirstOrDefault().ReminderDate);
                await CreateReminderProduct(salesItems, userReminder);
            }
        }

        internal async Task<CustomerReminder> CreateReminder(CustomerReminder reminder)
        {
            var customerReminder = await SaveReminder(reminder);
            foreach (var reminderProduct in customerReminder.ReminderProduct)
            {
                SetMetaData(customerReminder, reminderProduct);
                reminderProduct.UserReminderId = customerReminder.Id;
            }

            await DataAccess.CreateReminderProduct(customerReminder.ReminderProduct);
            return customerReminder;
        }

        private async Task<CustomerReminder> SaveReminder(CustomerReminder reminder)
        {
            await Save(reminder);
            return reminder;
        }

        private UserReminderType GetReminderType(IEnumerable<IReminderItem> salesItems)
        {
            var reminders = salesItems.Select(x => x.SetReminder);
            return GetUserReminderType(reminders);
        }

        private static UserReminderType GetUserReminderType(IEnumerable<UserReminderType> reminders)
        {
            var reminder = UserReminderType.None;

            var userReminderTypes = reminders as UserReminderType[] ?? reminders.ToArray();
            if (userReminderTypes.Any(x => x == UserReminderType.Weekly))
                reminder = reminder | UserReminderType.Weekly;

            if (userReminderTypes.Any(x => x == UserReminderType.Monthly))
                reminder = reminder | UserReminderType.Monthly;

            if (userReminderTypes.Any(x => x == UserReminderType.Yearly))
                reminder = reminder | UserReminderType.Yearly;

            if (userReminderTypes.Any(x => x == UserReminderType.Once))
                reminder = reminder | UserReminderType.Once;

            return reminder;
        }

        private async Task CreateReminderProduct(IEnumerable<IReminderItem> salesItems, UserReminder userReminder)
        {
            var reminderItem = GetReminderItem(salesItems, userReminder);
            await DataAccess.CreateReminderProduct(reminderItem);
        }

        private IEnumerable<ReminderProduct> GetReminderItem(IEnumerable<IReminderItem> salesItems, UserReminder userReminder)
        {
            var reminderItem = salesItems.Select(x =>
            {
                var product = new ReminderProduct
                {
                    ProductStockId = x.ProductStockId,
                    ProductName = x.ProductName,
                    Quantity = x.Quantity,
                    ReminderFrequency = x.ReminderFrequency.Value,
                    UserReminderId = userReminder.Id
                };
                SetMetaData(x, product);
                return product;
            });
            return reminderItem;
        }

        private async Task<UserReminder> CreateReminder(IReminder data, UserReminderType reminder,DateTime? reminderDate)
        {
            var userReminder = new UserReminder
            {
                ReferenceId = data.Id,
                ReminderPhone = data.Mobile,
                CustomerName = data.Name,
                CustomerGender = data.Gender,
                CustomerAge = data.Age.ToString(),
                DoctorName = data.DoctorName,

                Description = "Customer reminder",
                ReminderFrequency = (int)reminder,
                ReminderType = UserReminder.Customer,
                StartDate = GetStartDate(reminder, reminderDate),
                ReminderTime = 9
            };
            SetMetaData(data, userReminder);
            if (userReminder.WriteExecutionQuery)
            {
                DataAccess.SetInsertMetaData(userReminder, UserReminderTable.Table);
                DataAccess.WriteInsertExecutionQuery(userReminder, UserReminderTable.Table);
            }
            else
            {
                await Save(userReminder);
            }
            return userReminder;
        }

        private static DateTime? GetStartDate(UserReminderType reminder, DateTime? reminderDate)
        {
            if (reminderDate.HasValue)
                return reminderDate.Value;

            if (reminder.HasFlag(UserReminderType.Weekly))
                return CustomDateTime.IST.AddDays(7);

            if (reminder.HasFlag(UserReminderType.Monthly))
                return CustomDateTime.IST.AddMonths(1);

            if (reminder.HasFlag(UserReminderType.Yearly))
                return CustomDateTime.IST.AddYears(1);

            return CustomDateTime.IST;
        }
    }
}
