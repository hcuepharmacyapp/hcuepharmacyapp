﻿using HQue.Contract.Infrastructure;
using HQue.Contract.Infrastructure.Accounts;
using HQue.Contract.Infrastructure.Communication;
using HQue.Contract.Infrastructure.Dashboard;
using HQue.Contract.Infrastructure.Inventory;
using HQue.Contract.Infrastructure.Master;
using HQue.Contract.Infrastructure.Setup;
using HQue.DataAccess.Report;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace HQue.Biz.Core.Report
{
    public class SupplierReportManager
    {
        private readonly SupplierReportDataAccess _supplierReportDataAccesss;
        public SupplierReportManager(SupplierReportDataAccess supplierReportDataAccesss)
        {
            _supplierReportDataAccesss = supplierReportDataAccesss;
        }

        public async Task<List<Payment>> SupplierWiseBalanceList(string accountId, string instanceId)
        {
            return await _supplierReportDataAccesss.SupplierWiseBalanceList(accountId, instanceId);
        }
        public async Task<List<Payment>> SupplierDetailsList(Payment data)
        {
            return await _supplierReportDataAccesss.SupplierDetailsList(data);
        }
        public virtual Task<List<Vendor>> GetSupplierName(string supplier, string mobile, string accountid, string instanceid)
        {
            return _supplierReportDataAccesss.GetSupplierName(supplier, mobile, accountid, instanceid);           
        }
        public virtual Task<List<Vendor>> GetSupplierByInstance(string accountid, string instanceid)
        {
            return _supplierReportDataAccesss.GetSupplierByInstance(accountid, instanceid);           
        }

        public async Task<List<VendorPurchaseReturn>> SupplierWisePurchaseReturnDetail(SupplierFilter data, bool IsInvoiceDate)
        {
            return await _supplierReportDataAccesss.SupplierWisePurchaseReturnDetail(data, IsInvoiceDate);
        }
        public async Task<List<Payment>> SupplierPaymentDetails(Payment data)
        {
            return await _supplierReportDataAccesss.SupplierPaymentDetails(data);
        }

        public async Task<List<Payment>> SupplierLedgerDetails(Payment data)
        {
            return await _supplierReportDataAccesss.SupplierLedgerDetails(data);
        }
        public async Task<List<VendorPurchaseReturn>> SupplierWiseExpiryReturnDetail(SupplierFilter data)
        {
            return await _supplierReportDataAccesss.SupplierWiseExpiryReturnDetail(data);
        }

        public async Task<List<Payment>> supplierWisePaymentReceiptCancellationList(Payment data)
        {
            return await _supplierReportDataAccesss.supplierWisePaymentReceiptCancellationList(data);
        }
    }
}
