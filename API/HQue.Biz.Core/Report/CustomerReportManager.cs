﻿using HQue.Contract.Infrastructure;
using HQue.Contract.Infrastructure.Accounts;
using HQue.Contract.Infrastructure.Communication;
using HQue.Contract.Infrastructure.Dashboard;
using HQue.Contract.Infrastructure.Inventory;
using HQue.Contract.Infrastructure.Setup;
using HQue.DataAccess.Report;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace HQue.Biz.Core.Report
{
    public class CustomerReportManager
    {
        private readonly CustomerReportDataAccess _customerReportDataAccess;
        public CustomerReportManager(CustomerReportDataAccess customerReportDataAccess)
        {
            _customerReportDataAccess = customerReportDataAccess;
        }


        public async Task<List<CustomerPayment>> CustomerWiseBalanceList(string accountId, string instanceId)
        {
            return await _customerReportDataAccess.CustomerWiseBalanceList(accountId, instanceId);
        }
        public async Task<List<CustomerPayment>> customerDetailsList(CustomerPayment data)
        {
            return await _customerReportDataAccess.customerDetailsList(data);
        }

        public async Task<List<PettyCashHdr>> GetPettycashDates(string AccountId, string InstanceId, string UserId, DateTime selectedDate)
        {
            return await _customerReportDataAccess.GetPettycashDates(AccountId, InstanceId, UserId, selectedDate);
        }

        public async Task<List<dynamic>> GetPettycashDetails(string pettyId)
        {
            return await _customerReportDataAccess.GetPettycashDetails(pettyId);
        }

        public virtual Task<List<CustomerPayment>> GetCustomerList(string customer,string mobile, string accountid, string instanceid)
        {
            return _customerReportDataAccess.GetCustomerList(customer, mobile, accountid, instanceid);           
        }
        
        public async Task<List<CustomerPayment>> customerWisePaymentReceiptCancellationList(CustomerPayment data)
        {
            return await _customerReportDataAccess.customerWisePaymentReceiptCancellationList(data);
        }

        public async Task<List<CustomerPayment>> customerPaymentDetails(CustomerPayment data)
        {
            return await _customerReportDataAccess.customerPaymentDetails(data);
        }
    }
}
