﻿using HQue.Contract.Infrastructure;
using HQue.Contract.Infrastructure.Audits;
using HQue.Contract.Infrastructure.Communication;
using HQue.Contract.Infrastructure.Dashboard;
using HQue.Contract.Infrastructure.Inventory;
using HQue.Contract.Infrastructure.Master;
using HQue.Contract.Infrastructure.Reports;
using HQue.Contract.Infrastructure.Settings;
using HQue.Contract.Infrastructure.Setup;
using HQue.Contract.Infrastructure.UserAccount;
using HQue.DataAccess.Report;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace HQue.Biz.Core.Report
{
    public class ReportManager
    {
        private readonly ReportDataAccess _reportDataAccess;
        public ReportManager(ReportDataAccess reportDataAccess)
        {
            _reportDataAccess = reportDataAccess;
        }

        //Created by Manivannan on 19-Jul-2017
        public async Task<List<Sales>> SalesReportDetailMultiLevel(string type, string accountId, string instanceId, int level, DateTime from, DateTime to, Sales data)
        {
            return await _reportDataAccess.SalesReportDetailMultiLevel(type, accountId, instanceId, level, from, to, data);
        }
        //Created by Bikas on 14/06/2018
        public async Task<List<Sales>> BranchWiseSalesReportDetailMultiLevel(string type, string accountId, string instanceId, int level, DateTime from, DateTime to, Sales data)
        {
            return await _reportDataAccess.BranchWiseSalesReportDetailMultiLevel(type, accountId, instanceId, level, from, to, data);
        }
        //Created by Manivannan on 24-Jul-2017
        public async Task<List<Sales>> SalesReportTaxGroupMultiLevel(string type, string accountId, string instanceId, int level, DateTime from, DateTime to, Sales data, double gstTotal)
        {
            return await _reportDataAccess.SalesReportTaxGroupMultiLevel(type, accountId, instanceId, level, from, to, data, gstTotal);
        }

        //Created by Sarubala on 26-Jul-2017 - start
        public async Task<Tuple<List<Sales>, List<Sales>>> ItemWiseSalesReportMultiLevel(string type, string accountId, string instanceId, int level, DateTime from, DateTime to, Sales data, string productId)
        {
            return await _reportDataAccess.ItemWiseSalesReportMultiLevel(type, accountId, instanceId, level, from, to, data, productId);
        }
        //Created by Sarubala on 26-Jul-2017 - end

        public async Task<List<SalesItem>> SalesReportList(string type, string accountId, string instanceId, DateTime from, DateTime to,string kind)
        {
            return await _reportDataAccess.SalesReportList(type, accountId, instanceId, from, to, kind);
        }

        public async Task<List<SalesItem>> SalesReportCustomerDetailList(string Name,string Mobile, string accountId, string instanceId, DateTime from, DateTime to)
        {
            return await _reportDataAccess.SalesReportCustomerDetailList(Name, Mobile, accountId, instanceId, from, to, null, null, null);
        }
        
        public async Task<List<SalesItem>> SalesProductwiseListData(string id,string Name,string Mobile, string accountId, string instanceId, DateTime from, DateTime to)
        {
            return await _reportDataAccess.SalesProductwiseListData(id,Name,Mobile, accountId, instanceId, from, to);
        }

        public async Task<List<Sales>> NonSaleCustomersListData(string accountId, string instanceId, DateTime from, DateTime to) 
        {
            return await _reportDataAccess.PatientListData(accountId, instanceId, from, to);
        }

        public async Task<List<VendorPurchaseItem>> BuyReportList(string type, string accountId, string instanceId, string sProductId, string sVendorId, DateTime from, DateTime to, bool IsInvoiceDate)
        {
            return await _reportDataAccess.BuyReportList(type, accountId, instanceId,  sProductId,  sVendorId, from, to, IsInvoiceDate);
        }
        public async Task<List<DcVendorPurchaseItem>> DcListData(string type, string accountId, string instanceId, string sProductId, string sVendorId, DateTime from, DateTime to)
        {
            return await _reportDataAccess.DcListData(type, accountId, instanceId,  sProductId,  sVendorId, from, to);
        }


        //Purchase Margin Report created by Poongodi on 20/07/2017
        public async Task<List<VendorPurchaseItem>> BuyMarginList(string type, string accountId, string instanceId , DateTime from, DateTime to, bool IsInvoiceDate)
        {
            return await _reportDataAccess.BuyMarginList(type, accountId, instanceId , from, to, IsInvoiceDate);
        }
        
        public async Task<List<VendorPurchaseItem>> BuyReporProductwisetList(string id, string accountId, string instanceId, DateTime from, DateTime to)
        {
            return await _reportDataAccess.BuyReporProductwisetList(id, accountId, instanceId, from, to);
        }
        public async Task<List<VendorPurchaseItem>> BuyReportVendorwiseList(string id, string accountId, string instanceId, DateTime from, DateTime to)
        {
            return await _reportDataAccess.BuyReportVendorwiseList(id, accountId, instanceId, from, to);
        }

        public async Task<List<getAllPharmaItem>> getAllPharmaList(string type, DateTime from, DateTime to,int registerType)
        {
            return await _reportDataAccess.getAllPharmaList(type, from, to, registerType);
        }

   

        public async Task<List<VendorPurchaseItem>> VendorView(string accountId, string instanceId, string id)
        {
            return await _reportDataAccess.VendorView( accountId, instanceId,id);
        }

        public async Task<List<VendorPurchaseItem>> BuySummaryList(string type, string accountId, string instanceId, DateTime from, DateTime to, bool IsInvoiceDate)
        {
            return await _reportDataAccess.BuySummaryList(type, accountId, instanceId, from, to, IsInvoiceDate);
        }

        public async Task<List<ProductPurchaseSalesVM>> BuySalesList(string type, string accountId, string instanceId, DateTime from, DateTime to)
        {
            return await _reportDataAccess.BuySalesList(type, accountId, instanceId, from, to);
        }
        public async Task<List<VendorPurchaseReturn>> ReturnSummaryListData(string type, string accountId, string instanceId, DateTime from, DateTime to, bool IsInvoiceDate)
        {
            return await _reportDataAccess.ReturnSummaryListData(type, accountId, instanceId, from, to, IsInvoiceDate);
        }

        public async Task<List<VendorOrderItem>> PurchaseOrderData(string type,VendorOrder order1)
        {
            return await _reportDataAccess.PurchaseOrderData(type, order1);
        }

        public async Task<List<ProductStock>> LowProductStockReportList(string instanceId)
        {
            return await _reportDataAccess.LowProductStockReportList(instanceId);
        }

        public async Task<PagerContract<Instance>> DistributorBranchList(params object[] parameters)
        {
            var pager = new PagerContract<Instance>
            {
                NoOfRows = await _reportDataAccess.DistributorBranchCount(parameters),
                List = await _reportDataAccess.DistributorBranchList(parameters)
            };

            return pager;
        }

        public async Task<List<ProductStock>> DistributorLowProductStockList(params object[] parameters)
        {
            return await _reportDataAccess.DistributorLowProductStockList(parameters);
        }
        //date parameter added by nandhini 13-9-17
        public async Task<List<ProductStock>> ExpireProductList(string instanceId, DateTime from, DateTime to)
        {
            return await _reportDataAccess.ExpireProductList(instanceId, from, to);
        }

        public async Task<List<SalesReturnItem>> SalesReturnReportList(string type, string accountId, string instanceId, DateTime from, DateTime to,int nIssummary,int fromvalues, int tovalues, int SearchType,string kind)
        {
            return await _reportDataAccess.SalesReturnReportList(type, accountId, instanceId, from, to, nIssummary, fromvalues, tovalues, SearchType, kind);
        }

        public async Task<List<VendorPurchaseReturnItem>> BuyReturnReportList(string type, string accountId, string instanceId, DateTime from, DateTime to, bool IsInvoiceDate)
        {
            return await _reportDataAccess.BuyReturnReportList(type, accountId, instanceId, from, to, IsInvoiceDate);
        }

        public async Task<List<TempVendorPurchaseItem>> TempStockListData(string type, string accountId, string instanceId,string productId, DateTime from, DateTime to)
        {
            return await _reportDataAccess.TempStockListData(type, accountId, instanceId, productId, from, to);
        }
        //added by nandhini for product list report
        public async Task<List<Product>> productListData(string type,string accountId, string instanceId)
        {
            return await _reportDataAccess.productListData( type,accountId, instanceId);
        }

        public async Task<List<Sales>> HomeDeliveryList(string type, string accountId, string instanceId, DateTime from, DateTime to, string invoiceType, string invoiceSearchType, int fromInvoice, int toInvoice)
        {
            return await _reportDataAccess.HomeDeliveryList(type, accountId, instanceId, from, to, invoiceType, invoiceSearchType, fromInvoice, toInvoice);
        }
        //add by nandhini for door delivery report
        public async Task<List<Sales>> doorDeliveryListData(string type, string accountId, string instanceId, DateTime from, DateTime to)
        {
            return await _reportDataAccess.doorDeliveryListData(type, accountId, instanceId, from, to);
        }

        public async Task<List<SalesItem>> SalesList(string type, string accountId, string instanceId, DateTime from, DateTime to, string invoiceType, string invoiceSearchType, int fromInvoice, int toInvoice)
        {
            return await _reportDataAccess.SalesList(type, accountId, instanceId, from, to, invoiceType, invoiceSearchType, fromInvoice, toInvoice);
        }
        
         public async Task<List<SalesItem>> SalesCustomerWiseList(string Name,string Mobile, string accountId, string instanceId, DateTime from, DateTime to)
        {
            var type = "CUSTOMERWISE";
            return await _reportDataAccess.SalesCustomerWiseList(Name, Mobile, accountId, instanceId, from, to, null, null, type);
        }
        

        public async Task<List<Sales>> SalesSummaryList(string type, string accountId, string instanceId, DateTime from, DateTime to, string InvoiceSeries, int fromInvoice, int toInvoice, int paymentType)
        {
            return await _reportDataAccess.SalesSummaryList(type, accountId, instanceId, from, to, InvoiceSeries, fromInvoice, toInvoice, paymentType);
        }

        public async Task<List<SalesItem>> SalesSummaryCustomerwiseList(string Name, string Mobile, string accountId, string instanceId, DateTime from, DateTime to)
        {
            var type = "CUSTOMERWISE";
            return await _reportDataAccess.SalesSummaryCustomerwiseList(Name, Mobile, accountId, instanceId, from, to, null, null, null, type);
        }
        public async Task<List<SalesReturnItem>> SalesSummaryReturnList(string type, string accountId, string instanceId, DateTime from, DateTime to)
        {
            return await _reportDataAccess.SalesSummaryReturnList(type, accountId, instanceId, from, to);
        }

        //Added by Sarubala on 28-08-17 - start
        public async Task<List<Doctor>> DoctorwiseReportList(string type, string accountId, string instanceId, DateTime from, DateTime to, string filterType, string doctorId, string productId)
        {
            return await _reportDataAccess.DoctorwiseReportList(type, accountId, instanceId, from, to, filterType, doctorId, productId);
        }
        //Added by Sarubala on 28-08-17 - end

        public async Task<List<SalesReturnItem>> BillCancelListData(string accountId, string instanceId,SalesReturnItem sri, string type)
        {
            return await _reportDataAccess.BillCancelListData(accountId, instanceId,sri, type);
        }
        //Invoice Series and Product Addedby Settu on29/05/2017
        public async Task<List<SalesItem>> DrugWiseReportList(string type, string accountId, string instanceId, DateTime from, DateTime to, string invoiceSeries, int fromInvoice, int toInvoice, string productVal)
        {
            return await _reportDataAccess.DrugWiseList(type, accountId, instanceId, from, to, invoiceSeries, fromInvoice, toInvoice, productVal);
        }

        public async Task<List<Contract.Infrastructure.Leads.Leads>> LeadsReportList(string type, string accountId, string instanceId, DateTime from, DateTime to)
        {
            return await _reportDataAccess.LeadsReportList(type, accountId, instanceId, from, to);
        }
        //Stock Type parameter added by Poongodi on 13/03/2017
       //Accountid, Rack, Box no parameter added by Poongodi on 21/07/2017
        public async Task<List<ProductStock>> StockReportList(string instanceId, string accountId, string sStockType, bool isProductWise, string category, string manufacturer, string sRackNo, string sBoxNo, string alphabet1, string alphabet2, DateTime from, DateTime to)
        {
            return await _reportDataAccess.StockReportList(instanceId, accountId,sStockType, isProductWise, category, manufacturer, sRackNo,sBoxNo, alphabet1, alphabet2, from, to);
        }

        public async Task<List<Instance>> StockAllBranchConsolidatedList(string accountId)
        {
            return await _reportDataAccess.StockAllBranchConsolidatedList(accountId);
        }

        public async Task<List<ProductStock>> StockReportCategoryList(string instanceId, string sStockType, string category) 
        {
            return await _reportDataAccess.StockReportCategoryList(instanceId, sStockType, category); 
        }

        public async Task<List<ProductStock>> StockReportManufacturerList(string instanceId, string sStockType, string manufacturer)
        {
            return await _reportDataAccess.StockReportManufacturerList(instanceId, sStockType, manufacturer); 
        }
        /// <summary>
        /// Get Rack and Box no
        /// </summary>
        /// <param name="instanceId"></param>
        /// <param name="sStockType"></param>
        /// <param name="sSearchText"></param>
        /// <param name="sSearchType"></param>
        /// <returns></returns>
        public async Task<List<ProductStock>> StockProductListByLocation(string instanceId, string sStockType, string sSearchText, string sSearchType)
        {
            return await _reportDataAccess.StockProductListByLocation(instanceId, sStockType,   sSearchText,   sSearchType);
        }
        public async Task<List<StockLedger>> GetStockLedger(StockLedger data, string AccId, string InsId)
        {
            return await _reportDataAccess.GetStockLedger(data, AccId, InsId);
        }

        public async Task<List<StockAdjustment>> GetStockAdjustmentReport(StockAdjustment sa, string type)
        {
            return await _reportDataAccess.GetStockAdjustmentReport(sa, type);
        }

        public async Task<List<StockValueReport>> GetStockValueReport(string accId, string insId)
        {
            return await _reportDataAccess.GetStockValueReport(accId, insId);
        }

        public async Task<List<StockValueReport>> GetSavedReport(StockValueReport svr)
        {
            return await _reportDataAccess.GetSavedReport(svr);
        }

        public async Task<List<StockValueReport>> SaveStockValueReport(List<StockValueReport> svlist)
        {
            return await _reportDataAccess.SaveStockValueReport(svlist);
        }

        public async Task<List<StockValueReport>> GetUniqueSavedReport(string reportNo, string insId, string accId)
        {
            return await _reportDataAccess.GetUniqueSavedReport(reportNo, insId, accId);
        }
        public async Task<List<ProductStock>> NonMovingStockReportList(string instanceId, DateTime from, DateTime to)//int stockMonth 
        {
            return await _reportDataAccess.NonMovingStockReportList(instanceId, from, to);
        }
        public async Task<List<ProductStock>> UpdateIsMovingStock(List<ProductStock> productStock)
        {
            return await _reportDataAccess.UpdateIsMovingStock(productStock);
        }
        //Newly Added Gavaskar 25-10-2016 Start
        public async Task<List<ProductStock>> UpdateIsMovingStockExpire(List<ProductStock> productStock)
        {
            return await _reportDataAccess.UpdateIsMovingStockExpire(productStock);
        }

        public async Task<ProductStock[]> vendorExpireUpdate(List<ProductStock> productStocks)
        {
            return await _reportDataAccess.vendorExpireUpdate(productStocks);
        }

        public async Task<ProductStock[]> vendorNonMovingUpdate(List<ProductStock> productStocks)
        {
            return await _reportDataAccess.vendorNonMovingUpdate(productStocks);
        }

        //Newly Added Gavaskar 25-10-2016 End
        public async Task<List<SalesItem>> SalesUserList(SalesItem data, string accountId, string instanceId)
        {
            return await _reportDataAccess.SalesUserList(data, accountId, instanceId);
        }

        public async Task<List<CommunicationLog>> SmsUserList(string accountId, string instanceId)
        {
            return await _reportDataAccess.SmsUserList(accountId, instanceId);
        }

        public async Task<List<CommunicationLog>> SmsBranchList(string type, string accountId, string instanceId, DateTime from, DateTime to)
        {
            return await _reportDataAccess.SmsBranchList(type, accountId, instanceId, from, to);
        }
        public async Task<List<SalesItem>> ScheduleReportList(string type, string accountId, string instanceId, string schedule, DateTime from, DateTime to, string search,string frombillno, string tobillno)
        {
            return await _reportDataAccess.ScheduleReportList(type,accountId, instanceId, schedule, from, to, search,frombillno, tobillno);
        }
        public async Task<List<SalesItem>> ScheduleReportListforPrint(string type, string accountId, string instanceId, string schedule, DateTime? from, DateTime? to, string frombillno, string tobillno,string search)
        {
            return await _reportDataAccess.ScheduleReportListforPrint(type, accountId, instanceId, schedule, from, to, frombillno, tobillno, search);
        }

        public async Task<List<VendorPurchaseItem>> PurchaseReportList(string type, string accountId, string instanceId, DateTime from, DateTime to,int filter)
        {
            return await _reportDataAccess.PurchaseReportList(type, accountId, instanceId, from, to,filter);
        }
        public async Task<List<VendorPurchaseReturnItem>> PurchaseReturnReportList(string type, string accountId, string instanceId, DateTime from, DateTime to)
        {
            return await _reportDataAccess.PurchaseReturnReportList(type, accountId, instanceId, from, to);
        }

        public async Task<List<SalesItem>> SalesReportListData(string type, string accountId, string instanceId, DateTime from, DateTime to)
        {
            return await _reportDataAccess.SalesReportListData(type, accountId, instanceId, from, to);
        }

        public async Task<List<SalesReturnItem>> SalesReturnList(string type, string accountId, string instanceId, DateTime from, DateTime to)
        {
            return await _reportDataAccess.SalesReturnList(type, accountId, instanceId, from, to);
        }

        public async Task<List<VendorPurchaseItem>> PurchaseReportListCst(string type, string accountId, string instanceId, DateTime from, DateTime to)
        {
            return await _reportDataAccess.PurchaseReportListCst(type, accountId, instanceId, from, to);
        }
        public async Task<List<VendorPurchaseReturnItem>> PurchaseReturnReportListCst(string type, string accountId, string instanceId, DateTime from, DateTime to)
        {
            return await _reportDataAccess.PurchaseReturnReportListCst(type, accountId, instanceId, from, to);
        }
        public async Task<List<SalesItem>> LocalSalesReportListData(string type, string accountId, string instanceId, DateTime from, DateTime to)
        {
            return await _reportDataAccess.LocalSalesReportListData(type, accountId, instanceId, from, to);
        }
        public async Task<List<SalesItem>> LocalSaleConsolidatedVATListData(string type, string accountId, string instanceId, DateTime from, DateTime to)
        {
          return await _reportDataAccess.LocalSaleConsolidatedVATListData(type, accountId, instanceId, from, to);
            
        }
        public async Task<List<StockInventoryData>> StockInventoryList(string accountId, string instanceId)
        {
            return await _reportDataAccess.StockInventoryList(accountId, instanceId);
        }
        public async Task<List<SalesReturn>> GetReturnsByDate(string type, string accountId, string instanceId, DateTime from, DateTime to, string invoiceType, string invoiceSearchType, int fromInvoice, int toInvoice)
        {
            return await _reportDataAccess.GetReturnsByDate(type, accountId, instanceId, from, to,invoiceType, invoiceSearchType,fromInvoice, toInvoice);
        }
        public async Task<List<SalesReturn>> GetSalesDetailReturnsByDate(string type, string accountId, string instanceId, DateTime from, DateTime to)
        {
            return await _reportDataAccess.GetSalesDetailReturnsByDate(type, accountId, instanceId, from, to);
        }

        //New Multi-location reports
        public async Task<List<SalesSummaryDayWiseReport>> SalesSummaryListData(string accountId, string type, bool allInstances, List<string> instanceIds, DateTime from, DateTime to)
        {
            return await _reportDataAccess.GetSalesSummaryListData(accountId, type, allInstances, instanceIds, from, to);
            
        }
        public async Task<List<SalesItem>> GetDoctorSalesList(string AccId, string DoctorId)
        {
            return await _reportDataAccess.GetDoctorSalesList(AccId, DoctorId);
        }
        public async Task<List<ProductStock>> ProductAgeAnalyseListData(string type, string accountId, string instanceId, string productId, DateTime from, DateTime to)
        {
            return await _reportDataAccess.ProductAgeAnalyseListData(type, accountId, instanceId, productId, from, to);
        }


        public async Task<List<SelfConsumption>> selfConsumptionListData(string type, string accountId, string instanceId, string productId, string invoiceNo, DateTime from, DateTime to)
        {
            return await _reportDataAccess.selfConsumptionListData(type, accountId, instanceId, productId, invoiceNo, from, to);
        }

        public async Task<List<SalesItem>> SalesMarginProductWiseList(string filter, string accountId, string instanceId, DateTime from, DateTime to)
        {
            return await _reportDataAccess.SalesMarginProductWiseList(filter, accountId, instanceId, from, to);
        }
        // Added Gavaskar 06-03-2017
        public async Task<List<VendorPurchaseItemAudit>> PurchaseModifiedList(VendorPurchaseItemAudit data, string AccountId, string sInstanceId)
        {
            return await _reportDataAccess.PurchaseModifiedList(data, AccountId, sInstanceId);
        }

        // Added Gavaskar 04-03-2017
        public async Task<List<InvoiceSeriesItem>> GetInvoiceSeriesItems(InvoiceSeriesItem IS, bool flag)
        {
            return await _reportDataAccess.getInvoiceSeriesItems(IS, flag);
        }

        //Created by Sarubala on 21-Jul-2017
        public async Task<Tuple<List<VendorPurchase>, List<VendorPurchase>>> PurchaseReportDetailMultiLevel(string type, string accountId, string instanceId, int level, DateTime from, DateTime to, VendorPurchase data)
        {
            return await _reportDataAccess.PurchaseReportDetailMultiLevel(type, accountId, instanceId, level, from, to, data);
        }

        public async Task<List<SalesReturn>> GetReturnsByDateSalesList(string type, string accountId, string instanceId, DateTime from, DateTime to, string invoiceType, string invoiceSearchType)
        {
            return await _reportDataAccess.GetReturnsByDateSalesList(type, accountId, instanceId, from, to, invoiceType, invoiceSearchType);
        }

        public virtual Task<List<HQueUser>> GetUserName(string data, string accountid, string instanceid)
        {
            var plist = _reportDataAccess.GetUserName(data, accountid, instanceid);
            return plist;
        }

        public virtual Task<List<HQueUser>> GetUserId(string data, string accountid, string instanceid)
        {
            var plist = _reportDataAccess.GetUserId(data, accountid, instanceid);
            return plist;
        }

        public async Task<List<Sales>> consolidatedsalesReportList(string type, string accountId, string instanceId, DateTime from, DateTime to, string invoiceType, string cashType)
        {
            return await _reportDataAccess.consolidatedsalesReportList(type, accountId, instanceId, from, to, invoiceType, cashType);
        }
        //add by nandhini for consolidated sales return report 9/6/17
        public async Task<List<SalesReturn>> consolidatedsalesReturnReportList(string type, string accountId, string instanceId, DateTime from, DateTime to, string invoiceType, string cashType)
        {
            return 
                await _reportDataAccess.consolidatedsalesReturnReportList(type, accountId, instanceId, from, to, invoiceType, cashType);
        }

        public async Task<List<StockTransferItem>> StockTransferList(string type, string accountId, string instanceId, DateTime from, DateTime to)
        {
            return await _reportDataAccess.StockTransferList(type, accountId, instanceId, from, to);
        }

        
        public async Task<List<PhysicalStockHistory>> physicalStockHistoryList(DateTime FromDate, DateTime ToDate, string type, string AccountId, string InstanceId, string ProductId, string UserName, string UserId)
        {
            return await _reportDataAccess.physicalStockHistoryList(FromDate, ToDate, type, AccountId, InstanceId, ProductId, UserName, UserId);
        }
        //Added by Poongodi on 14/11/2017
        public async Task<string> ClosingStockLog( string AccountId, string InstanceId  )
        {
            return await _reportDataAccess.ClosingStockLog( AccountId, InstanceId);
        }

        public async Task<List<StockMovement>> StockMovementList(string type, string AccountId, string InstanceId, DateTime FromDate, DateTime ToDate)
        {
            return await _reportDataAccess.StockMovementList(type, AccountId, InstanceId, FromDate, ToDate );
        }

        public async Task<List<StockTransferItem>> StockTransferDetail(string type, string accountId, string instanceId, DateTime from, DateTime to,string sReportType)
        {
            if (sReportType.ToLower()=="summary")
            {

                return await _reportDataAccess.StockTransfeSummary(type, accountId, instanceId, from, to);
            }
            else if (sReportType.ToLower() == "detail")
            {

                return await _reportDataAccess.StockTransferProductDetail(type, accountId, instanceId, from, to);
            }
            else { 
            return await _reportDataAccess.StockTransferDetail(type, accountId, instanceId, from, to);
            }
        }
        /// <summary>
        /// GST 3BReport 
        /// </summary>
        /// <param name="type"></param>
        /// <param name="accountId"></param>
        /// <param name="instanceId"></param>
        /// <param name="from"></param>
        /// <param name="to"></param>
        /// <param name="filter"></param>
        /// <returns></returns>
        public async Task<List<GSTReport>> GST3BReportList(string type, string accountId, string instanceId, DateTime from, DateTime to, int filter, string sSearchType, string ssGsttinNo , int nInvoiceDt)
        {
            return await _reportDataAccess.GST3BReportList(type, accountId, instanceId, from, to, filter,sSearchType,ssGsttinNo, nInvoiceDt);
        }

        // added for GST REPORTS by Nandhini on 29/08/2017
        public async Task<List<GSTReport>> hsnReportList(string accountId)
        {
            return await _reportDataAccess.hsnReportList( accountId);
        }

   
        public async Task<List<ControlReport>> controlReportList(string accountId, string instanceId, DateTime from, DateTime to, int filter)
        {
            return await _reportDataAccess.controlReportList(accountId, instanceId, from, to, filter);
        }
        /*GSTR -1 Report Added by Poongodi on 30/08/2017*/
        public async Task<List<GSTReport>> GSTR1PurchaseList(string type, string accountId, string instanceId, DateTime from, DateTime to, int filter, string sSearchType, string ssGsttinNo, int nSummary, int nRegister, int nInvoiceDt)
        {
            return await _reportDataAccess.GSTR1PurchaseList(type, accountId, instanceId, from, to, filter, sSearchType, ssGsttinNo, nSummary, nRegister, nInvoiceDt);
        }
        /*GSTR -2 Report Added by Poongodi on 05/09/2017*/
        public async Task<List<GSTReport>> GSTR2PurchaseReturn(string type, string accountId, string instanceId, DateTime from, DateTime to, int filter, string sSearchType, string ssGsttinNo, int nRegister, int nInvoiceDt)
        {
            return await _reportDataAccess.GSTR2PurchaseReturn(type, accountId, instanceId, from, to, filter, sSearchType, ssGsttinNo, nRegister, nInvoiceDt);
        }
        
               public async Task<List<GSTReport>> GSTR3BPurchaseReturnList(string type, string accountId, string instanceId, DateTime from, DateTime to, int filter, string sSearchType, string ssGsttinNo, int nRegister, int nInvoiceDt)
        {
            return await _reportDataAccess.GSTR3BPurchaseReturnList(type, accountId, instanceId, from, to, filter, sSearchType, ssGsttinNo, nRegister, nInvoiceDt);
        }

        public async Task<List<GSTReport>> GSTR3BDetailsList(string type, string accountId, string instanceId, DateTime from, DateTime to, int filter, string sSearchType, string ssGsttinNo, int nRegister, int nInvoiceDt)
        {
            return await _reportDataAccess.GSTR3BDetailsList(type, accountId, instanceId, from, to, filter, sSearchType, ssGsttinNo, nRegister, nInvoiceDt);
        }
        public async Task<List<GSTReport>> GSTR3BSalesDetailsList(string type, string accountId, string instanceId, DateTime from, DateTime to, int filter, string sSearchType, string ssGsttinNo,int nSummary)
        {
            return await _reportDataAccess.GSTR3BSalesDetailsList(type, accountId, instanceId, from, to, filter, sSearchType, ssGsttinNo, nSummary);
        }
        
               public async Task<List<GSTReport>> GSTR3BSalesReturnList(string type, string accountId, string instanceId, DateTime from, DateTime to, int filter, string sSearchType, string ssGsttinNo, int nSummary)
        {
            return await _reportDataAccess.GSTR3BSalesReturnList(type, accountId, instanceId, from, to, filter, sSearchType, ssGsttinNo,nSummary);
        }
        public async Task<List<GSTReport>> GSTR1SalesList(string type, string accountId, string instanceId, DateTime from, DateTime to, int filter, string sSearchType, string ssGsttinNo, int IsSummary)
        {
            return await _reportDataAccess.GSTR1SalesList(type, accountId, instanceId, from, to, filter, sSearchType, ssGsttinNo, IsSummary);// Added by Sarubala on 04-09-17
        }
        public async Task<List<GSTReport>> GSTR1SalesReturn(string type, string accountId, string instanceId, DateTime from, DateTime to, int filter, string sSearchType, string ssGsttinNo, int nSummary)
        {
            return await _reportDataAccess.GSTR1SalesReturn(type, accountId, instanceId, from, to, filter, sSearchType, ssGsttinNo,nSummary);
        }
        //Added By Bikas for GST Report on 06/06/18
        public async Task<List<SalesCoordinateGst>> LocalSalesConsolidatedGSTListData(string type, string accountId, string instanceId, DateTime from, DateTime to)
        {
            return await _reportDataAccess.LocalSalesConsolidatedGSTListData(type, accountId, instanceId, from, to);

        }
        //Added By Bikas for GST Report on 07/06/18
        public async Task<List<SalesCoordinateGstReturn>> LocalSalesConsolidatedGSTReturnListData(string type, string accountId, string instanceId, DateTime from, DateTime to)
        {
            return await _reportDataAccess.LocalSalesConsolidatedGSTReturnListData(type, accountId, instanceId, from, to);

        }
        //Added By Bikas for GST Report on 20/06/18
        public async Task<List<PurchaseConsolidatedGst>> LocalPurchaseConsolidatedGSTListData(string type, string accountId, string instanceId, DateTime from, DateTime to)
        {
            return await _reportDataAccess.LocalPurchaseConsolidatedGSTListData(type, accountId, instanceId, from, to);

        }
        //Added By Bikas for GST Report on 21/06/18
        public async Task<List<PurchaseReturnConsolidatedGst>> LocalPurchaseReturnConsolidatedGSTListData(string type, string accountId, string instanceId, DateTime from, DateTime to)
        {
            return await _reportDataAccess.LocalPurchaseReturnConsolidatedGSTListData(type, accountId, instanceId, from, to);

        }

        //Added By Bikas for Tally Report on 22/06/18
        public async Task<List<SalesReportTally>> LocalSalesReportTallyListData(string type, string accountId, string instanceId, DateTime from, DateTime to)
        {
            return await _reportDataAccess.LocalSalesReportTallyListData(type, accountId, instanceId, from, to);

        }

        //Added By Bikas for Tally Report on 25/06/18
        public async Task<List<SalesreturnReportTally>> LocalSalesReturnReportTallyListData(string type, string accountId, string instanceId, DateTime from, DateTime to)
        {
            return await _reportDataAccess.LocalSalesReturnReportTallyListData(type, accountId, instanceId, from, to);

        }
        //Added By Bikas for Tally Report on 26/06/18
        public async Task<List<PurchaseReportTally>> LocalPurchaseReportTallyListData(string type, string accountId, string instanceId, DateTime from, DateTime to)
        {
            return await _reportDataAccess.LocalPurchaseReportTallyListData(type, accountId, instanceId, from, to);

        }

        //Added By Bikas for Tally Report on 27/06/18
        public async Task<List<PurchaseReturnReportTally>> LocalPurchaseReturnReportTallyListData(string type, string accountId, string instanceId, DateTime from, DateTime to)
        {
            return await _reportDataAccess.LocalPurchaseReturnReportTallyListData(type, accountId, instanceId, from, to);

        }


        //Added By Bikas for Tally Report on 28/06/18
        public async Task<List<JournalReport>> LocalJournalListData(string filterType, string type,string accountId, string instanceId, DateTime from, DateTime to)
        {
            return await _reportDataAccess.LocalJournalListData(filterType, type, accountId, instanceId, from, to);

        }

        public async Task<List<GSTIn>> GetGstinData(string accountId)
        {
            return await _reportDataAccess.GetGstinData(accountId);
        }



        public async Task<string[]> ValidateGSTR3B(string accountId)
        {
            return await _reportDataAccess.ValidateGSTR3B(accountId);
        }

        public async Task<List<ProductStock>> StockListDataForPrint(string instanceId, string accountId, string sStockType, bool isProductWise, string category, string manufacturer, string sRackNo, string sBoxNo, string alphabet1, string alphabet2, DateTime? from, DateTime? to)
        {
            return await _reportDataAccess.StockListDataForPrint(instanceId, accountId, sStockType, isProductWise, category, manufacturer, sRackNo, sBoxNo, alphabet1, alphabet2, from, to);
        }

        public async Task<List<Sales>> gstr4TaxRateWiseTurnOver(string accountId, string instanceId, DateTime from, DateTime to, string sGsttinNo)
        {
            return await _reportDataAccess.gstr4TaxRateWiseTurnOver(accountId, instanceId, from, to, sGsttinNo);
        }
    }
}
