﻿using HQue.Contract.Infrastructure.Reports;
using HQue.DataAccess.Report;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace HQue.Biz.Core.Report
{
    public class CustomReportManager
    {
        private readonly CustomReportDataAccess _customReportDataAccess;

        public CustomReportManager(CustomReportDataAccess customReportDataAccess)
        {
            _customReportDataAccess = customReportDataAccess;
        }

        public async Task<CustomReport> Save(CustomReport model)
        {
            await _customReportDataAccess.Save(model);
            return model;
        }

        public async Task<CustomDevReport> Save(CustomDevReport model)
        {
            await _customReportDataAccess.SaveDev(model);
            return model;
        }
        public async Task<CustomDevReport> UpdateReport(CustomDevReport model)
        {
            await _customReportDataAccess.UpdateReport(model);
            return model;
        }

        public async Task<List<CustomReport>> List(string instanceId)
        {
            return await _customReportDataAccess.List(instanceId);
        }

        public async Task<CustomDevReport> EditReportData(string id)
        {
            return await _customReportDataAccess.EditReportData(id);
        }

        public CustomReport GetCustomReport(string id)
        {
            return _customReportDataAccess.GetCustomReport(id);
        }

        public async Task<JArray> GetReportData(string query)
        {
            return await _customReportDataAccess.GetReportData(query);
        }
        public async Task<List<CustomDevReport>> GetDevReportList(string instanceId)
        {
            return await _customReportDataAccess.GetDevReportList(instanceId); ;
        }
        public CustomDevReport GetDevCustomReport(string id)
        {
            return _customReportDataAccess.GetDevCustomReport(id);
        }
        public async Task<JArray> GetdevReportData(string query)
        {
            return await _customReportDataAccess.GetDevReportData(query);
        }

    }
}
