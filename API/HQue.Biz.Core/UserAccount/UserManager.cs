﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using HQue.Contract.Infrastructure.UserAccount;
using HQue.DataAccess.UserAccount;
using HQue.Communication.Email;
using HQue.Communication.Sms;
using HQue.Contract.Infrastructure.Common;
using HQue.Contract.Infrastructure.Master;
using HQue.Contract.Infrastructure.Setup;
using HQue.ServiceConnector.Connector;
using HQue.Contract.Infrastructure.Inventory;
using Utilities.Enum;
using Utilities.Helpers;
using System.Text;
using HQue.Contract.Infrastructure.Accounts;
using HQue.Biz.Core.Accounts;
using System.Linq;
using System.IO;

namespace HQue.Biz.Core.UserAccount
{
    public class UserManager : BizManager
    {
        private readonly UserDataAccess _userDataAccess;
        private readonly SmsSender _smsSender;
        private readonly EmailSender _emailSender;
        private readonly UserManagementDataAccess _userManagementDataAccess;
        private readonly UserAccessManager _userAccessManager;
        private readonly ConfigHelper _configHelper;
        private readonly PaymentManager _paymentManager;

        public UserManager(UserDataAccess userDataAccess, SmsSender smsSender, EmailSender emailSender,
            UserManagementDataAccess userManagementDataAccess,UserAccessManager userAccessManager,ConfigHelper configHelper,PaymentManager paymentManager) : base(userDataAccess)
        {
            _userDataAccess = userDataAccess;
            _smsSender = smsSender;
            _emailSender = emailSender;
            _userManagementDataAccess = userManagementDataAccess;
            _userAccessManager = userAccessManager;
            _configHelper = configHelper;
            _paymentManager = paymentManager;
        }

        public async Task<IEnumerable<HQueUser>> UserAccessList(HQueUser data)
        {
            return await _userManagementDataAccess.GetUserDetails(data);
        }

        public async Task<Tuple<bool, HQueUser>> ValidateUser(HQueUser data)
        {
            return await _userDataAccess.ValidateUser(data);
        }

        public async Task<Either<HQueUser>> ValidateOnlineUser(HQueUser entity)
        {
            var sc = new RestConnector();
            return await sc.PostAsyc<Either<HQueUser>>(_configHelper.InternalApi.AuthUrl, entity);
        }

        public Task<HQueUser> UpdateResetPassword(HQueUser data)
        {
            return _userDataAccess.UpdateResetPassword(data);
        }

        public Task<HQueUser> ChangePassword(HQueUser data)
        {
            return _userDataAccess.ChangePassword(data);
        }

        public async Task<Tuple<bool, HQueUser>> ValidateEmail(HQueUser data)
        {
            var result = await _userDataAccess.ValidateEmail(data);
            SendResetPasswordCommunication(result);
            return result;
        }
        //added by nandhini on 13.11.17
        public async Task<IEnumerable<LoginHistory>> LoginHistory(string sAccountId, string sInstanceId, string UserId)
        {
         var result=    await _userDataAccess.LoginHistory(sAccountId, sInstanceId, UserId);


            if (result != null && result.Count() > 0)
            {
                string sEmail;
                if (result.First().LoggedOutDate != null)
                {
                    sEmail = "Branch Name : " + result.FirstOrDefault().BranchName.ToString() + " ,  Closing Date and Time : " + result.FirstOrDefault().LoggedOutDate.ToString() + " , Username : " + result.FirstOrDefault().LogInUserId.ToString()+ " , Opening Date and Time : "+ result.FirstOrDefault().LoggedInDate.ToString();
                }
                else
                {
                    sEmail = "Branch Name : " + result.FirstOrDefault().BranchName.ToString() + " , Username : " + result.FirstOrDefault().LogInUserId.ToString() + " , Opening Date and Time : " + result.FirstOrDefault().LoggedInDate.ToString();
                }


                if (Convert.ToString( sEmail)!="" && Convert.ToString(result.FirstOrDefault().ContactEmail)!="")
                {
                    _emailSender.Send(result.FirstOrDefault().ContactEmail, 18, _emailSender.DailyLoginMailBodyBuilder(result));
                }

            }
            return result;
        }
        public async Task<LoginHistory> logOutHistory(string sAccountId, string sInstanceId, string sUserId,bool OfflineStatus)
        {
            return await _userDataAccess.logOutHistory(sAccountId, sInstanceId, sUserId, OfflineStatus);
        }
        
        //by San
        public async Task<Tuple<bool, HQueUser>> ExternalResetPassword(string userId)
        {
            HQueUser user = new HQueUser();
            user.UserId = userId;
            var result = await _userDataAccess.ValidateEmail(user);
            ExternalSendResetPasswordCommunication(result);
            return result;
        }

        public Task<bool> ValidateResetKey(string key)
        {
            return _userDataAccess.ValidateResetKey(key);
        }

        public async Task<HQueUser> Save(HQueUser model)
        {
            if (await UserExists(model))
            {

                throw new Exception($"User id {model.UserId} already exists");
                //System.ArgumentException argEx = new System.ArgumentException("Index is out of range", "index");
                //throw argEx;
                //throw new ArgumentException("A start-up parameter is required.");
            }

            SetNewUserInfo(model);
            var users = await _userDataAccess.Save(model);
            await _userAccessManager.SaveUserAccess(model);
            SendInviteCommunication(model);
            return model;
        }

        public async Task<HQueUser> SaveForOffline(HQueUser model)
        {
            if (await UserExists(model))
            {

                throw new Exception($"User id {model.UserId} already exists");
                //System.ArgumentException argEx = new System.ArgumentException("Index is out of range", "index");
                //throw argEx;
                //throw new ArgumentException("A start-up parameter is required.");
            }

            //SetNewUserInfo(model);
            model.Status = 1;
            model.CreatedBy = "localadmin";
            model.UpdatedBy = "localadmin";
           // var user = await _userDataAccess.Save(model);

            //By San - insert same userid for Offline 11-01-2017
            var user = await _userDataAccess.SaveToLocal(model);
            //await _userAccessManager.SaveUserAccess(model);

            //By San - insert same UserAccessId for Offline  11-01-2017
            await _userAccessManager.SaveUserAccessToLocal(model);
            //SendInviteCommunication(model);
            return user;
        }

        public async Task SaveAdminUser(Account account)
        {
            var user = account.User;
            SetMetaData(account, user);
            user.AccountId = account.Id;
            SetNewUserInfo(user);
            var obj = await _userDataAccess.Save(user);
            obj.SetAllUserAccess();
            await _userAccessManager.SaveUserAccess(obj);

            SendInviteCommunication(user);
        }


        

        public async Task SaveFinyear(Account account)
        {

            FinYearMaster Findata = new FinYearMaster();
            Findata.AccountId = account.Id;
            Findata.InstanceId = "";
            Findata.FinYearEndDate = account.ToDate;
            Findata.FinYearStartDate = account.FromDate;
            Findata.CreatedBy = account.CreatedBy;
            Findata.UpdatedBy = account.UpdatedBy;
            await _paymentManager.saveFinancialYear(Findata);


        }

        public async Task<string> SaveVendorUser(Vendor model)
        {
            var user = new HQueUser
            {
                Mobile = model.Mobile,
                UserId = model.Email
            };

            if (string.IsNullOrEmpty(user.Mobile) || string.IsNullOrEmpty(user.UserId))
            {
                return "Vendor user mobile no or email id is empty.";
            }

            SetMetaData(model, user);

            user.Name = model.Name;

            if (await UserExists(user))
            {
                //throw new Exception($"Vendor User already exists");
                return "Vendor User already exists.";
            }

            SetNewUserInfo(user);
            user.UserType = UserType.DistributorUser;
            await _userDataAccess.Save(user);

            //SendInviteCommunication(user); // Command Gavaskar 22-10-2016

            // Newly Added Gavaskar 22-10-2016 Start

            SendInviteCommunicationVendorMail(user);

            // Newly Added Gavaskar 22-10-2016 End
            return "";
        }

        // Newly Added Gavaskar 22-10-2016 Start
        private void SendInviteCommunicationVendorMail(HQueUser user)
        {
            var url = $"{_configHelper.UrlBuilder.ResetUrl}{user.PasswordResetKey}";

            _emailSender.Send(user.UserId, 4, url);
        }

        public void SendInviteCommunicationVendorUpdate(Vendor vendor)
        {
            //Added by Sarubala on 17-11-17
            SmsLog log1 = new SmsLog();
            log1.SmsDate = DateTime.Today;
            log1.LogType = "Vendor Update";
            log1.ReceiverName = vendor.Name;
            log1.InvoiceDate = DateTime.Now;
            log1.ReferenceId = vendor.Id;

            if ((bool)vendor.Mobile1checked)
            {
                if (!string.IsNullOrEmpty(vendor.Mobile1))
                {
                    _smsSender.SendSms(vendor.Mobile1, 4, log1);
                }
            }

            if ((bool)vendor.Mobile2checked)
            {
                if (!string.IsNullOrEmpty(vendor.Mobile2))
                {
                    _smsSender.SendSms(vendor.Mobile2, 4, log1);
                }
            }
        }

        public void SendInviteCommunicationVendor(Vendor vendor)
        {
            //Added by Sarubala on 17-11-17
            SmsLog log1 = new SmsLog();
            log1.SmsDate = DateTime.Today;
            log1.LogType = "Vendor Create";
            log1.ReceiverName = vendor.Name;            
            log1.InvoiceDate = vendor.CreatedAt;
            log1.ReferenceId = vendor.Id;

            if (!string.IsNullOrEmpty(vendor.Mobile))
            {
                _smsSender.SendSms(vendor.Mobile, 4, log1);
            }

            if ((bool)vendor.Mobile1checked)
            {
                if (!string.IsNullOrEmpty(vendor.Mobile1))
                {
                    _smsSender.SendSms(vendor.Mobile1, 4, log1);
                }
            }

            if ((bool)vendor.Mobile2checked)
            {
                if (!string.IsNullOrEmpty(vendor.Mobile2))
                {
                    _smsSender.SendSms(vendor.Mobile2, 4, log1);
                }
            }
        }

        // Newly Added Gavaskar 22-10-2016 End

        public async Task SaveInstanceUser(Instance model)
        {
            foreach (var user in model.UserList)
            {
                SetMetaData(model, user);
                user.InstanceId = model.Id;
                SetNewUserInfo(user);
                var obj = await _userDataAccess.Save(user);
                obj.SetAllUserAccess();
                await _userAccessManager.SaveUserAccess(obj);

                SendInviteCommunication(user);
            }
        }

        private async Task<bool> UserExists(HQueUser user)
        {
            var users = await _userDataAccess.CheckUniqueUserId(user);
            return users.Count != 0;
        }

        private static void SetNewUserInfo(HQueUser model)
        {
            model.UserType = UserType.InstanceUser;
            //for temp password
            model.Password = "12345";

            model.UserType = UserType.InstanceUser;
            model.Status = HQueUser.StatusActive;
            model.PasswordResetKey = Guid.NewGuid().ToString();
            model.ResetRequestTime = CustomDateTime.IST;
        }

        private void SendInviteCommunication(HQueUser user)
        {
            //Added by Sarubala on 20-11-17
            bool sendSms = Convert.ToBoolean(_userManagementDataAccess.GetIsUserCreateSms(user.InstanceId).Result);

            var url = $"{_configHelper.UrlBuilder.ResetUrl}{user.PasswordResetKey}";
            if(sendSms == true)
            {
                SmsLog log1 = new SmsLog();
                log1.SmsDate = DateTime.Today;
                log1.LogType = "User Create";
                log1.ReceiverName = user.Name;
                log1.InvoiceDate = DateTime.Now;
                log1.ReferenceId = user.Id;


                _smsSender.SendSms(user.Mobile, 4, log1);
            }
            
            _emailSender.Send(user.UserId, 4, url);
        }

        private void SendResetPasswordCommunication(Tuple<bool, HQueUser> result)
        {
            var url = $"{_configHelper.UrlBuilder.ResetUrl}{result.Item2.PasswordResetKey}";
            _emailSender.OverrideConfig = true;
            _emailSender.Send(result.Item2.UserId, 3, url);
        }

        //By San
        private void ExternalSendResetPasswordCommunication(Tuple<bool, HQueUser> result)
        {
            var url = $"{_configHelper.UrlBuilder.ExternalResetUrl}{result.Item2.PasswordResetKey}";
            _emailSender.OverrideConfig = true;
            //By San
            StringBuilder BodyContent = _emailSender.PasswordResetBodyBuilder(result,url);
            _emailSender.Send(result.Item2.UserId, 12, BodyContent);
        }

        public async Task<int> getOfflineUserCount()
        {
            return await _userDataAccess.getOfflineUserCount();
        }
        public async Task<HQueUser> AddSalesPerson(HQueUser model)
        {
            if (await SalesPersonExists(model))
            {
                throw new Exception($"Password already exists");
            }
            var users = await _userDataAccess.AddSalesPerson(model);
            return users;
        }
        public async Task<HQueUser> updateUserValuesStatus(HQueUser model)
        {
            return await _userDataAccess.UpdateStatus(model);
          
        }
        
        private async Task<bool> SalesPersonExists(HQueUser user)
        {
            return await _userDataAccess.SalesPersonExists(user);            
        }
        public async Task<IEnumerable<HQueUser>> SalesPersonList(HQueUser user)
        {
            return await _userDataAccess.SalesPersonList(user);             
        }

        public async Task<HQueUser> validateExistUser(HQueUser model)
        {            
            var users = await _userDataAccess.validateExistUser(model);
            return users;
        }
    }
}
