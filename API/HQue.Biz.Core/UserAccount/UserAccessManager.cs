﻿using HQue.Contract.Infrastructure.UserAccount;
using HQue.DataAccess.UserAccount;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace HQue.Biz.Core.UserAccount
{
    public class UserAccessManager : BizManager
    {
        private readonly UserManagementDataAccess _userManagementDataAccess;
        private readonly UserDataAccess _userDataAccess;

        public UserAccessManager(UserManagementDataAccess userManagementDataAccess, UserDataAccess userDataAccess)
            : base(userManagementDataAccess)
        {
            _userManagementDataAccess = userManagementDataAccess;
            _userDataAccess = userDataAccess;
        }

        public static IEnumerable<UserAccess> GetAllPermission()
        {
            for (var i = 1; i <= 31; i++)
                yield return new UserAccess { AccessRight = i };
        }

        public async Task<bool> DeleteUserAccess(HQueUser model)
        {
            model.UserAccess.UserId = model.Id;
            await _userManagementDataAccess.Delete(model.UserAccess);
            return true;
        }

        public async Task<HQueUser> UpdateUserAccess(HQueUser model)
        {
            var users = await _userDataAccess.UpdateStatus(model);
            await SaveUserAccess(model);
            return users;
        }

        public async Task<bool> SaveUserAccess(HQueUser model)
        {
            foreach (var userAccess in model.RoleAccess)
            {
                SetMetaData(model, userAccess);
                userAccess.UserId = model.Id;
                await _userManagementDataAccess.Save(userAccess);
            }
            return true;
        }

        //By San - insert same UserAccessId for Offline  11-01-2017
        public async Task<bool> SaveUserAccessToLocal(HQueUser model)
        {
            foreach (var userAccess in model.RoleAccess)
            {
                //SetMetaData(model, userAccess);
                userAccess.UserId = model.Id;
                await _userManagementDataAccess.SaveToLocal(userAccess);
            }
            return true;
        }

        //Added by Sarubala on 20-11-17
        public async Task<bool> GetIsUserCreateSms(string InstanceId)
        {
            return await _userManagementDataAccess.GetIsUserCreateSms(InstanceId);
        }

    }
       
}
