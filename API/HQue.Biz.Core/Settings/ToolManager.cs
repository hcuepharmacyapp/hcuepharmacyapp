﻿using HQue.Communication.Email;
using HQue.Contract.Infrastructure;
using HQue.Contract.Infrastructure.Settings;
using HQue.Contract.Infrastructure.Setup;
using HQue.Contract.Infrastructure.Inventory;
using HQue.DataAccess.DbModel;
using HQue.DataAccess.Settings;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using HQue.Contract.Infrastructure.UserAccount;

namespace HQue.Biz.Core.Settings
{
    public class ToolManager
    {
        private readonly ToolDataAccess _toolDataAccess;
        private readonly EmailSender _emailSender;

        public ToolManager(ToolDataAccess tooltDataAccess, EmailSender emailSender)
        {
            _toolDataAccess = tooltDataAccess;
            _emailSender = emailSender;
        }

        public async Task<Tools> Save(Tools model)
        {
            return await _toolDataAccess.Save(model);
        }

        public async Task<List<Tools>> List()
        {
            return await _toolDataAccess.List();
        }

        public async Task<Tools> updateStatus(Tools model)
        {
            return await _toolDataAccess.updateStatus(model);
        }

        public async Task<string> GetRequirementNo(string instanceId)
        {
            return await _toolDataAccess.GetRequirementNo(instanceId);
        }

        public async Task<List<Requirements>> saveRequirement(List<Requirements> reqList, string accId, string insId, string userId)
        {
            return await _toolDataAccess.SaveRequirement(reqList, accId, insId, userId);
        }

        public async Task<List<Tools>> NegativeStockList()
        {
            return await _toolDataAccess.NegativeStockList();
        }

        public async Task<PagerContract<Requirements>> ListPager(Requirements data, int type, string status)
        {
            var pager = new PagerContract<Requirements>
            {
                NoOfRows = await _toolDataAccess.Count(),
                List = await _toolDataAccess.List(data, type, status)
            };

            return pager;
        }

        public async Task<Requirements> updateStatus(Requirements data)
        {
            return await _toolDataAccess.updateStatus(data);
        }
        public async Task<List<Tools>> getBranchList(string accountId, string branchId, string name)
        {
            return await _toolDataAccess.getBranchList(accountId, branchId,name);
        }
        //ADDED BY NANDHINI
        public async Task<List<Tools>> salesReport(string from)
        {
            return await _toolDataAccess.salesReport(from);
        }
        public async Task<List<Tools>> offlineReport()
        {
            return await _toolDataAccess.offlineReport();
        }

        public async Task<List<Tools>> updateList(string accountId, string purchaseLowestPrice, string branchId)
        {
            return await _toolDataAccess.updateList(accountId, purchaseLowestPrice, branchId);
        }
        public async Task<Instance> updateOfflineStatus(Instance data, string UserId)
        {
            var txtEmail = "";
            if (data.OfflineStatus == true)
                txtEmail = string.Format("<p>Dear Team,<br/><br/>Online has been blocked by " + UserId + " for the below branch.<br/><br/><B>Id:</B> " + data.Id + "<br/><B>Branch:</B> " + data.Name + "<br/><br/><br/>Thank you</p><p>Powered By hCue</p>");
            else
                txtEmail = string.Format("<p>Dear Team,<br/><br/>Online has been un-blocked by " + UserId + " for the below branch.<br/><br/><B>Id:</B> " + data.Id + "<br/><B>Branch:</B> " + data.Name + "<br/><br/><br/>Thank you</p><p>Powered By hCue</p>");

            _emailSender.Send("sundar@hcue.co", 14, txtEmail);
            return await _toolDataAccess.updateOfflineStatus(data);
        }
        public async Task<Instance> updateOfflineVersionNo(Instance data, string UserId)
        {
            return await _toolDataAccess.updateOfflineVersionNo(data);
        }
        public async Task<List<Account>> getAccountList()
        {
            return await _toolDataAccess.getAccountList();
        }
        public async Task<List<Instance>> getBranchListData()
        {
            return await _toolDataAccess.getBranchListData();
        }
        public async Task<List<Instance>> GetInstanceSettingsList(string accountId, string branchId)
        {
            return await _toolDataAccess.GetInstanceSettingsList(accountId, branchId);
        }
        public async Task<List<Instance>> UpdateHcueProductMasterSettings(List<Instance> model)
        {
            return await _toolDataAccess.UpdateHcueProductMasterSettings(model);
        }

        public async Task<List<Instance>> updateHcuePurchasePriceSettings(List<Instance> model)
        {
            return await _toolDataAccess.updateHcuePurchasePriceSettings(model);
        }
        public async Task<List<Instance>> updateSaleBillcsv(List<Instance> model)
        {
            return await _toolDataAccess.updateSaleBillcsv(model);
        }

        public async Task<List<Instance>> updateSalesPriceSettings(List<Instance> model, String SalesPrice)
        {
            return await _toolDataAccess.updateSalesPriceSettings(model, SalesPrice);
        }
        public async Task<int> getSalesPriceSetting(String insId)
        {
            return await _toolDataAccess.getSalesPriceSetting(insId);
        }
        public async Task<int> getSaleBillcsv(String insId)
        {
            return await _toolDataAccess.getSaleBillcsv(insId);
        }

        public async Task<Setting> getexportToCsvSetting(string AccountId, string InstanceId)
        {
            return await _toolDataAccess.getexportToCsvSetting(AccountId, InstanceId);
        }

        //Added by Sarubala on 25-11-17
        public async Task<Instance> getSmsCount(string accountId, string instanceId)
        {
            return await _toolDataAccess.getSmsCount(accountId, instanceId);
        }

        //Added by Sarubala on 27-11-17
        public async Task<List<SmsConfiguration>> GetSmsConfiguration(string accountId, string instanceId)
        {
            return await _toolDataAccess.GetSmsConfiguration(accountId, instanceId);
        }

        public async Task<SmsConfiguration> SaveSmsConfiguration(SmsConfiguration data)
        {
            return await _toolDataAccess.SaveSmsConfiguration(data);
        }
        // Added by Gavaskar Stock Ledger Edit 25-12-2017 Start 
        public async Task<PagerContract<StockLedgerUpdateHistory>> StockLedgerEditList(string AccountId, string InstanceId, string ProductName)
        {
            var pager = new PagerContract<StockLedgerUpdateHistory>
            {
                //NoOfRows = await _productDataAccess.GstNullMasterCount(accountid, instanceid, data),
                List = await _toolDataAccess.StockLedgerEditList(AccountId, InstanceId, ProductName)
            };

            return pager;
        }
        public async Task<List<StockLedgerUpdateHistory>> BulkStockLedgerUpdate(List<StockLedgerUpdateHistory> stockLedgerList, bool internet)
        {

            foreach (var stockLedger in stockLedgerList)
            {
                if (stockLedger.isChecked == true)
                {
                    stockLedger.AccountId = stockLedgerList[0].AccountId;
                    stockLedger.InstanceId = stockLedgerList[0].InstanceId;
                    stockLedger.CreatedBy = stockLedgerList[0].CreatedBy;
                    stockLedger.UpdatedBy = stockLedgerList[0].UpdatedBy;
                    stockLedger.ProductStockId = stockLedger.Stockid;
                    var StockLedgerUpdateHistory = await _toolDataAccess.SaveStockLedgerUpdateHistory(stockLedger);
                    await _toolDataAccess.StockLedgerUpdateHistoryItem(GetEnumerable(StockLedgerUpdateHistory), internet);
                }

            }
            return stockLedgerList;
            //return await _toolDataAccess.BulkStockLedgerUpdate(stockLedgerList);
        }

        private IEnumerable<StockLedgerUpdateHistoryItem> GetEnumerable(StockLedgerUpdateHistory data)
        {
            foreach (var item in data.StockLedgerUpdateHistoryItem)
            {
                item.AccountId = data.AccountId;
                item.InstanceId = data.InstanceId;
                item.CreatedBy = data.CreatedBy;
                item.UpdatedBy = data.UpdatedBy;
                item.StockLedgerUpdateId = data.Id;
                yield return item;
            }
        }
        public async Task<dynamic> BulkUpdateProductStockOfflineToOnline(string AccountId, string InstanceId)
        {
            return await _toolDataAccess.BulkUpdateProductStockOfflineToOnline(AccountId, InstanceId);

        }
        public async Task<List<ProductStock>> BulkUpdateNegativeStockOfflineToOnline(string AccountId, string InstanceId)
        {
            return await _toolDataAccess.BulkUpdateNegativeStockOfflineToOnline(AccountId, InstanceId);
        }
        public async Task<List<ProductStock>> GetNagativeStockList(string AccountId, string InstanceId)
        {
            return await _toolDataAccess.GetNagativeStockList(AccountId, InstanceId);
        }
        public async Task<string> GetReverseSyncOfflineToOnlineCompare(string AccountId, string InstanceId)
        {
            return await _toolDataAccess.GetReverseSyncOfflineToOnlineCompare(AccountId, InstanceId);
        }
        // Added by Gavaskar Stock Ledger Edit 25-12-2017 End 
    }
}
