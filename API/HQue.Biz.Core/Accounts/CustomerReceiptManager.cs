﻿using System.Collections.Generic;
using System.Threading.Tasks;
using HQue.Contract.Infrastructure.Accounts;
using HQue.Contract.Infrastructure.Inventory;
using HQue.DataAccess.Accounts;
using Utilities.Helpers;
using HQue.DataAccess.Helpers;
using HQue.DataAccess.Master;

namespace HQue.Biz.Core.Accounts
{
    public class CustomerReceiptManager : BizManager
    {
        public const string ReturnPaymentType = "Return";
        public const string VendorPurchase = "Purchase";
        public const string PurchasePaid = "PurchasePaid";
        public const string UpdateVendorPurchase = "Update";
        protected readonly PatientDataAccess _patientDataAccess;
        public CustomerReceiptManager(CustomerReceiptDataAccess dataAccess, PatientDataAccess patientDataAccess) : base(dataAccess)
        {
            _patientDataAccess = patientDataAccess;
        }

        private CustomerReceiptDataAccess CustomerReceiptDataAccess => (CustomerReceiptDataAccess)DataAccess;



        public async Task<List<Sales>> CustomerPaymentList(string accountId, string instanceId, string customerId)
        {
            return await CustomerReceiptDataAccess.CustomerPaymentList(accountId, instanceId, customerId);
        }
        public async Task updateChequeStatus(string accountId, string instanceId, Sales model)
        {
            if (model.Type == "Sales")
            {
                await CustomerReceiptDataAccess.updateSale(accountId, instanceId, model);
            }
            if (model.Type == "CustomerReceipt")
            {
                await CustomerReceiptDataAccess.updateCustomerPayment(accountId, instanceId, model);
            }
        }
        public async Task<List<Sales>> CustomerPaymentDetails(string accountId, string instanceId, string customerMobile, string customerName)
        {
            return await CustomerReceiptDataAccess.CustomerPaymentDetails(accountId, instanceId, customerMobile, customerName);
        }
        public async Task<CustomerPayment> GetCustomerBalance(string accountId, string instanceId, string customerMobile, string customerName)
        {
            return await CustomerReceiptDataAccess.GetCustomerBalance(accountId, instanceId, customerMobile, customerName);
        }
        public async Task<List<Sales>> CustomerList(string accountId, string instanceId)
        {
            return await CustomerReceiptDataAccess.CustomerList(accountId, instanceId);
        }


        public async Task<List<Sales>> searchCustomer(string AccountId, string InstanceId, string keyword, string type)
        {
            return await CustomerReceiptDataAccess.searchCustomer(AccountId, InstanceId, keyword, type);
        }

        public async Task<List<Sales>> GetCustomerCheque(string AccountId, string InstanceId, string PatientId)
        {
            return await CustomerReceiptDataAccess.GetCustomerCheque(AccountId, InstanceId, PatientId);
        }
        public async Task<CustomerPayment> UpdateCustomerPayment(CustomerPayment data)
        {
            if (data.PaymentAmount > 0)
            {
                await CustomerReceiptDataAccess.UpdateCustomerPayment(data);
            }
            return data;
        }
        public async Task<Sales> SaveCustomerPayment(Sales data)
        {
            foreach (var getPayment in data.CustomerPayments)
            {
                if (getPayment.PaymentAmount > 0)
                {
                    getPayment.InstanceId = data.InstanceId;
                    getPayment.AccountId = data.AccountId;
                    getPayment.CreatedBy = data.CreatedBy;
                    getPayment.UpdatedBy = data.UpdatedBy;
                    getPayment.Debit = getPayment.PaymentAmount;
                    getPayment.Credit = 0;
                    await DataAccess.SaveCustomerPayment(getPayment);
                    if(!string.IsNullOrEmpty(getPayment.SalesId))
                    {
                        await CustomerReceiptDataAccess.UpdateSaleCredit(getPayment);
                    }
                    if (data.PatientStatus == 3)
                    {
                        data.Patient.Status = 2; //data.PatientStatus
                        data.Patient.Id = data.PatientId;
                        await _patientDataAccess.UpdatePatient(data.Patient);
                    }
                }
            }
            // Added by Settu to capture adjustments from customer receipt
            foreach (var voucher in data.VoucherList)
            {
                if (voucher.AdjustedAmount > 0)
                {
                    SetMetaData(data, voucher);
                    if (voucher.Amount == 0)
                        voucher.Status = true;
                    await CustomerReceiptDataAccess.UpdateVoucherNote(voucher);
                }
                if (voucher.ReturnAmount > 0)
                {
                    SetMetaData(data, voucher);
                    if (voucher.Amount == 0)
                        voucher.Status = true;
                    await CustomerReceiptDataAccess.UpdateVoucherNote(voucher);
                    Settlements settlements = new Settlements
                    {
                        AdjustedAmount = voucher.ReturnAmount,
                        VoucherId = voucher.Id,
                        TransactionType = (int)HelperDataAccess.TransactionType.SalesReturn,
                    };
                    SetMetaData(data, settlements);
                    settlements = await CustomerReceiptDataAccess.SaveSettlements(settlements);
                }
            }
            foreach (var settlements in data.SettlementsList)
            {
                settlements.TransactionType = (int)HelperDataAccess.SettlementsTransactionType.Sales;
                SetMetaData(data, settlements);
                await CustomerReceiptDataAccess.SaveSettlements(settlements);
            }
            return data;
        }

    }
}
