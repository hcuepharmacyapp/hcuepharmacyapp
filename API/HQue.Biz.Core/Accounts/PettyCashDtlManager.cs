﻿using HQue.Contract.Infrastructure;
using System;
using System.Threading.Tasks;
using HQue.DataAccess.Accounts;
using HQue.Contract.Infrastructure.Accounts;
using Utilities.Helpers;


namespace HQue.Biz.Core.Accounts
{
    public class PettyCashDtlManager: BizManager
    {
        private readonly PettyCashDtlDataAccess _pettyCashDtlDataAccess;

        public PettyCashDtlManager(PettyCashDtlDataAccess pettyCashDtlDataAccess) : base(pettyCashDtlDataAccess)
        {
            _pettyCashDtlDataAccess = pettyCashDtlDataAccess;
        }

        public async Task<PettyCashDtl> Save(PettyCashDtl model)
        {
            model.TransactionDate = model.TransactionDate ?? CustomDateTime.IST;
            return await _pettyCashDtlDataAccess.Save(model);
        }

        public async Task<PettyCashDtl> Update(PettyCashDtl model)
        {
            return await _pettyCashDtlDataAccess.UpdateDtl(model);
        }

        public async Task<PagerContract<PettyCashDtl>> ListPager(string type, string accountId, string instanceId, DateTime from, DateTime to, string userId, bool isoffline)
        {
            var pager = new PagerContract<PettyCashDtl>
            {
                NoOfRows = await _pettyCashDtlDataAccess.Count(type, accountId, instanceId, from, to),
                List = await _pettyCashDtlDataAccess.ListData(type, accountId, instanceId, from, to, userId,isoffline)
            };

            return pager;
        }


        public async Task<string> getLatestupdatedDate(string accountId, string instanceId,string userId)
        {
           return  Convert.ToString(await _pettyCashDtlDataAccess.getLatestupdatedDate(accountId, instanceId, userId));
        }
    }
}
