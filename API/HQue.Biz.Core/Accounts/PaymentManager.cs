﻿using System.Collections.Generic;
using System.Threading.Tasks;
using HQue.Contract.Infrastructure.Accounts;
using HQue.Contract.Infrastructure.Inventory;
using HQue.DataAccess.Accounts;
using Utilities.Helpers;
using System;
using System.Linq;
using HQue.DataAccess.Master;

namespace HQue.Biz.Core.Accounts
{
    public class PaymentManager : BizManager
    {
        public const string ReturnPaymentType = "Return";
        public const string VendorPurchase = "Purchase";
        public const string PurchasePaid = "PurchasePaid";
        public const string UpdateVendorPurchase = "Update";

        private readonly VendorDataAccess VendorDataAccess;
        public PaymentManager(PaymentDataAccess dataAccess, VendorDataAccess _vendorDataAccess) : base(dataAccess)
        {
            VendorDataAccess = _vendorDataAccess;
        }
        private PaymentDataAccess PaymentDataAccess => (PaymentDataAccess)DataAccess;

        public async Task<Payment> Save(Payment data)
        {
            foreach (var getPayment in data.PaymentList)
            {
                if (getPayment.PaymentAmount > 0)
                {
                    getPayment.ClientId = data.ClientId;
                    getPayment.InstanceId = data.InstanceId;
                    getPayment.AccountId = data.AccountId;
                    getPayment.CreatedBy = data.CreatedBy;
                    getPayment.UpdatedBy = data.UpdatedBy;
                    getPayment.TransactionDate = data.TransactionDate;
                    getPayment.Remarks = data.Remarks;
                    getPayment.PaymentType = PurchasePaid;
                    getPayment.PaymentMode = data.PaymentType;
                    getPayment.Credit = 0;
                    getPayment.VendorId = data.VendorId;
                    getPayment.Debit = getPayment.PaymentAmount;
                    if (data.PaymentType == "cheque")
                    {
                        getPayment.BankName = data.BankName;
                        getPayment.BankBranchName = data.BankBranchName;
                        getPayment.IfscCode = data.IfscCode;
                        getPayment.ChequeNo = data.ChequeNo;
                        getPayment.ChequeDate = data.ChequeDate;
                    }
                    await DataAccess.Save(getPayment);
                    if (data.Vendor.Status == 4)
                        await VendorDataAccess.Update(data.Vendor);
                }
            }
            return data;
        }
        public async Task<FinYearMaster> saveFinancialYear(FinYearMaster data)
        {
            var result = await PaymentDataAccess.saveFinancialYear(data);
            return result;
        }
        public async Task<FinYearMaster> saveFinyearStatus(FinYearMaster data)
        {
            //FinYearMaster finyearmasterdata = new FinYearMaster();
            //finyearmasterdata.AccountId = data.AccountId;
            //finyearmasterdata.FinYearEndDate = data.FinYearEndDate;
            //finyearmasterdata.FinYearStartDate = data.FinYearStartDate;
            //finyearmasterdata.CreatedBy = data.CreatedBy;
            //finyearmasterdata.UpdatedBy = data.UpdatedBy;
            //finyearmasterdata.InstanceId = string.Empty;
            //var res = await PaymentDataAccess.resetFinyearmaster(finyearmasterdata);
            //res.FinYearStatus = data.FinYearStatus;
            //var result = await PaymentDataAccess.saveFinyearStatus(res);
            //return result;
            return await PaymentDataAccess.saveFinyearStatus(data);
        }
        public async Task<FinYearMaster> getFinyearAvailable(string accountId)
        {
            var data = await PaymentDataAccess.getFinyearAvailable(accountId);
            return data;
        }
        public async Task<Payment> Update(Payment data)
        {
            data.UpdatedBy = data.UpdatedBy;
            data.UpdatedAt = CustomDateTime.IST;
            data.PaymentHistoryStatus = "Modified";
            data.OfflineStatus = Convert.ToBoolean(DataAccess.user.OfflineStatus());
            await PaymentDataAccess.Update(data);
            return data;
        }
        public async Task<Payment> Delete(Payment data)
        {
            data.UpdatedBy = data.UpdatedBy;
            data.UpdatedAt = CustomDateTime.IST;
            data.PaymentHistoryStatus = "Cancelled";
            data.OfflineStatus = Convert.ToBoolean(DataAccess.user.OfflineStatus());
            await PaymentDataAccess.Delete(data);
            if (data.Vendor.Status == 4)
            {
                data.Vendor.Status = 3;
                await VendorDataAccess.Update(data.Vendor);
            }
            return data;
        }
        public async Task<List<Payment>> GetVendorInvoice(Payment payment, string InstanceId)
        {
            return await PaymentDataAccess.GetVendorInvoice(payment, InstanceId);
        }
        //history added by nandhini 20.9.17
        public async Task<List<Payment>> GetVendorHistory(Payment history, string InstanceId)
        {
            return await PaymentDataAccess.GetVendorHistory(history, InstanceId);
        }
        public async Task SaveVendorPurchasePayment(VendorPurchase data)
        {
                
            var credit = new Payment
            {
                //Credit = data.NetTotal,
                Credit = data.NetValue,
                VendorPurchaseId = data.Id,
                VendorId = data.VendorId,
                PaymentType = VendorPurchase,
                TransactionDate = CustomDateTime.IST
            };
            SetMetaData(data, credit);
            await PaymentDataAccess.Save(credit);
            if (!data.Credit.HasValue || data.Credit.Value <= 0)
                return;
            var debit = new Payment
            {
                Debit = data.Credit,
                VendorPurchaseId = data.Id,
                VendorId = data.VendorId,
                PaymentType = PurchasePaid,
                TransactionDate = CustomDateTime.IST
            };
            SetMetaData(data, debit);
            await PaymentDataAccess.Save(debit);
        }
        public async Task SaveVendorPurchaseReturnPayment(VendorPurchaseReturn data)
        {
            var debit = new Payment
            {
                Debit = data.NetReturn,
                VendorPurchaseId = data.VendorPurchaseId,
                VendorId = data.VendorId,
                TransactionDate = CustomDateTime.IST,
                PaymentType = ReturnPaymentType
            };
            SetMetaData(data, debit);
            await PaymentDataAccess.Save(debit);
        }
        public async Task SaveVendorPurchaseBulkReturnPayment(VendorPurchaseReturnItem data)
        {
            var debit = new Payment
            {
                Debit = data.ReturnTotal,
                VendorPurchaseId = data.VendorPurchaseId,
                VendorId = data.ProductStock.Vendor.Id,
                TransactionDate = CustomDateTime.IST,
                PaymentType = ReturnPaymentType
            };
            SetMetaData(data, debit);
            await PaymentDataAccess.Save(debit);
        }
        public async Task<List<Payment>> PaymentReportList(string accountId, string instanceId)
        {
            return await PaymentDataAccess.GetReportList(accountId, instanceId);
        }
        public async Task UpdateVendorPurchasePayment(VendorPurchase data)
        {
            ////await UpdateAllPreviousPaymentStauts(data.Id);
            //if (data.ActualPaymentTotal == null)
            //    data.ActualPaymentTotal = 0;
            //decimal? roundOf = data.RoundOff;
            //if (data.NoteType == "credit")
            //{
            //    roundOf += data.NoteAmount;
            //}
            //else
            //{
            //    roundOf -= data.NoteAmount;
            //}
            //if (roundOf > data.ActualPaymentTotal)
            //{
            //    var credit = new Payment
            //    {
            //        Credit = data.PaymentTotalChange - data.NoteAmount,
            //        //Credit= data.UpdatePaymentTotal,
            //        VendorPurchaseId = data.Id,
            //        VendorId = data.VendorId,
            //        PaymentType = UpdateVendorPurchase,
            //        TransactionDate = CustomDateTime.IST,
            //        Status = true
            //    };
            //    SetMetaData(data, credit);
            //    await PaymentDataAccess.Save(credit);
            //}
            //if (data.ActualPaymentTotal > roundOf)
            //{
            //    var debit = new Payment
            //    {
            //        Debit = (data.PaymentTotalChange * -1) - data.NoteAmount,
            //        VendorPurchaseId = data.Id,
            //        VendorId = data.VendorId,
            //        PaymentType = PurchasePaid,
            //        TransactionDate = CustomDateTime.IST,
            //        Status = true
            //    };
            //    SetMetaData(data, debit);
            //    await PaymentDataAccess.Save(debit);
            //}


            //if (data.NoteType == "credit")
            //{
            //    data.UpdatePaymentTotal = data.UpdatePaymentTotal - data.NoteAmount;
            //}
            //else
            //{
            //    data.UpdatePaymentTotal = data.UpdatePaymentTotal + data.NoteAmount;
            //}

            var credit = new Payment
            {
                Credit = data.UpdatePaymentTotal,
                VendorPurchaseId = data.Id,
                VendorId = data.VendorId,
                PaymentType = UpdateVendorPurchase,
                TransactionDate = CustomDateTime.IST,
                Status = 1
            };
            SetMetaData(data, credit);
            await PaymentDataAccess.UpdatePreviousPaymentRecords(credit);
            await PaymentDataAccess.Save(credit);

        }
        //public async Task UpdateAllPreviousPaymentStauts(string vendorpurchaseid)
        //{
        //    await PaymentDataAccess.UpdateAllPreviousPaymentStauts(vendorpurchaseid);
        //}
        public async Task<List<CustomerPayment>> CustomerPaymentList(string accountId, string instanceId, string customername, string mobile)
        {
            return await PaymentDataAccess.CustomerPaymentList(accountId, instanceId, customername, mobile);
        }
        //Customer receipt shown by PatientId instead of patient mobile & name
        public async Task<Sales> CustomerPaymentDetails(string accountId, string instanceId, string PatientId)
        {
            return await PaymentDataAccess.CustomerPaymentDetails(accountId, instanceId, PatientId);
        }
        public async Task<List<CustomerPayment>> CustomerList(string accountId, string instanceId)
        {
            return await PaymentDataAccess.CustomerList(accountId, instanceId);
        }
        public async Task<CustomerPayment> SaveCustomerPayment(CustomerPayment data)
        {
            foreach (var getPayment in data.CustomerPaymentList)
            {
                if (getPayment.PaymentAmount > 0)
                {
                    getPayment.ClientId = data.ClientId;
                    getPayment.InstanceId = data.InstanceId;
                    getPayment.AccountId = data.AccountId;
                    getPayment.CreatedBy = data.CreatedBy;
                    getPayment.UpdatedBy = data.UpdatedBy;
                    getPayment.TransactionDate = data.TransactionDate;
                    getPayment.Remarks = data.Remarks;
                    getPayment.PaymentType = data.PaymentType;
                    getPayment.Credit = 0;
                    getPayment.Debit = getPayment.PaymentAmount;
                    await DataAccess.Save(getPayment);
                }
            }
            return data;
        }
        //Not used anywhere across application
        //public async Task<bool> SaveDeletedItem(VendorPurchaseItem data)
        //{
        //    var payment = new Payment
        //    {
        //        Debit = data.TotalWithVat,
        //        Credit = 0.00M,
        //        VendorPurchaseId = data.VendorPurchaseId,
        //        VendorId = data.Vendor.Id,
        //        PaymentType = "Delete",
        //        TransactionDate = CustomDateTime.IST
        //    };
        //    SetMetaData(data, payment);
        //    await PaymentDataAccess.Save(payment);
        //    return true;
        //}



        public async Task PayCustomerPendingAmount(Sales sales)
        {

            var result = await PaymentDataAccess.GetCustomerReceiptItems(sales.InstanceId, sales.Mobile, sales.Name);

            sales.CustomerPayments = result.OrderBy(item => item.InvoiceDate).ToList();



        }
    }
}
