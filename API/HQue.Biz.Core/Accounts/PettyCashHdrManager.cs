﻿using HQue.Contract.Infrastructure;
using System;
using System.Threading.Tasks;
using HQue.DataAccess.Accounts;
using HQue.Contract.Infrastructure.Accounts;
using Utilities.Helpers;

namespace HQue.Biz.Core.Accounts
{
    public class PettyCashHdrManager : BizManager
    {
        private readonly PettyCashHdrDataAccess _pettyCashHdrDataAccess;

        public PettyCashHdrManager(PettyCashHdrDataAccess pettyCashHdrDataAccess) : base(pettyCashHdrDataAccess)
        {
            _pettyCashHdrDataAccess = pettyCashHdrDataAccess;
        }

        public async Task<PettyCashHdr> Save(PettyCashHdr model)
        {
            model.TransactionDate = model.TransactionDate ?? CustomDateTime.IST;
            return await _pettyCashHdrDataAccess.Save(model);
        }

        public async Task<PettyCashHdr> Update(PettyCashHdr model)
        {
            return await _pettyCashHdrDataAccess.UpdateHdr(model);
        }
        public async Task<decimal> GetSalesValue(string AccId, string InsId, DateTime from, DateTime to,  string UserId , bool offline)
        {
            return await _pettyCashHdrDataAccess.GetSalesValue(AccId, InsId, from, to,  UserId, offline);
        }

        public async Task<decimal> GetVendorValue(string AccId, string InsId, DateTime from, DateTime to, string UserId, bool offline)
        {
            return await _pettyCashHdrDataAccess.GetVendorPaymentValue(AccId, InsId, from, to, UserId, offline);
        }

        public async Task<decimal> GetCustomerValue(string AccId, string InsId, DateTime from, DateTime to, string UserId, bool offline)
        {
            return await _pettyCashHdrDataAccess.GetCustomerPaymentValue(AccId, InsId, from, to, UserId, offline);
        }
        public async Task<PagerContract<PettyCashHdr>> ListPager(string type, string accountId, string instanceId, DateTime from, DateTime to, string userId)
        {
            // this will gives you the cash details if it is closed on the same day before
          
               var pager = new PagerContract<PettyCashHdr>
                {
                    NoOfRows = await _pettyCashHdrDataAccess.Count(type, accountId, instanceId, from, to),
                    List = await _pettyCashHdrDataAccess.ListData(type, accountId, instanceId, from, to, userId)
                };

            return pager;
        }

        public async Task<string> getLatestupdatedDate(string accountId, string instanceId, string userId)
        {
            return Convert.ToString(await _pettyCashHdrDataAccess.getLatestupdatedDate(accountId, instanceId, userId));
        }


    }
}
