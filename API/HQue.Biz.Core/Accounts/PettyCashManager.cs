﻿using HQue.Contract.Infrastructure;
using System;
using System.Threading.Tasks;
using HQue.DataAccess.Accounts;
using HQue.Contract.Infrastructure.Accounts;
using Utilities.Helpers;

namespace HQue.Biz.Core.Accounts
{
    public class PettyCashManager : BizManager
    {
        private readonly PettyCashDataAccess _pettyCashDataAccess;

        public PettyCashManager(PettyCashDataAccess pettyCashDataAccess) : base(pettyCashDataAccess)
        {
            _pettyCashDataAccess = pettyCashDataAccess;
        }

        public async Task<PettyCash> Save(PettyCash model)
        {
            model.TransactionDate = model.TransactionDate ?? CustomDateTime.IST;
            return await _pettyCashDataAccess.Save(model);
        }

        public async Task<PagerContract<PettyCash>> ListPager(string type, string accountId, string instanceId, DateTime from, DateTime to)
        {
            var pager = new PagerContract<PettyCash>
            {
                NoOfRows = await _pettyCashDataAccess.Count(type, accountId, instanceId, from, to),
                List = await _pettyCashDataAccess.ListData(type, accountId, instanceId, from, to)
            };

            return pager;
        }

        //public async Task<PettyCashSettings> SaveSettings(PettyCashSettings model)
        //{
        //    //model.TransactionDate = model.TransactionDate ?? CustomDateTime.IST;
        //    return await _pettyCashDataAccess.SaveSettings(model);
        //}
    }
}
