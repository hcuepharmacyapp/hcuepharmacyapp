﻿using System.Threading.Tasks;
using HQue.Contract.Infrastructure;
using HQue.DataAccess.Master;
using HQue.ApiDataAccess.External;
using HQue.Biz.Core.Setup;
using System;
using System.Linq;
using Utilities.Helpers;
using System.Collections.Generic;
using HQue.Contract.External.Leads;
using Newtonsoft.Json;
using System.IO;
using System.Text;
using HQue.DataAccess.Setup;

namespace HQue.Biz.Core.Leads
{
    public class LeadsManager
    {
        private readonly LeadsDataAccess _leadsDataAccess;
        private readonly LeadsApi _leadsApi;
        private readonly InstanceSetupManager _instanceSetupManager;
        private readonly InstanceSetupDataAccess _instanceDataAccess;

        public LeadsManager(LeadsDataAccess leadsDataAccess, LeadsApi leadsApi, InstanceSetupDataAccess instanceDataAccess,
            InstanceSetupManager instanceSetupManager)
        {
            _leadsDataAccess = leadsDataAccess;
            _leadsApi = leadsApi;
            _instanceDataAccess = instanceDataAccess;
            _instanceSetupManager = instanceSetupManager;
        }

        public async Task<Contract.Infrastructure.Leads.Leads> UpdateStatus(Contract.Infrastructure.Leads.Leads model)
        {
            if (model.LocalLeadStatus == "CMPT" && model.LeadStatus == "ACPH")
            {
                var result = await _leadsDataAccess.UpdateStatus(model);
                return result;
            }
            else
            {
                var result = await _leadsDataAccess.UpdateStatus(model);
                var itemCount = await _leadsDataAccess.getExternalRowCount(model);
                if (itemCount > 0)
                {
                    var rowId = string.Concat("[", string.Join(",", Enumerable.Range(0, itemCount).Select(i => i.ToString()).ToArray()), "]");
                    await UpdateLeadsExternal(model.LeadStatus, Convert.ToInt64(model.ExternalId), rowId);
                    var instance = await _instanceDataAccess.GetById(model.InstanceId);
                    var ExternalPharmaId = instance.ExternalId;
                    await updatePharmaLeads(model.LeadStatus, Convert.ToInt64(model.ExternalId), rowId, Convert.ToInt32(ExternalPharmaId));
                }
                return result;
            }


        }
        public async Task UpdateLeadStatus(string instanceId, string leadId, string leadStatus)
        {
            Contract.Infrastructure.Leads.Leads model = new Contract.Infrastructure.Leads.Leads();
            model.InstanceId = instanceId;
            model.ExternalId = leadId;
            model.LeadStatus = leadStatus;

            var result = await _leadsDataAccess.UpdateStatusWithLeadId(model);
            var itemCount = await _leadsDataAccess.getExternalRowCount(model);
            if (itemCount > 0)
            {
                var rowId = string.Concat("[", string.Join(",", Enumerable.Range(0, itemCount).Select(i => i.ToString()).ToArray()), "]");
                await UpdateLeadsExternal(model.LeadStatus, Convert.ToInt64(model.ExternalId), rowId);
            }

        }
        public async Task UpdateLeadInfo(LeadInfo info)
        {
            Contract.Infrastructure.Leads.Leads model = new Contract.Infrastructure.Leads.Leads();
            model.InstanceId = info.InstanceId;
            model.ExternalId = info.LeadId;

            if (!string.IsNullOrEmpty(info.PatientName)) model.Name = info.PatientName;
            if (!string.IsNullOrEmpty(info.Mobile)) model.Mobile = info.Mobile;
            if (!string.IsNullOrEmpty(info.Address)) model.Address = info.Address;
            if (!string.IsNullOrEmpty(info.Pincode)) model.Pincode = info.Pincode;
            if (!string.IsNullOrEmpty(info.Email)) model.Email = info.Email;
            if (!string.IsNullOrEmpty(info.DeliveryMode)) model.DeliveryMode = info.DeliveryMode;
            if (info.Latitude != 0) model.Lat = info.Latitude;
            if (info.Longitude != 0) model.Lng = info.Longitude;


            var result = await _leadsDataAccess.UpdateInfoWithLeadId(model);
            var itemCount = await _leadsDataAccess.getExternalRowCount(model);
            if (itemCount > 0)
            {
                var rowId = string.Concat("[", string.Join(",", Enumerable.Range(0, itemCount).Select(i => i.ToString()).ToArray()), "]");
                await UpdateLeadsExternal(model.LeadStatus, Convert.ToInt64(model.ExternalId), rowId);
            }

        }
        public async Task<PagerContract<Contract.Infrastructure.Leads.Leads>> ListPager(
            Contract.Infrastructure.Leads.Leads data)
        {
            var pager = new PagerContract<Contract.Infrastructure.Leads.Leads>
            {
                NoOfRows = await _leadsDataAccess.Count(data),
                List = await _leadsDataAccess.List(data)
            };

            return pager;
        }

        public async Task<List<Contract.Infrastructure.Leads.Leads>> GetAllLeadsById(string instanceId, string externalId)
        {
            return await _leadsDataAccess.LeadByExternalId(instanceId, externalId);
        }

        public async Task<List<Contract.Infrastructure.Leads.Leads>> GetAllLeadsByStatus(string instanceId, string leadStatus)
        {
            Contract.Infrastructure.Leads.Leads lead = new Contract.Infrastructure.Leads.Leads();
            lead.InstanceId = instanceId;
            lead.LeadStatus = leadStatus;


            return await _leadsDataAccess.ListByStatus(lead);
        }

        public async Task GetLeadsExternal(Int64 instanceId, Int64 leadId, string leadStatus)
        {
            var result = await _leadsApi.GetLeadsExternal(instanceId, leadId, leadStatus);
            /*As suggested by Martin Log writting commented by Poongodion 6/10/2017*/
            /* string name = string.Format("patientAPI.txt");
               FileInfo info = new FileInfo(name);
               using (StreamWriter writer = info.AppendText())
               {
                   string jsonStr =JsonConvert.SerializeObject(result);
                   //var msg = string.Format("{0} : InstanceID : {1} | LeadID : {2} \r\n Response : \r\n", DateTime.Now.ToString(), instanceId, leadId);
                   writer.Write("\r\n Response : \r\n", false, Encoding.UTF8);
                   writer.Write(jsonStr);
                   writer.Write("\r\n --------------------------------------------------------------------------------------------------------------------------------\r\n", false, Encoding.UTF8);
               }*/


            if (result.count > 0)
            {
                var instance = await _instanceSetupManager.GetByExternalPharmaId(instanceId);

                foreach (var leads in result.Rows)
                {
                    var mobile = "";
                    if (result.Rows[0].PatientDetails.PatientPhone.Length > 0)
                    {
                        mobile = Convert.ToString(leads.PatientDetails.PatientPhone[0].PhNumber);
                    }
                    var leadsModel = new Contract.Infrastructure.Leads.Leads
                    {
                        AccountId = instance.AccountId,
                        InstanceId = instance.Id,
                        Mobile = mobile,
                        DoctorName = leads.DoctorDetails.Doctor.FullName,
                        Name = leads.PatientDetails.Patient.FullName,
                        PatientId = Convert.ToString(leads.PatientDetails.Patient.PatientID),
                        ExternalId = leads.PatientPrescription.Select(m => m.PatientCaseID).FirstOrDefault().ToString(),
                        LeadDate = Convert.ToDouble(leads.requestSubmitDate).UnixTimeStampToDateTime().AddHours(5).AddMinutes(30),
                        CreatedBy = "admin",
                        UpdatedBy = "admin"
                    };

                    if (leads.PatientDetails.PatientAddress != null && leads.PatientDetails.PatientAddress.Length > 0)
                    {
                        leadsModel.Address = string.Format("{0}, {1}, {2}", leads.PatientDetails.PatientAddress[0].Address1, leads.PatientDetails.PatientAddress[0].Address2, leads.PatientDetails.PatientAddress[0].CityTown);
                        if (leadsModel.Address.Length == 4)
                            leadsModel.Address = null;

                        if (!string.IsNullOrEmpty(leads.PatientDetails.PatientAddress[0].Latitude))
                            leadsModel.Lat = decimal.Parse(leads.PatientDetails.PatientAddress[0].Latitude) as decimal? ?? 0;
                        if (!string.IsNullOrEmpty(leads.PatientDetails.PatientAddress[0].Longitude))
                            leadsModel.Lng = decimal.Parse(leads.PatientDetails.PatientAddress[0].Longitude) as decimal? ?? 0;

                    }
                    leadsModel = await _leadsDataAccess.Save(leadsModel);

                    foreach (var leadsItem in leads.PatientPrescription)
                    {
                        var leadsItemModel = new Contract.Infrastructure.Leads.LeadsProduct
                        {
                            AccountId = instance.AccountId,
                            InstanceId = instance.Id,
                            LeadsId = leadsModel.Id,
                            Name = leadsItem.Medicine,
                            Type = leadsItem.MedicineType,
                            Quantity = leadsItem.Quantity,
                            BA = leadsItem.BeforeAfter,
                            Dosage = $"{leadsItem.Dosage1}-{leadsItem.Dosage2}-{leadsItem.Dosage4}",
                            NumberofDays = leadsItem.NumberofDays,
                            CreatedBy = "admin",
                            UpdatedBy = "admin"
                        };

                        leadsItemModel = await _leadsDataAccess.SaveItem(leadsItemModel);
                    }
                }
            }
        }

        public async Task SaveLeads(Contract.Infrastructure.Leads.Leads leadData)
        {
            var instance = await _instanceSetupManager.GetByExternalPharmaId(Convert.ToInt64(leadData.PharmaExternalId));

            if(instance != null)
            {

                leadData.AccountId = instance.AccountId;
                leadData.InstanceId = instance.Id;
                leadData.LeadDate = Convert.ToDateTime(leadData.LeadDate);
                leadData.CreatedBy = "admin";
                leadData.UpdatedBy = "admin";

                var leadsModel = await _leadsDataAccess.Save(leadData);

                foreach (var item in leadData.LeadsProduct)
                {
                    item.AccountId = instance.AccountId;
                    item.InstanceId = instance.Id;
                    item.LeadsId = leadsModel.Id;
                    item.CreatedBy = "admin";
                    item.UpdatedBy = "admin";

                    var leadsItemModel = await _leadsDataAccess.SaveItem(item);
                }
            }


        }

        public async Task UpdateLeadsExternal(string leadStatus, Int64 patientCaseId, string rowId)
        {
            await _leadsApi.UpdateLeadsExternal(leadStatus, patientCaseId, rowId);
        }

        public async Task updatePharmaLeads(string leadStatus, Int64 patientCaseId, string rowId, Int32 ExternalPharmaId)
        {
            await _leadsApi.updatePharmaLeads(leadStatus, patientCaseId, rowId, ExternalPharmaId);
        }
    }
}