﻿using System;
using HQue.DataAccess.Dashboard;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HQue.Contract.Infrastructure.Dashboard;
using HQue.Communication.Email;
using Utilities.Helpers;
using System.Text;
using HQue.DataAccess.Setup;
using HQue.Contract.Infrastructure.Setup;

namespace HQue.Biz.Core.Dashboard
{
    public class DashboardManager
    {
        private readonly DashboardDataAccess _dashboardDataAccess;
        private readonly InstanceSetupDataAccess _instanceSetupDataAccess;
        private readonly EmailSender _emailSender;
        private readonly ConfigHelper _configHelper;

        public DashboardManager(DashboardDataAccess dashboardDataAccess, InstanceSetupDataAccess instanceSetupDataAccess, EmailSender emailSender, ConfigHelper configHelper)
        {
            _dashboardDataAccess = dashboardDataAccess;
            _instanceSetupDataAccess = instanceSetupDataAccess;
            _emailSender = emailSender;
            _configHelper = configHelper;
        }

        public async Task<IEnumerable<NameValue>> GetTopLowStockItem(string accountId, string instanceId)
        {
            var result = await _dashboardDataAccess.GetTopLowStockItem(accountId, instanceId);
            //return result.OrderBy(x => Convert.ToDecimal(x.Value)).ToList();
            return result;
        }

        public async Task<IEnumerable<NameValue>> TrendingTopSales(string accountId, string instanceId)
        {
            var result = await _dashboardDataAccess.GetTopTrendingSales(accountId, instanceId);
            //return result.OrderByDescending(x => Convert.ToDecimal(x.Value)).ToList();
            return result;
        }

        // Newly Added Gavaskar 27-10-2016 Start
        public async Task<IEnumerable<NameValue>> Sales(string instanceId, string accountId)
        {
            return await _dashboardDataAccess.Sales(instanceId, accountId);
        }

        public async Task<IEnumerable<NameValue>> Purchase(string instanceId, string accountId)
        {
            return await _dashboardDataAccess.Purchase(instanceId, accountId);
        }

        // Newly Added Gavaskar 27-10-2016 End

        // Newly Added Gavaskar 15-11-2016 Start

        public async Task<IEnumerable<NameValue>> CurrentStock(string accountId, string instanceId)
        {
            return await _dashboardDataAccess.CurrentStock(accountId, instanceId);
        }

        public async Task DashboardDailyMail(string accountId)
        {
            var url = $"{_configHelper.UrlBuilder.InvoiceUrl}{accountId}";

            var result = await _dashboardDataAccess.DashboarDailyReport_CardDetails(accountId);
            var Marginresult = await _dashboardDataAccess.DashboarDailyReport_GrossMargin(accountId);
            var PatientVisit = await _dashboardDataAccess.DashboarDailyReport_PatientVisit(accountId);
            var BouncedItems = await _dashboardDataAccess.DashboarDailyReport_BouncedItems(accountId);
            StringBuilder BodyContent = _emailSender.DashboardBodyBuilder(result, Marginresult, PatientVisit, BouncedItems, url);
            // _emailSender.Send("dr.rajesh@goclinix.com", 10, _emailSender.DashboardBodyBuilder(result, Marginresult, PatientVisit, BouncedItems, url));
            _emailSender.Send("saravanan@goclinix.com", 10, BodyContent);
            _emailSender.Send("sundar@hcue.co", 10, BodyContent);
            _emailSender.Send("gavaskar@hcue.co", 10, BodyContent);
            _emailSender.Send("Dr.rajesh@goclinix.com", 10, BodyContent);
            _emailSender.Send("Lokesh@goclinix.com", 10, BodyContent);
            _emailSender.Send("accounts@goclinix.com", 10, BodyContent);
            _emailSender.Send("martin@qbitz.in", 10, BodyContent);
        }

        public async Task PlusPharmacyMail()
        {
            var adminDetails = await _dashboardDataAccess.DashboardGetAdminDetails();
            foreach (var item in adminDetails)
            {
                var url = $"{_configHelper.UrlBuilder.InvoiceUrl}{item.OwnerId}";
                var result = await _dashboardDataAccess.PlusPharmacyReport_CardDetails(item.OwnerId, item.EmailId);
                var Marginresult = await _dashboardDataAccess.PlusPharmacyReport_GrossMargin(item.OwnerId);
                StringBuilder BodyContent = _emailSender.PlusPharmacyBodyBuilder(result, Marginresult, url);
                _emailSender.Send(item.EmailId, 15, BodyContent);
               
            }

            //var url = $"{_configHelper.UrlBuilder.InvoiceUrl}{accountId}";
            //var result = await _dashboardDataAccess.PlusPharmacyReport_CardDetails(accountId);
            //var Marginresult = await _dashboardDataAccess.PlusPharmacyReport_GrossMargin(accountId);
            //StringBuilder BodyContent = _emailSender.PlusPharmacyBodyBuilder(result, Marginresult, url);
            //_emailSender.Send("sundar@hcue.co", 15, BodyContent);
            //_emailSender.Send("bagyanath@hcue.co", 15, BodyContent);
            //_emailSender.Send("gavaskar@hcue.co", 15, BodyContent);
            //_emailSender.Send("siva@hcue.co", 15, BodyContent);
            //_emailSender.Send("martin@qbitz.in", 15, BodyContent);
            //_emailSender.Send("prateekkarora@gmail.com", 15, BodyContent); //Added by Gavaskar on 05/10/2017 (by Sundar)
            //_emailSender.Send("anilbhatia82@yahoo.co.in", 15, BodyContent); //Added by Gavaskar on 05/10/2017 (by Sundar)
            //_emailSender.Send("prateekkarora@gmail.com", 15, BodyContent); //Added by Poongodi on 21/06/2017 (by Sundar)
        }

        public async Task DashboardManagementMail()
        {
            var url = $"{_configHelper.UrlBuilder.InvoiceUrl}{""}";

            var result = await _dashboardDataAccess.DashboarManagementReport_Details();
            var PharmaLiveList = await _dashboardDataAccess.DashboarManagementReport_PharmacyLiveList();
            var PharmaTrialList = await _dashboardDataAccess.DashboarManagementReport_PharmacyTrialList();
            var PharmaEdgeList = await _dashboardDataAccess.DashboarManagementReport_PharmacyEdgeList();

            StringBuilder BodyContent = _emailSender.DashboardManagementBodyBuilder(result, PharmaLiveList, PharmaTrialList, PharmaEdgeList, url);
            //_emailSender.Send("management@hcue.co", 11, BodyContent);
            _emailSender.Send("sundar@hcue.co", 11, BodyContent); 
            // _emailSender.Send("venky@hcue.co", 11, BodyContent); //Commented by Gavaskar on 21/10/2017 (by Bagyanath)
            // _emailSender.Send("sthulasi@hcue.co", 11, BodyContent); //Commented by Gavaskar on 21/10/2017 (by Bagyanath)
            _emailSender.Send("gavaskar@hcue.co", 11, BodyContent);
            //  _emailSender.Send("salessupport@hcue.co", 11, BodyContent); //Commented by Gavaskar on 21/10/2017 (by Bagyanath)

            //_emailSender.Send("harish.kumaar@hcue.co", 11, BodyContent);
            //_emailSender.Send("Anusha.c@hcue.co", 11, BodyContent);
            //_emailSender.Send("manivannan@hcue.co", 11, BodyContent);
        }
        public async Task DailyMailForShopOwner()
        {
            var url = $"{_configHelper.UrlBuilder.InvoiceUrl}{""}";

            var result = await _dashboardDataAccess.DashboarManagementReport_Details();
            var PharmaLiveList = await _dashboardDataAccess.DashboarManagementReport_PharmacyLiveList();
            var PharmaTrialList = await _dashboardDataAccess.DashboarManagementReport_PharmacyTrialList();
            var PharmaEdgeList = await _dashboardDataAccess.DashboarManagementReport_PharmacyEdgeList();

            StringBuilder BodyContent = _emailSender.DashboardManagementBodyBuilder(result, PharmaLiveList, PharmaTrialList, PharmaEdgeList, url);
            //_emailSender.Send("management@hcue.co", 11, BodyContent);
            _emailSender.Send("sundar@hcue.co", 11, BodyContent);
        }


        public async Task DailyMailerForStock(string instanceid)
        {
            var url = $"{_configHelper.UrlBuilder.InvoiceUrl}{""}";
            var result = await _dashboardDataAccess.DashboarDailyStockReport(instanceid);
            var ReorderResult = await _dashboardDataAccess.DashboarDailyStockReportOfReorderQuantity(instanceid);
            var ZeroQuantityResult=await _dashboardDataAccess.DashboarDailyStockReportOfZeroQuantity(instanceid);
            var TrendingSalesResult= await _dashboardDataAccess.DashboarDailyTrendindSalesQuantity(instanceid);
            var NonMovingResult = await _dashboardDataAccess.DashboarDailyNonMovingQuantity(instanceid);
            var AbouttoExpireResult= await _dashboardDataAccess.DashboarDailyAboutToExpireReport(instanceid);
            var ExpiredResult = await _dashboardDataAccess.DashboarDailyExpiredResultReport(instanceid);

 
            StringBuilder BodyContent = _emailSender.DailyStockBodyBuilder(result,ReorderResult, ZeroQuantityResult, NonMovingResult, TrendingSalesResult, AbouttoExpireResult, ExpiredResult, url);
            //_emailSender.Send("manivannan@hcue.co", 13, BodyContent);
            // _emailSender.Send("bagyanath@hcue.co", 13, BodyContent);
            _emailSender.Send("sundar@hcue.co", 10, BodyContent);
            _emailSender.Send("bagyanath@hcue.co", 10, BodyContent);
            _emailSender.Send("durga.reddy@hcue.co", 10, BodyContent);
            _emailSender.Send("drvenkat@hcue.co", 10, BodyContent);
            _emailSender.Send("poongodi@hcue.co", 10, BodyContent);
            _emailSender.Send("pharmatesting@hcue.co", 10, BodyContent);
            _emailSender.Send("manivannan@hcue.co", 10, BodyContent);


        }
        // Newly Added Gavaskar 15-11-2016 End

        public async Task<BarGraph> GetPendingLeads(string accountId, string instanceId)
        {
            return await _dashboardDataAccess.GetPendingLeads(accountId, instanceId);
        }

        public async Task<IEnumerable<NameValue>> AboutToExpireProduct(string accountId, string instanceId)
        {
            return await _dashboardDataAccess.GetAboutToExpireProduct(accountId, instanceId);
        }

        //Newly added by Manivannan on 25-02-2017
        public async Task<IEnumerable<Instance>> LoadInstances(string accountId)
        {
            return await _instanceSetupDataAccess.GetInstancesByAccountId(accountId);
        }
    }
}
