﻿using System.Collections.Generic;
using HQue.Contract.Infrastructure.Setup;
using HQue.DataAccess.Setup;
using HQue.DataAccess.UserAccount;
using HQue.Contract.Infrastructure.UserAccount;
using HQue.Contract.Infrastructure;
using System;
using System.Threading.Tasks;
using System.Linq;
using HQue.Biz.Core.UserAccount;
using HQue.Contract.Infrastructure.Settings;
using HQue.Contract.Infrastructure.Inventory;

namespace HQue.Biz.Core.Setup
{
    public class AccountSetupManager : BizManager
    {
        private readonly AccountSetupDataAccess _accountSetupDataAccess;
        private readonly UserManager _userManager;
        private readonly UserDataAccess _userDataAccess;

        public AccountSetupManager(AccountSetupDataAccess accountSetupDataAccess, UserManager userManager,
            UserDataAccess userDataAccess)
            : base(accountSetupDataAccess)
        {
            _accountSetupDataAccess = accountSetupDataAccess;
            _userManager = userManager;
            _userDataAccess = userDataAccess;
        }

        public async Task<Account> Save(Account model)
        {
            if (!await ValidateAccount(model) || !await ValidateUser(model.User))
                return model;

            var account = await _accountSetupDataAccess.Save(model);
             
            await _userManager.SaveAdminUser(account);

            await _userManager.SaveFinyear(account);

            return model;
        }

        public Task<List<Account>> List(Account data)
        {
            return _accountSetupDataAccess.List(data);
        }

        public async Task<PagerContract<Account>> ListPager(Account data)
        {
            var pager = new PagerContract<Account>
            {
                NoOfRows = await _accountSetupDataAccess.Count(data),
                List = await _accountSetupDataAccess.List(data)
            };

            return pager;
        }

        public async Task<Account> UpdateGroupDetails(Account group1)
        {
            IEnumerable<Account> list1 = await _accountSetupDataAccess.CheckUniqueAccount(group1);

            if (list1.Any())
            {
                if (list1.FirstOrDefault().Id != group1.Id || list1.Count() > 1)
                    throw new Exception($" Record {group1.Phone} already exists");
            }

            await _accountSetupDataAccess.UpdateGroupDetails(group1);
            return group1;
        }

        public async Task<Account> GetGroupDetails(string id)
        {
            return await _accountSetupDataAccess.GetGroupDetails(id);
        }

        private async Task<bool> ValidateAccount(Account account)
        {
            var accounts = await _accountSetupDataAccess.CheckUniqueAccount(account);
            if (accounts.Any())
                throw new Exception($" Record {account.Phone} already exists");

            return true;
        }

        private async Task<bool> ValidateUser(HQueUser user)
        {
            var users = await _userDataAccess.CheckUniqueUserId(user);
            if (users.Any())
                throw new Exception($"User id {user.UserId} already exists");

            return true;
        }

        public async Task<List<CustomSettingsDetail>> GetCustomSettings(string AccountId, string InstanceId)
        {
            return await _accountSetupDataAccess.GetCustomSettings(AccountId, InstanceId);
        }

        public async Task<List<CustomSettingsDetail>> SaveCustomSettings(List<CustomSettingsDetail> list)
        {
            return await _accountSetupDataAccess.SaveCustomSettings(list);
        }

        //Added by Sumathi on 10-Nov-18
        public async Task<bool?> GetCustomSettingsDetail(string AccountId, string InstanceId,int CustomSettingsGroupId)
        {
            List<CustomSettingsDetail> lst = await this.GetCustomSettings(AccountId, InstanceId);
            CustomSettingsDetail cs = lst.Where(x => x.CustomSettingsGroupId == CustomSettingsGroupId).FirstOrDefault<CustomSettingsDetail>();
            return  cs !=null ? cs.IsActive : false;
            
        }

        //Added by Sarubala on 14-12-2018
        public async Task<List<DomainValues>> GetInstanceType()
        {
            return await _accountSetupDataAccess.GetInstanceType();
        }
    }
}
