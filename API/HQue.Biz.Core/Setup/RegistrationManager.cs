﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HQue.DataAccess.Setup;
using HQue.Contract.Infrastructure.Setup;
using HQue.Biz.Core.UserAccount;
// Newly Added Gavaskar 05/10/2016
using HQue.Communication.Sms;
using HQue.Contract.Base;
using HQue.Contract.Infrastructure.UserAccount;
using HQue.Communication.Email;
using System.Text;
using HQue.Contract.Infrastructure.Master;

namespace HQue.Biz.Core.Setup
{
    public class RegistrationManager : BizManager
    {
        private readonly RegistrationDataAccess _registrationDataAccess;
        private readonly AccountSetupManager _accountSetupManager;
        private readonly InstanceSetupManager _instanceSetupManager;
        private readonly UserManager _userManager;
        // Newly Added Gavaskar 05/10/2016
        private readonly SmsSender _smsSender;
        private readonly EmailSender _emailSender;//Newly addded by lawrence on 26Jun2017

        public RegistrationManager(RegistrationDataAccess registrationDataAccess, AccountSetupManager accountSetupManager, InstanceSetupManager instanceSetupManager, UserManager userManager, SmsSender smsSender, EmailSender emailSender) : base(registrationDataAccess)
        {
            _registrationDataAccess = registrationDataAccess;
            _accountSetupManager = accountSetupManager;
            _instanceSetupManager = instanceSetupManager;
            _userManager = userManager;
            // Newly Added SmsSender Gavaskar  05/10/2016
            _smsSender = smsSender;
            _emailSender = emailSender;
        }

        //public async Task<bool> GetRegisterType(string accountId)
        //{
        //    return await _accountSetupManager.GetRegisterType(accountId);
        //}

        //public async Task<Registration> Save(Registration model, int[] bdoPermissions)
        //{

        //    //var account = await _registrationDataAccess.Save(model.account);
        //    var account = await _accountSetupManager.SaveAccount(model.account);
        //    model.instance.AccountId = account.Id;

        //    var instance = await _instanceSetupManager.Save(model.instance);

        //    model.instance.Timings.Select(timing => { timing.AccountId = model.account.Id; timing.InstanceId = instance.Id; return timing; }).ToList();

        //    await _registrationDataAccess.DeleteTimings(model.account.Id, instance.Id);
        //    var timings = await _registrationDataAccess.SaveTimings(model.instance.Timings);

        //    if (bdoPermissions.Contains(UserAccess.Payment))
        //    {
        //        var payments = _registrationDataAccess.SavePayment(model.account.Payments);
        //    }

        //    return model;
        //}

        public async Task EmailToVendors(Instance instance)
        {
            List<Vendor> lstVendor = new List<Vendor>();

            lstVendor = await _registrationDataAccess.List(instance.AccountId);

            foreach (var item in lstVendor)
            {
                List<string> str = new List<string>();
                if (!string.IsNullOrEmpty(item.Email) && !string.IsNullOrEmpty(item.Name))
                {
                    str.Add(instance.Name);
                    str.Add(string.Format("{0} {1} - {2}", instance.Area, instance.City, instance.Pincode));
                    str.Add(instance.GsTinNo);
                    str.Add(item.Name);
                    str.Add(instance.ContactName);
                    str.Add(instance.Mobile);
                    //Unit testing
                    //item.Email = "balasundar@hcue.co";
                    SendNotificationCommunication(item.Email, str);
                }
            }
        }

        public async Task<Registration> CreateBranch(Registration model)
        {
            var instance = await _instanceSetupManager.Save(model.instance);
            List<Vendor> lstVendor = new List<Vendor>();

            model.instance.Timings.Select(timing => { timing.AccountId = model.instance.AccountId; timing.InstanceId = model.instance.Id; return timing; }).ToList();

            await _registrationDataAccess.DeleteTimings(model.account.Id, instance.Id);
            var timings = await _registrationDataAccess.SaveTimings(model.instance.Timings);

            //Lawrence start
            if (model.instance.isEmail)
            {
                await EmailToVendors(model.instance);
            }
            //Lawrence End
            model.instance.Id = instance.Id; // Added by Sarubala on 31-12-18
            return model;
        }

        public async Task<PharmacyUploads> SaveFiles(PharmacyUploads imageUploads)
        {
            var uploads = _registrationDataAccess.SaveUploads(imageUploads);
            return imageUploads;
        }

        //public async Task<Registration> Update(Registration model, int[] bdoPermissions)
        //{
        //    var account = await _accountSetupManager.UpdateAccount(model.account);
        //    //model.instance.AccountId = account.Id;
        //    if (bdoPermissions.Contains(UserAccess.PharmacyEdit) || bdoPermissions.Contains(UserAccess.CreateLicense))
        //    {
        //        var instance = await _instanceSetupManager.UpdateInstance(model.instance);

        //        await _registrationDataAccess.DeleteTimings(model.account.Id, instance.Id);
        //        var timings = await _registrationDataAccess.SaveTimings(model.instance.Timings);
        //    }
        //    if (bdoPermissions.Contains(UserAccess.Payment))
        //    {
        //        await _registrationDataAccess.DeletePayments(model.account.Id);
        //        var payments = _registrationDataAccess.SavePayment(model.account.Payments);
        //    }

        //    return model;
        //}

        public async Task<Registration> UpdateBranch(Registration model)
        {
            var instance = await _instanceSetupManager.UpdateInstance(model.instance);
            List<Vendor> lstVendor = new List<Vendor>();

            await _registrationDataAccess.DeleteTimings(model.instance.AccountId, instance.Id);
            var timings = await _registrationDataAccess.SaveTimings(model.instance.Timings);

            if (model.instance.isEmail)
            {
                await EmailToVendors(model.instance);
            }

            return model;
        }

        private void SendNotificationCommunication(string userid, List<string> msg)
        {
            var status = _emailSender.SendGSTMail(userid, msg);
        }


        //public async Task<HQueUser> CreateLicense(Account account)
        //{
        //    var accountId = account.Id;
        //    account.AccountId = accountId;

        //    var user = _accountSetupManager.SaveAdminUser(account);


        //    var licenseCount = account.LicenseCount;
        //    //return await _vendorPurchaseDataAccess.GetVendorPurchaseSingleData(id);
        //    await _registrationDataAccess.CreateLicense(account);
        //    return account.User;
        //}

        //public async Task<int> AddLicense(string accountId, int addnLicense)
        //{
        //    //return await _vendorPurchaseDataAccess.GetVendorPurchaseSingleData(id);
        //    return await _registrationDataAccess.AddLicense(accountId, addnLicense);
        //}

        //public async Task<Registration> EditRegistration(string accountId, string instanceId)
        //{
        //    return await _registrationDataAccess.GetRegistrationSingleData(accountId, instanceId);
        //}

        public async Task<Registration> EditBranch(string instanceId)
        {
            return await _registrationDataAccess.GetBranchData(instanceId);
        }

        public async Task<List<PharmacyUploads>> GetBranchImages(string accountId, string instanceId)
        {
            return await _registrationDataAccess.GetBranchImages(accountId, instanceId);
        }

        public async Task DeleteBranchImage(string imgId)
        {
            await _registrationDataAccess.DeleteBranchImage(imgId);
        }

        ////Newly Added Gavaskar 04-10-2016 Start
        //public async Task<bool> SaveOTP(BaseRegistrationOTP registrationOTP, string mobileno)
        //{
        //    var mobileExists = await _registrationDataAccess.contactMobileExists(mobileno);
        //    if (!mobileExists)
        //    {
        //        _smsSender.GenerateOTP();
        //        registrationOTP.OTPNumber = _smsSender.GenerateOTP();
        //        await _registrationDataAccess.SaveOTP(registrationOTP);
        //        await _smsSender.Send(mobileno, 7, registrationOTP.OTPNumber);
        //    }
        //    return mobileExists;
        //}

        //public async Task<bool> ConfirmOTP(BaseRegistrationOTP registrationOTP, string OTP)
        //{
        //    return await _registrationDataAccess.ConfirmOTP(registrationOTP);
        //}

        //public Task<List<HQueUser>> BDOList()
        //{
        //    return _registrationDataAccess.BDOList();
        //}

        //public async Task AssignBDO(string instanceId, string bdoId)
        //{
        //    await _registrationDataAccess.AssignBDO(instanceId, bdoId);
        //}

        //public async Task<List<Account>> ListPager(int registrationType, int[] bdoPermissions, string bdoId)
        //{
        //    //var pager = new PagerContract<Account>
        //    // {
        //    //NoOfRows = await _accountSetupDataAccess.Count(data),
        //    if (bdoPermissions.Contains(UserAccess.Payment) || bdoPermissions.Contains(UserAccess.CreateLicense))
        //    {
        //        return await _registrationDataAccess.List(registrationType);
        //    }
        //    else
        //    {
        //        return await _registrationDataAccess.List(registrationType, bdoId);
        //    }
        //    // };

        //    //return pager;
        //}
        // Added by Gavaskar 01-12-2017 Start
        public async Task<string> GetGstSelectedCustomer(string AccountId)
        {
            return await _registrationDataAccess.GetGstSelectedCustomer(AccountId);
        }
        public async Task<string> GetGstSelectedVendor(string AccountId)
        {
            return await _registrationDataAccess.GetGstSelectedVendor(AccountId);
        }
        // Added by Gavaskar 01-12-2017 End
    }
}
