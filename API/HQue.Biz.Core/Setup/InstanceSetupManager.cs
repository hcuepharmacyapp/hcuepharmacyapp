﻿using System.Collections.Generic;
using HQue.Contract.Infrastructure.Setup;
using HQue.Contract.Infrastructure.UserAccount;
using HQue.DataAccess.Setup;
using HQue.DataAccess.UserAccount;
using Utilities.Helpers;
using System;
using HQue.Contract.Infrastructure;
using System.Threading.Tasks;
using HQue.ApiDataAccess.External;
using HQue.Contract.External.Common;
using System.Linq;
using HQue.ApiDataAccess.Internal;
using HQue.Biz.Core.UserAccount;
using HQue.Contract.Infrastructure.Settings;

namespace HQue.Biz.Core.Setup
{
    public class InstanceSetupManager : BizManager
    {
        private readonly InstanceSetupDataAccess _instanceDataAccess;
        private readonly UserDataAccess _userDataAccess;
        private readonly PharmaApi _pharmaApi;
        private readonly UserManager _userManager;
        private readonly ConfigHelper _configHelper;
        private readonly InstanceApi _instanceApi;

        public InstanceSetupManager(InstanceSetupDataAccess instanceDataAccess, UserDataAccess userDataAccess, PharmaApi pharmaApi, UserManager userManager,
            ConfigHelper configHelper,InstanceApi instanceApi) : base(instanceDataAccess)
        {
            _instanceDataAccess = instanceDataAccess;
            _userDataAccess = userDataAccess;
            _pharmaApi = pharmaApi;
            _userManager = userManager;
            _configHelper = configHelper;
            _instanceApi = instanceApi;
        }

        public async Task<Instance> Save(Instance model)
        {
            await ValidateLicense(model);
            await ValidateUserId(model.UserList);

            model.Status = true;
            var extPharma = await SavePharmaExternal(model);
            if(extPharma != null && extPharma.PharmaID.ToString() != null)
            {
                model.ExternalId = extPharma.PharmaID.ToString();
            }
           
            var instance = await _instanceDataAccess.Save(model);
            if (!model.UserList.Any())
                return model;

            await _userManager.SaveInstanceUser(instance);
            //return model; // Change for Registration
			return instance;
        }

        public async Task<Instance> SaveForOffline(Instance model)
        {
            model.Status = true;            
            var instance = await _instanceDataAccess.SaveToLocal(model);
            return model;
        }

        public async Task<Account> SaveAccountsForOffline(Account model)
        {
            var account = await _instanceDataAccess.SaveAccountToLocal(model);
            return model;
        }
        /// <summary>
        /// Offline version number method added by POongodi on 05/07/2017
        /// </summary>
        /// <param name="model"></param>
        /// <param name="sVersion"></param>
        /// <param name="sUpdatedby"></param>
        /// <returns></returns>
        public async Task<bool> UpdateOfflineVersion(Instance model, string sVersion, string sUpdatedby)
        {
            bool result = await _instanceDataAccess.UpdateOfflineVersion(model, sVersion, sUpdatedby);
            return result;
        }
        
        private async Task ValidateLicense(Instance model)
        {
            var balance = await GetInstanceCount(model.AccountId);
            if (balance <= 0)
            {
                throw new Exception("You have reached the limit of your license");
            }
        }

        public async Task<Instance> GetById(string instanceId)
        {
            var result = await _instanceDataAccess.GetById(instanceId);

            if (string.IsNullOrEmpty(result.GsTinNo))
            {
                result.GsTinNo = "";
            }
            return result;
        }

        public async Task<List<Instance>> GetInstancesByAccountId(string accountid)
        {
            return await _instanceDataAccess.GetInstancesByAccountId(accountid);
        }

        public async Task<PagerContract<Instance>> ListPager(Instance data)
        {
            var pager = new PagerContract<Instance>
            {
                NoOfRows = await _instanceDataAccess.Count(data),
                List = await _instanceDataAccess.List(data)
            };

            return pager;
        }

        public async Task<int> GetInstanceCount(string accountId)
        {
            return await _instanceDataAccess.GetInstanceCount(accountId);
        }

        public async Task<int> GetTotalInstanceCount(string accountId)
        {
            return await _instanceDataAccess.GetTotalInstanceCount(accountId);
        }
        public async Task<List<Instance>> List(string accountId)
        {
            if (_configHelper.AppConfig.IsSqlServer)
                return await _instanceDataAccess.List(accountId);
            return await _instanceApi.GetInstance(accountId);
        }
        public async Task<string> UpdateOfflineStatus(string instance)
        {            
            var offlineVersionNo = _configHelper.AppConfig.Version.ToString();
            return await _instanceDataAccess.UpdateOfflineStatus(instance, offlineVersionNo);
        }

        public async Task<SaleSettings> GetShortCutKeySetting(string AccountId, string InstanceId)
        {
            var ShortCutKeySetting = await _instanceDataAccess.GetShortCutKeySetting(AccountId, InstanceId);
            return ShortCutKeySetting;
        }

        public async Task<IEnumerable<Instance>> List(Instance data)
        {
            return await _instanceDataAccess.List(data);
        }

        public async Task<IEnumerable<Instance>> GetByTinNo(Instance data)
        {
            return await _instanceDataAccess.GetByTinNo(data.TinNo);
        }

        private async Task<bool> ValidateUserId(IEnumerable<HQueUser> userList)
        {
            foreach(var user in userList)
            {
                var users = await _userDataAccess.CheckUniqueUserId(user);
                if (users.Count > 0)
                    throw new Exception($"User id {user.UserId} already exists");
            }
            return true;
        }

        public async Task<List<Instance>> ReportList(string accountId)
        {
            return await _instanceDataAccess.List(accountId);
        }

        public async Task<ExtPharma> SavePharmaExternal(Instance data)
        {
            var result= await _pharmaApi.SavePharmaExternal(data);
            return result;
        }

        public async Task<Instance> GetByExternalPharmaId(Int64 instanceId)
        {
            return await _instanceDataAccess.GetByExternalPharmaId(instanceId);
        }

        public async Task<Instance> UpdateInstance(Instance data)
        {
            var instance = await _instanceDataAccess.Update(data);
            //var extPharma = await SavePharmaExternal(data);
            return instance;
        }

        public async Task<HQueUser> GetHqueUserId(string AccountId,string LoginUserId)
        {
            var HqueUserId = await _instanceDataAccess.GetHqueUserId(AccountId, LoginUserId);
            return HqueUserId;
        }
        public async Task<List<UserAccess>> GetUserAccess(string AccountId, string Id)
        {
            var UserAccess = await _instanceDataAccess.GetUserAccess(AccountId, Id);
            return UserAccess;
        }
        public async Task<Account> AccountList(string accountId)
        {
           return await _instanceDataAccess.AccountList(accountId);           
        }
    }
}
