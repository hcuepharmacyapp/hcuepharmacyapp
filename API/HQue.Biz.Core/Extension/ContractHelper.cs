using HQue.Contract.Infrastructure.Estimates;
using HQue.Contract.Infrastructure.Inventory;
using HQue.Contract.Infrastructure.Master;

namespace HQue.Biz.Core.Extension
{
    internal static class ContractHelper
    {
        public static Doctor GetDoctorObject(this Sales data)
        {
            var doctor = new Doctor
            {
                Name = data.DoctorName,
                Code = "",
                Mobile = data.DoctorMobile,
                City = data.DoctorAddress
            };
            doctor.SetExecutionQuery(data.GetExecutionQuery(), data.WriteExecutionQuery);
            return doctor;
        }

        public static Doctor GetDoctorObject(this Estimate data)
        {
            var doctor = new Doctor
            {
                Name = data.DoctorName,
                Code = "",
                Mobile = data.DoctorMobile
            };

            return doctor;
        }
    }
}