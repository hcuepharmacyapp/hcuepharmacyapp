﻿using HQue.Biz.Core.UserAccount;
using HQue.Contract.Infrastructure.UserAccount;

namespace HQue.Biz.Core
{
    public static class BizExtensionHelper
    {
        public static void SetAllUserAccess(this HQueUser data)
        {
            data.RoleAccess = UserAccessManager.GetAllPermission();
        }
    }
}
