﻿using HQue.Contract.External.Sync;
using HQue.DataAccess.Sync;
using HQue.ServiceConnector.Connector;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utilities.Helpers;

namespace HQue.Biz.Core.Sync
{
    public class SyncDataManager
    {
        private readonly ConfigHelper _configHelper;
        public SyncDataManager(ConfigHelper configHelper)
        {
            _configHelper = configHelper;
        }
        
        public Task<string> ExportOnlineData(string instanceID)
        {
            var sc = new RestConnector();
            var extData = new ExportDataRequest { InstanceID = instanceID };
            
            var result = sc.Post2Asyc<string>(_configHelper.InternalApi.SyncUrl, extData);
            return result;
        }
        public Task<string> GetOnlinePatientData(string accountID,string instanceID)
        {
            var sc = new RestConnector();
            var extData = new ExportDataRequest { AccountID = accountID, InstanceID = instanceID };

            var result = sc.Post2Asyc<string>(_configHelper.InternalApi.SyncPatientUrl, extData);
            return result;
        }

    }

   
}
