﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HQue.DataAccess;
using HQue.DataAccess.Sync;
using HQue.Contract.External;
using HQue.Contract.Infrastructure.Settings;

namespace HQue.Biz.Core.Sync
{
    public class SyncManager
    {
        private readonly SyncDataAccess _syncDataAccess;
        private readonly SyncOnlineDataAccess _syncOnlineDataAccess;

        public SyncManager(SyncDataAccess syncDataAccess, SyncOnlineDataAccess syncOnlineDataAccess)
        {
            _syncDataAccess = syncDataAccess;
            _syncOnlineDataAccess = syncOnlineDataAccess;
        }

        public Task<bool> SyncDataToServer(SyncObject data)
        {
            return _syncDataAccess.WriteToDatabase(data);
        }
        
        public Task<string> ExportOnlineData(string instanceID)
        {
            return _syncOnlineDataAccess.GetOnlineInstanceData(instanceID);
        }

        public Task<string> ExportSeedData(string accountId, string instanceID, int seedIndex)
        {
            return _syncOnlineDataAccess.GetInstanceSeedData(accountId, instanceID, seedIndex);
        }

        public Task<bool> SaveSyncPendingQuery(List<SyncObject> list)
        {
            return _syncDataAccess.SaveSyncPendingQuery(list);
        }

        public Task<bool> ExecuteSyncPendingQuery(string AccountId, string InstanceId)
        {
            return _syncDataAccess.ExecuteSyncPendingQuery(AccountId, InstanceId);
        }

        public async Task<bool> GetFeatureAccess(int GroupId, string AccountId, string InstanceId)
        {
            return await _syncDataAccess.GetFeatureAccess(GroupId, AccountId, InstanceId);
        }

        //public Task<List<CustomSettingsDetail>> GetCustomSettings(string accountId, string instanceId,int customSettingsGroupId)
        //{
        //    return  _syncDataAccess.GetCustomSettings(accountId, instanceId, customSettingsGroupId);
        //}



        //public Task<bool> RemoveSyncData(string AccountID, string InstanceID, int SeedIndex)
        //{
        //    return _syncOnlineDataAccess.RemoveSyncData(AccountID, InstanceID,SeedIndex);
        //}


        //Added by Sumathi on 25-Nov-18
        public Task<bool> IsSyncEnabled(string AccountId, string InstanceId)
        {
            return Task.FromResult(_syncDataAccess.IsSyncEnabled(AccountId, InstanceId));
        }
        public async Task<bool> SyncDataDeleteScheduler(string AccountId, string Instanceid, string UserId)
        {
            return await _syncDataAccess.SyncDataDeleteScheduler(AccountId, Instanceid, UserId);
        }

        public async Task<bool> LogScheduler(string Id, int EventType, string Data, DateTime StartTime)
        {
            return await _syncDataAccess.LogScheduler(Id, EventType, Data, StartTime);
        }

        public async Task<bool> LogSchedulerUpdate(string Id, int EventType, string Data, DateTime EndTime)
        {
            return await _syncDataAccess.LogSchedulerUpdate(Id, EventType, Data, EndTime);
        }
    }
}
