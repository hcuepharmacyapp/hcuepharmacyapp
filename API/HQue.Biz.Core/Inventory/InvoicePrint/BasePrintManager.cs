﻿using HQue.Contract.Infrastructure.Inventory;
using System;
using System.Collections.Generic;
using System.Linq;
using Utilities.Report;
using Utilities.Helpers;

namespace HQue.Biz.Core.Inventory.InvoicePrint
{
    public class BasePrintManager
    {
        #region A4_Generic_template

        protected string GenerateSalesReport_A4(IInvoice data)
        {
            bool gstSelection = Convert.ToBoolean(data.Instance.Gstselect);
            var rpt = new PlainTextReport
            {
                Columns = 80,
                Rows = 40
            };
            WriteReportHeader_A4(rpt, data);

            if (data.InvoiceItems.Count() == 0)
            {
                WriteReportOnlyReturnItems_A4(rpt, data);
            }
            else
            {
                WriteReportItems_A4(rpt, data);
                if (data.TaxRefNo == 0)
                    WriteReportFooter_A4(rpt, data);
                else
                    WriteReportFooter_A4_GST(rpt, data);
            }

            return rpt.Content.ToString();
        }

        private void WriteReportHeader_A4(PlainTextReport rpt, IInvoice data)
        {
            bool gstSelection = Convert.ToBoolean(data.Instance.Gstselect);
            //Write header
            rpt.NewLine();
            //rpt.Write("#BB#"+data.Instance.Name.ToUpper()+"#BE#", 67, 2, Alignment.Center, TextAdjustment.Wrap);
            rpt.Write(data.Instance.Name.ToUpper(), 67, 2, Alignment.Center, TextAdjustment.Wrap, data.IsBold);

            string adddressarea = string.Empty;

            if (!string.IsNullOrEmpty(data.Instance.Address))
            {
                adddressarea += data.Instance.Address;
            }
            if (!string.IsNullOrEmpty(data.Instance.Area))
            {
                adddressarea += "," + data.Instance.Area;
            }
            if (!string.IsNullOrEmpty(adddressarea))
            {
                rpt.NewLine();
                rpt.Write(string.Format("{0}", adddressarea.ToUpper().TrimEnd()), 69, 2, Alignment.Center, TextAdjustment.Wrap);
            }

            string cityState = string.Empty;
            if (!string.IsNullOrEmpty(data.Instance.City))
            {
                cityState += data.Instance.City + ",";
            }
            if (!string.IsNullOrEmpty(data.Instance.State))
            {
                cityState += data.Instance.State + ",";
            }
            if (!string.IsNullOrEmpty(data.Instance.Pincode))
            {
                cityState += data.Instance.Pincode;
            }

            if (!string.IsNullOrEmpty(cityState))
            {
                rpt.NewLine();
                rpt.Write(cityState.ToUpper(), 69, 2, Alignment.Center, TextAdjustment.Wrap);
            }


            string contact = string.Empty;

            if (!string.IsNullOrEmpty(data.Instance.Phone))
            {
                contact += "Ph: " + data.Instance.Phone;
            }
            if (!string.IsNullOrEmpty(data.Instance.Mobile))
            {
                contact += "," + "Mobile: " + data.Instance.Mobile;
            }
            if (!string.IsNullOrEmpty(contact))
            {
                rpt.NewLine();
                rpt.Write(contact.ToUpper(), 69, 2, Alignment.Center, TextAdjustment.Wrap);
            }

            rpt.NewLine();
            rpt.Write("DL No : " + data.Instance.DrugLicenseNo, 69, 2, Alignment.Center, TextAdjustment.Wrap);

            rpt.NewLine();
            if (data.TaxRefNo == 1)
                rpt.Write("GSTIN No: " + data.Instance.GsTinNo, 67, 2, Alignment.Center, TextAdjustment.Wrap);
            else
                rpt.Write("TIN   : " + data.Instance.TinNo, 69, 2, Alignment.Center, TextAdjustment.Wrap);
            rpt.Write();
            rpt.NewLine();
            if (gstSelection)
            {
                rpt.Write("Bill of Supply", 44, 1, Alignment.Center, TextAdjustment.Wrap);
            }
            rpt.Drawline();

            rpt.NewLine();
            rpt.Write("Doctor Name  : " + data.DoctorName.ToUpper(), 45);
            if (data.InvoiceItems.Count() == 0)
            {
                rpt.Write("   Return No : " + data.InvoiceSeries + data.InvoiceNo, 30, 50);
            }
            else
            {
                rpt.Write("     Bill No : " + data.InvoiceSeries + data.InvoiceNo, 30, 50);
            }

            // rpt.Write("Bill No: " + data.InvoiceSeries + " " + data.InvoiceNo, 18, 31);

            rpt.NewLine();
            rpt.Write("Patient Name : " + data.Name.ToUpper(), 45);
            rpt.Write("     B Date  : " + string.Format("{0:dd/MM/yyyy}", data.InvoiceDate), 30, 50);


            rpt.NewLine();
            rpt.Write("Address      : " + data.Address, 45);
            //rpt.Write("     B Type  : " + data.PaymentType, 30, 50);
            rpt.Write("     Time    : " + Convert.ToDateTime(data.SalesCreatedAt).ToString("hh:mm tt"), 30, 50);

            if (data.Address.Length > 30)
            {
                rpt.NewLine();
                rpt.Write("" + data.Address.Substring(30, data.Address.Length - 30), 46, 2);
                rpt.Write(" " + "", 30, 46);
            }

            rpt.NewLine();
            rpt.Write("City         : " + data.Patient.City, 45);
            //rpt.Write("     ID No.  : ", 30, 50);
            rpt.Write("     B Type  : " + data.PaymentType, 30, 50);


            rpt.NewLine();
            rpt.Write("Pincode      : " + data.Patient.Pincode, 45);
            rpt.Write("     ID No.  : " + Convert.ToString(data.Patient.EmpID), 30, 50);


            rpt.Write();
            rpt.Drawline();

            rpt.NewLine();
            rpt.Write("Particulars", 25, Alignment.Center);
            rpt.Write("Batch", 8, 32);
            rpt.Write("Exp", 6, 39);
            rpt.Write("Qty", 4, 47);
            rpt.Write("MRP", 6, 55);
            if (data.TaxRefNo == 1 && !gstSelection)
            {
                rpt.Write("GST%", 5, 63);
            }
            else if (!gstSelection)
            {
                rpt.Write("VAT", 5, 63);
            }
            rpt.Write("Amount", 6, 71, Alignment.Center);
            rpt.Write();
            rpt.Drawline();
        }

        protected string GenerateSalesReport_A4_New(IInvoice data)
        {
            var rpt = new PlainTextReport
            {
                Columns = 87,
                Rows = 40
            };
            WriteReportHeader_A4_New(rpt, data);
            
            if (data.InvoiceItems.Count() == 0)
            {
                WriteReportOnlyReturnedItems_A4_New(rpt, data);
            }
            else
            {
                WriteReportItems_A4_New(rpt, data);
                WriteReportFooter_A4_GST_New(rpt, data);
            }

            return rpt.Content.ToString();
        }

        private void WriteReportHeader_A4_New(PlainTextReport rpt, IInvoice data)
        {
            bool gstSelection = Convert.ToBoolean(data.Instance.Gstselect);
            rpt.NewLine();
            if (data.InvoiceItems.Count() == 0)
                rpt.Write("RETURN BILL", 67, 2, Alignment.Center, data.IsBold);
            else
                rpt.Write("RETAIL INVOICE/BILL", 67, 2, Alignment.Center, data.IsBold);

            rpt.NewLine();
            rpt.Write("PLUS PHARMACY", 67, 2, Alignment.Center, data.IsBold);

            rpt.NewLine();
            rpt.Write("(PHARMACEUTICAL DISTRIBUTORS)", 67, 2, Alignment.Center);

            rpt.NewLine();
            //rpt.Write(data.Instance.Name.ToUpper(), 67, 2, Alignment.Center, TextAdjustment.Wrap, data.IsBold);

            string adddressarea = string.Empty;

            if (!string.IsNullOrEmpty(data.Instance.Address))
            {
                adddressarea += data.Instance.Address;
            }
            if (!string.IsNullOrEmpty(data.Instance.Area))
            {
                adddressarea += "," + data.Instance.Area;
            }
            if (!string.IsNullOrEmpty(adddressarea))
            {
                rpt.NewLine();
                rpt.Write(string.Format("{0}", adddressarea.ToUpper().TrimEnd()), 69, 2, Alignment.Center, TextAdjustment.Wrap);
            }

            string cityState = string.Empty;
            if (!string.IsNullOrEmpty(data.Instance.City))
            {
                cityState += data.Instance.City + ",";
            }
            if (!string.IsNullOrEmpty(data.Instance.State))
            {
                cityState += data.Instance.State + ",";
            }
            if (!string.IsNullOrEmpty(data.Instance.Pincode))
            {
                cityState += data.Instance.Pincode;
            }

            if (!string.IsNullOrEmpty(cityState))
            {
                rpt.NewLine();
                rpt.Write(cityState.ToUpper(), 69, 2, Alignment.Center, TextAdjustment.Wrap);
            }

            string contact = string.Empty;

            if (!string.IsNullOrEmpty(data.Instance.Phone))
            {
                contact += "Ph: " + data.Instance.Phone;
            }
            if (!string.IsNullOrEmpty(data.Instance.Mobile))
            {
                contact += "," + "Mobile: " + data.Instance.Mobile;
            }
            if (!string.IsNullOrEmpty(contact))
            {
                rpt.NewLine();
                rpt.Write(contact.ToUpper(), 69, 2, Alignment.Center, TextAdjustment.Wrap);
            }

            rpt.NewLine();
            if (data.TaxRefNo == 1)
                rpt.Write("GSTIN No: " + data.Instance.GsTinNo, 30, 2, Alignment.Left);
            else
                rpt.Write("TIN   : " + data.Instance.TinNo, 30, 2, Alignment.Left);
            if (gstSelection)
            {
                rpt.Write("Bill of Supply", 44, 1, Alignment.Center, TextAdjustment.Wrap);
                rpt.NewLine();
            }


            rpt.Write("D.L.No : " + data.Instance.DrugLicenseNo, 30, 42, Alignment.Right);
            rpt.Write();
            rpt.Drawline();

            rpt.NewLine();
            if (!string.IsNullOrEmpty(data.DoctorName))
                rpt.Write("Doctor Name  : " + data.DoctorName.ToUpper(), 45);
            else
                rpt.Write("Doctor Name  : " + data.DoctorName, 45);

            rpt.NewLine();
            string patientName = string.Empty;
            patientName = data.Name;
            if (patientName != null && patientName != "")
            {
                patientName = patientName.ToUpper();
                rpt.Write("Department   : " + patientName, 45, 0, Alignment.Left);
            }

            if (data.InvoiceItems.Count() == 0)
            {
                rpt.Write("   Return No : " + data.InvoiceSeries + data.InvoiceNo, 30, 50);
            }
            else
            {
                rpt.Write("  Invoice No : " + data.InvoiceSeries + data.InvoiceNo, 30, 50);
            }
            rpt.NewLine();
            string customerName = string.Empty;
            if (!string.IsNullOrEmpty(data.Address) && data.InvoiceItems.Count() != 0)
            {                
                customerName = data.Address;
            }
            rpt.Write("Customer Name: " + customerName, 45, 0, Alignment.Left, TextAdjustment.Wrap);
            if (data.InvoiceItems.Count() == 0)
            {
                rpt.Write("Return Date : " + string.Format("{0:dd/MM/yyyy}", data.SalesReturn.ReturnDate), 30, 50);
            }
            else
            {
                rpt.Write("Invoice Date : " + string.Format("{0:dd/MM/yyyy}", data.InvoiceDate), 30, 50);
            }
            rpt.NewLine();
            string address = string.Empty;
            string patientAddress = string.Empty;
            if (data.Patient.Address != null && data.Patient.Address != "")
                patientAddress = data.Patient.Address.ToUpper();
            rpt.Write("  " , 45, 0, Alignment.Left, TextAdjustment.Wrap);
            //rpt.Write("Address: " + patientAddress, 45, 0, Alignment.Left, TextAdjustment.Wrap);

            if (!string.IsNullOrEmpty(data.Patient.City))
            {
                address += "City: " + data.Patient.City;
            }
            if (!string.IsNullOrEmpty(data.Patient.Pincode))
            {
                address += "," + "Pin: " + data.Patient.Pincode;
            }
            if (!string.IsNullOrEmpty(address))
            {
                rpt.NewLine();
                rpt.Write(address.ToUpper(), 45, 0, Alignment.Left, TextAdjustment.Wrap);
            }
            if (!gstSelection)
            {
                rpt.Write("    GSTIN No : " + data.Patient.GsTin, 30, 50);
                rpt.NewLine();
            }        
            
            rpt.Write();
            rpt.Drawline();

            rpt.NewLine();
            rpt.Write("Pack", 5, Alignment.Left);
            rpt.Write("Particulars", 21, Alignment.Left);
            rpt.Write("Batch", 8, 30);
            rpt.Write("Exp", 6, 38);
            rpt.Write("Qty", 4, 44, Alignment.Right);
            rpt.Write("MRP", 7, 52, Alignment.Right);
            rpt.Write("Cost", 6, 61, Alignment.Right);
            if (!gstSelection)
            {
                rpt.Write("GST%", 8, 67, Alignment.Right);
            }
          
            rpt.Write("Amount", 7, 79, Alignment.Right);  //DISC%
            rpt.Write();
            rpt.Drawline();
        }

        private void WriteReportItems_A4(PlainTextReport rpt, IInvoice data)
        {
            bool gstSelection = Convert.ToBoolean(data.Instance.Gstselect);
            int len = 0;
            foreach (var items in data.InvoiceItems)
            {
                if (items.Quantity <= 0)
                { break; }
                rpt.NewLine();
                rpt.Write(items.ProductStock.Product.Name.ToUpper(), 31);
                rpt.Write(items.ProductStock.BatchNo, 8, 32);
                rpt.Write(string.Format(" {0:MM/yy}", items.ProductStock.ExpireDate), 6, 39);
                rpt.Write(string.Format("{0:0}", items.Quantity), 4, 47);

                if (items.SellingPrice > 0)
                {
                    decimal? vatPrice = ((items.SellingPrice * 100) / (items.ProductStock.VAT + 100));
                    rpt.Write(string.Format("{0:0.00}", items.SellingPrice), 7, 54);
                    if (data.TaxRefNo == 1 && !gstSelection)
                    {
                        var gst = (items.Igst == 0 || items.Igst == null) ? (items.Cgst + items.Sgst) : items.Igst;
                        rpt.Write(string.Format("{0:0.00}", (gst)), 5, 63);
                    }
                    else if(!gstSelection)
                    {
                        rpt.Write(string.Format("{0:0.00}", (items.ProductStock.VAT)), 5, 63);
                    }
                    //rpt.Write(string.Format("{0:0.00}", vatPrice), 7, 54);
                    //rpt.Write(string.Format("{0:0.00}", (items.SellingPrice - vatPrice)), 5, 63);
                }
                else
                {
                    decimal? vatPrice = items.ProductStock.SellingPrice - items.ProductStock.VatInPrice;
                    rpt.Write(string.Format("{0:0.00}", items.ProductStock.SellingPrice), 7, 54);
                    rpt.Write(string.Format("{0:0.00}", (items.ProductStock.VAT)), 5, 63);
                    //rpt.Write(string.Format("{0:0.00}", items.ProductStock.VatInPrice), 7, 54);                   
                    //rpt.Write(string.Format("{0:0.00}", (vatPrice)), 5, 63);

                }

                rpt.Write(string.Format("{0:0.00}", items.Total), 8, 71, Alignment.Right);
                rpt.Write();
                len++;
            }

            for (int i = 0; i < 5 - len; i++)
                rpt.NewLine();

            rpt.Write();
            rpt.Drawline();
        }

        private void WriteReportItems_A4_New(PlainTextReport rpt, IInvoice data)
        {
            int len = 0;
            foreach (var items in data.InvoiceItems)
            {
                decimal? costPrice = 0;
                decimal? gst = 0;

                if (items.Quantity <= 0)
                { break; }
                rpt.NewLine();
                rpt.Write(string.Format("{0:0}", items.ProductStock.PackageSize), 5, Alignment.Right);
                rpt.Write(items.ProductStock.Product.Name.ToUpper(), 21);
                rpt.Write(items.ProductStock.BatchNo, 8, 30);
                rpt.Write(string.Format(" {0:MM/yy}", items.ProductStock.ExpireDate), 6, 38);
                rpt.Write(string.Format("{0:0}", items.Quantity), 4, 44, Alignment.Right);

                if (items.SellingPrice > 0)
                {                    
                    decimal? vatPrice = ((items.SellingPrice * 100) / (items.ProductStock.VAT + 100));
                    rpt.Write(string.Format("{0:0.00}", items.ProductStock.MRP), 7, 52, Alignment.Right);
                    //rpt.Write(string.Format("{0:0.00}", items.SellingPrice), 5, 63, Alignment.Right);               
                    //costPrice = (items.SellingPrice - ((items.SellingPrice * 10) / 110)) - (((items.SellingPrice - ((items.SellingPrice * 10) / 110)) * (items.GstTotal)) / (100 + items.GstTotal));
                    items.GstTotal = (items.GstTotal == null) ? 0 : items.GstTotal;
                    costPrice = (items.SellingPrice - ((items.SellingPrice * items.GstTotal) / (100 + items.GstTotal)));
                    rpt.Write(string.Format("{0:0.00}", Math.Round((decimal)costPrice, 2)), 5, 62, Alignment.Right);
                    gst = (costPrice * items.GstTotal) / 100;
                    rpt.Write(string.Format("{0:0.00}", Math.Round((decimal)items.GstTotal, 2)), 5, 70, Alignment.Right);

                }
                else
                {
                    decimal? vatPrice = items.ProductStock.SellingPrice - items.ProductStock.VatInPrice;
                    rpt.Write(string.Format("{0:0.00}", items.ProductStock.MRP), 7, 54, Alignment.Right);
                    rpt.Write(string.Format("{0:0.00}", (vatPrice)), 6, 63, Alignment.Right);
                }
                //rpt.Write(string.Format("{0:0.00}", items.Total), 8, 70, Alignment.Right);
                rpt.Write(string.Format("{0:0.00}", ((Math.Round((decimal)costPrice, 2) + Math.Round((decimal)gst, 2)) * (items.Quantity))), 8, 78, Alignment.Right);
                //rpt.Write(string.Format("{0:0.00}", items.Discount), 5, 80, Alignment.Right);
                //rpt.Write(string.Format("{0:0.00}", items.Total), 8, 79, Alignment.Right);

                items.DiscountAmount = (items.Discount / 100) * items.Total;

                rpt.Write();
                len++;
            }

            for (int i = 0; i < 5 - len; i++)
                rpt.NewLine();

            rpt.Write();
            rpt.Drawline();
        }

        private void WriteReportFooter_A4_GST_New(PlainTextReport rpt, IInvoice data)
        {
            rpt.NewLine();
            WriteReportReturnedItems_A4_New(rpt, data);

            rpt.Write();
            rpt.NewLine();
            //Newly added for Print Footer by Mani on 30-02-2017
            GenerateFooter(rpt, 75, data.PrintFooterNote);
        }

        private void WriteReportFooter_A4(PlainTextReport rpt, IInvoice data)
        {
            rpt.NewLine();
            bool gstSelection = Convert.ToBoolean(data.Instance.Gstselect);
            if (gstSelection)
            {
                rpt.Write("Composite tax payer not eligible to collect taxes", 65, 1, Alignment.Left);
                rpt.NewLine();
            }
            rpt.Write("Total: " + string.Format("{0:0.00}", data.PurchasedTotal), 20);
            rpt.Write("Disc.: " + string.Format("{0:0.00}", data.DiscountInValue), 30, 27);
            if (data.SalesReturnItem.Count == 0)
            {
                rpt.Write("Round Off: " + string.Format("{0:0.00}", data.RoundoffNetAmount), 30, 42);
                rpt.Write("Net Amount: " + string.Format("{0:0.00}", data.NetAmount), 20, 59, Alignment.Right, data.IsBold);
            }
            else
                rpt.Write("Net Amount: " + string.Format("{0:0.00}", data.NetAmount), 20, 59, Alignment.Right, data.IsBold);
            //rpt.Write("Net Amount: " + string.Format("{0:0.00}", data.Net + data.RoundoffNetAmount), 20, 59, Alignment.Right, data.isBold);

            rpt.Write();
            rpt.Drawline(1);
            if (data.SalesReturnItem.Count > 0)
            {
                WriteReportReturnedItems_A4(rpt, data);
            }
            rpt.NewLine();
            //Newly added for Print Footer by Mani on 30-02-2017
            GenerateFooter(rpt, 75, data.PrintFooterNote);
        }

        private void WriteReportFooter_A4_GST(PlainTextReport rpt, IInvoice data)
        {
            bool gstSelection = Convert.ToBoolean(data.Instance.Gstselect);
            if (data.SalesReturnItem.Count == 0)
            {
                rpt.NewLine();
                if (!gstSelection)
                {
                    rpt.Write("CGST:" + string.Format("{0:0.00}", Math.Round((decimal)data.CgstTotalValue, 2)), 20);
                    rpt.Write((data.Instance.isUnionTerritory == true ? "UTGST:" : "SGST:") + string.Format("{0:0.00}", Math.Round((decimal)data.SgstTotalValue, 2)), 20, 27);
                }
                rpt.Write("Total: " + string.Format("{0:0.00}", data.PurchasedTotal), 20, 59, Alignment.Right);
                rpt.NewLine();
                rpt.Write("Disc.: " + string.Format("{0:0.00}", data.DiscountInValue), 12, 67, Alignment.Right);
                rpt.NewLine();
                string payAmountInWords = ((decimal)data.NetAmount).ConvertNumbertoWords();
                rpt.Write("Rupees: " + payAmountInWords, 40, 1, TextAdjustment.Wrap);
                rpt.Write("Round Off: " + string.Format("{0:0.00}", data.RoundoffNetAmount), 20, 59, Alignment.Right);
                rpt.NewLine();
                rpt.Write("Net Amount: " + string.Format("{0:0.00}", data.NetAmount), 20, 54, Alignment.Right, data.IsBold);
                if (gstSelection)
                {
                    rpt.Write();
                    rpt.Drawline(1);
                    GenerateGstSelected(rpt);
                }
            }
            else
            {                
                rpt.NewLine();
                WriteReportReturnedItems_A4(rpt, data);
                if (gstSelection)
                {
                    GenerateGstSelected(rpt);
                }
            }

            rpt.Write();
            rpt.NewLine();
            //Newly added for Print Footer by Mani on 30-02-2017
            GenerateFooter(rpt, 75, data.PrintFooterNote);
        }

        private void WriteReportReturnedItems_A4(PlainTextReport rpt, IInvoice data)
        {
            bool gstSelection = Convert.ToBoolean(data.Instance.Gstselect);
            rpt.Write("RETURNS", 8, 0, Alignment.Center, TextAdjustment.Wrap);

            decimal? totReturnAmount = 0;
            decimal? totReturnDiscount = 0;
            decimal? ReturnDiscount = 0;
            decimal? returnAmt = 0;
            int len = 0;
            decimal? rtnGSTTotalValue = 0;

            foreach (var items in data.SalesReturnItem)
            {
                if (items.ReturnedQuantity > 0)
                {
                    rpt.NewLine();
                    len++;

                    rpt.Write(items.ProductStock.Product.Name.ToUpper(), 31);
                    rpt.Write(items.ProductStock.BatchNo, 8, 32);
                    rpt.Write(string.Format(" {0:MM/yy}", items.ProductStock.ExpireDate), 6, 39);
                    rpt.Write(string.Format("{0:0}", items.ReturnedQuantity), 4, 47);

                    decimal? SellingPrice = 0;
                    if (items.SellingPrice > 0)
                    {
                        SellingPrice = items.SellingPrice;
                        rpt.Write(string.Format("{0:0.00}", SellingPrice), 7, 54);
                        if (data.TaxRefNo == 1 && !gstSelection)
                            rpt.Write(string.Format("{0:0.00}", (items.GstTotal)), 5, 63);
                        else if(!gstSelection)
                            rpt.Write(string.Format("{0:0.00}", (items.ProductStock.VAT)), 5, 63);
                    }
                    else
                    {
                        SellingPrice = items.ProductStock.SellingPrice;
                        rpt.Write(string.Format("{0:0.00}", SellingPrice), 7, 54);
                        if (data.TaxRefNo == 1)
                            rpt.Write(string.Format("{0:0.00}", (items.ProductStock.GstTotal)), 5, 63);
                        else
                            rpt.Write(string.Format("{0:0.00}", (items.ProductStock.VAT)), 5, 63);
                    }
                    decimal? getGstPercent = items.GstTotal + 100;
                    decimal? SalesItemDiscount = ((items.Discount / 100) * items.Total);
                    var ActualValue = (items.Total - SalesItemDiscount);
                    rtnGSTTotalValue += ActualValue - ((ActualValue * 100) / getGstPercent);
                    rpt.Write(string.Format("{0:0.00}", SellingPrice * (items.ReturnedQuantity)), 8, 71, Alignment.Right);
                    returnAmt += (SellingPrice * items.ReturnedQuantity);
                    totReturnAmount = totReturnAmount + SellingPrice * (items.ReturnedQuantity);
                    totReturnDiscount = ((items.Discount / 100) * items.Total);
                    totReturnAmount = totReturnAmount - totReturnDiscount;
                    ReturnDiscount += totReturnDiscount; // Need to Plus totReturnDiscount
                }
            }
            rpt.NewLine();
            rpt.NewLine();

            rpt.Write();
            rpt.Drawline(1);

            rpt.NewLine();
            decimal RoffAmount = data.RoundoffNetAmount as decimal? ?? 0; //RoundOffCalculation(data.Net, totReturnAmount);
            if (data.TaxRefNo == 1)
            {
                decimal? totValue = data.InvoiceItems.Sum(x => x.Total);
                decimal? totCgstValue = data.InvoiceItems.Sum(x => x.CgstValue);
                decimal? totSgstValue = data.InvoiceItems.Sum(x => x.SgstValue);
                decimal? RtntotgstValue = rtnGSTTotalValue / 2;
                if (!gstSelection) {
                rpt.Write("CGST:" + string.Format("{0:0.00}", (totCgstValue - RtntotgstValue)), 12, 1);
                rpt.Write((data.Instance.isUnionTerritory == true ? "UTGST:" : "SGST:") + string.Format("{0:0.00}", (totSgstValue - RtntotgstValue)), 12, 13);
                }
                rpt.Write("TOTAL:" + string.Format("{0:0.00}", (totValue - returnAmt)), 15, 63, Alignment.Right);
                rpt.NewLine();
                //string payAmountInWords = ((long)(Math.Round(((decimal)data.Net - (decimal)totReturnAmount) + RoffAmount))).ConvertNumbertoWords();
                string payAmountInWords = ((decimal)(data.NetAmount)).ConvertNumbertoWords();
                rpt.Write("Rupees " + string.Format("{0:0.00}", payAmountInWords), 40, 1,TextAdjustment.Wrap);
                rpt.Write("R.Off: " + string.Format("{0:0.00}", RoffAmount), 15, 63, Alignment.Right);
                rpt.NewLine();
                rpt.Write("Disc.: " + string.Format("{0:0.00}", (data.DiscountInValue - ReturnDiscount)), 15, 63, Alignment.Right);
                rpt.NewLine();
                rpt.Write("RETURN AMT: " + string.Format("{0:0.00}", totReturnAmount), 20, 58, Alignment.Right, data.IsBold);
                rpt.Write();
                rpt.Drawline(1);
                //rpt.Write("PAYABLE AMOUNT: " + string.Format("{0:0.00}", Math.Round(((decimal)data.Net - (decimal)totReturnAmount) + RoffAmount)), 28, 50, Alignment.Right, data.IsBold);
                rpt.Write("PAYABLE AMOUNT: " + string.Format("{0:0.00}", data.NetAmount), 28, 50, Alignment.Right, data.IsBold);
            }
            else
            {
                rpt.Write("R.Off: " + string.Format("{0:0.00}", RoffAmount), 15, 1, Alignment.Left);
                if (ReturnDiscount != 0)
                    rpt.Write("Disc.: -" + string.Format("{0:0.00}", ReturnDiscount), 15, 17);
                else
                    rpt.Write("Disc.: " + string.Format("{0:0.00}", ReturnDiscount), 15, 17);

                rpt.Write("RETURN AMT: " + string.Format("{0:0.00}", totReturnAmount), 20, 59, Alignment.Right, data.IsBold);
                rpt.NewLine();
                //rpt.Write("PAYABLE AMOUNT: " + string.Format("{0:0.00}", Math.Round(((decimal)data.Net - (decimal)totReturnAmount) + RoffAmount)), 28, 51, Alignment.Right, data.IsBold);
                rpt.Write("PAYABLE AMOUNT: " + string.Format("{0:0.00}", data.NetAmount), 28, 51, Alignment.Right, data.IsBold);
            }
            rpt.Write();
            rpt.Drawline(1);
        }

        private void WriteReportReturnedItems_A4_New(PlainTextReport rpt, IInvoice data)
        {
            bool gstSelection = Convert.ToBoolean(data.Instance.Gstselect);
            decimal? totReturnAmount = 0;
            decimal? costPrice = 0;
            decimal? gst = 0;

            decimal? rtnGst = 0;
            decimal? rtndGstSum = 0;
            decimal? rtngrossTotalSum = 0;
            decimal? rtngrossDiscSum = 0;
            decimal? rtntaxableSum = 0;
            decimal? rtngstAmountSum = 0;
            decimal? rtnNetSum = 0;
            if (!gstSelection)
            {
                rpt.Write("GST%", 7, 0, Alignment.Right);
            }
            
            rpt.Write("Gross Amt", 12, 12, Alignment.Right);
            rpt.Write("DISC", 7, 28, Alignment.Right);
           
            if (!gstSelection)
            {
                rpt.Write("TAXABLE", 8, 38, Alignment.Right);
                rpt.Write("CGST Amt", 8, 48, Alignment.Right);
                rpt.Write((data.Instance.isUnionTerritory == true ? "UTGST" : "SGST") + " Amt", 9, 58, Alignment.Right);
            }
          
            rpt.Write("Net Amt", 12, 67, Alignment.Right);
            rpt.Write();
            rpt.Drawline(1);
            if (gstSelection)
            {
                rpt.NewLine();
                rpt.Write("Composite tax payer not eligible to collect taxes", 65, 1, Alignment.Left);
            }
            rpt.NewLine();

            var lst = data.InvoiceItems.GroupBy(x => x.GstTotal).ToList();
            int i = 0;
            decimal dGst = 0;

            decimal? dGstSum = 0;
            decimal? grossTotalSum = 0;
            decimal? grossDiscSum = 0;
            decimal? taxableSum = 0;
            decimal? gstAmountSum = 0;
            decimal? NetSum = 0;

            for (i = 0; i < lst.Count(); i++)
            {
                dGst = Convert.ToDecimal(lst[i].Key);
                if (dGst >= 0)
                {
                    dGstSum = dGstSum + dGst;
                    var item = (data.InvoiceItems.Where(x => x.GstTotal == dGst));
                    if (!gstSelection)
                    {
                        rpt.Write(string.Format("{0:0.00}", dGst), 7, 0, Alignment.Right);
                    }
                    

                    decimal? grossTotal = item.Sum(x => x.Total);
                    if (!gstSelection)
                        rpt.Write(string.Format("{0:0.00}", Math.Round((decimal)grossTotal, 2)), 12, 12, Alignment.Right);
                    grossTotalSum = grossTotalSum + grossTotal;

                    decimal? grossDisc = item.Sum(x => x.DiscountAmount);
                    if (!gstSelection)
                        rpt.Write(string.Format("{0:0.00}", Math.Round((decimal)grossDisc, 2)), 7, 28, Alignment.Right);
                    grossDiscSum = grossDiscSum + grossDisc;

                    decimal? taxable = (grossTotal - grossDisc);
                    decimal? gstAmount = ((taxable) * dGst) / (100 + dGst);
                    taxable = taxable - gstAmount;
                    taxableSum = taxableSum + taxable;
                    if (!gstSelection)
                    {
                        rpt.Write(string.Format("{0:0.00}", (Math.Round((decimal)taxable, 2))), 8, 38, Alignment.Right);
                        rpt.Write(string.Format("{0:0.00}", (Math.Round((decimal)(gstAmount / 2), 2)).ToString()), 8, 48, Alignment.Right);
                        rpt.Write(string.Format("{0:0.00}", (Math.Round((decimal)(gstAmount / 2), 2)).ToString()), 8, 58, Alignment.Right);

                    }

                    gstAmountSum = gstAmountSum + gstAmount;

                    var Net = (grossTotal - grossDisc);
                    NetSum = NetSum + Net;
                    rpt.Write(string.Format("{0:0.00}", (Math.Round((decimal)Net, 2))), 12, 67, Alignment.Right);
                    rpt.NewLine();
                }
            }

            if ((data.SalesReturnItem.Count > 0) && true)
            {
                rpt.Write();
                rpt.Drawline(1);
                //
                
                //
                rpt.Write("RETURNS", 8, 0, Alignment.Center, TextAdjustment.Wrap);
                rpt.NewLine();
                int len = 0;
                foreach (var items in data.SalesReturnItem)
                {
                    if (items.ReturnedQuantity > 0)
                    {
                        rpt.NewLine();
                        len++;

                        rpt.Write(items.ProductStock.Product.Name.ToUpper(), 31);
                        rpt.Write(items.ProductStock.BatchNo, 8, 30);
                        rpt.Write(string.Format(" {0:MM/yy}", items.ProductStock.ExpireDate), 6, 38);
                        rpt.Write(string.Format("{0:0}", items.ReturnedQuantity), 4, 44, Alignment.Right);

                        decimal? SellingPrice = 0;
                        if (items.SellingPrice > 0)
                        {
                            SellingPrice = items.SellingPrice;
                            rpt.Write(string.Format("{0:0.00}", SellingPrice), 7, 52, Alignment.Right);
                            if(!gstSelection)
                            rpt.Write(string.Format("{0:0.00}", (items.GstTotal)), 5, 63, Alignment.Right);

                            costPrice = (items.SellingPrice - ((items.SellingPrice * items.GstTotal) / (100 + items.GstTotal)));
                            rpt.Write(string.Format("{0:0.00}", Math.Round((decimal)costPrice, 2)), 5, 62, Alignment.Right);
                            gst = (costPrice * items.GstTotal) / 100;
                            if (!gstSelection)
                                rpt.Write(string.Format("{0:0.00}", Math.Round((decimal)items.GstTotal, 2)), 5, 70, Alignment.Right);                            
                        }
                        else
                        {
                            SellingPrice = items.ProductStock.SellingPrice;
                            rpt.Write(string.Format("{0:0.00}", SellingPrice), 7, 54, Alignment.Right);
                            if (!gstSelection)
                                rpt.Write(string.Format("{0:0.00}", (items.ProductStock.GstTotal)), 5, 63, Alignment.Right);
                        }
                        items.Total = SellingPrice * (items.ReturnedQuantity);
                        rpt.Write(string.Format("{0:0.00}", ((Math.Round((decimal)costPrice, 2) + Math.Round((decimal)gst, 2)) * (items.Quantity))), 8, 78, Alignment.Right);

                        decimal? SalesItemDiscount = ((items.Discount / 100) * items.Total);
                        var ActualValue = (items.Total - SalesItemDiscount);
                        totReturnAmount += ActualValue;

                        items.DiscountAmount = (items.Discount / 100) * (SellingPrice * (items.ReturnedQuantity));
                    }
                }
                rpt.NewLine();
                rpt.NewLine();
                rpt.NewLine();

                rpt.Write();
                rpt.Drawline(1);

                rpt.NewLine();
                if(!gstSelection)
                rpt.Write("GST%", 7, 0, Alignment.Right);
                rpt.Write("Gross Amt", 12, 12, Alignment.Right);
                rpt.Write("DISC", 7, 28, Alignment.Right);
                if (!gstSelection)
                {
                    rpt.Write("TAXABLE", 8, 38, Alignment.Right);
                    rpt.Write("CGST Amt", 8, 48, Alignment.Right);
                    rpt.Write((data.Instance.isUnionTerritory == true ? "UTGST" : "SGST") + " Amt", 9, 58, Alignment.Right);
                }
                   
                rpt.Write("Net Amt", 12, 67, Alignment.Right);
                rpt.Write();
                rpt.Drawline(1);

                var rtnLst = data.SalesReturnItem.GroupBy(x => x.GstTotal).ToList();

                int j = 0;
                rtnGst = 0;
                rtndGstSum = 0;
                rtngrossTotalSum = 0;
                rtngrossDiscSum = 0;
                rtntaxableSum = 0;
                rtngstAmountSum = 0;
                rtnNetSum = 0;

                for (j = 0; j < rtnLst.Count(); j++)
                {
                    rtnGst = Convert.ToDecimal(rtnLst[j].Key == null ? 0 : rtnLst[j].Key);
                    if (rtnGst >= 0)
                    {
                        rtndGstSum = rtndGstSum + rtnGst;
                        var item = (data.SalesReturnItem.Where(x => x.GstTotal == rtnGst));
                        if(!gstSelection)
                        rpt.Write(string.Format("{0:0.00}", rtnGst), 7, 0, Alignment.Right);

                        decimal? grossTotal = item.Sum(x => x.Total);
                        rpt.Write(string.Format("{0:0.00}", Math.Round((decimal)grossTotal, 2)), 12, 12, Alignment.Right);
                        rtngrossTotalSum = rtngrossTotalSum + grossTotal;

                        decimal? grossDisc = item.Sum(x => x.DiscountAmount);
                        rpt.Write(string.Format("{0:0.00}", Math.Round((decimal)grossDisc, 2)), 7, 28, Alignment.Right);
                        rtngrossDiscSum = rtngrossDiscSum + grossDisc;

                        decimal? taxable = (grossTotal - grossDisc);
                        decimal? gstAmount = ((taxable) * rtnGst) / (100 + rtnGst);
                        taxable = taxable - gstAmount;
                        rtntaxableSum = rtntaxableSum + taxable;
                        if (!gstSelection)
                        {
                            rpt.Write(string.Format("{0:0.00}", (Math.Round((decimal)taxable, 2))), 8, 38, Alignment.Right);
                            rpt.Write(string.Format("{0:0.00}", (Math.Round((decimal)(gstAmount / 2), 2)).ToString()), 8, 48, Alignment.Right);
                            rpt.Write(string.Format("{0:0.00}", (Math.Round((decimal)(gstAmount / 2), 2)).ToString()), 8, 58, Alignment.Right);

                        }

                        rtngstAmountSum = rtngstAmountSum + gstAmount;

                        var Net = (grossTotal - grossDisc);
                        rtnNetSum = rtnNetSum + Net;
                        rpt.Write(string.Format("{0:0.00}", (Math.Round((decimal)Net, 2))), 12, 67, Alignment.Right);
                        rpt.NewLine();
                    }
                }
            }
            //
            rpt.Write();
            rpt.Drawline(1);

            rpt.Write("TOTAL:", 7, 0, Alignment.Left);
            rpt.Write(string.Format("{0:0.00}", Math.Round((decimal)(grossTotalSum - rtngrossTotalSum), 2)), 12, 12, Alignment.Right);
            rpt.Write(string.Format("{0:0.00}", Math.Round((decimal)(grossDiscSum - rtngrossDiscSum), 2)), 7, 28, Alignment.Right);
            if (!gstSelection)
            {
                rpt.Write(string.Format("{0:0.00}", (Math.Round((decimal)(taxableSum - rtntaxableSum), 2))), 8, 38, Alignment.Right);

                rpt.Write(string.Format("{0:0.00}", (Math.Round((decimal)((gstAmountSum - rtngstAmountSum) / 2), 2)).ToString()), 8, 48, Alignment.Right);
                rpt.Write(string.Format("{0:0.00}", (Math.Round((decimal)((gstAmountSum - rtngstAmountSum) / 2), 2)).ToString()), 8, 58, Alignment.Right);
               
            }
            rpt.Write(string.Format("{0:0.00}", (Math.Round((decimal)(NetSum - rtnNetSum), 2))), 12, 67, Alignment.Right);

            rpt.NewLine();
            rpt.Drawline(1);
            decimal? dueAmount = (NetSum - totReturnAmount);
            decimal? RoffAmt = (Math.Round((decimal)dueAmount, 2) - Math.Round((decimal)dueAmount, 0, MidpointRounding.AwayFromZero));
            decimal? RoffAmount = (RoffAmt != (decimal)0.50) ? (-1) * RoffAmt : RoffAmt;

            if (NetSum > 0 && totReturnAmount > 0)
            {
                rpt.Write("Sale Amt: " + string.Format("{0:0.00}", NetSum), 20, 0, Alignment.Left);
                rpt.Write("Ret Amt: " + string.Format("{0:0.00}", totReturnAmount), 17, 21, Alignment.Left);
            }

            if (NetSum > 0 && totReturnAmount == 0)
                rpt.Write("Sale Amt: " + string.Format("{0:0.00}", NetSum), 20, 0, Alignment.Left);

            if (totReturnAmount > 0 && NetSum == 0)
                rpt.Write("Ret Amt: " + string.Format("{0:0.00}", totReturnAmount), 17, 20, Alignment.Left);

            //if (NetSum > 0 && totReturnAmount > 0)
            //{
            //    rpt.Write();
            //    rpt.Drawline(1);
            //}

            rpt.Write("R.Off: " + string.Format("{0:0.00}", RoffAmount), 14, 40, Alignment.Center);
            rpt.Write("Due Amt:" + string.Format("{0:0.00}", Math.Round(Math.Round((decimal)dueAmount, 2, MidpointRounding.AwayFromZero), 0, MidpointRounding.AwayFromZero)), 23, 60, Alignment.Right);
        }

        private void WriteReportOnlyReturnedItems_A4_New(PlainTextReport rpt, IInvoice data)
        {
            bool gstSelection = Convert.ToBoolean(data.Instance.Gstselect);
            if (data.SalesReturnItem.Count > 0)
            {                
                int len = 0;
                foreach (var items in data.SalesReturnItem)
                {
                    if (items.ReturnedQuantity > 0)
                    {
                        decimal? costPrice = 0;
                        decimal? gst = 0;

                        if (items.Quantity <= 0)
                        { break; }
                        rpt.NewLine();
                        rpt.Write(string.Format("{0:0}", items.ProductStock.PackageSize), 5, Alignment.Right);
                        rpt.Write(items.ProductStock.Product.Name.ToUpper(), 21);
                        rpt.Write(items.ProductStock.BatchNo, 8, 30);
                        rpt.Write(string.Format(" {0:MM/yy}", items.ProductStock.ExpireDate), 6, 38);
                        rpt.Write(string.Format("{0:0}", items.Quantity), 4, 44, Alignment.Right);
                        if (!gstSelection)
                        {
                            if (items.SellingPrice > 0)
                            {
                                decimal? vatPrice = ((items.SellingPrice * 100) / (items.ProductStock.VAT + 100));
                                rpt.Write(string.Format("{0:0.00}", items.ProductStock.MRP), 7, 52, Alignment.Right);
                                items.GstTotal = (items.GstTotal == null) ? 0 : items.GstTotal;
                                costPrice = (items.SellingPrice - ((items.SellingPrice * items.GstTotal) / (100 + items.GstTotal)));
                                rpt.Write(string.Format("{0:0.00}", Math.Round((decimal)costPrice, 2)), 5, 62, Alignment.Right);
                                gst = (costPrice * items.GstTotal) / 100;
                                rpt.Write(string.Format("{0:0.00}", Math.Round((decimal)items.GstTotal, 2)), 5, 70, Alignment.Right);

                            }
                            else
                            {
                                decimal? vatPrice = items.ProductStock.SellingPrice - items.ProductStock.VatInPrice;
                                rpt.Write(string.Format("{0:0.00}", items.ProductStock.MRP), 7, 54, Alignment.Right);
                                rpt.Write(string.Format("{0:0.00}", (vatPrice)), 6, 63, Alignment.Right);
                            }
                        }
                        rpt.Write(string.Format("{0:0.00}", ((Math.Round((decimal)costPrice, 2) + Math.Round((decimal)gst, 2)) * (items.Quantity))), 8, 78, Alignment.Right);
                        items.DiscountAmount = (items.Discount / 100) * items.Total;

                        rpt.Write();
                        len++;
                    }
                }
                rpt.NewLine();
                rpt.NewLine();
                
                rpt.Write();
                rpt.Drawline(1);
            }
            if (gstSelection)
            {
                rpt.NewLine();
                rpt.Write("Composite tax payer not eligible to collect taxes", 65, 1, Alignment.Left);
            }
            rpt.NewLine();
            if (!gstSelection)
            {
                rpt.Write("GST%", 7, 0, Alignment.Right);
            }
           
            rpt.Write("Gross Amt", 12, 12, Alignment.Right);
            rpt.Write("DISC", 7, 28, Alignment.Right);
            rpt.Write("TAXABLE", 8, 38, Alignment.Right);
            if (!gstSelection)
            {
                rpt.Write("CGST Amt", 8, 48, Alignment.Right);
                rpt.Write((data.Instance.isUnionTerritory == true ? "UTGST" : "SGST") + " Amt", 9, 58, Alignment.Right);
             
            }
            rpt.Write("Net Amt", 12, 67, Alignment.Right);
            rpt.Write();
            rpt.Drawline(1);
            rpt.NewLine();

            var lst = data.SalesReturnItem.GroupBy(x => x.GstTotal).ToList();  
            int i = 0;
            decimal dGst = 0;

            decimal? dGstSum = 0;
            decimal? grossTotalSum = 0;
            decimal? grossDiscSum = 0;
            decimal? taxableSum = 0;
            decimal? gstAmountSum = 0;
            decimal? NetSum = 0;

            for (i = 0; i < lst.Count(); i++)
            {
                dGst = Convert.ToDecimal(lst[i].Key);
                if (dGst >= 0)
                {
                    dGstSum = dGstSum + dGst;
                    var item = (data.SalesReturnItem.Where(x => x.GstTotal == dGst));
                    if (!gstSelection)
                    {
                       
                        rpt.Write(string.Format("{0:0.00}", dGst), 7, 0, Alignment.Right);
                    }
                 

                    decimal? grossTotal = item.Sum(x => x.Total);
                    rpt.Write(string.Format("{0:0.00}", Math.Round((decimal)grossTotal, 2)), 12, 12, Alignment.Right);
                    grossTotalSum = grossTotalSum + grossTotal;

                    decimal? grossDisc = item.Sum(x => x.DiscountAmount);
                    rpt.Write(string.Format("{0:0.00}", Math.Round((decimal)grossDisc, 2)), 7, 28, Alignment.Right);
                    grossDiscSum = grossDiscSum + grossDisc;

                    decimal? taxable = (grossTotal - grossDisc);
                    decimal? gstAmount = ((taxable) * dGst) / (100 + dGst);
                    taxable = taxable - gstAmount;
                    taxableSum = taxableSum + taxable;
                    if (!gstSelection)
                    {

                        rpt.Write(string.Format("{0:0.00}", (Math.Round((decimal)taxable, 2))), 8, 38, Alignment.Right);
                        rpt.Write(string.Format("{0:0.00}", (Math.Round((decimal)(gstAmount / 2), 2)).ToString()), 8, 48, Alignment.Right);
                        rpt.Write(string.Format("{0:0.00}", (Math.Round((decimal)(gstAmount / 2), 2)).ToString()), 8, 58, Alignment.Right);
                    }

                    gstAmountSum = gstAmountSum + gstAmount;

                    var Net = (grossTotal - grossDisc);
                    NetSum = NetSum + Net;
                    rpt.Write(string.Format("{0:0.00}", (Math.Round((decimal)Net, 2))), 12, 67, Alignment.Right);
                    rpt.NewLine();
                }
            }

            rpt.Write();
            rpt.Drawline(1);
            rpt.NewLine();

            rpt.Write("TOTAL:", 7, 0, Alignment.Left);
            rpt.Write(string.Format("{0:0.00}", Math.Round((decimal)grossTotalSum, 2)), 12, 12, Alignment.Right);
            rpt.Write(string.Format("{0:0.00}", Math.Round((decimal)grossDiscSum, 2)), 7, 28, Alignment.Right);
            if (!gstSelection)
            {
                rpt.Write(string.Format("{0:0.00}", (Math.Round((decimal)taxableSum, 2))), 8, 38, Alignment.Right);
                rpt.Write(string.Format("{0:0.00}", (Math.Round((decimal)(gstAmountSum / 2), 2)).ToString()), 8, 48, Alignment.Right);
                rpt.Write(string.Format("{0:0.00}", (Math.Round((decimal)(gstAmountSum / 2), 2)).ToString()), 8, 58, Alignment.Right);
            }
            
            rpt.Write(string.Format("{0:0.00}", (Math.Round((decimal)NetSum, 2))), 12, 67, Alignment.Right);

            rpt.NewLine();
            rpt.Drawline(1);
            decimal? dueAmount = (NetSum);  // - totReturnAmount
            decimal? RoffAmt = (Math.Round((decimal)dueAmount, 2) - Math.Round((decimal)dueAmount, 0, MidpointRounding.AwayFromZero));
            decimal? RoffAmount = (RoffAmt != (decimal)0.50) ? (-1) * RoffAmt : RoffAmt;
            //rpt.Write("Return Amt.:" + string.Format("{0:0.00}", (Math.Round((decimal)totReturnAmount, 2))), 23, 0, Alignment.Left);
            rpt.Write("R.Off: " + string.Format("{0:0.00}", RoffAmount), 20, 25, Alignment.Center);
            decimal? dueAmt = Math.Round(Math.Round((decimal)dueAmount, 2, MidpointRounding.AwayFromZero), 0, MidpointRounding.AwayFromZero);
            //rpt.Write("Due Amt:" + string.Format("{0:0.00}", dueAmt), 23, 30); //60, Alignment.Right
            rpt.Write("Ret Amt: " + string.Format("{0:0.00}", dueAmt).ToString(), 23, 56, Alignment.Right); //60
            rpt.Write();
            rpt.NewLine();
        }
        #endregion

        #region 4InchRoll_Generic _template

        protected string GenerateSalesReport_4InchRoll(IInvoice data)
        {
            bool gstSelection = Convert.ToBoolean(data.Instance.Gstselect);
            var rpt = new PlainTextReport
            {
                Columns = 50,
                Rows = 30,

            };


            //some leading blank lines
            rpt.NewLine();
            rpt.NewLine();

            WriteReportHeader_4InchRoll(rpt, data);

            if (data.InvoiceItems.Count() == 0)
            {
                if (data.TaxRefNo == 1)
                    WriteReportOnlyReturnItems_4InchRoll_GST(rpt, data);
                else
                {
                    WriteReportOnlyReturnItems_4InchRoll(rpt, data);
                }
            }
            else
            {
                WriteReportItems_4InchRoll(rpt, data);
                if (data.TaxRefNo == 1)
                    WriteReportFooter_4InchRoll_GST(rpt, data);
                else
                {
                    if (data.SalesReturnItem.Count == 0)
                        WriteReportFooter_4InchRoll(rpt, data);
                    else
                        WriteReportReturnItems_4InchRoll(rpt, data);
                }
            }

            //some trailing blank lines
            rpt.NewLine();
            rpt.NewLine();
            rpt.Write();

            return rpt.Content.ToString();
        }

        private void WriteReportHeader_4InchRoll(PlainTextReport rpt, IInvoice data)
        {
            rpt.NewLine();
            //rpt.Write("#BB#"+data.Instance.Name.ToUpper()+ "#BE#", 50, 1, Alignment.Center);
            rpt.Write(data.Instance.Name.ToUpper(), 48, 1, Alignment.Center, data.IsBold);
            bool gstSelection = Convert.ToBoolean(data.Instance.Gstselect);
            string adddressarea = string.Empty;
            if (!string.IsNullOrEmpty(data.Instance.Address))
            {
                adddressarea += data.Instance.Address;
            }
            if (!string.IsNullOrEmpty(data.Instance.Area))
            {
                adddressarea += "," + data.Instance.Area;
            }
            if (!string.IsNullOrEmpty(adddressarea))
            {
                rpt.NewLine();
                rpt.Write(adddressarea.ToUpper(), 48, 1, Alignment.Center, TextAdjustment.Wrap);
            }
            string cityState = string.Empty;
            if (!string.IsNullOrEmpty(data.Instance.City))
            {
                cityState += data.Instance.City + ",";
            }
            if (!string.IsNullOrEmpty(data.Instance.State))
            {
                cityState += data.Instance.State + ",";
            }
            if (!string.IsNullOrEmpty(data.Instance.Pincode))
            {
                cityState += data.Instance.Pincode;
            }

            if (!string.IsNullOrEmpty(cityState))
            {
                rpt.NewLine();
                rpt.Write(cityState.ToUpper(), 48, 1, Alignment.Center, TextAdjustment.Wrap);
            }
            string contact = string.Empty;

            if (!string.IsNullOrEmpty(data.Instance.Phone))
            {
                contact += "Ph: " + data.Instance.Phone;
            }
            if (!string.IsNullOrEmpty(data.Instance.Mobile))
            {
                contact += "," + "Mobile: " + data.Instance.Mobile;
            }
            if (!string.IsNullOrEmpty(contact))
            {
                rpt.NewLine();
                rpt.Write(contact.ToUpper(), 48, 1, Alignment.Center, TextAdjustment.Wrap);
            }
            rpt.NewLine();
            rpt.Write("DL :" + data.Instance.DrugLicenseNo.ToUpper(), 48, 1, Alignment.Center, TextAdjustment.Wrap);

            rpt.NewLine();
            if (data.TaxRefNo == 1)
                rpt.Write("GSTIN No:" + data.Instance.GsTinNo, 44, 1, Alignment.Center, TextAdjustment.Wrap);
            else if (!gstSelection)
                rpt.Write("TIN:" + data.Instance.TinNo.ToUpper(), 48, 1, Alignment.Center, TextAdjustment.Wrap);

            if (gstSelection)
            {
                rpt.NewLine();
                rpt.Write("Bill of Supply", 44, 1, Alignment.Center, TextAdjustment.Wrap);
            }


            rpt.Write();

            rpt.Drawline(1);

            rpt.NewLine();
            rpt.Write("Doctor  : " + data.DoctorName.ToUpper(), 30, 1);
            if (data.InvoiceItems.Count() == 0)
            {
                rpt.Write("Return No : " + data.InvoiceSeries + data.InvoiceNo, 18, 31);
            }
            else
            {
                rpt.Write("Bill No: " + data.InvoiceSeries + data.InvoiceNo, 18, 31);
            }

            //rpt.Write("Bill No: "  + data.InvoiceNo, 18, 31);

            rpt.NewLine();
            rpt.Write("Patient : " + data.Name.ToUpper(), 30, 1);
            rpt.Write("B Date : " + string.Format("{0:dd/MM/yy}", data.InvoiceDate), 18, 31);



            rpt.NewLine();
            rpt.Write("Address : " + data.Address, 30, 1);
            //rpt.Write("B Type : " + data.PaymentType, 18, 31);
            rpt.Write("Time   : " + Convert.ToDateTime(data.SalesCreatedAt).ToString("hh:mm tt"), 18, 31);

            if (data.Address.Length > 20)
            {
                rpt.NewLine();
                rpt.Write("" + data.Address.Substring(20, data.Address.Length - 20), 30, 1);
                rpt.Write(" " + "", 18, 31);
            }
            rpt.NewLine();
            rpt.Write("City    : " + data.Patient.City, 30, 1);
            rpt.Write("B Type : " + data.PaymentType, 18, 31);
            //rpt.Write("ID No. : ", 18, 31);
            rpt.NewLine();
            rpt.Write("Pincode : " + data.Patient.Pincode, 30, 1, TextAdjustment.NewLine);
            rpt.Write("ID No. : " + Convert.ToString(data.Patient.EmpID), 18, 31);
            rpt.Write();
            rpt.Drawline(1);

            rpt.NewLine();
            //rpt.Write("Particulars", 25, 1);
            if (data.TaxRefNo == 1)
            {
                rpt.Write("Particulars", 25, 1);
                if (!gstSelection)
                {
                    rpt.Write("GST%", 4, 26);
                }
               
            }
            else
                rpt.Write("Particulars", 25, 1);
            rpt.NewLine();
            rpt.Write("Batch", 10, true);
            rpt.Write("Exp", 8, true);
            rpt.Write("Qty", 5, true, Alignment.Right);
            //rpt.Write("Price", 8, true);
            rpt.Write("MRP", 8, true, Alignment.Right);
            //rpt.Write("VAT", 5, true, Alignment.Right);
            rpt.Write("Amount", 9, true, Alignment.Right);
            rpt.Write();
            rpt.Drawline(1);
        }

        private void WriteReportItems_4InchRoll(PlainTextReport rpt, IInvoice data)
        {
            int len = 0;
            bool gstSelection = Convert.ToBoolean(data.Instance.Gstselect);
            foreach (var items in data.InvoiceItems)
            {
                if (items.Quantity <= 0)
                {
                    break;
                }
                rpt.NewLine();
                if (data.TaxRefNo == 1)
                {
                    var gst = items.GstTotal; //(items.Igst == 0 || items.Igst == null) ? (items.Cgst + items.Sgst) : items.Igst;

                    rpt.Write(items.ProductStock.Product.Name.ToUpper(), 16, 2);
                    if (!gstSelection)
                    {
                        rpt.Write(gst.ToString(), 4, 26);
                    }
                   
                }
                else
                    rpt.Write(items.ProductStock.Product.Name.ToUpper(), 25, 1);
                rpt.NewLine();
                rpt.Write(items.ProductStock.BatchNo.ToUpper(), 10, true);
                rpt.Write(string.Format("{0:MM/yy}", items.ProductStock.ExpireDate), 8, true);
                rpt.Write(string.Format("{0:0}", items.Quantity), 5, true, Alignment.Right);

                //if (items.SellingPrice > 0)
                //{
                //    decimal? vatPrice = ((items.SellingPrice * 100) / (items.ProductStock.VAT + 100));
                //    rpt.Write(string.Format("{0:0.00}", vatPrice), 8, true);
                //    rpt.Write(string.Format("{0:0.00}", items.SellingPrice - vatPrice), 6, true);
                //}
                //else
                //{
                //    decimal? vatPrice = items.ProductStock.SellingPrice - items.ProductStock.VatInPrice;
                //    rpt.Write(string.Format("{0:0.00}", items.ProductStock.VatInPrice), 8, true);
                //    rpt.Write(string.Format("{0:0.00}", vatPrice), 6, true);
                //}

                if (items.SellingPrice > 0)
                {
                    rpt.Write(string.Format("{0:0.00}", items.SellingPrice), 8, true, Alignment.Right);
                }
                else
                {
                    rpt.Write(string.Format("{0:0.00}", items.ProductStock.SellingPrice), 8, true, Alignment.Right);
                }

                //For MRP                
                // rpt.Write(string.Format("{0:0.00}", items.ProductStock.SellingPrice), 8, true, Alignment.Right);

                rpt.Write(string.Format("{0:0.00}", items.Total), 9, true, Alignment.Right);
                rpt.Write();
                len++;
            }

            for (int i = 0; i < 5 - len; i++)
                rpt.NewLine();

            rpt.Write();
            rpt.Drawline(1);
        }

        private void WriteReportFooter_4InchRoll(PlainTextReport rpt, IInvoice data)
        {
            bool gstSelection = Convert.ToBoolean(data.Instance.Gstselect);
            rpt.NewLine();
            if (gstSelection)
            {
                rpt.Write("Composite tax payer not eligible to collect taxes", 65, 1, Alignment.Left);
            }
            rpt.NewLine();
            rpt.Write("Total: " + string.Format("{0:0.00}", data.PurchasedTotal), 15, 1);
            rpt.Write("Disc.: " + string.Format("{0:0.00}", data.DiscountInValue), 15, 17);
            if (data.SalesReturnItem.Count == 0)
            {
                rpt.Write("R.Off: " + string.Format("{0:0.00}", data.RoundoffNetAmount), 17, 32, Alignment.Right);
                rpt.NewLine();
                rpt.Write("Net Amt: " + string.Format("{0:0.00}", data.NetAmount), 17, 32, Alignment.Right, data.IsBold);
                rpt.NewLine();
                rpt.NewLine();
                rpt.Write("E&OE ", 5, 1);
                rpt.Write(" Signature of the Regd. Pharmacist", 37, 12, Alignment.Right);
            }
            //rpt.Write("R.Off: " + string.Format("{0:0.00}", data.RoundoffNetAmount), 17, 32, Alignment.Right);
            //Model.sales.RoundOff - Model.sales.Net
            else
            {
                rpt.NewLine();
                rpt.Write("Net Amt: " + string.Format("{0:0.00}", data.Net), 17, 32, Alignment.Right, data.IsBold);

            }
            //rpt.Write("Net Amt: " + string.Format("{0:0.00}", data.Net + data.RoundoffNetAmount), 17, 32, Alignment.Right, data.isBold);
            rpt.Write();

            //Newly added for Print Footer by Mani on 28-02-2017
            GenerateFooter(rpt, 50, data.PrintFooterNote);
        }

        private void WriteReportFooter_4InchRoll_GST(PlainTextReport rpt, IInvoice data)
        {
            bool gstSelection = Convert.ToBoolean(data.Instance.Gstselect);

            if (data.SalesReturnItem.Count == 0)
            {
                rpt.NewLine();
                if (data.TaxRefNo == 1 && !gstSelection)
                {
                    rpt.Write("CGST: " + string.Format("{0:0.00}", Math.Round((decimal)data.CgstTotalValue, 2)), 13, 2);
                    rpt.Write((data.Instance.isUnionTerritory == true ? "UTGST: " : "SGST: ") + string.Format("{0:0.00}", data.SgstTotalValue), 13, 15);
                    //if (data.CgstTotalValue != 0 && data.SgstTotalValue != 0)
                    //{
                    //    rpt.Write("CGST: " + string.Format("{0:0.00}", Math.Round((decimal)data.CgstTotalValue, 2)), 13, 2);
                    //    rpt.Write("SGST: " + string.Format("{0:0.00}", data.SgstTotalValue), 13, 15);
                    //}
                    //else
                    //{
                    //    rpt.Write("IGST: " + string.Format("{0:0.00}", Math.Round((decimal)data.IgstTotalValue, 2)), 13, 2);
                    //}

                }

                rpt.Write("Total: " + string.Format("{0:0.00}", data.PurchasedTotal), 17, 32, Alignment.Right);
                rpt.NewLine();
                rpt.Write("Disc.: " + string.Format("{0:0.00}", data.DiscountInValue), 16, 32, Alignment.Right);
                rpt.NewLine();
                rpt.Write("R.Off: " + string.Format("{0:0.00}", data.RoundoffNetAmount), 15, 32, Alignment.Right);
                rpt.NewLine();
                rpt.Write("Net Amt: " + string.Format("{0:0.00}", data.NetAmount), 17, 28, Alignment.Right, data.IsBold);
                rpt.NewLine();
                rpt.NewLine();
                rpt.Write("E&OE ", 5, 1);
                rpt.Write(" Signature of the Regd. Pharmacist", 37, 12, Alignment.Right);
                rpt.Write();
            }
            else
            {
                WriteReportReturnedItems_4InchRoll_GST(rpt, data);
            }
            if (gstSelection)
            {
                rpt.NewLine();
                rpt.Write();
                rpt.Drawline(1);
                rpt.Write("Composite tax payer not eligible to collect taxes", 65, 1, Alignment.Left);
            }

            //Newly added for Print Footer by Mani on 28-02-2017
            GenerateFooter(rpt, 50, data.PrintFooterNote);
        }

        private void WriteReportReturnedItems_4InchRoll_GST(PlainTextReport rpt, IInvoice data)
        {
            bool gstSelection = Convert.ToBoolean(data.Instance.Gstselect);
            rpt.Write("RETURNS", 8, 0, Alignment.Center, TextAdjustment.Wrap);
            rpt.NewLine();

            decimal? totReturnAmount = 0;
            decimal? totReturnDiscount = 0;
            decimal? ReturnDiscount = 0;
            decimal? ReturnAmount = 0;
            int len = 0;
            decimal? rtnGSTTotalValue = 0;

            foreach (var items in data.SalesReturnItem)
            {
                if (items.ReturnedQuantity > 0)
                {
                    rpt.NewLine(); 
                    len++;
                    
                    //rpt.Write(items.ProductStock.Product.Name.ToUpper(), 25, 1);
                    //if (data.SalesReturn.TaxRefNo == 1)
                    //    rpt.Write(string.Format("{0:0.00}", items.ProductStock.GstTotal), 9, true, Alignment.Right);

                    if (data.TaxRefNo == 1 && !gstSelection)
                    {
                        var gst = (items.Igst == 0 || items.Igst == null) ? (items.Cgst + items.Sgst) : items.Igst;

                        rpt.Write(items.ProductStock.Product.Name.ToUpper(), 16, 2);
                        rpt.Write(string.Format("{0:0.00}", items.ProductStock.GstTotal), 11, true, Alignment.Right);
                    }
                    else
                        rpt.Write(items.ProductStock.Product.Name.ToUpper(), 25, 1);


                    rpt.NewLine();
                    rpt.Write(items.ProductStock.BatchNo.ToUpper(), 10, true);                   
                    rpt.Write(string.Format("{0:MM/yy}", items.ProductStock.ExpireDate), 8, true);
                    rpt.Write(string.Format("{0:0}", items.ReturnedQuantity), 5, true, Alignment.Right);

                    totReturnAmount += items.ReturnAmt;
                    ReturnAmount += items.ReturnAmt;

                    decimal? getGstPercent = items.GstTotal + 100;
                    decimal? SalesItemDiscount = ((items.Discount / 100) * items.Total);

                    if (items.SellingPrice > 0)
                    {
                        decimal? vatPrice = ((items.SellingPrice * 100) / (items.ProductStock.VAT + 100));
                        rpt.Write(string.Format("{0:0.00}", items.SellingPrice), 8, true, Alignment.Right);
                        //if (data.SalesReturn.TaxRefNo == 1)
                        //    rpt.Write(string.Format("{0:0.00}", items.ProductStock.GstTotal), 9, true, Alignment.Right);
                    }
                    else
                    {
                        rpt.Write(string.Format("{0:0.00}", items.ProductStock.SellingPrice), 9, true, Alignment.Right);
                        //if (data.SalesReturn.TaxRefNo == 1)
                        //    rpt.Write(string.Format("{0:0.00}", items.ProductStock.GstTotal), 8, true, Alignment.Right);
                    }
                    var ActualValue = (items.Total - SalesItemDiscount);
                    rtnGSTTotalValue += ActualValue - ((ActualValue * 100) / getGstPercent);

                    rpt.Write(string.Format("{0:0.00}", items.ReturnAmt), 9, true, Alignment.Right);
                    
                    totReturnDiscount = ((items.Discount / 100) * items.Total);
                    totReturnAmount = totReturnAmount - totReturnDiscount;
                    ReturnDiscount += totReturnDiscount;
                }
            }
            rpt.NewLine();
            rpt.Write();
            rpt.Drawline(1);

            rpt.NewLine();
            decimal? totValue = data.InvoiceItems.Sum(x => x.Total);
            decimal? totCgstValue = Math.Round((decimal)data.InvoiceItems.Sum(x => x.CgstValue), 2);
            decimal? totSgstValue = Math.Round((decimal)data.InvoiceItems.Sum(x => x.SgstValue), 2);

            decimal? RtntotgstValue = rtnGSTTotalValue / 2;//Math.Round((decimal)data.SalesReturnItem.Sum(x => x.CgstValue), 2);
            //decimal? RtntotSgstValue = rtnGSTTotalValue / 2;//Math.Round((decimal)data.SalesReturnItem.Sum(x => x.SgstValue), 2);

            if (data.CgstTotalValue != 0 && data.SgstTotalValue != 0)
            {
                if (!gstSelection)
                {
                    rpt.Write("CGST: " + string.Format("{0:0.00}", (totCgstValue - RtntotgstValue)), 12, 2);
                    rpt.Write((data.Instance.isUnionTerritory == true ? "UTGST: " : "SGST: ") + string.Format("{0:0.00}", (totSgstValue - RtntotgstValue)), 12, 15);
                }
               
            }
            rpt.Write("Total: " + string.Format("{0:0.00}", (totValue - ReturnAmount)), 15, 32, Alignment.Right);

            rpt.NewLine();
            rpt.Write("Disc.: " + string.Format("{0:0.00}", Math.Round((decimal)(data.DiscountInValue - ReturnDiscount), 2)), 15, 32, Alignment.Right);

            rpt.NewLine();
            decimal retAmount = data.RoundoffNetAmount as decimal? ?? 0; //RoundOffCalculation(data.Net, totReturnAmount);
            rpt.Write("Rupees: " + ((decimal)(data.NetAmount)).ConvertNumbertoWords(), 35, 2, TextAdjustment.Wrap);
            rpt.Write("R.Off: " + string.Format("{0:0.00}", retAmount), 15, 32, Alignment.Right);

            rpt.NewLine();
            rpt.Write("RETURN AMT: " + string.Format("{0:0.00}", totReturnAmount), 24, 21, Alignment.Right, data.IsBold);

            rpt.Write();
            rpt.Drawline(1);
            rpt.NewLine();
            //rpt.Write("PAYABLE AMOUNT: "
            //    + string.Format("{0:0.00}", Math.Round(((decimal)data.Net - (decimal)totReturnAmount) + retAmount)), 26, 19, Alignment.Right, data.IsBold);
            rpt.Write("PAYABLE AMOUNT: "
                + string.Format("{0:0.00}",data.NetAmount), 26, 19, Alignment.Right, data.IsBold);
        }

        private void WriteReportReturnItems_4InchRoll(PlainTextReport rpt, IInvoice data)
        {
            rpt.Drawline(1);
            rpt.Write("RETURNS", 8, 0, Alignment.Center, TextAdjustment.Wrap);
            rpt.NewLine();

            decimal? totReturnAmount = 0;
            decimal? totReturnDiscount = 0;
            decimal? ReturnDiscount = 0;
            int len = 0;

            foreach (var items in data.SalesReturnItem)
            {
                if (items.ReturnedQuantity > 0)
                {
                    rpt.NewLine();
                    rpt.Write(items.ProductStock.Product.Name.ToUpper(), 25, 1);
                    rpt.NewLine();
                    rpt.Write(items.ProductStock.BatchNo.ToUpper(), 10, true);
                    rpt.Write(string.Format("{0:MM/yy}", items.ProductStock.ExpireDate), 8, true);
                    rpt.Write(string.Format("{0:0}", items.ReturnedQuantity), 5, true, Alignment.Right);

                    totReturnAmount += items.ReturnAmt;

                    if (items.SellingPrice > 0)
                    {
                        rpt.Write(string.Format("{0:0.00}", items.SellingPrice), 8, true, Alignment.Right);
                    }
                    else
                    {
                        rpt.Write(string.Format("{0:0.00}", items.ProductStock.SellingPrice), 8, true, Alignment.Right);
                    }

                    totReturnDiscount = ((items.Discount / 100) * items.Total);
                    totReturnAmount = totReturnAmount - totReturnDiscount;
                    ReturnDiscount += totReturnDiscount;

                    rpt.Write(string.Format("{0:0.00}", items.ReturnAmt), 9, true, Alignment.Right);
                    rpt.Write();
                    len++;
                }
            }

            rpt.NewLine();
            rpt.NewLine();

            rpt.Write("RETURN AMT: " + string.Format("{0:0.00}", totReturnAmount), 18, 31, Alignment.Right, data.IsBold);

            rpt.Write();
            rpt.Drawline(1);

            rpt.NewLine();
            decimal retAmount = data.RoundoffNetAmount as decimal? ?? 0; //RoundOffCalculation(data.Net, totReturnAmount);
            rpt.Write("R.Off: " + string.Format("{0:0.00}", retAmount), 15, 1, Alignment.Left);
            if (ReturnDiscount != 0)
                rpt.Write("Disc.: -" + string.Format("{0:0.00}", ReturnDiscount), 15, 17);
            else
                rpt.Write("Disc.: " + string.Format("{0:0.00}", ReturnDiscount), 15, 17);
            rpt.NewLine();
            //rpt.Write("PAYABLE AMOUNT: " + string.Format("{0:0.00}", Math.Round((decimal)data.Net - (decimal)totReturnAmount + retAmount)), 28, 21, Alignment.Right, data.IsBold);  //Alignment Issue 
            rpt.Write("PAYABLE AMOUNT: " + string.Format("{0:0.00}",data.NetAmount), 28, 21, Alignment.Right, data.IsBold);  //Alignment Issue 
            rpt.NewLine();
            rpt.NewLine();
            rpt.Write("E&OE ", 5, 1);
            rpt.Write(" Signature of the Regd. Pharmacist", 37, 12, Alignment.Right);
        }
        #endregion

        #region 4InchRoll_Generic _template New

        protected string GenerateSalesReport_4InchRollNew(IInvoice data)
        {
            bool gstSelection = Convert.ToBoolean(data.Instance.Gstselect);
            var rpt = new PlainTextReport
            {
                Columns = 38,
                Rows = 30
            };

            //some leading blank lines
            rpt.NewLine();
            rpt.NewLine();

            WriteReportHeader_4InchRollNew(rpt, data);
            if (data.InvoiceItems.Count() == 0)
            {
                if (data.TaxRefNo == 1)
                    WriteReportOnlyReturnItems_4InchRollNew_GST(rpt, data);
                else
                {
                    WriteReportOnlyReturnItems_4InchRollNew(rpt, data);
                }
            }
            else
            {
                WriteReportItems_4InchRollNew(rpt, data);
                if (data.TaxRefNo == 1)
                    WriteReportFooter_4InchRollNew_GST(rpt, data);
                else
                    WriteReportFooter_4InchRollNew(rpt, data);
            }


            //some trailing blank lines
            rpt.NewLine();
            rpt.NewLine();
            rpt.Write();

            return rpt.Content.ToString();
        }

        private void WriteReportHeader_4InchRollNew(PlainTextReport rpt, IInvoice data)
        {
             rpt.NewLine();
            bool gstSelection = Convert.ToBoolean(data.Instance.Gstselect);
            //rpt.Write("#BB#"+data.Instance.Name.ToUpper()+"#BE#", 36, 1, Alignment.Center);
            rpt.Write(data.Instance.Name.ToUpper(), 36, 1, Alignment.Center, data.IsBold);
            string adddressarea = string.Empty;
            if (!string.IsNullOrEmpty(data.Instance.Address))
            {
                adddressarea += data.Instance.Address;
            }
            if (!string.IsNullOrEmpty(data.Instance.Area))
            {
                adddressarea += "," + data.Instance.Area;
            }
            if (!string.IsNullOrEmpty(adddressarea))
            {
                rpt.NewLine();
                rpt.Write(adddressarea.ToUpper(), 36, 1, Alignment.Center, TextAdjustment.Wrap);
            }
            string cityState = string.Empty;
            if (!string.IsNullOrEmpty(data.Instance.City))
            {
                cityState += data.Instance.City + ",";
            }
            if (!string.IsNullOrEmpty(data.Instance.State))
            {
                cityState += data.Instance.State + ",";
            }
            if (!string.IsNullOrEmpty(data.Instance.Pincode))
            {
                cityState += data.Instance.Pincode;
            }

            if (!string.IsNullOrEmpty(cityState))
            {
                rpt.NewLine();
                rpt.Write(cityState.ToUpper(), 36, 1, Alignment.Center, TextAdjustment.Wrap);
            }
            string contact = string.Empty;

            if (!string.IsNullOrEmpty(data.Instance.Phone))
            {
                contact += "Ph: " + data.Instance.Phone;
            }
            if (!string.IsNullOrEmpty(data.Instance.Mobile))
            {
                contact += "," + "Mobile: " + data.Instance.Mobile;
            }
            if (!string.IsNullOrEmpty(contact))
            {
                rpt.NewLine();
                rpt.Write(contact.ToUpper(), 36, 1, Alignment.Center, TextAdjustment.Wrap);
            }
            //rpt.WriteLine();
            rpt.NewLine();

            rpt.Write("DL No.:" + data.Instance.DrugLicenseNo.ToUpper(), 36, 1, Alignment.Center, TextAdjustment.Wrap);

            rpt.NewLine();
            if (data.TaxRefNo == 1)
            {
                rpt.Write("GSTIN No:" + data.Instance.GsTinNo.ToUpper(), 33, 1, Alignment.Center, TextAdjustment.Wrap);
            }              
            else
            {
                rpt.Write("TIN No.:" + data.Instance.TinNo.ToUpper(), 36, 1, Alignment.Center, TextAdjustment.Wrap);
            }

            rpt.NewLine();
            rpt.Write("Date : " + string.Format("{0:dd/MM/yy}", data.InvoiceDate), 18, 1);

            //rpt.Write("INVOICE" + data.InvoiceNo, 7, 22);

            if (data.InvoiceItems.Count() == 0)
            {
                rpt.Write("Return No : " + data.InvoiceSeries + data.InvoiceNo, 16, 20);
            }
            else
            {
                rpt.Write("Bill No.: " + data.InvoiceSeries + data.InvoiceNo, 16, 20);
            }


            //rpt.Write("Bill No: " + data.InvoiceSeries + " " + data.InvoiceNo, 18, 31);
            rpt.NewLine();
            rpt.Write("Time : " + Convert.ToDateTime(data.SalesCreatedAt).ToString("hh:mm tt"), 18, 1);
            rpt.Write("B.Type: " + data.PaymentType, 16, 20);
            //rpt.WriteLine();
            rpt.NewLine();
            rpt.Write("DOCTOR  : " + data.DoctorName.ToUpper(), 36, 1, TextAdjustment.Wrap);
            //rpt.Write("Bill Type: " + data.PaymentType, 15, 33);

            rpt.NewLine();
            rpt.Write("PATIENT : " + data.Name.ToUpper(), 36, 1, TextAdjustment.Wrap);
            rpt.NewLine();
            rpt.Write("Address : " + data.Address, 36, 1);

            if (data.Address.Length > 26)
            {
                rpt.NewLine();
                rpt.Write("" + data.Address.Substring(26, data.Address.Length - 26), 36, 1);
                // rpt.Write(" " + "", 18, 36);
            }
            if (gstSelection)
            {
                rpt.NewLine();
                rpt.Write("Bill of Supply", 44, 1, Alignment.Center, TextAdjustment.Wrap);
            }
            rpt.NewLine();
            rpt.Drawline();
            rpt.Write("#", 3, 1);
            //rpt.Write("Product", 27, 4);            
            if (data.TaxRefNo == 1)
            {
                rpt.Write("Product", 22, 4);
                if (!gstSelection)
                    rpt.Write("GST%", 4, 23);
            }
            //rpt.Write("Batch", 8, 28);
            rpt.Write("Batch", 6, 30);
            rpt.NewLine();
            rpt.Write("Exp", 6, 4);
            rpt.Write("MRP", 8, 11, Alignment.Right);
            rpt.Write("Qty", 6, 19, Alignment.Right);
            //rpt.Write("Value", 9, 39, Alignment.Right);
            rpt.Write("Value", 11, 25, Alignment.Right);

            rpt.NewLine();
            rpt.Drawline();

        }

        private void WriteReportItems_4InchRollNew(PlainTextReport rpt, IInvoice data)
        {
            int len = 0;
            bool gstSelection = Convert.ToBoolean(data.Instance.Gstselect);
            foreach (var items in data.InvoiceItems)
            {
                if (items.Quantity <= 0)
                {
                    break;
                }
                rpt.NewLine();
                rpt.Write((len + 1).ToString(), 3, 1);

                if (items.ProductStock.Product.Name.Length > 27)
                {
                    if (data.TaxRefNo == 1 && !gstSelection)
                    {
                        var gst = (items.Igst == 0 || items.Igst == null) ? (items.Cgst + items.Sgst) : items.Igst;
                        rpt.Write(items.ProductStock.Product.Name.ToUpper().Substring(0, 16), 17, 4);
                        rpt.Write(gst.ToString(), 4, 21);
                    }
                    else
                    {
                        rpt.Write(items.ProductStock.Product.Name.ToUpper().Substring(0, 16), 17, 4);
                    }
                }
                else
                {
                    if (data.TaxRefNo == 1 && !gstSelection)
                    {
                        var gst = (items.Igst == 0 || items.Igst == null) ? (items.Cgst + items.Sgst) : items.Igst;
                        rpt.Write(items.ProductStock.Product.Name.ToUpper(), 16, 4);
                        rpt.Write(gst.ToString(), 6, 22);
                    }
                    else
                        rpt.Write(items.ProductStock.Product.Name.ToUpper(), 16, 4);
                }

                var batchNo = items.ProductStock.BatchNo.ToUpper();
                if (batchNo.Length > 8)
                    rpt.Write(items.ProductStock.BatchNo.ToUpper().Substring(items.ProductStock.BatchNo.Length - 8), 8, 28);                
                else
                    rpt.Write(items.ProductStock.BatchNo.ToUpper(), 7, 29);

                rpt.NewLine();
                rpt.Write(string.Format("{0:MM/yy}", items.ProductStock.ExpireDate), 6, 4, Alignment.Right);

                rpt.Write(string.Format("{0:0.00}", items.SellingPrice == null ? items.ProductStock.SellingPrice : items.SellingPrice), 8, 11, Alignment.Right);
                rpt.Write(string.Format("{0:0}", items.Quantity), 6, 19, Alignment.Right);
                rpt.Write(string.Format("{0:0.00}", ((items.SellingPrice == null ? items.ProductStock.SellingPrice : items.SellingPrice) * items.Quantity)), 11, 25, Alignment.Right);

                len++;

            }
            rpt.Write();
            rpt.Drawline();
        }

        private void WriteReportFooter_4InchRollNew(PlainTextReport rpt, IInvoice data)
        {

            rpt.NewLine();

            rpt.Write("Disc.: " + string.Format("{0:0.00}", data.DiscountInValue), 13, 1);
            //rpt.Write("R.Off: " + string.Format("{0:0.00}", data.RoundOff - data.Net), 12, 15);
            rpt.Write("T.VAT", 9, 16);
            rpt.Write(string.Format("{0:0.00}", data.Vat), 09, 27, Alignment.Right);

            if (data.SalesReturnItem.Count == 0)
            {
                rpt.NewLine();
                rpt.Write("TOTAL", 9, 16);
                rpt.Write(string.Format("{0:0.00}", data.NetAmount), 11, 25, Alignment.Right, data.IsBold);
                rpt.Drawline(1);
                rpt.NewLine();
                rpt.NewLine();
                rpt.Write("E&OE ", 5, 1);
                rpt.Write(" Pharmacist Signature", 27, 10, Alignment.Right);
            }
            else
            {
                rpt.NewLine();
                rpt.Write("TOTAL", 9, 16);
                rpt.Write(string.Format("{0:0.00}", data.Net), 11, 25, Alignment.Right, data.IsBold);
                rpt.Drawline(1);
            }

            rpt.Write();

            if (data.SalesReturnItem.Count > 0)
            {
                string payableAmount = "";
                WriteReportReturnItems_4InchRollNew(rpt, data, out payableAmount);
                data.InWords = payableAmount;
            }
            //rpt.WriteLine();
            rpt.NewLine();
            string payAmountInWords = ((decimal)data.NetAmount).ConvertNumbertoWords();
            rpt.Write(string.Format("Rupees {0} only.", payAmountInWords), 36, 1, TextAdjustment.Wrap);

            //rpt.WriteLine();
            rpt.NewLine();
            rpt.NewLine();
            rpt.NewLine();
            rpt.Write("*** THANK YOU ***", 36, 1, Alignment.Center);

            //Newly added for Print Footer by Mani on 28-02-2017
            GenerateFooter(rpt, 35, data.PrintFooterNote);

        }

        private void WriteReportFooter_4InchRollNew_GST(PlainTextReport rpt, IInvoice data)
        {
            bool gstSelection = Convert.ToBoolean(data.Instance.Gstselect);
            
            if (data.SalesReturnItem.Count == 0)
            {
                rpt.NewLine();
                rpt.Write("Disc.:" + string.Format("{0:0.00}", data.DiscountInValue), 13, 1);
                
                if (data.TaxRefNo == 1 && !gstSelection)
                {
                    rpt.Write("CGST:" + string.Format("{0:0.00}", Math.Round((decimal)data.CgstTotalValue, 2)), 12, 15);
                    rpt.Write((data.Instance.isUnionTerritory == true ? "UTGST:" : "SGST:") + string.Format("{0:0.00}", Math.Round((decimal)data.SgstTotalValue, 2)), 12, 25);
                    //if (data.CgstTotalValue != 0 && data.SgstTotalValue != 0)
                    //{
                    //    rpt.Write("CGST:" + string.Format("{0:0.00}", Math.Round((decimal)data.CgstTotalValue, 2)), 12, 15);
                    //    rpt.Write("SGST:" + string.Format("{0:0.00}", Math.Round((decimal)data.SgstTotalValue, 2)), 12, 25);
                    //}
                    //else
                    //{
                    //    rpt.Write("IGST: " + string.Format("{0:0.00}", Math.Round((decimal)data.IgstTotalValue, 2)), 15, 15);
                    //}

                }

                //rpt.Write("T.VAT", 9, 16);
                //rpt.Write(string.Format("{0:0.00}", data.Vat), 09, 27, Alignment.Right);

                rpt.NewLine();
                rpt.Write("TOTAL", 9, 16);
                rpt.Write(string.Format("{0:0.00}", data.NetAmount), 11, 25, Alignment.Right, data.IsBold);
                rpt.Drawline(1);
                rpt.NewLine();
                rpt.NewLine();
                rpt.Write("E&OE ", 5, 1);
                rpt.Write(" Pharmacist Signature", 27, 10, Alignment.Right);
                rpt.Write();

                //rpt.WriteLine();
                rpt.NewLine();
                string payAmountInWords = ((decimal)data.NetAmount).ConvertNumbertoWords();
                rpt.Write(string.Format("Rupees {0} only.", payAmountInWords), 36, 1, TextAdjustment.Wrap);
                //rpt.Write(string.Format("Rupees {0} only.", payAmountInWords), 30, 1, TextAdjustment.Wrap);
                
                //rpt.WriteLine();
                if (gstSelection)
                {
                    rpt.Write();
                    rpt.NewLine();
                    rpt.Write("Composite tax payer not eligible to collect taxes", 36, 1, TextAdjustment.Wrap);
                }
                rpt.NewLine();
                rpt.NewLine();
                rpt.NewLine();
                rpt.Write("*** THANK YOU ***", 36, 1, Alignment.Center);
            }
            else
            {
                string payableAmount = "";
                WriteReportReturnItems_4InchRollNew(rpt, data, out payableAmount);
                data.InWords = payableAmount + " Only";
            }
            //Newly added for Print Footer by Mani on 28-02-2017
            GenerateFooter(rpt, 35, data.PrintFooterNote);

        }

        private void WriteReportReturnItems_4InchRollNew(PlainTextReport rpt, IInvoice data, out string payAmount)
        {
            rpt.Write("RETURNS", 8, 0, Alignment.Center, TextAdjustment.Wrap);
            rpt.NewLine();
            bool gstSelection = Convert.ToBoolean(data.Instance.Gstselect);
            decimal? totReturnAmount = 0;
            decimal? totReturnDiscount = 0;
            decimal? ReturnDiscount = 0;
            decimal? returnAmt = 0;
            int len = 0;
            decimal? rtnGSTTotalValue = 0;

            foreach (var items in data.SalesReturnItem)
            {
                if (items.ReturnedQuantity > 0)
                {
                    rpt.NewLine();
                    rpt.Write((len + 1).ToString(), 3, 1);

                    if (items.ProductStock.Product.Name.Length > 27)
                    {
                        if (data.TaxRefNo == 1)
                        {
                            var gst = (items.Igst == 0 || items.Igst == null) ? (items.Cgst + items.Sgst) : items.Igst;
                            rpt.Write(items.ProductStock.Product.Name.ToUpper().Substring(0, 16), 17, 4);
                            rpt.Write(gst.ToString(), 4, 21);
                        }
                        else
                        {
                            rpt.Write(items.ProductStock.Product.Name.ToUpper().Substring(0, 16), 17, 4);
                        }
                    }
                    else
                    {
                        if (data.TaxRefNo == 1)
                        {
                            var gst = (items.Igst == 0 || items.Igst == null) ? (items.Cgst + items.Sgst) : items.Igst;
                            rpt.Write(items.ProductStock.Product.Name.ToUpper(), 16, 4);
                            rpt.Write(gst.ToString(), 6, 22);
                        }
                        else
                            rpt.Write(items.ProductStock.Product.Name.ToUpper(), 16, 4);
                    }
                    totReturnAmount += items.ReturnAmt;
                    returnAmt += items.ReturnAmt;

                    decimal? getGstPercent = items.GstTotal + 100;
                    decimal? SalesItemDiscount = ((items.Discount / 100) * items.Total);

                    var batchNo = items.ProductStock.BatchNo.ToUpper();
                    if (batchNo.Length > 8)
                        rpt.Write(items.ProductStock.BatchNo.ToUpper().Substring(items.ProductStock.BatchNo.Length - 8), 8, 28);
                    else
                        rpt.Write(items.ProductStock.BatchNo.ToUpper(), 7, 29);

                    rpt.NewLine();
                    rpt.Write(string.Format("{0:MM/yy}", items.ProductStock.ExpireDate), 6, 4, Alignment.Right);

                    rpt.Write(string.Format("{0:0.00}", items.SellingPrice == null ? items.ProductStock.SellingPrice : items.SellingPrice), 8, 11, Alignment.Right);
                    rpt.Write(string.Format("{0:0}", items.ReturnedQuantity), 6, 19, Alignment.Right);
                    rpt.Write(string.Format("{0:0.00}", ((items.SellingPrice == null ? items.ProductStock.SellingPrice : items.SellingPrice) * items.ReturnedQuantity)), 11, 25, Alignment.Right);
                    len++;

                    var ActualValue = (items.Total - SalesItemDiscount);
                    rtnGSTTotalValue += ActualValue - ((ActualValue * 100) / getGstPercent);

                    totReturnDiscount = ((items.Discount / 100) * items.OnlyReturnAmount);
                    totReturnAmount = totReturnAmount - totReturnDiscount;
                    //totReturnDiscount = totReturnDiscount + (items.ReturnDiscountAmt * items.ReturnedQuantity);
                    //totReturnAmount = totReturnAmount - totReturnDiscount;
                    ReturnDiscount += totReturnDiscount;
                }
            }

            rpt.NewLine();
            rpt.NewLine();
        
            rpt.Write();
            rpt.Drawline(1);

            rpt.NewLine();
            decimal? totValue = data.InvoiceItems.Sum(x => x.Total);
            decimal? totCgstValue = data.InvoiceItems.Sum(x => x.CgstValue);
            decimal? totSgstValue = data.InvoiceItems.Sum(x => x.SgstValue);

            decimal? RtntotgstValue = rtnGSTTotalValue / 2;
            //decimal? RtntotCgstValue = data.SalesReturnItem.Sum(x => x.CgstValue);
            //decimal? RtntotSgstValue = data.SalesReturnItem.Sum(x => x.SgstValue);

            if (data.CgstTotalValue != 0 && data.SgstTotalValue != 0 && !gstSelection)
            {
                rpt.Write("CGST:" + string.Format("{0:0.00}", (totCgstValue - RtntotgstValue)), 15, 2);                
            }
            rpt.Write("TOTAL: " + string.Format("{0:0.00}", (totValue - returnAmt)), 15, 21, Alignment.Right);

            rpt.NewLine();
            decimal retAmount = data.RoundoffNetAmount as decimal? ?? 0; //RoundOffCalculation(data.Net, totReturnAmount);
            if (data.CgstTotalValue != 0 && data.SgstTotalValue != 0 && !gstSelection)
            {
                rpt.Write((data.Instance.isUnionTerritory == true ? "UTGST:" : "SGST:") + string.Format("{0:0.00}", (totSgstValue - RtntotgstValue)), 15, 2);
            }
            rpt.Write("R.Off: " + string.Format("{0:0.00}", retAmount), 15, 21, Alignment.Right);

            rpt.NewLine();
            rpt.Write("Disc.: " + string.Format("{0:0.00}", (data.DiscountInValue - ReturnDiscount)), 15, 21, Alignment.Right); //data.TotalDiscount

            rpt.NewLine();
            rpt.Write("RETURN AMT ", 14, 14);
            rpt.Write(string.Format("{0:0.00}", totReturnAmount), 11, 24, Alignment.Right, data.IsBold);

            rpt.Write();
            rpt.Drawline(1);
            rpt.NewLine();
            rpt.Write("PAYABLE AMOUNT ", 19, 10);
            //rpt.Write(string.Format("{0:0.00}", Math.Round((decimal)data.Net - (decimal)totReturnAmount + retAmount)), 11, 24, Alignment.Right, data.IsBold);
            rpt.Write(string.Format("{0:0.00}", data.NetAmount), 11, 24, Alignment.Right, data.IsBold);
            payAmount = ((decimal)(data.NetAmount)).ConvertNumbertoWords();
            
            if (gstSelection)
            {
                rpt.NewLine();
                rpt.Write("Composite tax payer not eligible to collect taxes", 35, 1, TextAdjustment.Wrap);
            }
            rpt.NewLine();
            rpt.NewLine();
            rpt.Write("E&OE ", 5, 1);
            rpt.Write(" Pharmacist Signature", 27, 10, Alignment.Right);
        }
        #endregion



        #region A5_WithHeader_Generic _template

        protected string GenerateSalesReport_A5_WithHeader(IInvoice data)
        {
            PlainTextReport rpt = new PlainTextReport();
            bool gstSelection = Convert.ToBoolean(data.Instance.Gstselect);
            rpt.Columns = 63;
            rpt.Rows = 30;

            //some leading blank lines
            rpt.NewLine();
            rpt.NewLine();

            WriteReportHeader_A5_WithHeader(rpt, data);

            if (data.InvoiceItems.Count() == 0)
            {
                if (data.TaxRefNo == 1)
                    WriteReportOnlyReturnItems_A5_WithHeader_GST(rpt, data);
                else
                {
                    WriteReportOnlyReturnItems_A5_WithHeader(rpt, data);
                }
            }
            else
            {
                WriteReportItems_A5_WithHeader(rpt, data);
                if (data.TaxRefNo == 1)
                    WriteReportFooter_A5_WithHeader_GST(rpt, data);
                else
                    WriteReportFooter_A5_WithHeader(rpt, data);
            }

            //some trailing blank lines
            rpt.NewLine();
            rpt.NewLine();
            rpt.Write();

            return rpt.Content.ToString();
        }

        private void WriteReportHeader_A5_WithHeader(PlainTextReport rpt, IInvoice data)
        {
            rpt.NewLine();
            bool gstSelection = Convert.ToBoolean(data.Instance.Gstselect);
            //rpt.Write("#BB#"+data.Instance.Name.ToUpper()+ "#BE#", 60, 0, Alignment.Center, TextAdjustment.Wrap);
            rpt.Write(data.Instance.Name.ToUpper(), 60, 0, Alignment.Center, TextAdjustment.Wrap, data.IsBold);
            string adddressarea = string.Empty;
            if (!string.IsNullOrEmpty(data.Instance.Address))
            {
                adddressarea += data.Instance.Address;
            }
            if (!string.IsNullOrEmpty(data.Instance.Area))
            {
                adddressarea += "," + data.Instance.Area;
            }
            if (!string.IsNullOrEmpty(adddressarea))
            {
                rpt.NewLine();
                rpt.Write(adddressarea.ToUpper(), 60, 0, Alignment.Center, TextAdjustment.Wrap);
            }

            string cityState = string.Empty;
            if (!string.IsNullOrEmpty(data.Instance.City))
            {
                cityState += data.Instance.City + ",";
            }
            if (!string.IsNullOrEmpty(data.Instance.State))
            {
                cityState += data.Instance.State + ",";
            }
            if (!string.IsNullOrEmpty(data.Instance.Pincode))
            {
                cityState += data.Instance.Pincode;
            }

            if (!string.IsNullOrEmpty(cityState))
            {
                rpt.NewLine();
                rpt.Write(cityState.ToUpper(), 60, 0, Alignment.Center, TextAdjustment.Wrap);
            }


            string contact = string.Empty;

            if (!string.IsNullOrEmpty(data.Instance.Phone))
            {
                contact += "Phone No: " + data.Instance.Phone;
            }
            if (!string.IsNullOrEmpty(data.Instance.Mobile))
            {
                contact += "," + "Mobile No: " + data.Instance.Mobile;
            }
            if (!string.IsNullOrEmpty(contact))
            {
                rpt.NewLine();
                rpt.Write(contact.ToUpper(), 60, 0, Alignment.Center, TextAdjustment.Wrap);
            }

            rpt.NewLine();
            rpt.Write("DL No : " + data.Instance.DrugLicenseNo, 60, 2, Alignment.Center, TextAdjustment.Wrap);

            rpt.NewLine();
            if (data.TaxRefNo == 1)
                rpt.Write("GSTIN No: " + data.Instance.GsTinNo, 60, 2, Alignment.Center, TextAdjustment.Wrap);
            else
                rpt.Write("TIN   : " + data.Instance.TinNo, 60, 2, Alignment.Center, TextAdjustment.Wrap);

            if (gstSelection)
            {
                rpt.Write("Bill of Supply", 44, 10, Alignment.Center, TextAdjustment.Wrap);
            }
            rpt.NewLine();
            rpt.Drawline(1);

            //rpt.NewLine();
            rpt.Write("Doctor  : " + data.DoctorName.ToUpper(), 40, 2);

            if (data.InvoiceItems.Count() == 0)
            {
                rpt.Write("Return No : " + data.InvoiceSeries + data.InvoiceNo, 18, 41);
            }
            else
            {
                rpt.Write("Bill No: " + data.InvoiceSeries + data.InvoiceNo, 18, 41);
            }

            //  rpt.Write("Bill No: " + data.InvoiceSeries + " " + data.InvoiceNo, 18, 31);

            rpt.NewLine();
            rpt.Write("Patient : " + data.Name.ToUpper(), 40, 2);
            rpt.Write("B Date : " + string.Format("{0:dd/MM/yy}", data.InvoiceDate), 18, 41);


            rpt.NewLine();
            rpt.Write("Address : " + data.Address, 40, 2);
            rpt.Write("Time   : " + Convert.ToDateTime(data.SalesCreatedAt).ToString("hh:mm tt"), 18, 41);

            if (data.Address.Length > 30)
            {
                rpt.NewLine();
                rpt.Write("" + data.Address.Substring(29, data.Address.Length - 29), 41, 2);
                rpt.Write(" " + "", 18, 41);
            }

            // rpt.Write("B Type : " + data.PaymentType, 18, 41);

            rpt.NewLine();
            rpt.Write("City    : " + data.Patient.City, 40, 2);
            rpt.Write("B Type : " + data.PaymentType, 18, 41);
            //rpt.Write("ID No. : ", 18, 41);
            rpt.NewLine();

            rpt.Write("Pincode : " + data.Patient.Pincode, 40, 2, TextAdjustment.NewLine);
            rpt.Write("ID No. : " + Convert.ToString(data.Patient.EmpID), 18, 41);
            rpt.Write();
            rpt.Drawline(1);

            rpt.NewLine();
            rpt.Write("SNo", 3, 1, Alignment.Right);
            rpt.Write("Particulars", 18, 5);
            rpt.Write("Batch", 6, true, Alignment.Right);
            rpt.Write("Exp", 5, true);
            rpt.Write("Qty", 4, true, Alignment.Right);
            rpt.Write("MRP", 6, true, Alignment.Right);
            if (data.TaxRefNo == 1 && !gstSelection)
                rpt.Write("GST%", 5, true, Alignment.Right);
            else if (!gstSelection)
                rpt.Write("VAT", 5, true, Alignment.Right);
            else
                rpt.Write(" ", 3, true, Alignment.Right);
            rpt.Write("Amount", 8, true, Alignment.Right);
            rpt.Write();
            rpt.Drawline(1);
        }

        private void WriteReportItems_A5_WithHeader(PlainTextReport rpt, IInvoice data)
        {
            bool gstSelection = Convert.ToBoolean(data.Instance.Gstselect);
            int len = 0;
            foreach (var items in data.InvoiceItems)
            {
                if (items.Quantity <= 0)
                {
                    break;
                }
                rpt.NewLine();
                len++;

                rpt.Write(string.Format("{0:0}", len), 3, 1, Alignment.Right);

                rpt.Write(items.ProductStock.Product.Name.ToUpper(), 18, 5);
                rpt.Write(items.ProductStock.BatchNo, 6, true, Alignment.Right);
                rpt.Write(string.Format("{0:MM/yy}", items.ProductStock.ExpireDate), 5, true);
                rpt.Write(string.Format("{0:0}", items.Quantity), 4, true, Alignment.Right);

                if (items.SellingPrice > 0)
                {
                    decimal? vatPrice = ((items.SellingPrice * 100) / (items.ProductStock.VAT + 100));                    
                    rpt.Write(string.Format("{0:0.00}", items.SellingPrice), 6, true, Alignment.Right);
                    if (data.TaxRefNo == 1 && !gstSelection)
                        rpt.Write(string.Format("{0:0.00}", items.GstTotal), 6, true, Alignment.Right);
                    else if (!gstSelection)
                        rpt.Write(string.Format("{0:0.00}", items.ProductStock.VAT), 5, true, Alignment.Right);                    
                    else
                        rpt.Write(" ", 3, true, Alignment.Right);
                }
                else
                {
                    decimal? vatPrice = items.ProductStock.SellingPrice - items.ProductStock.VatInPrice;
                    rpt.Write(string.Format("{0:0.00}", items.ProductStock.SellingPrice), 6, true, Alignment.Right);
                    if (data.TaxRefNo == 1 && !gstSelection)
                        rpt.Write(string.Format("{0:0.00}", items.GstTotal), 6, true, Alignment.Right);
                    else if (!gstSelection)
                        rpt.Write(string.Format("{0:0.00}", items.ProductStock.VAT), 5, true, Alignment.Right);
                }

                rpt.Write(string.Format("{0:0.00}", items.Total), 7, true, Alignment.Right);
                rpt.Write();                
            }

            for (int i = 0; i < 5 - len; i++)
                rpt.NewLine();

            rpt.Write();
            rpt.Drawline(1);
        }

        private void WriteReportFooter_A5_WithHeader(PlainTextReport rpt, IInvoice data)
        {
            rpt.NewLine();
            bool gstSelection = Convert.ToBoolean(data.Instance.Gstselect);
            if (gstSelection)
            {
                rpt.Write("Composite tax payer not eligible to collect taxes", 65, 1, Alignment.Left);
            }
            rpt.NewLine();
            rpt.Write("Total: " + string.Format("{0:0.00}", data.PurchasedTotal), 20, 2);
            rpt.Write(" Dis: " + string.Format("{0:0.00}", data.DiscountInValue), 18, 17);

            if (data.SalesReturnItem.Count == 0)
            {
                rpt.Write(" R.Off: " + string.Format("{0:0.00}", data.RoundoffNetAmount), 18, 29);
                rpt.Write("Net Amt: " + string.Format("{0:0.00}", data.NetAmount), 18, 42, Alignment.Right, data.IsBold);
            }
            //rpt.Write("R.Off: " + string.Format("{0:0.00}", data.RoundoffNetAmount), 17, 32, Alignment.Right);
            //Model.sales.RoundOff - Model.sales.Net
            else
            {
                rpt.Write("Net Amt: " + string.Format("{0:0.00}", data.Net), 18, 42, Alignment.Right, data.IsBold);
            }

            //rpt.Write(" R.Off: " + string.Format("{0:0.00}", data.RoundoffNetAmount), 18, 29);            
            //rpt.Write("Net Amt: " + string.Format("{0:0.00}", data.Net + data.RoundoffNetAmount), 18, 44, Alignment.Right, data.isBold);
            rpt.Write();
            rpt.Drawline(1);

            if (data.SalesReturnItem.Count > 0)
            {
                string payableAmount = "";
                WriteReportReturnedItems_A5_WithHeader(rpt, data, out payableAmount);
                data.InWords = payableAmount + " Only";
            }

            //Newly added for Print Footer by Mani on 28-02-2017
            GenerateFooter(rpt, 60, data.PrintFooterNote);
        }

        private void WriteReportFooter_A5_WithHeader_GST(PlainTextReport rpt, IInvoice data)
        {
            bool gstSelection = Convert.ToBoolean(data.Instance.Gstselect);
            
            if (data.SalesReturnItem.Count == 0)
            {
                rpt.NewLine();
                rpt.Write("Total: " + string.Format("{0:0.00}", data.PurchasedTotal), 20, 42, Alignment.Right);

                if (data.TaxRefNo == 1 && !gstSelection)
                {
                    if (data.CgstTotalValue != 0 && data.SgstTotalValue != 0)
                    {
                        rpt.Write("CGST: " + string.Format("{0:0.00}", data.CgstTotalValue), 15, 2);
                        rpt.Write((data.Instance.isUnionTerritory == true ? "UTGST: " : "SGST: ") + string.Format("{0:0.00}", data.SgstTotalValue), 15, 18);
                    }
                    else
                    {
                        rpt.Write("CGST:" + string.Format("{0:0.00}", data.CgstTotalValue), 15, 2, Alignment.Right);
                        rpt.Write((data.Instance.isUnionTerritory == true ? "UTGST:" : "SGST:") + string.Format("{0:0.00}", data.SgstTotalValue), 15, 18, Alignment.Right);
                    }
                }

                rpt.NewLine();
                rpt.Write("User Name: " + data.HQueUser.Name, 30, 2);
                rpt.Write("Disc." + Math.Round((decimal)data.TotalDiscount, 2) + "%: " + " -" + string.Format("{0:0.00}", data.DiscountInValue), 20, 42, Alignment.Right);

                rpt.NewLine();
                if (data.SalesReturnItem.Count == 0)
                {
                    rpt.Write("Rupees " + ((decimal)data.NetAmount).ConvertNumbertoWords(), 40, 2, TextAdjustment.Wrap);
                    rpt.Write(" R.Off: " + string.Format("{0:0.00}", data.RoundoffNetAmount), 18, 44, Alignment.Right);
                    rpt.NewLine();
                    rpt.Write("Net Amt: " + string.Format("{0:0.00}", data.NetAmount), 18, 42, Alignment.Right, data.IsBold);
                }
                rpt.Write();
                rpt.Drawline(1);

                if (gstSelection)
                {                 
                    GenerateGstSelected(rpt);
                }
            }
            else
            {                
                WriteReportReturnedItems_A5_WithHeader_GST(rpt, data);                
            }

            GenerateFooter(rpt, 60, data.PrintFooterNote);
        }

        private void WriteReportReturnedItems_A5_WithHeader(PlainTextReport rpt, IInvoice data, out string payableAmount)
        {
            rpt.Write("RETURNS", 8, 0, Alignment.Center, TextAdjustment.Wrap);
            rpt.NewLine();

            decimal? totReturnAmount = 0;
            decimal? totReturnDiscount = 0;
            decimal? ReturnDiscount = 0;
            int len = 0;

            foreach (var items in data.SalesReturnItem)
            {
                if (items.ReturnedQuantity > 0)
                {
                    rpt.NewLine();
                    len++;

                    rpt.Write(string.Format("{0:0}", len), 3, 1, Alignment.Right);

                    rpt.Write(items.ProductStock.Product.Name.ToUpper(), 18, 5);
                    rpt.Write(items.ProductStock.BatchNo, 6, true, Alignment.Right);
                    rpt.Write(string.Format("{0:MM/yy}", items.ProductStock.ExpireDate), 5, true);
                    rpt.Write(string.Format("{0:0}", items.ReturnedQuantity), 4, true, Alignment.Right);

                    totReturnAmount += items.ReturnAmt;
                    if (items.SellingPrice > 0)
                    {
                        decimal? vatPrice = ((items.SellingPrice * 100) / (items.ProductStock.VAT + 100));
                        rpt.Write(string.Format("{0:0.00}", items.SellingPrice), 6, true, Alignment.Right);
                        rpt.Write(string.Format("{0:0.00}", items.ProductStock.VAT), 5, true, Alignment.Right);
                    }
                    else
                    {
                        decimal? vatPrice = items.ProductStock.SellingPrice - items.ProductStock.VatInPrice;
                        rpt.Write(string.Format("{0:0.00}", items.ProductStock.SellingPrice), 6, true, Alignment.Right);
                        rpt.Write(string.Format("{0:0.00}", items.ProductStock.VAT), 5, true, Alignment.Right);
                    }

                    rpt.Write(string.Format("{0:0.00}", ((items.SellingPrice == null ? items.ProductStock.SellingPrice : items.SellingPrice) * items.ReturnedQuantity)), 8, true, Alignment.Right);
                    rpt.Write();

                    totReturnDiscount = ((items.Discount / 100) * items.Total);
                    totReturnAmount = totReturnAmount - totReturnDiscount;
                    ReturnDiscount += totReturnDiscount;
                }
            }

            rpt.NewLine();
            rpt.NewLine();

            rpt.Write("RETURN AMT: " + string.Format("{0:0.00}", totReturnAmount), 18, 42, Alignment.Right, data.IsBold);

            rpt.Write();
            rpt.Drawline(1);

            rpt.NewLine();
            decimal retAmount = data.RoundoffNetAmount as decimal? ?? 0; //RoundOffCalculation(data.Net, totReturnAmount);
            rpt.Write("R.Off: " + string.Format("{0:0.00}", retAmount), 15, 1, Alignment.Left);
            if (ReturnDiscount != 0)
                rpt.Write("Disc.: -" + string.Format("{0:0.00}", ReturnDiscount), 15, 17);
            else
                rpt.Write("Disc.: " + string.Format("{0:0.00}", ReturnDiscount), 15, 17);
            //rpt.NewLine();
            //rpt.Write("PAYABLE AMOUNT: " + string.Format("{0:0.00}", Math.Round(((decimal)data.Net - (decimal)totReturnAmount) + retAmount)), 28, 32, Alignment.Right, data.IsBold);
            rpt.Write("PAYABLE AMOUNT: " + string.Format("{0:0.00}", data.NetAmount), 28, 32, Alignment.Right, data.IsBold);

            payableAmount = ((decimal)(data.NetAmount)).ConvertNumbertoWords();
        }

        private void WriteReportReturnedItems_A5_WithHeader_GST(PlainTextReport rpt, IInvoice data)
        {
            rpt.Write("RETURNS", 8, 0, Alignment.Center, TextAdjustment.Wrap);
            rpt.NewLine();
            bool gstSelection = Convert.ToBoolean(data.Instance.Gstselect);
            decimal? totReturnAmount = 0;
            decimal? totReturnDiscount = 0;
            decimal? ReturnDiscount = 0;
            decimal? ReturnAmount = 0;
            int len = 0;
            decimal? rtnGSTTotalValue = 0;

            foreach (var items in data.SalesReturnItem)
            {
                if (items.ReturnedQuantity > 0)
                {
                    rpt.NewLine();
                    len++;

                    rpt.Write(string.Format("{0:0}", len), 3, 1, Alignment.Right);

                    rpt.Write(items.ProductStock.Product.Name.ToUpper(), 18, 5);
                    rpt.Write(items.ProductStock.BatchNo, 6, true, Alignment.Right);
                    rpt.Write(string.Format("{0:MM/yy}", items.ProductStock.ExpireDate), 5, true);
                    rpt.Write(string.Format("{0:0}", items.ReturnedQuantity), 4, true, Alignment.Right);

                    decimal? getGstPercent = items.GstTotal + 100;
                    decimal? SalesItemDiscount = ((items.Discount / 100) * items.Total);
                    totReturnAmount += items.ReturnAmt;
                    ReturnAmount += items.ReturnAmt;
                    if (items.SellingPrice > 0)
                    {
                        decimal? vatPrice = ((items.SellingPrice * 100) / (items.ProductStock.VAT + 100));
                        rpt.Write(string.Format("{0:0.00}", items.SellingPrice), 6, true, Alignment.Right);
                        if (data.SalesReturn.TaxRefNo == 1 && !gstSelection)
                            rpt.Write(string.Format("{0:0.00}", items.ProductStock.GstTotal), 6, true, Alignment.Right);
                        else if (!gstSelection)
                            rpt.Write(string.Format("{0:0.00}", items.ProductStock.VAT), 5, true, Alignment.Right);
                        else
                            rpt.Write(" ", 2, true, Alignment.Right);
                    }
                    else
                    {
                        decimal? vatPrice = items.ProductStock.SellingPrice - items.ProductStock.VatInPrice;
                        rpt.Write(string.Format("{0:0.00}", items.ProductStock.SellingPrice), 6, true, Alignment.Right);
                        if (data.SalesReturn.TaxRefNo == 1 && !gstSelection)
                            rpt.Write(string.Format("{0:0.00}", items.ProductStock.GstTotal), 6, true, Alignment.Right);
                        else if (!gstSelection)
                            rpt.Write(string.Format("{0:0.00}", items.ProductStock.VAT), 5, true, Alignment.Right);
                    }

                    rpt.Write(string.Format("{0:0.00}", ((items.SellingPrice == null ? items.ProductStock.SellingPrice : items.SellingPrice) * items.ReturnedQuantity)), 7, true, Alignment.Right);
                    rpt.Write();

                    var ActualValue = (items.Total - SalesItemDiscount);
                    rtnGSTTotalValue += ActualValue - ((ActualValue * 100) / getGstPercent);
                    totReturnDiscount = ((items.Discount / 100) * items.Total);
                    totReturnAmount = totReturnAmount - totReturnDiscount;
                    ReturnDiscount += totReturnDiscount;
                }
            }
            rpt.NewLine();
            rpt.Write();
            rpt.Drawline(1);

            decimal? totValue = data.InvoiceItems.Sum(x => x.Total);
            rpt.NewLine();

            decimal? totCgstValue = data.InvoiceItems.Sum(x => x.CgstValue);
            decimal? totSgstValue = data.InvoiceItems.Sum(x => x.SgstValue);

            decimal? RtntotgstValue = rtnGSTTotalValue / 2;
            //decimal? RtntotCgstValue = data.SalesReturnItem.Sum(x => x.CgstValue);
            //decimal? RtntotSgstValue = data.SalesReturnItem.Sum(x => x.SgstValue);

            if (data.CgstTotalValue != 0 && data.SgstTotalValue != 0 && !gstSelection)
            {
                rpt.Write("CGST: " + string.Format("{0:0.00}", (totCgstValue - RtntotgstValue)), 15, 2);
                rpt.Write((data.Instance.isUnionTerritory == true ? "UTGST: " : "SGST: ") + string.Format("{0:0.00}", (totSgstValue - RtntotgstValue)), 15, 18);
            }
            rpt.Write("Total: " + string.Format("{0:0.00}", (totValue - ReturnAmount)), 18, 42, Alignment.Right); //items.Total

            rpt.NewLine();            
            rpt.Write("Disc.: " + string.Format("{0:0.00}", Math.Round((decimal)(data.DiscountInValue - ReturnDiscount), 2)), 15, 45, Alignment.Right);
            
            rpt.NewLine();
            decimal retAmount = data.RoundoffNetAmount as decimal? ?? 0; //RoundOffCalculation(data.Net, totReturnAmount);
            rpt.Write("Rupees: " + ((decimal)(data.NetAmount)).ConvertNumbertoWords(), 40, 2, TextAdjustment.Wrap);
            rpt.Write("R.Off: " + string.Format("{0:0.00}", retAmount), 15, 45, Alignment.Right);

            rpt.NewLine();
            rpt.Write("RETURN AMT: " + string.Format("{0:0.00}", totReturnAmount), 18, 42, Alignment.Right, data.IsBold);

            rpt.Write();
            rpt.Drawline(1);
            rpt.NewLine();
            //rpt.Write("PAYABLE AMOUNT: " 
            //    + string.Format("{0:0.00}", Math.Round(((decimal)data.Net - (decimal)totReturnAmount) + retAmount)), 28, 32, Alignment.Right, data.IsBold);   
            rpt.Write("PAYABLE AMOUNT: "
               + string.Format("{0:0.00}", data.NetAmount), 28, 32, Alignment.Right, data.IsBold);
            if (gstSelection)
            {
                rpt.Write();
                rpt.Drawline(1);
                GenerateGstSelected(rpt);
            }
        }
        #endregion

        #region A6_WithHeader_Generic _template

        protected string GenerateSalesReport_A6_WithHeader(IInvoice data)
        {
            PlainTextReport rpt = new PlainTextReport();
            bool gstSelection = Convert.ToBoolean(data.Instance.Gstselect);
            rpt.Columns = 49;
            rpt.Rows = 30;

            //some leading blank lines
            rpt.NewLine();
            rpt.NewLine();

            WriteReportHeader_A6_WithHeader(rpt, data);

            if (data.InvoiceItems.Count() == 0)
            {
                if (data.TaxRefNo == 1)
                    WriteReportOnlyReturnItems_A6_WithHeader_GST(rpt, data);
                else
                {
                    WriteReportOnlyReturnItems_A6_WithHeader(rpt, data);
                }
            }
            else
            {
                WriteReportItems_A6_WithHeader(rpt, data);
                if (data.TaxRefNo == 1)
                    WriteReportFooter_A6_WithHeader_GST(rpt, data);
                else
                    WriteReportFooter_A6_WithHeader(rpt, data);
            }

            //some trailing blank lines
            //rpt.NewLine();
            //rpt.NewLine();
            rpt.Write();

            return rpt.Content.ToString();
        }

        private void WriteReportHeader_A6_WithHeader(PlainTextReport rpt, IInvoice data)
        {
            //rpt.WriteLine();

            bool gstSelection = Convert.ToBoolean(data.Instance.Gstselect);
            //Write header
            rpt.NewLine();
            //rpt.Write("#BB#" + data.Instance.Name.ToUpper()+ "#BE#", 27, 10, Alignment.Center);
            rpt.Write(data.Instance.Name.ToUpper(), 27, 10, Alignment.Center, data.IsBold);

            rpt.NewLine();
            rpt.Write(data.Instance.Address + " " + data.Instance.Area, 27, 10, Alignment.Center);

            rpt.NewLine();
            rpt.Write(data.Instance.City + " " + data.Instance.State + "-" + data.Instance.Pincode, 30, 10);
            rpt.NewLine();
            rpt.Write("Ph - " + data.Instance.Phone, 30, 10, Alignment.Center);

            rpt.NewLine();
            rpt.Write("DL No : " + data.Instance.DrugLicenseNo, 40, 2);

            rpt.NewLine();
            if (data.TaxRefNo == 1)
            {
                rpt.Write("GSTIN No: " + data.Instance.GsTinNo, 28, 2);
            }
            else
            {
                rpt.Write("TIN   : " + data.Instance.TinNo, 30, 2);
            }

            if (gstSelection)
            {
                rpt.NewLine();
                rpt.Write("Bill of Supply", 44, 1, Alignment.Center, TextAdjustment.Wrap);
            }

            rpt.NewLine();
            rpt.Drawline(1);

            //rpt.NewLine();
            rpt.Write("Doctor  : " + data.DoctorName.ToUpper(), 30, 2);

            if (data.InvoiceItems.Count() == 0)
            {
                rpt.Write(" R No : " + data.InvoiceSeries + data.InvoiceNo, 18, 30);
            }
            else
            {
                rpt.Write(" B No: " + data.InvoiceSeries + data.InvoiceNo, 18, 30);
            }
            //   rpt.Write("Bill No: " + data.InvoiceSeries + " " + data.InvoiceNo, 18, 31);

            rpt.NewLine();
            rpt.Write("Patient : " + data.Name.ToUpper(), 30, 2);
            rpt.Write("Date : " + string.Format("{0:dd/MM/yy}", data.InvoiceDate), 18, 30);


            rpt.NewLine();
            rpt.Write("Address : " + data.Address, 30, 2);
            rpt.Write("Type : " + data.PaymentType, 18, 30);

            if (data.Address.Length > 18)
            {
                rpt.NewLine();
                rpt.Write("" + data.Address.Substring(18, data.Address.Length - 18), 31, 2);
                rpt.Write(" " + "", 18, 31);
            }

            rpt.NewLine();
            rpt.Write("City    : " + data.Patient.City, 30, 2);
            //rpt.Write("ID No. : ", 18, 30);
            rpt.Write("Time : " + Convert.ToDateTime(data.SalesCreatedAt).ToString("hh:mm tt"), 18, 30);

            rpt.Write();
            rpt.Drawline(1);

            rpt.NewLine();
            if (data.TaxRefNo == 1 && !gstSelection)
            {
                rpt.Write("  Particulars", 27);
                rpt.Write("GST%", 8, true, Alignment.Right);
            }

            rpt.NewLine();
            rpt.Write(" Batch", 10, true);
            rpt.Write("Exp", 7, true);
            rpt.Write("Qty", 5, true, Alignment.Right);
            //rpt.Write("Price", 6, true);
            rpt.Write("MRP", 8, true, Alignment.Right);
            //rpt.Write("VAT", 3, true);
            rpt.Write("Amount", 9, true, Alignment.Right);
            rpt.Write();
            rpt.Drawline(1);
        }

        private void WriteReportItems_A6_WithHeader(PlainTextReport rpt, IInvoice data)
        {
            int len = 0;
            bool gstSelection = Convert.ToBoolean(data.Instance.Gstselect);
            foreach (var items in data.InvoiceItems)
            {
                if (items.Quantity <= 0)
                { break; }
                rpt.NewLine();
                if (data.TaxRefNo == 1)
                {
                    var Gst = "";
                    if (items.Igst == 0 || items.Igst == null)
                        Gst = (items.Cgst + items.Sgst).ToString();
                    else
                        Gst = items.Igst.ToString();

                    rpt.Write("  " + items.ProductStock.Product.Name.ToUpper(), 27);
                    if (!gstSelection)
                        rpt.Write(Gst, 8, true, Alignment.Right);
                }
                rpt.NewLine();
                rpt.Write(" " + items.ProductStock.BatchNo, 10, true);
                rpt.Write(string.Format("{0:MM/yy}", items.ProductStock.ExpireDate), 7, true);
                rpt.Write(string.Format("{0:0}", items.Quantity), 5, true, Alignment.Right);

                // For Price
                if (items.SellingPrice > 0)
                {
                    //decimal? vatPrice = ((items.SellingPrice * 100) / (items.ProductStock.VAT + 100));                   
                    rpt.Write(string.Format("{0:0.00}", items.SellingPrice), 8, true, Alignment.Right);
                }
                else
                {
                    rpt.Write(string.Format("{0:0.00}", items.ProductStock.SellingPrice), 8, true, Alignment.Right);
                }

                //For MRP
                // rpt.Write(string.Format("{0:0.00}", items.ProductStock.SellingPrice), 8, true, Alignment.Right);
                rpt.Write(string.Format("{0:0.00}", items.Total), 9, true, Alignment.Right);
                rpt.Write();
                len++;
            }

            for (int i = 0; i < 5 - len; i++)
                rpt.NewLine();

            rpt.Write();
            rpt.Drawline(1);
        }

        private void WriteReportFooter_A6_WithHeader(PlainTextReport rpt, IInvoice data)
        {
            rpt.NewLine();
            rpt.Write("Total:" + string.Format("{0:0.00}", data.PurchasedTotal), 12, 2);
            rpt.Write("Vat:" + string.Format("{0:0.00}", data.Vat), 12, 20);
            rpt.Write(" Dis:" + string.Format("{0:0.00}", data.DiscountInValue), 12, 33, Alignment.Right);
            rpt.NewLine();
            if (data.SalesReturnItem.Count == 0)
            {
                rpt.Write(" R.Off:" + string.Format("{0:0.00}", data.RoundoffNetAmount), 12, 2);
                rpt.Write("Net:" + string.Format("{0:0.00}", data.NetAmount), 12, 33, Alignment.Right, data.IsBold);
            }           
            else
                rpt.Write("Net:" + string.Format("{0:0.00}", data.Net), 12, 33, Alignment.Right, data.IsBold);
            
            rpt.NewLine();

            if (data.SalesReturnItem.Count > 0)
            {
                WriteReportReturnedItems_A6_WithHeader(rpt, data);
            }
            rpt.NewLine();

            rpt.Write("User:" + data.HQueUser.Name, 48, 2, Alignment.Left);
            rpt.Write();

            //Newly added for Print Footer by Mani on 28-02-2017
            GenerateFooter(rpt, 47, data.PrintFooterNote);
        }

        private void WriteReportFooter_A6_WithHeader_GST(PlainTextReport rpt, IInvoice data)
        {
            bool gstSelection = Convert.ToBoolean(data.Instance.Gstselect);

            if (data.SalesReturnItem.Count == 0)
            {
                rpt.NewLine();  
                if(!gstSelection)
                {
                    rpt.Write("CGST:" + string.Format("{0:0.00}", Math.Round((decimal)data.CgstTotalValue, 2)), 12, 1);
                    rpt.Write((data.Instance.isUnionTerritory == true ? "UTGST:" : "SGST:") + string.Format("{0:0.00}", Math.Round((decimal)data.SgstTotalValue, 2)), 12, 15);
                }           
               
                rpt.Write("Total:" + string.Format("{0:0.00}", data.PurchasedTotal), 12, 33, Alignment.Right);

                rpt.NewLine();
                rpt.Write(" Dis:" + string.Format("{0:0.00}", data.DiscountInValue), 12, 33, Alignment.Right);
                rpt.NewLine();
                rpt.Write(" R.Off:" + string.Format("{0:0.00}", data.RoundoffNetAmount), 12, 33, Alignment.Right);
                rpt.NewLine();
                rpt.Write("Net:" + string.Format("{0:0.00}", data.NetAmount), 12, 33, Alignment.Right, data.IsBold);

                rpt.NewLine();
                rpt.Drawline(1);
            }
            if (data.SalesReturnItem.Count > 0)
            {
                WriteReportReturnedItems_A6_WithHeader_GST(rpt, data);
            }
            rpt.NewLine();

            rpt.Write("User:" + data.HQueUser.Name, 48, 2, Alignment.Left);
            if (gstSelection)
            {
                GenerateGstSelected(rpt);
            }
            rpt.Write();

            //Newly added for Print Footer by Mani on 28-02-2017
            GenerateFooter(rpt, 47, data.PrintFooterNote);
        }
        private void WriteReportReturnedItems_A6_WithHeader(PlainTextReport rpt, IInvoice data)
        {
            rpt.Drawline(1);
            rpt.Write("RETURNS", 8, 0, Alignment.Center, TextAdjustment.Wrap);

            decimal? totReturnAmount = 0;
            decimal? totReturnDiscount = 0;
            decimal? ReturnDiscount = 0;
            int len = 0;

            foreach (var items in data.SalesReturnItem)
            {
                if (items.ReturnedQuantity > 0)
                {
                    rpt.NewLine();
                    rpt.Write("  " + items.ProductStock.Product.Name.ToUpper(), 27);
                    rpt.NewLine();
                    rpt.Write(" " + items.ProductStock.BatchNo, 10, true);
                    rpt.Write(string.Format("{0:MM/yy}", items.ProductStock.ExpireDate), 7, true);
                    rpt.Write(string.Format("{0:0}", items.ReturnedQuantity), 5, true, Alignment.Right);

                    if (items.SellingPrice > 0)
                    {
                        rpt.Write(string.Format("{0:0.00}", items.SellingPrice), 8, true, Alignment.Right);
                    }
                    else
                    {
                        rpt.Write(string.Format("{0:0.00}", items.ProductStock.SellingPrice), 8, true, Alignment.Right);
                    }

                    rpt.Write(string.Format("{0:0.00}", (items.SellingPrice == null ? items.ProductStock.SellingPrice : items.SellingPrice) * (items.ReturnedQuantity)), 9, true, Alignment.Right);
                    rpt.Write();
                    len++;

                    totReturnAmount = totReturnAmount + (items.SellingPrice == null ? items.ProductStock.SellingPrice : items.SellingPrice) * (items.ReturnedQuantity);
                    totReturnDiscount = ((items.Discount / 100) * items.Total);
                    totReturnAmount = totReturnAmount - totReturnDiscount;
                    ReturnDiscount += totReturnDiscount;
                }
            }
            rpt.NewLine();
            rpt.NewLine();

            rpt.Write("RETURN AMT: " + string.Format("{0:0.00}", totReturnAmount), 20, 25, Alignment.Right, data.IsBold);

            rpt.Write();
            rpt.Drawline(1);

            rpt.NewLine();
            decimal retAmount = data.RoundoffNetAmount as decimal? ?? 0; //RoundOffCalculation(data.Net, totReturnAmount);
            rpt.Write("R.Off: " + string.Format("{0:0.00}", retAmount), 15, 1, Alignment.Left);
            if (ReturnDiscount != 0)
                rpt.Write("Disc.: -" + string.Format("{0:0.00}", ReturnDiscount), 15, 17);
            else
                rpt.Write("Disc.: " + string.Format("{0:0.00}", ReturnDiscount), 15, 17);
            rpt.NewLine();
            //rpt.Write("PAYABLE AMOUNT: " + string.Format("{0:0.00}", Math.Round(((decimal)data.Net - (decimal)totReturnAmount) + retAmount)), 28, 17, Alignment.Right, data.IsBold);
            rpt.Write("PAYABLE AMOUNT: " + string.Format("{0:0.00}", data.NetAmount), 28, 17, Alignment.Right, data.IsBold);

            rpt.Write();
            rpt.Drawline(1);
        }

        private void WriteReportReturnedItems_A6_WithHeader_GST(PlainTextReport rpt, IInvoice data)
        {
            rpt.Write("RETURNS", 8, 0, Alignment.Center, TextAdjustment.Wrap);
            bool gstSelection = Convert.ToBoolean(data.Instance.Gstselect);

            decimal? totReturnAmount = 0;
            decimal? totReturnDiscount = 0;
            decimal? ReturnDiscount = 0;
            int len = 0;
            decimal? rtnGSTTotalValue = 0;

            foreach (var items in data.SalesReturnItem)
            {
                if (items.ReturnedQuantity > 0)
                {
                    rpt.NewLine();
                    //rpt.Write("  " + items.ProductStock.Product.Name.ToUpper(), 27);
                    if (data.TaxRefNo == 1)
                    {
                        var Gst = "";
                        if (items.Igst == 0 || items.Igst == null)
                            Gst = (items.Cgst + items.Sgst).ToString();
                        else
                            Gst = items.Igst.ToString();

                        rpt.Write("  " + items.ProductStock.Product.Name.ToUpper(), 27);
                        if (!gstSelection)
                            rpt.Write(Gst, 8, true, Alignment.Right);
                    }
                    rpt.NewLine();
                    rpt.Write(" " + items.ProductStock.BatchNo, 10, true);
                    rpt.Write(string.Format("{0:MM/yy}", items.ProductStock.ExpireDate), 7, true);
                    rpt.Write(string.Format("{0:0}", items.ReturnedQuantity), 5, true, Alignment.Right);

                    if (items.SellingPrice > 0)
                    {
                        rpt.Write(string.Format("{0:0.00}", items.SellingPrice), 8, true, Alignment.Right);
                    }
                    else
                    {
                        rpt.Write(string.Format("{0:0.00}", items.ProductStock.SellingPrice), 8, true, Alignment.Right);
                    }

                    rpt.Write(string.Format("{0:0.00}", (items.SellingPrice == null ? items.ProductStock.SellingPrice : items.SellingPrice) * (items.ReturnedQuantity)), 9, true, Alignment.Right);
                    rpt.Write();
                    len++;
                    decimal? getGstPercent = items.GstTotal + 100;
                    decimal? SalesItemDiscount = ((items.Discount / 100) * items.Total);
                    var ActualValue = (items.Total - SalesItemDiscount);
                    rtnGSTTotalValue += ActualValue - ((ActualValue * 100) / getGstPercent);
                    
                    totReturnAmount = totReturnAmount + (items.SellingPrice == null ? items.ProductStock.SellingPrice : items.SellingPrice) * (items.ReturnedQuantity);
                    totReturnDiscount = ((items.Discount / 100) * items.Total);
                    totReturnAmount = totReturnAmount - totReturnDiscount;
                    ReturnDiscount += totReturnDiscount;
                }
            }
            rpt.NewLine();
            rpt.NewLine();
            rpt.Drawline(1);
            decimal? totValue = data.InvoiceItems.Sum(x => x.Total);
            decimal? totCgstValue = data.InvoiceItems.Sum(x => x.CgstValue);
            decimal? totSgstValue = data.InvoiceItems.Sum(x => x.SgstValue);

            decimal? RtntotgstValue = rtnGSTTotalValue / 2;

            //rpt.NewLine();
            decimal retAmount = data.RoundoffNetAmount as decimal? ?? 0; //RoundOffCalculation(data.Net, totReturnAmount);
            if (!gstSelection)
            {
                rpt.Write("CGST: " + string.Format("{0:0.00}", (totCgstValue - RtntotgstValue)), 12, 1);
                rpt.Write((data.Instance.isUnionTerritory == true ? "UTGST: " : "SGST: ") + string.Format("{0:0.00}", (totSgstValue - RtntotgstValue)), 12, 15);
            }
            rpt.Write("TOTAL: " + string.Format("{0:0.00}", (totValue - (totReturnAmount + ReturnDiscount))), 15, 30, Alignment.Right);
            rpt.NewLine();
            rpt.Write("Disc.: " + string.Format("{0:0.00}", (data.TotalDiscount - ReturnDiscount)), 15, 30, Alignment.Right);

            rpt.NewLine();
            string payAmountInWords = ((decimal)(data.NetAmount)).ConvertNumbertoWords();
            rpt.Write("Rupees " + payAmountInWords, 40, 2, TextAdjustment.Wrap);
            rpt.Write("R.Off: " + string.Format("{0:0.00}", retAmount), 15, 30, Alignment.Right);
            
            rpt.NewLine();
            rpt.Write("RETURN AMT: " + string.Format("{0:0.00}", totReturnAmount), 20, 25, Alignment.Right, data.IsBold);

            rpt.Write();
            rpt.Drawline(1);

            //rpt.Write("PAYABLE AMOUNT: " + string.Format("{0:0.00}", Math.Round(((decimal)data.Net - (decimal)totReturnAmount) + retAmount)), 28, 17, Alignment.Right, data.IsBold);
            rpt.Write("PAYABLE AMOUNT: " + string.Format("{0:0.00}",data.NetAmount), 28, 17, Alignment.Right, data.IsBold);

            rpt.Write();
            rpt.Drawline(1);
        }
        #endregion

        #region A5_Vat_WithHeader_Generic _template

        protected string GenerateSalesReport_A5_Vat_WithHeader(IInvoice data)
        {
            bool gstSelection = Convert.ToBoolean(data.Instance.Gstselect);
            PlainTextReport rpt = new PlainTextReport();

            rpt.Columns = 63;
            rpt.Rows = 30;
            rpt.NewLine();
            WriteReportHeader_A5_Vat_WithHeader(rpt, data);           

            if (data.InvoiceItems.Count() == 0)
            {
                if (data.TaxRefNo == 1)
                    WriteReportOnlyReturnItems_A5_Vat_WithHeader_GST(rpt, data);
                else
                {
                    WriteReportOnlyReturnItems_A5_Vat_WithHeader(rpt, data);
                }
            }
            else
            {
                WriteReportItems_A5_Vat_WithHeader(rpt, data);
                if (data.TaxRefNo == 1 )
                    WriteReportFooter_A5_GST_WithHeader(rpt, data);
                else
                    WriteReportFooter_A5_Vat_WithHeader(rpt, data);
            }
            //some trailing blank lines           
            rpt.NewLine();
            rpt.NewLine();
            rpt.Write();


            return rpt.Content.ToString();
        }

        protected string GenerateSalesReport_A5_Vat_WithHeader_RackNo(IInvoice data)
        {
            PlainTextReport rpt = new PlainTextReport();

            rpt.Columns = 63;
            rpt.Rows = 30;

            WriteReportHeader_A5_Vat_WithHeader_RackNo(rpt, data);

            if (data.InvoiceItems.Count() == 0)
            {
                if (data.TaxRefNo == 1)
                    WriteReportOnlyReturnItems_A5_Vat_WithHeader_RackNo_GST(rpt, data);                
                else
                    WriteReportOnlyReturnItems_A5_Vat_WithHeader_RackNo(rpt, data);

            }
            else
            {
                WriteReportItems_A5_Vat_WithHeader_RackNo(rpt, data);
                WriteReportFooter_A5_GST_WithHeader_RackNo(rpt, data);
            }
            rpt.NewLine();
            rpt.NewLine();
            rpt.Write();


            return rpt.Content.ToString();
        }

        #endregion A5_Vat_WithHeader_Generic _template

        #region A5_Without_Vat_RackNo
        protected string GenerateSalesReport_A5_Without_Vat_RackNo(IInvoice data)
        {
            PlainTextReport rpt = new PlainTextReport();

            rpt.Columns = 63;
            rpt.Rows = 30;

            WriteReportHeader_A5_Without_Vat_With_RackNo(rpt, data);

            if (data.InvoiceItems.Count() == 0)
            {
                if (data.TaxRefNo == 1)
                    WriteReportOnlyReturnItems_A5_Without_Vat_With_RackNo_GST(rpt, data);
                else
                    WriteReportOnlyReturnItems_A5_Without_Vat_With_RackNo(rpt, data);
            }
            else
            {
                WriteReportItems_A5_Without_Vat_With_RackNo(rpt, data);
                WriteReportFooter_A5_GST_Without_Vat_With_RackNo(rpt, data);
            }
            rpt.NewLine();
            rpt.NewLine();
            rpt.Write();

            return rpt.Content.ToString();
        }

        private void WriteReportHeader_A5_Without_Vat_With_RackNo(PlainTextReport rpt, IInvoice data)
        {
            rpt.NewLine();
            bool gstSelection = Convert.ToBoolean(data.Instance.Gstselect);
            rpt.Write(data.Instance.Name.ToUpper(), 60, 0, Alignment.Center, TextAdjustment.Wrap, data.IsBold);
            string adddressarea = string.Empty;
            if (!string.IsNullOrEmpty(data.Instance.Address))
            {
                adddressarea += data.Instance.Address;
            }
            if (!string.IsNullOrEmpty(data.Instance.Area))
            {
                adddressarea += "," + data.Instance.Area;
            }
            if (!string.IsNullOrEmpty(adddressarea))
            {
                rpt.NewLine();
                rpt.Write(adddressarea.ToUpper(), 60, 0, Alignment.Center, TextAdjustment.Wrap);
            }

            string cityState = string.Empty;
            if (!string.IsNullOrEmpty(data.Instance.City))
            {
                cityState += data.Instance.City + ",";
            }
            if (!string.IsNullOrEmpty(data.Instance.State))
            {
                cityState += data.Instance.State + ",";
            }
            if (!string.IsNullOrEmpty(data.Instance.Pincode))
            {
                cityState += data.Instance.Pincode;
            }

            if (!string.IsNullOrEmpty(cityState))
            {
                rpt.NewLine();
                rpt.Write(cityState.ToUpper(), 60, 0, Alignment.Center, TextAdjustment.Wrap);
            }

            string contact = string.Empty;

            if (!string.IsNullOrEmpty(data.Instance.Phone))
            {
                contact += "Phone No: " + data.Instance.Phone;
            }
            if (!string.IsNullOrEmpty(data.Instance.Mobile))
            {
                contact += "," + "Mobile No: " + data.Instance.Mobile;
            }
            if (!string.IsNullOrEmpty(contact))
            {
                rpt.NewLine();
                rpt.Write(contact.ToUpper(), 60, 0, Alignment.Center, TextAdjustment.Wrap);
            }

            rpt.NewLine();
            rpt.Write("DL No : " + data.Instance.DrugLicenseNo, 60, 2, Alignment.Center, TextAdjustment.Wrap);

            rpt.NewLine();
            if (data.TaxRefNo == 1)
                rpt.Write("GSTIN No:" + data.Instance.GsTinNo, 60, 2, Alignment.Center, TextAdjustment.Wrap);
            else
                rpt.Write("TIN   : " + data.Instance.TinNo, 60, 2, Alignment.Center, TextAdjustment.Wrap);
            //rpt.Write("CST   : ", 18, 41);
            if (gstSelection)
            {
                rpt.NewLine();
                rpt.Write("Bill of Supply", 44, 8, Alignment.Center, TextAdjustment.Wrap);
            }

            rpt.NewLine();
            rpt.Drawline(2);

            rpt.Write("Doctor  : " + data.DoctorName.ToUpper(), 40, 2);

            if (data.InvoiceItems.Count() == 0)
            {
                rpt.Write("Return No: " + data.InvoiceSeries + data.InvoiceNo, 18, 41);
            }
            else
            {
                rpt.Write("Bill No: " + data.InvoiceSeries + data.InvoiceNo, 18, 41);
            }

            //  rpt.Write("Bill No: " + data.InvoiceSeries + " " + data.InvoiceNo, 18, 31);

            rpt.NewLine();
            rpt.Write("Patient : " + data.Name.ToUpper(), 40, 2);
            rpt.Write("B Date : " + string.Format("{0:dd/MM/yy}", data.InvoiceDate), 18, 41);

            rpt.NewLine();
            rpt.Write("Address : " + data.Address, 40, 2);
            // rpt.Write("B Type : " + data.PaymentType, 18, 41);
            rpt.Write("Time   : " + Convert.ToDateTime(data.SalesCreatedAt).ToString("hh:mm tt"), 18, 41);

            if (data.Address.Length > 30)
            {
                rpt.NewLine();
                rpt.Write("" + data.Address.Substring(29, data.Address.Length - 29), 41, 2);
                rpt.Write(" " + "", 18, 41);
            }

            rpt.NewLine();
            rpt.Write("City    : " + data.Patient.City, 40, 2);
            // rpt.Write("ID No. : ", 18, 41);
            rpt.Write("B Type : " + data.PaymentType, 18, 41);


            rpt.NewLine();
            rpt.Write("Pincode : " + data.Patient.Pincode, 40, 2, TextAdjustment.NewLine);
            rpt.Write("ID No. : " + Convert.ToString(data.Patient.EmpID), 18, 41);

            rpt.Write();
            rpt.Drawline(2);

            rpt.NewLine();

            rpt.Write("Particulars", 16, 2);
            rpt.Write("RackNo", 6, 19);

            rpt.Write("Batch", 6, 27);
            rpt.Write("Exp", 5, 34);
            rpt.Write("Qty", 4, 41, Alignment.Right);
            rpt.Write("MRP", 6, 46, Alignment.Right);
            rpt.Write("Amount", 8, 52, Alignment.Right);
            rpt.Write();
            rpt.Drawline(2);
        }

        private void WriteReportOnlyReturnItems_A5_Without_Vat_With_RackNo_GST(PlainTextReport rpt, IInvoice data)
        {
            bool gstSelection = Convert.ToBoolean(data.Instance.Gstselect);
            decimal? totReturnAmount = 0;
            decimal? totReturnDiscount = 0;
            decimal? ReturnDiscount = 0;
            decimal? TotDiscountPer = 0;
            int len = 0;

            foreach (var items in data.SalesReturnItem)
            {
                if (items.ReturnedQuantity > 0)
                {
                    rpt.NewLine();

                    rpt.Write(items.ProductStock.Product.Name.ToUpper(), 16, 2);

                    if (items.ProductStock.RackNo.ToString() == "")
                        rpt.Write("  -  ", 4, 18); // Only return RankNo
                    else
                        rpt.Write(items.ProductStock.RackNo.ToString(), 4, 18);

                    rpt.Write(items.ProductStock.BatchNo, 7, true);
                    rpt.Write(string.Format("{0:MM/yy}", items.ProductStock.ExpireDate), 6, true);
                    rpt.Write(string.Format("{0:0}", items.ReturnedQuantity), 4, true, Alignment.Right);
                    decimal? SellingAmount = 0;

                    if (items.SellingPrice > 0)
                    {
                        rpt.Write(string.Format("{0:0.00}", items.SellingPrice), 7, true, Alignment.Right);
                        SellingAmount = (items.ReturnedQuantity * items.SellingPrice);
                    }
                    else
                    {
                        rpt.Write(string.Format("{0:0.00}", items.ProductStock.SellingPrice), 7, true, Alignment.Right);
                        SellingAmount = items.ReturnedQuantity * items.ProductStock.SellingPrice;
                    }

                    totReturnAmount += items.ReturnAmt;
                    rpt.Write(string.Format("{0:0.00}", SellingAmount), 8, true, Alignment.Right);
                    rpt.Write();

                    len++;
                    totReturnDiscount = ((items.Discount / 100) * items.OnlyReturnAmount);
                    totReturnAmount = totReturnAmount - totReturnDiscount;
                    ReturnDiscount += totReturnDiscount;

                    TotDiscountPer = TotDiscountPer + items.Discount;

                    items.IgstValue = ((items.OnlyReturnAmount - totReturnDiscount) - (((items.OnlyReturnAmount - totReturnDiscount) * 100) / (items.Igst + 100)));
                    decimal? getGst = ((items.OnlyReturnAmount - totReturnDiscount) - (((items.OnlyReturnAmount - totReturnDiscount) * 100) / ((items.Cgst + items.Sgst) + 100)));
                    items.CgstValue = getGst / 2;
                    items.SgstValue = getGst / 2;
                }
            }
            rpt.NewLine();
            rpt.NewLine();

            rpt.Write();
            rpt.Drawline(1);

            rpt.NewLine();


            if (ReturnDiscount != 0)
            {
                rpt.Write("Disc." + Math.Round((decimal)TotDiscountPer, 2) + "%: " + " -" + string.Format("{0:0.00}", ReturnDiscount), 28, 31, Alignment.Right);
            }
            else
            {
                rpt.Write("Disc." + Math.Round((decimal)TotDiscountPer, 2) + "%: " + " -" + string.Format("{0:0.00}", ReturnDiscount), 28, 31, Alignment.Right);
            }
            rpt.Write("User Name: " + data.HQueUser.Name, 30, 2);
            decimal retAmount = data.RoundoffNetAmount as decimal? ?? 0; //RoundOffCalculation(data.Net, totReturnAmount);
            rpt.NewLine();
            if (data.SalesReturn.ReturnCharges > 0 && data.SalesReturn.ReturnCharges != null)
            {
                //data.InWords = ((long)(Math.Round(((decimal)data.Net - (decimal)totReturnAmount + retAmount)) * (-1)- (decimal)data.SalesReturn.ReturnCharges)).ConvertNumbertoWords();
                data.InWords = ((decimal)(data.NetAmount * (-1) - (decimal)data.SalesReturn.ReturnCharges)).ConvertNumbertoWords();
            }
            else
            {
                //data.InWords = ((long)(Math.Round((decimal)data.Net - (decimal)totReturnAmount + retAmount)) * (-1)).ConvertNumbertoWords();
                data.InWords = ((decimal)(data.NetAmount) * (-1)).ConvertNumbertoWords();
            }
            rpt.Write("Rupees " + data.InWords + " Only", 30, 2, TextAdjustment.Wrap);
            rpt.Write("R.Off: " + string.Format("{0:0.00}", retAmount), 28, 31, Alignment.Right);
            if (data.SalesReturn.ReturnCharges > 0 && data.SalesReturn.ReturnCharges != null)
            {
                rpt.NewLine();
                rpt.Write("Ret. Charges: " + string.Format("{0:0.00}", data.SalesReturn.ReturnCharges), 28, 31, Alignment.Right);
                rpt.NewLine();
                //rpt.Write("Return Amount: " + string.Format("{0:0.00}", Math.Round((((decimal)data.Net - (decimal)totReturnAmount + retAmount)) * (-1) - (decimal)data.SalesReturn.ReturnCharges)), 28, 31, Alignment.Right, data.IsBold);
                rpt.Write("Return Amount: " + string.Format("{0:0.00}", data.NetAmount * (-1) - (decimal)data.SalesReturn.ReturnCharges), 28, 31, Alignment.Right, data.IsBold);
            }
            else
            {
                rpt.NewLine();
                //rpt.Write("Return Amount: " + string.Format("{0:0.00}", Math.Round(((decimal)data.Net - (decimal)totReturnAmount + retAmount)) * (-1)), 28, 31, Alignment.Right, data.IsBold);
                rpt.Write("Return Amount: " + string.Format("{0:0.00}", data.NetAmount * (-1)), 28, 31, Alignment.Right, data.IsBold);
            }
            if (gstSelection)
            {
                rpt.Write();
                rpt.Drawline(1);
                GenerateGstSelected(rpt);
            }
        }

        private void WriteReportOnlyReturnItems_A5_Without_Vat_With_RackNo(PlainTextReport rpt, IInvoice data)
        {
            decimal? totReturnAmount = 0;
            decimal? totReturnDiscount = 0;
            decimal? ReturnDiscount = 0;
            decimal? TotDiscountPer = 0;
            int len = 0;

            foreach (var items in data.SalesReturnItem)
            {
                if (items.ReturnedQuantity > 0)
                {
                    rpt.NewLine();

                    rpt.Write(items.ProductStock.Product.Name.ToUpper(), 16, 2);

                    if (items.ProductStock.RackNo.ToString() == "")
                        rpt.Write("  -  ", 4, 18); // Only return RankNo
                    else
                        rpt.Write(items.ProductStock.RackNo.ToString(), 4, 18);

                    rpt.Write(items.ProductStock.BatchNo, 7, true);
                    rpt.Write(string.Format("{0:MM/yy}", items.ProductStock.ExpireDate), 6, true);
                    rpt.Write(string.Format("{0:0}", items.ReturnedQuantity), 4, true, Alignment.Right);
                    decimal? SellingAmount = 0;

                    if (items.SellingPrice > 0)
                    {
                        rpt.Write(string.Format("{0:0.00}", items.SellingPrice), 7, true, Alignment.Right);
                        SellingAmount = (items.ReturnedQuantity * items.SellingPrice);
                    }
                    else
                    {
                        rpt.Write(string.Format("{0:0.00}", items.ProductStock.SellingPrice), 7, true, Alignment.Right);
                        SellingAmount = items.ReturnedQuantity * items.ProductStock.SellingPrice;
                    }

                    totReturnAmount += items.ReturnAmt;
                    rpt.Write(string.Format("{0:0.00}", SellingAmount), 8, true, Alignment.Right);
                    rpt.Write();

                    len++;
                    totReturnDiscount = ((items.Discount / 100) * items.OnlyReturnAmount);
                    totReturnAmount = totReturnAmount - totReturnDiscount;
                    ReturnDiscount += totReturnDiscount;

                }
            }
            rpt.NewLine();
            rpt.NewLine();

            rpt.Write();
            rpt.Drawline(1);

            rpt.NewLine();

            decimal retAmount = data.RoundoffNetAmount as decimal? ?? 0; //RoundOffCalculation(data.Net, totReturnAmount);

            rpt.Write("R.Off: " + string.Format("{0:0.00}", retAmount), 28, 2, Alignment.Left);
            if (ReturnDiscount != 0)
            {
                rpt.Write("Disc." + Math.Round((decimal)TotDiscountPer, 2) + "%: " + " -" + string.Format("{0:0.00}", ReturnDiscount), 28, 31, Alignment.Right);
            }
            else
            {
                rpt.Write("Disc." + Math.Round((decimal)TotDiscountPer, 2) + "%: " + " -" + string.Format("{0:0.00}", ReturnDiscount), 28, 31, Alignment.Right);
            }

            rpt.Write("User Name: " + data.HQueUser.Name, 30, 2);
            rpt.NewLine();
            //data.InWords = ((long)(Math.Round((decimal)data.Net - (decimal)totReturnAmount + retAmount)) * (-1)).ConvertNumbertoWords();
            data.InWords = ((decimal)(data.NetAmount) * (-1)).ConvertNumbertoWords();
            rpt.Write("Rupees " + data.InWords + " Only", 30, 2, TextAdjustment.Wrap);
            //rpt.Write("RETURN AMOUNT: " + string.Format("{0:0.00}", Math.Round(((decimal)data.Net - (decimal)totReturnAmount + retAmount)) * (-1)), 28, 31, Alignment.Right, data.IsBold, true);
            //rpt.Write("Return Amount: " + string.Format("{0:0.00}", Math.Round(((decimal)data.Net - (decimal)totReturnAmount + retAmount)) * (-1)), 28, 31, Alignment.Right, data.IsBold);
            rpt.Write("Return Amount: " + string.Format("{0:0.00}", data.NetAmount * (-1)), 28, 31, Alignment.Right, data.IsBold);


        }

        private void WriteReportItems_A5_Without_Vat_With_RackNo(PlainTextReport rpt, IInvoice data)
        {
            int len = 0;
            foreach (var items in data.InvoiceItems)
            {
                if (items.Quantity <= 0)
                {
                    break;
                }
                rpt.NewLine();

                rpt.Write(items.ProductStock.Product.Name.ToUpper(), 16, 2);
                if (items.ProductStock.RackNo.ToString() == "")
                    rpt.Write("  -  ", 6, 19); // RankNo
                else
                    rpt.Write(items.ProductStock.RackNo.ToString(), 6, 19);

                rpt.Write(items.ProductStock.BatchNo, 6, 27);
                rpt.Write(string.Format("{0:MM/yy}", items.ProductStock.ExpireDate), 5, 34);
                rpt.Write(string.Format("{0:0}", items.Quantity), 4, 41, Alignment.Right);
                decimal? SellingAmount = 0;

                if (items.SellingPrice > 0)
                {
                    rpt.Write(string.Format("{0:0.00}", items.SellingPrice), 6, 46, Alignment.Right);
                    SellingAmount = (items.Quantity * items.SellingPrice);
                }
                else
                {
                    rpt.Write(string.Format("{0:0.00}", items.ProductStock.SellingPrice), 6, 46, Alignment.Right);
                    SellingAmount = items.Quantity * items.ProductStock.SellingPrice;
                }

                rpt.Write(string.Format("{0:0.00}", SellingAmount), 8, 52, Alignment.Right);
                rpt.Write();
                len++;
            }

            for (int i = 0; i < 5 - len; i++)
                rpt.NewLine();

            rpt.Write();
            rpt.Drawline(2);
        }

        private void WriteReportFooter_A5_GST_Without_Vat_With_RackNo(PlainTextReport rpt, IInvoice data)
        {
            bool gstSelection = Convert.ToBoolean(data.Instance.Gstselect);
            if (data.SalesReturnItem.Count == 0)
            {
                rpt.NewLine();

                rpt.Write("Total: " + string.Format("{0:0.00}", data.PurchasedTotal), 18, 41, Alignment.Right);

                rpt.NewLine();
                rpt.Write("User Name: " + data.HQueUser.Name, 30, 2);

                rpt.Write("Disc." + Math.Round((decimal)data.TotalDiscount, 2) + "%: " + " -" + string.Format("{0:0.00}", data.DiscountInValue), 20, 39, Alignment.Right);

                rpt.NewLine();
                rpt.Write("Rupees " + ((decimal)data.NetAmount).ConvertNumbertoWords() + " Only", 30, 2, TextAdjustment.Wrap);
                rpt.Write("R.Off: " + string.Format("{0:0.00}", data.RoundoffNetAmount), 28, 31, Alignment.Right);
                rpt.NewLine();
                //rpt.Write("Net Amt: " + string.Format("{0:0.00}", data.NetAmount), 18, 41, Alignment.Right, data.IsBold, true);
                rpt.Write("Net Amt: " + string.Format("{0:0.00}", data.NetAmount), 18, 41, Alignment.Right, data.IsBold);
                if (gstSelection)
                {
                    rpt.Write();
                    rpt.Drawline(1);
                    GenerateGstSelected(rpt);
                }
            }
            else
            {
                rpt.Write("RETURNS", 8, 0, Alignment.Center, TextAdjustment.Wrap);
                rpt.NewLine();

                decimal? totIgstValue = 0;
                decimal? totReturnAmount = 0;
                decimal? totReturnDiscount = 0;
                decimal? ReturnDiscountAmt = 0;
                decimal? ReturnDiscount = 0;
                decimal? returnAmt = 0;
                int len = 0;
                decimal? rtnGSTTotalValue = 0;
                foreach (var items in data.SalesReturnItem)
                {
                    if (items.ReturnedQty <= 0)
                    {
                        break;
                    }
                    rpt.NewLine();

                    rpt.Write(items.ProductStock.Product.Name.ToUpper(), 16, 2);
                    if (items.ProductStock.RackNo.ToString() == "")
                        rpt.Write("  -  ", 6, 19);
                    else
                        rpt.Write(items.ProductStock.RackNo.ToString(), 6, 19);

                    rpt.Write(items.ProductStock.BatchNo, 6, 27);
                    rpt.Write(string.Format("{0:MM/yy}", items.ProductStock.ExpireDate), 5, 34);
                    rpt.Write(string.Format("{0:0}", items.Quantity), 4, 41, Alignment.Right);
                    decimal? SellingAmount = 0;
                    totIgstValue = totIgstValue + items.IgstValue;
                    totReturnAmount += items.ReturnAmt;
                    returnAmt += items.ReturnAmt;
                    decimal? getGstPercent = items.GstTotal + 100;
                    decimal? SalesItemDiscount = ((items.Discount / 100) * items.Total);
                    if (items.SellingPrice > 0)
                    {
                        rpt.Write(string.Format("{0:0.00}", items.SellingPrice), 6, 46, Alignment.Right);
                        SellingAmount = (items.Quantity * items.SellingPrice);
                    }
                    else
                    {
                        rpt.Write(string.Format("{0:0.00}", items.ProductStock.SellingPrice), 6, 46, Alignment.Right);
                        SellingAmount = items.Quantity * items.ProductStock.SellingPrice;
                    }

                    rpt.Write(string.Format("{0:0.00}", SellingAmount), 8, 52, Alignment.Right);
                    rpt.Write();

                    var ActualValue = (items.Total - SalesItemDiscount);
                    rtnGSTTotalValue += ActualValue - ((ActualValue * 100) / getGstPercent);
                    totReturnDiscount = ((items.Discount / 100) * items.Total);
                    totReturnAmount = totReturnAmount - totReturnDiscount;
                    ReturnDiscountAmt += totReturnDiscount;
                    ReturnDiscount = ReturnDiscount + items.Discount;
                    len++;
                }

                rpt.Write();
                rpt.Drawline(2);
                rpt.NewLine();
                decimal? RtntotgstValue = rtnGSTTotalValue / 2;
                decimal? totValue = data.InvoiceItems.Sum(x => x.Total);

                rpt.Write("Total: " + string.Format("{0:0.00}", (totValue - returnAmt)), 18, 41, Alignment.Right);   //data.PurchasedTotal

                rpt.NewLine();
                rpt.Write("User Name: " + data.HQueUser.Name, 30, 2);
                rpt.Write("Disc." + Math.Round((decimal)(data.TotalDiscount - ReturnDiscount), 2) + "%: " + string.Format("{0:0.00}", (data.DiscountInValue - ReturnDiscountAmt)), 20, 39, Alignment.Right);

                rpt.NewLine();
                decimal retAmount = data.RoundoffNetAmount as decimal? ?? 0; //RoundOffCalculation(data.Net, totReturnAmount);
                //rpt.Write("Rupees " + ((long)(Math.Round((decimal)data.Net - (decimal)totReturnAmount + retAmount))).ConvertNumbertoWords() + " Only", 30, 2, TextAdjustment.Wrap);
                rpt.Write("Rupees " + ((decimal)(data.NetAmount)).ConvertNumbertoWords() + " Only", 30, 2, TextAdjustment.Wrap);
                rpt.Write("R.Off: " + string.Format("{0:0.00}", retAmount), 28, 31, Alignment.Right);
                rpt.NewLine();
                rpt.Write("Return Amt: " + string.Format("{0:0.00}", (decimal)totReturnAmount), 18, 41, Alignment.Right);

                rpt.Write();
                rpt.Drawline(2);
                rpt.NewLine();
                rpt.Write("Net Amt: " + string.Format("{0:0.00}", data.NetAmount), 18, 41, Alignment.Right, data.IsBold);
                if (gstSelection)
                {
                    rpt.Write();
                    rpt.Drawline(1);
                    GenerateGstSelected(rpt);
                }
            }
        }

        private void WriteReportHeader_A5_Vat_WithHeader(PlainTextReport rpt, IInvoice data)
        {
            bool gstSelection = Convert.ToBoolean(data.Instance.Gstselect);
            rpt.NewLine();

            //rpt.Write("#BB#" + data.Instance.Name.ToUpper()+ "#BE#", 60, 0, Alignment.Center, TextAdjustment.Wrap);
            rpt.Write(data.Instance.Name.ToUpper(), 60, 0, Alignment.Center, TextAdjustment.Wrap, data.IsBold);
            string adddressarea = string.Empty;
            if (!string.IsNullOrEmpty(data.Instance.Address))
            {
                adddressarea += data.Instance.Address;
            }
            if (!string.IsNullOrEmpty(data.Instance.Area))
            {
                adddressarea += "," + data.Instance.Area;
            }
            if (!string.IsNullOrEmpty(adddressarea))
            {
                rpt.NewLine();
                rpt.Write(adddressarea.ToUpper(), 60, 0, Alignment.Center, TextAdjustment.Wrap);
            }



            string cityState = string.Empty;
            if (!string.IsNullOrEmpty(data.Instance.City))
            {
                cityState += data.Instance.City + ",";
            }
            if (!string.IsNullOrEmpty(data.Instance.State))
            {
                cityState += data.Instance.State + ",";
            }
            if (!string.IsNullOrEmpty(data.Instance.Pincode))
            {
                cityState += data.Instance.Pincode;
            }

            if (!string.IsNullOrEmpty(cityState))
            {
                rpt.NewLine();
                rpt.Write(cityState.ToUpper(), 60, 0, Alignment.Center, TextAdjustment.Wrap);
            }



            string contact = string.Empty;

            if (!string.IsNullOrEmpty(data.Instance.Phone))
            {
                contact += "Phone No: " + data.Instance.Phone;
            }
            if (!string.IsNullOrEmpty(data.Instance.Mobile))
            {
                contact += "," + "Mobile No: " + data.Instance.Mobile;
            }
            if (!string.IsNullOrEmpty(contact))
            {
                rpt.NewLine();
                rpt.Write(contact.ToUpper(), 60, 0, Alignment.Center, TextAdjustment.Wrap);
            }

            rpt.NewLine();
            rpt.Write("DL No : " + data.Instance.DrugLicenseNo, 60, 2, Alignment.Center, TextAdjustment.Wrap);

            rpt.NewLine();
            if (data.TaxRefNo == 1)
            {
                rpt.Write("GSTIN No:" + data.Instance.GsTinNo, 60, 2, Alignment.Center, TextAdjustment.Wrap);
            }
            else
            {
                rpt.Write("TIN   : " + data.Instance.TinNo, 60, 2, Alignment.Center, TextAdjustment.Wrap);
            }

            if (gstSelection)
            {
                rpt.NewLine();
                rpt.Write("Bill of Supply", 44, 8, Alignment.Center, TextAdjustment.Wrap);
            }

            rpt.NewLine();
            rpt.Drawline(2);

            rpt.NewLine();
            rpt.Write("Doctor  : " + data.DoctorName.ToUpper(), 40, 2);

            if (data.InvoiceItems.Count() == 0)
            {
                rpt.Write("Return No: " + data.InvoiceSeries + data.InvoiceNo, 18, 41);
            }
            else
            {
                rpt.Write("Bill No: " + data.InvoiceSeries + data.InvoiceNo, 18, 41);
            }

            //  rpt.Write("Bill No: " + data.InvoiceSeries + " " + data.InvoiceNo, 18, 31);

            rpt.NewLine();
            rpt.Write("Patient : " + data.Name.ToUpper(), 40, 2);
            rpt.Write("B Date : " + string.Format("{0:dd/MM/yy}", data.InvoiceDate), 18, 41);

            rpt.NewLine();
            rpt.Write("Address : " + data.Address, 40, 2);
            // rpt.Write("B Type : " + data.PaymentType, 18, 41);
            rpt.Write("Time   : " + Convert.ToDateTime(data.SalesCreatedAt).ToString("hh:mm tt"), 18, 41);

            if (data.Address.Length > 30)
            {
                rpt.NewLine();
                rpt.Write("" + data.Address.Substring(29, data.Address.Length - 29), 41, 2);
                rpt.Write(" " + "", 18, 41);
            }

            rpt.NewLine();
            rpt.Write("City    : " + data.Patient.City, 40, 2);
            // rpt.Write("ID No. : ", 18, 41);
            rpt.Write("B Type : " + data.PaymentType, 18, 41);


            rpt.NewLine();
            rpt.Write("Pincode : " + data.Patient.Pincode, 40, 2, TextAdjustment.NewLine);
            rpt.Write("ID No. : " + Convert.ToString(data.Patient.EmpID), 18, 41);

            rpt.Write();
            rpt.Drawline(2);

            rpt.NewLine();
            //rpt.Write("Particulars", 20, 2);
            if (data.TaxRefNo == 1 && !gstSelection)
            {
                rpt.Write("Particulars", 16, 2);
                rpt.Write("GST%", 4, 17);
            }
            else
                rpt.Write("Particulars", 20, 2);
            rpt.Write("Batch", 6, true);
            rpt.Write("Exp", 5, true);
            rpt.Write("Qty", 5, true, Alignment.Right);
            rpt.Write("MRP", 6, true, Alignment.Right);
            //rpt.Write("VAT", 3, true);
            rpt.Write("Amount", 10, true, Alignment.Right);
            rpt.Write();
            rpt.Drawline(2);
        }

        private void WriteReportHeader_A5_Vat_WithHeader_RackNo(PlainTextReport rpt, IInvoice data) 
        {
            rpt.NewLine();
            bool gstSelection = Convert.ToBoolean(data.Instance.Gstselect);
            rpt.Write(data.Instance.Name.ToUpper(), 60, 0, Alignment.Center, TextAdjustment.Wrap, data.IsBold);
            string adddressarea = string.Empty;
            if (!string.IsNullOrEmpty(data.Instance.Address))
            {
                adddressarea += data.Instance.Address;
            }
            if (!string.IsNullOrEmpty(data.Instance.Area))
            {
                adddressarea += "," + data.Instance.Area;
            }
            if (!string.IsNullOrEmpty(adddressarea))
            {
                rpt.NewLine();
                rpt.Write(adddressarea.ToUpper(), 60, 0, Alignment.Center, TextAdjustment.Wrap);
            }

            string cityState = string.Empty;
            if (!string.IsNullOrEmpty(data.Instance.City))
            {
                cityState += data.Instance.City + ",";
            }
            if (!string.IsNullOrEmpty(data.Instance.State))
            {
                cityState += data.Instance.State + ",";
            }
            if (!string.IsNullOrEmpty(data.Instance.Pincode))
            {
                cityState += data.Instance.Pincode;
            }

            if (!string.IsNullOrEmpty(cityState))
            {
                rpt.NewLine();
                rpt.Write(cityState.ToUpper(), 60, 0, Alignment.Center, TextAdjustment.Wrap);
            }

            string contact = string.Empty;

            if (!string.IsNullOrEmpty(data.Instance.Phone))
            {
                contact += "Phone No: " + data.Instance.Phone;
            }
            if (!string.IsNullOrEmpty(data.Instance.Mobile))
            {
                contact += "," + "Mobile No: " + data.Instance.Mobile;
            }
            if (!string.IsNullOrEmpty(contact))
            {
                rpt.NewLine();
                rpt.Write(contact.ToUpper(), 60, 0, Alignment.Center, TextAdjustment.Wrap);
            }

            rpt.NewLine();
            rpt.Write("DL No : " + data.Instance.DrugLicenseNo, 60, 2, Alignment.Center, TextAdjustment.Wrap);

            rpt.NewLine();
            if (data.TaxRefNo == 1)
                rpt.Write("GSTIN No:" + data.Instance.GsTinNo, 60, 2, Alignment.Center, TextAdjustment.Wrap);
            else
                rpt.Write("TIN   : " + data.Instance.TinNo, 60, 2, Alignment.Center, TextAdjustment.Wrap);
            //rpt.Write("CST   : ", 18, 41);
            if (gstSelection)
            {
                rpt.NewLine();
                rpt.Write("Bill of Supply", 44, 8, Alignment.Center, TextAdjustment.Wrap);
            }
            rpt.NewLine();
            rpt.Drawline(2);

            rpt.Write("Doctor  : " + data.DoctorName.ToUpper(), 40, 2);

            if (data.InvoiceItems.Count() == 0)
            {
                rpt.Write("Return No: " + data.InvoiceSeries + data.InvoiceNo, 18, 41);
            }
            else
            {
                rpt.Write("Bill No: " + data.InvoiceSeries + data.InvoiceNo, 18, 41);
            }

            //  rpt.Write("Bill No: " + data.InvoiceSeries + " " + data.InvoiceNo, 18, 31);

            rpt.NewLine();
            rpt.Write("Patient : " + data.Name.ToUpper(), 40, 2);
            rpt.Write("B Date : " + string.Format("{0:dd/MM/yy}", data.InvoiceDate), 18, 41);

            rpt.NewLine();
            rpt.Write("Address : " + data.Address, 40, 2);
            // rpt.Write("B Type : " + data.PaymentType, 18, 41);
            rpt.Write("Time   : " + Convert.ToDateTime(data.SalesCreatedAt).ToString("hh:mm tt"), 18, 41);

            if (data.Address.Length > 30)
            {
                rpt.NewLine();
                rpt.Write("" + data.Address.Substring(29, data.Address.Length - 29), 41, 2);
                rpt.Write(" " + "", 18, 41);
            }

            rpt.NewLine();
            rpt.Write("City    : " + data.Patient.City, 40, 2);
            // rpt.Write("ID No. : ", 18, 41);
            rpt.Write("B Type : " + data.PaymentType, 18, 41);


            rpt.NewLine();
            rpt.Write("Pincode : " + data.Patient.Pincode, 40, 2, TextAdjustment.NewLine);
            rpt.Write("ID No. : " + Convert.ToString(data.Patient.EmpID), 18, 41);

            rpt.Write();
            rpt.Drawline(2);

            rpt.NewLine();
           
            rpt.Write("Particulars", 16, 2);
            rpt.Write("RackNo", 6, 19);

            rpt.Write("Batch", 6, 27);
            rpt.Write("Exp", 5, 34);
            rpt.Write("Qty", 4, 41, Alignment.Right);
            rpt.Write("MRP", 6, 46, Alignment.Right);            
            rpt.Write("Amount", 8, 52, Alignment.Right);
            rpt.Write();
            rpt.Drawline(2);
        }
        
        
        
        private void WriteReportItems_A5_Vat_WithHeader(PlainTextReport rpt, IInvoice data)
        {
            int len = 0;
            bool gstSelection = Convert.ToBoolean(data.Instance.Gstselect);
            foreach (var items in data.InvoiceItems)
            {
                if (items.Quantity <= 0)
                {
                    break;
                }
                rpt.NewLine();
                //rpt.Write(items.ProductStock.Product.Name.ToUpper(), 20, 2);
                if (data.TaxRefNo == 1 && !gstSelection)
                {
                    var gst = (items.Igst == 0 || items.Igst == null) ? (items.Cgst + items.Sgst) : items.Igst;

                    rpt.Write(items.ProductStock.Product.Name.ToUpper(), 14, 2);
                    rpt.Write(gst.ToString(), 5, 18);
                }
                else
                    rpt.Write(items.ProductStock.Product.Name.ToUpper(), 20, 2);
                rpt.Write(items.ProductStock.BatchNo, 5, 23);
                rpt.Write(string.Format("{0:MM/yy}", items.ProductStock.ExpireDate), 5, true);
                rpt.Write(string.Format("{0:0}", items.Quantity), 4, true, Alignment.Right);
                decimal? SellingAmount = 0;

                if (items.SellingPrice > 0)
                {
                    //rpt.Write(string.Format("{0:0.00}", ((items.SellingPrice * items.ProductStock.VAT) / 100)), 5, 67);
                    rpt.Write(string.Format("{0:0.00}", items.SellingPrice), 7, true, Alignment.Right);
                    //rpt.Write(string.Format("{0:0.00}%", items.ProductStock.VAT), 5, true, Alignment.Right);
                    SellingAmount = (items.Quantity * items.SellingPrice);
                }
                else
                {
                    rpt.Write(string.Format("{0:0.00}", items.ProductStock.SellingPrice), 7, true, Alignment.Right);
                    //rpt.Write(string.Format("{0:0.00}%", items.ProductStock.VAT), 5, true, Alignment.Right);
                    SellingAmount = items.Quantity * items.ProductStock.SellingPrice;
                }

                //rpt.Write(string.Format("{0:0.00}", items.ProductStock.VatInPrice), 6, true, Alignment.Right);
                //rpt.Write(string.Format("{0:0}%", items.ProductStock.VAT), 3, true, Alignment.Right);
                rpt.Write(string.Format("{0:0.00}", SellingAmount), 10, true, Alignment.Right);
                rpt.Write();
                len++;
            }

            for (int i = 0; i < 5 - len; i++)
                rpt.NewLine();

            rpt.Write();
            rpt.Drawline(2);
        }

        private void WriteReportItems_A5_Vat_WithHeader_RackNo(PlainTextReport rpt, IInvoice data)
        {
            int len = 0;
            foreach (var items in data.InvoiceItems)
            {
                if (items.Quantity <= 0)
                {
                    break;
                }
                rpt.NewLine();
                
                rpt.Write(items.ProductStock.Product.Name.ToUpper(), 16, 2);
                if (items.ProductStock.RackNo.ToString() == "")
                    rpt.Write("  -  ", 6, 19); // RankNo
               else
                    rpt.Write(items.ProductStock.RackNo.ToString(), 6, 19);

                rpt.Write(items.ProductStock.BatchNo, 6, 27);
                rpt.Write(string.Format("{0:MM/yy}", items.ProductStock.ExpireDate), 5, 34);
                rpt.Write(string.Format("{0:0}", items.Quantity), 4, 41, Alignment.Right);
                decimal? SellingAmount = 0;

                if (items.SellingPrice > 0)
                {                    
                    rpt.Write(string.Format("{0:0.00}", items.SellingPrice), 6, 46, Alignment.Right);                    
                    SellingAmount = (items.Quantity * items.SellingPrice);
                }
                else
                {
                    rpt.Write(string.Format("{0:0.00}", items.ProductStock.SellingPrice), 6, 46, Alignment.Right);                
                    SellingAmount = items.Quantity * items.ProductStock.SellingPrice;
                }
                
                rpt.Write(string.Format("{0:0.00}", SellingAmount), 8, 52, Alignment.Right);
                rpt.Write();
                len++;
            }

            for (int i = 0; i < 5 - len; i++)
                rpt.NewLine();

            rpt.Write();
            rpt.Drawline(2);
        }
        

        private void WriteReportFooter_A5_GST_WithHeader_RackNo(PlainTextReport rpt, IInvoice data)
        {
            bool gstSelection = Convert.ToBoolean(data.Instance.Gstselect);
            
            if (data.SalesReturnItem.Count == 0)
            {
                rpt.NewLine();
                if (data.TaxRefNo == 1 && !gstSelection)
                {
                    rpt.Write("CGST: " + string.Format("{0:0.00}", data.InvoiceItems.Sum(z => z.CgstValue)), 13, 2);
                    rpt.Write((data.Instance.isUnionTerritory == true ? "UTGST: " : "SGST: ") + string.Format("{0:0.00}", data.InvoiceItems.Sum(z => z.SgstValue)), 13, 15);
                    //if (data.CgstTotalValue != 0 && data.SgstTotalValue != 0)
                    //{
                    //    rpt.Write("CGST: " + string.Format("{0:0.00}", data.InvoiceItems.Sum(z => z.CgstValue)), 13, 2);
                    //    rpt.Write("SGST: " + string.Format("{0:0.00}", data.InvoiceItems.Sum(z => z.SgstValue)), 13, 15);
                    //}
                    //else
                    //{
                    //    rpt.Write("IGST: " + string.Format("{0:0.00}", data.InvoiceItems.Sum(z => z.IgstValue)), 13, 2);
                    //}
                }

                rpt.Write("Total: " + string.Format("{0:0.00}", data.PurchasedTotal), 18, 41, Alignment.Right);

                rpt.NewLine();
                rpt.Write("User Name: " + data.HQueUser.Name, 30, 2);

                rpt.Write("Disc." + Math.Round((decimal)data.TotalDiscount, 2) + "%: " + " -" + string.Format("{0:0.00}", data.DiscountInValue), 20, 39, Alignment.Right);

                rpt.NewLine();
                rpt.Write("Rupees " + ((decimal)((decimal)data.NetAmount)).ConvertNumbertoWords() + " Only", 30, 2, TextAdjustment.Wrap);
                rpt.Write("R.Off: " + string.Format("{0:0.00}", data.RoundoffNetAmount), 28, 31, Alignment.Right);

                rpt.NewLine();
                rpt.Write("Net Amt: " + string.Format("{0:0.00}", data.NetAmount), 18, 41, Alignment.Right, data.IsBold);
            }
            else
            {
                rpt.Write("RETURNS", 8, 0, Alignment.Center, TextAdjustment.Wrap);
                rpt.NewLine();

                decimal? totIgstValue = 0;
                decimal? totReturnAmount = 0;
                decimal? totReturnDiscount = 0;
                decimal? ReturnDiscountAmt = 0;
                decimal? ReturnDiscount = 0;
                decimal? returnAmt = 0;
                int len = 0;
                decimal? rtnGSTTotalValue = 0;
                foreach (var items in data.SalesReturnItem)
                {
                    if (items.ReturnedQty <= 0)
                    {
                        break;
                    }
                    rpt.NewLine();

                    rpt.Write(items.ProductStock.Product.Name.ToUpper(), 16, 2);
                    if (items.ProductStock.RackNo.ToString() == "")
                        rpt.Write("  -  ", 6, 19);
                    else
                        rpt.Write(items.ProductStock.RackNo.ToString(), 6, 19);

                    rpt.Write(items.ProductStock.BatchNo, 6, 27);
                    rpt.Write(string.Format("{0:MM/yy}", items.ProductStock.ExpireDate), 5, 34);
                    rpt.Write(string.Format("{0:0}", items.Quantity), 4, 41, Alignment.Right);
                    decimal? SellingAmount = 0;
                    totIgstValue = totIgstValue + items.IgstValue;
                    totReturnAmount += items.ReturnAmt;
                    returnAmt += items.ReturnAmt;
                    decimal? getGstPercent = items.GstTotal + 100;
                    decimal? SalesItemDiscount = ((items.Discount / 100) * items.Total);
                    if (items.SellingPrice > 0)
                    {
                        rpt.Write(string.Format("{0:0.00}", items.SellingPrice), 6, 46, Alignment.Right);
                        SellingAmount = (items.Quantity * items.SellingPrice);
                    }
                    else
                    {
                        rpt.Write(string.Format("{0:0.00}", items.ProductStock.SellingPrice), 6, 46, Alignment.Right);
                        SellingAmount = items.Quantity * items.ProductStock.SellingPrice;
                    }

                    rpt.Write(string.Format("{0:0.00}", SellingAmount), 8, 52, Alignment.Right);
                    rpt.Write();

                    var ActualValue = (items.Total - SalesItemDiscount);
                    rtnGSTTotalValue += ActualValue - ((ActualValue * 100) / getGstPercent);
                    totReturnDiscount = ((items.Discount / 100) * items.Total);
                    totReturnAmount = totReturnAmount - totReturnDiscount;
                    ReturnDiscountAmt += totReturnDiscount;
                    ReturnDiscount = ReturnDiscount + items.Discount;
                    len++;
                }
                
                rpt.Write();
                rpt.Drawline(2);
                rpt.NewLine();
                decimal? RtntotgstValue = rtnGSTTotalValue / 2;
                decimal? ItemCgstValue = data.InvoiceItems.Sum(x => x.CgstValue);
                decimal? ItemSgstValue = data.InvoiceItems.Sum(z => z.SgstValue);
                decimal? totValue = data.InvoiceItems.Sum(x => x.Total);
                if (!gstSelection)
                {
                    rpt.Write("CGST: " + string.Format("{0:0.00}", (ItemCgstValue - RtntotgstValue)), 13, 2);
                    rpt.Write((data.Instance.isUnionTerritory == true ? "UTGST: " : "SGST: ") + string.Format("{0:0.00}", (ItemSgstValue - RtntotgstValue)), 13, 15);
                }
              
                

                rpt.Write("Total: " + string.Format("{0:0.00}", (totValue - returnAmt)), 18, 41, Alignment.Right);   //data.PurchasedTotal

                rpt.NewLine();
                rpt.Write("User Name: " + data.HQueUser.Name, 30, 2);
                rpt.Write("Disc." + Math.Round((decimal)(data.TotalDiscount - ReturnDiscount), 2) + "%: " + string.Format("{0:0.00}", (data.DiscountInValue - ReturnDiscountAmt)), 20, 39, Alignment.Right);

                rpt.NewLine();
                decimal retAmount = data.RoundoffNetAmount as decimal? ?? 0; //RoundOffCalculation(data.Net, totReturnAmount);
                rpt.Write("Rupees " + ((decimal)(data.NetAmount)).ConvertNumbertoWords() + " Only", 30, 2, TextAdjustment.Wrap);
                rpt.Write("R.Off: " + string.Format("{0:0.00}", retAmount), 28, 31, Alignment.Right);
                rpt.NewLine();
                rpt.Write("RETURN AMT: " + string.Format("{0:0.00}", (decimal)totReturnAmount), 18, 41, Alignment.Right, data.IsBold);

                rpt.Write();
                rpt.Drawline(2);
                rpt.NewLine();
                rpt.Write("Net Amt: " + string.Format("{0:0.00}", data.NetAmount), 18, 41, Alignment.Right, data.IsBold);
            }

            if (gstSelection)
            {
                rpt.Write();
                rpt.Drawline(1);
                GenerateGstSelected(rpt);
            }
        }

        //        rpt.Write();
        //        rpt.Drawline(2);
        //        rpt.NewLine();
        //        rpt.Write("Net Amt: " + string.Format("{0:0.00}", ((decimal)data.Net - (decimal)totReturnAmount + retAmount)), 18, 41, Alignment.Right, data.IsBold);
        //    }
        //}

        private void WriteReportFooter_A5_Vat_WithHeader(PlainTextReport rpt, IInvoice data)
        {
            rpt.NewLine();
            rpt.Write("Total: " + string.Format("{0:0.00}", data.PurchasedTotal), 20, 2);
            rpt.Write(" Dis: " + string.Format("{0:0.00}", data.DiscountInValue), 18, 17);
            if (data.SalesReturnItem.Count == 0)
            {
                rpt.Write(" R.Off: " + string.Format("{0:0.00}", data.RoundoffNetAmount), 17, 27);
                rpt.Write("Net Amt: " + string.Format("{0:0.00}", data.NetAmount), 18, 41, Alignment.Right, data.IsBold);
            }
            else
            {
                rpt.Write("Net Amt: " + string.Format("{0:0.00}", data.Net), 18, 41, Alignment.Right, data.IsBold);
            }

            //rpt.Write(" R.Off: " + string.Format("{0:0.00}", data.RoundOff - data.Net), 17, 27);
            //rpt.Write(" R.Off: " + string.Format("{0:0.00}", data.RoundoffNetAmount), 17, 27);
            //rpt.Write("Net Amt: " + string.Format("{0:0.00}", data.RoundOff), 18, 41, Alignment.Right, data.IsBold);
            //rpt.Write("Net Amt: " + string.Format("{0:0.00}", data.Net+data.RoundoffNetAmount), 18, 41, Alignment.Right, data.isBold);
            rpt.Write();
            rpt.Drawline(1);

            if (data.SalesReturnItem.Count > 0)
            {
                string payableAmount = "";
                WriteReportReturnedItems_A5_Vat_WithHeader(rpt, data, out payableAmount);
                data.InWords = payableAmount + " Only";
                rpt.NewLine();
                rpt.NewLine();
            }

            //Newly added for Print Footer by Mani on 28-02-2017
            GenerateFooter(rpt, 50, data.PrintFooterNote, false);

            //rpt.Drawline(2);
        }

        private void WriteReportFooter_A5_GST_WithHeader(PlainTextReport rpt, IInvoice data) //Lawrence
        {
            bool gstSelection = Convert.ToBoolean(data.Instance.Gstselect);

            if (data.SalesReturnItem.Count == 0)
            {
                rpt.NewLine();
                 
                if (data.TaxRefNo == 1 && !gstSelection)
                {
                    rpt.Write("CGST: " + string.Format("{0:0.00}", Math.Round((decimal)data.CgstTotalValue, 2)), 13, 2);
                    rpt.Write((data.Instance.isUnionTerritory == true ? "UTGST: " : "SGST: ") + string.Format("{0:0.00}", Math.Round((decimal)data.SgstTotalValue, 2)), 13, 15);
                    //if (data.CgstTotalValue != 0 && data.SgstTotalValue != 0)
                    //{
                    //    rpt.Write("CGST: " + string.Format("{0:0.00}", Math.Round((decimal)data.CgstTotalValue, 2)), 13, 2);
                    //    rpt.Write("SGST: " + string.Format("{0:0.00}", Math.Round((decimal)data.SgstTotalValue, 2)), 13, 15);
                    //}
                    //else
                    //{
                    //    rpt.Write("IGST: " + string.Format("{0:0.00}", Math.Round((decimal)data.IgstTotalValue, 2)), 13, 2);
                    //}
                     
                }

                rpt.Write("Total: " + string.Format("{0:0.00}", data.PurchasedTotal), 18, 41, Alignment.Right);
                rpt.NewLine();
                rpt.Write(" Dis: " + string.Format("{0:0.00}", data.DiscountInValue), 18, 41, Alignment.Right);
                //if (data.SalesReturnItem.Count == 0)
                //{
                rpt.NewLine();
                rpt.Write(" R.Off: " + string.Format("{0:0.00}", data.RoundoffNetAmount), 18, 41, Alignment.Right);
                rpt.NewLine();
                rpt.Write("Net Amt: " + string.Format("{0:0.00}", data.NetAmount), 18, 41, Alignment.Right, data.IsBold);
                //}
                //else
                //{
                //    rpt.Write("Net Amt: " + string.Format("{0:0.00}", data.Net), 18, 41, Alignment.Right, data.IsBold);
                //}
                 
                rpt.Write();
                rpt.Drawline(1);
                if (gstSelection)
                {
                    GenerateGstSelected(rpt);
                }
            }
            if (data.SalesReturnItem.Count > 0)
            {
                string payableAmount = "";
                WriteReportReturnedItems_A5_Vat_WithHeader(rpt, data, out payableAmount);
                data.InWords = payableAmount + " Only";
                rpt.NewLine();
                if (gstSelection)
                {
                    rpt.Write();
                    rpt.Drawline(1);
                    GenerateGstSelected(rpt);
                }
            }
            //Newly added for Print Footer by Mani on 28-02-2017
            GenerateFooter(rpt, 50, data.PrintFooterNote, false);
        }

        private void WriteReportReturnedItems_A5_Vat_WithHeader(PlainTextReport rpt, IInvoice data, out string payableAmount) 
        {
            rpt.Write("RETURNS", 8, 0, Alignment.Center, TextAdjustment.Wrap);
            rpt.NewLine();
            bool gstSelection = Convert.ToBoolean(data.Instance.Gstselect);

            decimal? totReturnAmount = 0;
            decimal? totReturnDiscount = 0;
            decimal? ReturnDiscount = 0;
            decimal? returnAmt = 0;
            int len = 0;
            decimal? rtnGSTTotalValue = 0;

            foreach (var items in data.SalesReturnItem)
            {
                if (items.ReturnedQuantity > 0)
                {
                    rpt.NewLine();
                    if (data.TaxRefNo == 1)
                    {
                        var gst = (items.Igst == 0 || items.Igst == null) ? (items.Cgst + items.Sgst) : items.Igst;
                        rpt.Write(items.ProductStock.Product.Name.ToUpper(), 14, 2);
                        rpt.Write(gst.ToString(), 5, 18);
                    }
                    else
                        rpt.Write(items.ProductStock.Product.Name.ToUpper(), 20, 2);

                    //rpt.NewLine();
                    //rpt.Write(items.ProductStock.Product.Name.ToUpper(), 20, 2);
                    rpt.Write(items.ProductStock.BatchNo, 5, 23);//, true
                    rpt.Write(string.Format("{0:MM/yy}", items.ProductStock.ExpireDate), 5, true);
                    rpt.Write(string.Format("{0:0}", items.ReturnedQuantity), 4, true, Alignment.Right);
                    decimal? SellingAmount = 0;

                    if (items.SellingPrice > 0)
                    {
                        rpt.Write(string.Format("{0:0.00}", items.SellingPrice), 7, true, Alignment.Right);
                        SellingAmount = (items.ReturnedQuantity * items.SellingPrice);
                    }
                    else
                    {
                        rpt.Write(string.Format("{0:0.00}", items.ProductStock.SellingPrice), 7, true, Alignment.Right);
                        SellingAmount = items.ReturnedQuantity * items.ProductStock.SellingPrice;
                    }
                    decimal? getGstPercent = items.GstTotal + 100;
                    decimal? SalesItemDiscount = ((items.Discount / 100) * items.Total);
                    totReturnAmount += items.ReturnAmt;
                    returnAmt+= items.ReturnAmt;
                    rpt.Write(string.Format("{0:0.00}", SellingAmount), 10, true, Alignment.Right);
                    rpt.Write();

                    len++;
                    var ActualValue = (items.Total - SalesItemDiscount);
                    rtnGSTTotalValue += ActualValue - ((ActualValue * 100) / getGstPercent);
                    totReturnDiscount = ((items.Discount / 100) * items.Total);
                    totReturnAmount = totReturnAmount - totReturnDiscount;
                    ReturnDiscount += totReturnDiscount;
                }
            }
            rpt.NewLine();          
            rpt.Write();
            rpt.Drawline(1);
            rpt.NewLine();
            decimal? totValue = data.InvoiceItems.Sum(x => x.Total);
            decimal? totSgstValue = data.InvoiceItems.Sum(x => x.SgstValue);
            decimal? totCgstValue = data.InvoiceItems.Sum(x => x.CgstValue);
            decimal? RtntotgstValue = rtnGSTTotalValue / 2;

            if (!gstSelection)
            {
                rpt.Write("CGST: " + string.Format("{0:0.00}", (totSgstValue - RtntotgstValue)), 13, 1);
                rpt.Write((data.Instance.isUnionTerritory == true ? "UTGST: " : "SGST: ") + string.Format("{0:0.00}", (totCgstValue - RtntotgstValue)), 13, 14);
            }
            rpt.Write("TOTAL: " + string.Format("{0:0.00}", (totValue - returnAmt)), 18, 40, Alignment.Right);

            rpt.NewLine();
            decimal retAmount = data.RoundoffNetAmount as decimal? ?? 0; //RoundOffCalculation(data.Net, totReturnAmount);
            rpt.Write("R.Off: " + string.Format("{0:0.00}", retAmount), 13, 45, Alignment.Right);
            rpt.NewLine();
            rpt.Write("Disc.: " + string.Format("{0:0.00}", (data.DiscountInValue - ReturnDiscount)), 13, 45, Alignment.Right);

            rpt.NewLine();
            rpt.Write("RETURN AMT: " + string.Format("{0:0.00}", totReturnAmount), 18, 40, Alignment.Right, data.IsBold);

            rpt.Drawline(1);
            rpt.NewLine();
            //rpt.Write("PAYABLE AMOUNT: " + string.Format("{0:0.00}", Math.Round(((decimal)data.Net - (decimal)totReturnAmount + retAmount))), 28, 30, Alignment.Right, data.IsBold);
            rpt.Write("PAYABLE AMOUNT: " + string.Format("{0:0.00}",data.NetAmount), 28, 30, Alignment.Right, data.IsBold);

            payableAmount = ((decimal)(data.NetAmount)).ConvertNumbertoWords();
        }
        #endregion



        #region A5_Vat_EPSON_Generic _template

        protected string GenerateSalesReport_A5_Vat_EPSON(IInvoice data)
        {
            PlainTextReport rpt = new PlainTextReport();
            bool gstSelection = Convert.ToBoolean(data.Instance.Gstselect);
            rpt.Columns = 63;
            rpt.Rows = 30;

            //some leading blank lines
            //rpt.NewLine();
            //rpt.NewLine();

            WriteReportHeader_A5_Vat_EPSON(rpt, data);


            if (data.InvoiceItems.Count() == 0)
            {
                if (data.TaxRefNo == 1)
                    WriteReportOnlyReturnItems_A5_Vat_EPSON_GST(rpt, data);
                else
                {
                    WriteReportOnlyReturnItems_A5_Vat_EPSON(rpt, data);
                }
            }
            else
            {
                WriteReportItems_A5_Vat_EPSON(rpt, data);
                if (data.TaxRefNo == 1)
                {
                    WriteReportFooter_A5_GST_EPSON(rpt, data);
                }
                else
                    WriteReportFooter_A5_Vat_EPSON(rpt, data);
            }

            //some trailing blank lines           
            rpt.NewLine();
            //rpt.NewLine();
            //rpt.NewLine();
            //rpt.NewLine();
            //rpt.NewLine();
            //rpt.NewLine();

            rpt.Write();


            return rpt.Content.ToString();
        }

        private void WriteReportHeader_A5_Vat_EPSON(PlainTextReport rpt, IInvoice data)
        {

            //rpt.NewLine();
            bool gstSelection = Convert.ToBoolean(data.Instance.Gstselect);
            //rpt.Write("#BB#" + data.Instance.Name.ToUpper()+ "#BE#", 60, 0, Alignment.Center, TextAdjustment.Wrap);
            rpt.Write(data.Instance.Name.ToUpper(), 60, 0, Alignment.Center, TextAdjustment.Wrap, data.IsBold);
            string adddressarea = string.Empty;
            if (!string.IsNullOrEmpty(data.Instance.Address))
            {
                adddressarea += data.Instance.Address;
            }
            if (!string.IsNullOrEmpty(data.Instance.Area))
            {
                adddressarea += "," + data.Instance.Area;
            }
            if (!string.IsNullOrEmpty(adddressarea))
            {
                rpt.NewLine();
                rpt.Write(adddressarea.ToUpper(), 60, 0, Alignment.Center, TextAdjustment.Wrap);
            }



            string cityState = string.Empty;
            if (!string.IsNullOrEmpty(data.Instance.City))
            {
                cityState += data.Instance.City + ",";
            }
            if (!string.IsNullOrEmpty(data.Instance.State))
            {
                cityState += data.Instance.State + ",";
            }
            if (!string.IsNullOrEmpty(data.Instance.Pincode))
            {
                cityState += data.Instance.Pincode;
            }

            if (!string.IsNullOrEmpty(cityState))
            {
                rpt.NewLine();
                rpt.Write(cityState.ToUpper(), 60, 0, Alignment.Center, TextAdjustment.Wrap);
            }



            string contact = string.Empty;

            if (!string.IsNullOrEmpty(data.Instance.Phone))
            {
                contact += "Phone No: " + data.Instance.Phone;
            }
            if (!string.IsNullOrEmpty(data.Instance.Mobile))
            {
                contact += "," + "Mobile No: " + data.Instance.Mobile;
            }
            if (!string.IsNullOrEmpty(contact))
            {
                rpt.NewLine();
                rpt.Write(contact.ToUpper(), 60, 0, Alignment.Center, TextAdjustment.Wrap);
            }

            rpt.NewLine();
            rpt.Write("DL No : " + data.Instance.DrugLicenseNo, 60, 2, Alignment.Center, TextAdjustment.Wrap);

            rpt.NewLine();
            if (data.TaxRefNo == 1)
                rpt.Write("GSTIN No: " + data.Instance.GsTinNo, 60, 2, Alignment.Center, TextAdjustment.Wrap);
            else
                rpt.Write("TIN   : " + data.Instance.TinNo, 60, 2, Alignment.Center, TextAdjustment.Wrap);

            if (gstSelection)
            {
                rpt.NewLine();
                rpt.Write("Bill of Supply", 44, 8, Alignment.Center);
            }

            rpt.NewLine();
            rpt.Drawline(2);

            rpt.NewLine();
            rpt.Write("Doctor  : " + data.DoctorName.ToUpper(), 40, 2);


            if (data.InvoiceItems.Count() == 0)
            {
                rpt.Write("Return No: " + data.InvoiceSeries + data.InvoiceNo, 18, 41);
            }
            else
            {
                rpt.Write("Bill No: " + data.InvoiceSeries + data.InvoiceNo, 18, 41);
            }
            //  rpt.Write("Bill No: " + data.InvoiceSeries + " " + data.InvoiceNo, 18, 31);

            rpt.NewLine();
            rpt.Write("Patient : " + data.Name.ToUpper(), 40, 2);
            rpt.Write("B Date : " + string.Format("{0:dd/MM/yy}", data.InvoiceDate), 18, 41);


            rpt.NewLine();
            rpt.Write("Address : " + data.Address, 40, 2);
            rpt.Write("Time   : " + Convert.ToDateTime(data.SalesCreatedAt).ToString("hh:mm tt"), 18, 41);
            // rpt.Write("B Type : " + data.PaymentType, 18, 41);

            if (data.Address.Length > 30)
            {
                rpt.NewLine();
                rpt.Write("" + data.Address.Substring(29, data.Address.Length - 29), 41, 2);
                rpt.Write(" " + "", 18, 41);
            }

            rpt.NewLine();
            rpt.Write("City    : " + data.Patient.City, 40, 2);
            rpt.Write("B Type : " + data.PaymentType, 20, 41); //Altered by Sarubala on 01/11/17
            //  rpt.Write("ID No. : ", 18, 41);


            rpt.NewLine();
            rpt.Write("Pincode : " + data.Patient.Pincode, 40, 2, TextAdjustment.NewLine);
            rpt.Write("ID No. : " + Convert.ToString(data.Patient.EmpID), 18, 41);

            rpt.Write();
            rpt.Drawline(2);

            rpt.NewLine();
            //rpt.Write("Particulars", 20, 2);
            if (data.TaxRefNo == 1 && !gstSelection)
            {
                rpt.Write("Particulars", 16, 2);
                rpt.Write("GST%", 4, 17);
            }
            else
                rpt.Write("Particulars", 20, 2);
            rpt.Write("Batch", 7, true);
            rpt.Write("Exp", 5, true);
            rpt.Write("Qty", 4, true, Alignment.Right);
            rpt.Write("MRP", 7, true, Alignment.Right);
            //rpt.Write("VAT", 3, true);
            rpt.Write("Amount", 8, true, Alignment.Right);
            rpt.Write();
            rpt.Drawline(2);
        }

        private void WriteReportItems_A5_Vat_EPSON(PlainTextReport rpt, IInvoice data)
        {
            int len = 0;
            bool gstSelection = Convert.ToBoolean(data.Instance.Gstselect);
            foreach (var items in data.InvoiceItems)
            {
                if (items.Quantity <= 0)
                { break; }
                rpt.NewLine();
                //rpt.Write(items.ProductStock.Product.Name.ToUpper(), 20, 2);
                if (data.TaxRefNo == 1 && !gstSelection)
                {
                    var gst = (items.Igst == 0 || items.Igst == null) ? (items.Cgst + items.Sgst) : items.Igst;

                    rpt.Write(items.ProductStock.Product.Name.ToUpper(), 13, 2);
                    rpt.Write(gst.ToString(), 4, 17);
                    rpt.Write(items.ProductStock.BatchNo, 6, true);
                }
                else
                {
                    rpt.Write(items.ProductStock.Product.Name.ToUpper(), 20, 2);
                    rpt.Write(items.ProductStock.BatchNo, 8, true);
                }
                rpt.Write(string.Format("{0:MM/yy}", items.ProductStock.ExpireDate), 5, true);
                rpt.Write(string.Format("{0:0}", items.Quantity), 4, true, Alignment.Right);
                decimal? SellingAmount = 0;

                if (items.SellingPrice > 0)
                {
                    //rpt.Write(string.Format("{0:0.00}", ((items.SellingPrice * items.ProductStock.VAT) / 100)), 5, 67);
                    rpt.Write(string.Format("{0:0.00}", items.SellingPrice), 7, true, Alignment.Right);
                    //rpt.Write(string.Format("{0:0.00}%", items.ProductStock.VAT), 5, true, Alignment.Right);
                    SellingAmount = (items.Quantity * items.SellingPrice);
                }
                else
                {
                    rpt.Write(string.Format("{0:0.00}", items.ProductStock.SellingPrice), 7, true, Alignment.Right);
                    //rpt.Write(string.Format("{0:0.00}%", items.ProductStock.VAT), 5, true, Alignment.Right);
                    SellingAmount = items.Quantity * items.ProductStock.SellingPrice;
                }

                //rpt.Write(string.Format("{0:0.00}", items.ProductStock.VatInPrice), 6, true, Alignment.Right);
                //rpt.Write(string.Format("{0:0}%", items.ProductStock.VAT), 3, true, Alignment.Right);
                rpt.Write(string.Format("{0:0.00}", SellingAmount), 8, true, Alignment.Right);
                rpt.Write();
                len++;
            }

            for (int i = 0; i < 5 - len; i++)
                rpt.NewLine();

            rpt.Write();
            rpt.Drawline(2);
        }

        private void WriteReportFooter_A5_Vat_EPSON(PlainTextReport rpt, IInvoice data)
        {
            rpt.NewLine();
            rpt.Write("Total: " + string.Format("{0:0.00}", data.PurchasedTotal), 20, 2);
            rpt.Write(" Dis: " + string.Format("{0:0.00}", data.DiscountInValue), 18, 17);

            if (data.SalesReturnItem.Count == 0)
            {
                rpt.Write(" R.Off: " + string.Format("{0:0.00}", data.RoundoffNetAmount), 17, 27);
                rpt.Write("Net Amt: " + string.Format("{0:0.00}", data.NetAmount), 18, 41, Alignment.Right, data.IsBold);
            }
            else
            {
                rpt.Write("Net Amt: " + string.Format("{0:0.00}", data.Net), 18, 41, Alignment.Right, data.IsBold);
            }

            //rpt.Write(" R.Off: " + string.Format("{0:0.00}", data.RoundoffNetAmount), 17, 27);            
            //rpt.Write("Net Amt: " + string.Format("{0:0.00}", data.Net+data.RoundoffNetAmount), 18, 41, Alignment.Right, data.isBold);
            rpt.Write();

            if (data.SalesReturnItem.Count > 0)
            {
                string payableAmount = "";
                WriteReportReturnedItems_A5_Vat_EPSON(rpt, data, out payableAmount);
                data.InWords = payableAmount + " Only";
            }

            //Newly added for Print Footer by Mani on 28-02-2017
            GenerateFooter(rpt, 50, data.PrintFooterNote);
        }

        private void WriteReportFooter_A5_GST_EPSON(PlainTextReport rpt, IInvoice data)
        {
            bool gstSelection = Convert.ToBoolean(data.Instance.Gstselect);
            rpt.NewLine();
            if (data.SalesReturnItem.Count == 0 )
            {
                if (!gstSelection)
                {
                    rpt.Write("CGST: " + string.Format("{0:0.00}", Math.Round((decimal)data.CgstTotalValue, 2)), 13, 2);
                    rpt.Write((data.Instance.isUnionTerritory == true ? "UTGST: " : "SGST: ") + string.Format("{0:0.00}", Math.Round((decimal)data.SgstTotalValue, 2)), 13, 15);
                }
                    //if (data.CgstTotalValue != 0 && data.SgstTotalValue != 0)
                //{
                //    rpt.Write("CGST: " + string.Format("{0:0.00}", Math.Round((decimal)data.CgstTotalValue, 2)), 13, 2);
                //    rpt.Write("SGST: " + string.Format("{0:0.00}", Math.Round((decimal)data.SgstTotalValue, 2)), 13, 15);
                //}
                //else
                //{
                //    rpt.Write("IGST: " + string.Format("{0:0.00}", Math.Round((decimal)data.IgstTotalValue, 2)), 13, 2);
                //}



                rpt.Write("Total: " + string.Format("{0:0.00}", data.PurchasedTotal), 18, 41, Alignment.Right);
                rpt.NewLine();
                rpt.Write(" Dis: " + string.Format("{0:0.00}", data.DiscountInValue), 18, 41, Alignment.Right);
                rpt.NewLine();
                rpt.Write(" R.Off: " + string.Format("{0:0.00}", data.RoundoffNetAmount), 18, 41, Alignment.Right);
                rpt.NewLine();
                rpt.Write("Net Amt: " + string.Format("{0:0.00}", data.NetAmount), 18, 41, Alignment.Right, data.IsBold);
                
                //rpt.Write("Net Amt: " + string.Format("{0:0.00}", data.Net), 18, 41, Alignment.Right, data.IsBold);
                rpt.Write();
                if (gstSelection)
                {
                    rpt.Write();
                    rpt.Drawline(1);
                    GenerateGstSelected(rpt);
                }
            }
            else
            {
                WriteReportReturnedItems_A5_GST_EPSON(rpt, data);
                if (gstSelection)
                {                 
                    GenerateGstSelected(rpt);
                }
            }
            //Newly added for Print Footer by Mani on 28-02-2017
            GenerateFooter(rpt, 50, data.PrintFooterNote);
        }

        private void WriteReportReturnedItems_A5_Vat_EPSON(PlainTextReport rpt, IInvoice data, out string payableAmount)
        {
            rpt.Drawline(1);
            rpt.Write("RETURNS", 8, 0, Alignment.Center, TextAdjustment.Wrap);
            rpt.NewLine();

            decimal? totReturnAmount = 0;
            decimal? totReturnDiscount = 0;
            decimal? ReturnDiscount = 0;
            int len = 0;

            foreach (var items in data.SalesReturnItem)
            {
                if (items.ReturnedQuantity > 0)
                {
                    rpt.NewLine();
                    rpt.Write(items.ProductStock.Product.Name.ToUpper(), 20, 2);
                    rpt.Write(items.ProductStock.BatchNo, 8, true);
                    rpt.Write(string.Format("{0:MM/yy}", items.ProductStock.ExpireDate), 5, true);
                    rpt.Write(string.Format("{0:0}", items.ReturnedQuantity), 4, true, Alignment.Right);
                    decimal? SellingAmount = 0;

                    if (items.SellingPrice > 0)
                    {
                        rpt.Write(string.Format("{0:0.00}", items.SellingPrice), 7, true, Alignment.Right);
                        SellingAmount = (items.ReturnedQuantity * items.SellingPrice);
                    }
                    else
                    {
                        rpt.Write(string.Format("{0:0.00}", items.ProductStock.SellingPrice), 7, true, Alignment.Right);
                        SellingAmount = items.ReturnedQuantity * items.ProductStock.SellingPrice;
                    }

                    totReturnAmount += items.ReturnAmt;
                    rpt.Write(string.Format("{0:0.00}", SellingAmount), 8, true, Alignment.Right);
                    rpt.Write();

                    len++;

                    totReturnDiscount = ((items.Discount / 100) * items.Total);
                    totReturnAmount = totReturnAmount - totReturnDiscount;
                    ReturnDiscount += totReturnDiscount;
                }
            }
            rpt.NewLine();
            rpt.NewLine();

            rpt.Write("RETURN AMT: " + string.Format("{0:0.00}", totReturnAmount), 18, 41, Alignment.Right, data.IsBold);

            rpt.Write();
            rpt.Drawline(1);

            rpt.NewLine();
            decimal retAmount = data.RoundoffNetAmount as decimal? ?? 0; //RoundOffCalculation(data.Net, totReturnAmount);
            rpt.Write("R.Off: " + string.Format("{0:0.00}", retAmount), 15, 1, Alignment.Left);
            if (ReturnDiscount != 0)
                rpt.Write("Disc.: -" + string.Format("{0:0.00}", ReturnDiscount), 15, 17);
            else
                rpt.Write("Disc.: " + string.Format("{0:0.00}", ReturnDiscount), 15, 17);
            //rpt.NewLine();
            //rpt.Write("PAYABLE AMOUNT: " + string.Format("{0:0.00}", Math.Round(((decimal)data.Net - (decimal)totReturnAmount) + retAmount)), 28, 31, Alignment.Right, data.IsBold);
            rpt.Write("PAYABLE AMOUNT: " + string.Format("{0:0.00}", data.NetAmount), 28, 31, Alignment.Right, data.IsBold);
            payableAmount = ((decimal)(data.NetAmount)).ConvertNumbertoWords();
        }

        private void WriteReportReturnedItems_A5_GST_EPSON(PlainTextReport rpt, IInvoice data)
        {
            rpt.Write("RETURNS", 8, 0, Alignment.Center, TextAdjustment.Wrap);
            rpt.NewLine();
            bool gstSelection = Convert.ToBoolean(data.Instance.Gstselect);

            decimal? totReturnAmount = 0;
            decimal? totReturnDiscount = 0;
            decimal? ReturnDiscount = 0;
            decimal? returnAmt = 0;
            int len = 0;
            decimal? rtnGSTTotalValue = 0;

            foreach (var items in data.SalesReturnItem)
            {
                if (items.ReturnedQuantity > 0)
                {
                    rpt.NewLine();

                    var gst = (items.Igst == 0 || items.Igst == null) ? (items.Cgst + items.Sgst) : items.Igst;
                    rpt.Write(items.ProductStock.Product.Name.ToUpper(), 13, 2);
                    if (!gstSelection)
                        rpt.Write(gst.ToString(), 4, 17);
                    else
                        rpt.Write(" ", 4, 17);
                    rpt.Write(items.ProductStock.BatchNo, 6, true);

                    rpt.Write(string.Format("{0:MM/yy}", items.ProductStock.ExpireDate), 5, true);
                    rpt.Write(string.Format("{0:0}", items.ReturnedQuantity), 4, true, Alignment.Right);
                    decimal? SellingAmount = 0;

                    if (items.SellingPrice > 0)
                    {
                        if (!gstSelection)
                            rpt.Write(string.Format("{0:0.00}", items.SellingPrice), 7, true, Alignment.Right);
                        else
                            rpt.Write(string.Format("{0:0.00}", items.SellingPrice), 10, true, Alignment.Right);
                        SellingAmount = (items.ReturnedQuantity * items.SellingPrice);
                    }
                    else
                    {
                        rpt.Write(string.Format("{0:0.00}", items.ProductStock.SellingPrice), 7, true, Alignment.Right);
                        SellingAmount = items.ReturnedQuantity * items.ProductStock.SellingPrice;
                    }
                    decimal? getGstPercent = items.GstTotal + 100;
                    decimal? SalesItemDiscount = ((items.Discount / 100) * items.Total);
                    totReturnAmount += items.ReturnAmt;
                    returnAmt += items.ReturnAmt;
                    rpt.Write(string.Format("{0:0.00}", SellingAmount), 8, true, Alignment.Right);
                    rpt.Write();

                    len++;
                    var ActualValue = (items.Total - SalesItemDiscount);
                    rtnGSTTotalValue += ActualValue - ((ActualValue * 100) / getGstPercent);
                    totReturnDiscount = ((items.Discount / 100) * items.Total);
                    totReturnAmount = totReturnAmount - totReturnDiscount;
                    ReturnDiscount += totReturnDiscount;
                }
            }

            rpt.NewLine();
            rpt.Write();
            rpt.Drawline(1);
            rpt.NewLine();

            decimal? totValue = data.InvoiceItems.Sum(x => x.Total);
            decimal? totCgstValue = data.InvoiceItems.Sum(x => x.CgstValue);
            decimal? totSgstValue = data.InvoiceItems.Sum(x => x.SgstValue);
            decimal? RtntotgstValue = rtnGSTTotalValue / 2;

            if (!gstSelection)
            {
                rpt.Write("CGST:" + string.Format("{0:0.00}", Math.Round((decimal)(totCgstValue - RtntotgstValue), 2)), 12, 1);
                rpt.Write((data.Instance.isUnionTerritory == true ? "UTGST:" : "SGST:") + string.Format("{0:0.00}", Math.Round((decimal)(totSgstValue - RtntotgstValue), 2)), 12, 13);
            }

            rpt.Write("TOTAL: " + string.Format("{0:0.00}", (totValue - returnAmt)), 15, 43, Alignment.Right); //data.NetAmount 

            rpt.NewLine();
            decimal ROffAmount = data.RoundoffNetAmount as decimal? ?? 0; //RoundOffCalculation(data.Net, totReturnAmount);
            string payableInWords = ((decimal)(data.NetAmount)).ConvertNumbertoWords();
            rpt.Write("Rupees " + payableInWords, 40, 2, TextAdjustment.Wrap);
            rpt.Write("R.Off: " + string.Format("{0:0.00}", ROffAmount), 12, 46, Alignment.Right);

            rpt.NewLine();
            rpt.Write("Disc.: " + string.Format("{0:0.00}", (data.DiscountInValue - ReturnDiscount)), 12, 46, Alignment.Right);

            rpt.NewLine();
            rpt.Write("RETURN AMT " + string.Format("{0:0.00}", totReturnAmount), 18, 40, Alignment.Right, data.IsBold);

            rpt.NewLine();
            rpt.Drawline(1);            
            //rpt.Write("PAYABLE AMOUNT " + string.Format("{0:0.00}", Math.Round(((decimal)data.Net - (decimal)totReturnAmount) + ROffAmount)), 28, 30, Alignment.Right, data.IsBold);
            rpt.Write("PAYABLE AMOUNT " + string.Format("{0:0.00}",data.NetAmount), 28, 30, Alignment.Right, data.IsBold);

            rpt.Write();
            rpt.Drawline(1);
            rpt.NewLine();
            
            /*
            rpt.NewLine();
            rpt.NewLine();

            rpt.Write("RETURN AMT: " + string.Format("{0:0.00}", totReturnAmount), 18, 41, Alignment.Right, data.IsBold);

            rpt.Write();
            rpt.Drawline(1);

            rpt.NewLine();
            decimal retAmount = RoundOffCalculation(data.Net, totReturnAmount);
            rpt.Write("R.Off: " + string.Format("{0:0.00}", retAmount), 15, 1, Alignment.Left);
            if (ReturnDiscount != 0)
                rpt.Write("Disc.: -" + string.Format("{0:0.00}", ReturnDiscount), 15, 17);
            else
                rpt.Write("Disc.: " + string.Format("{0:0.00}", ReturnDiscount), 15, 17);

            rpt.Write("PAYABLE AMOUNT: " + string.Format("{0:0.00}", Math.Round(((decimal)data.Net - (decimal)totReturnAmount) + retAmount)), 28, 31, Alignment.Right, data.IsBold);
            */
        }
        #endregion

        //Newly created for Print Footer by Mani on 2017-03-01
        protected void GenerateFooter(PlainTextReport rpt, int width, string footer, bool extraLines = true)
        {
            if (!String.IsNullOrEmpty(footer) && footer != "undefined")
            {
                if (extraLines)
                {
                    rpt.NewLine();
                    rpt.NewLine();
                }
                rpt.Write(footer, width, 1, Alignment.Center);
                rpt.Write();


                rpt.NewLine();
            }
        }

        #region A4_NoHeader_Generic_template

        protected string GenerateSalesReport_A4_NoHeader(IInvoice data)
        {
            bool gstSelection = Convert.ToBoolean(data.Instance.Gstselect);
            PlainTextReport rpt = new PlainTextReport();

            rpt.Columns = 80;
            rpt.Rows = 40;

            WriteReportHeader_A4_NoHeader(rpt, data);
            WriteReportItems_A4_NoHeader(rpt, data);
            WriteReportFooter_A4_NoHeader(rpt, data);

            return rpt.Content.ToString();
        }

        private void WriteReportHeader_A4_NoHeader(PlainTextReport rpt, IInvoice data)
        {
            //Write header
            rpt.NewLine();
            rpt.Write("-", 5, 10);
            rpt.NewLine();
            rpt.Write();

            rpt.NewLine();
            rpt.Write(data.DoctorName.ToUpper(), 45, 15);
            rpt.Write(data.InvoiceSeries + data.InvoiceNo, 20, 60);
            // rpt.Write("Bill No: " + data.InvoiceSeries + " " + data.InvoiceNo, 18, 31);

            rpt.NewLine();
            rpt.NewLine();
            rpt.Write(data.Name.ToUpper(), 45, 15);
            rpt.Write(string.Format("{0:dd/MM/yyyy HH:mm:ss}", data.InvoiceDate), 20, 60);


            rpt.NewLine();
            rpt.NewLine();
            rpt.Write(data.Address, 45, 15);
            rpt.Write("", 20, 60);

            rpt.NewLine();
            rpt.Write();

        }

        private void WriteReportItems_A4_NoHeader(PlainTextReport rpt, IInvoice data)
        {
            int len = 0;
            foreach (var items in data.InvoiceItems)
            {
                if (items.Quantity <= 0)
                { break; }
                rpt.NewLine();
                rpt.Write(items.ProductStock.Product.Name.ToUpper(), 33);
                rpt.Write(items.ProductStock.BatchNo, 8, 36);
                rpt.Write(string.Format("{0:MM/yy}", items.ProductStock.ExpireDate), 8, 45);
                rpt.Write(string.Format("{0:0}", items.Quantity), 4, 55);

                if (items.SellingPrice > 0)
                {
                    rpt.Write(string.Format("{0:0.00}", (items.SellingPrice - ((items.SellingPrice * items.ProductStock.VAT) / 100))), 6, 59, Alignment.Right);
                    rpt.Write(string.Format("{0:0.00}%", items.ProductStock.VAT), 5, 67);
                }
                else
                {
                    rpt.Write(string.Format("{0:0.00}", (items.ProductStock.SellingPrice - items.ProductStock.VatInPrice)), 6, 59, Alignment.Right);
                    rpt.Write(string.Format("{0:0.00}%", items.ProductStock.VAT), 5, 67);
                }

                //rpt.Write(string.Format("{0:0.00}", items.ProductStock.VatInPrice), 6, 59, Alignment.Right);
                //rpt.Write(string.Format("{0:0.00}", (items.ProductStock.SellingPrice - items.ProductStock.VatInPrice)), 5, 67);
                rpt.Write(string.Format("{0:0.00}", items.Total), 8, 72, Alignment.Right);
                rpt.Write();
                len++;
            }

            for (int i = 0; i < 10 - len; i++)
                rpt.NewLine();

            rpt.Write();
            rpt.Drawline();
        }

        private void WriteReportFooter_A4_NoHeader(PlainTextReport rpt, IInvoice data)
        {
            rpt.NewLine();
            rpt.Write("Total: " + string.Format("{0:0.00}", data.PurchasedTotal), 20);
            rpt.Write("Disc: " + string.Format("{0:0.00}", data.DiscountInValue), 30, 30);
            if (data.SalesReturnItem.Count == 0)
            {
                rpt.Write("Round Off: " + string.Format("{0:0.00}", data.RoundoffNetAmount), 30, 43);
                rpt.Write("Net Amount: " + string.Format("{0:0.00}", data.NetAmount), 19, 60, Alignment.Right);
            }
            //rpt.Write("Round Off: " + string.Format("{0:0.00}", data.RoundoffNetAmount), 30, 43);
            //rpt.Write("Net Amount: #BB#" + string.Format("{0:0.00}", data.RoundOff)+ "#BE#", 19, 60, Alignment.Right);
            else
                rpt.Write("Net Amount: " + string.Format("{0:0.00}", data.Net), 19, 60, Alignment.Right);
            //rpt.Write("Net Amount: " + string.Format("{0:0.00}", data.Net+data.RoundoffNetAmount), 19, 60, Alignment.Right);
            rpt.Write();

            if (data.SalesReturnItem.Count > 0)
            {
                WriteReportReturnedItems_A4_NoHeader(rpt, data);
            }
            rpt.NewLine();

            //Newly added for Print Footer by Mani on 28-02-2017
            GenerateFooter(rpt, 70, data.PrintFooterNote);
        }

        private void WriteReportReturnedItems_A4_NoHeader(PlainTextReport rpt, IInvoice data)
        {
            rpt.Drawline(1);
            rpt.Write("RETURNS", 8, 0, Alignment.Center, TextAdjustment.Wrap);

            decimal? totReturnAmount = 0;
            decimal? totReturnDiscount = 0;
            decimal? ReturnDiscount = 0;
            int len = 0;

            foreach (var items in data.SalesReturnItem)
            {
                if (items.ReturnedQuantity > 0)
                {
                    rpt.NewLine();
                    rpt.Write(items.ProductStock.Product.Name.ToUpper(), 33);
                    rpt.Write(items.ProductStock.BatchNo, 8, 36);
                    rpt.Write(string.Format("{0:MM/yy}", items.ProductStock.ExpireDate), 8, 45);
                    rpt.Write(string.Format("{0:0}", items.ReturnedQuantity), 4, 55);

                    if (items.SellingPrice > 0)
                    {
                        rpt.Write(string.Format("{0:0.00}", (items.SellingPrice - ((items.SellingPrice * items.ProductStock.VAT) / 100))), 6, 59, Alignment.Right);
                        rpt.Write(string.Format("{0:0.00}%", items.ProductStock.VAT), 5, 67);
                    }
                    else
                    {
                        rpt.Write(string.Format("{0:0.00}", (items.ProductStock.SellingPrice - items.ProductStock.VatInPrice)), 6, 59, Alignment.Right);
                        rpt.Write(string.Format("{0:0.00}%", items.ProductStock.VAT), 5, 67);
                    }

                    rpt.Write(string.Format("{0:0.00}", (items.SellingPrice == null ? items.ProductStock.SellingPrice : items.SellingPrice) * (items.ReturnedQuantity)), 8, 72, Alignment.Right);
                    rpt.Write();
                    len++;

                    totReturnAmount = totReturnAmount + (items.SellingPrice == null ? items.ProductStock.SellingPrice : items.SellingPrice) * (items.ReturnedQuantity);
                    totReturnDiscount = ((items.Discount / 100) * items.Total);
                    totReturnAmount = totReturnAmount - totReturnDiscount;
                    ReturnDiscount += totReturnDiscount;
                }
            }
            rpt.NewLine();
            rpt.NewLine();

            rpt.Write("RETURN AMT: " + string.Format("{0:0.00}", totReturnAmount), 20, 59, Alignment.Right);

            rpt.Write();
            rpt.Drawline(1);

            rpt.NewLine();
            decimal retAmount = data.RoundoffNetAmount as decimal? ?? 0; //RoundOffCalculation(data.Net, totReturnAmount);
            rpt.Write("R.Off: " + string.Format("{0:0.00}", retAmount), 15, 1, Alignment.Left);
            if (ReturnDiscount != 0)
                rpt.Write("Disc.: -" + string.Format("{0:0.00}", ReturnDiscount), 15, 17);
            else
                rpt.Write("Disc.: " + string.Format("{0:0.00}", ReturnDiscount), 15, 17);
            //rpt.NewLine();
            //rpt.Write("PAYABLE AMOUNT: " + string.Format("{0:0.00}", Math.Round(((decimal)data.Net - (decimal)totReturnAmount) + retAmount)), 28, 51, Alignment.Right);
            rpt.Write("PAYABLE AMOUNT: " + string.Format("{0:0.00}", data.NetAmount), 28, 51, Alignment.Right);

            rpt.Write();
            rpt.Drawline(1);
        }

        #endregion

        #region A5_Vat_WithHeader_WithMFR

        protected string GenerateSalesReport_A5_Vat_WithMFR(IInvoice data)
        {
            PlainTextReport rpt = new PlainTextReport();
            bool gstSelection = Convert.ToBoolean(data.Instance.Gstselect);

            rpt.Columns = 75;
            rpt.Rows = 30;
            //rpt.Columns = 90;
            //rpt.Rows = 30;

            //some leading blank lines
            rpt.NewLine();
            rpt.NewLine();

            WriteReportHeader_A5_Vat_WithHeaderWithMFR(rpt, data);

            if (data.InvoiceItems.Count() == 0)
            {
                if (data.TaxRefNo == 1)
                    WriteReportOnlyReturnItems_A5_Vat_WithHeaderWithMFR_GST(rpt, data);
                else
                {
                    WriteReportOnlyReturnItems_A5_Vat_WithHeaderWithMFR(rpt, data);
                }
            }
            else
            {
                WriteReportItems_A5_Vat_WithHeaderWithMFR(rpt, data);
                if (data.TaxRefNo == 1)
                    WriteReportFooter_A5_GST_WithHeaderWithMFR(rpt, data);
                else
                    WriteReportFooter_A5_Vat_WithHeaderWithMFR(rpt, data); //WriteReportFooter_A5_Vat_WithHeaderWithMFR(rpt, data);
            }

            //some trailing blank lines
            rpt.NewLine();
            rpt.NewLine();
            rpt.Write();

            return rpt.Content.ToString();
        }

        private void WriteReportHeader_A5_Vat_WithHeaderWithMFR(PlainTextReport rpt, IInvoice data)
        {

            rpt.NewLine();
            bool gstSelection = Convert.ToBoolean(data.Instance.Gstselect);
            rpt.Write(data.Instance.Name.ToUpper(), 72, 0, Alignment.Center, TextAdjustment.Wrap, data.IsBold);
            string adddressarea = string.Empty;
            if (!string.IsNullOrEmpty(data.Instance.Address))
            {
                adddressarea += data.Instance.Address;
            }
            if (!string.IsNullOrEmpty(data.Instance.Area))
            {
                adddressarea += "," + data.Instance.Area;
            }
            if (!string.IsNullOrEmpty(adddressarea))
            {
                rpt.NewLine();
                rpt.Write(adddressarea.ToUpper(), 72, 0, Alignment.Center, TextAdjustment.Wrap);
            }
            string cityState = string.Empty;
            if (!string.IsNullOrEmpty(data.Instance.City))
            {
                cityState += data.Instance.City + ",";
            }
            if (!string.IsNullOrEmpty(data.Instance.State))
            {
                cityState += data.Instance.State + ",";
            }
            if (!string.IsNullOrEmpty(data.Instance.Pincode))
            {
                cityState += data.Instance.Pincode;
            }

            if (!string.IsNullOrEmpty(cityState))
            {
                rpt.NewLine();
                rpt.Write(cityState.ToUpper(), 72, 0, Alignment.Center, TextAdjustment.Wrap);
            }

            string contact = string.Empty;

            if (!string.IsNullOrEmpty(data.Instance.Phone))
            {
                contact += "Phone No: " + data.Instance.Phone;
            }
            if (!string.IsNullOrEmpty(data.Instance.Mobile))
            {
                contact += "," + "Mobile No: " + data.Instance.Mobile;
            }
            if (!string.IsNullOrEmpty(contact))
            {
                rpt.NewLine();
                //rpt.Write(contact.ToUpper(), 108, 0, Alignment.Center, TextAdjustment.Wrap);
                rpt.Write(contact.ToUpper(), 72, 0, Alignment.Center, TextAdjustment.Wrap);
            }

            rpt.NewLine();
            //rpt.Write("DL No : " + data.Instance.DrugLicenseNo, 108, 2, Alignment.Center, TextAdjustment.Wrap);
            rpt.Write("DL No : " + data.Instance.DrugLicenseNo, 72, 2, Alignment.Center, TextAdjustment.Wrap);

            rpt.NewLine();
            if (data.TaxRefNo == 1)
                rpt.Write("GSTIN No: " + data.Instance.GsTinNo, 72, 2, Alignment.Center, TextAdjustment.Wrap);
            else
                rpt.Write("TIN   : " + data.Instance.TinNo, 72, 2, Alignment.Center, TextAdjustment.Wrap);

            rpt.NewLine();
            if (gstSelection)
            {
                rpt.Write("Bill of Supply", 44, 12, Alignment.Center, TextAdjustment.Wrap);
                rpt.NewLine();
            }
            rpt.Drawline(2);

            rpt.NewLine();
            rpt.Write("Doctor  : " + data.DoctorName.ToUpper(), 50, 2);

            if (data.InvoiceItems.Count() == 0)
            {
                rpt.Write("Return No: " + data.InvoiceSeries + data.InvoiceNo, 18, 51);
            }
            else
            {
                rpt.Write("Bill No: " + data.InvoiceSeries + data.InvoiceNo, 18, 51);
            }

            rpt.NewLine();
            rpt.Write("Patient : " + data.Name.ToUpper(), 50, 2);
            rpt.Write("B Date : " + string.Format("{0:dd/MM/yy}", data.InvoiceDate), 18, 51);


            rpt.NewLine();
            rpt.Write("Address : " + data.Address, 50, 2);
            rpt.Write("Time   : " + Convert.ToDateTime(data.SalesCreatedAt).ToString("hh:mm tt"), 18, 51);

            if (data.Address.Length > 39)
            {
                rpt.NewLine();
                rpt.Write("" + data.Address.Substring(38, data.Address.Length - 38), 51, 2);
                rpt.Write(" " + "", 18, 51);
            }

            rpt.NewLine();

            rpt.Write("City    : " + data.Patient.City, 50, 2);
            rpt.Write("B Type : " + data.PaymentType, 18, 51);

            rpt.NewLine();
            rpt.Write("Pincode : " + data.Patient.Pincode, 50, 2, TextAdjustment.NewLine);
            rpt.Write("ID No. : " + Convert.ToString(data.Patient.EmpID), 18, 51);

            rpt.Write();
            rpt.Drawline(2);


            rpt.NewLine();

            rpt.Write("SNo", 3, 0);
            rpt.Write("Particulars", 18, true);
            rpt.Write("MFG", 4, true);
            rpt.Write("Qty", 4, true, Alignment.Right);
            rpt.Write("Batch", 8, true, Alignment.Right);
            rpt.Write("Exp", 5, true, Alignment.Right);
            rpt.Write("MRP", 6, true, Alignment.Right);
            if (data.TaxRefNo == 1 && !gstSelection)
                rpt.Write("GST%", 5, true, Alignment.Right);
            else
                rpt.Write(" ", 5, true, Alignment.Right);
            rpt.Write("Amount", 8, true, Alignment.Right);
            rpt.Write();
            rpt.Drawline(2);
        }
        private void WriteReportItems_A5_Vat_WithHeaderWithMFR(PlainTextReport rpt, IInvoice data)
        {
            int len = 0;
            bool gstSelection = Convert.ToBoolean(data.Instance.Gstselect);
            foreach (var items in data.InvoiceItems)
            {
                if (items.Quantity <= 0)
                { break; }
                rpt.NewLine();
                len++;

                rpt.Write(string.Format("{0:0}", len), 3, true, Alignment.Center);
                rpt.Write(items.ProductStock.Product.Name.ToUpper(), 18, true);

                if (string.IsNullOrEmpty(items.ProductStock.Product.Manufacturer))
                {
                    //rpt.Write("-", 6, true);
                    rpt.Write("-", 4, true);
                }
                else
                {
                    if (items.ProductStock.Product.Manufacturer.ToUpper().Length > 3)
                    {
                        //rpt.Write(items.ProductStock.Product.Manufacturer.ToUpper().Substring(0, 3), 6, true);
                        rpt.Write(items.ProductStock.Product.Manufacturer.ToUpper().Substring(0, 3), 4, true);
                    }
                    else
                    {
                        //rpt.Write(items.ProductStock.Product.Manufacturer.ToUpper(), 6, true);
                        rpt.Write(items.ProductStock.Product.Manufacturer.ToUpper(), 4, true);
                    }
                }

                rpt.Write(string.Format("{0:0}", items.Quantity), 4, true, Alignment.Right);
                rpt.Write(items.ProductStock.BatchNo, 6, true, Alignment.Right);

                rpt.Write(string.Format("{0:MM/yy}", items.ProductStock.ExpireDate), 5, true);

                decimal? SellingAmount = 0;

                if (items.SellingPrice > 0)
                {
                    //rpt.Write(string.Format("{0:0.00}", items.SellingPrice), 11, true, Alignment.Right);
                    rpt.Write(string.Format("{0:0.00}", items.SellingPrice), 6, true, Alignment.Right);
                    if (items.GstTotal != null && rpt.Columns > 65 && !gstSelection)
                        rpt.Write(string.Format("{0:0.00}", items.GstTotal), 7, true, Alignment.Right);
                    else if (rpt.Columns > 65)
                        rpt.Write(" ", 7, true, Alignment.Right);
                    else { }
                    SellingAmount = (items.Quantity * items.SellingPrice);
                }
                else
                {
                    //rpt.Write(string.Format("{0:0.00}", items.ProductStock.SellingPrice), 11, true, Alignment.Right);
                    rpt.Write(string.Format("{0:0.00}", items.ProductStock.SellingPrice), 6, true, Alignment.Right);
                    if (items.GstTotal != null && rpt.Columns > 65 && !gstSelection)
                        rpt.Write(string.Format("{0:0.00}", items.GstTotal), 7, true, Alignment.Right);
                    else
                        rpt.Write(" ", 7, true, Alignment.Right);
                    SellingAmount = items.Quantity * items.ProductStock.SellingPrice;
                }
                //rpt.Write(string.Format("{0:0.00}", SellingAmount), 12, true, Alignment.Right);
                rpt.Write(string.Format("{0:0.00}", SellingAmount), 7, true, Alignment.Right);
                rpt.Write();

            }

            for (int i = 0; i < 5 - len; i++)
                rpt.NewLine();

            rpt.Write();
            rpt.Drawline(2);
        }
        private void WriteReportFooter_A5_Vat_WithHeaderWithMFR(PlainTextReport rpt, IInvoice data)
        {
            rpt.NewLine();


            rpt.Write("Total: " + string.Format("{0:0.00}", data.PurchasedTotal), 15, 2);
            rpt.Write("Dis: " + string.Format("{0:0.00}", data.DiscountInValue), 18, 20);
            if (data.SalesReturnItem.Count == 0)
            {
                rpt.Write(" R.Off: " + string.Format("{0:0.00}", data.RoundoffNetAmount), 18, 29);
                rpt.Write("Net Amt: " + string.Format("{0:0.00}", data.NetAmount), 18, 41, Alignment.Right, data.IsBold);//52
            }
            //rpt.Write(" R.Off: " + string.Format("{0:0.00}", data.RoundoffNetAmount), 18, 36);
            //rpt.Write("Net Amt: #BB#" + string.Format("{0:0.00}", data.RoundOff)+ "#BE#", 19, 64, Alignment.Right);
            else
                rpt.Write("Net Amt: " + string.Format("{0:0.00}", data.Net), 16, 41, Alignment.Right, data.IsBold);//52
            //rpt.Write("Net Amt: " + string.Format("{0:0.00}", data.Net+data.RoundoffNetAmount), 18, 52, Alignment.Right, data.isBold);
            rpt.NewLine();

            if (data.SalesReturnItem.Count == 0 && data.TaxRefNo == 1)
            {
                if (data.CgstTotalValue != 0 && data.SgstTotalValue != 0)
                {
                    rpt.Write("CGST:" + string.Format("{0:0.00}", Math.Round((decimal)data.CgstTotalValue, 2)), 15, 2);
                    rpt.Write((data.Instance.isUnionTerritory == true ? "UTGST:" : "SGST:") + string.Format("{0:0.00}", Math.Round((decimal)data.SgstTotalValue, 2)), 15, 20);
                }
                else
                {
                    rpt.Write("IGST: " + string.Format("{0:0.00}", Math.Round((decimal)data.IgstTotalValue, 2)), 15, 2);
                }
                rpt.NewLine();
            }

            if (data.SalesReturnItem.Count > 0)
            {
                WriteReportReturnedItems_A5_Vat_WithHeaderWithMFR(rpt, data);
            }

            rpt.NewLine();
            rpt.Write("E&OE ", 18, 2);
            rpt.Write(" Signature of the Regd. Pharmacist", 40, 23, Alignment.Right);
            rpt.NewLine();
            rpt.Write("1.Exchange NOT accepted after 72 hrs of Purchase", 55, 2);
            rpt.NewLine();
            rpt.Write("2.Bill is required for Exchange", 40, 2);
            rpt.NewLine();
            rpt.Write("3.Fridge item will not be taken back", 45, 2);


            //if (data.RoundOff.Equals(null))
            //{
            //    rpt.Write("R.Off: " + string.Format("{0:0.00}", data.RoundOff - data.Net), 18, 36);
            //    rpt.Write("Net Amt: " + string.Format("{0:0.00}", data.PurchasedTotal + (data.RoundOff - data.Net)), 18, 52, Alignment.Right);
            //}
            //else
            //{
            //    rpt.Write("R.Off: " + string.Format("{0:0.00}", data.RoundOff), 18, 36);
            //    rpt.Write("Net Amt: " + string.Format("{0:0.00}", data.PurchasedTotal + data.RoundOff), 18, 52, Alignment.Right);
            //}
            rpt.Write();

            //Newly added for Print Footer by Mani on 28-02-2017
            GenerateFooter(rpt, 70, data.PrintFooterNote);
        }

        private void WriteReportFooter_A5_GST_WithHeaderWithMFR(PlainTextReport rpt, IInvoice data)
        {
            bool gstSelection = Convert.ToBoolean(data.Instance.Gstselect);

            if (data.SalesReturnItem.Count == 0 && data.TaxRefNo == 1)
            {
                rpt.NewLine();

                if (data.CgstTotalValue != 0 && data.SgstTotalValue != 0 && !gstSelection)
                {
                    rpt.Write("CGST:" + string.Format("{0:0.00}", Math.Round((decimal)data.CgstTotalValue, 2)), 15, 2);
                    rpt.Write((data.Instance.isUnionTerritory == true ? "UTGST:" : "SGST:") + string.Format("{0:0.00}", Math.Round((decimal)data.SgstTotalValue, 2)), 15, 20);
                }
                else if(!gstSelection)
                {
                    rpt.Write("IGST: " + string.Format("{0:0.00}", Math.Round((decimal)data.IgstTotalValue, 2)), 15, 2);
                }

                rpt.Write("Total: " + string.Format("{0:0.00}", data.PurchasedTotal), 15, 45, Alignment.Right);
                rpt.NewLine();
                rpt.Write("Dis: " + string.Format("{0:0.00}", data.DiscountInValue), 18, 42, Alignment.Right);
                rpt.NewLine();
                rpt.Write(" R.Off: " + string.Format("{0:0.00}", data.RoundoffNetAmount), 18, 42, Alignment.Right);
                rpt.NewLine();
                rpt.Write("Net Amt: " + string.Format("{0:0.00}", data.NetAmount), 18, 41, Alignment.Right, data.IsBold);                                
                rpt.NewLine();
                rpt.Drawline(1);
            }

            if (data.SalesReturnItem.Count > 0)
            {
                WriteReportReturnedItems_A5_GST_WithHeaderWithMFR(rpt, data);
            }

            rpt.NewLine();
            rpt.Write("E&OE ", 18, 2);
            rpt.Write(" Signature of the Regd. Pharmacist", 40, 23, Alignment.Right);
            rpt.NewLine();
            rpt.Write("1.Exchange NOT accepted after 72 hrs of Purchase", 55, 2);
            rpt.NewLine();
            rpt.Write("2.Bill is required for Exchange", 40, 2);
            rpt.NewLine();
            rpt.Write("3.Fridge item will not be taken back", 45, 2);
            
            rpt.Write();

            if (gstSelection)
            {
                rpt.Write();
                rpt.Drawline(1);
                GenerateGstSelected(rpt);
            }

            GenerateFooter(rpt, 70, data.PrintFooterNote);
        }

        private void WriteReportReturnedItems_A5_Vat_WithHeaderWithMFR(PlainTextReport rpt, IInvoice data) //, out string payableAmount
        {
            rpt.Drawline(1);
            rpt.Write("RETURNS", 8, 0, Alignment.Center, TextAdjustment.Wrap);

            decimal? totReturnAmount = 0;
            decimal? totReturnDiscount = 0;
            decimal? ReturnDiscount = 0;
            int len = 0;

            foreach (var items in data.SalesReturnItem)
            {
                if (items.ReturnedQuantity > 0)
                {
                    rpt.NewLine();
                    rpt.Write(string.Format("{0:0}", (len + 1)), 3, true, Alignment.Right);
                    rpt.Write(items.ProductStock.Product.Name.ToUpper(), 18, true);

                    if (string.IsNullOrEmpty(items.ProductStock.Product.Manufacturer))
                    {
                        rpt.Write("-", 4, true);
                    }
                    else
                    {
                        if (items.ProductStock.Product.Manufacturer.ToUpper().Length > 3)
                        {
                            rpt.Write(items.ProductStock.Product.Manufacturer.ToUpper().Substring(0, 3), 4, true);
                        }
                        else
                        {
                            rpt.Write(items.ProductStock.Product.Manufacturer.ToUpper(), 4, true);
                        }
                    }

                    rpt.Write(string.Format("{0:0}", items.ReturnedQuantity), 4, true, Alignment.Right);
                    rpt.Write(items.ProductStock.BatchNo, 8, true, Alignment.Right);

                    rpt.Write(string.Format("{0:MM/yy}", items.ProductStock.ExpireDate), 5, true);

                    decimal? SellingAmount = 0;
                    if (items.SellingPrice > 0)
                    {
                        rpt.Write(string.Format("{0:0.00}", items.SellingPrice), 6, true, Alignment.Right);
                        SellingAmount = (items.ReturnedQuantity * items.SellingPrice);
                    }
                    else
                    {
                        rpt.Write(string.Format("{0:0.00}", items.ProductStock.SellingPrice), 6, true, Alignment.Right);
                        SellingAmount = items.ReturnedQuantity * items.ProductStock.SellingPrice;
                    }

                    totReturnAmount = totReturnAmount + SellingAmount;
                    rpt.Write(string.Format("{0:0.00}", SellingAmount), 6, true, Alignment.Right);
                    rpt.Write();

                    len++;
                    totReturnDiscount = ((items.Discount / 100) * items.Total);
                    totReturnAmount = totReturnAmount - totReturnDiscount;
                    ReturnDiscount += totReturnDiscount;
                }
            }
            rpt.NewLine();
            rpt.NewLine();

            rpt.Write("RETURN AMT: " + string.Format("{0:0.00}", totReturnAmount), 18, 41, Alignment.Right, data.IsBold);//20, 50

            rpt.Write();
            rpt.Drawline(1);

            rpt.NewLine();
            decimal retAmount = data.RoundoffNetAmount as decimal? ?? 0; //RoundOffCalculation(data.Net, totReturnAmount);
            rpt.Write("R.Off: " + string.Format("{0:0.00}", retAmount), 15, 1, Alignment.Left);
            if (ReturnDiscount != 0)
                rpt.Write("Disc.: -" + string.Format("{0:0.00}", ReturnDiscount), 15, 17);
            else
                rpt.Write("Disc.: " + string.Format("{0:0.00}", ReturnDiscount), 15, 17);
            //rpt.NewLine();
            //rpt.Write("PAYABLE AMOUNT: " + string.Format("{0:0.00}", Math.Round(((decimal)data.Net - (decimal)totReturnAmount) + retAmount)), 23, 37, Alignment.Right, data.IsBold);
            rpt.Write("PAYABLE AMOUNT: " + string.Format("{0:0.00}", data.NetAmount), 23, 37, Alignment.Right, data.IsBold);

            //payableAmount = ((long)(data.RoundOff - totReturnAmount)).ConvertNumbertoWords();
            rpt.Write();
            rpt.Drawline(1);
        }

        private void WriteReportReturnedItems_A5_GST_WithHeaderWithMFR(PlainTextReport rpt, IInvoice data)
        {
            rpt.Write("RETURNS", 8, 0, Alignment.Center, TextAdjustment.Wrap);
            bool gstSelection = Convert.ToBoolean(data.Instance.Gstselect);

            decimal? totReturnAmount = 0;
            decimal? totReturnDiscount = 0;
            decimal? ReturnDiscount = 0;
            decimal? returnAmt = 0;
            int len = 0;
            decimal? rtnGSTTotalValue = 0;

            foreach (var items in data.SalesReturnItem)
            {
                if (items.ReturnedQuantity > 0)
                {
                    rpt.NewLine();
                    rpt.Write(string.Format("{0:0}", (len + 1)), 3, true, Alignment.Right);
                    rpt.Write(items.ProductStock.Product.Name.ToUpper(), 18, true);

                    if (string.IsNullOrEmpty(items.ProductStock.Product.Manufacturer))
                    {
                        rpt.Write("-", 4, true);
                    }
                    else
                    {
                        if (items.ProductStock.Product.Manufacturer.ToUpper().Length > 3)
                        {
                            rpt.Write(items.ProductStock.Product.Manufacturer.ToUpper().Substring(0, 3), 4, true);
                        }
                        else
                        {
                            rpt.Write(items.ProductStock.Product.Manufacturer.ToUpper(), 4, true);
                        }
                    }

                    rpt.Write(string.Format("{0:0}", items.ReturnedQuantity), 4, true, Alignment.Right);
                    //if (rpt.Columns > 65)
                    rpt.Write(items.ProductStock.BatchNo, 6, true, Alignment.Right);
                    //else
                        //rpt.Write(items.ProductStock.BatchNo, 6, true, Alignment.Right);

                    rpt.Write(string.Format("{0:MM/yy}", items.ProductStock.ExpireDate), 5, true);

                    items.GstTotal = (items.GstTotal == null) ? 0 : items.GstTotal;
                    decimal? SellingAmount = 0;
                    if (items.SellingPrice > 0)
                    {
                        rpt.Write(string.Format("{0:0.00}", items.SellingPrice), 6, true, Alignment.Right);
                        if (data.SalesReturn.TaxRefNo == 1 && rpt.Columns > 65 && !gstSelection)
                            rpt.Write(string.Format("{0:0.00}", items.GstTotal), 7, true, Alignment.Right);
                        else if (!gstSelection)
                            rpt.Write(string.Format("{0:0.00}", items.GstTotal), 7, true, Alignment.Right);
                        else
                            rpt.Write(" ", 7, true, Alignment.Right);
                        SellingAmount = (items.ReturnedQuantity * items.SellingPrice);
                    }
                    else
                    {
                        rpt.Write(string.Format("{0:0.00}", items.ProductStock.SellingPrice), 6, true, Alignment.Right);
                        if (data.SalesReturn.TaxRefNo == 1 && rpt.Columns > 65 && !gstSelection)
                            rpt.Write(string.Format("{0:0.00}", items.GstTotal), 7, true, Alignment.Right);
                        else if (!gstSelection)
                            rpt.Write(string.Format("{0:0.00}", items.GstTotal), 7, true, Alignment.Right);
                        else
                            rpt.Write(" ", 7, true, Alignment.Right);
                        SellingAmount = items.ReturnedQuantity * items.ProductStock.SellingPrice;
                    }
                    decimal? getGstPercent = items.GstTotal + 100;
                    decimal? SalesItemDiscount = ((items.Discount / 100) * items.Total);
                    //rpt.Write(string.Format("{0:0.00}", SellingAmount), 7, true, Alignment.Right);
                    if (rpt.Columns == 75)
                        rpt.Write(string.Format("{0:0.00}", SellingAmount), 7, true, Alignment.Right);
                    else
                        rpt.Write(string.Format("{0:0.00}", SellingAmount), 7, 54, Alignment.Right);
                    rpt.Write();

                    totReturnAmount = totReturnAmount + SellingAmount;
                    returnAmt += SellingAmount;
                    //rpt.Write(string.Format("{0:0.00}", SellingAmount), 7, true, Alignment.Right);
                    //rpt.Write();
                    var ActualValue = (items.Total - SalesItemDiscount);
                    rtnGSTTotalValue += ActualValue - ((ActualValue * 100) / getGstPercent);
                    len++;
                    totReturnDiscount = ((items.Discount / 100) * items.Total);
                    totReturnAmount = totReturnAmount - totReturnDiscount;
                    ReturnDiscount += totReturnDiscount;
                }
            }
            rpt.NewLine();
            rpt.NewLine();            
            rpt.Write();
            rpt.Drawline(1);

            rpt.NewLine();
            decimal retAmount = data.RoundoffNetAmount as decimal? ?? 0; //RoundOffCalculation(data.Net, totReturnAmount);
            decimal? totValue = data.InvoiceItems.Sum(x => x.Total);
            decimal? totCgstValue = data.InvoiceItems.Sum(x => x.CgstValue);
            decimal? totSgstValue = data.InvoiceItems.Sum(x => x.SgstValue);

            decimal? RtntotgstValue = rtnGSTTotalValue / 2;

            if (!gstSelection)
            {
                rpt.Write("CGST: " + string.Format("{0:0.00}", (totCgstValue - RtntotgstValue)), 12, 1);
                rpt.Write((data.Instance.isUnionTerritory == true ? "UTGST: " : "SGST: ") + string.Format("{0:0.00}", (totSgstValue - RtntotgstValue)), 12, 15);
            }
            if (rpt.Columns > 65)
                rpt.Write("TOTAL:" + string.Format("{0:0.00}", (totValue - returnAmt)), 15, 53, Alignment.Right); //data.Net 
            else
                rpt.Write("TOTAL:" + string.Format("{0:0.00}", (totValue - returnAmt)), 15, 45, Alignment.Right);
            rpt.NewLine();
            if (rpt.Columns > 65)
                rpt.Write("Disc.: " + string.Format("{0:0.00}", (data.DiscountInValue - ReturnDiscount)), 15, 53, Alignment.Right); //TotalDiscount
            else
                rpt.Write("Disc.: " + string.Format("{0:0.00}", (data.DiscountInValue - ReturnDiscount)), 15, 45, Alignment.Right);
            rpt.NewLine();
            string payAmountInWords = ((decimal)(data.NetAmount)).ConvertNumbertoWords();
            rpt.Write("Rupees " + payAmountInWords, 40, 1, TextAdjustment.Wrap);
            if (rpt.Columns > 65)
                rpt.Write("R.Off: " + string.Format("{0:0.00}", retAmount), 12, 56, Alignment.Right);
            else
                rpt.Write("R.Off: " + string.Format("{0:0.00}", retAmount), 12, 48, Alignment.Right);
            rpt.NewLine();
            if (rpt.Columns > 65)
                rpt.Write("RETURN AMT: " + string.Format("{0:0.00}", totReturnAmount), 18, 48, Alignment.Right, data.IsBold);
            else
                rpt.Write("RETURN AMT: " + string.Format("{0:0.00}", totReturnAmount), 18, 42, Alignment.Right, data.IsBold);
            rpt.NewLine();
            if (rpt.Columns > 65)
                rpt.Write("PAYABLE AMOUNT: " + string.Format("{0:0.00}", data.NetAmount), 23, 43, Alignment.Right, data.IsBold);
            else
                rpt.Write("PAYABLE AMOUNT: " + string.Format("{0:0.00}", data.NetAmount), 23, 37, Alignment.Right, data.IsBold);

            rpt.Write();
            rpt.Drawline(1);
        }

        #endregion A5_Vat_WithHeaderPalani


        protected string GenerateSalesReport_A5_Vat_WithMFR6Inch(IInvoice data)
        {
            PlainTextReport rpt = new PlainTextReport();
            bool gstSelection = Convert.ToBoolean(data.Instance.Gstselect);

            rpt.Columns = 63;
            rpt.Rows = 30;

            rpt.NewLine();
            rpt.NewLine();

            WriteReportHeader_A5_Vat_WithHeaderWithMFR6Inch(rpt, data);

            if (data.InvoiceItems.Count() == 0)
            {
                if (data.TaxRefNo == 1)
                    WriteReportOnlyReturnItems_A5_Vat_WithHeaderWithMFR6Inch_GST(rpt, data);
                else
                {
                    WriteReportOnlyReturnItems_A5_Vat_WithHeaderWithMFR6Inch(rpt, data);
                }
            }
            else
            {
                WriteReportItems_A5_Vat_WithHeaderWithMFR(rpt, data);
                if (data.TaxRefNo == 1)
                    WriteReportFooter_A5_GST_WithHeaderWithMFR(rpt, data);
                else
                    WriteReportFooter_A5_Vat_WithHeaderWithMFR(rpt, data);
            }

            //some trailing blank lines
            rpt.NewLine();
            rpt.NewLine();
            rpt.Write();

            return rpt.Content.ToString();
        }
        #region A5 Without VAT With MFR
        private void WriteReportHeader_A5_Vat_WithHeaderWithMFR6Inch(PlainTextReport rpt, IInvoice data)
        {

            rpt.NewLine();
            bool gstSelection = Convert.ToBoolean(data.Instance.Gstselect);
            rpt.Write(data.Instance.Name.ToUpper(), 60, 0, Alignment.Center, TextAdjustment.Wrap, data.IsBold);
            string adddressarea = string.Empty;
            if (!string.IsNullOrEmpty(data.Instance.Address))
            {
                adddressarea += data.Instance.Address;
            }
            if (!string.IsNullOrEmpty(data.Instance.Area))
            {
                adddressarea += "," + data.Instance.Area;
            }
            if (!string.IsNullOrEmpty(adddressarea))
            {
                rpt.NewLine();
                rpt.Write(adddressarea.ToUpper(), 60, 0, Alignment.Center, TextAdjustment.Wrap);
            }
            string cityState = string.Empty;
            if (!string.IsNullOrEmpty(data.Instance.City))
            {
                cityState += data.Instance.City + ",";
            }
            if (!string.IsNullOrEmpty(data.Instance.State))
            {
                cityState += data.Instance.State + ",";
            }
            if (!string.IsNullOrEmpty(data.Instance.Pincode))
            {
                cityState += data.Instance.Pincode;
            }

            if (!string.IsNullOrEmpty(cityState))
            {
                rpt.NewLine();
                rpt.Write(cityState.ToUpper(), 60, 0, Alignment.Center, TextAdjustment.Wrap);
            }

            string contact = string.Empty;

            if (!string.IsNullOrEmpty(data.Instance.Phone))
            {
                contact += "Phone No: " + data.Instance.Phone;
            }
            if (!string.IsNullOrEmpty(data.Instance.Mobile))
            {
                contact += "," + "Mobile No: " + data.Instance.Mobile;
            }
            if (!string.IsNullOrEmpty(contact))
            {
                rpt.NewLine();
                //rpt.Write(contact.ToUpper(), 108, 0, Alignment.Center, TextAdjustment.Wrap);
                rpt.Write(contact.ToUpper(), 60, 0, Alignment.Center, TextAdjustment.Wrap);
            }

            rpt.NewLine();
            //rpt.Write("DL No : " + data.Instance.DrugLicenseNo, 108, 2, Alignment.Center, TextAdjustment.Wrap);
            rpt.Write("DL No : " + data.Instance.DrugLicenseNo, 60, 2, Alignment.Center, TextAdjustment.Wrap);

            rpt.NewLine();
            //rpt.Write("TIN   : " + data.Instance.TinNo, 108, 2, Alignment.Center, TextAdjustment.Wrap);
            if (data.TaxRefNo == 1)
            {
                rpt.Write("GSTIN No: " + data.Instance.GsTinNo, 58, 2, Alignment.Center, TextAdjustment.Wrap);
            }
            else
            {
                rpt.Write("TIN   : " + data.Instance.TinNo, 60, 2, Alignment.Center, TextAdjustment.Wrap);
            }

            if (gstSelection)
            {
                rpt.NewLine();
                rpt.Write("Bill of Supply", 44, 1, Alignment.Center, TextAdjustment.Wrap);
            }

            rpt.NewLine();
            rpt.Drawline(2, 1);

            rpt.NewLine();
            rpt.Write("Doctor  : " + data.DoctorName.ToUpper(), 40, 2);

            if (data.InvoiceItems.Count() == 0)
            {
                rpt.Write("Return No: " + data.InvoiceSeries + data.InvoiceNo, 18, 41);
            }
            else
            {
                rpt.Write("Bill No: " + data.InvoiceSeries + data.InvoiceNo, 18, 41);
            }


            rpt.NewLine();
            rpt.Write("Patient : " + data.Name.ToUpper(), 40, 2);
            rpt.Write("B Date : " + string.Format("{0:dd/MM/yy}", data.InvoiceDate), 18, 41);


            rpt.NewLine();
            rpt.Write("Address : " + data.Address, 40, 2);
            rpt.Write("Time   : " + Convert.ToDateTime(data.SalesCreatedAt).ToString("hh:mm tt"), 18, 41);

            if (data.Address.Length > 30)
            {
                rpt.NewLine();
                rpt.Write("" + data.Address.Substring(29, data.Address.Length - 29), 41, 2);
                rpt.Write(" " + "", 18, 41);
            }

            rpt.NewLine();

            rpt.Write("City    : " + data.Patient.City, 40, 2);
            rpt.Write("B Type : " + data.PaymentType, 18, 41);

            rpt.NewLine();
            rpt.Write("Pincode : " + data.Patient.Pincode, 40, 2, TextAdjustment.NewLine);
            rpt.Write("ID No. : " + Convert.ToString(data.Patient.EmpID), 18, 41);

            rpt.Write();
            rpt.Drawline(2, 1);


            rpt.NewLine();

            rpt.Write("SNo", 3, 2);
            rpt.Write("Particulars", 18, true);
            rpt.Write("MFG", 3, true);
            rpt.Write("Qty", 4, true, Alignment.Right);
            rpt.Write("Batch", 6, true);
            rpt.Write("Exp", 5, true, Alignment.Center);
            rpt.Write("MRP", 6, true, Alignment.Right);
            rpt.Write("Amount", 7, true, Alignment.Right);
            rpt.Write();
            rpt.Drawline(2, 1);

        }
        private void WriteReportItems_A5_Vat_WithHeaderWithMFR6Inch(PlainTextReport rpt, IInvoice data)
        {
            int len = 0;
            foreach (var items in data.InvoiceItems)
            {
                if (items.Quantity <= 0)
                { break; }
                rpt.NewLine();
                len++;

                rpt.Write(string.Format("{0:0}", len), 3, true, Alignment.Right);
                rpt.Write(items.ProductStock.Product.Name.ToUpper(), 18, true);

                if (string.IsNullOrEmpty(items.ProductStock.Product.Manufacturer))
                {
                    rpt.Write("-", 3, true);
                }
                else
                {
                    if (items.ProductStock.Product.Manufacturer.ToUpper().Length > 3)
                    {
                        rpt.Write(items.ProductStock.Product.Manufacturer.ToUpper().Substring(0, 3), 3, true);
                    }
                    else
                    {
                        rpt.Write(items.ProductStock.Product.Manufacturer.ToUpper(), 3, true);
                    }
                }

                rpt.Write(string.Format("{0:0}", items.Quantity), 4, true, Alignment.Right);
                rpt.Write(items.ProductStock.BatchNo, 8, true);

                rpt.Write(string.Format("{0:MM/yy}", items.ProductStock.ExpireDate), 5, true);

                decimal? SellingAmount = 0;

                if (items.SellingPrice > 0)
                {
                    rpt.Write(string.Format("{0:0.00}", items.SellingPrice), 6, true, Alignment.Right);
                    SellingAmount = (items.Quantity * items.SellingPrice);
                }
                else
                {
                    rpt.Write(string.Format("{0:0.00}", items.ProductStock.SellingPrice), 6, true, Alignment.Right);
                    SellingAmount = items.Quantity * items.ProductStock.SellingPrice;
                }
                rpt.Write(string.Format("{0:0.00}", SellingAmount), 7, true, Alignment.Right);
                rpt.Write();

            }

            for (int i = 0; i < 5 - len; i++)
                rpt.NewLine();

            rpt.Write();
            rpt.Drawline(2, 1);
        }
        private void WriteReportFooter_A5_Vat_WithHeaderWithMFR6Inch(PlainTextReport rpt, IInvoice data)
        {
            rpt.NewLine();
            rpt.Write("Total: " + string.Format("{0:0.00}", data.PurchasedTotal), 15, 2);
            rpt.Write("Dis: " + string.Format("{0:0.00}", data.DiscountInValue), 18, 17);
            if (data.SalesReturnItem.Count == 0)
            {
                rpt.Write(" R.Off: " + string.Format("{0:0.00}", data.RoundoffNetAmount), 17, 27);
                rpt.Write("Net Amt: " + string.Format("{0:0.00}", data.NetAmount), 18, 41, Alignment.Right, data.IsBold);
            }
            //rpt.Write(" R.Off: " + string.Format("{0:0.00}", data.RoundoffNetAmount), 17, 27);
            else
                rpt.Write("Net Amt: " + string.Format("{0:0.00}", data.Net), 18, 41, Alignment.Right, data.IsBold);
            //rpt.Write("Net Amt: " + string.Format("{0:0.00}", data.Net+data.RoundoffNetAmount), 18, 41, Alignment.Right, data.isBold);
            rpt.NewLine();

            if (data.SalesReturnItem.Count > 0)
            {
                WriteReportReturnedItems_A5_Vat_WithHeaderWithMFR6Inch(rpt, data);
            }
            rpt.NewLine();

            rpt.Write("E&OE ", 18, 2);
            rpt.Write(" Signature of the Regd. Pharmacist", 35, 27, Alignment.Right);
            rpt.NewLine();
            rpt.Write("1.Exchange NOT accepted after 72 hrs of Purchase", 48, 2);
            rpt.NewLine();
            rpt.Write("2.Bill is required for Exchange", 40, 2);
            rpt.NewLine();
            rpt.Write("3.Fridge item will not be taken back", 40, 2);

            rpt.Write();
            GenerateFooter(rpt, 60, data.PrintFooterNote);
        }
        private void WriteReportReturnedItems_A5_Vat_WithHeaderWithMFR6Inch(PlainTextReport rpt, IInvoice data)
        {
            rpt.Drawline(1);
            rpt.Write("RETURNS", 8, 0, Alignment.Center, TextAdjustment.Wrap);

            decimal? totReturnAmount = 0;
            decimal? totReturnDiscount = 0;
            decimal? ReturnDiscount = 0;
            int len = 0;

            foreach (var items in data.SalesReturnItem)
            {
                if (items.ReturnedQuantity > 0)
                {
                    rpt.NewLine();
                    len++;

                    rpt.Write(string.Format("{0:0}", len), 3, true, Alignment.Right);
                    rpt.Write(items.ProductStock.Product.Name.ToUpper(), 18, true);

                    if (string.IsNullOrEmpty(items.ProductStock.Product.Manufacturer))
                    {
                        rpt.Write("-", 3, true);
                    }
                    else
                    {
                        if (items.ProductStock.Product.Manufacturer.ToUpper().Length > 3)
                        {
                            rpt.Write(items.ProductStock.Product.Manufacturer.ToUpper().Substring(0, 3), 3, true);
                        }
                        else
                        {
                            rpt.Write(items.ProductStock.Product.Manufacturer.ToUpper(), 3, true);
                        }
                    }

                    rpt.Write(string.Format("{0:0}", items.ReturnedQuantity), 4, true, Alignment.Right);
                    rpt.Write(items.ProductStock.BatchNo, 8, true);

                    rpt.Write(string.Format("{0:MM/yy}", items.ProductStock.ExpireDate), 5, true);

                    decimal? SellingAmount = 0;

                    if (items.SellingPrice > 0)
                    {
                        rpt.Write(string.Format("{0:0.00}", items.SellingPrice), 6, true, Alignment.Right);
                        SellingAmount = (items.ReturnedQuantity * items.SellingPrice);
                    }
                    else
                    {
                        rpt.Write(string.Format("{0:0.00}", items.ProductStock.SellingPrice), 6, true, Alignment.Right);
                        SellingAmount = items.ReturnedQuantity * items.ProductStock.SellingPrice;
                    }
                    rpt.Write(string.Format("{0:0.00}", SellingAmount), 7, true, Alignment.Right);
                    rpt.Write();

                    totReturnAmount = totReturnAmount + SellingAmount;

                    totReturnDiscount = ((items.Discount / 100) * items.Total);
                    totReturnAmount = totReturnAmount - totReturnDiscount;
                    ReturnDiscount += totReturnDiscount;
                }
            }
            rpt.NewLine();
            rpt.NewLine();

            rpt.Write("RETURN AMT: " + string.Format("{0:0.00}", totReturnAmount), 20, 39, Alignment.Right, data.IsBold);

            rpt.Write();
            rpt.Drawline(1);

            rpt.NewLine();
            decimal retAmount = data.RoundoffNetAmount as decimal? ?? 0; //RoundOffCalculation(data.Net, totReturnAmount);
            rpt.Write("R.Off: " + string.Format("{0:0.00}", retAmount), 15, 1, Alignment.Left);
            if (ReturnDiscount != 0)
            { rpt.Write("Disc.: -" + string.Format("{0:0.00}", ReturnDiscount), 15, 17); }
            else
            { rpt.Write("Disc.: " + string.Format("{0:0.00}", ReturnDiscount), 15, 17); }            
            //rpt.Write("PAYABLE AMOUNT: " + string.Format("{0:0.00}", Math.Round(((decimal)data.Net - (decimal)totReturnAmount) + retAmount)), 28, 31, Alignment.Right, data.IsBold);
            rpt.Write("PAYABLE AMOUNT: " + string.Format("{0:0.00}", data.NetAmount), 28, 31, Alignment.Right, data.IsBold);

            rpt.Write();
            rpt.Drawline(1);
        }
        #endregion

        #region 4InchRoll_Generic_OrderPrintContent
        protected string GeneraCustomteOrderReport_A4(VendorOrder data, int customOrder)
        {
            var rpt = new PlainTextReport
            {
                Columns = 80,
                Rows = 40
            };


            if (customOrder == 1)
            {
                WriteCustomOrderReportHeader_A4(rpt, data);
                WriteCustomOrderReportItems_A4(rpt, data);
            }
            else
            {
                WriteOrderReportHeader_A4(rpt, data);
                WriteOrderReportItems_A4(rpt, data);
            }


            WriteOrderReportFooter_A4(rpt, data);
            return rpt.Content.ToString();
        }
        private void WriteOrderReportHeader_A4(PlainTextReport rpt, VendorOrder data)
        {

            rpt.NewLine();
            rpt.Write(data.Instance.Name.ToUpper(), 67, 2, Alignment.Center, TextAdjustment.Wrap, data.IsBold);

            string adddressarea = string.Empty;

            if (!string.IsNullOrEmpty(data.Instance.Address))
            {
                adddressarea += data.Instance.Address;
            }
            if (!string.IsNullOrEmpty(data.Instance.Area))
            {
                adddressarea += "," + data.Instance.Area;
            }
            if (!string.IsNullOrEmpty(adddressarea))
            {
                rpt.NewLine();
                rpt.Write(string.Format("{0}", adddressarea.ToUpper().TrimEnd()), 69, 2, Alignment.Center, TextAdjustment.Wrap);
            }

            string cityState = string.Empty;
            if (!string.IsNullOrEmpty(data.Instance.City))
            {
                cityState += data.Instance.City + ",";
            }
            if (!string.IsNullOrEmpty(data.Instance.State))
            {
                cityState += data.Instance.State + ",";
            }
            if (!string.IsNullOrEmpty(data.Instance.Pincode))
            {
                cityState += data.Instance.Pincode;
            }

            if (!string.IsNullOrEmpty(cityState))
            {
                rpt.NewLine();
                rpt.Write(cityState.ToUpper(), 69, 2, Alignment.Center, TextAdjustment.Wrap);
            }


            string contact = string.Empty;

            if (!string.IsNullOrEmpty(data.Instance.Phone))
            {
                contact += "Ph: " + data.Instance.Phone;
            }
            if (!string.IsNullOrEmpty(data.Instance.Mobile))
            {
                contact += "," + "Mobile: " + data.Instance.Mobile;
            }
            if (!string.IsNullOrEmpty(contact))
            {
                rpt.NewLine();
                rpt.Write(contact.ToUpper(), 69, 2, Alignment.Center, TextAdjustment.Wrap);
            }

            rpt.NewLine();
            rpt.Write("DL No : " + data.Instance.DrugLicenseNo, 69, 2, Alignment.Center, TextAdjustment.Wrap);

            rpt.NewLine();
            rpt.Write("TIN   : " + data.Instance.TinNo, 69, 2, Alignment.Center, TextAdjustment.Wrap);
            rpt.Write();

            rpt.Drawline();

            rpt.NewLine();
            rpt.Write("Vendor Name   : " + data.Vendor.Name.ToUpper(), 45);
            rpt.Write("     Order No    : " + data.OrderId, 30, 50);


            rpt.NewLine();

            if (string.IsNullOrEmpty(data.Vendor.Address))
            {
                rpt.Write("Vendor Address: " + "-", 45);
            }
            else
            {
                rpt.Write("Vendor Address: " + data.Vendor.Address.ToUpper(), 45);
            }
            rpt.Write("     Order Date  : " + string.Format("{0:dd/MM/yyyy}", data.CreatedAt), 30, 50);


            rpt.NewLine();

            if (string.IsNullOrEmpty(data.Vendor.Mobile))
            {
                rpt.Write("Vendor Mobile : " + "-", 45);
            }
            else
            {
                rpt.Write("Vendor Mobile : " + data.Vendor.Mobile.ToUpper(), 45);
            }
            rpt.Write("     Order Time  : " + Convert.ToDateTime(data.CreatedAt).ToString("hh:mm tt"), 30, 50);

            rpt.NewLine();
            if (string.IsNullOrEmpty(data.Vendor.Email))
            {
                rpt.Write("Vendor Mail   : " + "-", 45);
            }
            else
            {
                rpt.Write("Vendor Mail   : " + data.Vendor.Email, 45);
            }
            rpt.Write("-", 1, 55);

            rpt.NewLine();
            if (string.IsNullOrEmpty(data.Vendor.TinNo))
            {
                rpt.Write("Vendor TinNo  : " + "-", 45);
            }
            else
            {
                rpt.Write("Vendor Tin    : " + data.Vendor.TinNo, 45);
            }
            rpt.Write("-", 1, 55);

            rpt.Write();
            rpt.Drawline();

            rpt.NewLine();
            rpt.Write("Product", 31, Alignment.Center);
            rpt.Write("Quantity(Units)", 15, 31);
            rpt.Write("NoOfStrips", 10, 50);
            rpt.Write("P.Price/Strip", 13, 65);
            rpt.Write();
            rpt.Drawline();
        }
        private void WriteCustomOrderReportHeader_A4(PlainTextReport rpt, VendorOrder data)
        {

            rpt.NewLine();
            rpt.Write(data.Instance.Name.ToUpper(), 67, 2, Alignment.Center, TextAdjustment.Wrap, data.IsBold);

            string adddressarea = string.Empty;

            if (!string.IsNullOrEmpty(data.Instance.Address))
            {
                adddressarea += data.Instance.Address;
            }
            if (!string.IsNullOrEmpty(data.Instance.Area))
            {
                adddressarea += "," + data.Instance.Area;
            }
            if (!string.IsNullOrEmpty(adddressarea))
            {
                rpt.NewLine();
                rpt.Write(string.Format("{0}", adddressarea.ToUpper().TrimEnd()), 69, 2, Alignment.Center, TextAdjustment.Wrap);
            }

            string cityState = string.Empty;
            if (!string.IsNullOrEmpty(data.Instance.City))
            {
                cityState += data.Instance.City + ",";
            }
            if (!string.IsNullOrEmpty(data.Instance.State))
            {
                cityState += data.Instance.State + ",";
            }
            if (!string.IsNullOrEmpty(data.Instance.Pincode))
            {
                cityState += data.Instance.Pincode;
            }

            if (!string.IsNullOrEmpty(cityState))
            {
                rpt.NewLine();
                rpt.Write(cityState.ToUpper(), 69, 2, Alignment.Center, TextAdjustment.Wrap);
            }


            string contact = string.Empty;

            if (!string.IsNullOrEmpty(data.Instance.Phone))
            {
                contact += "Ph: " + data.Instance.Phone;
            }
            if (!string.IsNullOrEmpty(data.Instance.Mobile))
            {
                contact += "," + "Mobile: " + data.Instance.Mobile;
            }
            if (!string.IsNullOrEmpty(contact))
            {
                rpt.NewLine();
                rpt.Write(contact.ToUpper(), 69, 2, Alignment.Center, TextAdjustment.Wrap);
            }

            rpt.NewLine();
            rpt.Write("DL No : " + data.Instance.DrugLicenseNo, 69, 2, Alignment.Center, TextAdjustment.Wrap);

            rpt.NewLine();
            rpt.Write("TIN   : " + data.Instance.TinNo, 69, 2, Alignment.Center, TextAdjustment.Wrap);
            rpt.Write();


            rpt.NewLine();
            rpt.NewLine();
            rpt.NewLine();
            rpt.Write("Purchase Order", 69, 2, Alignment.Center, TextAdjustment.Wrap);
            rpt.Write();




            rpt.Drawline();

            rpt.NewLine();
            rpt.Write("Vendor Name   : " + data.Vendor.Name.ToUpper(), 45);
            rpt.Write("     Order No    : " + data.OrderId, 30, 50);


            rpt.NewLine();




            if (string.IsNullOrEmpty(data.Vendor.Address))
            {
                rpt.Write("Vendor Address: " + "-", 45);
            }
            else
            {
                rpt.Write("Vendor Address: " + data.Vendor.Address.ToUpper(), 45);
            }
            rpt.Write("     Order Date  : " + string.Format("{0:dd/MM/yyyy}", data.CreatedAt), 30, 50);


            rpt.NewLine();

            if (string.IsNullOrEmpty(data.Vendor.Mobile))
            {
                rpt.Write("Vendor Mobile : " + "-", 45);
            }
            else
            {
                rpt.Write("Vendor Mobile : " + data.Vendor.Mobile.ToUpper(), 45);
            }
            rpt.Write("     Order Time  : " + Convert.ToDateTime(data.CreatedAt).ToString("hh:mm tt"), 30, 50);


            rpt.NewLine();
            if (string.IsNullOrEmpty(data.Vendor.Email))
            {
                rpt.Write("Vendor Mail   : " + "-", 45);
            }
            else
            {
                rpt.Write("Vendor Mail   : " + data.Vendor.Email, 45);
            }
            rpt.Write("-", 1, 55);


            rpt.NewLine();
            if (string.IsNullOrEmpty(data.Vendor.TinNo))
            {
                rpt.Write("Vendor TinNo  : " + "-", 45);
            }
            else
            {
                rpt.Write("Vendor Tin    : " + data.Vendor.TinNo, 45);
            }
            rpt.Write("-", 1, 55);


            rpt.Write();
            rpt.Drawline();

            rpt.NewLine();
            rpt.Write("Product", 31, Alignment.Center);
            rpt.Write("Ord.Qty", 7, 31);
            rpt.Write("Free Qty", 8, 31);
            rpt.Write("Mrp/Strip", 9, 41);
           // Commented by nandhini on 27.12.17
         rpt.Write("Gst%", 7, 55, Alignment.Center);
            rpt.Write("P.Price/Strip", 13, 66);
            rpt.Write("Amount", 13, 66, Alignment.Right);
            rpt.Write();
            rpt.Drawline();
        }

        private void WriteOrderReportItems_A4(PlainTextReport rpt, VendorOrder data)
        {
            int val = 0;
            foreach (var item in data.VendorOrderItem)
            {
                val++;
                rpt.NewLine();
                rpt.Write("" + val + ") " + item.Product.Name.ToUpper(), 31);
                rpt.Write(string.Format("{0:0}", item.Quantity), 15, 31, Alignment.Right);
                if (item.PackageQty == 0.00M)
                {
                    rpt.Write(string.Format("{0:0}", "-"), 10, 50, Alignment.Right);
                }
                else
                {
                    rpt.Write(string.Format("{0:0}", item.PackageQty), 10, 50, Alignment.Right);
                }
                rpt.Write(string.Format("{0:0.00}", item.Product.purchaseprice), 13, 65, Alignment.Right);
            }
            rpt.Write();
            rpt.Drawline();
        }




        private void WriteCustomOrderReportItems_A4(PlainTextReport rpt, VendorOrder data)
        {
            int val = 0;
            foreach (var item in data.VendorOrderItem)
            {
                val++;
                rpt.NewLine();
                rpt.Write("" + val + ") " + item.Product.Name.ToUpper(), 31);
                rpt.Write(string.Format("{0:0}", item.Quantity), 7, 31, Alignment.Right);
                rpt.Write(string.Format("{0:0}", item.FreeQty), 7, 55, Alignment.Center);
                rpt.Write(string.Format("{0:0.00}", item.SellingPrice), 9, 41, Alignment.Right);
                //Commented by nandhini on 27.12.17
             // rpt.Write(string.Format("{0:MM/yy}", item.ExpireDate), 7, 55, Alignment.Center);
              rpt.Write(string.Format("{0:0.00}", item.GstTotal), 7, 55, Alignment.Center);
               
                rpt.Write(string.Format("{0:0.00}", item.Product.purchaseprice), 13, 66, Alignment.Right);
                rpt.Write(string.Format("{0:0.00}", (item.Quantity * item.Product.purchaseprice)), 13, 66, Alignment.Right);
            }
            rpt.Write();
            rpt.Drawline();
        }
        private void WriteOrderReportFooter_A4(PlainTextReport rpt, VendorOrder data)
        {
            rpt.NewLine();
            rpt.Write("Total Items Ordered: " + string.Format("{0:0}", data.VendorOrderItem.Count), 45);
            rpt.Write();
        }
        #endregion

        ///

        #region A5QMS_WithHeader_Generic _template

        //public string ConvertNumbertoWords(long number)
        //{
        //    if (number == 0) return "ZERO";
        //    if (number < 0) return "minus " + ConvertNumbertoWords(Math.Abs(number));
        //    string words = "";
        //    if ((number / 1000000) > 0)
        //    {
        //        words += ConvertNumbertoWords(number / 100000) + " LAKES ";
        //        number %= 1000000;
        //    }
        //    if ((number / 1000) > 0)
        //    {
        //        words += ConvertNumbertoWords(number / 1000) + " THOUSAND ";
        //        number %= 1000;
        //    }
        //    if ((number / 100) > 0)
        //    {
        //        words += ConvertNumbertoWords(number / 100) + " HUNDRED ";
        //        number %= 100;
        //    }
        //    //if ((number / 10) > 0)  
        //    //{  
        //    // words += ConvertNumbertoWords(number / 10) + " RUPEES ";  
        //    // number %= 10;  
        //    //}  
        //    if (number > 0)
        //    {
        //        if (words != "") words += "AND ";
        //        var unitsMap = new[]
        //        {
        //    "ZERO", "ONE", "TWO", "THREE", "FOUR", "FIVE", "SIX", "SEVEN", "EIGHT", "NINE", "TEN", "ELEVEN", "TWELVE", "THIRTEEN", "FOURTEEN", "FIFTEEN", "SIXTEEN", "SEVENTEEN", "EIGHTEEN", "NINETEEN"
        //};
        //        var tensMap = new[]
        //        {
        //    "ZERO", "TEN", "TWENTY", "THIRTY", "FORTY", "FIFTY", "SIXTY", "SEVENTY", "EIGHTY", "NINETY"
        //};
        //        if (number < 20) words += unitsMap[number];
        //        else
        //        {
        //            words += tensMap[number / 10];
        //            if ((number % 10) > 0) words += " " + unitsMap[number % 10];
        //        }
        //    }
        //    return words;
        //}

        protected string GenerateSalesReport_A5QMS_WithHeader(IInvoice data)
        {
            PlainTextReport rpt = new PlainTextReport();
            rpt.Columns = 86;
            rpt.Rows = 30;

            rpt.NewLine();
            rpt.NewLine();
            bool gstSelection = Convert.ToBoolean(data.Instance.Gstselect);
            if (gstSelection)
            {
                rpt.Write("Bill of Supply", 80, 1, Alignment.Center);
            }
            else
                rpt.Write("RETAIL INVOICE", 80, 1, Alignment.Center);
            rpt.NewLine();
            rpt.Drawline(1);
            WriteReportHeader_A5QMS_WithHeader(rpt, data);
            if (!data.IsReturnsOnly)
            {
                WriteReportItems_A5QMS_WithHeader(rpt, data);
                WriteReportFooter_A5QMS_WithHeader(rpt, data);
            }
            else
            {
                WriteReportOnlyReturnItems_A5QMS_WithHeader(rpt, data);
                WriteReportOnlyReturnFooter_A5QMS_WithHeader(rpt, data);
            }

            rpt.NewLine();
            rpt.NewLine();
            rpt.Write();

            return rpt.Content.ToString();
        }

        #region A5QMS_WithHeaderNoGST
        protected string GenerateSalesReport_A5QMS_WithHeaderNoGST(IInvoice data)
        {
            PlainTextReport rpt = new PlainTextReport();
            rpt.Columns = 86;
            rpt.Rows = 30;

            rpt.NewLine();
            rpt.NewLine();
            bool gstSelection = Convert.ToBoolean(data.Instance.Gstselect);
            if (gstSelection)
            {
                rpt.Write("Bill of Supply", 80, 1, Alignment.Center);
            }
            else rpt.Write("RETAIL INVOICE", 80, 1, Alignment.Center);
            rpt.NewLine();
            rpt.Drawline(1);
            WriteReportHeader_A5QMS_WithHeader(rpt, data);
            if (!data.IsReturnsOnly)
            {
                WriteReportItems_A5QMS_WithHeaderNoGST(rpt, data);
                WriteReportFooter_A5QMS_WithHeaderNoGST(rpt, data);
            }
            else
            {
                WriteReportOnlyReturnItems_A5QMS_WithHeader(rpt, data);
                WriteReportOnlyReturnFooter_A5QMS_WithHeaderNoGST(rpt, data);
            }

            rpt.NewLine();
            rpt.NewLine();
            rpt.Write();

            return rpt.Content.ToString();
        }

        private void WriteReportItems_A5QMS_WithHeaderNoGST(PlainTextReport rpt, IInvoice data)
        {
            int len = 0;
            int itemsHeight = 7;

            List<IInvoiceItem> groupedList = new List<IInvoiceItem>();

            foreach (var items in data.InvoiceItems)
            {
                if (items.Total == 0)   // Added by San - 06-05-2017
                {
                    items.Quantity = items.ReturnedQty;
                }
                rpt.NewLine();
                len++;

                if (groupedList.Count() != itemsHeight)
                {
                    groupedList.Add(items);
                    WriteReportItemsforA5NoGST(rpt, items);
                }
                else if (groupedList.Count() == itemsHeight && data.InvoiceItems.Count() > 1)
                {
                    if (len < data.InvoiceItems.Count())
                    {
                        WriteReportEmptyFooter_A5(rpt, groupedList.Count(), itemsHeight);
                    }
                    groupedList.RemoveAll(y => true);
                    groupedList.Add(items);
                    rpt.NewLine();
                    rpt.NewLine();

                    rpt.Write("RETAIL INVOICE", 80, 1, Alignment.Center);
                    rpt.NewLine();
                    rpt.Drawline(1);
                    WriteReportHeader_A5QMS_WithHeader(rpt, data);
                    WriteReportItemsforA5NoGST(rpt, items);
                }

            }

            for (int i = 0; i < 5 - len; i++)
                rpt.NewLine();

            rpt.Write();
            rpt.Drawline(1);
        }

        private void WriteReportItemsforA5NoGST(PlainTextReport rpt, IInvoiceItem items)
        {
            rpt.Write((items.ProductStock.RackNo.Trim().Length > 0 ? items.ProductStock.RackNo.ToUpper() : "  "), 4, 1, Alignment.Right);
            rpt.Write(string.Format("{0:0}", items.Quantity), 4, 7, Alignment.Right);
            rpt.Write(items.ProductStock.Product.Name.ToUpper(), 25, 14, Alignment.Left, TextAdjustment.Wrap);
            rpt.Write(items.ProductStock.BatchNo, 6, 39, Alignment.Center);
            rpt.Write(string.Format("{0:MM/yy}", items.ProductStock.ExpireDate), 5, 48, Alignment.Center);

            if (items.ProductStock.MRP > 0)
            {
                rpt.Write(string.Format("{0:0.00}", items.ProductStock.MRP), 6, 54, Alignment.Right);
            }
            else
            {
                if (items.SellingPrice > 0)
                {
                    decimal? vatPrice = ((items.SellingPrice * 100) / (items.ProductStock.VAT + 100));
                    rpt.Write(string.Format("{0:0.00}", items.SellingPrice), 6, 54, Alignment.Right);
                }
                else
                {
                    decimal? vatPrice = items.ProductStock.SellingPrice - items.ProductStock.VatInPrice;
                    rpt.Write(string.Format("{0:0.00}", items.ProductStock.SellingPrice), 6, 54, Alignment.Right);
                }
            }

            if (items.SellingPrice > 0)
            {
                decimal? vatPrice = ((items.SellingPrice * 100) / (items.ProductStock.VAT + 100));
                rpt.Write(string.Format("{0:0.00}", items.SellingPrice), 6, 61, Alignment.Right);
            }
            else
            {
                decimal? vatPrice = items.ProductStock.SellingPrice - items.ProductStock.VatInPrice;
                rpt.Write(string.Format("{0:0.00}", items.ProductStock.SellingPrice), 6, 61, Alignment.Right);
            }

            var tmpMRP = items.Quantity * items.ProductStock.MRP;
            var tmpSellPrice = items.Quantity * items.ProductStock.SellingPrice;
            decimal? tmpDiscount = 0;
            if (tmpMRP == 0)
                tmpDiscount = 0;
            else
                tmpDiscount = (((tmpMRP - tmpSellPrice) / tmpMRP) * 100);


            rpt.Write(string.Format("{0:0.00}", (tmpDiscount == null ? 0 : tmpDiscount)), 5, 69, Alignment.Center);
            rpt.Write(string.Format("{0:0.00}", items.Total), 6, 77, Alignment.Right);
            rpt.Write();

        }

        private void WriteReportFooter_A5QMS_WithHeaderNoGST(PlainTextReport rpt, IInvoice data)
        {

            var youHaveSaved = data.MRPTotal - data.PurchasedTotal;
            if (Convert.ToInt32(youHaveSaved) > 0)
            {
                if (Convert.ToInt32(data.DiscountInValue) > 0)
                {
                    youHaveSaved += data.DiscountInValue;
                }
            }
            else
            {
                youHaveSaved = data.DiscountInValue;
            }

            decimal? totReturnAmount = 0;
            decimal? itemTotal = 0;
            rpt.NewLine();
            if (data.SalesReturnItem.Count == 0)
            {
                rpt.Write("Total Amt: " + string.Format("{0:0.00}", data.PurchasedTotal), 18, 1, Alignment.Right);
                rpt.Write("Net Amt: " + string.Format("{0:0.00}", data.NetAmount), 16, 63, Alignment.Right, data.IsBold);
            }
            else
            {
                decimal? totReturnDiscount = 0;
                decimal? ReturnDiscount = 0;
                decimal? rtnGSTTotalValue = 0;

                foreach (var items in data.SalesReturnItem)
                {
                    if (items.ReturnedQuantity > 0)
                    {
                        decimal? getGstPercent = items.GstTotal + 100;
                        decimal? SalesItemDiscount = ((items.Discount / 100) * items.Total);
                        var ActualValue = (items.Total - SalesItemDiscount);
                        rtnGSTTotalValue += ActualValue - ((ActualValue * 100) / getGstPercent);

                        totReturnAmount = totReturnAmount + (items.SellingPrice == null ? items.ProductStock.SellingPrice : items.SellingPrice) * (items.ReturnedQuantity);
                        totReturnDiscount = ((items.Discount / 100) * items.Total);
                        totReturnAmount = totReturnAmount - totReturnDiscount;
                        ReturnDiscount += totReturnDiscount;
                    }
                }

                rpt.Write("Total Amt: " + string.Format("{0:0.00}", data.PurchasedTotal), 18, 1, Alignment.Right);
                rpt.Write("Return Amt: " + string.Format("{0:0.00}", totReturnAmount), 20, 63, Alignment.Right);
                rpt.NewLine();
                itemTotal = data.InvoiceItems.Sum(x => x.Total);
                rpt.Write("Net Amt: " + string.Format("{0:0.00}", (data.NetAmount), MidpointRounding.AwayFromZero), 22, 57, Alignment.Right, data.IsBold);
                //rpt.Write("Net Amt: " + string.Format("{0:0.00}", Math.Round((decimal)((itemTotal) - youHaveSaved), MidpointRounding.AwayFromZero)), 22, 57, Alignment.Right, data.IsBold);
            }

            if (data.SalesReturnItem.Count > 0)
            {
                rpt.Write("Amt. in words Rs." + ((decimal)(data.NetAmount)).ConvertNumbertoWords(), 55, 1, Alignment.Left, TextAdjustment.Wrap);//itemTotal - youHaveSaved
            }
            else
            {
                rpt.NewLine();
                rpt.Write("Amt. in words Rs." + ((decimal)(data.NetAmount)).ConvertNumbertoWords(), 55, 1, Alignment.Left, TextAdjustment.Wrap);
            }
            rpt.Write();
            rpt.Drawline(1);
            rpt.NewLine();

            if (Convert.ToInt32(youHaveSaved) > 0)
            {
                rpt.Write("*************** You have saved Rs.  : " + string.Format("{0:0.00}" + " ***************", youHaveSaved), 80, 1, Alignment.Center);
                rpt.NewLine();
                rpt.Drawline(1);
                rpt.NewLine();
            }
			LaserA5FooterText(rpt, data);

            bool gstSelection = Convert.ToBoolean(data.Instance.Gstselect);
            if (gstSelection)
            {
                rpt.Write("Composite tax payer not eligible to collect taxes", 65, 1, Alignment.Left);
            }
            //Newly added for Print Footer by Mani on 28-02-2017
            GenerateFooter(rpt, 83, data.PrintFooterNote);
        }
        #endregion A5QMS_WithHeaderNoGST

        private void WriteReportEmptyFooter_A5(PlainTextReport rpt, int len, int itemsHeight)
        {
            int emptyBreaks = 0;
            emptyBreaks = 25 - len - 11;

            rpt.Write();
            rpt.Drawline();
            rpt.Write("Continue...", 83, 1, Alignment.Right);
            rpt.NewLine();
            rpt.Write("--EOP--", 83, 1, Alignment.Left);
            rpt.NewLine();
            //if (emptyBreaks > 0)
            //{
            //    for (int i = 0; i < emptyBreaks; i++)
            //        rpt.NewLine();
            //}

        }

        private void WriteReportHeader_A5QMS_WithHeader(PlainTextReport rpt, IInvoice data)
        {
            rpt.NewLine(); //
            var halfMargin = 48;
            rpt.Write(data.Instance.Name.ToUpper(), 38, 1, Alignment.Left, TextAdjustment.Wrap, data.IsBold);
            string adddressarea = string.Empty;
            if (!string.IsNullOrEmpty(data.Instance.Address))
            {
                adddressarea += data.Instance.Address;
            }
            if (!string.IsNullOrEmpty(data.Instance.Area))
            {
                adddressarea += "," + data.Instance.Area;
            }
            if (!string.IsNullOrEmpty(adddressarea))
            {
                rpt.NewLine();
                rpt.Write(adddressarea.ToUpper(), 40, 1, Alignment.Left, TextAdjustment.Wrap);
            }

            rpt.Write("Bill No: " + data.InvoiceSeries + data.InvoiceNo, 20, halfMargin, Alignment.Left, TextAdjustment.Wrap);

            string cityState = string.Empty;
            if (!string.IsNullOrEmpty(data.Instance.City))
            {
                cityState += data.Instance.City + ",";
            }
            if (!string.IsNullOrEmpty(data.Instance.State))
            {
                cityState += data.Instance.State + ",";
            }
            if (!string.IsNullOrEmpty(data.Instance.Pincode))
            {
                cityState += data.Instance.Pincode;
            }

            if (!string.IsNullOrEmpty(cityState))
            {
                rpt.NewLine();
                rpt.Write(cityState.ToUpper(), 40, 1, Alignment.Left, TextAdjustment.Wrap);
            }

            rpt.Write("B Date : " + string.Format("{0:dd/MM/yyyy}", data.InvoiceDate), 20, halfMargin, Alignment.Left, TextAdjustment.Wrap);

            string contact = string.Empty;

            if (!string.IsNullOrEmpty(data.Instance.Phone))
            {
                contact += "Phone No: " + data.Instance.Phone;
            }
            if (!string.IsNullOrEmpty(data.Instance.Mobile))
            {
                contact += "," + "Mobile No: " + data.Instance.Mobile;
            }
            if (!string.IsNullOrEmpty(contact))
            {
                rpt.NewLine();
                rpt.Write(contact.ToUpper(), 40, 1, Alignment.Left, TextAdjustment.Wrap);
            }
            rpt.Write("Name   : " + data.Name.ToUpper(), 20, halfMargin, Alignment.Left, TextAdjustment.Wrap);

            rpt.NewLine();
            rpt.Write("DL No   : " + data.Instance.DrugLicenseNo, 40, 1, Alignment.Left, TextAdjustment.Wrap);
            //rpt.Write("ID     : " + Convert.ToString(data.Patient.EmpID), 20, halfMargin, Alignment.Left, TextAdjustment.Wrap);
            rpt.Write("Mobile : " + data.Mobile, 20, halfMargin, Alignment.Left, TextAdjustment.Wrap);

            rpt.NewLine();
            if (data.TaxRefNo == 1)
                rpt.Write("GSTIN No:" + data.Instance.GsTinNo.ToUpper(), 40, 1, Alignment.Left, TextAdjustment.Wrap);
            else
                rpt.Write("TIN No.:" + data.Instance.TinNo.ToUpper(), 40, 1, Alignment.Left, TextAdjustment.Wrap);
            //rpt.Write("TIN     : " + data.Instance.TinNo, 40, 1, Alignment.Left, TextAdjustment.Wrap);
            rpt.Write("Doctor : " + data.DoctorName.ToUpper(), 20, halfMargin, Alignment.Left, TextAdjustment.Wrap);

            rpt.NewLine();
            rpt.Write("      ", 40, 1, Alignment.Left, TextAdjustment.Wrap);
            rpt.Write("Sale Type : " + data.PaymentType, 20, halfMargin, Alignment.Left, TextAdjustment.Wrap);

            rpt.NewLine();
            //if (gstSelection)
            //{                
            //    rpt.Write("Bill of Supply", 44, 1, Alignment.Center, TextAdjustment.Wrap);
            //    rpt.NewLine();
            //}

            rpt.Drawline(1);

            rpt.NewLine();
            rpt.Write("Rack No", 7, 1, Alignment.Right);
            rpt.Write("Qty.", 4, 9, Alignment.Right);
            rpt.Write("Description", 11, 14);
            rpt.Write("Batch", 5, 40, Alignment.Right);
            rpt.Write("Exp.", 5, 48);
            rpt.Write("M.R.P", 5, 54, Alignment.Right);
            rpt.Write("Rate", 4, 62, Alignment.Right);
            rpt.Write("Disc.%", 6, 68, Alignment.Right);
            rpt.Write("Amount", 6, 76, Alignment.Right);
            rpt.Write();
            rpt.Drawline(1);
        }

        private void WriteReportItems_A5QMS_WithHeader(PlainTextReport rpt, IInvoice data)
        {
            int len = 0;
            int itemsHeight = 7;
            bool gstSelection = Convert.ToBoolean(data.Instance.Gstselect);
            List<IInvoiceItem> groupedList = new List<IInvoiceItem>();

            foreach (var items in data.InvoiceItems)
            {

                if (items.Total == 0)   // Added by San - 06-05-2017
                {
                    items.Quantity = items.ReturnedQty;
                }

                //if (items.Quantity <= 0) // Commented by San - 06-05-2017
                //{ break; }
                rpt.NewLine();
                len++;

                if (groupedList.Count() != itemsHeight)
                {
                    groupedList.Add(items);
                    WriteReportItemsforA5(rpt, items);
                }
                else if (groupedList.Count() == itemsHeight && data.InvoiceItems.Count() > 1)
                {
                    if (len < data.InvoiceItems.Count())
                    {
                        WriteReportEmptyFooter_A5(rpt, groupedList.Count(), itemsHeight);
                    }
                    groupedList.RemoveAll(y => true);
                    groupedList.Add(items);
                    rpt.NewLine();
                    rpt.NewLine();

                    rpt.Write("RETAIL INVOICE", 80, 1, Alignment.Center);
                    rpt.NewLine();
                    rpt.Drawline(1);
                    WriteReportHeader_A5QMS_WithHeader(rpt, data);
                    WriteReportItemsforA5(rpt, items);
                }

            }

            for (int i = 0; i < 5 - len; i++)
                rpt.NewLine();

            rpt.Write();
            rpt.Drawline(1);
        }

        private void WriteReportItemsforA5(PlainTextReport rpt, IInvoiceItem items)
        {
            rpt.Write((items.ProductStock.RackNo.Trim().Length > 0 ? items.ProductStock.RackNo.ToUpper() : "  "), 4, 1, Alignment.Right);
            rpt.Write(string.Format("{0:0}", items.Quantity), 4, 7, Alignment.Right);
            rpt.Write(items.ProductStock.Product.Name.ToUpper(), 25, 14, Alignment.Left, TextAdjustment.Wrap);
            rpt.Write(items.ProductStock.BatchNo, 6, 39, Alignment.Center);
            rpt.Write(string.Format("{0:MM/yy}", items.ProductStock.ExpireDate), 5, 48, Alignment.Center);

            if (items.ProductStock.MRP > 0)
            {
                rpt.Write(string.Format("{0:0.00}", items.ProductStock.MRP), 6, 54, Alignment.Right);
            }
            else
            {
                if (items.SellingPrice > 0)
                {
                    decimal? vatPrice = ((items.SellingPrice * 100) / (items.ProductStock.VAT + 100));
                    rpt.Write(string.Format("{0:0.00}", items.SellingPrice), 6, 54, Alignment.Right);
                }
                else
                {
                    decimal? vatPrice = items.ProductStock.SellingPrice - items.ProductStock.VatInPrice;
                    rpt.Write(string.Format("{0:0.00}", items.ProductStock.SellingPrice), 6, 54, Alignment.Right);
                }
            }

            if (items.SellingPrice > 0)
            {
                decimal? vatPrice = ((items.SellingPrice * 100) / (items.ProductStock.VAT + 100));
                rpt.Write(string.Format("{0:0.00}", items.SellingPrice), 6, 61, Alignment.Right);
            }
            else
            {
                decimal? vatPrice = items.ProductStock.SellingPrice - items.ProductStock.VatInPrice;
                rpt.Write(string.Format("{0:0.00}", items.ProductStock.SellingPrice), 6, 61, Alignment.Right);
            }

            var tmpMRP = items.Quantity * items.ProductStock.MRP;
            var tmpSellPrice = items.Quantity * items.ProductStock.SellingPrice;
            decimal? tmpDiscount = 0;
            if (tmpMRP == 0)
                tmpDiscount = 0;
            else
                tmpDiscount = (((tmpMRP - tmpSellPrice) / tmpMRP) * 100);


            rpt.Write(string.Format("{0:0.00}", (tmpDiscount == null ? 0 : tmpDiscount)), 5, 69, Alignment.Center);
            rpt.Write(string.Format("{0:0.00}", items.Total), 6, 77, Alignment.Right);
            rpt.Write();

        }

        private void WriteReportFooter_A5QMS_WithHeader(PlainTextReport rpt, IInvoice data)
        {
            bool gstSelection = Convert.ToBoolean(data.Instance.Gstselect);
            var youHaveSaved = data.MRPTotal - data.PurchasedTotal;
            if (Convert.ToInt32(youHaveSaved) > 0)
            {
                if (Convert.ToInt32(data.DiscountInValue) > 0)
                {
                    youHaveSaved += data.DiscountInValue;
                }
            }
            else
            {
                youHaveSaved = data.DiscountInValue;
            }

            decimal? totReturnAmount = 0;
            decimal? itemTotal = 0;
            rpt.NewLine();
            if (data.SalesReturnItem.Count == 0)
            {
                //rpt.Write("Total Amt: " + string.Format("{0:0.00}", data.PurchasedTotal), 18, 5, Alignment.Right);
                rpt.Write("Total Amt: " + string.Format("{0:0.00}", data.PurchasedTotal), 18, 1, Alignment.Right);
                if (data.TaxRefNo == 1)
                {
                    if (data.CgstTotalValue != 0 && data.SgstTotalValue != 0 && !gstSelection)
                    {
                        rpt.Write("CGST:" + string.Format("{0:0.00}", data.CgstTotalValue), 12, 20, Alignment.Right);
                        rpt.Write((data.Instance.isUnionTerritory == true ? "UTGST:" : "SGST:") + string.Format("{0:0.00}", data.SgstTotalValue), 12, 34, Alignment.Right);
                    }
                    else if(!gstSelection)
                    {
                        rpt.Write("CGST:" + string.Format("{0:0.00}", data.CgstTotalValue), 12, 20, Alignment.Right);
                        rpt.Write((data.Instance.isUnionTerritory == true ? "UTGST:" : "SGST:") + string.Format("{0:0.00}", data.SgstTotalValue), 12, 34, Alignment.Right);
                    }
                }
                rpt.Write("Net Amt: " + string.Format("{0:0.00}", data.NetAmount), 16, 63, Alignment.Right, data.IsBold);
            }
            else
            {
                decimal? totReturnDiscount = 0;
                decimal? ReturnDiscount = 0;
                decimal? rtnGSTTotalValue = 0;

                foreach (var items in data.SalesReturnItem)
                {
                    if (items.ReturnedQuantity > 0)
                    {                       
                        decimal? getGstPercent = items.GstTotal + 100;
                        decimal? SalesItemDiscount = ((items.Discount / 100) * items.Total);
                        var ActualValue = (items.Total - SalesItemDiscount);
                        rtnGSTTotalValue += ActualValue - ((ActualValue * 100) / getGstPercent);

                        totReturnAmount = totReturnAmount + (items.SellingPrice == null ? items.ProductStock.SellingPrice : items.SellingPrice) * (items.ReturnedQuantity);
                        totReturnDiscount = ((items.Discount / 100) * items.Total);
                        totReturnAmount = totReturnAmount - totReturnDiscount;
                        ReturnDiscount += totReturnDiscount;

                        //totReturnAmount = totReturnAmount + (items.SellingPrice == null ? items.ProductStock.SellingPrice : items.SellingPrice) * (items.ReturnedQuantity);
                    }
                }
                decimal? RtntotgstValue = rtnGSTTotalValue / 2;
                rpt.Write("Total Amt: " + string.Format("{0:0.00}", data.PurchasedTotal), 18, 1, Alignment.Right);
                if (data.TaxRefNo == 1)
                {
                    if (data.CgstTotalValue != 0 && data.SgstTotalValue != 0 && !gstSelection)
                    {
                        rpt.Write("CGST:" + string.Format("{0:0.00}", (data.CgstTotalValue - RtntotgstValue)), 12, 20, Alignment.Right);
                        rpt.Write((data.Instance.isUnionTerritory == true ? "UTGST:" : "SGST:") + string.Format("{0:0.00}", (data.SgstTotalValue - RtntotgstValue)), 12, 32, Alignment.Right);
                    }
                    else if (!gstSelection)
                    {
                        rpt.Write("CGST:" + string.Format("{0:0.00}", data.CgstTotalValue), 12, 20, Alignment.Right);
                        rpt.Write((data.Instance.isUnionTerritory == true ? "UTGST:" : "SGST:") + string.Format("{0:0.00}", data.SgstTotalValue), 12, 32, Alignment.Right);
                    }
                }
                rpt.Write("Return Amt: " + string.Format("{0:0.00}", totReturnAmount), 20, 63, Alignment.Right);
                rpt.NewLine();
                itemTotal = data.InvoiceItems.Sum(x => x.Total);
                rpt.Write("Net Amt: " + string.Format("{0:0.00}", (data.NetAmount), MidpointRounding.AwayFromZero), 22, 57, Alignment.Right, data.IsBold);  //(itemTotal - totReturnAmount)
            }      

            if (data.SalesReturnItem.Count > 0)
            {
                rpt.Write("Amt. in words Rs." + ((decimal)(data.NetAmount)).ConvertNumbertoWords(), 55, 1, Alignment.Left, TextAdjustment.Wrap);//itemTotal - youHaveSaved
            }
            else
            {
                rpt.NewLine();
                rpt.Write("Amt. in words Rs." + ((decimal)(data.NetAmount)).ConvertNumbertoWords(), 55, 1, Alignment.Left, TextAdjustment.Wrap);
            }
            rpt.Write();
            rpt.Drawline(1);
            rpt.NewLine();

            if (Convert.ToInt32(youHaveSaved) > 0)
            {
                rpt.Write("*************** You have saved Rs.  : " + string.Format("{0:0.00}" + " ***************", youHaveSaved), 80, 1, Alignment.Center);
                rpt.NewLine();
                rpt.Drawline(1);
                rpt.NewLine();
            }
            LaserA5FooterText(rpt,data);
        }

        private void WriteReportOnlyReturnItems_A5QMS_WithHeader(PlainTextReport rpt, IInvoice data)
        {
            int len = 0;
            int itemsHeight = 7;
            bool gstSelection = Convert.ToBoolean(data.Instance.Gstselect);
            List<SalesReturnItem> groupedList = new List<SalesReturnItem>();

            foreach (var items in data.SalesReturnItem)
            {
                if (items.ReturnedQty != null)
                    items.Quantity = items.ReturnedQty;

                rpt.NewLine();
                len++;

                if (groupedList.Count() != itemsHeight)
                {
                    groupedList.Add(items);
                    WriteReportOnlyReturnedItemsforA5(rpt, items);
                }
                else if (groupedList.Count() == itemsHeight && data.SalesReturnItem.Count() > 1)
                {
                    if (len < data.SalesReturnItem.Count())
                    {
                        WriteReportEmptyFooter_A5(rpt, groupedList.Count(), itemsHeight);
                    }
                    groupedList.RemoveAll(y => true);
                    groupedList.Add(items);
                    rpt.NewLine();
                    rpt.NewLine();

                    rpt.Write("RETAIL INVOICE", 80, 1, Alignment.Center);
                    rpt.NewLine();
                    rpt.Drawline(1);
                    WriteReportHeader_A5QMS_WithHeader(rpt, data);
                    WriteReportOnlyReturnedItemsforA5(rpt, items);
                }

            }

            for (int i = 0; i < 5 - len; i++)
                rpt.NewLine();

            rpt.Write();
            rpt.Drawline(1);
        }

        private void WriteReportOnlyReturnedItemsforA5(PlainTextReport rpt, SalesReturnItem items)
        {
            rpt.Write((items.ProductStock.RackNo.Trim().Length > 0 ? items.ProductStock.RackNo.ToUpper() : "  "), 4, 1, Alignment.Right);
            rpt.Write(string.Format("{0:0}", items.ReturnedQuantity), 4, 7, Alignment.Right);
            rpt.Write(items.ProductStock.Product.Name.ToUpper(), 25, 14, Alignment.Left, TextAdjustment.Wrap);
            rpt.Write(items.ProductStock.BatchNo, 6, 39, Alignment.Center);
            rpt.Write(string.Format("{0:MM/yy}", items.ProductStock.ExpireDate), 5, 48, Alignment.Center);

            if (items.MRP > 0)
            {
                rpt.Write(string.Format("{0:0.00}", items.MRP), 6, 54, Alignment.Right);
            }
            else
            {
                rpt.Write(string.Format("{0:0.00}", items.ProductStock.MRP), 6, 54, Alignment.Right);

                //    if (items.SellingPrice > 0)
                //    {
                //        decimal? vatPrice = ((items.SellingPrice * 100) / (items.ProductStock.VAT + 100));
                //        rpt.Write(string.Format("{0:0.00}", items.SellingPrice), 6, 54, Alignment.Right);
                //    }
                //    else
                //    {
                //        decimal? vatPrice = items.ProductStock.SellingPrice - items.ProductStock.VatInPrice;
                //        rpt.Write(string.Format("{0:0.00}", items.ProductStock.SellingPrice), 6, 54, Alignment.Right);
                //    }
            }

            if (items.SellingPrice > 0)
            {
                decimal? vatPrice = ((items.SellingPrice * 100) / (items.ProductStock.VAT + 100));
                rpt.Write(string.Format("{0:0.00}", items.SellingPrice), 6, 61, Alignment.Right);
            }
            else
            {
                decimal? vatPrice = items.ProductStock.SellingPrice - items.ProductStock.VatInPrice;
                rpt.Write(string.Format("{0:0.00}", items.ProductStock.SellingPrice), 6, 61, Alignment.Right);
            }

            var tmpMRP = items.ReturnedQuantity * (items.MRP > 0 ? items.MRP : items.ProductStock.MRP);
            var tmpSellPrice = items.ReturnedQuantity * (items.SellingPrice > 0 ? items.SellingPrice : items.ProductStock.SellingPrice);

            decimal? tmpDiscount = 0;
            if (tmpMRP == 0)
                tmpDiscount = 0;
            else
                tmpDiscount = (((tmpMRP - tmpSellPrice) / tmpMRP) * 100);

            rpt.Write(string.Format("{0:0.00}", (tmpDiscount == null ? 0 : tmpDiscount)), 5, 69, Alignment.Center);
            rpt.Write(string.Format("{0:0.00}", tmpSellPrice), 6, 77, Alignment.Right);
            rpt.Write();
        }

        private void WriteReportOnlyReturnFooter_A5QMS_WithHeader(PlainTextReport rpt, IInvoice data)
        {
            decimal? totReturnAmount = 0;
            decimal? totReturnDiscount = 0;
            decimal? ReturnDiscount = 0;
            bool gstSelection = Convert.ToBoolean(data.Instance.Gstselect);
            
            foreach (var items in data.SalesReturnItem)
            {
                totReturnAmount = totReturnAmount + (items.SellingPrice == null ? items.ProductStock.SellingPrice : items.SellingPrice) * (items.ReturnedQuantity);
                totReturnDiscount = ((items.Discount / 100) * items.OnlyReturnAmount);
                totReturnAmount = totReturnAmount - totReturnDiscount;
                ReturnDiscount += totReturnDiscount;

                items.IgstValue = ((items.OnlyReturnAmount - totReturnDiscount) - (((items.OnlyReturnAmount - totReturnDiscount) * 100) / (items.Igst + 100)));
                decimal? getGst = ((items.OnlyReturnAmount - totReturnDiscount) - (((items.OnlyReturnAmount - totReturnDiscount) * 100) / ((items.Cgst + items.Sgst) + 100)));
                items.CgstValue = (getGst / 2);
                items.SgstValue = (getGst / 2);
            }            
            rpt.NewLine();
            if (data.TaxRefNo == 1 && !gstSelection)
            {
                rpt.Write("CGST: " + string.Format("{0:0.00}", data.SalesReturnItem.Sum(z => z.CgstValue)), 13, 2);
                rpt.Write((data.Instance.isUnionTerritory == true ? "UTGST: " : "SGST: ") + string.Format("{0:0.00}", data.SalesReturnItem.Sum(z => z.SgstValue)), 13, 15);
                if (data.SalesReturn.ReturnCharges > 0 && data.SalesReturn.ReturnCharges != null)
                    rpt.Write("Ret. Charges: " + string.Format("{0:0.00}", data.SalesReturn.ReturnCharges), 20, 30);
            }

            if (data.SalesReturn.ReturnCharges > 0 && data.SalesReturn.ReturnCharges != null)
            {
                rpt.Write("Return Amt: " + string.Format("{0:0.00}", Math.Round((decimal)(totReturnAmount - data.SalesReturn.ReturnCharges), MidpointRounding.AwayFromZero)), 22, 61, Alignment.Right, data.IsBold);
                rpt.NewLine();
                rpt.Write("Amt. in words Rs." + ((decimal)(totReturnAmount - data.SalesReturn.ReturnCharges)).ConvertNumbertoWords(), 63, 1, Alignment.Left, TextAdjustment.Wrap);
            }
            else
            {
                //rpt.Write("Return Amt: " + string.Format("{0:0.00}", Math.Round((decimal)(totReturnAmount), MidpointRounding.AwayFromZero)), 22, 61, Alignment.Right, data.IsBold);
                //rpt.NewLine();
                //rpt.Write("Amt. in words Rs." + ((decimal)(totReturnAmount)).ConvertNumbertoWords(), 63, 1, Alignment.Left, TextAdjustment.Wrap);
                rpt.Write("Return Amt: " + string.Format("{0:0.00}", (data.NetAmount)), 22, 61, Alignment.Right, data.IsBold);
                rpt.NewLine();
                rpt.Write("Amt. in words Rs." + data.InWords, 63, 1, Alignment.Left, TextAdjustment.Wrap);
            }
                


            rpt.Write();
            rpt.Drawline(1);
            rpt.NewLine();
            var youHaveSaved = data.MRPTotal - data.PurchasedTotal;
            if (Convert.ToInt32(youHaveSaved) > 0)
            {
                if (Convert.ToInt32(data.DiscountInValue) > 0)
                {
                    youHaveSaved += data.DiscountInValue;
                }
            }
            else
            {
                youHaveSaved = data.DiscountInValue;
            }
            if (Convert.ToInt32(youHaveSaved) > 0)
            {
                rpt.Write("*************** You have saved Rs.  : " + string.Format("{0:0.00}" + " ***************", youHaveSaved), 80, 1, Alignment.Center);
                rpt.NewLine();
                rpt.Drawline(1);
                rpt.NewLine();
            }
            LaserA5FooterText(rpt, data);
        }

        private void WriteReportOnlyReturnFooter_A5QMS_WithHeaderNoGST(PlainTextReport rpt, IInvoice data)
        {
            decimal? totReturnAmount = 0;
            decimal? totReturnDiscount = 0;
            decimal? ReturnDiscount = 0;

            rpt.NewLine();
            foreach (var items in data.SalesReturnItem)
            {
                totReturnAmount = totReturnAmount + (items.SellingPrice == null ? items.ProductStock.SellingPrice : items.SellingPrice) * (items.ReturnedQuantity);
                totReturnDiscount = ((items.Discount / 100) * (items.SellingPrice == null ? items.ProductStock.SellingPrice : items.SellingPrice) * (items.ReturnedQuantity));
                totReturnAmount = totReturnAmount - totReturnDiscount;
                ReturnDiscount += totReturnDiscount;
            }
            if (data.SalesReturn.ReturnCharges > 0 && data.SalesReturn.ReturnCharges != null)
                rpt.Write("Ret. Charges: " + string.Format("{0:0.00}", data.SalesReturn.ReturnCharges), 25, 1);

            if (data.SalesReturn.ReturnCharges > 0 && data.SalesReturn.ReturnCharges != null)
            {
                rpt.Write("Return Amt: " + string.Format("{0:0.00}", Math.Round((decimal)(totReturnAmount - data.SalesReturn.ReturnCharges), MidpointRounding.AwayFromZero)), 22, 61, Alignment.Right, data.IsBold);
                rpt.NewLine();
                rpt.Write("Amt. in words Rs." + ((decimal)(totReturnAmount - data.SalesReturn.ReturnCharges)).ConvertNumbertoWords(), 63, 1, Alignment.Left, TextAdjustment.Wrap);
            }
            else
            {
                //rpt.Write("Return Amt: " + string.Format("{0:0.00}", Math.Round((decimal)(totReturnAmount), MidpointRounding.AwayFromZero)), 22, 61, Alignment.Right, data.IsBold);
                //rpt.NewLine();
                //rpt.Write("Amt. in words Rs." + ((decimal)(totReturnAmount)).ConvertNumbertoWords(), 63, 1, Alignment.Left, TextAdjustment.Wrap);
                rpt.Write("Return Amt: " + string.Format("{0:0.00}",(data.NetAmount)), 22, 61, Alignment.Right, data.IsBold);
                rpt.NewLine();
                rpt.Write("Amt. in words Rs." + data.InWords, 63, 1, Alignment.Left, TextAdjustment.Wrap);
            }


            rpt.Write();
            rpt.Drawline(1);
            rpt.NewLine();
            var youHaveSaved = data.MRPTotal - data.PurchasedTotal;
            if (Convert.ToInt32(youHaveSaved) > 0)
            {
                if (Convert.ToInt32(data.DiscountInValue) > 0)
                {
                    youHaveSaved += data.DiscountInValue;
                }
            }
            else
            {
                youHaveSaved = data.DiscountInValue;
            }
            if (Convert.ToInt32(youHaveSaved) > 0)
            {
                rpt.Write("*************** You have saved Rs.  : " + string.Format("{0:0.00}" + " ***************", youHaveSaved), 80, 1, Alignment.Center);
                rpt.NewLine();
                rpt.Drawline(1);
                rpt.NewLine();
            }

            LaserA5FooterText(rpt, data);
        }

        #endregion

        #region 4InchRoll_NonPharma_Generic _template New

        protected string GenerateSalesReport_4InchRollNew_NonPharma(IInvoice data)
        {
            var rpt = new PlainTextReport
            {
                Columns = 38,
                Rows = 30
            };

            //some leading blank lines
            rpt.NewLine();
            rpt.NewLine();

            WriteReportHeader_4InchRollNew_NonPharma(rpt, data);

            if (data.InvoiceItems.Count() == 0)
            {
                WriteReportOnlyReturnItems_4InchRollNew_NonPharma(rpt, data);
            }
            else
            {
                WriteReportItems_4InchRollNew_NonPharma(rpt, data);
                WriteReportFooter_4InchRollNew_NonPharma(rpt, data);
            }

            //some trailing blank lines
            rpt.NewLine();
            rpt.NewLine();
            rpt.Write();

            return rpt.Content.ToString();
        }

        private void WriteReportHeader_4InchRollNew_NonPharma(PlainTextReport rpt, IInvoice data)
        {
            rpt.NewLine();
            bool gstSelection = Convert.ToBoolean(data.Instance.Gstselect);
            //rpt.Write("#BB#"+data.Instance.Name.ToUpper()+"#BE#", 36, 1, Alignment.Center);
            rpt.Write(data.Instance.Name.ToUpper(), 36, 1, Alignment.Center, data.IsBold);
            string adddressarea = string.Empty;
            if (!string.IsNullOrEmpty(data.Instance.Address))
            {
                adddressarea += data.Instance.Address;
            }
            if (!string.IsNullOrEmpty(data.Instance.Area))
            {
                adddressarea += "," + data.Instance.Area;
            }
            if (!string.IsNullOrEmpty(adddressarea))
            {
                rpt.NewLine();
                rpt.Write(adddressarea.ToUpper(), 36, 1, Alignment.Center, TextAdjustment.Wrap);
            }
            string cityState = string.Empty;
            if (!string.IsNullOrEmpty(data.Instance.City))
            {
                cityState += data.Instance.City + ",";
            }
            if (!string.IsNullOrEmpty(data.Instance.State))
            {
                cityState += data.Instance.State + ",";
            }
            if (!string.IsNullOrEmpty(data.Instance.Pincode))
            {
                cityState += data.Instance.Pincode;
            }

            if (!string.IsNullOrEmpty(cityState))
            {
                rpt.NewLine();
                rpt.Write(cityState.ToUpper(), 36, 1, Alignment.Center, TextAdjustment.Wrap);
            }
            string contact = string.Empty;

            if (!string.IsNullOrEmpty(data.Instance.Phone))
            {
                contact += "Ph: " + data.Instance.Phone;
            }
            if (!string.IsNullOrEmpty(data.Instance.Mobile))
            {
                contact += "," + "Mobile: " + data.Instance.Mobile;
            }
            if (!string.IsNullOrEmpty(contact))
            {
                rpt.NewLine();
                rpt.Write(contact.ToUpper(), 36, 1, Alignment.Center, TextAdjustment.Wrap);
            }
            ////rpt.WriteLine();
            //rpt.NewLine();

            //rpt.Write("DL No.:" + data.Instance.DrugLicenseNo.ToUpper(), 36, 1, Alignment.Center, TextAdjustment.Wrap);

            rpt.NewLine();
            if (data.TaxRefNo == 1)
                rpt.Write("GSTIN No:" + data.Instance.GsTinNo.ToUpper(), 34, 1, Alignment.Center, TextAdjustment.Wrap);
            else
                rpt.Write("TIN No.:" + data.Instance.TinNo.ToUpper(), 36, 1, Alignment.Center, TextAdjustment.Wrap);

            rpt.NewLine();
            rpt.Write("Date : " + string.Format("{0:dd/MM/yy}", data.InvoiceDate), 18, 1);

            //rpt.Write("INVOICE" + data.InvoiceNo, 7, 22);

            if (data.InvoiceItems.Count() == 0)
            {
                rpt.Write("Return No: " + data.InvoiceSeries + data.InvoiceNo, 16, 20);
            }
            else
            {
                rpt.Write("Bill No.: " + data.InvoiceSeries + data.InvoiceNo, 16, 20);
            }


            //rpt.Write("Bill No: " + data.InvoiceSeries + " " + data.InvoiceNo, 18, 31);
            rpt.NewLine();
            rpt.Write("Time : " + Convert.ToDateTime(data.SalesCreatedAt).ToString("hh:mm tt"), 18, 1);
            rpt.Write("B.Type: " + data.PaymentType, 16, 20);
            ////rpt.WriteLine();
            //rpt.NewLine();
            //rpt.Write("DOCTOR  : " + data.DoctorName.ToUpper(), 36, 1, TextAdjustment.Wrap);
            ////rpt.Write("Bill Type: " + data.PaymentType, 15, 33);

            rpt.NewLine();
            rpt.Write("Customer : " + data.Name.ToUpper(), 36, 1, TextAdjustment.Wrap);
            if (gstSelection)
            {
                rpt.NewLine();
                rpt.Write("Bill of Supply", 44, 1, Alignment.Center, TextAdjustment.Wrap);
            }
            //rpt.NewLine();
            //rpt.Write("Address : " + data.Address, 36, 1, TextAdjustment.Wrap);

            rpt.NewLine();
            rpt.Drawline();
            rpt.Write("#", 3, 1);
            rpt.Write("Product", 13, 4);
            ////rpt.WriteLine();


            ////rpt.Write("Batch", 8, 28);
            ////rpt.NewLine();
            ///rpt.Write("Exp", 6, 4);
            rpt.Write("MRP", 7, 17, Alignment.Right);
            rpt.Write("Qty", 4, 26, Alignment.Right);
            //rpt.Write("Value", 9, 39, Alignment.Right);
            rpt.Write("Value", 8, 30, Alignment.Right);

            rpt.NewLine();
            rpt.Drawline();

        }

        private void WriteReportItems_4InchRollNew_NonPharma(PlainTextReport rpt, IInvoice data)
        {
            int len = 0;
            foreach (var items in data.InvoiceItems)
            {
                if (items.Quantity <= 0)
                {
                    break;
                }
                rpt.NewLine();
                rpt.Write((len + 1).ToString(), 3, 1);

                if (items.ProductStock.Product.Name.Length > 13)
                {
                    rpt.Write(items.ProductStock.Product.Name.ToUpper().Substring(0, 13), 13, 4);
                }
                else
                {
                    rpt.Write(items.ProductStock.Product.Name.ToUpper(), 13, 4);
                }

                //var batchNo = items.ProductStock.BatchNo.ToUpper();
                //if (batchNo.Length > 7)
                //    rpt.Write(items.ProductStock.BatchNo.ToUpper().Substring(items.ProductStock.BatchNo.Length - 7), 7, 20);
                //else
                //    rpt.Write(items.ProductStock.BatchNo.ToUpper(), 7, 20);

                //rpt.NewLine();
                // rpt.Write(string.Format("{0:MM/yy}", items.ProductStock.ExpireDate), 6, 4, Alignment.Right);

                rpt.Write(string.Format("{0:0.00}", items.SellingPrice == null ? items.ProductStock.SellingPrice : items.SellingPrice), 7, 18, Alignment.Right);
                rpt.Write(string.Format("{0:0}", items.Quantity), 4, 27, Alignment.Center);
                rpt.Write(string.Format("{0:0.00}", ((items.SellingPrice == null ? items.ProductStock.SellingPrice : items.SellingPrice) * items.Quantity)), 8, 30, Alignment.Right);

                len++;

            }
            rpt.Write();
            rpt.Drawline();
        }

        private void WriteReportFooter_4InchRollNew_NonPharma(PlainTextReport rpt, IInvoice data)
        {
            bool gstSelection = Convert.ToBoolean(data.Instance.Gstselect);

            if (data.SalesReturnItem.Count == 0)
            {
                rpt.NewLine();

                rpt.Write("Disc.:" + string.Format("{0:0.00}", data.DiscountInValue), 13, 1);
                //rpt.Write("R.Off: " + string.Format("{0:0.00}", data.RoundOff - data.Net), 12, 15); 
                if (!gstSelection)
                {
                    if (data.TaxRefNo == 1)
                    {
                        rpt.Write("CGST:" + string.Format("{0:0.00}", Math.Round((decimal)data.CgstTotalValue, 2)), 12, 14);
                        rpt.Write((data.Instance.isUnionTerritory == true ? "UTGST:" : "SGST:") + string.Format("{0:0.00}", Math.Round((decimal)data.SgstTotalValue, 2)), 12, 26);
                    }
                    else
                    {
                        rpt.Write("T.VAT", 9, 16);
                        rpt.Write(string.Format("{0:0.00}", data.Vat), 09, 29, Alignment.Right);
                    }
                }
                if (data.SalesReturnItem.Count == 0)
                {
                    rpt.NewLine();
                    rpt.Write("TOTAL", 9, 16);
                    rpt.Write(string.Format("{0:0.00}", data.NetAmount), 11, 25, Alignment.Right, data.IsBold);
                }
                else
                {
                    if (data.TaxRefNo != 1)
                    {
                        rpt.NewLine();
                        rpt.Write("TOTAL", 9, 16);
                        rpt.Write(string.Format("{0:0.00}", data.Net), 11, 25, Alignment.Right, data.IsBold);
                    }
                }
                 
                rpt.Write();
                rpt.Drawline(1);
                 
                if (data.SalesReturnItem.Count == 0 && data.TaxRefNo == 1)
                {
                    rpt.NewLine();                     
                }
            }
            if (data.SalesReturnItem.Count > 0)
            {
                string payableAmount = "";
                WriteReportReturnItems_4InchRollNew_NonPharma(rpt, data, out payableAmount);
                data.InWords = payableAmount;
            }

            //rpt.WriteLine();
            rpt.NewLine();
            rpt.Write(string.Format("Rupees {0} only.", data.InWords), 36, 1, TextAdjustment.Wrap);
            if (gstSelection)
            {
                rpt.NewLine();
                rpt.Write("Composite tax payer not eligible to collect taxes", 36, 1, TextAdjustment.Wrap);
                rpt.NewLine();
            }
            //rpt.WriteLine();
            rpt.NewLine();
            rpt.Write("*** THANK YOU ***", 36, 1, Alignment.Center);

            //Newly added for Print Footer by Mani on 28-02-2017
            GenerateFooter(rpt, 35, data.PrintFooterNote);

        }

        private void WriteReportReturnItems_4InchRollNew_NonPharma(PlainTextReport rpt, IInvoice data, out string payableAmount)
        {
            rpt.Write("RETURNS", 8, 0, Alignment.Center, TextAdjustment.Wrap);
            rpt.NewLine();
            bool gstSelection = Convert.ToBoolean(data.Instance.Gstselect);
            decimal? totReturnAmount = 0;
            decimal? totReturnDiscount = 0;
            decimal? ReturnDiscount = 0;
            decimal? returnAmt = 0;
            int len = 0;
            decimal? rtnGSTTotalValue = 0;

            foreach (var items in data.SalesReturnItem)
            {
                if (items.ReturnedQuantity > 0)
                {
                    rpt.NewLine();
                    rpt.Write((len + 1).ToString(), 3, 1);

                    if (items.ProductStock.Product.Name.Length > 13)
                    {
                        rpt.Write(items.ProductStock.Product.Name.ToUpper().Substring(0, 13), 13, 4);
                    }
                    else
                    {
                        rpt.Write(items.ProductStock.Product.Name.ToUpper(), 13, 4);
                    }
                    totReturnAmount += items.ReturnAmt;
                    returnAmt += items.ReturnAmt;
                    decimal? getGstPercent = items.GstTotal + 100;
                    decimal? SalesItemDiscount = ((items.Discount / 100) * items.Total);

                    rpt.Write(string.Format("{0:0.00}", items.SellingPrice == null ? items.ProductStock.SellingPrice : items.SellingPrice), 7, 18, Alignment.Right);
                    rpt.Write(string.Format("{0:0}", items.ReturnedQuantity), 4, 27, Alignment.Center);
                    rpt.Write(string.Format("{0:0.00}", ((items.SellingPrice == null ? items.ProductStock.SellingPrice : items.SellingPrice) * items.ReturnedQuantity)), 8, 30, Alignment.Right);

                    len++;

                    var ActualValue = (items.Total - SalesItemDiscount);
                    rtnGSTTotalValue += ActualValue - ((ActualValue * 100) / getGstPercent);

                    totReturnDiscount = ((items.Discount / 100) * items.Total);
                    totReturnAmount = totReturnAmount - totReturnDiscount;
                    ReturnDiscount += totReturnDiscount;
                }
            }
            rpt.NewLine();
            rpt.NewLine();

            rpt.Write();
            rpt.Drawline(1);            
            rpt.NewLine();

            decimal? totValue = data.InvoiceItems.Sum(x => x.Total);
            decimal? totCgstValue = data.InvoiceItems.Sum(x => x.CgstValue);
            decimal? totSgstValue = data.InvoiceItems.Sum(x => x.SgstValue);
            decimal? RtntotgstValue = rtnGSTTotalValue / 2;

            if (!gstSelection)
            {
                rpt.Write("CGST:" + string.Format("{0:0.00}", (totCgstValue - RtntotgstValue)), 12, 0);
                rpt.Write((data.Instance.isUnionTerritory == true ? "UTGST:" : "SGST:") + string.Format("{0:0.00}", (totSgstValue - RtntotgstValue)), 12, 13);
            }
                       
            //rpt.Write("CGST:" + string.Format("{0:0.00}", Math.Round((decimal)data.CgstTotalValue, 2)), 12, 0);
            //rpt.Write("SGST:" + string.Format("{0:0.00}", Math.Round((decimal)data.SgstTotalValue, 2)), 12, 13);

            rpt.Write("TOTAL:", 6, 25);
            rpt.Write(string.Format("{0:0.00}", (totValue - returnAmt)), 9, 28, Alignment.Right);

            rpt.NewLine();
            decimal retAmount = data.RoundoffNetAmount as decimal? ?? 0; //RoundOffCalculation(data.Net, totReturnAmount);
            rpt.Write("R.Off: " + string.Format("{0:0.00}", retAmount), 12, 25, Alignment.Right);

            rpt.NewLine();
            rpt.Write("Disc.: " + string.Format("{0:0.00}", (data.DiscountInValue - ReturnDiscount)), 12, 25, Alignment.Right);

            rpt.NewLine();
            rpt.Write("RETURN AMT ", 14, 14);
            rpt.Write(string.Format("{0:0.00}", totReturnAmount), 11, 25, Alignment.Right, data.IsBold);

            rpt.Write();
            rpt.Drawline(1);
            rpt.NewLine();
            rpt.Write("PAYABLE AMOUNT ", 19, 10);
            rpt.Write(string.Format("{0:0.00}", data.NetAmount), 11, 25, Alignment.Right, data.IsBold);

            rpt.Write();
            rpt.Drawline(1);
            payableAmount = ((decimal)(data.NetAmount)).ConvertNumbertoWords();
        }
        #endregion

        //private decimal RoundOffCalculation(decimal? Net, decimal? totReturnAmount)
        //{
        //    decimal? Retval = Math.Round((decimal)Net - (decimal)totReturnAmount, MidpointRounding.AwayFromZero);
        //    decimal? RetroundOff = Net - totReturnAmount;

        //    return (decimal)(Retval - RetroundOff);
        //}

        //Only Return Items - By San 07-06-2017
        private void WriteReportOnlyReturnItems_4InchRollNew(PlainTextReport rpt, IInvoice data)
        {

            decimal? totReturnAmount = 0;
            decimal? totReturnDiscount = 0;
            decimal? ReturnDiscount = 0;
            int len = 0;

            foreach (var items in data.SalesReturnItem)
            {
                if (items.ReturnedQuantity > 0)
                {
                    rpt.NewLine();
                    rpt.Write((len + 1).ToString(), 3, 1);

                    if (items.ProductStock.Product.Name.Length > 27)
                    {
                        rpt.Write(items.ProductStock.Product.Name.ToUpper().Substring(0, 27), 27, 4);
                    }
                    else
                    {
                        rpt.Write(items.ProductStock.Product.Name.ToUpper(), 27, 4);
                    }
                    totReturnAmount += items.ReturnAmt;
                    var batchNo = items.ProductStock.BatchNo.ToUpper();
                    if (batchNo.Length > 8)
                        rpt.Write(items.ProductStock.BatchNo.ToUpper().Substring(items.ProductStock.BatchNo.Length - 8), 8, 28);
                    else
                        rpt.Write(items.ProductStock.BatchNo.ToUpper(), 8, 27);

                    rpt.NewLine();
                    rpt.Write(string.Format("{0:MM/yy}", items.ProductStock.ExpireDate), 6, 4, Alignment.Right);

                    rpt.Write(string.Format("{0:0.00}", items.SellingPrice == null ? items.ProductStock.SellingPrice : items.SellingPrice), 8, 11, Alignment.Right);
                    rpt.Write(string.Format("{0:0}", items.ReturnedQuantity), 6, 19, Alignment.Right);
                    rpt.Write(string.Format("{0:0.00}", ((items.SellingPrice == null ? items.ProductStock.SellingPrice : items.SellingPrice) * items.ReturnedQuantity)), 11, 25, Alignment.Right);
                    //
                    len++;

                    totReturnDiscount = ((items.Discount / 100) * items.OnlyReturnAmount);
                    totReturnAmount = totReturnAmount - totReturnDiscount;
                    ReturnDiscount += totReturnDiscount;
                }
            }

            rpt.NewLine();
            rpt.NewLine();

            //rpt.Write("RETURN AMT ", 14, 14);
            //rpt.Write(string.Format("{0:0.00}", totReturnAmount), 11, 25, Alignment.Right, data.IsBold);

            rpt.Write();
            rpt.Drawline(1);

            rpt.NewLine();
            decimal retAmount = data.RoundoffNetAmount as decimal? ?? 0; //RoundOffCalculation(data.Net, totReturnAmount);
            rpt.Write("R.Off: " + string.Format("{0:0.00}", retAmount), 15, 1, Alignment.Left);
            if (ReturnDiscount != 0)
                rpt.Write("Disc.: -" + string.Format("{0:0.00}", ReturnDiscount), 15, 17);
            else
                rpt.Write("Disc.: " + string.Format("{0:0.00}", ReturnDiscount), 15, 17);

            rpt.NewLine();
            rpt.Write("RETURN AMOUNT ", 19, 10);
            //rpt.Write(string.Format("{0:0.00}", Math.Round((decimal)data.Net - (decimal)totReturnAmount + retAmount) * (-1)), 11, 25, Alignment.Right, data.IsBold);
            rpt.Write(string.Format("{0:0.00}", data.NetAmount * (-1)), 11, 25, Alignment.Right, data.IsBold);
            //data.InWords = ((long)(Math.Round((decimal)data.Net - (decimal)totReturnAmount + retAmount) * (-1))).ConvertNumbertoWords();
            data.InWords = ((decimal)(data.NetAmount * (-1))).ConvertNumbertoWords();
            rpt.NewLine();
            rpt.NewLine();
            rpt.Write("E&OE ", 5, 1);
            rpt.Write(" Pharmacist Signature", 27, 10, Alignment.Right);
            rpt.NewLine();
            rpt.Write(string.Format("Rupees {0} only.", data.InWords), 36, 1, TextAdjustment.Wrap);
            rpt.NewLine();
            rpt.Write("*** THANK YOU ***", 36, 1, Alignment.Center);

            //Newly added for Print Footer by Mani on 28-02-2017
            GenerateFooter(rpt, 35, data.PrintFooterNote);
        }

        private void WriteReportOnlyReturnItems_4InchRollNew_GST(PlainTextReport rpt, IInvoice data)
        {
            decimal? totReturnAmount = 0;
            decimal? totReturnDiscount = 0;
            decimal? ReturnDiscount = 0;
            int len = 0;
            bool gstSelection = Convert.ToBoolean(data.Instance.Gstselect);
            foreach (var items in data.SalesReturnItem)
            {
                if (items.ReturnedQuantity > 0)
                {
                    rpt.NewLine();
                    rpt.Write((len + 1).ToString(), 3, 1);

                    if (data.TaxRefNo == 1 && !gstSelection)
                    {
                        var gst = (items.Igst == 0 || items.Igst == null) ? (items.Cgst + items.Sgst) : items.Igst;

                        if (items.ProductStock.Product.Name.Length > 27)
                        {
                            rpt.Write(items.ProductStock.Product.Name.ToUpper().Substring(0, 27), 27, 4);
                        }
                        else
                        {
                            rpt.Write(items.ProductStock.Product.Name.ToUpper(), 27, 4);
                        }
                        rpt.Write(gst.ToString(), 4, 29);
                    }
                    else
                    {
                        if (items.ProductStock.Product.Name.Length > 27)
                        {
                            rpt.Write(items.ProductStock.Product.Name.ToUpper().Substring(0, 27), 27, 4);
                        }
                        else
                        {
                            rpt.Write(items.ProductStock.Product.Name.ToUpper(), 27, 4);
                        }
                    }
                    totReturnAmount += items.ReturnAmt;
                    var batchNo = items.ProductStock.BatchNo.ToUpper();
                    if (batchNo.Length > 8)
                        rpt.Write(items.ProductStock.BatchNo.ToUpper().Substring(items.ProductStock.BatchNo.Length - 8), 8, 28);
                    else
                        rpt.Write(items.ProductStock.BatchNo.ToUpper(), 8, 27);

                    rpt.NewLine();
                    rpt.Write(string.Format("{0:MM/yy}", items.ProductStock.ExpireDate), 6, 4, Alignment.Right);

                    rpt.Write(string.Format("{0:0.00}", items.SellingPrice == null ? items.ProductStock.SellingPrice : items.SellingPrice), 8, 11, Alignment.Right);
                    rpt.Write(string.Format("{0:0}", items.ReturnedQuantity), 6, 19, Alignment.Right);
                    rpt.Write(string.Format("{0:0.00}", ((items.SellingPrice == null ? items.ProductStock.SellingPrice : items.SellingPrice) * items.ReturnedQuantity)), 11, 25, Alignment.Right);
                    //
                    len++;

                    totReturnDiscount = ((items.Discount / 100) * items.OnlyReturnAmount);
                    totReturnAmount = totReturnAmount - totReturnDiscount;
                    ReturnDiscount += totReturnDiscount;
                }
            }

            rpt.NewLine();
            rpt.NewLine();

            rpt.Write();
            rpt.Drawline(1);

            rpt.NewLine();
            decimal retAmount = data.RoundoffNetAmount as decimal? ?? 0; //RoundOffCalculation(data.Net, totReturnAmount);
            rpt.Write("R.Off: " + string.Format("{0:0.00}", retAmount), 15, 1, Alignment.Left);
            if (ReturnDiscount != 0)
                rpt.Write("Disc.: -" + string.Format("{0:0.00}", ReturnDiscount), 15, 17);
            else
                rpt.Write("Disc.: " + string.Format("{0:0.00}", ReturnDiscount), 15, 17);

            rpt.NewLine();
            if (data.SalesReturn.ReturnCharges > 0 && data.SalesReturn.ReturnCharges != null)
            {
                rpt.Write("Ret. Charges: " + string.Format("{0:0.00}", data.SalesReturn.ReturnCharges), 26, 10, Alignment.Right);
                rpt.NewLine();
                rpt.Write("Return Amount ", 19, 10);
                //rpt.Write(string.Format("{0:0.00}", Math.Round(((decimal)data.Net - (decimal)totReturnAmount + retAmount) * (-1) - (decimal)data.SalesReturn.ReturnCharges)), 11, 25, Alignment.Right, data.IsBold);
                rpt.Write(string.Format("{0:0.00}", (decimal)data.NetAmount * (-1) - (decimal)data.SalesReturn.ReturnCharges), 11, 25, Alignment.Right, data.IsBold);
                //data.InWords = ((long)(Math.Round(((decimal)data.Net - (decimal)totReturnAmount + retAmount) * (-1) - (decimal)data.SalesReturn.ReturnCharges))).ConvertNumbertoWords();
                data.InWords = ((decimal)((decimal)data.NetAmount * (-1) - (decimal)data.SalesReturn.ReturnCharges)).ConvertNumbertoWords();
            }
            else
            {
                rpt.Write("Return Amount ", 19, 10);
                //rpt.Write(string.Format("{0:0.00}", Math.Round((decimal)data.Net - (decimal)totReturnAmount + retAmount) * (-1)), 11, 25, Alignment.Right, data.IsBold);
                rpt.Write(string.Format("{0:0.00}", data.NetAmount * (-1)), 11, 25, Alignment.Right, data.IsBold);
                //data.InWords = ((long)(Math.Round((decimal)data.Net - (decimal)totReturnAmount + retAmount) * (-1))).ConvertNumbertoWords();
                data.InWords = ((decimal)((decimal)data.NetAmount * (-1))).ConvertNumbertoWords();
            }
            
            rpt.NewLine();
            rpt.NewLine();
            rpt.Write("E&OE ", 5, 1);
            rpt.Write(" Pharmacist Signature", 27, 10, Alignment.Right);
            rpt.NewLine();
            rpt.Write(string.Format("Rupees {0} only.", data.InWords), 36, 1, TextAdjustment.Wrap);
            if (gstSelection)
            {
                rpt.NewLine();
                rpt.Write("Composite tax payer not eligible to collect taxes", 35, 1, TextAdjustment.Wrap);
                rpt.NewLine();
            }
            rpt.NewLine();
            rpt.Write("*** THANK YOU ***", 36, 1, Alignment.Center);

            GenerateFooter(rpt, 35, data.PrintFooterNote);
        }

        private void WriteReportOnlyReturnItems_A4(PlainTextReport rpt, IInvoice data)
        {
            decimal? totReturnAmount = 0;
            decimal? totReturnDiscount = 0;
            decimal? ReturnDiscount = 0;
            int len = 0;
            bool gstSelection = Convert.ToBoolean(data.Instance.Gstselect);

            foreach (var items in data.SalesReturnItem)
            {
                if (items.ReturnedQuantity > 0)
                {
                    rpt.NewLine();
                    len++;

                    rpt.Write(items.ProductStock.Product.Name.ToUpper(), 31);
                    rpt.Write(items.ProductStock.BatchNo, 8, 32);
                    rpt.Write(string.Format(" {0:MM/yy}", items.ProductStock.ExpireDate), 6, 39);
                    rpt.Write(string.Format("{0:0}", items.ReturnedQuantity), 4, 47);

                    if (items.SellingPrice > 0)
                    {                        
                        rpt.Write(string.Format("{0:0.00}", items.SellingPrice), 7, 54);
                        if (data.TaxRefNo == 1 && !gstSelection)
                        {
                            var gst = (items.Igst == 0 || items.Igst == null) ? (items.Cgst + items.Sgst) : items.Igst;
                            rpt.Write(string.Format("{0:0.00}", (gst)), 5, 63);
                        }
                        else if(!gstSelection)
                        {
                            rpt.Write(string.Format("{0:0.00}", (items.ProductStock.VAT)), 5, 63);
                        }                        
                    }
                    else
                    {
                        rpt.Write(string.Format("{0:0.00}", items.ProductStock.SellingPrice), 7, 54);
                        rpt.Write(string.Format("{0:0.00}", (items.ProductStock.VAT)), 5, 63);
                    }

                    rpt.Write(string.Format("{0:0.00}", (items.SellingPrice == null ? items.ProductStock.SellingPrice : items.SellingPrice) * (items.ReturnedQuantity)), 8, 71, Alignment.Right);

                    totReturnAmount = totReturnAmount + (items.SellingPrice == null ? items.ProductStock.SellingPrice : items.SellingPrice) * (items.ReturnedQuantity);
                    totReturnDiscount = ((items.Discount / 100) * items.OnlyReturnAmount);
                    totReturnAmount = totReturnAmount - totReturnDiscount;
                    ReturnDiscount += totReturnDiscount; // Need to Plus totReturnDiscount
                }
            }
            rpt.NewLine();
            rpt.NewLine();

            //rpt.Write("RETURN AMT: " + string.Format("{0:0.00}", totReturnAmount), 20, 59, Alignment.Right, data.IsBold);

            rpt.Write();
            rpt.Drawline(1);

            rpt.NewLine();
            decimal retAmount = data.RoundoffNetAmount as decimal? ?? 0; //RoundOffCalculation(data.Net, totReturnAmount);
            rpt.Write("R.Off: " + string.Format("{0:0.00}", retAmount), 15, 1, Alignment.Left);
            if (ReturnDiscount != 0)
                rpt.Write("Disc.: -" + string.Format("{0:0.00}", ReturnDiscount), 15, 17);
            else
                rpt.Write("Disc.: " + string.Format("{0:0.00}", ReturnDiscount), 15, 17);
            if (data.SalesReturn.ReturnCharges > 0 && data.SalesReturn.ReturnCharges != null)
            {
                rpt.Write("Ret. Charges: " + string.Format("{0:0.00}", data.SalesReturn.ReturnCharges), 25, 54, Alignment.Right);
                rpt.NewLine();
                //rpt.Write("Return Amount: " + string.Format("{0:0.00}", ((((decimal)data.Net - (decimal)totReturnAmount) + retAmount) * (-1) - (decimal)data.SalesReturn.ReturnCharges)), 28, 51, Alignment.Right, data.IsBold);
                rpt.Write("Return Amount: " + string.Format("{0:0.00}", (data.NetAmount * (-1) - (decimal)data.SalesReturn.ReturnCharges)), 28, 51, Alignment.Right, data.IsBold);
            }
            else
            {
                //rpt.Write("Return Amount: " + string.Format("{0:0.00}", Math.Round(((decimal)data.Net - (decimal)totReturnAmount) + retAmount) * (-1)), 28, 51, Alignment.Right, data.IsBold);
                rpt.Write("Return Amount: " + string.Format("{0:0.00}", data.NetAmount * (-1)), 28, 51, Alignment.Right, data.IsBold);
            }

            rpt.Write();
            rpt.Drawline(1);
            if (gstSelection)
            {                
                GenerateGstSelected(rpt);
                rpt.Write();
                rpt.Drawline(1);
            }

        }
        /*
         private void WriteReportOnlyReturnItems_A4_GST(PlainTextReport rpt, IInvoice data)
        {
            decimal? totReturnAmount = 0;
            decimal? totReturnDiscount = 0;
            decimal? ReturnDiscount = 0;
            int len = 0;

            foreach (var items in data.SalesReturnItem)
            {
                if (items.ReturnedQuantity > 0)
                {
                    rpt.NewLine();
                    len++;
                    if (data.TaxRefNo == 1)
                    {
                        var gst = (items.Igst == 0 || items.Igst == null) ? (items.Cgst + items.Sgst) : items.Igst;

                        rpt.Write(items.ProductStock.Product.Name.ToUpper(), 31, 1);
                        rpt.Write(gst.ToString(), 4, 32);
                    }
                    else
                        rpt.Write(items.ProductStock.Product.Name.ToUpper(), 31);
                    rpt.Write(items.ProductStock.BatchNo, 8, 32);
                    rpt.Write(string.Format(" {0:MM/yy}", items.ProductStock.ExpireDate), 6, 39);
                    rpt.Write(string.Format("{0:0}", items.ReturnedQuantity), 4, 47);

                    if (items.SellingPrice > 0)
                    {
                        rpt.Write(string.Format("{0:0.00}", items.SellingPrice), 7, 54);
                        rpt.Write(string.Format("{0:0.00}", (items.ProductStock.VAT)), 5, 63);
                    }
                    else
                    {
                        rpt.Write(string.Format("{0:0.00}", items.ProductStock.SellingPrice), 7, 54);
                        rpt.Write(string.Format("{0:0.00}", (items.ProductStock.VAT)), 5, 63);
                    }

                    rpt.Write(string.Format("{0:0.00}", (items.SellingPrice == null ? items.ProductStock.SellingPrice : items.SellingPrice) * (items.ReturnedQuantity)), 8, 71, Alignment.Right);

                    totReturnAmount = totReturnAmount + (items.SellingPrice == null ? items.ProductStock.SellingPrice : items.SellingPrice) * (items.ReturnedQuantity);
                    totReturnDiscount = ((items.Discount / 100) * items.OnlyReturnAmount);
                    totReturnAmount = totReturnAmount - totReturnDiscount;
                    ReturnDiscount += totReturnDiscount; // Need to Plus totReturnDiscount
                }
            }
            rpt.NewLine();
            rpt.NewLine();

            //rpt.Write("RETURN AMT: " + string.Format("{0:0.00}", totReturnAmount), 20, 59, Alignment.Right, data.IsBold);

            rpt.Write();
            rpt.Drawline(1);

            rpt.NewLine();
            decimal retAmount = RoundOffCalculation(data.Net, totReturnAmount);
            rpt.Write("R.Off: " + string.Format("{0:0.00}", retAmount), 15, 1, Alignment.Left);
            if (ReturnDiscount != 0)
                rpt.Write("Disc.: -" + string.Format("{0:0.00}", ReturnDiscount), 15, 17);
            else
                rpt.Write("Disc.: " + string.Format("{0:0.00}", ReturnDiscount), 15, 17);
            //rpt.NewLine();
            rpt.Write("RETURN AMOUNT: " + string.Format("{0:0.00}", Math.Round(((decimal)data.Net - (decimal)totReturnAmount) + retAmount) * (-1)), 28, 51, Alignment.Right, data.IsBold);

            rpt.Write();
            rpt.Drawline(1);
        }
        */
        private void WriteReportOnlyReturnItems_4InchRoll(PlainTextReport rpt, IInvoice data)
        {
            rpt.NewLine();

            decimal? totReturnAmount = 0;
            decimal? totReturnDiscount = 0;
            decimal? ReturnDiscount = 0;
            int len = 0;

            foreach (var items in data.SalesReturnItem)
            {
                if (items.ReturnedQuantity > 0)
                {
                    rpt.NewLine();
                    rpt.Write(items.ProductStock.Product.Name.ToUpper(), 25, 1);
                    rpt.NewLine();
                    rpt.Write(items.ProductStock.BatchNo.ToUpper(), 10, true);
                    rpt.Write(string.Format("{0:MM/yy}", items.ProductStock.ExpireDate), 8, true);
                    rpt.Write(string.Format("{0:0}", items.ReturnedQuantity), 5, true, Alignment.Right);

                    totReturnAmount += items.ReturnAmt;

                    if (items.SellingPrice > 0)
                    {
                        rpt.Write(string.Format("{0:0.00}", items.SellingPrice), 8, true, Alignment.Right);
                    }
                    else
                    {
                        rpt.Write(string.Format("{0:0.00}", items.ProductStock.SellingPrice), 8, true, Alignment.Right);
                    }

                    totReturnDiscount = ((items.Discount / 100) * items.OnlyReturnAmount);
                    totReturnAmount = totReturnAmount - totReturnDiscount;
                    ReturnDiscount += totReturnDiscount;

                    rpt.Write(string.Format("{0:0.00}", items.ReturnAmt), 9, true, Alignment.Right);
                    rpt.Write();
                    len++;
                }
            }

            rpt.NewLine();
            rpt.NewLine();

            //rpt.Write("RETURN AMT: " + string.Format("{0:0.00}", totReturnAmount), 18, 31, Alignment.Right, data.IsBold);

            rpt.Write();
            rpt.Drawline(1);

            rpt.NewLine();
            decimal retAmount = data.RoundoffNetAmount as decimal? ?? 0; //RoundOffCalculation(data.Net, totReturnAmount);
            rpt.Write("R.Off: " + string.Format("{0:0.00}", retAmount), 15, 1, Alignment.Left);
            if (ReturnDiscount != 0)
                rpt.Write("Disc.: -" + string.Format("{0:0.00}", ReturnDiscount), 15, 17);
            else
                rpt.Write("Disc.: " + string.Format("{0:0.00}", ReturnDiscount), 15, 17);
            rpt.NewLine();
            //rpt.Write("RETURN AMOUNT: " + string.Format("{0:0.00}", Math.Round((decimal)data.Net - (decimal)totReturnAmount + retAmount) * (-1)), 28, 21, Alignment.Right, data.IsBold);  //Alignment Issue 
            rpt.Write("RETURN AMOUNT: " + string.Format("{0:0.00}", data.NetAmount * (-1)), 28, 21, Alignment.Right, data.IsBold);  //Alignment Issue 
            rpt.NewLine();
            rpt.NewLine();
            rpt.Write("E&OE ", 5, 1);
            rpt.Write(" Signature of the Regd. Pharmacist", 37, 12, Alignment.Right);

        }

        private void WriteReportOnlyReturnItems_4InchRoll_GST(PlainTextReport rpt, IInvoice data)
        {
            rpt.NewLine();
            bool gstSelection = Convert.ToBoolean(data.Instance.Gstselect);
            decimal? totReturnAmount = 0;
            decimal? totReturnDiscount = 0;
            decimal? ReturnDiscount = 0;
            int len = 0;

            foreach (var items in data.SalesReturnItem)
            {
                if (items.ReturnedQuantity > 0)
                {
                    rpt.NewLine();
                    if (data.TaxRefNo == 1 && !gstSelection)
                    {
                        var gst = (items.Igst == 0 || items.Igst == null) ? (items.Cgst + items.Sgst) : items.Igst;

                        rpt.Write(items.ProductStock.Product.Name.ToUpper(), 16, 1);
                        rpt.Write(gst.ToString(), 4, 18);
                    }
                    else
                        rpt.Write(items.ProductStock.Product.Name.ToUpper(), 16, 1);
                    rpt.NewLine();
                    rpt.Write(items.ProductStock.BatchNo.ToUpper(), 10, true);
                    rpt.Write(string.Format("{0:MM/yy}", items.ProductStock.ExpireDate), 8, true);
                    rpt.Write(string.Format("{0:0}", items.ReturnedQuantity), 5, true, Alignment.Right);

                    totReturnAmount += items.ReturnAmt;

                    if (items.SellingPrice > 0)
                    {
                        rpt.Write(string.Format("{0:0.00}", items.SellingPrice), 8, true, Alignment.Right);
                    }
                    else
                    {
                        rpt.Write(string.Format("{0:0.00}", items.ProductStock.SellingPrice), 8, true, Alignment.Right);
                    }

                    totReturnDiscount = ((items.Discount / 100) * items.OnlyReturnAmount);
                    totReturnAmount = totReturnAmount - totReturnDiscount;
                    ReturnDiscount += totReturnDiscount;

                    rpt.Write(string.Format("{0:0.00}", items.ReturnAmt), 9, true, Alignment.Right);
                    rpt.Write();
                    len++;
                }
            }

            rpt.NewLine();
            rpt.NewLine();

            //rpt.Write("RETURN AMT: " + string.Format("{0:0.00}", totReturnAmount), 18, 31, Alignment.Right, data.IsBold);

            rpt.Write();
            rpt.Drawline(1);

            rpt.NewLine();

            if (data.TaxRefNo == 1 && !gstSelection)
            {
                rpt.Write("CGST: " + string.Format("{0:0.00}", data.SalesReturn.CgstTotalValue), 13, 2);
                rpt.Write((data.Instance.isUnionTerritory == true ? "UTGST: " : "SGST: ") + string.Format("{0:0.00}", data.SalesReturn.SgstTotalValue), 13, 15);
            }

            if (ReturnDiscount != 0)
                rpt.Write("Disc.: -" + string.Format("{0:0.00}", ReturnDiscount), 15, 30, Alignment.Right);
            else
                rpt.Write("Disc.: " + string.Format("{0:0.00}", ReturnDiscount), 15, 30, Alignment.Right);
            rpt.NewLine();
            decimal retAmount = data.RoundoffNetAmount as decimal? ?? 0; //RoundOffCalculation(data.Net, totReturnAmount);
            rpt.Write("R.Off: " + string.Format("{0:0.00}", retAmount), 15, 30, Alignment.Right);
            if (data.SalesReturn.ReturnCharges > 0 && data.SalesReturn.ReturnCharges != null)
            {
                rpt.NewLine();
                rpt.Write("Ret. Charges: " + string.Format("{0:0.00}", data.SalesReturn.ReturnCharges), 26, 19, Alignment.Right);
                rpt.NewLine();
                //decimal? retAmt = (Math.Round((decimal)data.Net - (decimal)totReturnAmount + retAmount) * (-1)) - Math.Round((decimal)data.SalesReturn.ReturnCharges);
                decimal? retAmt = ((decimal)data.NetAmount * (-1)) - (decimal)data.SalesReturn.ReturnCharges;
                rpt.Write("Return Amount: " + string.Format("{0:0.00}", retAmt), 26, 15, Alignment.Right, data.IsBold);
            }
            else
            {
                rpt.NewLine();
                //rpt.Write("Return Amount: " + string.Format("{0:0.00}", Math.Round((decimal)data.Net - (decimal)totReturnAmount + retAmount) * (-1)), 26, 15, Alignment.Right, data.IsBold);
                rpt.Write("Return Amount: " + string.Format("{0:0.00}", data.NetAmount * (-1)), 26, 15, Alignment.Right, data.IsBold);
            }
            rpt.NewLine();
            rpt.NewLine();
            rpt.Write("E&OE ", 5, 1);
            rpt.Write(" Signature of the Regd. Pharmacist", 37, 12, Alignment.Right);
            if (gstSelection)
            {
                rpt.NewLine();
                rpt.Write();
                rpt.Drawline(1);
                rpt.Write("Composite tax payer not eligible to collect taxes", 65, 1, Alignment.Left);
            }
        }

        private void WriteReportOnlyReturnItems_A5_WithHeader(PlainTextReport rpt, IInvoice data)
        {
            rpt.NewLine();

            decimal? totReturnAmount = 0;
            decimal? totReturnDiscount = 0;
            decimal? ReturnDiscount = 0;
            int len = 0;

            foreach (var items in data.SalesReturnItem)
            {
                if (items.ReturnedQuantity > 0)
                {
                    rpt.NewLine();
                    len++;

                    rpt.Write(string.Format("{0:0}", len), 3, 1, Alignment.Right);

                    rpt.Write(items.ProductStock.Product.Name.ToUpper(), 18, 5);
                    rpt.Write(items.ProductStock.BatchNo, 6, true, Alignment.Right);
                    rpt.Write(string.Format("{0:MM/yy}", items.ProductStock.ExpireDate), 5, true);
                    rpt.Write(string.Format("{0:0}", items.ReturnedQuantity), 4, true, Alignment.Right);

                    totReturnAmount += items.ReturnAmt;
                    if (items.SellingPrice > 0)
                    {
                        decimal? vatPrice = ((items.SellingPrice * 100) / (items.ProductStock.VAT + 100));
                        rpt.Write(string.Format("{0:0.00}", items.SellingPrice), 6, true, Alignment.Right);
                        rpt.Write(string.Format("{0:0.00}", items.ProductStock.VAT), 5, true, Alignment.Right);
                    }
                    else
                    {
                        decimal? vatPrice = items.ProductStock.SellingPrice - items.ProductStock.VatInPrice;
                        rpt.Write(string.Format("{0:0.00}", items.ProductStock.SellingPrice), 6, true, Alignment.Right);
                        rpt.Write(string.Format("{0:0.00}", items.ProductStock.VAT), 5, true, Alignment.Right);
                    }

                    rpt.Write(string.Format("{0:0.00}", ((items.SellingPrice == null ? items.ProductStock.SellingPrice : items.SellingPrice) * items.ReturnedQuantity)), 8, true, Alignment.Right);
                    rpt.Write();

                    totReturnDiscount = ((items.Discount / 100) * items.OnlyReturnAmount);
                    totReturnAmount = totReturnAmount - totReturnDiscount;
                    ReturnDiscount += totReturnDiscount;
                }
            }

            rpt.NewLine();
            rpt.NewLine();

            //rpt.Write("RETURN AMT: " + string.Format("{0:0.00}", totReturnAmount), 18, 42, Alignment.Right, data.IsBold);

            rpt.Write();
            rpt.Drawline(1);

            rpt.NewLine();
            decimal retAmount = data.RoundoffNetAmount as decimal? ?? 0; //RoundOffCalculation(data.Net, totReturnAmount);
            rpt.Write("R.Off: " + string.Format("{0:0.00}", retAmount), 15, 1, Alignment.Left);
            if (ReturnDiscount != 0)
                rpt.Write("Disc.: -" + string.Format("{0:0.00}", ReturnDiscount), 15, 17);
            else
                rpt.Write("Disc.: " + string.Format("{0:0.00}", ReturnDiscount), 15, 17);
            //rpt.NewLine();
            //rpt.Write("RETURN AMOUNT: " + string.Format("{0:0.00}", Math.Round(((decimal)data.Net - (decimal)totReturnAmount) + retAmount) * (-1)), 28, 32, Alignment.Right, data.IsBold);
            rpt.Write("RETURN AMOUNT: " + string.Format("{0:0.00}", data.NetAmount * (-1)), 28, 32, Alignment.Right, data.IsBold);

            //data.InWords = ((long)(data.Net - totReturnAmount)).ConvertNumbertoWords() + " only";
            data.InWords = ((decimal)(data.NetAmount * (-1))).ConvertNumbertoWords() + " only";
        }
        private void WriteReportOnlyReturnItems_A5_WithHeader_GST(PlainTextReport rpt, IInvoice data)
        {
            rpt.NewLine();
            bool gstSelection = Convert.ToBoolean(data.Instance.Gstselect);
            decimal? totReturnAmount = 0;
            decimal? totReturnDiscount = 0;
            decimal? ReturnDiscount = 0;
            int len = 0;

            foreach (var items in data.SalesReturnItem)
            {
                if (items.ReturnedQuantity > 0)
                {
                    rpt.NewLine();
                    len++;

                    rpt.Write(string.Format("{0:0}", len), 3, 1, Alignment.Right);
                    
                    rpt.Write(items.ProductStock.Product.Name.ToUpper(), 18, 5);


                    rpt.Write(items.ProductStock.BatchNo, 6, true, Alignment.Right);
                    rpt.Write(string.Format("{0:MM/yy}", items.ProductStock.ExpireDate), 5, true);
                    rpt.Write(string.Format("{0:0}", items.ReturnedQuantity), 4, true, Alignment.Right);

                    totReturnAmount += items.ReturnAmt;
                    if (items.SellingPrice > 0)
                    {
                        decimal? vatPrice = ((items.SellingPrice * 100) / (items.ProductStock.VAT + 100));
                        rpt.Write(string.Format("{0:0.00}", items.SellingPrice), 6, true, Alignment.Right);
                    }
                    else
                    {
                        decimal? vatPrice = items.ProductStock.SellingPrice - items.ProductStock.VatInPrice;
                        rpt.Write(string.Format("{0:0.00}", items.ProductStock.SellingPrice), 6, true, Alignment.Right);
                    }
                    if (data.TaxRefNo == 1 && !gstSelection)
                    {
                        var gst = (items.Igst == 0 || items.Igst == null) ? (items.Cgst + items.Sgst) : items.Igst;
                        rpt.Write(gst.ToString(), 5, true, Alignment.Right);
                    }
                    else
                        rpt.Write(" ", 3, true, Alignment.Right);

                    rpt.Write(string.Format("{0:0.00}", ((items.SellingPrice == null ? items.ProductStock.SellingPrice : items.SellingPrice) * items.ReturnedQuantity)), 8, true, Alignment.Right);
                    rpt.Write();

                    totReturnDiscount = ((items.Discount / 100) * items.OnlyReturnAmount);
                    totReturnAmount = totReturnAmount - totReturnDiscount;
                    ReturnDiscount += totReturnDiscount;

                    items.IgstValue = ((items.OnlyReturnAmount - totReturnDiscount) - (((items.OnlyReturnAmount - totReturnDiscount) * 100) / (items.Igst + 100)));
                    decimal? getGst = ((items.OnlyReturnAmount - totReturnDiscount) - (((items.OnlyReturnAmount - totReturnDiscount) * 100) / ((items.Cgst + items.Sgst) + 100)));
                    items.CgstValue = getGst / 2;
                    //items.SgstValue = ((items.OnlyReturnAmount - totReturnDiscount) - (((items.OnlyReturnAmount - totReturnDiscount) * 100) / (items.Sgst + 100)));
                    items.SgstValue = getGst / 2;
                }
            }

            rpt.NewLine();
            rpt.NewLine();

            rpt.Write();
            rpt.Drawline(1);
            rpt.NewLine();
            
            if (data.TaxRefNo == 1 && !gstSelection)
            {
                    rpt.Write("CGST: " + string.Format("{0:0.00}", data.SalesReturnItem.Sum(z => z.CgstValue)), 13, 2);
                    rpt.Write((data.Instance.isUnionTerritory == true ? "UTGST: " : "SGST: ") + string.Format("{0:0.00}", data.SalesReturnItem.Sum(z => z.SgstValue)), 13, 15);
            }
                
            if (ReturnDiscount != 0)
                rpt.Write("Disc.: -" + string.Format("{0:0.00}", ReturnDiscount), 28, 34, Alignment.Right);
            else
                rpt.Write("Disc.: " + string.Format("{0:0.00}", ReturnDiscount), 28, 34, Alignment.Right);
            rpt.NewLine();
            decimal retAmount = data.RoundoffNetAmount as decimal? ?? 0; //RoundOffCalculation(data.Net, totReturnAmount);
            rpt.Write("R.Off: " + string.Format("{0:0.00}", retAmount), 28, 34, Alignment.Right);
            if (data.SalesReturn.ReturnCharges > 0 && data.SalesReturn.ReturnCharges != null)
            {
                rpt.NewLine();
                rpt.Write("Ret. Charges: " + string.Format("{0:0.00}", data.SalesReturn.ReturnCharges), 28, 34, Alignment.Right);
                rpt.NewLine();
                //rpt.Write("Return Amount: " + string.Format("{0:0.00}", (Math.Round(((decimal)data.Net - (decimal)totReturnAmount) + retAmount) * (-1)) - Math.Round((decimal)data.SalesReturn.ReturnCharges)), 26, 36, Alignment.Right, data.IsBold);                
                rpt.Write("Return Amount: " + string.Format("{0:0.00}", (data.NetAmount * (-1)) - (decimal)data.SalesReturn.ReturnCharges), 26, 36, Alignment.Right, data.IsBold);
            }
            else
            {
                rpt.NewLine();
                //rpt.Write("Return Amount: " + string.Format("{0:0.00}", Math.Round(((decimal)data.Net - (decimal)totReturnAmount) + retAmount) * (-1)), 28, 34, Alignment.Right, data.IsBold);                
                rpt.Write("Return Amount: " + string.Format("{0:0.00}", data.NetAmount * (-1)), 28, 34, Alignment.Right, data.IsBold);
            }
			if (gstSelection)
            {
                rpt.Write();
                rpt.Drawline(1);
                GenerateGstSelected(rpt);
            }
            //data.InWords = ((long)(data.Net - totReturnAmount)).ConvertNumbertoWords() + " only";
            data.InWords = ((decimal)(data.NetAmount * (-1))).ConvertNumbertoWords() + " only";
        }

        private void WriteReportOnlyReturnItems_A6_WithHeader(PlainTextReport rpt, IInvoice data)
        {

            decimal? totReturnAmount = 0;
            decimal? totReturnDiscount = 0;
            decimal? ReturnDiscount = 0;
            int len = 0;

            foreach (var items in data.SalesReturnItem)
            {
                if (items.ReturnedQuantity > 0)
                {
                    rpt.NewLine();
                    rpt.Write("  " + items.ProductStock.Product.Name.ToUpper(), 27);
                    rpt.NewLine();
                    rpt.Write(" " + items.ProductStock.BatchNo, 10, true);
                    rpt.Write(string.Format("{0:MM/yy}", items.ProductStock.ExpireDate), 7, true);
                    rpt.Write(string.Format("{0:0}", items.ReturnedQuantity), 5, true, Alignment.Right);

                    if (items.SellingPrice > 0)
                    {
                        rpt.Write(string.Format("{0:0.00}", items.SellingPrice), 8, true, Alignment.Right);
                    }
                    else
                    {
                        rpt.Write(string.Format("{0:0.00}", items.ProductStock.SellingPrice), 8, true, Alignment.Right);
                    }

                    rpt.Write(string.Format("{0:0.00}", (items.SellingPrice == null ? items.ProductStock.SellingPrice : items.SellingPrice) * (items.ReturnedQuantity)), 9, true, Alignment.Right);
                    rpt.Write();
                    len++;

                    totReturnAmount = totReturnAmount + (items.SellingPrice == null ? items.ProductStock.SellingPrice : items.SellingPrice) * (items.ReturnedQuantity);
                    totReturnDiscount = ((items.Discount / 100) * items.OnlyReturnAmount);
                    totReturnAmount = totReturnAmount - totReturnDiscount;
                    ReturnDiscount += totReturnDiscount;
                }
            }
            rpt.NewLine();
            rpt.NewLine();

            //rpt.Write("RETURN AMT: " + string.Format("{0:0.00}", totReturnAmount), 20, 25, Alignment.Right, data.IsBold);

            rpt.Write();
            rpt.Drawline(1);

            rpt.NewLine();
            decimal retAmount = data.RoundoffNetAmount as decimal? ?? 0; //RoundOffCalculation(data.Net, totReturnAmount);
            rpt.Write("R.Off: " + string.Format("{0:0.00}", retAmount), 15, 1, Alignment.Left);
            if (ReturnDiscount != 0)
                rpt.Write("Disc.: -" + string.Format("{0:0.00}", ReturnDiscount), 15, 17);
            else
                rpt.Write("Disc.: " + string.Format("{0:0.00}", ReturnDiscount), 15, 17);
            rpt.NewLine();
            //rpt.Write("RETURN AMOUNT: " + string.Format("{0:0.00}", Math.Round(((decimal)data.Net - (decimal)totReturnAmount) + retAmount) * (-1)), 28, 17, Alignment.Right, data.IsBold);
            rpt.Write("RETURN AMOUNT: " + string.Format("{0:0.00}", data.NetAmount * (-1)), 28, 17, Alignment.Right, data.IsBold);

            rpt.Write();
            rpt.Drawline(1);
        }

        private void WriteReportOnlyReturnItems_A6_WithHeader_GST(PlainTextReport rpt, IInvoice data)
        {
            decimal? totReturnAmount = 0;
            decimal? totReturnDiscount = 0;
            decimal? ReturnDiscount = 0;
            int len = 0;
            bool gstSelection = Convert.ToBoolean(data.Instance.Gstselect);
            foreach (var items in data.SalesReturnItem)
            {
                if (items.ReturnedQuantity > 0)
                {
                    rpt.NewLine();
                    if (data.TaxRefNo == 1)
                    {
                        rpt.Write("  " + items.ProductStock.Product.Name.ToUpper(), 27);
                        var gst = (items.Igst == 0 || items.Igst == null) ? (items.Cgst + items.Sgst) : items.Igst;
                        if (!gstSelection)
                            rpt.Write(gst.ToString(), 4, 28);
                    }
                    else
                        rpt.Write("  " + items.ProductStock.Product.Name.ToUpper(), 27);
                    rpt.NewLine();
                    rpt.Write(" " + items.ProductStock.BatchNo, 10, true);
                    rpt.Write(string.Format("{0:MM/yy}", items.ProductStock.ExpireDate), 7, true);
                    rpt.Write(string.Format("{0:0}", items.ReturnedQuantity), 5, true, Alignment.Right);

                    if (items.SellingPrice > 0)
                    {
                        rpt.Write(string.Format("{0:0.00}", items.SellingPrice), 8, true, Alignment.Right);
                    }
                    else
                    {
                        rpt.Write(string.Format("{0:0.00}", items.ProductStock.SellingPrice), 8, true, Alignment.Right);
                    }

                    rpt.Write(string.Format("{0:0.00}", (items.SellingPrice == null ? items.ProductStock.SellingPrice : items.SellingPrice) * (items.ReturnedQuantity)), 9, true, Alignment.Right);
                    rpt.Write();
                    len++;

                    totReturnAmount = totReturnAmount + (items.SellingPrice == null ? items.ProductStock.SellingPrice : items.SellingPrice) * (items.ReturnedQuantity);
                    totReturnDiscount = ((items.Discount / 100) * items.OnlyReturnAmount);
                    totReturnAmount = totReturnAmount - totReturnDiscount;
                    ReturnDiscount += totReturnDiscount;

                    items.IgstValue = ((items.OnlyReturnAmount - totReturnDiscount) - (((items.OnlyReturnAmount - totReturnDiscount) * 100) / (items.Igst + 100)));
                    decimal? getGst = ((items.OnlyReturnAmount - totReturnDiscount) - (((items.OnlyReturnAmount - totReturnDiscount) * 100) / ((items.Cgst + items.Sgst) + 100)));
                    items.CgstValue = (getGst / 2);
                    items.SgstValue = (getGst / 2);
                }
            }
            rpt.NewLine();
            rpt.NewLine();

            rpt.Write();
            rpt.Drawline(1);
            rpt.NewLine();

            rpt.NewLine();
            if (data.TaxRefNo == 1 && !gstSelection)
            {
                rpt.Write("CGST: " + string.Format("{0:0.00}", data.SalesReturnItem.Sum(z => z.CgstValue)), 13, 2);
                rpt.Write((data.Instance.isUnionTerritory == true ? "UTGST: " : "SGST: ") + string.Format("{0:0.00}", data.SalesReturnItem.Sum(z => z.SgstValue)), 13, 15);
            }
              
            //rpt.NewLine();

            if (ReturnDiscount != 0)
                rpt.Write("Disc.: -" + string.Format("{0:0.00}", ReturnDiscount), 13, 27, Alignment.Right);
            else
                rpt.Write("Disc.: " + string.Format("{0:0.00}", ReturnDiscount), 26, 20, Alignment.Right);
            rpt.NewLine();
            decimal retAmount = data.RoundoffNetAmount as decimal? ?? 0; //RoundOffCalculation(data.Net, totReturnAmount);
            rpt.Write("R.Off: " + string.Format("{0:0.00}", retAmount), 26, 20, Alignment.Right);
            rpt.NewLine();
            if (data.SalesReturn.ReturnCharges > 0 && data.SalesReturn.ReturnCharges != null)
            {
                rpt.Write("Ret. Charges: " + string.Format("{0:0.00}", data.SalesReturn.ReturnCharges), 26, 20, Alignment.Right);
                rpt.NewLine();
                //rpt.Write("Return Amount: " + string.Format("{0:0.00}", ((((decimal)data.Net - (decimal)totReturnAmount) + retAmount) * (-1) - (decimal)data.SalesReturn.ReturnCharges)), 28, 17, Alignment.Right, data.IsBold);
                rpt.Write("Return Amount: " + string.Format("{0:0.00}", (data.NetAmount * (-1) - (decimal)data.SalesReturn.ReturnCharges)), 28, 17, Alignment.Right, data.IsBold);
            }
            else
            {
                //rpt.Write("Return Amount: " + string.Format("{0:0.00}", Math.Round(((decimal)data.Net - (decimal)totReturnAmount) + retAmount) * (-1)), 28, 17, Alignment.Right, data.IsBold);
                rpt.Write("Return Amount: " + string.Format("{0:0.00}", data.NetAmount * (-1)), 28, 17, Alignment.Right, data.IsBold);
            }

            rpt.Write();
            rpt.Drawline(1);
			if (gstSelection)
            {
                GenerateGstSelected(rpt);
            }
        }

        private void WriteReportOnlyReturnItems_A5_Vat_WithHeader(PlainTextReport rpt, IInvoice data)
        {
            decimal? totReturnAmount = 0;
            decimal? totReturnDiscount = 0;
            decimal? ReturnDiscount = 0;
            int len = 0;

            foreach (var items in data.SalesReturnItem)
            {
                if (items.ReturnedQuantity > 0)
                {
                    rpt.NewLine();

                    rpt.Write(items.ProductStock.Product.Name.ToUpper(), 20, 2);
                    rpt.Write(items.ProductStock.BatchNo, 8, true);
                    rpt.Write(string.Format("{0:MM/yy}", items.ProductStock.ExpireDate), 5, true);
                    rpt.Write(string.Format("{0:0}", items.ReturnedQuantity), 4, true, Alignment.Right);
                    decimal? SellingAmount = 0;

                    if (items.SellingPrice > 0)
                    {
                        rpt.Write(string.Format("{0:0.00}", items.SellingPrice), 7, true, Alignment.Right);
                        SellingAmount = (items.ReturnedQuantity * items.SellingPrice);
                    }
                    else
                    {
                        rpt.Write(string.Format("{0:0.00}", items.ProductStock.SellingPrice), 7, true, Alignment.Right);
                        SellingAmount = items.ReturnedQuantity * items.ProductStock.SellingPrice;
                    }

                    totReturnAmount += items.ReturnAmt;
                    rpt.Write(string.Format("{0:0.00}", SellingAmount), 10, true, Alignment.Right);
                    rpt.Write();

                    len++;
                    totReturnDiscount = ((items.Discount / 100) * items.OnlyReturnAmount);
                    totReturnAmount = totReturnAmount - totReturnDiscount;
                    ReturnDiscount += totReturnDiscount;
                }
            }
            rpt.NewLine();
            rpt.NewLine();

            //rpt.Write("RETURN AMT: " + string.Format("{0:0.00}", totReturnAmount), 18, 41, Alignment.Right, data.IsBold);

            rpt.Write();
            rpt.Drawline(1);

            rpt.NewLine();

            decimal retAmount = data.RoundoffNetAmount as decimal? ?? 0; //RoundOffCalculation(data.Net, totReturnAmount);
            rpt.Write("R.Off: " + string.Format("{0:0.00}", retAmount), 15, 1, Alignment.Left);
            if (ReturnDiscount != 0)
                rpt.Write("Disc.: -" + string.Format("{0:0.00}", ReturnDiscount), 15, 17);
            else
                rpt.Write("Disc.: " + string.Format("{0:0.00}", ReturnDiscount), 15, 17);
            //rpt.Write("RETURN AMOUNT: " + string.Format("{0:0.00}", Math.Round(((decimal)data.Net - (decimal)totReturnAmount + retAmount)) * (-1)), 28, 31, Alignment.Right, data.IsBold);
            rpt.Write("RETURN AMOUNT: " + string.Format("{0:0.00}", data.NetAmount * (-1)), 28, 31, Alignment.Right, data.IsBold);

            //data.InWords = ((long)(Math.Round((decimal)data.Net - (decimal)totReturnAmount + retAmount)) * (-1)).ConvertNumbertoWords() + " only";
            data.InWords = ((decimal)(data.NetAmount) * (-1)).ConvertNumbertoWords() + " only";
        }

        private void WriteReportOnlyReturnItems_A5_Vat_WithHeader_GST(PlainTextReport rpt, IInvoice data)
        {

            bool gstSelection = Convert.ToBoolean(data.Instance.Gstselect);
            decimal? totReturnAmount = 0;
            decimal? totReturnDiscount = 0;
            decimal? ReturnDiscount = 0;
            int len = 0;

            foreach (var items in data.SalesReturnItem)
            {
                if (items.ReturnedQuantity > 0)
                {
                    rpt.NewLine();
                    if (data.TaxRefNo == 1 && !gstSelection)
                    {
                        var gst = (items.Igst == 0 || items.Igst == null) ? (items.Cgst + items.Sgst) : items.Igst;
                        rpt.Write(items.ProductStock.Product.Name.ToUpper(), 14, 2);
                        rpt.Write(gst.ToString(), 5, 18);
                    }
                    else
                        rpt.Write(items.ProductStock.Product.Name.ToUpper(), 20, 2);
                    rpt.Write(items.ProductStock.BatchNo, 5, true);
                    rpt.Write(string.Format("{0:MM/yy}", items.ProductStock.ExpireDate), 5, true);
                    rpt.Write(string.Format("{0:0}", items.ReturnedQuantity), 4, true, Alignment.Right);
                    decimal? SellingAmount = 0;

                    if (items.SellingPrice > 0)
                    {
                        rpt.Write(string.Format("{0:0.00}", items.SellingPrice), 7, true, Alignment.Right);
                        SellingAmount = (items.ReturnedQuantity * items.SellingPrice);
                    }
                    else
                    {
                        rpt.Write(string.Format("{0:0.00}", items.ProductStock.SellingPrice), 7, true, Alignment.Right);
                        SellingAmount = items.ReturnedQuantity * items.ProductStock.SellingPrice;
                    }

                    totReturnAmount += items.ReturnAmt;
                    rpt.Write(string.Format("{0:0.00}", SellingAmount), 10, true, Alignment.Right);
                    rpt.Write();

                    len++;
                    totReturnDiscount = ((items.Discount / 100) * items.OnlyReturnAmount);
                    totReturnAmount = totReturnAmount - totReturnDiscount;
                    ReturnDiscount += totReturnDiscount;

                    items.IgstValue = ((items.OnlyReturnAmount - totReturnDiscount) - (((items.OnlyReturnAmount - totReturnDiscount) * 100) / (items.Igst + 100)));
                    decimal? getGst = ((items.OnlyReturnAmount - totReturnDiscount) - (((items.OnlyReturnAmount - totReturnDiscount) * 100) / ((items.Cgst + items.Sgst) + 100)));
                    items.CgstValue = getGst / 2;
                    //items.SgstValue = ((items.OnlyReturnAmount - totReturnDiscount) - (((items.OnlyReturnAmount - totReturnDiscount) * 100) / (items.Sgst + 100)));
                    items.SgstValue = getGst / 2;
                }
            }
            rpt.NewLine();
            rpt.NewLine();

            //rpt.Write("RETURN AMT: " + string.Format("{0:0.00}", totReturnAmount), 18, 41, Alignment.Right, data.IsBold);

            rpt.Write();
            rpt.Drawline(1);

            rpt.NewLine();
            if (data.TaxRefNo == 1 && !gstSelection)
            {
                    rpt.Write("CGST: " + string.Format("{0:0.00}", data.SalesReturnItem.Sum(z => z.CgstValue)), 13, 2);
                    rpt.Write((data.Instance.isUnionTerritory == true ? "UTGST: " : "SGST: ") + string.Format("{0:0.00}", data.SalesReturnItem.Sum(z => z.SgstValue)), 13, 15);
               
            }

            if (ReturnDiscount != 0)
                rpt.Write("Disc.: -" + string.Format("{0:0.00}", ReturnDiscount), 28, 31, Alignment.Right);
            else
                rpt.Write("Disc.: " + string.Format("{0:0.00}", ReturnDiscount), 28, 31, Alignment.Right);
            rpt.NewLine();
            decimal retAmount = data.RoundoffNetAmount as decimal? ?? 0; //RoundOffCalculation(data.Net, totReturnAmount);
            rpt.Write("R.Off: " + string.Format("{0:0.00}", retAmount), 28, 31, Alignment.Right);
            if (data.SalesReturn.ReturnCharges > 0 && data.SalesReturn.ReturnCharges != null)
            {
                rpt.NewLine();
                rpt.Write("Ret. Charges: " + string.Format("{0:0.00}", data.SalesReturn.ReturnCharges), 28, 31, Alignment.Right);

                rpt.NewLine();
                //rpt.Write("Return Amount: " + string.Format("{0:0.00}", (Math.Round(((decimal)data.Net - (decimal)totReturnAmount + retAmount)) * (-1)) - Math.Round((decimal)data.SalesReturn.ReturnCharges)), 28, 31, Alignment.Right, data.IsBold);
                rpt.Write("Return Amount: " + string.Format("{0:0.00}", (data.NetAmount * (-1)) - (decimal)data.SalesReturn.ReturnCharges), 28, 31, Alignment.Right, data.IsBold);

                //data.InWords = ((long)((Math.Round((decimal)data.Net - (decimal)totReturnAmount + retAmount)) * (-1) - data.SalesReturn.ReturnCharges)).ConvertNumbertoWords() + " only";
                data.InWords = ((decimal)((data.NetAmount) * (-1) - (decimal)data.SalesReturn.ReturnCharges)).ConvertNumbertoWords() + " only";
            }
            else
            {
                rpt.NewLine();
                //rpt.Write("Return Amount: " + string.Format("{0:0.00}", Math.Round(((decimal)data.Net - (decimal)totReturnAmount + retAmount)) * (-1)), 28, 31, Alignment.Right, data.IsBold);
                rpt.Write("Return Amount: " + string.Format("{0:0.00}", data.NetAmount * (-1)), 28, 31, Alignment.Right, data.IsBold);

                //data.InWords = ((long)(Math.Round((decimal)data.Net - (decimal)totReturnAmount + retAmount)) * (-1)).ConvertNumbertoWords() + " only";
                data.InWords = ((decimal)(data.NetAmount) * (-1)).ConvertNumbertoWords() + " only";
            }
			if (gstSelection)
            {
                rpt.Write();
                rpt.Drawline(1);
                GenerateGstSelected(rpt);
            }
        }

        private void WriteReportOnlyReturnItems_A5_Vat_WithHeader_RackNo_GST(PlainTextReport rpt, IInvoice data)
        {
            bool gstSelection = Convert.ToBoolean(data.Instance.Gstselect);
            decimal? totReturnAmount = 0;
            decimal? totReturnDiscount = 0;
            decimal? ReturnDiscount = 0;
            decimal? TotDiscountPer = 0; 
            int len = 0;

            foreach (var items in data.SalesReturnItem)
            {
                if (items.ReturnedQuantity > 0)
                {
                    rpt.NewLine();

                    rpt.Write(items.ProductStock.Product.Name.ToUpper(), 16, 2);

                    if (items.ProductStock.RackNo.ToString() == "")
                        rpt.Write("  -  ", 4, 18); // Only return RankNo
                    else
                        rpt.Write(items.ProductStock.RackNo.ToString(), 4, 18);

                    rpt.Write(items.ProductStock.BatchNo, 7, true);
                    rpt.Write(string.Format("{0:MM/yy}", items.ProductStock.ExpireDate), 6, true);
                    rpt.Write(string.Format("{0:0}", items.ReturnedQuantity), 4, true, Alignment.Right);
                    decimal? SellingAmount = 0;

                    if (items.SellingPrice > 0)
                    {
                        rpt.Write(string.Format("{0:0.00}", items.SellingPrice), 7, true, Alignment.Right);
                        SellingAmount = (items.ReturnedQuantity * items.SellingPrice);
                    }
                    else
                    {
                        rpt.Write(string.Format("{0:0.00}", items.ProductStock.SellingPrice), 7, true, Alignment.Right);
                        SellingAmount = items.ReturnedQuantity * items.ProductStock.SellingPrice;
                    }

                    totReturnAmount += items.ReturnAmt;
                    rpt.Write(string.Format("{0:0.00}", SellingAmount), 8, true, Alignment.Right);
                    rpt.Write();

                    len++;
                    totReturnDiscount = ((items.Discount / 100) * items.OnlyReturnAmount);
                    totReturnAmount = totReturnAmount - totReturnDiscount;
                    ReturnDiscount += totReturnDiscount;

                    TotDiscountPer = TotDiscountPer + items.Discount;

                    items.IgstValue = ((items.OnlyReturnAmount - totReturnDiscount) - (((items.OnlyReturnAmount - totReturnDiscount) * 100) / (items.Igst + 100)));
                    decimal? getGst = ((items.OnlyReturnAmount - totReturnDiscount) - (((items.OnlyReturnAmount - totReturnDiscount) * 100) / ((items.Cgst + items.Sgst) + 100)));
                    items.CgstValue = getGst / 2;
                    items.SgstValue = getGst / 2;
                }
            }
            rpt.NewLine();
            rpt.NewLine();
            
            rpt.Write();
            rpt.Drawline(1);

            rpt.NewLine();

            if (data.SalesReturnItem.Sum(z => z.CgstValue) != 0 && data.SalesReturnItem.Sum(z => z.SgstValue) != 0 && !gstSelection)
            {
                rpt.Write("CGST: " + string.Format("{0:0.00}", data.SalesReturnItem.Sum(z => z.CgstValue)), 13, 2);
                rpt.Write((data.Instance.isUnionTerritory == true ? "UTGST: " : "SGST: ") + string.Format("{0:0.00}", data.SalesReturnItem.Sum(z => z.SgstValue)), 13, 15);
            }
            else if(!gstSelection)
            {
                rpt.Write("CGST: " + string.Format("{0:0.00}", data.SalesReturnItem.Sum(z => z.CgstValue)), 13, 2);
                rpt.Write((data.Instance.isUnionTerritory == true ? "UTGST: " : "SGST: ") + string.Format("{0:0.00}", data.SalesReturnItem.Sum(z => z.SgstValue)), 13, 15);
            }

            if (ReturnDiscount != 0)
            {
                rpt.Write("Disc." + Math.Round((decimal)TotDiscountPer, 2) + "%: " + " -" + string.Format("{0:0.00}", ReturnDiscount), 28, 31, Alignment.Right);
            }
            else
            {
                rpt.Write("Disc." + Math.Round((decimal)TotDiscountPer, 2) + "%: " + " -" + string.Format("{0:0.00}", ReturnDiscount), 28, 31, Alignment.Right);
            }
            rpt.NewLine();

            rpt.Write("User Name: " + data.HQueUser.Name, 30, 2);
            decimal retAmount = data.RoundoffNetAmount as decimal? ?? 0; //RoundOffCalculation(data.Net, totReturnAmount);

            rpt.Write("R.Off: " + string.Format("{0:0.00}", retAmount), 28, 31, Alignment.Right);
            if (data.SalesReturn.ReturnCharges > 0 && data.SalesReturn.ReturnCharges != null)
            {
                rpt.NewLine();
                rpt.Write("Ret. Charges: " + string.Format("{0:0.00}", data.SalesReturn.ReturnCharges), 28, 31, Alignment.Right);

                rpt.NewLine();
                //rpt.Write("Return Amount: " + string.Format("{0:0.00}", Math.Round(((decimal)data.Net - (decimal)totReturnAmount + retAmount) * (-1) - (decimal)data.SalesReturn.ReturnCharges)), 28, 31, Alignment.Right, data.IsBold);
                rpt.Write("Return Amount: " + string.Format("{0:0.00}", (data.NetAmount) * (-1) - (decimal)data.SalesReturn.ReturnCharges), 28, 31, Alignment.Right, data.IsBold);

                rpt.NewLine();
                //data.InWords = ((long)((Math.Round((decimal)data.Net - (decimal)totReturnAmount + retAmount)) * (-1) - Math.Round((decimal)data.SalesReturn.ReturnCharges))).ConvertNumbertoWords();
                data.InWords = ((decimal)((data.NetAmount) * (-1) - (decimal)data.SalesReturn.ReturnCharges)).ConvertNumbertoWords();
            }
            else
            {
                rpt.NewLine();
                //rpt.Write("Return Amount: " + string.Format("{0:0.00}", Math.Round(((decimal)data.Net - (decimal)totReturnAmount + retAmount)) * (-1)), 28, 31, Alignment.Right, data.IsBold);
                rpt.Write("Return Amount: " + string.Format("{0:0.00}", data.NetAmount * (-1)), 28, 31, Alignment.Right, data.IsBold);

                rpt.NewLine();
                //data.InWords = ((long)(Math.Round((decimal)data.Net - (decimal)totReturnAmount + retAmount)) * (-1)).ConvertNumbertoWords();
                data.InWords = ((decimal)(data.NetAmount) * (-1)).ConvertNumbertoWords();
            }

            rpt.Write("Rupees " + data.InWords + " Only", 40, 2, TextAdjustment.Wrap);
			if (gstSelection)
            {
                rpt.NewLine();
                rpt.Write();
                rpt.Drawline(1);
                GenerateGstSelected(rpt);
            }
        }       

        private void WriteReportOnlyReturnItems_A5_Vat_WithHeader_RackNo(PlainTextReport rpt, IInvoice data)
        {
            decimal? totReturnAmount = 0;
            decimal? totReturnDiscount = 0;
            decimal? ReturnDiscount = 0;
            decimal? TotDiscountPer = 0;
            int len = 0;

            foreach (var items in data.SalesReturnItem)
            {
                if (items.ReturnedQuantity > 0)
                {
                    rpt.NewLine();

                    rpt.Write(items.ProductStock.Product.Name.ToUpper(), 16, 2);

                    if (items.ProductStock.RackNo.ToString() == "")
                        rpt.Write("  -  ", 4, 18); // Only return RankNo
                    else
                        rpt.Write(items.ProductStock.RackNo.ToString(), 4, 18);

                    rpt.Write(items.ProductStock.BatchNo, 7, true);
                    rpt.Write(string.Format("{0:MM/yy}", items.ProductStock.ExpireDate), 6, true);
                    rpt.Write(string.Format("{0:0}", items.ReturnedQuantity), 4, true, Alignment.Right);
                    decimal? SellingAmount = 0;

                    if (items.SellingPrice > 0)
                    {
                        rpt.Write(string.Format("{0:0.00}", items.SellingPrice), 7, true, Alignment.Right);
                        SellingAmount = (items.ReturnedQuantity * items.SellingPrice);
                    }
                    else
                    {
                        rpt.Write(string.Format("{0:0.00}", items.ProductStock.SellingPrice), 7, true, Alignment.Right);
                        SellingAmount = items.ReturnedQuantity * items.ProductStock.SellingPrice;
                    }

                    totReturnAmount += items.ReturnAmt;
                    rpt.Write(string.Format("{0:0.00}", SellingAmount), 8, true, Alignment.Right);
                    rpt.Write();

                    len++;
                    totReturnDiscount = ((items.Discount / 100) * items.OnlyReturnAmount);
                    totReturnAmount = totReturnAmount - totReturnDiscount;
                    ReturnDiscount += totReturnDiscount;

                }
            }
            rpt.NewLine();
            rpt.NewLine();

            rpt.Write();
            rpt.Drawline(1);

            rpt.NewLine();

            decimal retAmount = data.RoundoffNetAmount as decimal? ?? 0; //RoundOffCalculation(data.Net, totReturnAmount);

            rpt.Write("R.Off: " + string.Format("{0:0.00}", retAmount), 28, 2, Alignment.Left);
            if (ReturnDiscount != 0)
            {
                rpt.Write("Disc." + Math.Round((decimal)TotDiscountPer, 2) + "%: " + " -" + string.Format("{0:0.00}", ReturnDiscount), 28, 31, Alignment.Right);
            }
            else
            {
                rpt.Write("Disc." + Math.Round((decimal)TotDiscountPer, 2) + "%: " + " -" + string.Format("{0:0.00}", ReturnDiscount), 28, 31, Alignment.Right);
            }
            rpt.NewLine();

            rpt.Write("User Name: " + data.HQueUser.Name, 30, 2);

            //rpt.Write("RETURN AMOUNT: " + string.Format("{0:0.00}", Math.Round(((decimal)data.Net - (decimal)totReturnAmount + retAmount)) * (-1)), 28, 31, Alignment.Right, data.IsBold);
            rpt.Write("RETURN AMOUNT: " + string.Format("{0:0.00}", data.NetAmount * (-1)), 28, 31, Alignment.Right, data.IsBold);

            rpt.NewLine();
            //data.InWords = ((long)(Math.Round((decimal)data.Net - (decimal)totReturnAmount + retAmount)) * (-1)).ConvertNumbertoWords();
            data.InWords = ((decimal)(data.NetAmount) * (-1)).ConvertNumbertoWords();
            rpt.Write("Rupees " + data.InWords + " Only", 30, 2, TextAdjustment.Wrap);   
        }

        private void WriteReportOnlyReturnItems_A5_Vat_EPSON(PlainTextReport rpt, IInvoice data)
        {
            decimal? totReturnAmount = 0;
            decimal? totReturnDiscount = 0;
            decimal? ReturnDiscount = 0;
            int len = 0;

            foreach (var items in data.SalesReturnItem)
            {
                if (items.ReturnedQuantity > 0)
                {
                    rpt.NewLine();
                    rpt.Write(items.ProductStock.Product.Name.ToUpper(), 20, 2);
                    rpt.Write(items.ProductStock.BatchNo, 8, true);
                    rpt.Write(string.Format("{0:MM/yy}", items.ProductStock.ExpireDate), 5, true);
                    rpt.Write(string.Format("{0:0}", items.ReturnedQuantity), 4, true, Alignment.Right);
                    decimal? SellingAmount = 0;

                    if (items.SellingPrice > 0)
                    {
                        rpt.Write(string.Format("{0:0.00}", items.SellingPrice), 7, true, Alignment.Right);
                        SellingAmount = (items.ReturnedQuantity * items.SellingPrice);
                    }
                    else
                    {
                        rpt.Write(string.Format("{0:0.00}", items.ProductStock.SellingPrice), 7, true, Alignment.Right);
                        SellingAmount = items.ReturnedQuantity * items.ProductStock.SellingPrice;
                    }

                    totReturnAmount += items.ReturnAmt;
                    rpt.Write(string.Format("{0:0.00}", SellingAmount), 8, true, Alignment.Right);
                    rpt.Write();

                    len++;

                    totReturnDiscount = ((items.Discount / 100) * items.OnlyReturnAmount);
                    totReturnAmount = totReturnAmount - totReturnDiscount;
                    ReturnDiscount += totReturnDiscount;
                }
            }
            rpt.NewLine();
            rpt.NewLine();

            //rpt.Write("RETURN AMT: " + string.Format("{0:0.00}", totReturnAmount), 18, 41, Alignment.Right, data.IsBold);

            rpt.Write();
            rpt.Drawline(1);

            rpt.NewLine();
            decimal retAmount = data.RoundoffNetAmount as decimal? ?? 0; //RoundOffCalculation(data.Net, totReturnAmount);
            rpt.Write("R.Off: " + string.Format("{0:0.00}", retAmount), 15, 1, Alignment.Left);
            if (ReturnDiscount != 0)
                rpt.Write("Disc.: -" + string.Format("{0:0.00}", ReturnDiscount), 15, 17);
            else
                rpt.Write("Disc.: " + string.Format("{0:0.00}", ReturnDiscount), 15, 17);
            //rpt.NewLine();
            //rpt.Write("RETURN AMOUNT: " + string.Format("{0:0.00}", Math.Round(((decimal)data.Net - (decimal)totReturnAmount) + retAmount) * (-1)), 28, 31, Alignment.Right, data.IsBold);
            rpt.Write("RETURN AMOUNT: " + string.Format("{0:0.00}", data.NetAmount * (-1)), 28, 31, Alignment.Right, data.IsBold);

            //data.InWords = ((long)(Math.Round(((decimal)data.Net - (decimal)totReturnAmount) + retAmount)) * (-1)).ConvertNumbertoWords() + " only";
            data.InWords = ((decimal)(data.NetAmount) * (-1)).ConvertNumbertoWords() + " only";
        }

        private void WriteReportOnlyReturnItems_A5_Vat_EPSON_GST(PlainTextReport rpt, IInvoice data)
        {
            bool gstSelection = Convert.ToBoolean(data.Instance.Gstselect);
            decimal? totReturnAmount = 0;
            decimal? totReturnDiscount = 0;
            decimal? ReturnDiscount = 0;
            int len = 0;

            foreach (var items in data.SalesReturnItem)
            {
                if (items.ReturnedQuantity > 0)
                {
                    rpt.NewLine();
                    if (data.TaxRefNo == 1 && !gstSelection)
                    {
                        var gst = (items.Igst == 0 || items.Igst == null) ? (items.Cgst + items.Sgst) : items.Igst;
                        rpt.Write(items.ProductStock.Product.Name.ToUpper(), 16, 2);
                        rpt.Write(gst.ToString(), 4, 18);
                    }
                    else
                        rpt.Write(items.ProductStock.Product.Name.ToUpper(), 16, 2);
                    if (!gstSelection)
                        rpt.Write(items.ProductStock.BatchNo, 8, true);
                    else
                        rpt.Write(items.ProductStock.BatchNo, 12, true);
                    rpt.Write(string.Format("{0:MM/yy}", items.ProductStock.ExpireDate), 5, true);
                    rpt.Write(string.Format("{0:0}", items.ReturnedQuantity), 4, true, Alignment.Right);
                    decimal? SellingAmount = 0;

                    if (items.SellingPrice > 0)
                    {
                        rpt.Write(string.Format("{0:0.00}", items.SellingPrice), 7, true, Alignment.Right);
                        SellingAmount = (items.ReturnedQuantity * items.SellingPrice);
                    }
                    else
                    {
                        rpt.Write(string.Format("{0:0.00}", items.ProductStock.SellingPrice), 7, true, Alignment.Right);
                        SellingAmount = items.ReturnedQuantity * items.ProductStock.SellingPrice;
                    }

                    totReturnAmount += items.ReturnAmt;
                    rpt.Write(string.Format("{0:0.00}", SellingAmount), 8, true, Alignment.Right);
                    rpt.Write();

                    len++;

                    totReturnDiscount = ((items.Discount / 100) * items.OnlyReturnAmount);
                    totReturnAmount = totReturnAmount - totReturnDiscount;
                    ReturnDiscount += totReturnDiscount;


                    items.IgstValue = ((items.OnlyReturnAmount - totReturnDiscount) - (((items.OnlyReturnAmount - totReturnDiscount) * 100) / (items.Igst + 100)));
                    decimal? getGst = ((items.OnlyReturnAmount - totReturnDiscount) - (((items.OnlyReturnAmount - totReturnDiscount) * 100) / ((items.Cgst + items.Sgst) + 100)));
                    items.CgstValue = getGst / 2;
                    items.SgstValue = getGst / 2;
                }
            }
            rpt.NewLine();
            rpt.NewLine();

            rpt.Write();
            rpt.Drawline(1);

            rpt.NewLine();
            if (!gstSelection)
            {
                if (data.CgstTotalValue != 0 && data.SgstTotalValue != 0)
                {
                    rpt.Write("CGST: " + string.Format("{0:0.00}", data.SalesReturnItem.Sum(z => z.CgstValue)), 13, 2);
                    rpt.Write((data.Instance.isUnionTerritory == true ? "UTGST: " : "SGST: ") + string.Format("{0:0.00}", data.SalesReturnItem.Sum(z => z.SgstValue)), 13, 15);
                }
                else
                {
                    rpt.Write("CGST: " + string.Format("{0:0.00}", data.SalesReturnItem.Sum(z => z.CgstValue)), 13, 2);
                    rpt.Write((data.Instance.isUnionTerritory == true ? "UTGST: " : "SGST: ") + string.Format("{0:0.00}", data.SalesReturnItem.Sum(z => z.SgstValue)), 13, 15);
                }
            }
            if (ReturnDiscount != 0)
                rpt.Write("Disc.: -" + string.Format("{0:0.00}", ReturnDiscount), 28, 32, Alignment.Right);
            else
                rpt.Write("Disc.: " + string.Format("{0:0.00}", ReturnDiscount), 28, 32, Alignment.Right);
            rpt.NewLine();
            decimal retAmount = data.RoundoffNetAmount as decimal? ?? 0; //RoundOffCalculation(data.Net, totReturnAmount);
            rpt.Write("R.Off: " + string.Format("{0:0.00}", retAmount), 28, 32, Alignment.Right);
            rpt.NewLine();
            if (data.SalesReturn.ReturnCharges > 0 && data.SalesReturn.ReturnCharges != null)
            {                
                rpt.Write("Ret. Charges: " + string.Format("{0:0.00}", data.SalesReturn.ReturnCharges), 28, 32, Alignment.Right);
                rpt.NewLine();
                //rpt.Write("Return Amount: " + string.Format("{0:0.00}", Math.Round((((decimal)data.Net - (decimal)totReturnAmount) + retAmount) * (-1) - (decimal)data.SalesReturn.ReturnCharges)), 28, 31, Alignment.Right, data.IsBold);
                rpt.Write("Return Amount: " + string.Format("{0:0.00}", data.NetAmount * (-1) - (decimal)data.SalesReturn.ReturnCharges), 28, 31, Alignment.Right, data.IsBold);
            }
            else
            {
                //rpt.Write("Return Amount: " + string.Format("{0:0.00}", Math.Round(((decimal)data.Net - (decimal)totReturnAmount) + retAmount) * (-1)), 28, 31, Alignment.Right, data.IsBold);
                rpt.Write("Return Amount: " + string.Format("{0:0.00}", data.NetAmount * (-1)), 28, 31, Alignment.Right, data.IsBold);
            }

            //data.InWords = ((long)(Math.Round(((decimal)data.Net - (decimal)totReturnAmount) + retAmount)) * (-1)).ConvertNumbertoWords() + " only";
            data.InWords = ((decimal)(data.NetAmount) * (-1)).ConvertNumbertoWords() + " only";
            if (gstSelection)
            {
                rpt.Write();
                rpt.Drawline(1);
                GenerateGstSelected(rpt);
            }
        }

        private void WriteReportOnlyReturnItems_A5_Vat_WithHeaderWithMFR(PlainTextReport rpt, IInvoice data) //, out string payableAmount
        {
            decimal? totReturnAmount = 0;
            decimal? totReturnDiscount = 0;
            decimal? ReturnDiscount = 0;
            int len = 0;

            foreach (var items in data.SalesReturnItem)
            {
                if (items.ReturnedQuantity > 0)
                {
                    rpt.NewLine();
                    rpt.Write(string.Format("{0:0}", (len + 1)), 4, true, Alignment.Right);
                    rpt.Write(items.ProductStock.Product.Name.ToUpper(), 20, true);

                    if (string.IsNullOrEmpty(items.ProductStock.Product.Manufacturer))
                    {
                        rpt.Write("-", 4, true);
                    }
                    else
                    {
                        if (items.ProductStock.Product.Manufacturer.ToUpper().Length > 3)
                        {
                            rpt.Write(items.ProductStock.Product.Manufacturer.ToUpper().Substring(0, 3), 4, true);
                        }
                        else
                        {
                            rpt.Write(items.ProductStock.Product.Manufacturer.ToUpper(), 4, true);
                        }
                    }

                    rpt.Write(string.Format("{0:0}", items.ReturnedQuantity), 4, true, Alignment.Right);
                    rpt.Write(items.ProductStock.BatchNo, 8, true, Alignment.Right);

                    rpt.Write(string.Format("{0:MM/yy}", items.ProductStock.ExpireDate), 6, true);

                    decimal? SellingAmount = 0;
                    if (items.SellingPrice > 0)
                    {
                        rpt.Write(string.Format("{0:0.00}", items.SellingPrice), 7, true, Alignment.Right);
                        SellingAmount = (items.ReturnedQuantity * items.SellingPrice);
                    }
                    else
                    {
                        rpt.Write(string.Format("{0:0.00}", items.ProductStock.SellingPrice), 7, true, Alignment.Right);
                        SellingAmount = items.ReturnedQuantity * items.ProductStock.SellingPrice;
                    }

                    totReturnAmount = totReturnAmount + SellingAmount;
                    rpt.Write(string.Format("{0:0.00}", SellingAmount), 8, true, Alignment.Right);
                    rpt.Write();

                    len++;
                    totReturnDiscount = ((items.Discount / 100) * items.OnlyReturnAmount);
                    totReturnAmount = totReturnAmount - totReturnDiscount;
                    ReturnDiscount += totReturnDiscount;
                }
            }
            rpt.NewLine();
            rpt.NewLine();

            //rpt.Write("RETURN AMT: " + string.Format("{0:0.00}", totReturnAmount), 20, 50, Alignment.Right, data.IsBold);

            rpt.Write();
            rpt.Drawline(1);

            rpt.NewLine();
            decimal retAmount = data.RoundoffNetAmount as decimal? ?? 0; //RoundOffCalculation(data.Net, totReturnAmount);
            rpt.Write("R.Off: " + string.Format("{0:0.00}", retAmount), 15, 1, Alignment.Left);
            if (ReturnDiscount != 0)
                rpt.Write("Disc.: -" + string.Format("{0:0.00}", ReturnDiscount), 15, 17);
            else
                rpt.Write("Disc.: " + string.Format("{0:0.00}", ReturnDiscount), 15, 17);
            //rpt.NewLine();
            //rpt.Write("RETURN AMOUNT: " + string.Format("{0:0.00}", Math.Round(((decimal)data.Net - (decimal)totReturnAmount) + retAmount) * (-1)), 28, 42, Alignment.Right, data.IsBold);
            rpt.Write("RETURN AMOUNT: " + string.Format("{0:0.00}", data.NetAmount * (-1)), 28, 42, Alignment.Right, data.IsBold);

            //payableAmount = ((long)(data.RoundOff - totReturnAmount)).ConvertNumbertoWords();
            rpt.Write();
            rpt.Drawline(1);
        }


        private void WriteReportOnlyReturnItems_A5_Vat_WithHeaderWithMFR_GST(PlainTextReport rpt, IInvoice data) //, out string payableAmount
        {
            bool gstSelection = Convert.ToBoolean(data.Instance.Gstselect);
            decimal? totReturnAmount = 0;
            decimal? totReturnDiscount = 0;
            decimal? ReturnDiscount = 0;
            int len = 0;

            foreach (var items in data.SalesReturnItem)
            {
                if (items.ReturnedQuantity > 0)
                {
                    rpt.NewLine();
                    rpt.Write(string.Format("{0}", (len + 1)), 3, true, Alignment.Center);

                    rpt.Write(items.ProductStock.Product.Name.ToUpper(), 17, true);

                    if (string.IsNullOrEmpty(items.ProductStock.Product.Manufacturer))
                    {
                        rpt.Write("-", 4, true);
                    }
                    else
                    {
                        if (items.ProductStock.Product.Manufacturer.ToUpper().Length > 3)
                        {
                            rpt.Write(items.ProductStock.Product.Manufacturer.ToUpper().Substring(0, 3), 4, true);
                        }
                        else
                        {
                            rpt.Write(items.ProductStock.Product.Manufacturer.ToUpper(), 4, true);
                        }
                    }

                    rpt.Write(string.Format("{0:0}", items.ReturnedQuantity), 4, true, Alignment.Right);
                    rpt.Write(items.ProductStock.BatchNo, 8, true, Alignment.Right);

                    rpt.Write(string.Format("{0:MM/yy}", items.ProductStock.ExpireDate), 6, true);

                    decimal? SellingAmount = 0;
                    if (items.SellingPrice > 0)
                    {
                        rpt.Write(string.Format("{0:0.00}", items.SellingPrice), 7, true, Alignment.Right);
                        SellingAmount = (items.ReturnedQuantity * items.SellingPrice);
                    }
                    else
                    {
                        rpt.Write(string.Format("{0:0.00}", items.ProductStock.SellingPrice), 7, true, Alignment.Right);
                        SellingAmount = items.ReturnedQuantity * items.ProductStock.SellingPrice;
                    }

                    if (data.TaxRefNo == 1 && !gstSelection)
                    {
                        var gst = (items.Igst == 0 || items.Igst == null) ? (items.Cgst + items.Sgst) : items.Igst;
                        rpt.Write(gst.ToString(), 5, true, Alignment.Right);
                    }

                    totReturnAmount = totReturnAmount + SellingAmount;
                    if (!gstSelection)
                        rpt.Write(string.Format("{0:0.00}", SellingAmount), 7, true, Alignment.Right);
                    else
                        rpt.Write(string.Format("{0:0.00}", SellingAmount), 12, true, Alignment.Right);
                    rpt.Write();

                    len++;
                    totReturnDiscount = ((items.Discount / 100) * items.OnlyReturnAmount);
                    totReturnAmount = totReturnAmount - totReturnDiscount;
                    ReturnDiscount += totReturnDiscount;

                    items.IgstValue = ((items.OnlyReturnAmount - totReturnDiscount) - (((items.OnlyReturnAmount - totReturnDiscount) * 100) / (items.Igst + 100)));
                    decimal? getGst = ((items.OnlyReturnAmount - totReturnDiscount) - (((items.OnlyReturnAmount - totReturnDiscount) * 100) / ((items.Cgst + items.Sgst) + 100)));
                    items.CgstValue = getGst / 2;

                    items.SgstValue = getGst / 2;
                }
            }
            rpt.NewLine();
            rpt.NewLine();

            rpt.Write();
            rpt.Drawline(1);
            rpt.NewLine();

            if (data.TaxRefNo == 1 && !gstSelection)
            {
                rpt.Write("CGST: " + string.Format("{0:0.00}", data.SalesReturnItem.Sum(z => z.CgstValue)), 13, 2);
                rpt.Write((data.Instance.isUnionTerritory == true ? "UTGST: " : "SGST: ") + string.Format("{0:0.00}", data.SalesReturnItem.Sum(z => z.SgstValue)), 13, 15);
            }

            if (ReturnDiscount != 0)
                rpt.Write("Disc.: -" + string.Format("{0:0.00}", ReturnDiscount), 28, 42, Alignment.Right);
            else
                rpt.Write("Disc.: " + string.Format("{0:0.00}", ReturnDiscount), 28, 42, Alignment.Right);

            rpt.NewLine();
            decimal retAmount = data.RoundoffNetAmount as decimal? ?? 0; //RoundOffCalculation(data.Net, totReturnAmount);
            rpt.Write("R.Off: " + string.Format("{0:0.00}", retAmount), 28, 42, Alignment.Right);
            rpt.NewLine();
            if (data.SalesReturn.ReturnCharges > 0 && data.SalesReturn.ReturnCharges != null)
            {
                rpt.Write("Ret. Charges: " + string.Format("{0:0.00}", data.SalesReturn.ReturnCharges), 28, 42, Alignment.Right);
                rpt.NewLine();
                //rpt.Write("Return Amount: " + string.Format("{0:0.00}", ((((decimal)data.Net - (decimal)totReturnAmount) + retAmount) * (-1)) - (decimal)data.SalesReturn.ReturnCharges), 28, 42, Alignment.Right, data.IsBold);
                rpt.Write("Return Amount: " + string.Format("{0:0.00}", (data.NetAmount * (-1)) - (decimal)data.SalesReturn.ReturnCharges), 28, 42, Alignment.Right, data.IsBold);
            }
            else
            {
                //rpt.Write("Return Amount: " + string.Format("{0:0.00}", Math.Round(((decimal)data.Net - (decimal)totReturnAmount) + retAmount) * (-1)), 28, 42, Alignment.Right, data.IsBold);
                rpt.Write("Return Amount: " + string.Format("{0:0.00}", data.NetAmount * (-1)), 28, 42, Alignment.Right, data.IsBold);
            }
                

            //payableAmount = ((long)(data.RoundOff - totReturnAmount)).ConvertNumbertoWords();
            rpt.Write();
            rpt.Drawline(1);
            if (gstSelection)
            {
                GenerateGstSelected(rpt);
            }
        }

        private void WriteReportOnlyReturnItems_A5_Vat_WithHeaderWithMFR6Inch(PlainTextReport rpt, IInvoice data)
        {

            decimal? totReturnAmount = 0;
            decimal? totReturnDiscount = 0;
            decimal? ReturnDiscount = 0;
            int len = 0;

            foreach (var items in data.SalesReturnItem)
            {
                if (items.ReturnedQuantity > 0)
                {
                    rpt.NewLine();
                    len++;

                    rpt.Write(string.Format("{0:0}", len), 3, true, Alignment.Right);
                    rpt.Write(items.ProductStock.Product.Name.ToUpper(), 18, true);

                    if (string.IsNullOrEmpty(items.ProductStock.Product.Manufacturer))
                    {
                        rpt.Write("-", 3, true);
                    }
                    else
                    {
                        if (items.ProductStock.Product.Manufacturer.ToUpper().Length > 3)
                        {
                            rpt.Write(items.ProductStock.Product.Manufacturer.ToUpper().Substring(0, 3), 3, true);
                        }
                        else
                        {
                            rpt.Write(items.ProductStock.Product.Manufacturer.ToUpper(), 3, true);
                        }
                    }

                    rpt.Write(string.Format("{0:0}", items.ReturnedQuantity), 4, true, Alignment.Right);
                    rpt.Write(items.ProductStock.BatchNo, 8, true);

                    rpt.Write(string.Format("{0:MM/yy}", items.ProductStock.ExpireDate), 5, true);

                    decimal? SellingAmount = 0;

                    if (items.SellingPrice > 0)
                    {
                        rpt.Write(string.Format("{0:0.00}", items.SellingPrice), 6, true, Alignment.Right);
                        SellingAmount = (items.ReturnedQuantity * items.SellingPrice);
                    }
                    else
                    {
                        rpt.Write(string.Format("{0:0.00}", items.ProductStock.SellingPrice), 6, true, Alignment.Right);
                        SellingAmount = items.ReturnedQuantity * items.ProductStock.SellingPrice;
                    }
                    rpt.Write(string.Format("{0:0.00}", SellingAmount), 7, true, Alignment.Right);
                    rpt.Write();

                    totReturnAmount = totReturnAmount + SellingAmount;

                    totReturnDiscount = ((items.Discount / 100) * items.OnlyReturnAmount);
                    totReturnAmount = totReturnAmount - totReturnDiscount;
                    ReturnDiscount += totReturnDiscount;
                }
            }
            rpt.NewLine();
            rpt.NewLine();

            //rpt.Write("RETURN AMT: " + string.Format("{0:0.00}", totReturnAmount), 20, 39, Alignment.Right, data.IsBold);

            rpt.Write();
            rpt.Drawline(1);

            rpt.NewLine();
            decimal retAmount = data.RoundoffNetAmount as decimal? ?? 0; //RoundOffCalculation(data.Net, totReturnAmount);
            rpt.Write("R.Off: " + string.Format("{0:0.00}", retAmount), 15, 1, Alignment.Left);
            if (ReturnDiscount != 0)
            { rpt.Write("Disc.: -" + string.Format("{0:0.00}", ReturnDiscount), 15, 17); }
            else
            { rpt.Write("Disc.: " + string.Format("{0:0.00}", ReturnDiscount), 15, 17); }
            //rpt.NewLine();
            //rpt.Write("RETURN AMOUNT: " + string.Format("{0:0.00}", Math.Round(((decimal)data.Net - (decimal)totReturnAmount) + retAmount) * (-1)), 28, 31, Alignment.Right, data.IsBold);
            rpt.Write("RETURN AMOUNT: " + string.Format("{0:0.00}", data.NetAmount * (-1)), 28, 31, Alignment.Right, data.IsBold);

            rpt.Write();
            rpt.Drawline(1);
        }

        private void WriteReportOnlyReturnItems_A5_Vat_WithHeaderWithMFR6Inch_GST(PlainTextReport rpt, IInvoice data)
        {
            bool gstSelection = Convert.ToBoolean(data.Instance.Gstselect);
            decimal? totReturnAmount = 0;
            decimal? totReturnDiscount = 0;
            decimal? ReturnDiscount = 0;
            int len = 0;

            foreach (var items in data.SalesReturnItem)
            {
                if (items.ReturnedQuantity > 0)
                {
                    rpt.NewLine();
                    len++;

                    rpt.Write(string.Format("{0:0}", len), 3, true, Alignment.Right);

                    //if (data.TaxRefNo == 1)
                    //{
                    //    var gst = (items.Igst == 0 || items.Igst == null) ? (items.Cgst + items.Sgst) : items.Igst;                       
                    //    rpt.Write(gst.ToString(), 4, 18);
                    //}
                    
                    rpt.Write(items.ProductStock.Product.Name.ToUpper(), 18, true);

                    if (string.IsNullOrEmpty(items.ProductStock.Product.Manufacturer))
                    {
                        rpt.Write("-", 3, true);
                    }
                    else
                    {
                        if (items.ProductStock.Product.Manufacturer.ToUpper().Length > 3)
                        {
                            rpt.Write(items.ProductStock.Product.Manufacturer.ToUpper().Substring(0, 3), 3, true);
                        }
                        else
                        {
                            rpt.Write(items.ProductStock.Product.Manufacturer.ToUpper(), 3, true);
                        }
                    }

                    rpt.Write(string.Format("{0:0}", items.ReturnedQuantity), 4, true, Alignment.Right);
                    rpt.Write(items.ProductStock.BatchNo, 8, true);

                    rpt.Write(string.Format("{0:MM/yy}", items.ProductStock.ExpireDate), 5, true);

                    decimal? SellingAmount = 0;

                    if (items.SellingPrice > 0)
                    {
                        rpt.Write(string.Format("{0:0.00}", items.SellingPrice), 6, true, Alignment.Right);
                        SellingAmount = (items.ReturnedQuantity * items.SellingPrice);
                    }
                    else
                    {
                        rpt.Write(string.Format("{0:0.00}", items.ProductStock.SellingPrice), 6, true, Alignment.Right);
                        SellingAmount = items.ReturnedQuantity * items.ProductStock.SellingPrice;
                    }
                    rpt.Write(string.Format("{0:0.00}", SellingAmount), 7, true, Alignment.Right);
                    rpt.Write();

                    totReturnAmount = totReturnAmount + SellingAmount;

                    totReturnDiscount = ((items.Discount / 100) * items.OnlyReturnAmount);
                    totReturnAmount = totReturnAmount - totReturnDiscount;
                    ReturnDiscount += totReturnDiscount;

                    items.IgstValue = ((items.OnlyReturnAmount - totReturnDiscount) - (((items.OnlyReturnAmount - totReturnDiscount) * 100) / (items.Igst + 100)));
                    decimal? getGst = ((items.OnlyReturnAmount - totReturnDiscount) - (((items.OnlyReturnAmount - totReturnDiscount) * 100) / ((items.Cgst + items.Sgst) + 100)));
                    items.CgstValue = getGst / 2;
                    //items.SgstValue = ((items.OnlyReturnAmount - totReturnDiscount) - (((items.OnlyReturnAmount - totReturnDiscount) * 100) / (items.Sgst + 100)));
                    items.SgstValue = getGst / 2;
                }
            }
            rpt.NewLine();
            rpt.NewLine();


            rpt.Write();
            rpt.Drawline(1);
            rpt.NewLine();
            if (data.TaxRefNo == 1 && !gstSelection)
            {
                rpt.Write("CGST: " + string.Format("{0:0.00}", data.SalesReturnItem.Sum(z => z.CgstValue)), 13, 2);
                rpt.Write((data.Instance.isUnionTerritory == true ? "UTGST: " : "SGST: ") + string.Format("{0:0.00}", data.SalesReturnItem.Sum(z => z.SgstValue)), 13, 15);
            
            }

            if (ReturnDiscount != 0)
            { rpt.Write("Disc.: -" + string.Format("{0:0.00}", ReturnDiscount), 28, 32, Alignment.Right); }
            else
            { rpt.Write("Disc.: " + string.Format("{0:0.00}", ReturnDiscount), 28, 32, Alignment.Right); }
            rpt.NewLine();
            decimal retAmount = data.RoundoffNetAmount as decimal? ?? 0; //RoundOffCalculation(data.Net, totReturnAmount);
            rpt.Write("R.Off: " + string.Format("{0:0.00}", retAmount), 28, 32, Alignment.Right);
            rpt.NewLine();
            if (data.SalesReturn.ReturnCharges > 0 && data.SalesReturn.ReturnCharges != null)
            {
                rpt.Write("Ret. Charges: " + string.Format("{0:0.00}", data.SalesReturn.ReturnCharges), 28, 32, Alignment.Right);
                rpt.NewLine();
                //rpt.Write("RETURN AMOUNT: " + string.Format("{0:0.00}", ((((decimal)data.Net - (decimal)totReturnAmount) + retAmount) * (-1) - (decimal)data.SalesReturn.ReturnCharges)), 28, 31, Alignment.Right, data.IsBold);
                rpt.Write("RETURN AMOUNT: " + string.Format("{0:0.00}", (data.NetAmount * (-1) - (decimal)data.SalesReturn.ReturnCharges)), 28, 31, Alignment.Right, data.IsBold);
            }
            else
            {
                //rpt.Write("RETURN AMOUNT: " + string.Format("{0:0.00}", (((decimal)data.Net - (decimal)totReturnAmount) + retAmount) * (-1)), 28, 31, Alignment.Right, data.IsBold);
                rpt.Write("RETURN AMOUNT: " + string.Format("{0:0.00}", data.NetAmount * (-1)), 28, 31, Alignment.Right, data.IsBold);
            }

            rpt.Write();
            rpt.Drawline(1);
			if (gstSelection)
            {
                GenerateGstSelected(rpt);
            }
        }

        private void WriteReportOnlyReturnItems_4InchRollNew_NonPharma(PlainTextReport rpt, IInvoice data)
        {
            bool gstSelection = Convert.ToBoolean(data.Instance.Gstselect);
            decimal? totReturnAmount = 0;
            decimal? totReturnDiscount = 0;
            decimal? ReturnDiscount = 0;
            int len = 0;

            foreach (var items in data.SalesReturnItem)
            {
                if (items.ReturnedQuantity > 0)
                {
                    rpt.NewLine();
                    rpt.Write((len + 1).ToString(), 3, 1);

                    if (items.ProductStock.Product.Name.Length > 13)
                    {
                        rpt.Write(items.ProductStock.Product.Name.ToUpper().Substring(0, 13), 13, 4);
                    }
                    else
                    {
                        rpt.Write(items.ProductStock.Product.Name.ToUpper(), 13, 4);
                    }
                    totReturnAmount += items.ReturnAmt;
                    rpt.Write(string.Format("{0:0.00}", items.SellingPrice == null ? items.ProductStock.SellingPrice : items.SellingPrice), 7, 18, Alignment.Right);
                    rpt.Write(string.Format("{0:0}", items.ReturnedQuantity), 4, 27, Alignment.Center);
                    rpt.Write(string.Format("{0:0.00}", ((items.SellingPrice == null ? items.ProductStock.SellingPrice : items.SellingPrice) * items.ReturnedQuantity)), 8, 30, Alignment.Right);

                    len++;

                    totReturnDiscount = ((items.Discount / 100) * items.OnlyReturnAmount);
                    totReturnAmount = totReturnAmount - totReturnDiscount;
                    ReturnDiscount += totReturnDiscount;
                }
            }
            rpt.NewLine();
            rpt.NewLine();

            //rpt.Write("RETURN AMT ", 14, 14);
            //rpt.Write(string.Format("{0:0.00}", totReturnAmount), 11, 27, Alignment.Right, data.IsBold);

            rpt.Write();
            rpt.Drawline(1);

            rpt.NewLine();
            decimal retAmount = data.RoundoffNetAmount as decimal? ?? 0; //RoundOffCalculation(data.Net, totReturnAmount);
            rpt.Write("R.Off: " + string.Format("{0:0.00}", retAmount), 15, 1, Alignment.Left);
            if (ReturnDiscount != 0)
            {
                rpt.Write("Disc.: -" + string.Format("{0:0.00}", ReturnDiscount), 12, 25);
            }
            else
            {
                rpt.Write("Disc.: " + string.Format("{0:0.00}", ReturnDiscount), 12, 25);
            }
            if (data.SalesReturn.ReturnCharges > 0 && data.SalesReturn.ReturnCharges != null)
            {
                rpt.NewLine();
                rpt.Write("Ret. Charges: " + string.Format("{0:0.00}", data.SalesReturn.ReturnCharges), 26, 12, Alignment.Right);
                rpt.NewLine();
                rpt.Write("RETURN AMOUNT ", 19, 10);
                //rpt.Write(string.Format("{0:0.00}", (Math.Round(((decimal)data.Net - (decimal)totReturnAmount) + retAmount) * (-1)) - Math.Round((decimal)data.SalesReturn.ReturnCharges)), 11, 27, Alignment.Right, data.IsBold);
                rpt.Write(string.Format("{0:0.00}", (data.NetAmount * (-1)) - (decimal)data.SalesReturn.ReturnCharges), 11, 27, Alignment.Right, data.IsBold);
                //data.InWords = ((long)(Math.Round((decimal)data.Net - (decimal)totReturnAmount)) * (-1)).ConvertNumbertoWords() + " only";
                data.InWords = ((decimal)(data.NetAmount) * (-1)).ConvertNumbertoWords() + " only";
            }
            else
            {
                rpt.NewLine();
                rpt.Write("RETURN AMOUNT ", 19, 10);
                //rpt.Write(string.Format("{0:0.00}", Math.Round(((decimal)data.Net - (decimal)totReturnAmount) + retAmount) * (-1)), 11, 27, Alignment.Right, data.IsBold);
                rpt.Write(string.Format("{0:0.00}", data.NetAmount * (-1)), 11, 27, Alignment.Right, data.IsBold);
                //data.InWords = ((long)(Math.Round((decimal)data.Net - (decimal)totReturnAmount)) * (-1)).ConvertNumbertoWords() + " only";
                data.InWords = ((decimal)(data.NetAmount) * (-1)).ConvertNumbertoWords() + " only";
            }
            if (gstSelection)
            {
                rpt.Write();
                rpt.Drawline(1);
                rpt.NewLine();
                rpt.Write("Composite tax payer not eligible to collect taxes", 35, 1, TextAdjustment.Wrap);
            }
        }

        protected string GenerateTransferPrint(StockTransfer data)
        {
            var rpt = new PlainTextReport
            {
                Columns = 91,
                Rows = 40
            };

            WriteTransferReportHeader_A4(rpt, data);
            WriteTransferReportItems_A4(rpt, data);

            WriteTransferReportFooter_A4(rpt, data);

            return rpt.Content.ToString();
        }

        private void WriteTransferReportHeader_A4(PlainTextReport rpt, StockTransfer data)
        {

            rpt.NewLine();
            rpt.Write(data.Instance.Name.ToUpper(), 89, 0, Alignment.Center, TextAdjustment.Wrap, data.IsBold);

            string adddressarea = string.Empty;

            if (!string.IsNullOrEmpty(data.Instance.Address))
            {
                adddressarea += data.Instance.Address;
            }
            if (!string.IsNullOrEmpty(data.Instance.Area))
            {
                adddressarea += "," + data.Instance.Area;
            }
            if (!string.IsNullOrEmpty(adddressarea))
            {
                rpt.NewLine();
                rpt.Write(string.Format("{0}", adddressarea.ToUpper().TrimEnd()), 89, 0, Alignment.Center, TextAdjustment.Wrap);
            }

            string cityState = string.Empty;
            if (!string.IsNullOrEmpty(data.Instance.City))
            {
                cityState += data.Instance.City + ",";
            }
            if (!string.IsNullOrEmpty(data.Instance.State))
            {
                cityState += data.Instance.State + ",";
            }
            if (!string.IsNullOrEmpty(data.Instance.Pincode))
            {
                cityState += data.Instance.Pincode;
            }

            if (!string.IsNullOrEmpty(cityState))
            {
                rpt.NewLine();
                rpt.Write(cityState.ToUpper(), 89, 0, Alignment.Center, TextAdjustment.Wrap);
            }


            string contact = string.Empty;

            if (!string.IsNullOrEmpty(data.Instance.Phone))
            {
                contact += "Ph: " + data.Instance.Phone;
            }
            if (!string.IsNullOrEmpty(data.Instance.Mobile))
            {
                contact += "," + "Mobile: " + data.Instance.Mobile;
            }
            if (!string.IsNullOrEmpty(contact))
            {
                rpt.NewLine();
                rpt.Write(contact.ToUpper(), 89, 0, Alignment.Center, TextAdjustment.Wrap);
            }

            rpt.NewLine();
            rpt.Write("DL No:  " + data.Instance.DrugLicenseNo, 89, 0, Alignment.Center, TextAdjustment.Wrap);

            rpt.NewLine(); 
            /*Based on Taxrefno tin and GStin value displayed by Poongodi on 03/07/2017*/
            if (data.TaxRefNo == 1)
            {
                rpt.Write("GSTIN:  " + data.Instance.GsTinNo, 89, 0, Alignment.Center, TextAdjustment.Wrap);
            }
            else
            {
                rpt.Write("TIN:  " + data.Instance.TinNo, 89, 0, Alignment.Center, TextAdjustment.Wrap);
            }
            rpt.Write();

            rpt.NewLine();
            rpt.NewLine();
            rpt.Write("Stock Transfer ", 89, 0, Alignment.Center, data.IsBold);
            rpt.Write();

            rpt.Drawline();

            rpt.NewLine();


            //--------------------
            rpt.Write("Transfer To: " + data.ToInstance.Name, 43);
            rpt.Write("Transfer No  : " + data.TransferNo, 25, 66);

            rpt.NewLine();
            rpt.Write("Address    : " + data.ToInstance.Address.ToUpper() + ',' + data.ToInstance.Area.ToUpper(), 43);
            rpt.Write("Transfer Date: " + string.Format("{0:dd/MM/yyyy}", data.TransferDate), 25, 66);
            rpt.NewLine();

            rpt.Write("Mobile     : " + data.Instance.Mobile.ToUpper(), 43);

            rpt.Write("Transfer Time: " + Convert.ToDateTime(data.TransferDate).ToString("hh:mm tt"), 25, 66);

            rpt.NewLine();

            rpt.Write("DL No      : " + data.ToInstance.DrugLicenseNo, 43);

            rpt.NewLine();
            if (data.TaxRefNo == 1)
            {
                rpt.Write("GSTIN      : " + data.ToInstance.GsTinNo, 43);
            }
            else
            {
                rpt.Write("TIN        : " + data.ToInstance.TinNo, 43);
            }
            rpt.Write();
            rpt.Drawline();

            rpt.NewLine();
            /*Product name length increased from 22 to 26 Adjusted in Exp, Units column by Poongodi on 25/07/2017*/
            rpt.Write("Sno", 3, Alignment.Right);
            rpt.Write("Product", 26, 4, Alignment.Center);
            rpt.Write("Batch", 7, 37);
            rpt.Write("Exp", 5, 46);
            if (data.IsTransferSettings == true)
            {
                rpt.Write("Qty", 6, 30, Alignment.Right);
                rpt.Write("Price/Unit", 11, 54, Alignment.Right);
                rpt.Write("MRP/Unit", 9, 66, Alignment.Right);
            }
            else
            {
                rpt.Write("Strips", 6, 30, Alignment.Right);
                rpt.Write("Price/Strip", 11, 54, Alignment.Right);
                rpt.Write("MRP/Strip", 9, 66, Alignment.Right);
            }
            /*GST Details Added by Poongodi on 03/07/2017*/
            if (data.TaxRefNo != null && data.TaxRefNo == 1)
            {
                rpt.Write("GST%", 5, 77, Alignment.Right);
            }
            else
            {
                rpt.Write("Vat%", 5, 77, Alignment.Right);
            }
            rpt.Write("Amount", 9, 82, Alignment.Right);
            rpt.Write();
            rpt.Drawline();
        }

        private void WriteTransferReportItems_A4(PlainTextReport rpt, StockTransfer data)
        {
            int val = 0;
            /*GST Details Added by Poongodi on 03/07/2017*/
            /*Product name length increased from 22 to 26 Adjusted in Exp, Units column by Poongodi on 25/07/2017*/
            foreach (var item in data.StockTransferItems)
            {
                val++;
                rpt.NewLine();
                rpt.Write("" + val + ". " + item.ProductStock.productName.ToUpper(), 29);
                rpt.Write(item.BatchNo, 7, 37, Alignment.Left);
                rpt.Write(string.Format("{0:MM/yy}", item.ExpireDate), 5, 46, Alignment.Right);
                if (data.IsTransferSettings == true)
                {
                    item.IsTransferSettings = true;
                    rpt.Write(string.Format("{0:0}", item.Quantity), 6, 30, Alignment.Right);
                    rpt.Write(string.Format("{0:0.00}", item.PurchasePrice), 11, 54, Alignment.Right);
                    rpt.Write(string.Format("{0:0.00}", item.SellingPrice), 9, 66, Alignment.Right);
                    if (data.TaxRefNo != null && data.TaxRefNo == 1)
                    {
                        rpt.Write(string.Format("{0:0.00}", item.GstTotal), 5, 77, Alignment.Right);
                    }
                    else
                    {
                        rpt.Write(string.Format("{0:0.00}", item.VAT), 5, 77, Alignment.Right);
                    }
                    rpt.Write(string.Format("{0:0.00}", item.Total), 9, 82, Alignment.Right);
                }
                else
                {
                    rpt.Write(string.Format("{0:0}", item.Quantity / item.PackageSize), 6, 30, Alignment.Right);
                    rpt.Write(string.Format("{0:0.00}", item.PurchasePrice * item.PackageSize), 11, 54, Alignment.Right);
                    rpt.Write(string.Format("{0:0.00}", item.SellingPrice * item.PackageSize), 9, 66, Alignment.Right);
                    if (data.TaxRefNo != null && data.TaxRefNo == 1)
                    {
                        rpt.Write(string.Format("{0:0.00}", item.GstTotal), 5, 77, Alignment.Right);
                    }
                    else
                    {
                        rpt.Write(string.Format("{0:0.00}", item.VAT), 5, 77, Alignment.Right);
                    }
                    rpt.Write(string.Format("{0:0.00}", item.Total), 9, 82, Alignment.Right);
                }

            }
            rpt.Write();
            rpt.Drawline();
        }
        private void WriteTransferReportFooter_A4(PlainTextReport rpt, StockTransfer data)
        {

            rpt.NewLine();
            rpt.Write("Total Items: " + string.Format("{0:0}", data.StockTransferItems.Count), 27);
            if (data.IsTransferSettings == true)
            {
                rpt.Write("Total Qty: " + string.Format("{0:0}", data.StockTransferItems.Sum(s => s.Quantity)), 27, 27);
            }
            else
            {
                rpt.Write("Total Strips: " + string.Format("{0:0}", data.TotalNoOfStrips), 27, 28);
            }
            rpt.Write("Net Amount: " + string.Format("{0:0.00}", data.NetAmount), 29, 62, Alignment.Right, data.IsBold);
            rpt.NewLine();
            rpt.NewLine();
            /*GST Details Added by Poongodi on 03/07/2017*/
            if (data.TaxRefNo == 1 && data.TotalIGSTAmount == 0)
            {
                rpt.Write("CGST: " + string.Format("{0:0.00}", data.TotalCGSTAmount), 27);
                if (data.Instance.isUnionTerritory != null && data.Instance.isUnionTerritory == true)
                {
                    rpt.Write("UTGST: " + string.Format("{0:0.00}", data.TotalSGSTAmount), 27, 28);
                }
                else
                {
                    rpt.Write((data.Instance.isUnionTerritory == true ? "UTGST: " : "SGST: ") + string.Format("{0:0.00}", data.TotalSGSTAmount), 27, 28);
                }
                rpt.NewLine();
                rpt.NewLine();
            }
            else if (data.TaxRefNo == 1 && data.TotalIGSTAmount > 0)
            {
                rpt.Write("IGST: " + string.Format("{0:0.00}", data.TotalIGSTAmount), 27);
                rpt.NewLine();
                rpt.NewLine();
            }
            rpt.Write("Transfer By:  " + string.Format("{0:0}", data.HQueUser.Name), 89);
            rpt.NewLine();
            rpt.NewLine();
            rpt.NewLine();
            rpt.NewLine();
            rpt.NewLine();
            rpt.Write("Prepared By:  ", 29);
            rpt.Write("Checked By:  ", 29, 33);
            rpt.Write("Approved By:  ", 27, 56, Alignment.Right);

            rpt.Write();            
        }


        //Laser Print - A5 Footer Text Method

        private void LaserA5FooterText(PlainTextReport rpt, IInvoice data)
        {
            bool gstSelection = Convert.ToBoolean(data.Instance.Gstselect);
            rpt.NewLine();

            rpt.Write("* Return of cut strip/loose pills-caps/seal opened & broken bottles not accepted.", 82, Alignment.Left, TextAdjustment.Wrap);
            rpt.NewLine();
            rpt.Write("* Medicines will be retuned only within 7days with batch No. & expiry date.", 80, Alignment.Left);
            rpt.NewLine();
            rpt.Write("* Please consult your doctor before using the medicines.", 70, Alignment.Left);
            rpt.NewLine();
            //rpt.Write("* All disputes subject to ALIGARH Jurisdiction only.", 70, Alignment.Left);
            if (data.Instance.City != null)
                data.Instance.City = data.Instance.City.ToUpper();
            else
                data.Instance.City = "";

            rpt.Write("* All disputes subject to " + data.Instance.City + " Jurisdiction only.", 80, Alignment.Left, TextAdjustment.Wrap);
            rpt.NewLine();
            rpt.NewLine();
            rpt.NewLine();
            rpt.Write("USER: " + data.HQueUser.Name.ToUpper(), 25, 1, Alignment.Left, TextAdjustment.Wrap);
            rpt.Write("PHARMACIST", 12, 37);
            rpt.Write("CASHIER", 7, 70);
            rpt.NewLine();
            rpt.Drawline(1);
            if (gstSelection)
            {
                GenerateGstSelected(rpt);
            }
            //Newly added for Print Footer by Mani on 28-02-2017
            GenerateFooter(rpt, 83, data.PrintFooterNote);
        }

        // Added by Gavaskar 29-10-2017 Start
        protected string GeneralSalesOrderEstimateReport_A4(SalesOrderEstimate data)
        {
            var rpt = new PlainTextReport
            {
                Columns = 80,
                Rows = 40
            };
            WriteSalesOrderEstimateReportHeader_A4(rpt, data);
            WriteSalesOrderEstimateReportItems_A4(rpt, data);
            WriteSalesOrderEstimateReportFooter_A4(rpt, data);
            return rpt.Content.ToString();
        }

        private void WriteSalesOrderEstimateReportHeader_A4(PlainTextReport rpt, SalesOrderEstimate data)
        {

            rpt.NewLine();
            rpt.Write(data.Instance.Name.ToUpper(), 67, 2, Alignment.Center, TextAdjustment.Wrap, true);// data.IsBold

            string adddressarea = string.Empty;

            if (!string.IsNullOrEmpty(data.Instance.Address))
            {
                adddressarea += data.Instance.Address;
            }
            if (!string.IsNullOrEmpty(data.Instance.Area))
            {
                adddressarea += "," + data.Instance.Area;
            }
            if (!string.IsNullOrEmpty(adddressarea))
            {
                rpt.NewLine();
                rpt.Write(string.Format("{0}", adddressarea.ToUpper().TrimEnd()), 69, 2, Alignment.Center, TextAdjustment.Wrap);
            }

            string cityState = string.Empty;
            if (!string.IsNullOrEmpty(data.Instance.City))
            {
                cityState += data.Instance.City + ",";
            }
            if (!string.IsNullOrEmpty(data.Instance.State))
            {
                cityState += data.Instance.State + ",";
            }
            if (!string.IsNullOrEmpty(data.Instance.Pincode))
            {
                cityState += data.Instance.Pincode;
            }

            if (!string.IsNullOrEmpty(cityState))
            {
                rpt.NewLine();
                rpt.Write(cityState.ToUpper(), 69, 2, Alignment.Center, TextAdjustment.Wrap);
            }


            string contact = string.Empty;

            if (!string.IsNullOrEmpty(data.Instance.Phone))
            {
                contact += "Ph: " + data.Instance.Phone;
            }
            if (!string.IsNullOrEmpty(data.Instance.Mobile))
            {
                contact += "," + "Mobile: " + data.Instance.Mobile;
            }
            if (!string.IsNullOrEmpty(contact))
            {
                rpt.NewLine();
                rpt.Write(contact.ToUpper(), 69, 2, Alignment.Center, TextAdjustment.Wrap);
            }

            rpt.NewLine();
            rpt.Write("DL No : " + data.Instance.DrugLicenseNo, 69, 2, Alignment.Center, TextAdjustment.Wrap);

            rpt.NewLine();
            rpt.Write("GSTIN  No : " + data.Instance.TinNo, 69, 2, Alignment.Center, TextAdjustment.Wrap);
            rpt.Write();


            rpt.NewLine();
            rpt.NewLine();
            rpt.NewLine();
            if (data.SalesOrderEstimateType == 1)
            {
                rpt.Write("Sales Order", 69, 2, Alignment.Center, TextAdjustment.Wrap);
            }
            else if (data.SalesOrderEstimateType == 2)
            {
                rpt.Write("Sales Estimate", 69, 2, Alignment.Center, TextAdjustment.Wrap);
            }
            rpt.Write();

            rpt.Drawline();

            rpt.NewLine();
            rpt.Write("Customer Name   : " + data.Patient.Name.ToUpper(), 45);
            if (data.SalesOrderEstimateType == 1)
            {
                rpt.Write("     Order No    : " + data.OrderEstimateId, 30, 50);
            }
            else if (data.SalesOrderEstimateType == 2)
            {
                rpt.Write("     Estimate No    : " + data.OrderEstimateId, 30, 50);
            }
              
            rpt.NewLine();


            if (string.IsNullOrEmpty(data.Patient.Mobile))
            {
                rpt.Write("Customer Mobile : " + "-", 45);
            }
            else
            {
                rpt.Write("Customer Mobile : " + data.Patient.Mobile.ToUpper(), 45);
            }
            if (data.SalesOrderEstimateType == 1)
            {
                rpt.Write("     Order Time  : " + Convert.ToDateTime(data.CreatedAt).ToString("hh:mm tt"), 30, 50);
            }
            else if (data.SalesOrderEstimateType == 2)
            {
                rpt.Write("     Estimate Time  : " + Convert.ToDateTime(data.CreatedAt).ToString("hh:mm tt"), 30, 50);
            }

            rpt.NewLine();
            if (string.IsNullOrEmpty(data.Patient.Email))
            {
                rpt.Write("Customer Mail   : " + "-", 45);
            }
            else
            {
                rpt.Write("Customer Mail   : " + data.Patient.Email, 45);
            }
            rpt.Write("-", 1, 55);


            rpt.NewLine();

            rpt.Write();
            rpt.Drawline();

            if (data.SalesOrderEstimateType == 1)
            {   
                rpt.NewLine();
                rpt.Write("Product", 31, Alignment.Left);
                rpt.Write("Qty", 7, 32, Alignment.Right);
                rpt.Write("Dis%", 9, 41, Alignment.Right);
                rpt.Write("Mrp", 13, 52, Alignment.Right);
                rpt.Write("Total Price", 13, 66, Alignment.Right);
                rpt.Write();
                rpt.Drawline();
            }
            else if (data.SalesOrderEstimateType == 2)
            {
                rpt.NewLine();
                rpt.Write("Product", 28, Alignment.Left);
                rpt.Write("Qty", 6, 29,Alignment.Right);
                rpt.Write("BatchNo", 8, 36, Alignment.Left);
                rpt.Write("Exp", 5, 45, Alignment.Left);
                rpt.Write("Dis%", 5, 51, Alignment.Right);
                rpt.Write("Mrp", 9, 57, Alignment.Right);
                rpt.Write("Total Price", 12, 67, Alignment.Right);
                rpt.Write();
                rpt.Drawline();
            }



        }
        private void WriteSalesOrderEstimateReportItems_A4(PlainTextReport rpt, SalesOrderEstimate data)
        {
            if (data.SalesOrderEstimateType == 1)
            {
                int val = 0;
                foreach (var item in data.SalesOrderEstimateItem)
                {
                    val++;
                    rpt.NewLine();
                    if (item.Product==null)
                    {
                        rpt.Write("" + val + ") " + item.ProductMaster.Name.ToUpper(), 31, Alignment.Left);
                    }
                    else
                    {
                        rpt.Write("" + val + ") " + item.Product.Name.ToUpper(), 31, Alignment.Left);

                    }
                   
                    rpt.Write(string.Format("{0:0}", item.Quantity), 7, 32, Alignment.Right);
                    rpt.Write(string.Format("{0:0}", item.Discount), 9, 41, Alignment.Right);
                    rpt.Write(string.Format("{0:0.00}", item.SellingPrice), 13, 52, Alignment.Right);
                    rpt.Write(string.Format("{0:0.00}", item.Quantity * item.SellingPrice), 13, 66, Alignment.Right);
                }
                rpt.Write();
                rpt.Drawline();
            }
            else if (data.SalesOrderEstimateType == 2)
            {
                int val = 0;
                foreach (var item in data.SalesOrderEstimateItem)
                {
                    val++;
                    rpt.NewLine();
                    rpt.Write("" + val + ") " + item.Product.Name.ToUpper(), 28, Alignment.Left);
                    rpt.Write(string.Format("{0:0}", item.Quantity), 6, 29, Alignment.Right);
                    rpt.Write(item.BatchNo, 8, 36, Alignment.Left);
                    rpt.Write(string.Format("{0:MM/yy}", item.ExpireDate), 5, 45, Alignment.Left);
                    rpt.Write(string.Format("{0:0}", item.Discount), 5, 51, Alignment.Right);
                    rpt.Write(string.Format("{0:0.00}", item.SellingPrice), 9, 57, Alignment.Right);
                    rpt.Write(string.Format("{0:0.00}", item.Quantity * item.SellingPrice), 12, 67, Alignment.Right);
                   
                }
                rpt.Write();
                rpt.Drawline();
            }

        }
        private void WriteSalesOrderEstimateReportFooter_A4(PlainTextReport rpt, SalesOrderEstimate data)
        {
            //if (data.SalesOrderEstimateType == 1)
            //{
            //    rpt.NewLine();
            //    rpt.Write("Total Items: " + string.Format("{0:0}", data.SalesOrderEstimateItem.Count), 45);
            //    rpt.Write();
            //}
            //else if (data.SalesOrderEstimateType == 2)
            //{
            //    rpt.NewLine();
            //    rpt.Write("Total Items: " + string.Format("{0:0}", data.SalesOrderEstimateItem.Count), 45);
            //    rpt.Write();
            //}
            rpt.NewLine();
            rpt.Write("Total Items: " + string.Format("{0:0}", data.SalesOrderEstimateItem.Count), 20,1);
            rpt.Write("Total Amount: " + string.Format("{0:0.00}", data.SalesOrderEstimateItem.Sum(x=>x.Quantity * x.SellingPrice)), 27,52, Alignment.Right);
            rpt.Write();

        }
        // Added by Gavaskar 29-10-2017 End

        private void GenerateGstSelected(PlainTextReport rpt)
        {
            rpt.NewLine();
            rpt.Write("Composite tax payer not eligible to collect taxes", 65, 1, Alignment.Left);
        }
    }
}