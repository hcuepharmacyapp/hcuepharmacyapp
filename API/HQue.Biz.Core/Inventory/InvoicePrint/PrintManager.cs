﻿using System.IO;
using System.Text;
using System.Threading.Tasks;
using HQue.Biz.Core.Inventory.Estimates;
using HQue.Contract.Infrastructure.Inventory;
using HQue.WebCore.ViewModel;
using Utilities.Helpers;
using System;
using HQue.Biz.Core.Setup;
using System.Linq;
using HQue.Biz.Core.Accounts;

namespace HQue.Biz.Core.Inventory.InvoicePrint
{
    public class PrintManager : BasePrintManager
    {
        private readonly SalesManager _salesManager;
        private readonly EstimateManager _estimateManager;
        private readonly InstanceSetupManager _instanceSetupManager;
        private readonly VendorOrderManager _vendorOrderManager;
        private readonly StockTransferManager _stockTransferManager;
        private readonly SalesSaveManager _salessaveManager;
        private string undefined;
        private readonly CustomerReceiptManager _customerReceiptManager;

        public PrintManager(SalesManager salesManager, SalesSaveManager salessaveManager, EstimateManager estimateManager, InstanceSetupManager instanceSetupManager, VendorOrderManager vendorOrderManager, StockTransferManager stockTransferManager, CustomerReceiptManager customerReceiptManager)
        {
            _instanceSetupManager = instanceSetupManager;
            _salesManager = salesManager;
            _estimateManager = estimateManager;
            _vendorOrderManager = vendorOrderManager;
            _stockTransferManager = stockTransferManager;
            _salessaveManager = salessaveManager;
            _customerReceiptManager = customerReceiptManager;

        }

        public async Task<InvoiceVM> SalesInvoice(string id, int printerType, string accountId, string instanceId, bool isOffline)
        {
            var model = await SetupInvoiceData(id, accountId, instanceId);
            return WriteFile(printerType, model, isOffline);
        }

        public async Task<InvoiceVM> ReturnInvoice(string id, int printerType, string accountId, string instanceId, bool isOffline)
        {
            var model = await SetupReturnInvoiceData(id, accountId, instanceId);
            return WriteFile(printerType, model, isOffline);
        }
        

        public async Task<InvoiceVM> Estimate(string id, int printerType)
        {
            var model = await SetupEstimateData(id);
            return WriteFile(printerType, model, false);
        }

        private InvoiceVM WriteFile(int printerType, InvoiceVM model, bool isOffline)
        {
            var fileContent = GetFileContent(printerType, model.Sales, isOffline);

            if (string.IsNullOrEmpty(fileContent))
                return model;

            //if (isOffline)
            //{
            //    if(printerType!=12)
            //     model.FilePath = System.Environment.GetEnvironmentVariable("USERPROFILE") + "\\downloads\\hq_" + model.Sales.InvoiceNo + DateTime.Now.ToString("yyyy-dd-M--HH-mm-ss") + ".txt";
            //    else
            //        model.FilePath = System.Environment.GetEnvironmentVariable("USERPROFILE") + "\\downloads\\hq_PP_" + model.Sales.InvoiceNo + DateTime.Now.ToString("yyyy-dd-M--HH-mm-ss") + ".txt";
            //}
            //else
                model.FilePath = string.Format("./wwwroot/temp/salebill_{0}.txt", model.Sales.Instance.Id);


            var info = new FileInfo(model.FilePath);
            if (info.Exists)
            {
                info.Delete();
            }

            using (var writer = info.CreateText())
            {
                writer.Write(fileContent, false, Encoding.UTF8);
            }
            //System.IO.MemoryStream ms = new System.IO.MemoryStream();
            //System.IO.StreamWriter writer = new System.IO.StreamWriter(ms);

            //writer.Write(fileContent, false, Encoding.UTF8);
            return model;
        }

        private VendorOrder WriteFile(VendorOrder model, bool isOffline, int customOrder)
        {
            var fileContent = GetFileContent(model, isOffline, customOrder);
            if (string.IsNullOrEmpty(fileContent))
                return model;
            //if (isOffline)
            //{
            //    model.FilePath = System.Environment.GetEnvironmentVariable("USERPROFILE") + "\\downloads\\hq_" + model.OrderId + DateTime.Now.ToString("yyyy-dd-M--HH-mm-ss") + ".txt";
            //}
            //else
            //{
                model.FilePath = string.Format("./wwwroot/temp/salebill_{0}.txt", model.InstanceId);
            
            var info = new FileInfo(model.FilePath);
            if (info.Exists)
            {
                info.Delete();
            }

            using (var writer = info.CreateText())
            {
                writer.Write(fileContent, false, Encoding.UTF8);
            }
            return model;
        }




        private async Task<InvoiceVM> SetupEstimateData(string id)
        {
            var model = new InvoiceVM();
            if (string.IsNullOrEmpty(id))
                return model;

            model.Sales = await _estimateManager.GetEstimateDetails(id);
            return model;
        }

        private string GetFileContent(int printerType, IInvoice model, bool isOffline)
        {
            var fileContent = string.Empty;
            model.IsBold = isOffline;
            switch (printerType)
            {
                case 1:
                    fileContent = GenerateSalesReport_4InchRoll(model);
                    break;
                case 2:
                    fileContent = GenerateSalesReport_A5_WithHeader(model);
                    break;
                case 3:
                    fileContent = GenerateSalesReport_A4(model);
                    break;
                case 4:
                    fileContent = GenerateSalesReport_A4_NoHeader(model);
                    break;
                case 6:
                    fileContent = GenerateSalesReport_A6_WithHeader(model);
                    break;
                case 7:
                    fileContent = GenerateSalesReport_A5_Vat_WithHeader(model);
                    break;
                case 8:
                    fileContent = GenerateSalesReport_4InchRollNew(model);
                    break;
                case 9:
                    fileContent = GenerateSalesReport_A5_Vat_EPSON(model);
                    break;
                case 10:
                    fileContent = GenerateSalesReport_A5_Vat_WithMFR(model);
                    break;
                case 11:
                    fileContent = GenerateSalesReport_A5_Vat_WithMFR6Inch(model);
                    break;
                case 12:
                    fileContent = GenerateSalesReport_A5QMS_WithHeader(model);
                    break;
                case 13:
                    fileContent = GenerateSalesReport_4InchRollNew_NonPharma(model);
                    break;
                case 14:
                    fileContent = GenerateSalesReport_A5_Vat_WithHeader_RackNo(model);
                    break;
                case 15:
                    fileContent = GenerateSalesReport_A5_Without_Vat_RackNo(model);
                    break;
                case 16:
                    fileContent = GenerateSalesReport_A5QMS_WithHeaderNoGST(model);
                    break;
                case 17:
                    fileContent = GenerateSalesReport_A4_New(model);
                    break;
            }
            return fileContent;
        }
        private string GetFileContent(VendorOrder model, bool isOffline, int customOrder)
        {
            var fileContent = string.Empty;
            model.IsBold = isOffline;
            fileContent = GeneraCustomteOrderReport_A4(model, customOrder);
            return fileContent;
        }
        public async Task<InvoiceVM> SetupInvoiceData(string id, string accountId, string instanceId)
        {
            var model = new InvoiceVM();
            if (string.IsNullOrEmpty(id))
                return model;

            decimal? CreditBalance = 0;
            decimal? CreditBalanceAmount = 0;
            var sale = await _salesManager.GetSalesDetails(id);
            sale.InvoiceSeries = Convert.ToString(sale.Prefix) + Convert.ToString(sale.InvoiceSeries);
            //added by nandhini on 24.11.17
            if (sale.PatientId!=null || sale.PatientId != undefined)
            {
                var outBalance= await _customerReceiptManager.GetCustomerBalance(accountId, instanceId, sale.Mobile, sale.Name);                
                    CreditBalance = outBalance.Credit;
                CreditBalanceAmount = outBalance.Credit - outBalance.Debit; 
            }

            //Added by Sarubala to print eWallet on 26-10-17
            if (sale.SalesPayments != null &&  sale.SalesPayments.Count == 1 && sale.SalesPayments.First().PaymentInd == 5 && sale.PaymentType == "Multiple")
            {
                sale.PaymentType = "eWallet";
            }

            //if (sale.Discount > 0)
            //{
            //    foreach (var item in sale.SalesItem)
            //    {

            //        item.Discount = sale.Discount;
            //    }
            //}

            model.Sales = sale;
            var pt = await _salesManager.GetExistingPatientData(sale.PatientId,sale.Mobile, sale.Name, accountId, instanceId);

            if (pt != null)
            {
                model.Sales.Patient = pt;
            }

            if (model.Sales != null)
            {
                model.Sales.InWords = ((decimal)model.Sales.NetAmount).ConvertNumbertoWords();
                //model.Sales.InWords = ((long)(model.Sales.Net+model.Sales.RoundoffNetAmount)).ConvertNumbertoWords();
            }
            model.Sales.Patient.BalanceAmount = CreditBalance;
            model.Sales.Patient.CreditBalanceAmount = CreditBalanceAmount;

            return model;
        }


        private async Task<InvoiceVM> SetupReturnInvoiceData(string id, string accountId, string instanceId)
        {
            var model = new InvoiceVM();
            if (string.IsNullOrEmpty(id))
                return model;

            var sale = await _salesManager.GetReturnOnlyDetails(id);
            model.Sales = sale;
            model.Sales.IsReturnsOnly = true;
            if (model.Sales != null)
            {
                model.Sales.InWords = ((decimal)model.Sales.NetAmount).ConvertNumbertoWords();
                //model.Sales.InWords = ((long)(model.Sales.Net+model.Sales.RoundoffNetAmount)).ConvertNumbertoWords();
            }

            return model;
        }


        public async  Task<VendorOrder> orderPrint(string vendororderid, string accountId, string instanceId, bool isOffline, int customOrder)
        {
            VendorOrder data = new VendorOrder();
            data = await _vendorOrderManager.GetVenodrOrderById(vendororderid, accountId, instanceId);
            data.OrderId = Convert.ToString(data.Prefix) + data.OrderId;//Prefix added by Poogodi on 21/06/2017
            data.Instance = await _instanceSetupManager.GetById(instanceId);
            return WriteFile(data, isOffline, customOrder);
        }
        
        public async Task<StockTransfer> TransferPrint(string id, string accountId, string instanceId,bool transferSettings, bool isOffline)
        {
            StockTransfer data = new StockTransfer();
            data = await _stockTransferManager.TransferById(id, instanceId);
            //data.Instance = await _instanceSetupManager.GetById(instanceId);
            return TransferWriteFile(data, transferSettings, isOffline);
        }

        private StockTransfer TransferWriteFile(StockTransfer model,bool transferSettings, bool isOffline)
        {
            var fileContent = GetTransferFileContent(model, transferSettings, isOffline);
            if (string.IsNullOrEmpty(fileContent))
                return model;
            if (isOffline)
            {
                model.FilePath = System.Environment.GetEnvironmentVariable("USERPROFILE") + "\\downloads\\hq_SH_" + model.Id + DateTime.Now.ToString("yyyy-dd-M--HH-mm-ss") + ".txt";
            }
            else
            {
                model.FilePath = string.Format("./wwwroot/temp/salebill_{0}.txt", model.InstanceId);
            }

            var info = new FileInfo(model.FilePath);
            if (info.Exists)
            {
                info.Delete();
            }

            using (var writer = info.CreateText())
            {
                writer.Write(fileContent, false, Encoding.UTF8);
            }
            return model;
        }

        private string GetTransferFileContent(StockTransfer model, bool transferSettings, bool isOffline)
        {
            var fileContent = string.Empty;
            model.IsBold = isOffline;
            //model.IsTransferSettings = transferSettings;
            fileContent = GenerateTransferPrint(model);
            return fileContent;
        }

        // Added by Gavaskar 29-10-2017 Start
        public async Task<SalesOrderEstimate> salesOrderEstimatePrint(string salesOrderEstimateId, string accountId, string instanceId, bool isOffline)
        {
            SalesOrderEstimate data = new SalesOrderEstimate();
            data = await _salessaveManager.GetSalesOrderById(salesOrderEstimateId, accountId, instanceId);
            data.OrderEstimateId = Convert.ToString(data.Prefix) + data.OrderEstimateId;
            data.Instance = await _instanceSetupManager.GetById(instanceId);
            return salesOrderEstimateWriteFile(data, isOffline);
        }

        private SalesOrderEstimate salesOrderEstimateWriteFile(SalesOrderEstimate model, bool isOffline)
        {
            var fileContent = GetSalesOrderEstimateFileContent(model, isOffline);
            if (string.IsNullOrEmpty(fileContent))
                return model;
            model.FilePath = string.Format("./wwwroot/temp/salebill_{0}.txt", model.InstanceId);

            var info = new FileInfo(model.FilePath);
            if (info.Exists)
            {
                info.Delete();
            }

            using (var writer = info.CreateText())
            {
                writer.Write(fileContent, false, Encoding.UTF8);
            }
            return model;
        }
        private string GetSalesOrderEstimateFileContent(SalesOrderEstimate model, bool isOffline)
        {
            var fileContent = string.Empty;
           // model.IsBold = isOffline;
            fileContent = GeneralSalesOrderEstimateReport_A4(model);
            return fileContent;
        }
        // Added by Gavaskar 29-10-2017 End
    }
}
