﻿using HQue.Contract.Infrastructure;
using HQue.Contract.Infrastructure.Inventory;
using HQue.DataAccess.Inventory;
using HQue.DataAccess.Master;
using System.Collections.Generic;
using System.Threading.Tasks;
using HQue.Biz.Core.Accounts;
using Utilities.Helpers;
using HQue.Contract.Infrastructure.Master;
using System.Linq;
using System;

namespace HQue.Biz.Core.Inventory
{
    public class VendorPurchaseReturnManager : BizManager
    {
        private readonly VendorPurchaseReturnDataAccess _vendorPurchaseReturnDataAccess;
        private readonly VendorPurchaseDataAccess _vendorPurchaseDataAccess;
        private readonly ProductDataAccess _productDataAccess;
        private readonly PaymentManager _paymentManager;

        public VendorPurchaseReturnManager(VendorPurchaseReturnDataAccess vendorPurchaseReturnDataAccess,
            VendorPurchaseDataAccess vendorPurchaseDataAccess, ProductDataAccess productDataAccess,PaymentManager paymentManager)
            : base(vendorPurchaseReturnDataAccess)
        {
            _vendorPurchaseReturnDataAccess = vendorPurchaseReturnDataAccess;
            _vendorPurchaseDataAccess = vendorPurchaseDataAccess;
            _productDataAccess = productDataAccess;
            _paymentManager = paymentManager;
        }

        public async Task<VendorPurchaseReturn> GetVendorPurchaseReturnDetails(string id)
        {
            var vendorPurchase = await _vendorPurchaseDataAccess.VendorPurchaseDetail(id);

            int TaxRefNo = 0;
            if (vendorPurchase.TaxRefNo == null || vendorPurchase.TaxRefNo == 0)
            {
                TaxRefNo = 0;
            }
            else
            {
                TaxRefNo = Convert.ToInt32(vendorPurchase.TaxRefNo);
            }

            var vendorPurchaseReturn = new VendorPurchaseReturn
            {
                VendorId = vendorPurchase.Vendor.Id,
                VendorName = vendorPurchase.Vendor.Name,
                InvoiceNo = vendorPurchase.InvoiceNo,
                InvoiceDate = vendorPurchase.InvoiceDate,
                VendorPurchaseId = vendorPurchase.Id,
                Discount = vendorPurchase.Discount,
                GoodsRcvNo = vendorPurchase.GoodsRcvNo,
                TaxRefNo = TaxRefNo,
                BillSeries = Convert.ToString(vendorPurchase.Prefix)+ vendorPurchase.BillSeries, //Prefix Addedby Poongodi on 15/06/2017
                CreatedAt = vendorPurchase.CreatedAt
            };


            vendorPurchase.VendorPurchaseItem = await _vendorPurchaseDataAccess.GetVendorPurchaseItem(vendorPurchase.Id);

            vendorPurchase.VendorReturn.VendorPurchaseReturnItem = await _vendorPurchaseDataAccess.VendorPurchaseReturnItemList(vendorPurchase);
            foreach (var item in vendorPurchase.VendorPurchaseItem)
            {
                var vendorPurchaseReturnItem = new VendorPurchaseReturnItem
                {
                    ProductStock = item.ProductStock,
                    ProductStockId = item.ProductStock.Id,
                };
                var product = await _productDataAccess.GetProductById(item.ProductStock.ProductId);
                vendorPurchaseReturnItem.ProductStock.Product.Name = product.Name;
                vendorPurchaseReturnItem.Total = item.Total;
                vendorPurchaseReturnItem.PurchaseQuantity = item.Quantity;
                vendorPurchaseReturnItem.PackageQuantity = item.PackageQty;
                vendorPurchaseReturnItem.PackageSize = item.PackageSize;
                vendorPurchaseReturnItem.FreeQty= item.FreeQty;
                vendorPurchaseReturnItem.ReturnPurchasePrice = item.PurchasePrice;
                vendorPurchaseReturnItem.VendorPurchaseItem.PackagePurchasePrice = item.PackagePurchasePrice;
                vendorPurchaseReturnItem.Discount = item.Discount;
                vendorPurchaseReturnItem.DiscountValue = item.DiscountValue / item.Quantity;
                vendorPurchaseReturnItem.Igst = item.Igst;
                vendorPurchaseReturnItem.Cgst = item.Cgst;
                vendorPurchaseReturnItem.Sgst = item.Sgst;
                vendorPurchaseReturnItem.GstTotal = item.GstTotal;


                foreach (var returnItem in vendorPurchase.VendorReturn.VendorPurchaseReturnItem)
                {
                    if (vendorPurchaseReturnItem.ReturnedQuantity == null)
                        vendorPurchaseReturnItem.ReturnedQuantity = 0;

                    if (vendorPurchaseReturnItem.ProductStock.Id == returnItem.ProductStockId)
                        vendorPurchaseReturnItem.ReturnedQuantity += returnItem.Quantity;
                }
                vendorPurchaseReturn.VendorPurchaseReturnItem.Add(vendorPurchaseReturnItem);
            }

            vendorPurchaseReturn.SubTotal = vendorPurchase.GrossTotal;

            return vendorPurchaseReturn;
        }
        // Newly Added Gavaskar 26-10-2016 Start 

        public async Task<VendorPurchaseReturn> GetExpiremovingStocks(string instanceId)
        {

            var vendorStock = await _vendorPurchaseDataAccess.GetExpiremovingStocks(instanceId);

            var VendorPurchaseReturn = new VendorPurchaseReturn
            {

            };

            var getVendorPurchaseId = vendorStock.Select(i => i.VendorPurchaseId).Distinct().ToList();

            VendorPurchaseReturn.VendorPurchaseReturnItem = await _vendorPurchaseDataAccess.VendorPurchaseReturnItemList1(getVendorPurchaseId);
            foreach (var item in vendorStock)
            {
                var vendor = new Vendor
                {

                };
                vendor.Id = item.ProductStock.Vendor.Id;
                vendor.Name = item.ProductStock.Vendor.Name;

                var vendorPurchaseReturnItem = new VendorPurchaseReturnItem
                {
                    ProductStock = item.ProductStock,
                    ProductStockId = item.ProductStock.Id,
                };
                var product = await _productDataAccess.GetProductById(item.ProductStock.ProductId);
                vendorPurchaseReturnItem.ProductStock.Product.Name = product.Name;
                vendorPurchaseReturnItem.Total = item.Total;
                vendorPurchaseReturnItem.PurchaseQuantity = item.Quantity;
                vendorPurchaseReturnItem.FreeQty = item.FreeQty;
                vendorPurchaseReturnItem.ReturnPurchasePrice = item.PurchasePrice;
                vendorPurchaseReturnItem.VendorPurchaseItem.PackagePurchasePrice = item.PackagePurchasePrice;
                vendorPurchaseReturnItem.Quantity = item.ProductStock.Stock;
                vendorPurchaseReturnItem.VendorPurchaseId = item.VendorPurchaseId;

                foreach (var returnItem in VendorPurchaseReturn.VendorPurchaseReturnItem)
                {
                    if (vendorPurchaseReturnItem.ReturnedQuantity == null)
                        vendorPurchaseReturnItem.ReturnedQuantity = 0;

                    if (vendorPurchaseReturnItem.ProductStock.Id == returnItem.ProductStockId)
                        vendorPurchaseReturnItem.ReturnedQuantity += returnItem.Quantity;
                }


                vendor.VendorPurchaseReturnItem.Add(vendorPurchaseReturnItem);
                VendorPurchaseReturn.Vendor.Add(vendor);
            }

            return VendorPurchaseReturn;
        }

        // Newly Added Gavaskar 26-10-2016 End 

        public async Task<VendorPurchaseReturn> GetNonmovingStocks(string instanceId)
        {

            var vendorStock = await _vendorPurchaseDataAccess.GetNonmovingStocks(instanceId);
            
            var VendorPurchaseReturn = new VendorPurchaseReturn
            {
               
            };


            //foreach(var item in vendorStock)
            //{

            //}


            //foreach (var items in vendorStock)
            //{           
            //  var vendorPurchase = await _vendorPurchaseDataAccess.VendorPurchaseDetail(vendorPurchaseReturn.VendorPurchaseId);

            var getVendorPurchaseId = vendorStock.Select(i => i.VendorPurchaseId).Distinct().ToList();

            VendorPurchaseReturn.VendorPurchaseReturnItem = await _vendorPurchaseDataAccess.VendorPurchaseReturnItemList1(getVendorPurchaseId);
            foreach (var item in vendorStock)
            {
                var vendor = new Vendor
                {

                };
                vendor.Id = item.ProductStock.Vendor.Id;
                vendor.Name = item.ProductStock.Vendor.Name;

                var vendorPurchaseReturnItem = new VendorPurchaseReturnItem
                {
                    ProductStock = item.ProductStock,
                    ProductStockId = item.ProductStock.Id,
                };
                var product = await _productDataAccess.GetProductById(item.ProductStock.ProductId);
                vendorPurchaseReturnItem.ProductStock.Product.Name = product.Name;
                vendorPurchaseReturnItem.Total = item.Total;
                vendorPurchaseReturnItem.PurchaseQuantity = item.Quantity;
                vendorPurchaseReturnItem.FreeQty = item.FreeQty;
                vendorPurchaseReturnItem.ReturnPurchasePrice = item.PurchasePrice;
                vendorPurchaseReturnItem.VendorPurchaseItem.PackagePurchasePrice = item.PackagePurchasePrice;
                vendorPurchaseReturnItem.Quantity = item.ProductStock.Stock;
                vendorPurchaseReturnItem.VendorPurchaseId = item.VendorPurchaseId;

                foreach (var returnItem in VendorPurchaseReturn.VendorPurchaseReturnItem)
                {
                    if (vendorPurchaseReturnItem.ReturnedQuantity == null)
                        vendorPurchaseReturnItem.ReturnedQuantity = 0;

                    if (vendorPurchaseReturnItem.ProductStock.Id == returnItem.ProductStockId)
                        vendorPurchaseReturnItem.ReturnedQuantity += returnItem.Quantity;
                }


                vendor.VendorPurchaseReturnItem.Add(vendorPurchaseReturnItem);
                //VendorPurchaseReturn.VendorPurchaseReturnItem.Add(vendorPurchaseReturnItem);
                VendorPurchaseReturn.Vendor.Add(vendor);
            }

            //var groupedVendorList = VendorPurchaseReturn.Vendor
            //  .GroupBy(u => u.Id)
            //  .Select(l=>l.ToList());
            
           // VendorPurchaseReturn.Vendor =new List<Vendor>groupedVendorList.ToList();
            // var vendorStock1 = await _vendorPurchaseDataAccess.GetNonmovingStocks1(VendorPurchaseReturn);

            //ar allProducts = new[]  VendorPurchaseReturn.Vendor.ToArray()
            //var groupedCustomerList = VendorPurchaseReturn.Vendor.group(u => u.GroupID).Select(grp => grp.ToList()).ToList();

            return VendorPurchaseReturn;
        }

        public async Task<VendorPurchaseReturn> Save(VendorPurchaseReturn vendorPurchaseReturn)
        {
            vendorPurchaseReturn.ReturnDate = CustomDateTime.IST;
            vendorPurchaseReturn.ReturnNo = await GetReturnNo(vendorPurchaseReturn);
            vendorPurchaseReturn.FinyearId = await GetFinYear(vendorPurchaseReturn);
            for (int i = 0; i < vendorPurchaseReturn.VendorPurchaseReturnItem.Count; i++)
            {
                if (vendorPurchaseReturn.TotalGstValue==null)
                {
                    vendorPurchaseReturn.TotalGstValue = 0;
                }
                vendorPurchaseReturn.TotalGstValue = vendorPurchaseReturn.TotalGstValue + vendorPurchaseReturn.VendorPurchaseReturnItem[i].GSTAmount1;
                vendorPurchaseReturn.VendorPurchaseReturnItem[i].GstAmount = vendorPurchaseReturn.VendorPurchaseReturnItem[i].GSTAmount1;
                vendorPurchaseReturn.VendorPurchaseReturnItem[i].ReturnedTotal = vendorPurchaseReturn.VendorPurchaseReturnItem[i].ReturnTotal;
                //vendorPurchaseReturn.TotalDiscountValue = vendorPurchaseReturn.TotalDiscountValue + vendorPurchaseReturn.VendorPurchaseReturnItem[i].Discount;
            }// Vendor Return Item return amount

            var savedReturn = await _vendorPurchaseReturnDataAccess.Save(vendorPurchaseReturn);

            //for (int i = 0; i < vendorPurchaseReturn.VendorPurchaseReturnItem.Count; i++)
            //{
            //    savedReturn.VendorPurchaseReturnItem[i].ReturnedTotal = vendorPurchaseReturn.VendorPurchaseReturnItem[i].ReturnTotal;
            //}// Vendor Return Item return amount
            await SaveVendorPurchaseReturnItem(savedReturn);
            await _paymentManager.SaveVendorPurchaseReturnPayment(vendorPurchaseReturn); //Commented for Payment done by Accounts - Lawrence
            return savedReturn;
        }
        //Added by Lawrence
        public async Task<VendorPurchaseReturn> saveReturn(VendorPurchaseReturn vendorPurchaseReturn) 
        {
            vendorPurchaseReturn.ReturnDate = CustomDateTime.IST;
            vendorPurchaseReturn.ReturnNo = await GetReturnNo(vendorPurchaseReturn);
            vendorPurchaseReturn.FinyearId = await GetFinYear(vendorPurchaseReturn);
            var savedReturn = await _vendorPurchaseReturnDataAccess.saveReturn(vendorPurchaseReturn);
            await SaveVendorPurchaseReturnItems(savedReturn); 
            //await _paymentManager.SaveVendorPurchaseReturnPayment(vendorPurchaseReturn);
            return savedReturn;
        }

        public async Task<VendorPurchaseReturn> SaveBulkReturn(VendorPurchaseReturn vendorPurchaseReturn)
        {
            
            foreach (var vendorPurchaseReturnItem in vendorPurchaseReturn.VendorPurchaseReturnItem)
            {
               
                if (vendorPurchaseReturnItem.Quantity != null && vendorPurchaseReturnItem.Quantity > 0)
                {
                    int index = vendorPurchaseReturn.VendorPurchaseReturnItem.IndexOf(vendorPurchaseReturnItem);
                    vendorPurchaseReturn.ReturnDate = CustomDateTime.IST;
                    vendorPurchaseReturn.ReturnNo = await GetReturnNo(vendorPurchaseReturn);
                    vendorPurchaseReturn.FinyearId = await GetFinYear(vendorPurchaseReturn);
                    vendorPurchaseReturn.VendorPurchaseId = vendorPurchaseReturnItem.VendorPurchaseId;
                    vendorPurchaseReturn.TotalReturnedAmount = vendorPurchaseReturnItem.ReturnTotal; // Vendor Return total return amount

                    if (vendorPurchaseReturn.VendorId == null)
                    {
                        vendorPurchaseReturn.VendorId = vendorPurchaseReturn.Id; // Vendor Return VendorId 
                    }
                    vendorPurchaseReturnItem.ReturnedTotal = vendorPurchaseReturnItem.ReturnTotal; // Vendor Return Item return amount

                    //vendorPurchaseReturn.VendorPurchaseReturnItem.Add(vendorPurchaseReturn.VendorPurchaseReturnItem[index]);
                    //vendorPurchaseReturn.VendorPurchaseReturnItem=vendorPurchaseReturn.VendorPurchaseReturnItem.ElementAt(index);
                    var savedReturn = await _vendorPurchaseReturnDataAccess.Save(vendorPurchaseReturn);
                    
                    await SaveVendorPurchaseBulkReturnItem(savedReturn, index);
                    await _paymentManager.SaveVendorPurchaseBulkReturnPayment(vendorPurchaseReturnItem);
                }
            }
            return vendorPurchaseReturn;
        }

        private async Task SaveVendorPurchaseReturnItem(VendorPurchaseReturn data)
        {
            await _vendorPurchaseReturnDataAccess.Save(GetEnumerable(data));
        }

        //Added by Lawrence
        private async Task SaveVendorPurchaseReturnItems(VendorPurchaseReturn data) 
        {
            await _vendorPurchaseReturnDataAccess.SaveItems(GetEnumerables(data));
        }

        private IEnumerable<VendorPurchaseReturnItem> GetEnumerable(VendorPurchaseReturn vendorPurchaseReturn)
        {
            foreach (var item in vendorPurchaseReturn.VendorPurchaseReturnItem)
            {
                SetMetaData(vendorPurchaseReturn, item);
                item.VendorReturnId = vendorPurchaseReturn.Id;
                yield return item;
            }
        }

        private IEnumerable<VendorPurchaseReturnItem> GetEnumerables(VendorPurchaseReturn vendorPurchaseReturn) 
        {
            foreach (var item in vendorPurchaseReturn.VendorPurchaseReturnItem)
            {
                SetMetaData(vendorPurchaseReturn, item);
                item.VendorReturnId = vendorPurchaseReturn.Id;
                item.ProductStockId = item.ProductStock.Id;
                yield return item;
            }
        }

        private async Task SaveVendorPurchaseBulkReturnItem(VendorPurchaseReturn data, int index)
        {
            await _vendorPurchaseReturnDataAccess.Save(GetBulkEnumerable(data, index));
        }

        private IEnumerable<VendorPurchaseReturnItem> GetBulkEnumerable(VendorPurchaseReturn vendorPurchaseReturn,int index)
        {
            
                SetMetaData(vendorPurchaseReturn, vendorPurchaseReturn.VendorPurchaseReturnItem[index]);
            vendorPurchaseReturn.VendorPurchaseReturnItem[index].VendorReturnId = vendorPurchaseReturn.Id;
                yield return vendorPurchaseReturn.VendorPurchaseReturnItem[index];
            
        }

        public async Task<string> GetReturnNo(VendorPurchaseReturn  data)
        {
            return await _vendorPurchaseReturnDataAccess.GetReturnNo(data);
        }
        public async Task<string> GetFinYear(VendorPurchaseReturn data)
        {
            return await _vendorPurchaseReturnDataAccess.GetFinYear(data);
        }
        public async Task<PagerContract<VendorPurchaseReturn>> ListPager(VendorPurchaseReturn data, bool IsInvoiceDate, bool isFromReport)
        {
            var pager = new PagerContract<VendorPurchaseReturn>
            {
                NoOfRows = await _vendorPurchaseReturnDataAccess.Count(data, IsInvoiceDate),
                List = await _vendorPurchaseReturnDataAccess.List(data, IsInvoiceDate, isFromReport)
            };

            return pager;
        }
        // Added by Violet to new return on 19-05-2017

        //public Task<List<ProductStock>> GetPurchaseReturnItem(string productName,string instanceId)  
        public Task<List<VendorPurchaseItem>> GetPurchaseReturnItem(string productName,string instanceId)  
        {
            return _vendorPurchaseReturnDataAccess.GetVendorPurchaseReturnItem(productName, instanceId); //, VendorId
        }
        // Added by Settu to implement payment status & remarks
        public async Task<bool> updateVendorPurchaseReturn(VendorPurchaseReturn vendorPurchaseReturn)
        {
            return await _vendorPurchaseReturnDataAccess.updateVendorPurchaseReturn(vendorPurchaseReturn);
        }
        // Added by Gavaskar Purchase With Return
        public async Task<VendorPurchaseReturn> savePurchaseReturn(VendorPurchaseReturn vendorPurchaseReturn)
        {
            if (vendorPurchaseReturn.Id!=null)
            {
                var savedReturn = await _vendorPurchaseReturnDataAccess.updateVendorPurchaseReturnIsAlongWithPurchase(vendorPurchaseReturn);
                await SaveVendorPurchaseReturnItems_List(savedReturn);
                return savedReturn;
            }
            else
            {
                vendorPurchaseReturn.ReturnDate = CustomDateTime.IST;
                vendorPurchaseReturn.ReturnNo = await GetReturnNo(vendorPurchaseReturn);
                vendorPurchaseReturn.FinyearId = await GetFinYear(vendorPurchaseReturn);
                var savedReturn = await _vendorPurchaseReturnDataAccess.saveReturn(vendorPurchaseReturn);
                await SaveVendorPurchaseReturnItems_List(savedReturn);
                return savedReturn;
            }
        }
        // Added by Gavaskar Purchase With Return
        private async Task SaveVendorPurchaseReturnItems_List(VendorPurchaseReturn data)
        {
            await _vendorPurchaseReturnDataAccess.SaveIsAlongWithPurchaseList(GetEnumerables_List(data));
        }
        // Added by Gavaskar Purchase With Return
        private IEnumerable<VendorPurchaseReturnItem> GetEnumerables_List(VendorPurchaseReturn vendorPurchaseReturn)
        {
            foreach (var item in vendorPurchaseReturn.VendorPurchaseReturnItem)
            {
                SetMetaData(vendorPurchaseReturn, item);
                item.VendorReturnId = vendorPurchaseReturn.Id;
                item.ProductStockId = item.ProductStockId;
                yield return item;
            }
        }

    }
}
