﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HQue.Biz.Core.Setup;
using HQue.Contract.Infrastructure;
using HQue.Contract.Infrastructure.Settings;
using HQue.Contract.Infrastructure.Inventory;
using HQue.Contract.Infrastructure.Setup;
using HQue.DataAccess.Inventory;
using Microsoft.AspNetCore.Http;
using Utilities.Helpers;
using HQue.Contract.Infrastructure.Misc;
using HQue.DataAccess.DbModel;
using System.Net.NetworkInformation;
using HQue.Communication.Email;
using HQue.ServiceConnector.Connector;
using HQue.Contract.Infrastructure.Master;

namespace HQue.Biz.Core.Inventory
{
    public class StockTransferManager : BizManager
    {
        private readonly VendorOrderDataAccess _vendorOrderDataAccess;
        private readonly InstanceSetupManager _instanceSetupManager;
        private readonly ProductStockManager _productStockManager;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private new StockTransferDataAccess DataAccess => base.DataAccess as StockTransferDataAccess;
        private readonly ProductStockDataAccess _productStockDataAccess;
        private readonly ConfigHelper _configHelper;
        private readonly EmailSender _emailSender;
        private StockTransferDataAccess _dataAccess;

        public StockTransferManager(StockTransferDataAccess dataAccess, InstanceSetupManager instanceSetupManager, ProductStockManager productStockManager, ProductStockDataAccess productStockDataAccess, ConfigHelper configHelper, IHttpContextAccessor httpContextAccessor, VendorOrderDataAccess vendorOrderDataAccess, EmailSender emailSender) : base(dataAccess)
        {
            _productStockDataAccess = productStockDataAccess;
            _vendorOrderDataAccess = vendorOrderDataAccess;
            _instanceSetupManager = instanceSetupManager;
            _productStockManager = productStockManager;
            _httpContextAccessor = httpContextAccessor;
            _configHelper = configHelper;
            _dataAccess = dataAccess;
            _configHelper = configHelper;
            _emailSender = emailSender;
        }

        public async Task<StockTransfer> Save(StockTransfer data)
        {          
            data.TransferStatus = StockTransfer.TransferStatusPending;
            data.TransferDate = CustomDateTime.IST;
            data.TransferNo = await GetTransferNo(data.InstanceId,data.Prefix);             /*Prefix added by POongodi on 11/10/2017*/
            var getfinyear = await getFinYear(data); /*Added by Poongodi on 26/03/2017*/
            data.FinyearId = getfinyear;
           Instance branch = await _instanceSetupManager.GetById(data.ToInstanceId);
            bool bToInstanceOffline = false;
           if (branch !=null )
                bToInstanceOffline = branch.IsOfflineInstalled as bool? ?? false;
            var saved = data;
            if (data.WriteExecutionQuery)
            {
                data.IsToInstanceOfflineInstalled = bToInstanceOffline;
                DataAccess.SetInsertMetaData(data, StockTransferTable.Table);
                DataAccess.WriteInsertExecutionQuery(data, StockTransferTable.Table);
            }
            else
            {
                saved = await base.Save(data);

                //Manual Sync insert process for Online to offline transfer by Poongodi on 08/11/2017
                if (_configHelper.AppConfig.OfflineMode == false && bToInstanceOffline)
                {
                    DataAccess.WriteSyncQueueInsert(data, StockTransferTable.Table, "I", data.ToInstanceId, true);
                }
            }
         
            await SaveStockTransferItem(data, bToInstanceOffline);
            await updateVendorOrderItem(data);
            if (!data.WriteExecutionQuery)
            {
                await UpdateProductStock(data.StockTransferItems);
            }
            return saved;
          }
        public async Task<StockTransfer> SaveBulkData(StockTransfer data)
        {
            await _dataAccess.SaveBulkData(data);
            if (data.SaveFailed && data.ErrorLog.ErrorMessage.ToLower() != "stock validation failed" && NetworkInterface.GetIsNetworkAvailable())
            {
                var txtEmail = $@"<p>Dear Team,<br/><br/>Customer is facing below issue, please look into this.<br/>
                                <br/><B>Issue details:</B><br/>
                                <br/><B>Mode:</B> { (_configHelper.AppConfig.OfflineMode ? "Offline" : "Online") }
                                <br/><B>Branch Id:</B> { data.InstanceId }
                                <br/><B>Branch Name:</B> { data.Instance.Name }                                
                                <br/><B>Transaction Type:</B> Stock Transfer
                                <br/><br/><B>Error Message:</B> { data.ErrorLog.ErrorMessage }
                                <br/><B>Error Stack Trace:</B> { data.ErrorLog.ErrorStackTrace }
                                <br/><br/><br/>Thank You!</p><p>Powered By hCue</p>";

                _emailSender.Send("pharmacytech@myhcue.com", 19, txtEmail);
            }
            return data;
        }

        public async Task<string> getFinYear(StockTransfer data)
        {
            return await DataAccess.GetFinYear(data);
        }
        public async Task<StockTransfer> updateVendorOrderItem(StockTransfer vpData)
        {
            return await _vendorOrderDataAccess.updateVendorOrderItem(vpData);
        }

        public async Task<PagerContract<StockTransfer>> ListPager(StockTransfer data,bool forAccept)
        {
            var user = _httpContextAccessor.HttpContext.User;
            if (forAccept)
            {
                data.TransferStatus = StockTransfer.TransferStatusPending;
            }

            var pager = new PagerContract<StockTransfer>
            {
                NoOfRows = await DataAccess.Count(data, user.InstanceId(),forAccept),
                List = await DataAccess.List(data, user.InstanceId(), forAccept)
            };

            return pager;
        }

        private async Task UpdateProductStock(IEnumerable<StockTransferItem> stockTransferItems)
        {
          
            foreach (var stockTransferItem in stockTransferItems)
            {
                stockTransferItem.ProductStock.SetExecutionQuery(stockTransferItem.GetExecutionQuery(), stockTransferItem.WriteExecutionQuery);
                if (!stockTransferItem.StockTransfer.IsNew || stockTransferItem.ProductStockId != null)
                {
                    await _productStockManager.UpdateProductStock(stockTransferItem.ProductStockId, (-1) * stockTransferItem.Quantity);
                }
            }
        }

        private Task SaveStockTransferItem(StockTransfer data,bool bOfflineSyncQueue)
        { 
            //Parameter added BY poongodi on 08/11/2017
            return DataAccess.Save(GetEnumerable(data),data.ToInstanceId, bOfflineSyncQueue);
        }

        private IEnumerable<StockTransferItem> GetEnumerable(StockTransfer data)
        {
            foreach (var item in data.StockTransferItems)
            {
                SetMetaData(data, item);
                item.TransferId = data.Id;
                item.TaxRefNo = data.TaxRefNo;
                item.FromTinNo = data.FromTinNo as System.String ?? "";
                item.ToTinNo = data.ToTinNo as System.String ?? "";
                yield return item;
            }
        }

        public async Task<string> GetTransferNo(string instanceId,string sPrefix)
        {
            /*Prefix added by POongodi on 11/10/2017*/
            return await DataAccess.GetTransferNo(instanceId, sPrefix);
        }
        //finyear method added by Poongodi on 26/03/2017
        public async Task<string> GetFinYear(StockTransfer data)
        {
            return await DataAccess.GetFinYear(data);
        }

        public async Task<Instance> GetTransferInstance(string instanceId)
        {
            //if (string.IsNullOrEmpty(instanceId))
            //    return await AccountInstance();
            //var instance = await _instanceSetupManager.GetById(instanceId);
            //if (string.IsNullOrEmpty(instance.TinNo))
            //    return Enumerable.Empty<Instance>();
            //var instanceFilter = new Instance { TinNo = instance.TinNo };
            //return (await _instanceSetupManager.GetByTinNo(instanceFilter)).Where(i => i.Id != instanceId);
            return (await _instanceSetupManager.GetById(instanceId));
        }


        public async Task<List<Instance>> GetInstancesByAccountId(string accountid,string instanceid)
        {
            var list = await _instanceSetupManager.GetInstancesByAccountId(accountid);
            var result = list.Select(x =>
            {
                x.GsTinNo = string.IsNullOrEmpty(x.GsTinNo) ? "" : x.GsTinNo;
                return x;
            }).Where( y => y.Id != instanceid).ToList();
            return result;
        }

        private async Task<IEnumerable<Instance>> AccountInstance()
        {
            var user = _httpContextAccessor.HttpContext.User;
            var instances = await _instanceSetupManager.List(user.AccountId());

            return user.HasMultiInstanceAccess() ? instances : instances.Where(x => x.Id == user.InstanceId());
        }

        public async Task<bool> AcceptReject(string id, int status, List<StockTransferItem> stocktransferItem)
        {
            var user = _httpContextAccessor.HttpContext.User;
            bool bWriteSyncQue = false;
            if (stocktransferItem != null)
            {               
                //Manual Sync insert process for Online to offline transfer by Poongodi on 08/11/2017
                Instance branch = await _instanceSetupManager.GetById(stocktransferItem[0].InstanceId);
                bool bFromInstanceOffline = false;
               
                if (branch != null)
                    bFromInstanceOffline = branch.IsOfflineInstalled as bool? ?? false;
                if (_configHelper.AppConfig.OfflineMode == false && bFromInstanceOffline)
                    bWriteSyncQue = true;

                //Status updated method removed from outside and added by here by Poongodi on 23/05/2017
                //To InstanceId parameter(for offline sync) added for by Poongodi on 23/05/2017 
                stocktransferItem.ForEach(x => x.IsFromInstanceOfflineInstalled = bFromInstanceOffline);

             await DataAccess.UpdateStatus(id, status, user.UserId(), stocktransferItem[0], bWriteSyncQue);
 
            }
            if (stocktransferItem != null && stocktransferItem[0].ProductStockId != "" )
            {
               
                var  tmpTransItem = await DataAccess.GetStockTransferItem(new[] { id });
                tmpTransItem.ToList().ForEach(x =>
                {
                    x.SetExecutionQuery(stocktransferItem[0].GetExecutionQuery(), stocktransferItem[0].WriteExecutionQuery);
                    x.ProductStock.SetExecutionQuery(stocktransferItem[0].GetExecutionQuery(), stocktransferItem[0].WriteExecutionQuery);
                    x.IsFromInstanceOfflineInstalled = stocktransferItem[0].IsFromInstanceOfflineInstalled;
                });

                if ((tmpTransItem == null || tmpTransItem.ToList().Count == 0) && status != StockTransfer.TransferStatusRejected)
                {
                    var trnItem = await DataAccess.EditStockTransferItem(new[] { id });
                    var tmpTransferItem = trnItem.Where(y => y.TransferId == id).ToList();
                    tmpTransferItem.ToList().ForEach(x =>
                    {
                        x.SetExecutionQuery(stocktransferItem[0].GetExecutionQuery(), stocktransferItem[0].WriteExecutionQuery);
                        x.ProductStock.SetExecutionQuery(stocktransferItem[0].GetExecutionQuery(), stocktransferItem[0].WriteExecutionQuery);
                        x.IsFromInstanceOfflineInstalled = stocktransferItem[0].IsFromInstanceOfflineInstalled;
                    });

                    //Productstock Exist validation added by Poongodi on 25/05/2017
                    for (int n = 0; n < stocktransferItem.Count; n++)
                    {
                        /*ProductStock Accepted details added by poongodi on 16/06/2017*/
                        stocktransferItem[n].AcceptedPackageSize = stocktransferItem[n].PackageSize;
                        stocktransferItem[n].AcceptedUnits = stocktransferItem[n].Quantity1;
                        if (stocktransferItem[n].PackageSize > 0)
                        {
                            stocktransferItem[n].AcceptedStrip = stocktransferItem[n].Quantity1/stocktransferItem[n].PackageSize;
                        }
                        var ps = new List<ProductStock>();
                        tmpTransferItem[n].ProductStock.ExpireDate = stocktransferItem[n].ProductStock.ExpireDate;
                        tmpTransferItem[n].ProductStock.BatchNo = stocktransferItem[n].ProductStock.BatchNo; // batchNo;
                        tmpTransferItem[n].ProductStock.ProductId = stocktransferItem[n].ProductStock.Product.Id;
                        tmpTransferItem[n].ProductStock.AccountId = stocktransferItem[n].AccountId;
                        tmpTransferItem[n].ProductStock.InstanceId = user.InstanceId();

                        tmpTransferItem[n].ProductStock.PackageSize = stocktransferItem[n].PackageSize;
                        if (stocktransferItem[n].Quantity1 != stocktransferItem[n].Quantity && stocktransferItem[n].PackageSize>0)
                        {
                            tmpTransferItem[n].ProductStock.PurchasePrice = stocktransferItem[n].PurchasePrice / stocktransferItem[n].PackageSize;
                            tmpTransferItem[n].ProductStock.MRP = stocktransferItem[n].MRP / stocktransferItem[n].PackageSize;
                            tmpTransferItem[n].ProductStock.SellingPrice = stocktransferItem[n].SellingPrice / stocktransferItem[n].PackageSize;
                        }                        
                        await DataAccess.UpdateAcceptItem(stocktransferItem[n], stocktransferItem[n].InstanceId,bWriteSyncQue);
                        var productStockExist = await _productStockDataAccess.GetProductStock(tmpTransferItem[n].ProductStock);
                        if (productStockExist.Count == 0)
                        {

                            ps.Add(tmpTransferItem[n].ProductStock);
                            ps[0].SetLoggedUserDetails(user);
                            var data = await _productStockManager.saveNewStock(ps, user.AccountId(), user.InstanceId(), user.UserId(), true);
                             await DataAccess.UpdateStockTransferToPdtId(stocktransferItem[n].Id, stocktransferItem[n].ProductId, data[0].Id, stocktransferItem[n].InstanceId,bWriteSyncQue, stocktransferItem[n]);
                           
                        }
                        else
                        {
                            foreach (var stockItem in productStockExist)
                            {
                                stockItem.SetExecutionQuery(stocktransferItem[0].GetExecutionQuery(), stocktransferItem[0].WriteExecutionQuery);
                                stockItem.Stock += tmpTransferItem[n].ProductStock.Stock.Value;
                                await _productStockDataAccess.Update(stockItem);

                                //await DataAccess.UpdateStockTransferToPdtId("", "", stockTransferItem.ProductId);
                              await DataAccess.UpdateStockTransferToPdtId(stocktransferItem[n].Id, stocktransferItem[n].ProductId, stockItem.Id, stocktransferItem[n].InstanceId, bWriteSyncQue, stocktransferItem[n]);
                               
                            }
                        }
                        //Below line commented and UpdateStockTransferToPdtId method added by Poongodi on 18/05/2017
                        //await DataAccess.UpdateStockTransfer(stocktransferItem[n].Id, data[0].Id);
                       
                    }
                }
                else
                {                    
                    if (status == StockTransfer.TransferStatusAccepted)
                    {
                        await UpdateAcceptedProductStock(tmpTransItem, stocktransferItem, bWriteSyncQue); //stocktransferItem Added by  Poongodi on 25/05/2017


                    }
                    else if (status == StockTransfer.TransferStatusRejected)
                    {                     
                     //when the tmpTransferItem does not contain data Stock transfer details has taken for Stock Reversal By Poongodi on 24/05/2017
                        if (tmpTransItem==null || tmpTransItem.Count() ==0 )
                        {                            
                          int nReturn = DataAccess.RejectProductStockUpdate(stocktransferItem, stocktransferItem[0].InstanceId);
                        }
                        else
                        {
                            await UpdateRejectProductStock(tmpTransItem);
                        }
                    }
                }
            }
            else
            {
                var trnItem = await DataAccess.EditStockTransferItem(new[] { id });
                var tmpTransItem = trnItem.Where(y => y.TransferId == id).ToList();
                tmpTransItem.ToList().ForEach(x =>
                {
                    x.SetExecutionQuery(stocktransferItem[0].GetExecutionQuery(), stocktransferItem[0].WriteExecutionQuery);
                    x.ProductStock.SetExecutionQuery(stocktransferItem[0].GetExecutionQuery(), stocktransferItem[0].WriteExecutionQuery);
                    x.IsFromInstanceOfflineInstalled = stocktransferItem[0].IsFromInstanceOfflineInstalled;
                });

                for (int n = 0; n < stocktransferItem.Count; n++)
                {
                    /*ProductStock Accepted details added by poongodi on 20/06/2017*/
                    stocktransferItem[n].AcceptedPackageSize = stocktransferItem[n].PackageSize;
                    stocktransferItem[n].AcceptedUnits = stocktransferItem[n].Quantity1;
                    if (stocktransferItem[n].PackageSize > 0)
                    {
                        stocktransferItem[n].AcceptedStrip = stocktransferItem[n].Quantity1 / stocktransferItem[n].PackageSize;
                    }
                    var ps = new List<ProductStock>();

                    tmpTransItem[n].ProductStock.ExpireDate = stocktransferItem[n].ProductStock.ExpireDate;
                    tmpTransItem[n].ProductStock.BatchNo = stocktransferItem[n].ProductStock.BatchNo; // batchNo;
                    if(tmpTransItem[n].ProductStockId == "")
                    {
                        tmpTransItem[n].ProductStock.PurchasePrice = tmpTransItem[n].ProductStock.PurchasePrice / tmpTransItem[n].ProductStock.PackageSize;
                        tmpTransItem[n].ProductStock.SellingPrice = tmpTransItem[n].ProductStock.SellingPrice / tmpTransItem[n].ProductStock.PackageSize;
                        tmpTransItem[n].ProductStock.MRP = tmpTransItem[n].ProductStock.MRP / tmpTransItem[n].ProductStock.PackageSize;
                    }
                    tmpTransItem[n].ProductStock.PackageSize = stocktransferItem[n].PackageSize;
                    if (stocktransferItem[n].Quantity1 != stocktransferItem[n].Quantity && stocktransferItem[n].PackageSize > 0)
                    {
                        tmpTransItem[n].ProductStock.PurchasePrice = stocktransferItem[n].PurchasePrice / stocktransferItem[n].PackageSize;
                        tmpTransItem[n].ProductStock.MRP = stocktransferItem[n].MRP / stocktransferItem[n].PackageSize;
                        tmpTransItem[n].ProductStock.SellingPrice = stocktransferItem[n].SellingPrice / stocktransferItem[n].PackageSize;
                    }
                    await DataAccess.UpdateAcceptItem(stocktransferItem[n], stocktransferItem[n].InstanceId,bWriteSyncQue);
                    ps.Add(tmpTransItem[n].ProductStock);
                    ps[0].SetLoggedUserDetails(user);
                    var data = await _productStockManager.saveNewStock(ps, user.AccountId(), user.InstanceId(), user.UserId(), true);
                    await DataAccess.UpdateStockTransfer(stocktransferItem[n].Id, data[0].Id, stocktransferItem[n]);
                }
            }
           
            return true;
        }
        public async Task<bool> SaveBulkDataAcceptReject(List<StockTransferItem> stocktransferItem)
        {
            await _dataAccess.SaveBulkDataAcceptReject(stocktransferItem);
            if (stocktransferItem[0].SaveFailed && stocktransferItem[0].ErrorLog.ErrorMessage.ToLower() != ("It seems, Transfer already Accepted/Rejected.").ToLower() 
                && NetworkInterface.GetIsNetworkAvailable())
            {
                var txtEmail = $@"<p>Dear Team,<br/><br/>Customer is facing below issue, please look into this.<br/>
                                <br/><B>Issue details:</B><br/>
                                <br/><B>Mode:</B> { (_configHelper.AppConfig.OfflineMode ? "Offline" : "Online") }
                                <br/><B>Branch Id:</B> { stocktransferItem[0].Instance.Id }
                                <br/><B>Branch Name:</B> { stocktransferItem[0].Instance.Name}                                
                                <br/><B>Transaction Type:</B> Stock Transfer {(stocktransferItem[0].IsAccept ? "Accept" : "Reject")}
                                <br/><br/><B>Error Message:</B> { stocktransferItem[0].ErrorLog.ErrorMessage }
                                <br/><B>Error Stack Trace:</B> { stocktransferItem[0].ErrorLog.ErrorStackTrace }
                                <br/><br/><br/>Thank You!</p><p>Powered By hCue</p>";

                _emailSender.Send("pharmacytech@myhcue.com", 19, txtEmail);
            }

            return true;
        }
        private async Task UpdateRejectProductStock(IEnumerable<StockTransferItem> stockTransferItems)
        {          
            foreach (var stockTransferItem in stockTransferItems)
            {
                stockTransferItem.ProductStock.SetExecutionQuery(stockTransferItem.GetExecutionQuery(), stockTransferItem.WriteExecutionQuery);
                await _productStockManager.UpdateProductStock(stockTransferItem.ProductStockId, stockTransferItem.Quantity, stockTransferItem.ProductStock);
            }
        }
        //The below method commented and the same method created by Poongodi on 16/05/2017
        private async Task UpdateAcceptedProductStock_old(IEnumerable<StockTransferItem> stockTransferItems, IEnumerable<StockTransferItem> stockTransferId)
        {
            var user = _httpContextAccessor.HttpContext.User;

            var transferItems = stockTransferItems.Select(x =>
            {
                x.ProductStock.Stock = x.Quantity;
                return (ProductStock)x.ProductStock.SetLoggedUserDetails(user);
            });
            await _productStockManager.Update(transferItems, stockTransferId);
        }
        //The below method created by Poongodi  on 16/05/2017
        private async Task UpdateAcceptedProductStock(IEnumerable<StockTransferItem> stockTransferItems, IEnumerable<StockTransferItem> stockTransferId, bool bWriteSyncQue)
        {
                   /*Stock Transfer Accepted details Added by Poongodi on 16/06/2017*/
                   var user = _httpContextAccessor.HttpContext.User;
            foreach (var stockTransferItem in stockTransferItems)
            {
                StockTransfer stktrn = new StockTransfer();
                stktrn.StockTransferItems = stockTransferId.Where(y => y.Id == stockTransferItem.Id).ToList();
                stockTransferItem.AcceptedPackageSize = stktrn.StockTransferItems[0].PackageSize;
                stockTransferItem.AcceptedUnits = stktrn.StockTransferItems[0].Quantity1;
                if (stktrn.StockTransferItems[0].Quantity != stktrn.StockTransferItems[0].Quantity1 && stktrn.StockTransferItems[0].PackageSize > 0)
                {
                    stockTransferItem.ProductStock.PurchasePrice = stktrn.StockTransferItems[0].PurchasePrice / stktrn.StockTransferItems[0].PackageSize;
                    stockTransferItem.ProductStock.MRP = (stktrn.StockTransferItems[0].MRP * stockTransferItem.ProductStock.PackageSize) / stktrn.StockTransferItems[0].PackageSize;
                    stockTransferItem.ProductStock.SellingPrice = (stktrn.StockTransferItems[0].SellingPrice * stockTransferItem.ProductStock.PackageSize) / stktrn.StockTransferItems[0].PackageSize;
                }

                stockTransferItem.Quantity = stktrn.StockTransferItems[0].Quantity1;
                stockTransferItem.PackageSize = stktrn.StockTransferItems[0].PackageSize;
                if (stktrn.StockTransferItems[0].PackageSize > 0)
                {
                    stockTransferItem.AcceptedStrip = stktrn.StockTransferItems[0].Quantity1 / stktrn.StockTransferItems[0].PackageSize;
                }
                stockTransferItem.SetExecutionQuery(stktrn.StockTransferItems[0].GetExecutionQuery(), stktrn.StockTransferItems[0].WriteExecutionQuery);
                await DataAccess.UpdateAcceptItem(stockTransferItem, stockTransferId.FirstOrDefault().InstanceId, bWriteSyncQue);
                var productStock = (ProductStock)stockTransferItem.ProductStock;
                productStock.Stock = stockTransferItem.Quantity;
                productStock.PackageSize = stockTransferItem.PackageSize;
                productStock = (ProductStock)stockTransferItem.ProductStock.SetLoggedUserDetails(user);
                var transferItems = stockTransferId.Where(x => x.ProductStockId == stockTransferItem.ProductStockId);
                var productStockExist = await _productStockDataAccess.GetProductStock(productStock);



                //var productStockExist = await _productStockDataAccess.GetProductStockList(stockTransferItem.ProductStockId, stockTransferItem.ToInstanceId);
                if (productStockExist.Count >= 1)
                {
                    foreach (var stockItem in productStockExist)
                    {
                        stockItem.SetExecutionQuery(stktrn.StockTransferItems[0].GetExecutionQuery(), stktrn.StockTransferItems[0].WriteExecutionQuery);
                        stockItem.Stock += productStock.Stock.Value;
                        await _productStockDataAccess.Update(stockItem);

                        //await DataAccess.UpdateStockTransferToPdtId("", "", stockTransferItem.ProductId);
                        await DataAccess.UpdateStockTransferToPdtId(transferItems.FirstOrDefault().Id, stockTransferItem.ProductId, stockItem.Id, transferItems.FirstOrDefault().InstanceId, bWriteSyncQue, stockTransferItem);
                    }
                }
                else
                {
                    productStock.SetExecutionQuery(stktrn.StockTransferItems[0].GetExecutionQuery(), stktrn.StockTransferItems[0].WriteExecutionQuery);
                    var tmpId = await _productStockDataAccess.Save(productStock);

                    await DataAccess.UpdateStockTransferToPdtId(transferItems.FirstOrDefault().Id, stockTransferItem.ProductId, productStock.Id, transferItems.FirstOrDefault().InstanceId, bWriteSyncQue, stockTransferItem);
                }
            }
    
        }
        public async Task<IEnumerable<Instance>> GetFilterInstance(bool forAccept)
        {
            var user = _httpContextAccessor.HttpContext.User;
            return await DataAccess.GetFilterInstance(user.InstanceId(), forAccept);
        }

        public async Task<TransferSettings> GetTransferSetting(TransferSettings ts)
        {
            return await DataAccess.GetTransferSetting(ts);
        }

        public async Task<TransferSettings> UpdateTransferSetting(TransferSettings ts)
        {
            return await DataAccess.UpdateTransferSetting(ts);
        }

        public async Task<StockTransfer> TransferById(string id,string instanceId)
        {            
            return await DataAccess.TransferById(id, instanceId);
        }
        //Added by Settu for indent transfer with stock reducing
        public async Task<List<VendorOrder>> GetActiveIndentList(string OrderId, int Type, DateTime? fromDate,DateTime? toDate)
        {
            return await DataAccess.GetActiveIndentList(OrderId, Type, fromDate, toDate);
        }
        public async Task<List<ProductStock>> getAcceptedTransferList(string TransferNo)
        {
            return await DataAccess.getAcceptedTransferList(TransferNo);
        }

        //To Get Stock Transfer Items from Online Added by Sumathi on 08-Jan-2019
        public async Task<List<StockTransferItem>> GetStockTransferOnline(ProductSearch entity)
        {
            var sc = new RestConnector();
            var result = await sc.PostAsyc<List<StockTransferItem>>(_configHelper.InternalApi.StockTransferUrl, entity);
            return result;
        }

        public async Task<List<StockTransferItem>> GetStockTransferItem(string id, string instanceId)
        {
            return await DataAccess.GetStockTransferItem(id, instanceId);
        }

    }
}
