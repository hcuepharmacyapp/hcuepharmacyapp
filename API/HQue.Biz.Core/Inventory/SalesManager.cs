﻿using HQue.Contract.Infrastructure;
using HQue.Contract.Infrastructure.Communication;
using HQue.Contract.Infrastructure.Inventory;
using HQue.DataAccess.Inventory;
using HQue.DataAccess.Setup;
using System.Threading.Tasks;
using System.Collections.Generic;
using System;
using HQue.Contract.Infrastructure.Settings;
using HQue.Contract.Infrastructure.Master;
using System.Linq;
using Utilities.Helpers;
using HQue.Biz.Core.Setup;
using HQue.Contract.Infrastructure.Setup;

namespace HQue.Biz.Core.Inventory
{
    public class SalesManager : BizManager
    {
        private new SalesDataAccess DataAccess => base.DataAccess as SalesDataAccess;
        private readonly SalesSaveManager _salesSaveManager;
        private readonly InstanceSetupDataAccess _instanceDataAccess;
        private readonly RegistrationManager _registrationManager;
        private readonly ProductStockManager _productStockManager;
        public SalesManager(SalesDataAccess salesDataAccess, SalesSaveManager salesSaveManager, InstanceSetupDataAccess instanceDataAccess,
            RegistrationManager registrationManager, ProductStockManager productStockManager) : base(salesDataAccess)
        {
            _salesSaveManager = salesSaveManager;
            _instanceDataAccess = instanceDataAccess;
            _registrationManager = registrationManager;
            _productStockManager = productStockManager;
        }
        public Sales RemoveReturnItems(Sales data)
        {
            return DataAccess.RemoveReturnItems(data);
        }
        public async Task<Sales> SaveBulkData(Sales data)
        {
            return await _salesSaveManager.SaveBulkData(data);
        }
        public async Task<Sales> Save(Sales data, DateTime invoiceDate, string instanceid, bool fromNewScreen)
        {
            //Sales sales = new Sales();
            //List<ProductStock> availableItems = new List<ProductStock>();
            //if (string.IsNullOrEmpty(data.Id))
            //{
            //    bool flagStockAvail = true;
            //    foreach (var item in data.SalesItem)
            //    {
            //        var totalStock = await DataAccess.GetTotalStock(item.ProductStockId, instanceid);
            //        if (totalStock.Stock < item.Quantity)
            //        {
            //            flagStockAvail = false;
            //            availableItems.Add(new ProductStock { Id = item.ProductStockId, Stock = totalStock.Stock });
            //        }
            //    }
            //    if (flagStockAvail)
            //    {
            //        sales = await _salesSaveManager.CreateSale(data, invoiceDate);
            //    }
            //}
            //else
            //{
            //    sales = await _salesSaveManager.CreateSale(data, invoiceDate);
            //}
            //sales.AvailableItems = availableItems;
            //return sales;

            //data.SalesItem.ForEach(x =>
            //{
            //    if (!(x.Total.HasValue && x.ActualAmount.HasValue))
            //        x.VatAmount = 0;
            //    else
            //        x.VatAmount = x.Total - x.ActualAmount;
            //    if (!(x.Total.HasValue && x.Discount.HasValue))
            //        x.DiscountAmount = 0;
            //    else
            //        x.DiscountAmount = ((x.Discount / 100) * x.Total);
            //});
           
            if (data.DiscountType == 1)
            {
                //data.DiscountValue = (data.Discount * data.NetAmount / (100 - data.Discount));
                // data.DiscountValue = (data.Discount * data.PurchasedTotal / (100 - data.Discount));
                //data.DiscountValue = Math.Round(Convert.ToDecimal((data.PurchasedTotal / 100) * data.Discount),2,MidpointRounding.AwayFromZero);  //Added by Sarubala on 12-09-17 for rounding
                data.DiscountValue = Math.Round(Convert.ToDecimal((data.SalesItemAmount / 100) * data.Discount), 6, MidpointRounding.AwayFromZero); 
               
            }
            else
            {
                //data.Discount = data.DiscountValue / (data.DiscountValue + data.NetAmount) * 100;
                //  if(data.PurchasedTotal != 0)
                //  {
                //data.Discount = Math.Round(Convert.ToDecimal(data.DiscountValue / (data.PurchasedTotal) * 100),2,MidpointRounding.AwayFromZero);  //Added by Sarubala on 12-09-17 for rounding
                data.Discount = Math.Round(Convert.ToDecimal(data.DiscountValue / (data.SalesItemAmount) * 100), 2, MidpointRounding.AwayFromZero);  
                // }

            }

            if (data.IsNew == false && data.TaxRefNo == 0)
            {
                data.GSTEnabled = false;
            }

            if (data.GSTEnabled == true)
            {
                //commented for dead locks
                //data = await DataAccess.updateProductStockGST(data);// Added by sarubala on 30/06/2017 to update gst in product stock

                // data.GstAmount = data.VatAmount;
                data.GstAmount = Math.Round(Convert.ToDecimal(data.GstAmount), 6, MidpointRounding.AwayFromZero);
                data.VatAmount = 0;
                data.TaxRefNo = 1;
                foreach (var item in data.SalesItem)
                {
                    //item.GstAmount = item.SalesItemGstAmount;
                    //if(data.Discount > 0) // Added by Sarubala on 11-09-17
                    //{
                    //    item.GstAmount = (item.Total - (item.Total * data.Discount / 100)) * ( 1 - (100 / (item.GstTotal + 100)));
                    //}
                    item.Cgst = item.GstTotal / 2;
                    item.Sgst = item.GstTotal / 2;
                    item.VAT = null;
                    item.VatAmount = 0;
                    var pr = await _productStockManager.UpdateProductGST(item.ProductStock);  // Added by Poongodi on 04/07/2017 to update gst % in product table
                    item.SalesDiscount = data.Discount; // Added by Sarubala on 11-09-17
                }

            }
            else
            {
                data.GstAmount = 0;
                data.TaxRefNo = 0;
                foreach (var item in data.SalesItem)
                {
                    item.GstTotal = null;
                    item.GstAmount = 0;
                    item.SalesDiscount = data.Discount; // Added by Sarubala on 11-09-17
                }
            }


            return await _salesSaveManager.CreateSale(data, invoiceDate, fromNewScreen);
            //Sales sales = new Sales();
            //    List<ProductStock> availableItems = new List<ProductStock>();
            //    List<SalesItem> tempSalesItemList = new List<SalesItem>();
            //    if (string.IsNullOrEmpty(data.Id))
            //    {
            //        bool flagStockAvail = true;
            //        foreach (var item in data.SalesItem)
            //        {
            //            List<ProductStock> productlist = await DataAccess.GetTotalStock(item.productId, instanceid, item.BatchNo);
            //            int totalStock = 0;
            //            string productStockId = string.Empty;
            //            totalStock = Convert.ToInt32(productlist.Sum(x => x.Stock));
            //            productStockId = item.ProductStockId;
            //            if (productlist.Count > 1)
            //            {
            //                productStockId = productlist.Max(x => x.Id);
            //                decimal? sellingprice = data.SalesItem.Where(x => x.productId == item.productId).Select(y => y.SellingPrice).First();
            //                //Adding salesitem objects
            //                foreach (var item2 in productlist)
            //                {
            //                    decimal? quantity = 0.00M;
            //                    if (item.Quantity >= item2.Stock)
            //                    {
            //                        quantity = item2.Stock;
            //                        item.Quantity = item.Quantity - item2.Stock;
            //                    }
            //                    else
            //                    {
            //                        quantity = item.Quantity;//20
            //                    }
            //                    SalesItem item3 = new SalesItem()
            //                    {
            //                        ProductStockId = item2.Id,
            //                        SellingPrice = sellingprice,
            //                        Quantity = quantity,
            //                        Discount = item.Discount
            //                    };
            //                    tempSalesItemList.Add(item3);
            //                }
            //            }
            //            else
            //            {
            //                tempSalesItemList.Add(item);
            //            }
            //            if (totalStock < item.Quantity)
            //            {
            //                flagStockAvail = false;
            //                availableItems.Add(new ProductStock { Id = productStockId, Stock = totalStock });
            //            }
            //        }
            //        data.SalesItem = tempSalesItemList;
            //        if (flagStockAvail)
            //        {
            //            sales = await _salesSaveManager.CreateSale(data, invoiceDate);
            //        }
            //    }
            //    else
            //    {
            //        sales = await _salesSaveManager.CreateSale(data, invoiceDate);
            //    }
            //    sales.AvailableItems = availableItems;
            //    return sales;
        }
        public async Task<bool> IsInvoiceManualSeriesAvail(string invoiceSeries, string instanceId, string AccountId, DateTime Billdate)
        {
            return await DataAccess.IsInvoiceManualSeriesAvail(invoiceSeries, instanceId, AccountId, Billdate);
        }
        public async Task<string> GetInvoiceNo(string instanceId)
        {
            return await DataAccess.GetInvoiceNo(instanceId);
        }
        #region
        //Added by Manivannan for retrieving the Invoice No in sales screen on 30-Jan-2017 begins here
        public async Task<string> GetNumericInvoiceNo(string instanceId)
        {
            return await DataAccess.GetNumericInvoiceNo(instanceId);
        }
        public async Task<string> GetCustomInvoiceNo(string instanceId, string customSeries)
        {
            return await DataAccess.GetCustomInvoiceNum(instanceId, customSeries);
        }
        #endregion
        //Method modified by POongodi on 27/02/2016 for Saleshistory optimization
        public async Task<PagerContract<Sales>> ListPager(Sales data, string instanceId, string accountid)
        {
            var pager = new PagerContract<Sales>
            {
                //NoOfRows = await DataAccess.Count(data), Count method removed 
                List = await DataAccess.List(data, instanceId, accountid)
            };
            if (pager.List == null || pager.List.Count() <= 0)
            {
                pager.NoOfRows = 1;
            }
            else
                pager.NoOfRows = pager.List.FirstOrDefault().SalesCount;
            return pager;
        }

        //Method modified by violet on 09/05/2017 for Saleshistory optimization
        public async Task<Sales> getListPager(Sales data, string instanceId, string accountid)
        {
            var List = await DataAccess.getList(data, instanceId, accountid);
            return List;
        }

        //public async Task<Sales> GetSalesDetails(string id)
        //{
        //    return await DataAccess.GetById(id);
        //}
        public async Task<Sales> GetSalesDetails(string id)
        {
            var sales = await DataAccess.GetById(id);
            if (sales != null)
            {
                var salesPayment = await DataAccess.GetSalesPayments(sales.AccountId, sales.InstanceId, sales.Id);
                if (salesPayment != null && salesPayment.Count() > 0)
                {
                    sales.SalesPayments = salesPayment.ToList();
                }
            }

            //Added by Sarubala on 14-06-2018 to get loyalty settings
            if (!string.IsNullOrEmpty(sales.LoyaltyId) && !string.IsNullOrWhiteSpace(sales.LoyaltyId))
            {
                sales.loyaltySettings = await DataAccess.getSalesLoyaltyPoint(sales.AccountId, sales.LoyaltyId, sales.Id);
            }

            var billPrintSettings = await DataAccess.GetBillPrintStatus(sales.AccountId, sales.InstanceId);
            sales.PrintFooterNote = billPrintSettings.Item2;
            return sales;
        }

        public async Task<Sales> GetReturnOnlyDetails(string id)
        {
            var sales = await DataAccess.GetByReturnId(id);

            var billPrintSettings = await DataAccess.GetBillPrintStatus(sales.AccountId, sales.InstanceId);
            sales.PrintFooterNote = billPrintSettings.Item2;
            return sales;
        }

        public async Task<Patient> GetExistingPatientData(string patientId, string mobile, string name, string accountId, string instanceId)
        {
            return await DataAccess.GetExistingPatientData(patientId, mobile, name, accountId, instanceId);
        }

        //Added by Sarubala to get sales multiple payment details - start
        public async Task<List<SalesPayment>> GetSalesPaymentDetails(string id, string AccId, string InsId)
        {
            return await DataAccess.GetSalesPaymentDetails(id, AccId, InsId);
        }
        //Added by Sarubala to get sales multiple payment details - end

        public virtual Task<List<Sales>> PatientSearchList(string data, string accountid, string instanceid)
        {
            var plist = DataAccess.GetPatientList(data, accountid, instanceid);
            return plist;
        }
        public async Task<PagerContract<Sales>> PatientList(Sales data)
        {
            var pager = new PagerContract<Sales>
            {
                NoOfRows = await DataAccess.CustomerCount(data),
                List = await DataAccess.PatientList(data)
            };
            return pager;
        }
        public async Task<PagerContract<Sales>> PatientListBulkSms(Sales data)
        {
            var pager = new PagerContract<Sales>
            {
                NoOfRows = await DataAccess.PatientCount(data),
                List = await DataAccess.PatientListBulkSms(data)
            };
            return pager;
        }
        public async Task<Sales> CustomerInformation(Sales data)
        {
            var sales = await DataAccess.PatientDetail(data);
            return sales;
        }
        public async Task<PagerContract<Sales>> PatientSalesPager(Sales data)
        {
            var pager = new PagerContract<Sales>
            {
                //NoOfRows = await DataAccess.Count(data),
                NoOfRows = await DataAccess.CustomerHistoryInvoiceCount(data),
                List = await DataAccess.SalesList(data)
            };
            return pager;
        }
        //Added by Sarubala on 20-11-17
        public async Task<bool> GetCustomerBulkSmsSetting(string InsId)
        {
            return await DataAccess.GetCustomerBulkSmsSetting(InsId);
        }

        public async Task<Sales> UpdateSaleDetails(Sales data)
        {
            var sales = await DataAccess.UpdateSaleDetail(data);
            return sales;
        }
        public async Task<Sales> UpdateSaleDetailsforFileUpload(Sales data)
        {
            var sales = await DataAccess.UpdateSaleDetailsforFileUpload(data);
            return sales;
        }
        public async Task<DiscountRules> SaveDiscount(DiscountRules data)
        {
            var discountRules = await DataAccess.SaveDiscount(data);
            return discountRules;
        }
        public async Task<InventorySmsSettings> SaveSmsSetting(InventorySmsSettings inventorySmsSettings)
        {
            var discountRules = await DataAccess.saveSmsSetting(inventorySmsSettings);
            return discountRules;
        }
        public async Task<CardTypeSettings> SaveCardType(CardTypeSettings data)
        {
            return await DataAccess.SaveCardType(data);
        }
        public async Task<SaleSettings> SavePrintType(SaleSettings data)
        {
            return await DataAccess.SavePrintType(data);
        }
        public async Task<SalesBatchPopUpSettings> saveBatchPopUpSettings(SalesBatchPopUpSettings data)
        {
            return await DataAccess.saveBatchPopUpSettings(data);
        }
        public async Task<SaleSettings> saveDoctorSearchType(SaleSettings data)
        {
            var searchtype = await DataAccess.saveDoctorSearchType(data);
            return searchtype;
        }
        //Added by Sarubala on 03-10-17
        public async Task<bool> getSettingsIsCancelEditSms(string AccId, string InsId)
        {
            return await DataAccess.GetSettingsIsCancelEditSms(AccId, InsId);
        }
        // Insert Custom Template
        public async Task<SaleSettings> SaveCustomTemplate(SaleSettings data)
        {
            return await DataAccess.SaveCustomTemplate(data);
        }
        public async Task<SaleSettings> savePatientSearchType(SaleSettings data)
        {
            var searchtype = await DataAccess.savePatientSearchType(data);
            return searchtype;
        }
        public async Task<SaleSettings> isDoctorMandatory(SaleSettings data)
        {
            var searchtype = await DataAccess.isDoctorMandatory(data);
            return searchtype;
        }
        public async Task<SaleSettings> saveSalesTypeMandatory(SaleSettings data)
        {
            var searchtype = await DataAccess.saveSalesTypeMandatory(data);
            return searchtype;
        }
        public async Task<SaleSettings> isMaximumDiscountAvail(SaleSettings data)
        {
            var searchtype = await DataAccess.isMaximumDiscountAvail(data);
            return searchtype;
        }
        public async Task<SaleSettings> saveAutoTempStockAdd(SaleSettings data)
        {
            var result = await DataAccess.saveAutoTempStockAdd(data);
            return result;
        }
        public async Task<SaleSettings> isAutosavecustomer(SaleSettings data)
        {
            var searchtype = await DataAccess.isAutosavecustomer(data);
            return searchtype;
        }
        public async Task<SaleSettings> saveIsCreditInvoiceSeries(SaleSettings data)
        {
            var searchtype = await DataAccess.saveIsCreditInvoiceSeries(data);
            return searchtype;
        }
        public async Task<SaleSettings> saveDiscountType(SaleSettings data)
        {
            var searchtype = await DataAccess.saveDiscountType(data);
            return searchtype;
        }
        public async Task<SaleSettings> saveisDepartment(SaleSettings data)
        {
            var searchtype = await DataAccess.saveisDepartment(data);
            return searchtype;
        }
        public async Task<SaleSettings> saveCustomseriesInvoice(SaleSettings data)
        {
            var customseries = await DataAccess.saveCustomseriesInvoice(data);
            return customseries;
        }
        public async Task<SaleSettings> savecancelBillDays(SaleSettings data)
        {
            var CancelDays = await DataAccess.savecancelBillDays(data);
            return CancelDays;
        }
        public async Task<SaleSettings> saveMaxDiscount(SaleSettings data)
        {
            var CancelDays = await DataAccess.saveMaxDiscount(data);
            return CancelDays;
        }
        public async Task<InvoiceSeries> saveInvoiceSeries(InvoiceSeries data)
        {
            var inv = await DataAccess.saveInvoiceSeries(data);
            return inv;
        }
        public async Task SendSms(CustomerSms customerSms, Sales data)
        {
            await _salesSaveManager.SendSms(customerSms, data);
        }

        public async Task<int> CheckOBPaymentPaid(string accountId, string instanceId, string customerid)
        {
            var res = await DataAccess.CheckOBPaymentPaid(accountId, instanceId, customerid);
            return res;
        }
        public async Task<string> GetCardType(string accountId, string instanceId)
        {
            var cardType = await DataAccess.GetCardType(accountId, instanceId);
            return cardType;
        }
        public async Task<SaleSettings> getPatientSearchType(string AccountId, string InstanceId)
        {
            var searchType = await DataAccess.getPatientSearchType(AccountId, InstanceId);
            return searchType;
        }
        public async Task<SaleSettings> getDoctorSearchType(string AccountId, string InstanceId)
        {
            var searchType = await DataAccess.getDoctorSearchType(AccountId, InstanceId);
            return searchType;
        }
        public async Task<SaleSettings> getDoctorNameMandatoryType(string AccountId, string InstanceId)
        {
            var searchType = await DataAccess.getDoctorNameMandatoryType(AccountId, InstanceId);
            return searchType;
        }
        public async Task<SaleSettings> getIsMaxDiscountAvail(string AccountId, string InstanceId)
        {
            var searchType = await DataAccess.getIsMaxDiscountAvail(AccountId, InstanceId);
            return searchType;
        }
        public async Task<SaleSettings> getautoSaveisMandatory(string AccountId, string InstanceId)
        {
            var searchType = await DataAccess.getautoSaveisMandatory(AccountId, InstanceId);
            return searchType;
        }
        public async Task<SaleSettings> getIsCreditInvoiceSeries(string AccountId, string InstanceId)
        {
            var searchType = await DataAccess.getIsCreditInvoiceSeries(AccountId, InstanceId);
            return searchType;
        }
        public async Task<SalesBatchPopUpSettings> getBatchPopUpSettings(string accountId, string instanceId)
        {
            return await DataAccess.getBatchPopUpSettings(accountId, instanceId);
        }

        //Added by Sarubala on 06-11-17
        public async Task<int> getSalesPriceSetting(string InsId)
        {
            return await DataAccess.getSalesPriceSetting(InsId);
        }

        //Added by Sarubala on 15-11-17
        public async Task<SmsSettings> getSmsSettings(string InstanceId, string AccountId)
        {
            return await DataAccess.getSmsSettings(InstanceId, AccountId);
        }

        //Added by Sarubala on 23-11-17
        public async Task<SmsSettings> getSmsSettingsForInstance(string AccountId, string InstanceId)
        {
            return await DataAccess.getSmsSettingsForInstance(AccountId, InstanceId);
        }

        //Added by Sarubala on 28-11-17
        public async Task<long> GetRemainingSmsCount(string AccountId, string InstanceId)
        {
            return await DataAccess.GetRemainingSmsCount(AccountId, InstanceId);
        }

        public async Task<SmsSettings> saveSmsSettingsForInstance(SmsSettings data)
        {
            return await DataAccess.saveSmsSettingsForInstance(data);
        }

        public async Task<SaleSettings> getSalestypeMandatory(string AccountId, string InstanceId)
        {
            var searchType = await DataAccess.getSalestypeMandatory(AccountId, InstanceId);
            return searchType;
        }
        public async Task<SaleSettings> getAutoTempstockAdd(string AccountId, string InstanceId)
        {
            var result = await DataAccess.getAutoTempstockAdd(AccountId, InstanceId);
            return result;
        }
        public async Task<SaleSettings> getPharmacyDiscountType(string AccountId, string InstanceId)
        {
            var searchType = await DataAccess.getPharmacyDiscountType(AccountId, InstanceId);
            return searchType;
        }
        public async Task<SaleSettings> getCustomSeriesSelected(string AccountId, string InstanceId)
        {
            var customseries = await DataAccess.getCustomSeriesSelected(AccountId, InstanceId);
            return customseries;
        }

        //Added by Sarubala on 03-12-18 - start
        public async Task<SaleSettings> getCustomerIdSeriesSettings(string AccId, string InsId)
        {
            return await DataAccess.getCustomerIdSeriesSettings(AccId, InsId);
        }

        public async Task<SaleSettings> saveCustomerIdSeriesSettings(SaleSettings settings)
        {
            return await DataAccess.saveCustomerIdSeriesSettings(settings);
        }

        //Added by Sarubala on 03-12-18 - end

        public async Task<SaleSettings> getCancelBillDays(string AccountId, string InstanceId)
        {
            var Canceldays = await DataAccess.getCancelBillDays(AccountId, InstanceId);
            return Canceldays;
        }
        public async Task<SaleSettings> getMaxDiscountValue(string AccountId, string InstanceId)
        {
            var Canceldays = await DataAccess.getMaxDiscountValue(AccountId, InstanceId);
            return Canceldays;
        }
        public async Task<InvoiceSeries> getInvoiceSeries(string accountId, string instanceId)
        {
            var invoiceseries = await DataAccess.getInvoiceSeries(accountId, instanceId);
            return invoiceseries;
        }       
        public async Task<SaleSettings> getIsDepartmentadded(string accountId, string instanceId)
        {
            var sDepartmentadded = await DataAccess.getIsDepartmentadded(accountId, instanceId);
            return sDepartmentadded;
        }
        public async Task<Tuple<string, string>> GetBillPrintStatus(string accountId, string instanceId)
        {
            return await DataAccess.GetBillPrintStatus(accountId, instanceId);
        }
        public async Task<string> GetBatchListDetail(string accountId, string instanceId)
        {
            return await DataAccess.GetBatchListDetail(accountId, instanceId);
        }
        //Added by Sarubala on 14-10-17
        public async Task<SaleSettings> GetAllSalesSettings(string accountId, string instanceId)
        {
            return await DataAccess.GetAllSalesSettings(accountId, instanceId);
        }

        //Added by Sarubala on 20-10-17
        public async Task<SaleSettings> GetBaseSaleSettings(string accountId, string instanceId)
        {
            return await DataAccess.GetBaseSaleSettings(accountId, instanceId);
        }
        //Added by Sarubala on 20-10-17
        public async Task<Sales> GetLastSaleDetails(string accountId, string instanceId)
        {
            return await DataAccess.GetLastSaleDetails(accountId, instanceId);
        }

        public async Task<BatchListSettings> SaveBatchType(BatchListSettings bs)
        {
            return await DataAccess.SaveBatchType(bs);
        }
        public async Task<BillPrintSettings> SaveBillPrintStatus(BillPrintSettings ps)
        {
            return await DataAccess.SaveBillPrintStatus(ps);
        }
        //Newly Added Shortcut keys setting Gavaskar 03-02-2017 Start
        public async Task<SaleSettings> SaveShortCutKeySetting(SaleSettings SaleSettings)
        {
            return await DataAccess.SaveShortCutKeySetting(SaleSettings);
        }
        public async Task<SaleSettings> GetShortCutKeySetting(string AccountId, string InstanceId)
        {
            var ShortCutKeySetting = await DataAccess.GetShortCutKeySetting(AccountId, InstanceId);
            return ShortCutKeySetting;
        }

        //Added by Sarubala on 05-10-17
        public async Task<List<DomainValues>> GetDomainValues(string AccId, string InsId)
        {
            return await DataAccess.GetDomainValues(AccId, InsId);
        }

        //Newly Added Shortcut keys setting Gavaskar 03-02-2017 End
        public async Task<SalesType> GetSalesType(string accountId, string instanceId)
        {
            return await DataAccess.GetSalesType(accountId, instanceId);
        }
        public async Task<int> GetPrintType(string accountId, string instanceId)
        {
            return await DataAccess.GetPrintType(accountId, instanceId);
        }
        public async Task<string> GetSaleIdByInvoiceNo(int InvoiceSeriesType, string SeriesTypeValue, string InvNo, string accountid, string instanceid, bool returnStatus)
        {            
            return await DataAccess.GetSaleIdByInvoiceNo(InvoiceSeriesType, SeriesTypeValue, InvNo, accountid, instanceid, returnStatus);
        }
        public async Task<string> GetCustomTemplate(string accountId, string instanceId)
        {
            return await DataAccess.GetCustomTemplate(accountId, instanceId);
        }
        //public async Task<int> GetInvoicePrintType()
        //{
        //    SaleSettings saleSettings = new SaleSettings();
        //    SetMetaData(saleSettings);
        //    return await DataAccess.GetInvoicePrintType(saleSettings);
        //}
        public async Task<SalesType> UpdateSalesType(SalesType st)
        {
            return await DataAccess.UpdateSalesType(st);
        }
        public async Task<InventorySmsSettings> GetSmsSetting(string accountId, string instanceId)
        {
            return await DataAccess.GetSmsSetting(accountId, instanceId);
        }
        public async Task<SalesType> GetPrescriptionSetting(SalesType st)
        {
            return await DataAccess.GetPrescriptionSetting(st);
        }
        public async Task<List<InvoiceSeriesItem>> GetInvoiceSeriesItems(InvoiceSeriesItem IS)
        {
            return await DataAccess.getInvoiceSeriesItems(IS);
        }

        //Added By Bikas on 02/07/2018
        public async Task<Patient> GetCustomerIDSeriesItem(SaleSettings Data)
        {
            return await DataAccess.getCustomerIDSeriesItem(Data);
        }
        public async Task<SalesType> SaveSalesType(SalesType st)
        {
            return await DataAccess.SaveSalesType(st);
        }
        public async Task<InvoiceSeriesItem> SaveInvoiceSeriesItem(InvoiceSeriesItem Is)
        {
            return await DataAccess.saveInvoiceSeriesItem(Is);
        }   

        public async Task<string> RemoveSalesType(string id, string accountId)
        {
            return await DataAccess.RemoveSalesType(id, accountId);
        }
        public async Task<SalesType> SaveStatus(SalesType slist)
        {
            return await DataAccess.SaveStatus(slist);
        }
        public async Task<DiscountRules> GetDiscountRules(DiscountRules discountRules)
        {
            return await DataAccess.GetDiscountRules(discountRules);
        }
        public async Task<DiscountRules> EditDiscountRules(DiscountRules discountRules)
        {
            return await DataAccess.EditDiscountRules(discountRules);
        }
        public async Task<DiscountRules> EditDiscountRules1(DiscountRules discountRules)
        {
            return await DataAccess.EditDiscountRules1(discountRules);
        }
        public async Task<MissedOrder> CreateMissedOrder(MissedOrder data1)
        {
            return await DataAccess.CreateMissedOrder(data1);
        }
        public async Task<ReturnInvoice> GetReturnListById(string salesid, string salesreturnid)
        {
            return await DataAccess.GetReturnListById(salesid, salesreturnid);
        }

        public async Task<ReturnInvoice> GetReturnListWithoutSaleById(string salesreturnid)
        {
            return await DataAccess.GetReturnListWithoutSaleById(salesreturnid);
        }
        /// <summary>
        /// Get offline status from Data access
        /// </summary>
        /// <param name="accountId"></param>
        /// <param name="instanceId"></param>
        /// <returns></returns>
        public async Task<Boolean> getOfflineStatus(string accountId, string instanceId)
        {
            return await DataAccess.getOfflineStatus(accountId, instanceId);
        }
        //Added by Sarubala on 18-05-2018
        public async Task<Boolean> getOnlineEnabledStatus(string accountId, string userId)
        {
            return await DataAccess.getOnlineEnabledStatus(accountId, userId);
        }

        //Added by Sarubala on 23-10-17
        public async Task<Instance> GetInstanceValues(string accId, string insId)
        {
            return await DataAccess.GetInstanceValues(accId, insId);
        }

        public async Task<List<ProductStock>> searchSales(DateTime fromdate, DateTime todate, decimal reOrderFactor, string accountId, string instanceId, int type, bool loadAllProducts)
        {
            return await DataAccess.searchSales(fromdate, todate, reOrderFactor, accountId, instanceId, type, loadAllProducts);
        }
        public async Task<Boolean> getLastSalesPrintType(string accountId, string instanceId)
        {
            return await DataAccess.getLastSalesPrintType(accountId, instanceId);
        }
        public async Task<SaleSettings> saveShortName(SaleSettings SaleSettings)
        {
            return await DataAccess.saveShortName(SaleSettings);
        }
        public async Task<SaleSettings> getShortName(string AccountId, string InstanceId)
        {
            var ShortName = await DataAccess.getShortName(AccountId, InstanceId);
            return ShortName;
        }
        public async Task<bool> GetShortDoctorName(string accountId, string instanceId)
        {
            var shortDoctorName = await DataAccess.GetShortDoctorName(accountId, instanceId);
            return shortDoctorName;
        }
        public async Task<int> GetInstanceCount(string accountId)
        {
            return await _instanceDataAccess.GetTotalInstanceCount(accountId);
        }
        public async Task<bool> GetShortCustomerName(string accountId, string instanceId)
        {
            var shortCustomerName = await DataAccess.GetShortCustomerName(accountId, instanceId);
            return shortCustomerName;
        }
        public async Task<SaleSettings> saveCompleteSaleKeys(SaleSettings data)
        {
            var CompleteSaleType = await DataAccess.saveCompleteSaleKeys(data);
            return CompleteSaleType;
        }
        public async Task<string> getCompleteSaleType(string AccountId, string InstanceId)
        {
            var searchType = await DataAccess.getCompleteSaleType(AccountId, InstanceId);
            return searchType;
        }
        public async Task<SaleSettings> getCompleteSaleKeys(string AccountId, string InstanceId)
        {
            var searchType = await DataAccess.getCompleteSaleKeys(AccountId, InstanceId);
            return searchType;
        }
        public async Task<SaleSettings> getScanBarcodeOption(string AccountId, string InstanceId)
        {
            var searchType = await DataAccess.getScanBarcodeOption(AccountId, InstanceId);
            return searchType;
        }
        public async Task<SaleSettings> saveBarcodeOption(SaleSettings barcodeOption)
        {
            return await DataAccess.saveBarcodeOption(barcodeOption);
        }
        public async Task<string> GetCustomCreditInvoiceNo(string instanceId, string customSeries)
        {
            return await DataAccess.GetCustomInvoiceNum(instanceId, customSeries);
        }
        
               

        public async Task<SaleSettings> getCustomCreditSeriesSelected(string AccountId, string InstanceId)
        {
            var customseries = await DataAccess.getCustomCreditSeriesSelected(AccountId, InstanceId);
            return customseries;
        }
        //Added by San
        public async Task<Sales> getLastSalesId(string accountId, string instanceId)
        {
            return await DataAccess.getLastSalesId(accountId, instanceId);
        }
        public async Task<Boolean> getLastSalesSmsType(string accountId, string instanceId)
        {
            return await DataAccess.getLastSalesSmsType(accountId, instanceId);
        }
        public async Task<Boolean> getLastSalesEmailType(string accountId, string instanceId)
        {
            return await DataAccess.getLastSalesEmailType(accountId, instanceId);
        }
        //Method added to get  Custom series for Purchase  on 26/04/2017
        public async Task<string> getInvoiceSeriesItemForPurchaseSettings(string AccountId, string InstanceId)
        {
            return await DataAccess.getInvoiceSeriesItemForPurchaseSettings(AccountId, InstanceId);
        }

        public async Task<SalesReturn> GetLastReturnId(string userId, string instannceId)
        {
            return await DataAccess.GetLastReturnId(userId, instannceId);
        }
        //Method Added by SARUBALA - LoyaltyPointSettings
        public async Task<LoyaltyPointSettings> getLoyaltyPointSettings(string AccountId, string InstanceId)
        {
            return await DataAccess.getLoyaltyPointSettings(AccountId, InstanceId);
        }

        public async Task<LoyaltyPointSettings> saveLoyaltyPointSettings(LoyaltyPointSettings lp)
        {
            return await DataAccess.saveLoyaltyPointSettings(lp);
        }
        public async Task<int> GetfirstLoyaltyCheck(string patientId, string AccountId, string InstanceId)
        {
            return await DataAccess.GetfirstLoyaltyCheck(patientId, AccountId, InstanceId);
        }
        public async Task<LoyaltyPointSettings> GetSaleLoyaltyPointSettings(string loyaltyId, string AccountId, string InstanceId)
        {
            return await DataAccess.GetSaleLoyaltyPointSettings(loyaltyId, AccountId, InstanceId);
        }
        public async Task<List<Product>> GetKindOfProductDataList(string AccountId, string InstanceId)
        {
            return await DataAccess.GetKindOfProductDataList(AccountId, InstanceId);
        }

        //End LoyaltyPointSettings

        public Task<string> ExportOnlinePatient(string accountId, string instanceID)
        {
            return DataAccess.ExportOnlinePatient(accountId, instanceID);
            //return null;
        }

        //Code by Sarubala to get GSTin from db - start
        public async Task<string> getGSTINDetail(string AccountId, string InstanceId)
        {
            return await DataAccess.getGSTINDetail(AccountId, InstanceId);
        }

        public async Task<Instance> SaveGstIn(Instance data)
        {
            var result = await DataAccess.SaveGstIn(data);

            var instance = await _instanceDataAccess.GetById(data.InstanceId);

            if (data.isEmail)
            {
                await _registrationManager.EmailToVendors(instance);
            }
            return result;
        }
        //Code by Sarubala to get GSTin from db - end

        //Added by Annadurai - 07032017
        public async Task<List<SalesItem>> PreviousSalesDetailsForSelectedCustomer(string Name, string Mobile, string accountId, string SelectedProduct)
        {
            return await DataAccess.PreviousSalesDetailsForSelectedCustomer(Name, Mobile, accountId, SelectedProduct);
        }


        public async Task<SaleSettings> SaveRoundOff(SaleSettings data)
        {
            return await DataAccess.SaveRoundOff(data);
        }
        public async Task<SaleSettings> SaveFreeQty(SaleSettings data)
        {
            return await DataAccess.SaveFreeQty(data);
        }        
        public async Task<bool> GetRoundOffSettings(string accountId, string instanceId)
        {
            return await DataAccess.GetRoundOffSettings(accountId, instanceId);
        }
        public async Task<bool> GetSalesScreenSettings(string accountId, string instanceId)
        {
            return await DataAccess.GetSalesScreenSettings(accountId, instanceId);
        }
        public async Task<SaleSettings> SaveSalesScreenSettings(SaleSettings data)
        {
            return await DataAccess.SaveSalesScreenSettings(data);
        }
        public async Task<bool> getFreeQtySettings(string accountId, string instanceId)
        {
            return await DataAccess.getFreeQtySettings(accountId, instanceId);
        }
        
        public async Task<SaleSettings> SaveSaleUserSettings(SaleSettings data)
        {
            return await DataAccess.SaveSaleUserSettings(data);
        }
        public async Task<bool> GetSaleUserSettings(string accountId, string instanceId)
        {
            return await DataAccess.GetSaleUserSettings(accountId, instanceId);
        }

        public async Task<bool> SendOTPToAPI(string mobileNo)
        {
            return await DataAccess.SendOTPToAPI(mobileNo);
        }

        public async Task<bool> VerifyOTPAPI(string otp, string mobileNo)
        {
            return await DataAccess.VerifyOTPAPI(otp, mobileNo);
        }

    }
}
