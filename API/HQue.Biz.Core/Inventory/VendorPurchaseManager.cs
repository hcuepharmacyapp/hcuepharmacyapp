﻿using System.Collections.Generic;
using HQue.Contract.Infrastructure.Inventory;
using HQue.DataAccess.Inventory;
using HQue.DataAccess.Master;
using HQue.Contract.Infrastructure;
using System.Threading.Tasks;
using HQue.Biz.Core.Accounts;
using HQue.Contract.Infrastructure.Master;
using HQue.DataAccess.Audits;
using System;
using HQue.Contract.Infrastructure.Settings;
using HQue.Biz.Core.Master;
using System.Linq;
using System.Net.NetworkInformation;
using HQue.Communication.Email;
using Utilities.Helpers;


namespace HQue.Biz.Core.Inventory
{
    public class VendorPurchaseManager : BizManager
    {
        private readonly VendorPurchaseDataAccess _vendorPurchaseDataAccess;
        private readonly ProductDataAccess _productDataAccess;
        private readonly ProductStockDataAccess _productStockDataAccess;
        private readonly ProductManager _productManager;
        private readonly PaymentManager _paymentManager;
        private readonly VendorOrderManager _vendorOrderManager;
        private readonly AuditDataAccess _auditDataAccess;
        private readonly ConfigHelper _configHelper;
        private readonly EmailSender _emailSender;

        private readonly ProductStockManager _productStockManager;


        public VendorPurchaseManager(VendorPurchaseDataAccess vendorPurchaseDataAccess,
            ProductDataAccess productDataAccess, PaymentManager paymentManager, AuditDataAccess auditDataAccess, ProductManager productManager, VendorOrderManager vendorOrderManager,
            ProductStockManager productStockManager, ProductStockDataAccess productStockDataAccess,
            ConfigHelper configHelper, EmailSender emailSender) : base(vendorPurchaseDataAccess)
        {
            _vendorPurchaseDataAccess = vendorPurchaseDataAccess;
            _productDataAccess = productDataAccess;
            _paymentManager = paymentManager;
            _auditDataAccess = auditDataAccess;
            _productManager = productManager;
            _vendorOrderManager = vendorOrderManager;
            _productStockManager = productStockManager;
            _productStockDataAccess = productStockDataAccess;
            _configHelper = configHelper;
            _emailSender = emailSender;
        }
        public async Task SaveBuyTemplateSetting(List<TemplatePurchaseChildMaster> listTemplates, string VendorId)
        {
            //listTemplates.ForEach(async (x) => { await _vendorPurchaseDataAccess.SaveBuyTemplateSetting(x, VendorId); });
            await _vendorPurchaseDataAccess.SaveBuyTemplateSetting(listTemplates, VendorId);
        }
        public async Task SaveBuyTemplateChildSetting(TemplatePurchaseChildMasterSettings templatePurchaseChildMasterSettings, string VendorId)
        {
            await _vendorPurchaseDataAccess.SaveBuyTemplateChildSetting(templatePurchaseChildMasterSettings, VendorId);
        }
        public async Task<List<TemplatePurchaseChildMaster>> getBuyTemplateSetting(string AccountId, string InstanceId)
        {
            var listTemplates = await _vendorPurchaseDataAccess.getBuyTemplateSetting(AccountId, InstanceId);
            return listTemplates;
        }
        public async Task<VendorPurchase> SaveBulkData(VendorPurchase data)
        {
            await _vendorPurchaseDataAccess.SaveBulkData(data);
            if (data.SaveFailed && NetworkInterface.GetIsNetworkAvailable())
                if (data.EditMode)
                {
                    var txtEmail = $@"<p>Dear Team,<br/><br/>Customer is facing below issue, please look into this.<br/>
                                <br/><B>Issue details:</B><br/>
                                <br/><B>Mode:</B> { (_configHelper.AppConfig.OfflineMode ? "Offline" : "Online") }
                                <br/><B>Branch Id:</B> { data.InstanceId }
                                <br/><B>Branch Name:</B> { data.Instance.Name }                                
                                <br/><B>Transaction Type:</B>{ (data.EditMode ? "Purchase Edit" : "Purchase") } 
                                <br/><br/><B>Error Message:</B> { data.ErrorLog.ErrorMessage }
                                <br/><B>Error Stack Trace:</B> { data.ErrorLog.ErrorStackTrace }
                                <br/><br/><br/>Thank You!</p><p>Powered By hCue</p>";

                    _emailSender.Send("pharmacytech@myhcue.com", 19, txtEmail);
                }
            return data;
        }


        public async Task<VendorPurchase> Save(VendorPurchase data)
        {
            await ValidateInvoice(data);
            await ValidateEancode(data);
            VendorPurchase getData = new VendorPurchase();
            getData.InstanceId = data.InstanceId;
            getData.AccountId = data.AccountId;
            getData.Prefix = data.Prefix;             /*Prefix added by Poongodi on 10/10/2017*/
            var getGRN = await getGRNumber(getData);
            data.GoodsRcvNo = getGRN.GoodsRcvNo.ToString();   //Added by Sarubala on 22/04/17
            data.BillSeries = getGRN.BillSeries;
            var getfinyear = await getFinYear(getData); /*Added by Poongodi on 25/03/2017*/
            data.Finyear = getfinyear;


            if (string.IsNullOrEmpty(data.Discount.ToString()) && string.IsNullOrEmpty(data.DiscountInRupees.ToString()))
            {
                data.DiscountType = null;
            }

            //Added by Sarubala on 28-05-18
            foreach (var item in data.VendorPurchaseItem)
            {
                if (item.IsSchemeDiscountValue == true)
                {
                    item.SchemeDiscountValue = item.IndividualSchemeValue;
                }
            }

            var vendorPurchase = await _vendorPurchaseDataAccess.Save(data);
            await SaveVendorPurchaseItem(vendorPurchase);
            await _vendorOrderManager.updateVendorOrderItem(data);
            await _paymentManager.SaveVendorPurchasePayment(data);
            return vendorPurchase;
        }
        public async Task<DraftVendorPurchase> SaveDraft(DraftVendorPurchase data)
        {
            //Saving To Draft Table's
            var draftVendorPurchase = await _vendorPurchaseDataAccess.SaveDraftVendorPurchase(data);
            await _vendorPurchaseDataAccess.SaveDraftVendorPurchaseItem(GetEnumerable1(draftVendorPurchase));
            //Saving To Product Table
            await _productManager.SaveProduct(GetEnumerable1(draftVendorPurchase));
            return draftVendorPurchase;
        }
        public async Task<int> DeleteDraft(string draftVendorPurchaseId, string InstanceId)
        {
            await _vendorPurchaseDataAccess.DeleteDraftVendorPurchase(draftVendorPurchaseId, InstanceId);
            await _vendorPurchaseDataAccess.DeleteDraftVendorPurchaseItem(draftVendorPurchaseId, InstanceId);
            return await _vendorPurchaseDataAccess.GetDraftCount(InstanceId);
        }
        private async Task SaveVendorPurchaseItem(VendorPurchase vendorPurchase)
        {
            foreach (var item in vendorPurchase.VendorPurchaseItem)
            {
                if (string.IsNullOrEmpty(item.ProductStock.Product.ProductMasterID) && string.IsNullOrEmpty(item.ProductStock.ProductId))
                {
                    var product = new Product
                    {
                        Name = item.ProductStock.Product.Name,
                        Manufacturer = item.ProductStock.Product.Manufacturer,
                        Type = item.ProductStock.Product.Type,
                        Category = item.ProductStock.Product.Category,
                        Schedule = item.ProductStock.Product.Schedule,
                        Packing = item.ProductStock.Product.Packing,
                        GenericName = item.ProductStock.Product.GenericName,
                        RackNo = item.RackNo,
                        Eancode = item.ProductStock.Product.Eancode,

                        //GRN related fields
                        Igst = item.ProductStock.Product.Igst,
                        Cgst = item.ProductStock.Product.Cgst,
                        Sgst = item.ProductStock.Product.Sgst,
                        GstTotal = item.ProductStock.Product.GstTotal,
                        HsnCode = item.ProductStock.Product.HsnCode,
                        VAT = item.ProductStock.VAT //Added by Annadurai on 05/18/2017
                    };
                    SetMetaData(vendorPurchase, product);
                    //int NoOfRows = await _productDataAccess.TotalProductCount(product);
                    //var getChar = product.Name.Substring(0, 2);
                    //product.Code = (getChar.ToUpper() + (NoOfRows + 1)).ToString();
                    product = await _productDataAccess.SaveImportProduct(product);

                    //Modified by Sarubala on 09/05/18 to fix global product import productinstance saving 2 times
                    if ((item.ProductStock.Product.RackNo == null || item.ProductStock.Product.RackNo.Trim() == "") && (item.ProductStock.Product.BoxNo == null || item.ProductStock.Product.BoxNo.Trim() == ""))
                    {
                        await _productDataAccess.SaveProductInstance(product);
                    }
                        
                    item.ProductStock.ProductId = product.Id;
                }
                else
                {
                    if (item.Action == "U")
                    {
                        Product products = new Product();
                        products.ProductMasterID = item.ProductStock.Product.ProductMasterID;
                        products.AccountId = vendorPurchase.AccountId;
                        products.InstanceId = vendorPurchase.InstanceId;
                        products.Name = item.ProductStock.Product.Name;
                        products.Manufacturer = item.ProductStock.Product.Manufacturer;
                        products.Type = item.ProductStock.Product.Type;
                        products.Schedule = item.ProductStock.Product.Schedule;
                        products.Category = item.ProductStock.Product.Category;
                        products.Packing = item.ProductStock.Product.Packing;
                        products.GenericName = item.ProductStock.Product.GenericName;
                        products.RackNo = item.RackNo;
                        products.Eancode = item.ProductStock.Product.Eancode;
                        products.HsnCode = item.ProductStock.Product.HsnCode;
                        var getProducts = await _productDataAccess.GetImportProductById(products);
                        if (getProducts.Id != null)
                        {
                            item.ProductStock.ProductId = getProducts.Id;
                            item.ProductStock.Product.ProductMasterID = null;
                        }
                    }
                    var product = new Product
                    {

                        Name = item.ProductStock.Product.Name,
                        AccountId = vendorPurchase.AccountId,
                        InstanceId = vendorPurchase.InstanceId,
                        ProductMasterID = item.ProductStock.Product.ProductMasterID,
                        Manufacturer = item.ProductStock.Product.Manufacturer,
                        Type = item.ProductStock.Product.Type,
                        Category = item.ProductStock.Product.Category,
                        Schedule = item.ProductStock.Product.Schedule,
                        Packing = item.ProductStock.Product.Packing,
                        GenericName = item.ProductStock.Product.GenericName,
                        RackNo = item.RackNo,
                        Eancode = item.ProductStock.Product.Eancode,
                        //GST related fields added by POongodi on 25/07/2017
                        Igst = item.ProductStock.Igst,
                        Cgst = item.ProductStock.Cgst,
                        Sgst = item.ProductStock.Sgst,
                        GstTotal = item.ProductStock.GstTotal,
                        HsnCode = item.ProductStock.HsnCode

                    };
                    product.SetExecutionQuery(vendorPurchase.GetExecutionQuery(), vendorPurchase.WriteExecutionQuery);
                    item.ProductStock.Product.SetExecutionQuery(vendorPurchase.GetExecutionQuery(), vendorPurchase.WriteExecutionQuery);
                    item.ProductStock.Product.Igst = item.ProductStock.Igst;
                    item.ProductStock.Product.Cgst = item.ProductStock.Cgst;
                    item.ProductStock.Product.Sgst = item.ProductStock.Sgst;
                    item.ProductStock.Product.GstTotal = item.ProductStock.GstTotal;
                    item.ProductStock.Product.HsnCode = item.ProductStock.HsnCode;
                    //Product Instance Insert Added by Poongodi on 08/06/2017
                    if (Convert.ToString(item.ProductStock.Product.Id) != "")
                    {
                        int nCount = await _productDataAccess.GetProductInstanceCount(product.AccountId,
                                    product.InstanceId, item.ProductStock.Product.Id);
                        if (nCount == 0)
                        {
                            product.Id = item.ProductStock.Product.Id;
                            product.CreatedBy = vendorPurchase.CreatedBy;
                            product.UpdatedBy = vendorPurchase.UpdatedBy;

                            //Modified by Sarubala on 09/05/18 to fix global product import productinstance saving 2 times
                            if ((item.ProductStock.Product.RackNo == null || item.ProductStock.Product.RackNo.Trim() == "") && (item.ProductStock.Product.BoxNo == null || item.ProductStock.Product.BoxNo.Trim() == ""))
                            {
                                await _productDataAccess.SaveProductInstance(product);
                            }
                        }

                    }
                    if (Convert.ToString(item.ProductStock.ProductId) != "")  //Added by Poongodi on 25/07/2017
                    {
                        //Condition Apply Added by Gavaskar 08-09-2017
                        if (item.ProductStock.Product.Id != null)
                        {
                            var result = await _productDataAccess.UpdateProductGSTFromStock(item.ProductStock.Product, true, true, true, true, true);
                        }
                        var result1 = await _productDataAccess.ProductStockUpdate(item.ProductStock.ProductId, Convert.ToDecimal(item.ProductStock.GstTotal), vendorPurchase.UpdatedBy, vendorPurchase.AccountId);
                    }
                    if (string.IsNullOrEmpty(item.ProductStock.Product.ProductMasterID))
                    {
                        item.ProductStock.Product.Id = item.ProductStock.ProductId;
                        await _productDataAccess.UpdateProductBarcode(item.ProductStock.Product);
                        await _productDataAccess.UpdateProductGSTFromStock(item.ProductStock.Product, true, false, false, false, false); // Added for hsn code update in Product Master
                        continue;
                    }

                    SetMetaData(vendorPurchase, product);
                    var getExistData = await _productDataAccess.GetExistProductMasterById(product);
                    if (getExistData.Id != null)
                    {
                        item.ProductStock.ProductId = getExistData.Id;
                        item.ProductStock.Product.Id = item.ProductStock.ProductId;
                        await _productDataAccess.UpdateProductBarcode(item.ProductStock.Product);
                        continue;
                    }
                    //int NoOfRows = await _productDataAccess.TotalProductCount(product);
                    //var getChar = product.Name.Substring(0, 2);
                    //product.Code = (getChar.ToUpper() + (NoOfRows + 1)).ToString();
                    product = await _productDataAccess.SaveImportProduct(product);

                    //Modified by Sarubala on 09/05/18 to fix global product import productinstance saving 2 times
                    if ((item.ProductStock.Product.RackNo == null || item.ProductStock.Product.RackNo.Trim() == "") && (item.ProductStock.Product.BoxNo == null || item.ProductStock.Product.BoxNo.Trim() == ""))
                    {
                        await _productDataAccess.SaveProductInstance(product);
                    }
                    item.ProductStock.ProductId = product.Id;
                }
            }



            List<VendorPurchaseItem> list = (await _vendorPurchaseDataAccess.Save(GetEnumerable(vendorPurchase), vendorPurchase.TaxRefNo, vendorPurchase.Vendor.LocationType)).ToList();

            //Added by Sabarish For Updating UpdateProductGst
            // list.ForEach(async (x) => { await _productStockManager.UpdateProductGST(x.ProductStock); }); //Commented by Poongodi on 25/07/2017



        }
        public async Task SaveVendorPurchaseItem(List<TempVendorPurchaseItem> vendorPurchaseItemList)
        {
            foreach (var vendorPurchaseItem in vendorPurchaseItemList)
            {
                if (!string.IsNullOrEmpty(vendorPurchaseItem.ProductStock.Product.ProductMasterID))
                {
                    ////var product = new Product
                    ////{
                    ////    Name = vendorPurchaseItem.ProductStock.Product.Name,
                    ////    ProductMasterID = vendorPurchaseItem.ProductStock.Product.ProductMasterID,
                    ////    Manufacturer = vendorPurchaseItem.ProductStock.Product.Manufacturer,
                    ////    Type = vendorPurchaseItem.ProductStock.Product.Type,
                    ////    Category = vendorPurchaseItem.ProductStock.Product.Category,
                    ////    Schedule = vendorPurchaseItem.ProductStock.Product.Schedule,
                    ////    Packing = vendorPurchaseItem.ProductStock.Product.Packing,
                    ////    GenericName = vendorPurchaseItem.ProductStock.Product.GenericName,
                    ////    //GST related fields added by POongodi on 25/07/2017
                    ////    HsnCode = Convert.ToString(vendorPurchaseItem.ProductStock.HsnCode),
                    ////    Igst = vendorPurchaseItem.ProductStock.GstTotal,
                    ////    Cgst = vendorPurchaseItem.ProductStock.GstTotal / 2,
                    ////    GstTotal = vendorPurchaseItem.ProductStock.GstTotal,
                    ////    Sgst = vendorPurchaseItem.ProductStock.GstTotal / 2
                    ////};
                    ////SetMetaData(vendorPurchaseItem, product);
                    ////int NoOfRows = await _productDataAccess.TotalProductCount(product);
                    ////var getChar = product.Name.Substring(0, 2);
                    ////product.Code = (getChar.ToUpper() + (NoOfRows + 1)).ToString();
                    ////product = await _productDataAccess.Save(product);
                    ////vendorPurchaseItem.ProductStock.ProductId = product.Id;

                    var product = new Product
                    {

                        Name = vendorPurchaseItem.ProductStock.Product.Name,
                        ProductMasterID = vendorPurchaseItem.ProductStock.Product.ProductMasterID,
                        Manufacturer = vendorPurchaseItem.ProductStock.Product.Manufacturer,
                        Type = vendorPurchaseItem.ProductStock.Product.Type,
                        Category = vendorPurchaseItem.ProductStock.Product.Category,
                        Schedule = vendorPurchaseItem.ProductStock.Product.Schedule,
                        Packing = vendorPurchaseItem.ProductStock.Product.Packing,
                        GenericName = vendorPurchaseItem.ProductStock.Product.GenericName,
                        HsnCode = Convert.ToString(vendorPurchaseItem.ProductStock.HsnCode),
                        Igst = vendorPurchaseItem.ProductStock.GstTotal,
                        Cgst = vendorPurchaseItem.ProductStock.GstTotal / 2,
                        GstTotal = vendorPurchaseItem.ProductStock.GstTotal,
                        Sgst = vendorPurchaseItem.ProductStock.GstTotal / 2

                    };

                    if (Convert.ToString(vendorPurchaseItem.ProductStock.ProductId) != "")
                    {
                        var result = await _productDataAccess.UpdateProductGSTFromStock(vendorPurchaseItem.ProductStock.Product, true, true, true, true, true);
                    }
                    if (string.IsNullOrEmpty(vendorPurchaseItem.ProductStock.Product.ProductMasterID))
                    {
                        vendorPurchaseItem.ProductStock.Product.Id = vendorPurchaseItem.ProductStock.ProductId;
                        await _productDataAccess.UpdateProductBarcode(vendorPurchaseItem.ProductStock.Product);

                        continue;
                    }

                    SetMetaData(vendorPurchaseItem, product);
                    var getExistData = await _productDataAccess.GetExistProductMasterById(product);
                    if (getExistData.Id != null)
                    {
                        vendorPurchaseItem.ProductStock.ProductId = getExistData.Id;
                        vendorPurchaseItem.ProductStock.Product.Id = vendorPurchaseItem.ProductStock.ProductId;
                        await _productDataAccess.UpdateProductBarcode(vendorPurchaseItem.ProductStock.Product);
                        continue;
                    }
                    //int NoOfRows = await _productDataAccess.TotalProductCount(product);
                    //var getChar = product.Name.Substring(0, 2);
                    //product.Code = (getChar.ToUpper() + (NoOfRows + 1)).ToString();
                    product = await _productDataAccess.SaveImportProduct(product);
                    await _productDataAccess.SaveProductInstance(product);
                    vendorPurchaseItem.ProductStock.ProductId = product.Id;

                }
                else //Update GST Field value to Product Added by Poongodi on 25/07/2017 
                {
                    if (Convert.ToString(vendorPurchaseItem.ProductStock.ProductId) != "")
                    {
                        vendorPurchaseItem.ProductStock.Product.Id = vendorPurchaseItem.ProductStock.ProductId;
                        vendorPurchaseItem.ProductStock.Product.HsnCode = Convert.ToString(vendorPurchaseItem.ProductStock.HsnCode);
                        vendorPurchaseItem.ProductStock.Product.Igst = vendorPurchaseItem.ProductStock.GstTotal;
                        vendorPurchaseItem.ProductStock.Product.Cgst = vendorPurchaseItem.ProductStock.GstTotal / 2;
                        vendorPurchaseItem.ProductStock.Product.GstTotal = vendorPurchaseItem.ProductStock.GstTotal;
                        vendorPurchaseItem.ProductStock.Product.Sgst = vendorPurchaseItem.ProductStock.GstTotal / 2;
                        var result = await _productDataAccess.UpdateProductGSTFromStock(vendorPurchaseItem.ProductStock.Product, true, true, true, true, true);
                        var result1 = await _productDataAccess.ProductStockUpdate(vendorPurchaseItem.ProductStock.ProductId, Convert.ToDecimal(vendorPurchaseItem.ProductStock.GstTotal), vendorPurchaseItem.CreatedBy, vendorPurchaseItem.AccountId);
                    }
                }
            }
            await _vendorPurchaseDataAccess.SaveTempItems(vendorPurchaseItemList);
        }
        public async Task SaveDcVendorPurchaseItem(DcVendorPurchaseItem item)
        {
            if (!string.IsNullOrEmpty(item.ProductStock.Product.ProductMasterID))
            {
                var product = new Product
                {
                    Name = item.ProductStock.Product.Name,
                    ProductMasterID = item.ProductStock.Product.ProductMasterID,
                    Manufacturer = item.ProductStock.Product.Manufacturer,
                    Type = item.ProductStock.Product.Type,
                    Category = item.ProductStock.Product.Category,
                    Schedule = item.ProductStock.Product.Schedule,
                    Packing = item.ProductStock.Product.Packing,
                    GenericName = item.ProductStock.Product.GenericName,
                    //GST related fields added by POongodi on 25/07/2017
                    HsnCode = Convert.ToString(item.ProductStock.HsnCode),
                    Igst = item.ProductStock.Igst,
                    Cgst = item.ProductStock.Cgst,
                    GstTotal = item.ProductStock.GstTotal,
                    Sgst = item.ProductStock.Sgst

                };
                SetMetaData(item, product);
                //int NoOfRows = await _productDataAccess.TotalProductCount(product);
                //var getChar = product.Name.Substring(0, 2);
                //product.Code = (getChar.ToUpper() + (NoOfRows + 1)).ToString();
                product = await _productDataAccess.SaveDC(product);
                item.ProductStock.ProductId = product.Id;
                item.ProductStock.Product.Id = product.Id;
            }
            else //Update GST Field value to Product Added by Poongodi on 25/07/2017 
            {
                if (Convert.ToString(item.ProductStock.ProductId) != "")
                {
                    item.ProductStock.Product.Id = item.ProductStock.ProductId;
                    item.ProductStock.Product.HsnCode = Convert.ToString(item.ProductStock.HsnCode);
                    item.ProductStock.Product.Igst = item.ProductStock.GstTotal;
                    item.ProductStock.Product.Cgst = item.ProductStock.GstTotal / 2;
                    item.ProductStock.Product.GstTotal = item.ProductStock.GstTotal;
                    item.ProductStock.Product.Sgst = item.ProductStock.GstTotal / 2;
                    var result = await _productDataAccess.UpdateProductGSTFromStock(item.ProductStock.Product, true, true, true, true, true);
                    var result1 = await _productDataAccess.ProductStockUpdate(item.ProductStock.ProductId, Convert.ToDecimal(item.ProductStock.GstTotal), item.CreatedBy, item.AccountId);
                }
            }
            await _productDataAccess.UpdateProductRackNo(item.ProductStock.Product);
            await _vendorPurchaseDataAccess.SaveDCItems(item);
        }
        public async Task<List<DcVendorPurchaseItem>> getDCItems(string id, string accId, string instId)
        {
            return await _vendorPurchaseDataAccess.GetDCItems(id, accId, instId);
        }
        public async Task<List<DcVendorPurchaseItem>> getDCItemsDropDown(string id, string dcNo, string accId, string instId)
        {
            return await _vendorPurchaseDataAccess.getDCItemsDropDown(id, dcNo, accId, instId);
        }

        public async Task<int> POCancel(string accId, string instId, string poid, string updatedby)
        {
            return await _vendorPurchaseDataAccess.POCancel(accId, instId, poid, updatedby);
        }
        public async Task<string> removeDcItem(DcVendorPurchaseItem item1)
        {
            return await _vendorPurchaseDataAccess.RemoveDcItem(item1);
        }
        public async Task<DcVendorPurchaseItem> updateDcItem(DcVendorPurchaseItem item)
        {

            await _productDataAccess.UpdateProductRackNo(item.ProductStock.Product);
            return await _vendorPurchaseDataAccess.UpdateDcItem(item);

        }
        public async Task<IEnumerable<VendorPurchaseItem>> GetVendorPurchaseItemByStockId(IEnumerable<string> productStockId)
        {
            return await _vendorPurchaseDataAccess.GetVendorPurchaseItemByStockId(productStockId);
        }
        private IEnumerable<VendorPurchaseItem> GetEnumerable(VendorPurchase vendorPurchase)
        {
            foreach (var item in vendorPurchase.VendorPurchaseItem)
            {
                item.VendorPurchaseId = vendorPurchase.Id;
                item.ProductStock.VendorId = vendorPurchase.VendorId;
                SetMetaData(vendorPurchase, item);
                SetMetaData(vendorPurchase, item.ProductStock);
                yield return item;
            }
        }
        private IEnumerable<DraftVendorPurchaseItem> GetEnumerable1(DraftVendorPurchase draftvendorPurchase)
        {
            foreach (var item in draftvendorPurchase.DraftVendorPurchaseItem)
            {
                item.DraftVendorPurchaseId = draftvendorPurchase.Id;
                SetMetaData(draftvendorPurchase, item);
                yield return item;
            }
        }
        public async Task<PagerContract<VendorPurchase>> ListPager(VendorPurchase data, string instanceId, string accountId)
        {
            var pager = new PagerContract<VendorPurchase>
            {
                // NoOfRows = await _vendorPurchaseDataAccess.Count(data),
                List = await _vendorPurchaseDataAccess.List(data, instanceId, accountId)
            };
            if (pager.List == null || pager.List.Count() == 0)
            {
                pager.NoOfRows = 1;
            }
            else
                pager.NoOfRows = pager.List.FirstOrDefault().PurchaseCount;
            return pager;
        }
        //added by nandhini on 26.09.17
        public virtual Task<IEnumerable<Vendor>> VendorSearchPurchaseListLocal(string vendorName, string accountid, string instanceid)
        {
            var plist = _vendorPurchaseDataAccess.GetVendorPurchaseListLocal(vendorName, accountid, instanceid);
            return plist;
        }
        public async Task<PagerContract<VendorPurchase>> CancelListPager(VendorPurchase data, string instanceId, string accountId)
        {
            var pager = new PagerContract<VendorPurchase>
            {
                // NoOfRows = await _vendorPurchaseDataAccess.Count(data),
                List = await _vendorPurchaseDataAccess.CancelList(data, instanceId, accountId)
            };
            if (pager.List == null || pager.List.Count() == 0)
            {
                pager.NoOfRows = 1;
            }
            else
                pager.NoOfRows = pager.List.FirstOrDefault().PurchaseCount;
            return pager;
        }
        public async Task<List<DraftVendorPurchase>> GetAllDrafts(string InstanceId)
        {
            return await _vendorPurchaseDataAccess.GetAllDrafts(InstanceId);
        }
        public async Task<List<DraftVendorPurchase>> GetDraftsByInstance(string InstanceId)
        {
            return await _vendorPurchaseDataAccess.GetDraftsByInstance(InstanceId);
        }
        public async Task<Product> GetProductGST(string AccountId, string InstanceId, string productName)
        {
            return await _vendorPurchaseDataAccess.GetProductGST(AccountId, InstanceId, productName);
        }
        public async Task<List<DraftVendorPurchaseItem>> DraftItemsByDraftId(string InstanceId, string draftid)
        {
            return await _vendorPurchaseDataAccess.DraftItemsByDraftId(InstanceId, draftid);
        }
        public async Task<VendorPurchase> GetVendorPurchaseData(string id)
        {
            return await _vendorPurchaseDataAccess.BindOrderToVendorPurchase(id);
        }
        public async Task<bool> IsTempPurchaseItemAvail(string productId, string instanceId)
        {
            return await _vendorPurchaseDataAccess.IsTempPurchaseItemAvail(productId, instanceId);
        }
        public async Task<List<TempVendorPurchaseItem>> loadTempVendorPurchaseItem(string productId, string instanceId)
        {
            return await _vendorPurchaseDataAccess.loadTempVendorPurchaseItem(productId, instanceId);
        }
        public async Task<VendorPurchase> GetVendorPurchaseDetails(string id)
        {
            return await _vendorPurchaseDataAccess.GetById(id);
        }
        public async Task<VendorPurchase> EditVendorPurchase(string id)
        {
            return await _vendorPurchaseDataAccess.GetVendorPurchaseSingleData(id);
        }
        public async Task<PurchaseSettings> SaveTaxType(PurchaseSettings data)
        {
            var type = await _vendorPurchaseDataAccess.SaveTaxType(data);
            return type;
        }
        // Added Gavaskar 07-02-2017 Start 
        public async Task<PurchaseSettings> SaveBuyInvoiceDateEditOption(PurchaseSettings data)
        {
            var type = await _vendorPurchaseDataAccess.SaveBuyInvoiceDateEditOption(data);
            return type;
        }
        public async Task<PurchaseSettings> getBuyInvoiceDateEditSetting(string AccountId, string InstanceId)
        {
            var buyInvoiceDateEdit = await _vendorPurchaseDataAccess.getBuyInvoiceDateEditSetting(AccountId, InstanceId);
            return buyInvoiceDateEdit;
        }
        //Added Sarubala 18-04-2017
        public async Task<PurchaseSettings> getBillSeriesType(string accId, string insId)
        {
            return await _vendorPurchaseDataAccess.GetBillSeriesType(accId, insId);
        }
        public async Task<PurchaseSettings> saveBillSeriesType(PurchaseSettings ps)
        {
            return await _vendorPurchaseDataAccess.SaveBillSeriesType(ps);
        }
        //End Sarubala
        // Added Gavaskar 07-02-2017 End 
        public async Task<PurchaseSettings> getTaxtypedata(string AccountId, string InstanceId)
        {
            var taxtype = await _vendorPurchaseDataAccess.getTaxtypedata(AccountId, InstanceId);
            return taxtype;
        }

        //Added by Sarubala on 11/05/18 - start
        public async Task<PurchaseSettings> getInvoiceValueSettings(string AccountId, string InstanceId)
        {
            return await _vendorPurchaseDataAccess.getInvoiceValueSettings(AccountId, InstanceId);
        }
        public async Task<PurchaseSettings> saveInvoiceValueSettings(PurchaseSettings ps)
        {
            return await _vendorPurchaseDataAccess.saveInvoiceValueSettings(ps);
        }

        //Added by Sarubala on 11/05/18 - end
        public async Task<TaxSeriesItem> saveTaxTypeseriesItem(TaxSeriesItem TS)
        {
            return await _vendorPurchaseDataAccess.saveTaxTypeseriesItem(TS);
        }

        public async Task<List<TaxSeriesItem>> getCustomTaxSeriesItems(TaxSeriesItem IS)
        {
            return await _vendorPurchaseDataAccess.getCustomTaxSeriesItems(IS);
        }
        public async Task<ProductSearchSettings> saveSearchType(ProductSearchSettings type1)
        {
            return await _vendorPurchaseDataAccess.SaveSearchType(type1);
        }
        public async Task<string> getSearchType(string accId, string insId)
        {
            return await _vendorPurchaseDataAccess.GetSearchType(accId, insId);
        }
        public async Task<bool> GetAllowDecimalSetting(string accId, string insId)
        {
            return await _vendorPurchaseDataAccess.GetAllowDecimalSetting(accId, insId);
        }
        public async Task<string> GetAllowCompletePurchaseKeyTypeSetting(string accId, string insId)
        {
            return await _vendorPurchaseDataAccess.GetAllowCompletePurchaseKeyTypeSetting(accId, insId);
        }
        public async Task<PurchaseSettings> SaveAllowDecimalSetting(PurchaseSettings ps)
        {
            return await _vendorPurchaseDataAccess.SaveAllowDecimalSetting(ps);
        }
        public async Task<PurchaseSettings> SaveAllowCompletePurchaseKeyType(PurchaseSettings ps)
        {
            return await _vendorPurchaseDataAccess.SaveAllowCompletePurchaseKeyType(ps);
        }

        //Added by Sarubala on 26-09-17
        public async Task<PurchaseSettings> SaveisSortByLowestPriceSetting(PurchaseSettings PS)
        {
            return await _vendorPurchaseDataAccess.SaveisSortByLowestPriceSetting(PS);
        }

        public async Task<VendorPurchase> UpdateVendorPurchaseData(VendorPurchase data, string InstanceId, string AccountId)
        {
            string InvoiceGRNExist = await CheckInvoiceExist(data, InstanceId);
            await ValidateEancode(data); //Added by Poongodi on 01/08/2017
            if (string.IsNullOrEmpty(InvoiceGRNExist))
            {
                var getfinyear = await getFinYear(data); /*Added by Poongodi on 05/04/2017*/
                data.Finyear = getfinyear;

                var vendorPurchase = await _vendorPurchaseDataAccess.Update(data);
                List<VendorPurchaseItem> newItems = new List<VendorPurchaseItem>();
                List<VendorPurchaseItem> oldItems = new List<VendorPurchaseItem>();
                foreach (VendorPurchaseItem item in data.VendorPurchaseItem)
                {
                    //The below calculation handled in js itself based on vendor formula
                    //Direct Value Calculations By Sabarish Start
                    //item.Quantity = item.PackageSize * item.PackageQty;
                    //decimal toatlPrice = (decimal)(item.PackagePurchasePrice * (item.PackageQty - item.FreeQty));
                    //decimal discount = (decimal)(toatlPrice * item.Discount / 100);
                    //decimal priceAfterTax = 0;

                    ////if (item.ProductStock.VAT == null)
                    ////    item.ProductStock.VAT = 0;

                    //if (data.TaxRefNo == 1)
                    //{
                    //    priceAfterTax = (decimal)((toatlPrice - discount) + ((toatlPrice - discount) * item.ProductStock.GstTotal / 100));
                    //    item.ProductStock.CST = 0;
                    //    item.ProductStock.VAT = 0;

                    //    item.PurchasePrice = (priceAfterTax / item.PackageQty) / item.PackageSize;
                    //    item.PurchasePrice = Math.Round((decimal)item.PurchasePrice, 5);

                    //    item.VAT = 0.00M;
                    //   // item.GstValue = item.PurchasePrice * item.GstTotal / 100; // Commneted by Gavaskar 
                    //    item.GstValue = (decimal)(toatlPrice - discount) * item.GstTotal / 100; // Added by Gavaskar
                    //    item.VatValue = 0.00M;
                    //}
                    //else
                    //{
                    //    if (item.ProductStock.CST > 0)
                    //    {
                    //        priceAfterTax = (decimal)((toatlPrice - discount) + ((toatlPrice - discount) * item.ProductStock.CST / 100));
                    //    }
                    //    else
                    //    {
                    //        priceAfterTax = (decimal)((toatlPrice - discount) + ((toatlPrice - discount) * item.ProductStock.VAT / 100));
                    //    }

                    //    item.PurchasePrice = (priceAfterTax / item.PackageQty) / item.PackageSize;
                    //    item.PurchasePrice = Math.Round((decimal)item.PurchasePrice, 5);

                    //    item.VAT = item.ProductStock.VAT;
                    //    item.VatValue = item.PurchasePrice * item.VAT / 100;
                    //    item.GstValue = 0.00M;

                    //}

                    //if (item.Discount == 0)
                    //{
                    //    item.Discount = 0.0M;
                    //}
                    //else
                    //{
                    //    //item.DiscountValue = (item.PurchasePrice - item.VatValue) * (item.Discount / 100); // Commneted by Gavaskar 
                    //    item.DiscountValue = discount; // Added by Gavaskar 
                    //}
                    //Direct Value Calculations By Sabarish End

                    item.PurchasePrice = Math.Round((decimal)item.PurchasePrice, 6);
                    item.VendorPurchaseId = vendorPurchase.Id;
                    SetMetaData(vendorPurchase, item);
                    SetMetaData(vendorPurchase, item.ProductStock);
                    if (item.Action == "I")
                    {
                        newItems.Add(item);
                    }
                    else if (item.Action == "U" && !item.isDeleted)
                    {
                        oldItems.Add(item);
                    }
                    else
                    {
                        //If vendor changed in edit, which has to be updated in productstock
                        if (data.VendorChangedInEdit == true)
                        {
                            await _productStockDataAccess.UpdateProductStock(item.ProductStock);
                        }
                    }
                    if (item.isDeleted)
                    {
                        item.Action = "D";
                        item.Status = 2;
                        oldItems.Add(item);
                        item.ProductCount = data.VendorPurchaseItem.Count(p => p.ProductStock.ProductId == item.ProductStock.ProductId);
                        await inActiveProduct(item, InstanceId, AccountId);
                    }
                }
                await _auditDataAccess.SaveAudit(oldItems);
                data.VendorPurchaseItem = oldItems;
                List<VendorPurchaseItem> list = (await _vendorPurchaseDataAccess.UpdateVendorPurchaseItem(oldItems, data.Vendor.LocationType)).ToList();

                //Added By Sabarish For UpdateProductGSt 
                //list.ForEach(async (x) => { await _productStockManager.UpdateProductGST(x.ProductStock); }); //Commented by Poongodi on 25/07/2017

                await _productDataAccess.UpdateProductBarcode(data.VendorPurchaseItem.ToList());
                if (newItems.Count > 0)
                {
                    data.VendorPurchaseItem = newItems;
                    await SaveVendorPurchaseItem(data);
                    await _vendorOrderManager.updateVendorOrderItem(data);
                    await _auditDataAccess.SaveAudit(newItems);
                }
                await _paymentManager.UpdateVendorPurchasePayment(data);
                return vendorPurchase;
            }
            else
            {
                VendorPurchase obj = new VendorPurchase();
                obj.errorMessage = "Invoice Number Already Exist" + "for this GRN No. " + InvoiceGRNExist;
                return obj;
            }
        }
        public async Task<VendorPurchase> getGRNumber(VendorPurchase vendorPurchase)
        {
            return await _vendorPurchaseDataAccess.GetGRNumber(vendorPurchase);
        }
        public async Task<string> getFinYear(VendorPurchase vendorPurchase)
        {
            return await _vendorPurchaseDataAccess.GetFinYear(vendorPurchase);
        }
        public async Task<string> ValidateInvoice(VendorPurchase data)
        {
            string newInvoice = await _vendorPurchaseDataAccess.CheckUniqueInvoice(data);
            if (!string.IsNullOrEmpty(newInvoice))
            {
                string errorMessage = "Invoice Number Already Exist" + " for this GRN No. " + newInvoice;
                throw new Exception(errorMessage);
            }
            //("Invoice No already exists");
            //throw new Exception($"Invoice No already exists");
            if (data.InvoiceDate.Value.Date > DateTime.Now.Date)
            {
                throw new Exception("Invoice Date cannot be greater than current date");
            }
            return "ok";
        }
        private async Task<string> CheckInvoiceExist(VendorPurchase data, string instanceid)
        {
            string InvoiceGRNExist = await _vendorPurchaseDataAccess.CheckInvoiceExist(data, instanceid);
            if (!string.IsNullOrEmpty(InvoiceGRNExist))
            {
                string errorMessage = "Invoice Number Already Exist" + " for this GRN No. " + InvoiceGRNExist;
                throw new Exception(errorMessage);
            }

            return null;
        }
        public async Task<PurchaseDetails> getPurchaseDetails(ProductStock ps, string name, string instanceid)
        {
            return await _vendorPurchaseDataAccess.getPurchaseDetails(ps, name, instanceid);
        }
        public async Task<VendorPurchase> GetAlternateName(VendorPurchase v)
        {
            return await _vendorPurchaseDataAccess.GetAlternateName(v);
        }
        public async Task<VendorPurchaseItem> getPurchasePrice(string id)
        {
            return await _vendorPurchaseDataAccess.getPurchasePrice(id);
        }
        // Newly Added Gavaskar 21-02-2017 Start
        public async Task<IEnumerable<Vendor>> VendorList(Vendor vendor)
        {
            return await _vendorPurchaseDataAccess.VendorList(vendor);
        }
        // Newly Added Gavaskar 21-02-2017 End
        /// <summary>
        /// Get offline status from Data access
        /// </summary>
        /// <param name="accountId"></param>
        /// <param name="instanceId"></param>
        /// <returns></returns>
        public async Task<Boolean> getOfflineStatus(string accountId, string instanceId)
        {
            return await _vendorPurchaseDataAccess.getOfflineStatus(accountId, instanceId);
        }
        private async Task<bool> ValidateEancode(VendorPurchase data)
        {
            var isDuplicate = await _vendorPurchaseDataAccess.CheckUniqueEancode(data);
            if (isDuplicate != "")
                throw new Exception(isDuplicate);
            return true;
        }
        public async Task<bool> inActiveProduct(VendorPurchaseItem model, string InstanceId, string AccountId)
        {
            if (model.ProductStock.Stock != null)
            {
                model.ProductStock.Stock = model.ProductStock.Stock - (model.PackageQty * model.PackageSize);
                if ((int)model.ProductStock.Stock == 0)
                {
                    model.ProductStock.Status = 2;
                }
                await _vendorPurchaseDataAccess.inActiveProduct(model, InstanceId, AccountId);
                //await _paymentManager.SaveDeletedItem(model);
            }
            return true;
        }
        public async Task<bool> getEnableSelling(string accountId, string instanceId)
        {
            return await _vendorPurchaseDataAccess.getEnableSelling(accountId, instanceId);
        }
        public async Task saveEnableSelling(PurchaseSettings ps)
        {
            await _vendorPurchaseDataAccess.saveEnableSelling(ps);
        }

        // Added by Gavaskar Purchase With Return
        public VendorPurchase RemoveReturnItems(VendorPurchase data)
        {
            return _vendorPurchaseDataAccess.RemoveReturnItems(data);
        }
        // Added by Gavaskar Purchase With Return
        public async Task HandleReturnedItems(VendorPurchaseReturn srdata, string VendorPurchaseId)
        {
            List<VendorPurchaseReturnItem> data = await GetReturnedItems(VendorPurchaseId);
        }
        // Added by Gavaskar Purchase With Return
        private async Task<List<VendorPurchaseReturnItem>> GetReturnedItems(string VendorPurchaseId)
        {
            return await _vendorPurchaseDataAccess.VendorPurchaseReturnItemList(VendorPurchaseId);
        }

        // Wait by Gavaskar Purchase With Return
        private async Task updateDeleteReturnedItem(VendorPurchaseReturn sr, List<VendorPurchaseReturnItem> data)
        {
            foreach (var purchaseReturnItem in data)
            {
                if (sr.VendorPurchaseReturnItem.All(x => x.Id != purchaseReturnItem.Id))
                {
                    await _productStockManager.SalesEditProductStock(purchaseReturnItem.ProductStockId, (purchaseReturnItem.Quantity * (-1)));
                }
            }
        }

        public async Task<bool> checkPaymentstatus(string id)
        {
            return await _vendorPurchaseDataAccess.checkPaymentstatus(id);
        }
        public async Task<IEnumerable<TaxValues>> GetTaxValues(string accountId, string type = null)
        {
            return await _vendorPurchaseDataAccess.GetTaxValues(accountId, type);
        }


        //added by nandhini 30.11.17
        public async Task<TaxValues> saveTaxSeries(TaxValues TaxValues)
        {
            await ValidateTax(TaxValues);
            var Tax = await _vendorPurchaseDataAccess.saveTaxSeries(TaxValues);
            return Tax;
        }
        public async Task<string> ValidateTax(TaxValues data)
        {
            string newTemplate = await _vendorPurchaseDataAccess.CheckUniqueTax(data);
            if (!string.IsNullOrEmpty(newTemplate))
            {
                string errorMessage = "Tax % Already Exist";
                throw new Exception(errorMessage);
            }
            return "ok";
        }
        public async Task<TaxValues> UpdateTaxValuesStatus(TaxValues TaxValues)
        {
            return await _vendorPurchaseDataAccess.UpdateTaxValuesStatus(TaxValues);
        }

        public async Task<bool> DeleteTempVendorPurchaseItem(TempVendorPurchaseItem tempVendorPurchaseItem)
        {
            return await _vendorPurchaseDataAccess.DeleteTempVendorPurchaseItem(tempVendorPurchaseItem);
        }
    }
}
