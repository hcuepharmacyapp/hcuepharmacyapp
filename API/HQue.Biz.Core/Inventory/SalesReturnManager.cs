﻿using System.Collections.Generic;
using System.Threading.Tasks;
using HQue.Contract.Infrastructure.Inventory;
using HQue.DataAccess.Inventory;
using Utilities.Helpers;
using HQue.DataAccess.Master;
using HQue.Contract.Infrastructure.Settings;
using HQue.Contract.Infrastructure;
using System;
using System.Linq;
using HQue.DataAccess.DbModel;
using HQue.Contract.Infrastructure.Accounts;
using HQue.Contract.Infrastructure.Master;

namespace HQue.Biz.Core.Inventory
{
    public class SalesReturnManager : BizManager
    {
        private readonly SalesReturnDataAccess _salesReturnDataAccess;
        private readonly SalesDataAccess _salesDataAccess;
        private readonly ProductDataAccess _productDataAccess;
        private readonly ProductStockManager _productStockManager;
        private readonly PatientDataAccess _patientDataAccess;

        public SalesReturnManager(SalesReturnDataAccess salesReturnDataAccess, SalesDataAccess salesDataAccess,
            ProductDataAccess productDataAccess, ProductStockManager productStockManager, PatientDataAccess patientDataAccess) : base(salesReturnDataAccess)
        {
            _salesReturnDataAccess = salesReturnDataAccess;
            _salesDataAccess = salesDataAccess;
            _productDataAccess = productDataAccess;
            _productStockManager = productStockManager;
            _patientDataAccess = patientDataAccess;
        }

        public async Task<SalesReturn> Save(SalesReturn salesReturn, string AccountId, string InstanceId)
        {
            try
            {

                var Canceltype = "0";
                int updatedSalesReturn = 0;

                /*The below sectionadded by Poongodi on 30/06/2017*/
                if (salesReturn.TaxRefNo == 1)
                {
                    salesReturn = await _salesReturnDataAccess.updateProductStockGST(salesReturn);// Added by Poongodi on 30/06/2017 to update gst in product stock

                }

                //Added by Sarubala on 20-06-18 for loyalty points for sales return edit in sales page - start
                if (salesReturn.Id != null)
                {
                    SalesReturn sreturn = await _salesReturnDataAccess.GetSalesReturnDetails(salesReturn.Id);

                    if (Canceltype == "0" && !string.IsNullOrEmpty(salesReturn.PatientId) && !string.IsNullOrEmpty(salesReturn.PatientMobile))
                    {
                        var getLoyaltyPts = await _salesReturnDataAccess.getSalesReturnLoyaltyPoint(salesReturn.AccountId, sreturn.LoyaltyId, salesReturn.Id);

                        var LoyaltyCustomerCheck = await _salesDataAccess.getLoyaltyPatient(salesReturn.AccountId, salesReturn.PatientId);
                        if (getLoyaltyPts != null && LoyaltyCustomerCheck != null)
                        {
                            decimal CurrentLoyaltyProductPts = 0;
                            foreach (var item in salesReturn.SalesReturnItem)
                            {
                                decimal? previousReturnedQuantity = await _salesDataAccess.GetPreviousSalesRetunrItems(salesReturn.SalesId, item.ProductStockId);
                                decimal? totalReturnedQuantity = previousReturnedQuantity + item.Quantity;
                                item.ReturnedQuantity = item.Quantity - previousReturnedQuantity;

                                if (item.PurchaseQuantity < totalReturnedQuantity && item.Id == null)
                                {
                                    throw new Exception("Return Quantity Not Available");
                                }

                                var itemSelect = getLoyaltyPts.LoyaltyProductPoints.Where(x => x.KindName == item.ProductStock.Product.KindName).ToList();
                                                                
                                if (itemSelect != null && itemSelect.Count > 0 && itemSelect.First().KindOfProductPts > 0)
                                {
                                    item.LoyaltyProductPts = (getLoyaltyPts.LoyaltyPoint * item.TotalAmount * Convert.ToDecimal(itemSelect.First().KindOfProductPts)) / getLoyaltyPts.LoyaltySaleValue;
                                    CurrentLoyaltyProductPts = Convert.ToDecimal(CurrentLoyaltyProductPts) + Convert.ToDecimal(item.LoyaltyProductPts);
                                }
                                else
                                {
                                    item.LoyaltyProductPts = (getLoyaltyPts.LoyaltyPoint * item.TotalAmount) / getLoyaltyPts.LoyaltySaleValue;
                                    CurrentLoyaltyProductPts = Convert.ToDecimal(CurrentLoyaltyProductPts) + Convert.ToDecimal(item.LoyaltyProductPts);
                                }
                            }
                            salesReturn.LoyaltyId = getLoyaltyPts.Id;                            
                            salesReturn.LoyaltyPts = CurrentLoyaltyProductPts;
                            salesReturn.PreviousLoyaltyPts = sreturn.LoyaltyPts;

                            await _salesReturnDataAccess.ModifyLoyaltyPatientUpdate(salesReturn, "edit");
                        }
                    }
                }
                //Added by Sarubala on 20-06-18 for loyalty points for sales return edit in sales page - end

                var salesReturnItemList = new List<SalesReturnItem>();
                foreach (var item in salesReturn.SalesReturnItem)
                {
                    var pr = await _productStockManager.UpdateProductGST(item.ProductStock); // Added by Poongodi on 04/07/2017 to update gst % in product table
                    decimal? previousReturnedQuantity = await _salesDataAccess.GetPreviousSalesRetunrItems(salesReturn.SalesId, item.ProductStockId);
                    decimal? totalReturnedQuantity = previousReturnedQuantity + item.Quantity;
                    updatedSalesReturn = 0;

                    if (item.PurchaseQuantity < totalReturnedQuantity && item.Id == null)
                    {
                        //throw new Exception("Return Quantity Not Available for " + item.ProductStock.Name + "");
                        throw new Exception("Return Quantity Not Available");
                    }

                    //Added for sales along with return Edit by Sarubala
                    //Code to Edit Return Item in Sales Edit - Start
                    else if (item.Id != null && (item.CancelType == 0 || item.CancelType == null))
                    {
                        item.ReturnedQuantity = item.Quantity - previousReturnedQuantity;
                        SalesReturn sreturn = await _salesReturnDataAccess.GetSalesReturnDetails(salesReturn.Id);

                        salesReturn.ReturnNo = sreturn.ReturnNo;
                        salesReturn.ReturnDate = sreturn.ReturnDate;
                        salesReturn.FinyearId = sreturn.FinyearId;

                        var savedReturn1 = await _salesReturnDataAccess.Save(salesReturn);
                        var salesReturnItem1 = new List<SalesReturnItem>();
                        salesReturnItem1.Add(item);
                        savedReturn1.SalesReturnItem = salesReturnItem1;
                        await _salesReturnDataAccess.UpdateStockAndSalesReturnItem(GetEnumerable(savedReturn1), savedReturn1.SalesId);

                        int CustomerPaymentSalesCount1 = await _salesDataAccess.CustomerPaymentSalesCount(salesReturn.SalesId);
                        if (CustomerPaymentSalesCount1 > 0 && salesReturn.IsAlongWithSale != true)
                        {
                            await _salesDataAccess.UpdateCustomerPaymentSalesReturnDetail(salesReturn);
                        }

                        updatedSalesReturn = 1;
                    }
                    //Code to Edit Return Item in Sales Edit - End

                    //Code to Add New Return Item while Sales Edit - Start
                    else if (salesReturn.Id != null && item.Id == null && (item.CancelType == 0 || item.CancelType == null))
                    {
                        var savedReturnTemp = await _salesReturnDataAccess.Save(salesReturn);
                        var salesReturnItemTemp = new List<SalesReturnItem>();
                        salesReturnItemTemp.Add(item);
                        savedReturnTemp.SalesReturnItem = salesReturnItemTemp;
                        await SaveSalesReturnItem(savedReturnTemp);
                        updatedSalesReturn = 1;
                    }
                    //Code to Add New Return Item while Sales Edit - End

                    //End

                    if (item.CancelType == 1)
                    {
                        Canceltype = "1";
                        // Added by Gavaskar 25-11-2017 After converting Sales Order Cancel the Sales Order pending Quantity Start
                        if (item.SalesOrderEstimateId != null && item.SalesOrderProductSelected == 1)
                        {
                            await _salesReturnDataAccess.UpdateSalesOrderCancelQty(item, AccountId, InstanceId);
                        }
                        // Added by Gavaskar 25-11-2017 After converting Sales Order Cancel the Sales Order pending Quantity End
                    }

                    salesReturnItemList.Add(item);
                }
                salesReturn.SalesReturnItem = salesReturnItemList;

                if (updatedSalesReturn == 0)
                {
                    if (salesReturn.ReturnDate == null) // Added by Sarubala on 13-07-17 to change return date
                    {
                        salesReturn.ReturnDate = CustomDateTime.IST;
                    }

                    //Added by Sarubala on 20-06-18 for loyalty points for new return - start
                    if (Canceltype == "0" && !string.IsNullOrEmpty(salesReturn.PatientId) && !string.IsNullOrEmpty(salesReturn.PatientMobile))
                    {
                        var getLoyaltyPts = await _salesDataAccess.getSalesLoyaltyPoint(salesReturn.AccountId, salesReturn.LoyaltyId, salesReturn.SalesId);

                        var LoyaltyCustomerCheck = await _salesDataAccess.getLoyaltyPatient(salesReturn.AccountId, salesReturn.PatientId);
                        if (getLoyaltyPts != null && LoyaltyCustomerCheck != null)
                        {
                            if ((getLoyaltyPts.StartDate <= CustomDateTime.IST.Date && (getLoyaltyPts.EndDate >= CustomDateTime.IST.Date || getLoyaltyPts.EndDate == null || getLoyaltyPts.EndDate == DateTime.MinValue)) || Convert.ToBoolean(salesReturn.IsAlongWithSale) != true)
                            {
                                decimal CurrentLoyaltyProductPts = 0;

                                foreach (var item in salesReturn.SalesReturnItem)
                                {
                                    var itemSelect = getLoyaltyPts.LoyaltyProductPoints.Where(x => x.KindName == item.ProductStock.Product.KindName).ToList();
                                                                        
                                    if (itemSelect != null && itemSelect.Count > 0 && itemSelect.First().KindOfProductPts > 0)
                                    {
                                        item.LoyaltyProductPts = (getLoyaltyPts.LoyaltyPoint * item.TotalAmount * Convert.ToDecimal(itemSelect.First().KindOfProductPts)) / getLoyaltyPts.LoyaltySaleValue;
                                        CurrentLoyaltyProductPts = Convert.ToDecimal(CurrentLoyaltyProductPts) + Convert.ToDecimal(item.LoyaltyProductPts);
                                    }
                                    else
                                    {
                                        item.LoyaltyProductPts = (getLoyaltyPts.LoyaltyPoint * item.TotalAmount) / getLoyaltyPts.LoyaltySaleValue;
                                        CurrentLoyaltyProductPts = Convert.ToDecimal(CurrentLoyaltyProductPts) + Convert.ToDecimal(item.LoyaltyProductPts);
                                    }
                                }

                                salesReturn.LoyaltyId = getLoyaltyPts.Id;                                
                                salesReturn.LoyaltyPts = CurrentLoyaltyProductPts;

                                await _salesReturnDataAccess.ModifyLoyaltyPatientUpdate(salesReturn, "new");
                            }
                            
                            else
                            {
                                salesReturn.LoyaltyPts = 0;
                            }
                        }
                        else
                        {
                            salesReturn.LoyaltyPts = 0;
                        }
                    }
                    //Added by Sarubala on 20-06-18 for loyalty points for new return - end

                    //Added by sarubala to include invoice series in sales return - start
                    if (string.IsNullOrEmpty(salesReturn.InvoiceSeries) || string.IsNullOrWhiteSpace(salesReturn.InvoiceSeries))
                    {
                        salesReturn.InvoiceSeries = salesReturn.Sales.InvoiceSeries != null ? salesReturn.Sales.InvoiceSeries : "";
                    }
                    //Added by sarubala to include invoice series in sales return - end
                    /*Prefix Added by Poongodi on 10/10/2017*/
                    salesReturn.ReturnNo = await GetReturnNo(AccountId, InstanceId, Canceltype, (salesReturn.InvoiceSeries == null ? "" : salesReturn.InvoiceSeries), salesReturn.Prefix); //Updated by Settu
                    var getfinyear = await getFinYear(salesReturn); /*Added by Poongodi on 25/03/2017*/
                    salesReturn.FinyearId = getfinyear;

                    if (Canceltype == "1")
                    {
                        salesReturn.CancelType = 1;

                        //Cancel Return Items Along With Sale - Start

                        var data = await _salesReturnDataAccess.GetSalesReturnAlongWithSales(salesReturn.SalesId);
                        if (data.Count() > 0)
                        {
                            SalesReturn data_1 = data.FirstOrDefault();
                            data_1.CancelType = 2;
                            data_1.PatientMobile = salesReturn.PatientMobile;                            
                            data_1.SalesReturnItem = await _salesReturnDataAccess.GetSalesReturnItem(data_1.Id);

                            //Loyalty point calculation - start 
                            if (!string.IsNullOrEmpty(data_1.PatientId) && !string.IsNullOrEmpty(data_1.PatientMobile))
                            {
                                var getLoyaltyPts = await _salesReturnDataAccess.getSalesReturnLoyaltyPoint(data_1.AccountId, data_1.LoyaltyId, data_1.Id);

                                var LoyaltyCustomerCheck = await _salesDataAccess.getLoyaltyPatient(data_1.AccountId, data_1.PatientId);
                                if (getLoyaltyPts != null && LoyaltyCustomerCheck != null)
                                {
                                    decimal CurrentLoyaltyProductPts = 0;
                                    foreach (var item in data_1.SalesReturnItem)
                                    {
                                        var itemSelect = getLoyaltyPts.LoyaltyProductPoints.Where(x => x.KindName == item.ProductStock.Product.KindName).ToList();
                                        
                                        if (itemSelect != null && itemSelect.Count > 0 && itemSelect.First().KindOfProductPts > 0)
                                        {
                                            item.LoyaltyProductPts = (getLoyaltyPts.LoyaltyPoint * item.TotalAmount * Convert.ToDecimal(itemSelect.First().KindOfProductPts)) / getLoyaltyPts.LoyaltySaleValue;
                                            CurrentLoyaltyProductPts = Convert.ToDecimal(CurrentLoyaltyProductPts) + Convert.ToDecimal(item.LoyaltyProductPts);
                                        }
                                        else
                                        {
                                            item.LoyaltyProductPts = (getLoyaltyPts.LoyaltyPoint * item.TotalAmount) / getLoyaltyPts.LoyaltySaleValue;
                                            CurrentLoyaltyProductPts = Convert.ToDecimal(CurrentLoyaltyProductPts) + Convert.ToDecimal(item.LoyaltyProductPts);
                                        }
                                    }
                                    data_1.LoyaltyId = getLoyaltyPts.Id;                                    
                                    data_1.LoyaltyPts = CurrentLoyaltyProductPts;
                                    await _salesReturnDataAccess.ModifyLoyaltyPatientUpdate(data_1, "return_cancel");
                                }
                            }
                            //Loyalty point calculation - end 

                            var data1 = await _salesReturnDataAccess.Save(data_1);
                            foreach (var dt in data_1.SalesReturnItem)
                            {
                                dt.CancelType = 1;
                                SetMetaData(data1, dt);
                                var sritem = _salesReturnDataAccess.UpdateSalesReturnItemForCancel(dt);
                            }
                            await UpdateExistingVoucher(data_1);
                        }

                        //Cancel Return Items Along With Sale - End

                        //Added by Sarubala on 30-10-17 to inactive sales payments - start
                        if (!string.IsNullOrEmpty(salesReturn.SalesId))
                        {
                            Sales s = new Sales
                            {
                                Id = salesReturn.SalesId,
                                AccountId = AccountId,
                                InstanceId = InstanceId
                            };
                            await _salesDataAccess.InactiveSalesPayments(s);
                        }
                        //Added by Sarubala on 30-10-17 to inactive sales payments - end
                    }


                    //Loyalty point calculation for sales cancel - start 
                    if (!string.IsNullOrEmpty(salesReturn.PatientId) && !string.IsNullOrEmpty(salesReturn.PatientMobile) && Canceltype == "1")
                    {
                        var getLoyaltyPts1 = await _salesDataAccess.getSalesLoyaltyPoint(salesReturn.AccountId, salesReturn.LoyaltyId, salesReturn.SalesId);

                        var LoyaltyCustomerCheck1 = await _salesDataAccess.getLoyaltyPatient(salesReturn.AccountId, salesReturn.PatientId);
                        if (getLoyaltyPts1 != null && LoyaltyCustomerCheck1 != null)
                        {
                            decimal CurrentLoyaltyProductPts1 = 0;
                            foreach (var item1 in salesReturn.SalesReturnItem)
                            {
                                var itemSelect = getLoyaltyPts1.LoyaltyProductPoints.Where(x => x.KindName == item1.ProductStock.Product.KindName).ToList();
                                                                
                                if (itemSelect != null && itemSelect.Count > 0 && itemSelect.First().KindOfProductPts > 0)
                                {
                                    item1.LoyaltyProductPts = (getLoyaltyPts1.LoyaltyPoint * item1.TotalAmount * Convert.ToDecimal(itemSelect.First().KindOfProductPts)) / getLoyaltyPts1.LoyaltySaleValue;
                                    CurrentLoyaltyProductPts1 = Convert.ToDecimal(CurrentLoyaltyProductPts1) + Convert.ToDecimal(item1.LoyaltyProductPts);
                                }
                                else
                                {
                                    item1.LoyaltyProductPts = (getLoyaltyPts1.LoyaltyPoint * item1.TotalAmount) / getLoyaltyPts1.LoyaltySaleValue;
                                    CurrentLoyaltyProductPts1 = Convert.ToDecimal(CurrentLoyaltyProductPts1) + Convert.ToDecimal(item1.LoyaltyProductPts);
                                }
                            }
                            salesReturn.LoyaltyId = getLoyaltyPts1.Id;                           
                            salesReturn.LoyaltyPts = CurrentLoyaltyProductPts1;
                            await _salesReturnDataAccess.ModifyLoyaltyPatientUpdate(salesReturn, "cancel");
                        }
                    }
                    //Loyalty point calculation for sales cancel - end 
                    var savedReturn = await _salesReturnDataAccess.Save(salesReturn);
                    await SaveSalesReturnItem(savedReturn);


                    if (Canceltype == "1")
                    {
                        await _salesDataAccess.UpdateSaleTableDetail(salesReturn.SalesId);
                    }

                    // Added Gavaskar 31/01/2017 Cancel Sales Update Start
                    int cancelSalesCount = await _salesDataAccess.CustomerPaymentCancelSalesCount(salesReturn.SalesId);
                    if (cancelSalesCount > 0)
                    {
                        //await _salesDataAccess.UpdateCustomerPaymentSalesDetail(salesReturn);
                    }
                    else
                    {
                        int CustomerPaymentSalesCount = await _salesDataAccess.CustomerPaymentSalesCount(salesReturn.SalesId);
                        if (CustomerPaymentSalesCount > 0 && salesReturn.IsAlongWithSale != true)
                        {
                            await _salesDataAccess.UpdateCustomerPaymentSalesReturnDetail(salesReturn);
                        }
                    }


                    // Added Gavaskar 31/01/2017 Cancel Sales Update Start

                    return savedReturn;
                }
                return salesReturn;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<SalesReturn> SaveReturnInSale(SalesReturn salesReturn)
        {
            var Canceltype = "0";

            if (salesReturn.ReturnDate == null) // Added by Sarubala on 13-07-17 to change return date
            {
                salesReturn.ReturnDate = CustomDateTime.IST;
            }
            /*Prefix Added by Poongodi on 10/10/2017*/
            salesReturn.ReturnNo = await GetReturnNo(salesReturn.AccountId, salesReturn.InstanceId, Canceltype, salesReturn.InvoiceSeries, salesReturn.Prefix); //Updated by Settu
            var getfinyear = await getFinYear(salesReturn);
            salesReturn.FinyearId = getfinyear;

            //Added by Sarubala on 20-06-18 for loyalty points - start
            if (!string.IsNullOrEmpty(salesReturn.PatientId) && !string.IsNullOrEmpty(salesReturn.PatientMobile))
            {
                var getLoyaltyPts = await _salesDataAccess.getLoyaltyPointSettings(salesReturn.AccountId, salesReturn.InstanceId);

                var LoyaltyCustomerCheck = await _salesDataAccess.getLoyaltyPatient(salesReturn.AccountId, salesReturn.PatientId);
                if (getLoyaltyPts != null && LoyaltyCustomerCheck != null)
                {
                    if (getLoyaltyPts.StartDate <= CustomDateTime.IST.Date && (getLoyaltyPts.EndDate >= CustomDateTime.IST.Date || getLoyaltyPts.EndDate == null || getLoyaltyPts.EndDate == DateTime.MinValue))
                    {
                        decimal CurrentLoyaltyProductPts = 0;
                        foreach (var item in salesReturn.SalesReturnItem)
                        {
                            var itemSelect = getLoyaltyPts.LoyaltyProductPoints.Where(x => x.KindName == item.ProductStock.Product.KindName).ToList();
                                                        
                            if (itemSelect != null && itemSelect.Count > 0 && itemSelect.First().KindOfProductPts > 0)
                            {
                                item.LoyaltyProductPts = (getLoyaltyPts.LoyaltyPoint * item.TotalAmount * Convert.ToDecimal(itemSelect.First().KindOfProductPts)) / getLoyaltyPts.LoyaltySaleValue;
                                CurrentLoyaltyProductPts = Convert.ToDecimal(CurrentLoyaltyProductPts) + Convert.ToDecimal(item.LoyaltyProductPts);
                            }
                            else
                            {
                                item.LoyaltyProductPts = (getLoyaltyPts.LoyaltyPoint * item.TotalAmount) / getLoyaltyPts.LoyaltySaleValue;
                                CurrentLoyaltyProductPts = Convert.ToDecimal(CurrentLoyaltyProductPts) + Convert.ToDecimal(item.LoyaltyProductPts);
                            }                            
                        }

                        salesReturn.LoyaltyId = getLoyaltyPts.Id;
                        salesReturn.LoyaltyPts = CurrentLoyaltyProductPts;

                        await _salesReturnDataAccess.ModifyLoyaltyPatientUpdate(salesReturn, "new");
                    }
                }

            }
            //Added by Sarubala on 20-06-18 for loyalty points - end

            var savedReturn = await _salesReturnDataAccess.Save(salesReturn);
            /*The below sectionadded by Poongodi on 30/06/2017*/
            if (salesReturn.TaxRefNo == 1)
            {
                salesReturn = await _salesReturnDataAccess.updateProductStockGST(salesReturn);// Added by Poongodi on 30/06/2017 to update gst in product stock

            }


            await SaveSalesReturnItem(savedReturn);

            return savedReturn;
        }

        public async Task HandleReturnedItems(SalesReturn srdata, string salesId)
        {
            List<SalesReturnItem> data = await GetReturnedItems(salesId);
            //await DeleteReturnedItem(srdata,data);
            await updateDeleteReturnedItem(srdata, data);
        }

        private async Task<List<SalesReturnItem>> GetReturnedItems(string salesId)
        {
            return await _salesReturnDataAccess.SalesReturnItemList(salesId);
        }

        private async Task DeleteReturnedItem(SalesReturn sr, List<SalesReturnItem> data)
        {
            foreach (var salesReturnItem in data)
            {
                if (sr.SalesReturnItem.All(x => x.Id != salesReturnItem.Id))
                {
                    await _productStockManager.SalesEditProductStock(salesReturnItem.ProductStockId, (salesReturnItem.Quantity * (-1)));
                    await DataAccess.Delete(salesReturnItem, SalesReturnItemTable.Table);
                }

            }
        }
        private async Task updateDeleteReturnedItem(SalesReturn sr, List<SalesReturnItem> data)
        {
            foreach (var salesReturnItem in data)
            {
                if (sr.SalesReturnItem.All(x => x.Id != salesReturnItem.Id))
                {
                    await _productStockManager.SalesEditProductStock(salesReturnItem.ProductStockId, (salesReturnItem.Quantity * (-1)));

                    await _salesReturnDataAccess.updateReturnedItem(salesReturnItem);

                    await InsertSalesReturnItemAudit(salesReturnItem);  //Added by Sarubala to create sales return audit log              

                    //Code added by Sarubala on 10-07-17 for Accounts - start
                    if (!string.IsNullOrEmpty(sr.SalesId) && sr.IsUpdateNetAmt == true)
                    {

                        //Code added by Sarubala to update NetAmount in Sales for (Return Along With Sale) - Start

                        if (sr.IsAlongWithSale == true && sr.IsUpdateNetAmt == true)
                        {
                            var netAmount = await _salesReturnDataAccess.GetSalesNetAmt(sr.SalesId, sr.InstanceId, sr.AccountId);
                            netAmount = netAmount + (decimal)salesReturnItem.FinalPrice;

                            Sales s = new Sales();
                            s.Id = sr.SalesId;
                            s.AccountId = sr.AccountId;
                            s.InstanceId = sr.InstanceId;
                            s.UpdatedBy = sr.UpdatedBy;
                            s.RoundoffNetAmount = Math.Round(netAmount) - netAmount;
                            s.NetAmount = Math.Round(netAmount);

                            await _salesReturnDataAccess.UpdateSalesNetAmt(s);
                        }

                        //Code added by Sarubala to update NetAmount in Sales for (Return Along With Sale) - End

                        int CustomerPaymentSalesCount = await _salesDataAccess.CustomerPaymentSalesCount(sr.SalesId);
                        if (CustomerPaymentSalesCount > 0)
                        {
                            decimal creditAmt = 0;

                            creditAmt = (decimal)((salesReturnItem.Quantity * salesReturnItem.MrpSellingPrice) - (salesReturnItem.Quantity * salesReturnItem.MrpSellingPrice * salesReturnItem.Discount / 100));

                            var Credit = await _salesDataAccess.GetSalesCredit(sr.SalesId);
                            creditAmt = Credit + creditAmt;


                            await _salesReturnDataAccess.UpdateCustomerPaymentSalesReturn(sr, creditAmt);
                        }

                    }
                    //Code added by Sarubala on 10-07-17 for Accounts - end
                }

            }

        }

        public async Task InsertSalesReturnItemAudit(SalesReturnItem data)
        {
            data.Action = "D";
            data.Id = null;
            await _salesReturnDataAccess.Insert(data, SalesReturnItemAuditTable.Table);
        }

        private async Task SaveSalesReturnItem(SalesReturn data)
        {
            try
            {
                await _salesReturnDataAccess.Save(GetEnumerable(data), data.SalesId);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private IEnumerable<SalesReturnItem> GetEnumerable(SalesReturn salesReturn)
        {
            foreach (var item in salesReturn.SalesReturnItem)
            {
                SetMetaData(salesReturn, item);
                item.SalesReturnId = salesReturn.Id;
                yield return item;
            }
        }

        public async Task<SalesReturn> GetSalesReturnDetails(string id)
        {
            var sales = await _salesDataAccess.SalesDetail(id);
            Patient p = new Patient();

            if (!string.IsNullOrEmpty(sales.PatientId))
            {
                p = await _patientDataAccess.GetPatientById(sales.PatientId);
                sales.PatientStatus = p.Status;
            }

            var salesReturn = new SalesReturn();
            salesReturn = BindSalesReturnData(salesReturn, sales);

            sales.SalesItem = await _salesDataAccess.SalesItemList(sales);
            sales.SalesReturn.SalesReturnItem = await _salesDataAccess.SalesReturnItemList(sales);
            foreach (var item in sales.SalesItem)
            {
                item.ProductStock.GstTotal = item.GstTotal == null ? 0 : item.GstTotal; //Added by settu
                var salesReturnItem = new SalesReturnItem
                {
                    ProductStock = item.ProductStock,
                    ProductStockId = item.ProductStock.Id
                };

                var product = await _productDataAccess.GetProductById(item.ProductStock.ProductId);
                salesReturnItem.ProductStock.Product.Name = product.Name;
                salesReturnItem.ProductStock.Product.KindName = product.KindName;
                salesReturnItem.Total = item.Total;
                salesReturnItem.PurchaseQuantity = item.Quantity;
                salesReturnItem.SellingPrice = item.SellingPrice;
                salesReturnItem.MRP = item.MRP;
                // Added by Gavaskar 25-11-2017 After converting Sales Order Cancel the Sales Order pending Quantity Start
                salesReturnItem.SalesOrderEstimateId = item.SalesOrderEstimateId;
                salesReturnItem.SalesOrderProductSelected = item.SalesOrderEstimateType as int? ?? 0;
                // Added by Gavaskar 25-11-2017 After converting Sales Order Cancel the Sales Order pending Quantity End
                if (item.Discount != null)
                {
                    salesReturnItem.Discount = item.Discount;
                    salesReturnItem.originaldiscount = item.Discount;

                }
                else
                    salesReturnItem.Discount = 0;

                foreach (var returnItem in sales.SalesReturn.SalesReturnItem)
                {
                    if (salesReturnItem.ReturnedQuantity == null)
                        salesReturnItem.ReturnedQuantity = 0;

                    //if (salesReturnItem.ProductStock.Id == returnItem.ProductStockId && salesReturnItem.SellingPrice==returnItem.MrpSellingPrice && salesReturnItem.Discount == returnItem.Discount)

                    if (salesReturnItem.ProductStock.Id == returnItem.ProductStockId)
                        salesReturnItem.ReturnedQuantity += returnItem.Quantity;
                    //salesReturnItem.ReturnedQuantity = returnItem.Quantity;
                }


                salesReturn.SalesReturnItem.Add(salesReturnItem);
            }

            salesReturn.PurchasedTotal = sales.PurchasedTotal;
            salesReturn.ReturnItemAmount = sales.SalesItemAmount;
            salesReturn.Sales.NetAmount = sales.NetAmount;
            salesReturn.Sales.RoundoffNetAmount = sales.RoundoffNetAmount;

            return salesReturn;
        }

        public SalesReturn BindSalesReturnData(SalesReturn salesReturn, Sales sales)
        {
            salesReturn.SalesId = sales.Id;
            salesReturn.PatientId = sales.PatientId;
            salesReturn.PatientName = sales.Name;
            salesReturn.PatientMobile = sales.Mobile;
            salesReturn.PatientEmail = sales.Email;
            salesReturn.PatientGender = sales.Gender;
            salesReturn.PatientAge = sales.Age;
            salesReturn.PatientAddress = sales.Address;
            salesReturn.Discount = sales.Discount;
            salesReturn.PaymentType = sales.PaymentType;
            salesReturn.DeliveryType = sales.DeliveryType;
            salesReturn.BillPrint = sales.BillPrint;
            salesReturn.SendEmail = sales.SendEmail;
            salesReturn.SendSms = sales.SendSms;
            salesReturn.Sales.InvoiceNo = sales.InvoiceNo;
            salesReturn.Sales.InvoiceSeries = sales.InvoiceSeries;
            salesReturn.Sales.Prefix = sales.Prefix; //Added by Poongodi on 15/06/2017
            salesReturn.Sales.InvoiceDate = sales.InvoiceDate;
            salesReturn.TaxRefNo = sales.TaxRefNo;
            salesReturn.Sales.CreatedAt = sales.CreatedAt;
            salesReturn.ReturnCharges = 0; //Added by Poongodi
            salesReturn.ReturnChargePercent = 0;
            salesReturn.Patient.Status = sales.PatientStatus;
            salesReturn.LoyaltyId = sales.LoyaltyId;
            salesReturn.RedeemPts = sales.RedeemPts;
            salesReturn.isComposite = (sales.Instance != null) ? sales.Instance.Gstselect : null;

            return salesReturn;
        }


        public async Task<SalesReturn> GetReturnOnlyDetails(string id)
        {
            return await _salesReturnDataAccess.GetReturnOnlyDetails(id);
        }

        public async Task<SalesReturn> UpdateSalesReturn(SalesReturn sr)
        {
            var data = _salesReturnDataAccess.ReturnOnlyItemList(sr.Id);
            await updateDeleteReturnedItem(sr, data.Result);

            await _salesReturnDataAccess.UpdateSalesReturn(sr);
            await UpdateExistingVoucher(sr);
            return sr;
        }

        public async Task<string> GetReturnNo(string AccountId, string InstanceId, string Canceltype, string InvoiceSeries, string sPrefix)
        {
            /*Prefix Added by Poongodi on 10/10/2017*/
            return await _salesReturnDataAccess.GetReturnNo(AccountId, InstanceId, Canceltype, InvoiceSeries, sPrefix);
        }
        public async Task<string> getFinYear(SalesReturn data)
        {
            return await _salesReturnDataAccess.GetFinYear(data);
        }

        public async Task<PagerContract<SalesReturn>> ListPager(SalesReturn data)
        {
            var pager = new PagerContract<SalesReturn>
            {
                NoOfRows = await _salesReturnDataAccess.Count(data),
                List = await _salesReturnDataAccess.List(data)
            };

            return pager;
        }

        public async Task<SalesReturn> getSalesReturnDetails(string id)
        {
            return await _salesReturnDataAccess.GetSalesReturnDetails(id);
        }
        // Added by Settu to capture credit returns from sales
        public async Task<Voucher> saveVoucherNote(SalesReturn data)
        {
            SetMetaData(data, data.Voucher);
            data.Voucher.ReturnId = data.Id;
            data.Voucher = await _salesReturnDataAccess.saveVoucherNote(data.Voucher);
            return data.Voucher;
        }

        public async Task<SalesReturn> UpdateExistingVoucher(SalesReturn data)
        {
            var voucher = await _salesReturnDataAccess.GetExistingVoucher(data.AccountId, data.InstanceId, data.Id);
            if (voucher != null)
            {
                decimal? SalesNetAmount = 0;
                if (data.SalesId != null && data.SalesId != "")
                {
                    var sales = await _salesReturnDataAccess.GetSalesNet(data.SalesId);
                    SalesNetAmount = sales.SalesItemAmount;
                }
                voucher.Amount = data.ReturnTotal - SalesNetAmount;
                if (voucher.Amount < 0)
                    voucher.Amount = 0;
                voucher.UpdatedBy = data.UpdatedBy;
                voucher.OfflineStatus = data.OfflineStatus;
                await _salesReturnDataAccess.UpdateVoucherNote(voucher);
            }
            return data;
        }
        public async Task<List<SalesReturnItem>> GetReturnItemAuditList(string id)
        {
            return await _salesReturnDataAccess.GetReturnItemAuditList(id);
        }
        public async Task<string> GetSalesReturnIdByRetrunNo(int InvoiceSeriesType, string SeriesTypeValue, string InvNo, string accountid, string instanceid)
        {
            return await _salesReturnDataAccess.GetSalesReturnIdByRetrunNo(InvoiceSeriesType, SeriesTypeValue, InvNo, accountid, instanceid);
        }


    }
}
