﻿using System.Threading.Tasks;
using HQue.Contract.Infrastructure;
using HQue.Contract.Infrastructure.Estimates;
using HQue.DataAccess.Inventory.Estimates;
using Microsoft.AspNetCore.Http;

namespace HQue.Biz.Core.Inventory.Estimates
{
    public class EstimateManager : BizManager
    {
        private readonly EstimateSaveManager _estimateSaveManager;
        private new EstimateDataAccess DataAccess => base.DataAccess as EstimateDataAccess;
        public EstimateManager(EstimateDataAccess dataAccess, EstimateSaveManager estimateSaveManager, IHttpContextAccessor httpContextAccessor) : base(dataAccess, httpContextAccessor)
        {
            _estimateSaveManager = estimateSaveManager;
        }

        public async Task<Estimate> Save(Estimate data)
        {
            SetMetaData(data);

            return await _estimateSaveManager.CreateEstimate(data);
        }

        public async Task<Estimate> GetEstimateDetails(string id)
        {
            return await DataAccess.GetById(id);
        }

        public async Task<PagerContract<Estimate>> ListPager(Estimate data)
        {
            var pager = new PagerContract<Estimate>
            {
                NoOfRows = await DataAccess.Count(data),
                List = await DataAccess.List(data)
            };

            return pager;
        }
    }
}
