﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HQue.Biz.Core.Extension;
using HQue.Biz.Core.Master;
using HQue.Biz.Core.Reminder;
using HQue.Biz.Core.Setup;
using HQue.Contract.Infrastructure.Estimates;
using HQue.DataAccess.DbModel;
using HQue.DataAccess.Inventory.Estimates;
using HQue.DataAccess.Master;
using Utilities.Helpers;

namespace HQue.Biz.Core.Inventory.Estimates
{
    public class EstimateSaveManager : BizManager
    {
        private readonly ProductDataAccess _productDataAccess;
        private readonly InstanceSetupManager _instanceSetupManager;
        private readonly DoctorManager _doctorManager;
        private readonly UserReminderManager _userReminderManager;
        private List<EstimateItem> _savedItems;
        private new EstimateDataAccess DataAccess => base.DataAccess as EstimateDataAccess;

        public EstimateSaveManager(EstimateDataAccess dataAccess, ProductDataAccess productDataAccess, InstanceSetupManager instanceSetupManager,
            DoctorManager doctorManager, UserReminderManager userReminderManager) : base(dataAccess)
        {
            _productDataAccess = productDataAccess;
            _instanceSetupManager = instanceSetupManager;
            _doctorManager = doctorManager;
            _userReminderManager = userReminderManager;
        }

        public async Task<Estimate> CreateEstimate(Estimate data)
        {
            await PreSave(data);
            var sales = await Save(data);
            await PostSalesSave(sales);
            return data;
        }
        
        private async Task<Estimate> Save(Estimate data)
        {
            var sales = await DataAccess.Save(data);
            await SaveItem(sales);
            return sales;
        }

        private async Task PreSave(Estimate data)
        {
            if (data.IsNew)
            {
                await PrepareNewSave(data);
            }
            else
            {
                await HandleSoldItems(data);
                await CancelUserReminder(data);
            }
        }

        private async Task CancelUserReminder(Estimate data)
        {
            await _userReminderManager.CancelSalesReminder(data.Id);
        }

        private async Task HandleSoldItems(Estimate data)
        {
            await GetSavedItems(data.Id);
            await DeleteSoldItem(data);
        }

        private async Task DeleteSoldItem(Estimate data)
        {
            foreach (var salesItem in _savedItems)
            {
                if (data.EstimateItem.All(x => x.Id != salesItem.Id))
                    await DataAccess.Delete(salesItem, SalesItemTable.Table);
            }
        }

        private async Task GetSavedItems(string estimateId)
        {
            _savedItems = await DataAccess.ItemList(estimateId);
        }

        private async Task PrepareNewSave(Estimate data)
        {
            data.EstimateNo = await GetCustomInvoiceNo(data.InstanceId);
            data.EstimateDate = CustomDateTime.IST;
        }

        private async Task PostSalesSave(Estimate data)
        {
            if (data.DoctorName != null)
            {
                await _doctorManager.SaveDoctor(data.GetDoctorObject());
            }
            data.Instance = await _instanceSetupManager.GetById(data.InstanceId);
            await GetProduct(data);
            await _userReminderManager.SetSalesReminder(data);
        }

        private async Task SaveItem(Estimate sales)
        {
            await DataAccess.Save(GetEnumerable(sales));
        }

        private IEnumerable<EstimateItem> GetEnumerable(Estimate sales)
        {
            foreach (var item in sales.EstimateItem)
            {
                SetMetaData(sales, item);
                item.EstimateId = sales.Id;
                yield return item;
            }
        }

        private async Task<List<EstimateItem>> GetProduct(Estimate data)
        {
            var list = new List<EstimateItem>();

            foreach (var item in data.EstimateItem)
            {
                SetMetaData(data, item);
                item.EstimateId = data.Id;
                item.ProductStock.Product = await _productDataAccess.GetProductByProductStockId(item.ProductStockId);
                list.Add(item);
            }

            return list;
        }

        private async Task<string> GetCustomInvoiceNo(string instanceId)
        {
            return await DataAccess.GetCustomInvoiceNo(instanceId);
        }
    }
}
