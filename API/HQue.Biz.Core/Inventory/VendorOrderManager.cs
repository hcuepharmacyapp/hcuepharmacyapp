﻿using System.Collections.Generic;
using System.Linq;
using HQue.Biz.Core.Setup;
using HQue.Contract.Infrastructure;
using HQue.Contract.Infrastructure.Inventory;
using HQue.DataAccess.Inventory;
using HQue.DataAccess.Master;
using Utilities.Helpers;
using HQue.Communication.Email;
using HQue.Communication.Sms;
using System.Threading.Tasks;
using HQue.Contract.Infrastructure.UserAccount;
using Microsoft.AspNetCore.Http;
using HQue.Contract.Infrastructure.Master;
using HQue.Contract.Infrastructure.Settings;
using System;
using HQue.Contract.Infrastructure.Setup;
using HQue.DataAccess.DbModel;

namespace HQue.Biz.Core.Inventory
{
    public class VendorOrderManager : BizManager
    {
        private new VendorOrderDataAccess DataAccess => base.DataAccess as VendorOrderDataAccess;

        private VendorOrderDataAccess _vendorOrderDataAccess;
        private readonly VendorDataAccess _vendorDataAccess;
        private readonly ProductDataAccess _productDataAccess;
        private readonly InstanceSetupManager _instanceSetupManager;
        private readonly ConfigHelper _configHelper;
        private readonly SmsSender _smsSender;
        private readonly EmailSender _emailSender;
        private readonly IHttpContextAccessor _httpContextAccessor;

        public VendorOrderManager(VendorOrderDataAccess vendorPurchaseDataAccess, VendorDataAccess vendorDataAccess,
            ProductDataAccess productDataAccess, InstanceSetupManager instanceSetupManager, ConfigHelper configHelper, SmsSender smsSender,
            EmailSender emailSender, IHttpContextAccessor httpContextAccessor) : base(vendorPurchaseDataAccess)
        {
            _vendorOrderDataAccess = vendorPurchaseDataAccess;
            _vendorDataAccess = vendorDataAccess;
            _productDataAccess = productDataAccess;
            _instanceSetupManager = instanceSetupManager;
            _configHelper = configHelper;
            _smsSender = smsSender;
            _emailSender = emailSender;
            _httpContextAccessor = httpContextAccessor;

        }

        //public async Task<IEnumerable<VendorOrder>> Save(IEnumerable<VendorOrder> data)
        //{
        //    foreach (var order in data)
        //    {
        //        //added by nandhini for need to split the product to 100 while ordering on 11/10/2018
        //        int size = 100;
        //        var orderCount = order.VendorOrderItem.Count();
        //        var orderItem = order.VendorOrderItem;
        //        for (int i = 0; i <= orderCount; i = i + size)
        //        {
        //            order.VendorOrderItem = orderItem.GetRange(i, Math.Min(size, orderCount - i));
        //            order.OrderDate = CustomDateTime.IST;
        //            order.OrderId = await GetOrderNo(order.InstanceId, order.Prefix);             /*Prefix added by POongodi on 11/10/2017*/

        //            var getfinyear = await getFinYear(order); /*Added by Poongodi on 26/03/2017*/
        //            order.FinyearId = getfinyear;

        //            if (!_httpContextAccessor.HttpContext.User.HasPermission(UserAccess.Admin))
        //            {
        //                order.PendingAuthorisation = true;
        //            }

                //    var vendorOrder = await _vendorOrderDataAccess.SaveVendorOrder(order); // Altered by Sarubala on 21-08-18 as active indent in offline not coming

        //            //Manual Sync insert process for Online to offline transfer by Sarubala on 30/08/2018
        //            if (!string.IsNullOrEmpty(order.ToInstanceId) && !string.IsNullOrWhiteSpace(order.ToInstanceId))
        //            {
        //                Instance branch = await _instanceSetupManager.GetById(order.ToInstanceId);
        //                bool bToInstanceOffline = false;
        //                if (branch != null)
        //                    bToInstanceOffline = branch.IsOfflineInstalled as bool? ?? false;

        //                if (_configHelper.AppConfig.OfflineMode == false && bToInstanceOffline)
        //                {
        //                    DataAccess.WriteSyncQueueInsert(order, VendorOrderTable.Table, "I", order.ToInstanceId, true);
        //                }
        //            }


        //            order.VendorOrderItem = (await GetProductEnumerable(order)).ToList();
        //            order.VendorOrderItem.ForEach(x =>
        //            {

        //                x.SetExecutionQuery(order.GetExecutionQuery(), order.WriteExecutionQuery);
        //            });
        //            order.Id = vendorOrder.Id;
        //            await SaveVendorOrderItem(order);
        //            order.Vendor.Id = order.VendorId;
        //            order.Vendor = await _vendorDataAccess.GetById(order.Vendor);
        //            order.Instance = await _instanceSetupManager.GetById(order.InstanceId);

        //            if (_httpContextAccessor.HttpContext.User.HasPermission(UserAccess.Admin))
        //            {
        //                SendVendorOrderCommunication(order);
        //            }

        //        }
        //    }

        //    return data;
        //}

        public async Task<IEnumerable<VendorOrder>> Save(IEnumerable<VendorOrder> data)
        {
            foreach (var order in data)
            {
                order.OrderDate = CustomDateTime.IST;
                order.OrderId = await GetOrderNo(order.InstanceId, order.Prefix);             /*Prefix added by POongodi on 11/10/2017*/
            
                var getfinyear = await getFinYear(order); /*Added by Poongodi on 26/03/2017*/
                order.FinyearId = getfinyear;

                if (!_httpContextAccessor.HttpContext.User.HasPermission(UserAccess.Admin))
                {
                    order.PendingAuthorisation = true;
                }
                var vendorOrder = await _vendorOrderDataAccess.SaveVendorOrder(order); // 
               // var vendorOrder = await _vendorOrderDataAccess.Save(order);
                order.VendorOrderItem = (await GetProductEnumerable(order)).ToList();
                order.VendorOrderItem.ForEach(x =>
                           {
                               x.SetExecutionQuery(order.GetExecutionQuery(), order.WriteExecutionQuery);
                           });
                order.Id = vendorOrder.Id;
                await SaveVendorOrderItem(order);
                order.Vendor.Id = order.VendorId;
                order.Vendor = await _vendorDataAccess.GetById(order.Vendor);
                order.Instance = await _instanceSetupManager.GetById(order.InstanceId);

                if (_httpContextAccessor.HttpContext.User.HasPermission(UserAccess.Admin))
                {
                    SendVendorOrderCommunication(order);
                }
            }

            return data;
        }

        public async Task<List<VendorOrder>> SaveBulkOrder(List<VendorOrder> data)
        {
            await _vendorOrderDataAccess.SaveBulkOrder(data);
            return data;
        }
        public async Task<List<VendorOrderItem>> updateVendorOrderItem(List<VendorOrderItem> data)
        {
            foreach (var item in data)
            {
                await _vendorOrderDataAccess.updateOrderItem(item);
            }
            return data;
        }


        public async Task<string> GetOrderNo(string instanceid, string sPrefix)
        {
            return await _vendorOrderDataAccess.GetOrderNo(instanceid, sPrefix);            /*Prefix added by POongodi on 11/10/2017*/
        }
        public async Task<string> getFinYear(VendorOrder data)
        {
            return await _vendorOrderDataAccess.GetFinYear(data);
        }


        //added by nandhini 24.4.17
        public async Task<OrderSettings> GetOrderSettings(string AccId, string InsId)
        {
            return await _vendorOrderDataAccess.GetOrderSettings(AccId, InsId);
        }

        //Added by Sarubala on 24-11-18 - Start      
        public async Task<OrderSettings> GetHeadOfficeOrderSettings(string AccId, string InsId)
        {
            return await _vendorOrderDataAccess.GetHeadOfficeOrderSettings(AccId, InsId);
        }
        public async Task<List<Instance>> GetInstanceList(string AccId)
        {
            return await _vendorOrderDataAccess.GetInstanceList(AccId);
        }

        public async Task<OrderSettings> saveHeadOfficeSettings(OrderSettings settings)
        {
            return await _vendorOrderDataAccess.saveHeadOfficeSettings(settings);
        }

        public async Task<string> GetHeadOfficeVendorId(string AccId)
        {
            return await _vendorOrderDataAccess.GetHeadOfficeVendorId(AccId);
        }

        //Added by Sarubala on 24-11-18 - End

        public async Task<OrderSettings> SaveOrderSettings(OrderSettings data)
        {
            return await _vendorOrderDataAccess.SaveOrderSettings(data);
        }
        //end
        public async Task<ProductStock> getQuantityForProduct(string productid, string instanceid)
        {
            return await _vendorOrderDataAccess.getQuantityForProduct(productid, instanceid);
        }

        public async Task SaveVendorOrderItem(VendorOrder vendorOrder)
        {
            await _vendorOrderDataAccess.Save(GetEnumerable(vendorOrder));
        }

        private IEnumerable<VendorOrderItem> GetEnumerable(VendorOrder vendorOrder)
        {
            foreach (var item in vendorOrder.VendorOrderItem)
            {
                item.VendorOrderId = vendorOrder.Id;
                yield return item;
            }
        }

        public async Task<PagerContract<VendorOrder>> ListPager(VendorOrder data, string instanceid)
        {
            var pager = new PagerContract<VendorOrder>
            {
                NoOfRows = await _vendorOrderDataAccess.Count(data),
                List = await _vendorOrderDataAccess.List(data, instanceid)
            };
            return pager;
        }

        private async Task<IEnumerable<VendorOrderItem>> GetProductEnumerable(VendorOrder vendorOrder)
        {
            var list = new List<VendorOrderItem>();

            foreach (var item in vendorOrder.VendorOrderItem)
            {
                if (!string.IsNullOrEmpty(item.ProductMasterID))
                {
                    var product = new Product
                    {
                        Name = item.ProductName,
                        ProductMasterID = item.ProductMasterID,
                        Manufacturer = item.Manufacturer,
                        Schedule = item.Schedule,
                        GenericName = item.GenericName,
                        Packing = item.Packing,
                        Category = item.Category,
                        AccountId = item.AccountId,
                        InstanceId = item.InstanceId,
                        CreatedBy = item.CreatedBy,
                        UpdatedBy = item.UpdatedBy,
                        CreatedAt = CustomDateTime.IST,
                        UpdatedAt = CustomDateTime.IST
                    };

                    //int NoOfRows = await _productDataAccess.TotalProductCount(product);

                    //var getChar = product.Name.Substring(0, 2);
                    //product.Code = (getChar.ToUpper() + (NoOfRows + 1)).ToString();
                    product = await _productDataAccess.Save(product);
                    item.ProductId = product.Id;
                    item.Product.Name = product.Name;
                }
                /* Else block commented by sumathi on 14-Nov-18 to fix timeout error  */
                //else
                //{
                //    var product = await _productDataAccess.GetProductById(item.ProductId);
                //    item.Product.Name = product.Name;
                //}
                
                list.Add(item);
            }

            return list;
        }

        private void SendVendorOrderCommunication(VendorOrder vendorOrder)
        {
            //Added by Sarubala on 20-11-17 to implement sms
            bool sendSms = Convert.ToBoolean(_vendorOrderDataAccess.GetIsOrderCreateSms(vendorOrder.InstanceId).Result);

            vendorOrder.OrderId = Convert.ToString(vendorOrder.Prefix) + vendorOrder.OrderId; //Prefix added by Poongodi on 21/06/2017

            if (sendSms == true) //Modified by Sarubala on 20-11-17 to implement sms
            {
                //Added by Sarubala on 20-11-17
                SmsLog log1 = new SmsLog();
                log1.SmsDate = DateTime.Today;
                log1.LogType = "Order Create";
                log1.ReceiverName = vendorOrder.Vendor.Name;
                log1.InvoiceNo = vendorOrder.OrderId;
                log1.InvoiceDate = vendorOrder.OrderDate;
                log1.ReferenceId = vendorOrder.Id;


                if (!string.IsNullOrEmpty(vendorOrder.Vendor.Mobile))
                {
                    _smsSender.SendSms(vendorOrder.Vendor.Mobile, 0, log1, _smsSender.VendorOrderBodyBuilder(vendorOrder));
                }
                if ((bool)vendorOrder.Vendor.Mobile1checked)
                {
                    if (!string.IsNullOrEmpty(vendorOrder.Vendor.Mobile1))
                    {
                        _smsSender.SendSms(vendorOrder.Vendor.Mobile1, 0, log1, _smsSender.VendorOrderBodyBuilder(vendorOrder));
                    }

                }
                if ((bool)vendorOrder.Vendor.Mobile2checked)
                {
                    if (!string.IsNullOrEmpty(vendorOrder.Vendor.Mobile2))
                    {
                        _smsSender.SendSms(vendorOrder.Vendor.Mobile2, 0, log1, _smsSender.VendorOrderBodyBuilder(vendorOrder));
                    }
                }
            }

            if (!string.IsNullOrEmpty(vendorOrder.Vendor.Email) && vendorOrder.Vendor.IsSendMail == true) //Email flag added by Poongodi on 07/07/2017
            {
                _emailSender.Send(vendorOrder.Vendor.Email, 2, _emailSender.VendorOrderBodyBuilder(vendorOrder));
            }
        }
        private async Task<bool> SendVendorReOrderSms(VendorOrder vendorOrder, string InstanceId)
        {
            vendorOrder.Instance = await _instanceSetupManager.GetById(InstanceId);
            vendorOrder.OrderId = Convert.ToString(vendorOrder.Prefix) + vendorOrder.OrderId; //Prefix added by Poongodi on 21/06/2017
            if (!string.IsNullOrEmpty(vendorOrder.Vendor.Mobile))
            {
                await _smsSender.Send(vendorOrder.Vendor.Mobile, 0, _smsSender.VendorOrderBodyBuilder(vendorOrder));
            }
            if ((bool)vendorOrder.Vendor.Mobile1checked)
            {
                if (!string.IsNullOrEmpty(vendorOrder.Vendor.Mobile1))
                {
                    await _smsSender.Send(vendorOrder.Vendor.Mobile1, 0, _smsSender.VendorOrderBodyBuilder(vendorOrder));
                }

            }
            if ((bool)vendorOrder.Vendor.Mobile2checked)
            {
                if (!string.IsNullOrEmpty(vendorOrder.Vendor.Mobile2))
                {
                    await _smsSender.Send(vendorOrder.Vendor.Mobile2, 0, _smsSender.VendorOrderBodyBuilder(vendorOrder));
                }
            }
            return true;
        }
        private async Task<bool> SendVendorReOrderMail(VendorOrder vendorOrder, string InstanceId)
        {

            if (!string.IsNullOrEmpty(vendorOrder.Vendor.Email))
            {
                vendorOrder.Instance = await _instanceSetupManager.GetById(InstanceId);
                vendorOrder.OrderId = Convert.ToString(vendorOrder.Prefix) + vendorOrder.OrderId; //Prefix added by Poongodi on 21/06/2017
                _emailSender.Send(vendorOrder.Vendor.Email, 2, _emailSender.VendorOrderBodyBuilder(vendorOrder));
            }
            return true;
        }
        public async Task<PagerContract<VendorOrder>> GetVendorOrderList(VendorOrder vendorOrder)
        {
            var pager = new PagerContract<VendorOrder>
            {
                NoOfRows = await _vendorOrderDataAccess.Count(vendorOrder),
                List = await _vendorOrderDataAccess.GetAuthorisationList(vendorOrder)
            };
            return pager;
        }

        public async Task<List<MissedOrder>> MissedOrderList(MissedOrder model)
        {
            return await _vendorOrderDataAccess.MissedOrderList(model);
        }

        public async Task<string> cancelMissedOrder(string id)
        {
            return await _vendorOrderDataAccess.CancelMissedOrder(id);
        }

        public async Task<VendorOrder> SendAuthoriseOrderData(VendorOrder vendorOrder)
        {
            vendorOrder.Instance = await _instanceSetupManager.GetById(vendorOrder.InstanceId);
            SendVendorOrderCommunication(vendorOrder);
            return await _vendorOrderDataAccess.UpdateAuthorisationOrder(vendorOrder);
        }


        public async Task<List<VendorOrderItem>> getPreviousOrders(string productid, string instanceid)
        {
            return await _vendorOrderDataAccess.getPreviousOrders(productid, instanceid);
        }

        public async Task<List<OrderBasedSales>> GetOrderBasedSales(OrderBasedSales obs)
        {
            return await _vendorOrderDataAccess.GetOrderBasedSales(obs);
        }

        public async Task<List<Vendor>> getVendors(string AccountId, string InstanceId, string vendorname)
        {
            return await _vendorOrderDataAccess.getVendors(AccountId, InstanceId, vendorname);
        }

        public async Task<List<VendorOrderItem>> getVendorOrders(string id, bool PO, string instanceid, string accountid, string toInstance = "")
        {
            return await _vendorOrderDataAccess.getVendorOrders(id, PO, instanceid, accountid, toInstance);
        }

        public async Task<VendorPurchase> updateVendorOrderItem(VendorPurchase vpData)
        {
            return await _vendorOrderDataAccess.updateVendorOrderItem(vpData);
        }



        public async Task<VendorOrder> GetVenodrOrderById(string vendororderid, string AccountId, string instanceid)
        {
            return await _vendorOrderDataAccess.GetVenodrOrderById(vendororderid, AccountId, instanceid);
        }



        public async Task<bool> resendOrderSms(VendorOrder model, string InstanceId)
        {


            return await SendVendorReOrderSms(model, InstanceId);
        }
        public async Task<bool> resendOrderMail(VendorOrder model, string InstanceId)
        {
            return await SendVendorReOrderMail(model, InstanceId);
        }
        public async Task<List<ProductStock>> getProductDetails(string accountId, string instanceId, string[] prodsId, string fromDate, string toDate, int type)
        {
            return await _vendorOrderDataAccess.getProductDetails(accountId, instanceId, prodsId, fromDate, toDate, type);
        }
        //To get head office Id for stock transfer offline sync by Settu 
        public async Task<Instance> getHeadOfficeVendor(string AccountId)
        {
            return await _vendorOrderDataAccess.getHeadOfficeVendor(AccountId);
        }
        public async Task<List<ProductStock>> ReorderByMinMax(string AccountId, string InstanceId)
        {
            return await _vendorOrderDataAccess.ReorderByMinMax(AccountId, InstanceId);
        }
        //Added by Sarubala on 20-11-17
        public async Task<bool> GetIsOrderCreateSms(string InsId)
        {
            return await _vendorOrderDataAccess.GetIsOrderCreateSms(InsId);
        }
    }
}
