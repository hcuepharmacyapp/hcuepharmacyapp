﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using HQue.Contract.Infrastructure.Inventory;
using HQue.DataAccess.Inventory;
using HQue.Contract.Infrastructure;
using HQue.Contract.Infrastructure.Master;
using HQue.Biz.Core.Dashboard;
using HQue.Contract.Base;
using HQue.Contract.External;
using HQue.Contract.Infrastructure.Settings;
using System.Linq;
using HQue.ServiceConnector.Connector;
using Utilities.Helpers;
using HQue.Contract.Infrastructure.Common;
using HQue.DataAccess.Master;

namespace HQue.Biz.Core.Inventory
{
    public class ProductStockManager : BizManager
    {
        private readonly ProductStockDataAccess _productStockDataAccess;
        private readonly ProductDataAccess _productDataAccess;
        private readonly DashboardManager _dashboardManager;
       
        private new StockTransferDataAccess DataAccess => base.DataAccess as StockTransferDataAccess; //Added by Bala
        private readonly ConfigHelper _configHelper;
        public ProductStockManager(ProductStockDataAccess productStockDataAccess, ConfigHelper configHelper, DashboardManager dashboardManager, StockTransferDataAccess dataAccess, ProductDataAccess productDataAccess)
            : base(productStockDataAccess)
        {
            _productStockDataAccess = productStockDataAccess;
            _productDataAccess = productDataAccess;
            _dashboardManager = dashboardManager;
         
            _configHelper = configHelper;
        }
        public async Task<List<ProductStock>> GetInGetInStockItem(Product product, int showExpDateQty = 0, bool blnQtyType = true)
        {
            return await _productStockDataAccess.GetInStockItem(product, showExpDateQty, blnQtyType);
        }
 
        //Added for Brach wise stock display by bala on 1/June/2017
        public async Task<BranchWiseStock> GetBranchWiseStockItem(ProductSearch product, bool? bLowPrice = false)
        {
            return await _productStockDataAccess.GetBranchWiseStock(product, bLowPrice);
        }
        //To Get Other Branch stock from Online Added by Poongodi on 06/06/2017
        public async Task<BranchWiseStock> OnlineBranchWiseStock(ProductSearch entity)
        {
            var sc = new RestConnector();
           var result =  await sc.PostAsyc <BranchWiseStock>(_configHelper.InternalApi.ProductUrl, entity);
            return result ;
        }
        public async Task<List<ProductStock>> getBathcDetails(Product product)
        {
            return await _productStockDataAccess.getBathcDetails(product);
        }
        public async Task<List<ProductStock>> getAllProductBatch(Product product)
        {
            return await _productStockDataAccess.GetAllProductBatch(product);
        }
        public async Task<List<ProductStock>> getProductBatchForReturn(Product product)
        {
            return await _productStockDataAccess.GetProductBatchForReturn(product);
        }
        public async Task<List<ProductStock>> getActiveProductBatch(Product product)
        {
            return await _productStockDataAccess.GetActiveProductBatch(product);
        }
        public async Task<Product> GetProductDetails(Product product)
        {
            return await _productStockDataAccess.GetProductDetails(product);
        }
        public Task<List<ProductStockResult>> GetProductStockByExternalId(string externalId, string productName)
        {
            return _productStockDataAccess.GetProductStockByExternalID(externalId, productName);
        }

        //Newly added on 16-Aug-2017 by Manivannan
        public Task<ProductStockResult[]> GetProductStocksByRequest(ProductSearchRequest data)
        {
            string externalId = data.ExternalID.ToString();
            var result = data.TemplateDetails.Where(template => !string.IsNullOrEmpty(template.Medicine)).Select(async (x) => (await _productStockDataAccess.GetProductStockByExternalIDExactMatch(externalId, x.Medicine))).ToList();
            var returnData = Task.WhenAll(result);
            return returnData;
        }

        public Task<List<ProductStock>> GetInGetInStockItemList(string accountId, string instanceId, string productName,int showExpDateQty)
        {
            return _productStockDataAccess.GetInStockItemList(accountId, instanceId, productName, showExpDateQty);
        }
        //Separate method added by Poongodi on 26/07/2017
        public Task<List<ProductStock>> GetInStockProductForSales(string accountId, string instanceId, string productName)
        {
            return _productStockDataAccess.GetInStockProductForSales(accountId, instanceId, productName);
        }
        
        public Task<List<ProductStock>> GetInAllStockItemList(string productName, string instanceId)
        {
            return _productStockDataAccess.GetInAllStockItemList(productName, instanceId);
        }
        //added for return in sales page
        public Task<List<ProductStock>> GetInAllStockItemListForReturn(string productName, string instanceId)
        {
            return _productStockDataAccess.GetInAllStockItemListForReturn(productName, instanceId);
        }
        //
        public Task<List<ProductStock>> GetAllStockItemList(string productName, string instanceId)
        {
            return _productStockDataAccess.GetAllStockItemList(productName, instanceId);
        }
        public Task<List<ProductStock>> GetAllProductsinProductStock(string productName, string instanceId)
        {
            return _productStockDataAccess.GetAllProductsinProductStock(productName, instanceId);
        }
        public async Task<PagerContractInventory<ProductStock>> ListPager(Product data)
        {

            var pager = new PagerContractInventory<ProductStock>
            {

                NoOfRows = await _productStockDataAccess.Count(data),
                Available = await _productStockDataAccess.AvailableCount(data),
                OnlyAvailable = await _productStockDataAccess.OnlyAvailableCount(data),
                ZeroQty = await _productStockDataAccess.ZeroQty(data),
                Expiry = await _productStockDataAccess.ExpiryStock(data),
                Reorder = await _productStockDataAccess.ReorderItems(data),
                Inactive = await _productStockDataAccess.InactiveCount(data),
                List = await _productStockDataAccess.List(data)
            };
            //if (nGetcount == 1)
            //{
            //    pager.NoOfRows = await _productStockDataAccess.Count(data);
            //    pager.Available = await _productStockDataAccess.AvailableCount(data);
            //    pager.ZeroQty = await _productStockDataAccess.ZeroQty(data);
            //    pager.Expiry = await _productStockDataAccess.ExpiryStock(data);
            //    pager.Reorder = await _productStockDataAccess.ReorderItems(data);
            
            //}
            return pager;
        }
        public async Task<PagerContract<Product>> ProductListPager(Product data)
        {
            var pager = new PagerContract<Product>
            {
                NoOfRows = await _productStockDataAccess.Count(data),
                List = await _productStockDataAccess.ListInventory(data)
            };
            return pager;
        }
        public async Task<List<ProductStock>> GetAllProductStock(Product data)
        {
            return await _productStockDataAccess.GetStockData(data);
        }
        public async Task<ProductStock> UpdateStock(ProductStock data)
        {
            var stockAdjustment = new StockAdjustment
            {
                AdjustedBy = data.CreatedBy,
                AdjustedStock = data.ActualStock - data.Stock,
                ProductStockId = data.Id,
            };
            SetMetaData(data, stockAdjustment);
            await _productStockDataAccess.SaveStockAdjustment(stockAdjustment);
            data.Stock = data.ActualStock;
            var ps = await _productStockDataAccess.Update(data);
            var product = await UpdateProductGST(data);
            return ps;
        }

        //************ COMMENTED FOR DEAD LOCKS By Martin on 12/08/2017 *****************************************************
        //New function added for updating the GST fields of Product table from the ProductStock object
        public async Task<Product> UpdateProductGST(ProductStock productStock)
        {
            Product product = new Product();
            //if (productStock != null)
            //{
            //    if (!string.IsNullOrEmpty(productStock.ProductId))
            //    {
            //        product = await _productDataAccess.GetProductById(productStock.ProductId);
            //    }
            //}

            //if (product != null)
            //{
            //    var isGSTGiven = false;
            //    var isHsn = false;
            //    var isIgst = false;
            //    var isCgst = false;
            //    var isSgst = false;
            //    var isGstTotal = false;

            //    if (string.IsNullOrEmpty(product.HsnCode) && !string.IsNullOrEmpty(productStock.HsnCode))
            //    {
            //        isGSTGiven = true;
            //        isHsn = true;
            //        product.HsnCode = productStock.HsnCode;
            //    }
            //    if (product.Igst == null && productStock.Igst != null)
            //    {
            //        isGSTGiven = true;
            //        isIgst = true;
            //        product.Igst = productStock.Igst;
            //    }
            //    if (product.Cgst == null && productStock.Cgst != null)
            //    {
            //        isGSTGiven = true;
            //        isCgst = true;
            //        product.Cgst = productStock.Cgst;
            //    }
            //    if (product.Sgst == null && productStock.Sgst != null)
            //    {
            //        isGSTGiven = true;
            //        isSgst = true;
            //        product.Sgst = productStock.Sgst;
            //    }
            //    if (product.GstTotal == null && productStock.GstTotal != null)
            //    {
            //        isGSTGiven = true;
            //        isGstTotal = true;
            //        product.GstTotal = productStock.GstTotal;
            //    }

            //    if (isGSTGiven)
            //    {
            //        var result = await _productDataAccess.UpdateProductGSTFromStock(product, isHsn, isIgst, isCgst, isSgst, isGstTotal);
            //    }
            //    //GST% Update productstock details for the account Added byPoongodi on 04/07/2017
            //    var bResult = await (_productDataAccess.ProductStockUpdate(product.Id, Convert.ToDecimal(product.GstTotal), product.UpdatedBy, product.AccountId));
            //}
            return product;
        }

        public async Task<ProductStock> UpdateProductStock(string productStockId, decimal? quantity)
        {
            var productStock = await _productStockDataAccess.GetStockValue(productStockId);
            productStock.transQuantity = quantity;
            productStock.Stock = productStock.Stock + quantity;
            if (productStock.Stock >= 0)
            {
                   await _productStockDataAccess.Update(productStock);
            }
            return productStock;
        }
        public async Task<ProductStock> UpdateProductStock(string productStockId, decimal? quantity, ProductStock pStock)
        {

            var productStock = await _productStockDataAccess.GetStockValue(productStockId);
            productStock.transQuantity = quantity;
            productStock.Stock = productStock.Stock + quantity;
            productStock.SetExecutionQuery(pStock.GetExecutionQuery(), pStock.WriteExecutionQuery);
            if (productStock.Stock >= 0)
            {
                await _productStockDataAccess.Update(productStock);
            }
            return productStock;
        }
        public async Task<List<ProductStock>> saveNewStock(List<ProductStock> stock, string accId, string insId, string userid, bool blnIndent = false)
        {
            var ps= await _productStockDataAccess.SaveNewStock(stock, accId, insId, userid, blnIndent);
            //ps.ForEach(async (x) => { await UpdateProductGST(x); }); Removed and the same method incorporated in SAVENEWSTOCK Method by Poongodi on 25/07/2017 
             return ps;
        }
        // Added Gavaskar inventoryDataUpdate 16-02-2017 Start 
        public async Task<List<ProductStock>> inventoryDataUpdate(List<ProductStock> stock, string accId, string insId, string userid)
        {
            var ps = await _productStockDataAccess.inventoryDataUpdate(stock, accId, insId, userid);
            ps.ForEach(async (x) => { await UpdateProductGST(x); });
            return ps;
            //var stockAdjustment = new StockAdjustment
            //{
            //    AdjustedBy = data.CreatedBy,
            //    AdjustedStock = data.ActualStock - data.Stock,
            //    ProductStockId = data.Id,
            //};
            //SetMetaData(data, stockAdjustment);
            //await _productStockDataAccess.SaveStockAdjustment(stockAdjustment);
            //data.Stock = data.ActualStock;
            //return await _productStockDataAccess.Update(data);
            // return null;
        }
        // Added Gavaskar inventoryDataUpdate 16-02-2017 End 
        public async Task<List<SelfConsumption>> saveStockConsumption(List<SelfConsumption> pstock, string accId, string insId, string userid)
        {
            return await _productStockDataAccess.SaveStockConsumption(pstock, accId, insId, userid);
        }
        public async Task<List<ProductStock>> getNewOpenedInventory(ProductStock ps, string prodId, string InvoiceNo)
        {
            return await _productStockDataAccess.GetNewOpenedInventory(ps, prodId, InvoiceNo);
        }
        public async Task<InventorySettings> SaveSetting(InventorySettings data)
        {
            return await _productStockDataAccess.SaveSetting(data);
        }
        public async Task<InventorySettings> EditInventorySetting(InventorySettings inventorySettings)
        {
            return await _productStockDataAccess.EditInventorySetting(inventorySettings);
        }
        public async Task<ProductStock> UpdateProductInventory(ProductStock model, string filtertype)
        {
            return await _productStockDataAccess.UpdateProductInventory(model, filtertype);
        }
        public async Task<ProductStock> updateTransferProductStockInventory(ProductStock data)
        {
            var ps = await _productStockDataAccess.UpdateTransferProductStockInventory(data);
            return ps;
        }
        public async Task<bool> updateTransferProductInventory(ProductStock data)
        {
            var p = await _productStockDataAccess.UpdateTransferProductInventory(data);
            return p;
        }
        public async Task<ProductStock> UpdateSingleProductStock(ProductStock model)
        {
            return await _productStockDataAccess.UpdateSingleProductStock(model);
        }

        //stockTransferId added by Bala for offline transfer issue
        public async Task<bool> Update(IEnumerable<ProductStock> productStocks, IEnumerable<StockTransferItem> stockTransferId)
        {
            foreach (var productStock in productStocks)
            {
                var productStockExist = await _productStockDataAccess.GetProductStock(productStock);
                if (productStockExist.Count == 1)
                {
                    foreach (var stockItem in productStockExist)
                    {
                        stockItem.Stock += productStock.Stock.Value;
                        await _productStockDataAccess.Update(stockItem);

                        var transferItems = stockTransferId.Select(x => x.ProductStockId = productStock.Id);

                        await DataAccess.UpdateStockTransferToPdtId("", "", productStock.ProductId, "");
                    }
                }
                else
                {
                    var tmpId = await _productStockDataAccess.Save(productStock);

                    var transferItems = stockTransferId.Select (x => x.ProductId == productStock.ProductId  && x.ToInstanceId == productStock.InstanceId);

                   // await DataAccess.UpdateStockTransferToPdtId(transferItems., "", productStock.ProductId);
                }
            }
            return true;
        }

         public Task<List<Product>> TempStockProductList(string productName, string instanceId, string accountId)
        {
            return _productStockDataAccess.TempStockProductList(productName, instanceId, accountId);
        }
        public Task<List<Product>> GetEancodeExistData(string eancode, string instanceId, string accountId)
        {
            return _productStockDataAccess.GetEancodeExistData(eancode, instanceId, accountId);
        }
        public async Task<ProductStock> SalesEditProductStock(string productStockId, decimal? quantity)
        {
            var productStock = await _productStockDataAccess.GetStockValue(productStockId);
            productStock.Stock = productStock.Stock + quantity;
            if (productStock.Stock >= 0)
            {
                //productStock.isSaleEdit = true;
                await _productStockDataAccess.Update(productStock);
            }
            return productStock;
        }
        //Added by Settu on 04/07/17 for physical stock verification popup implementation
        public Task<List<PhysicalStockHistory>> getPhysicalStockProducts(string AccountId, string InstanceId)
        {
            return _productStockDataAccess.getPhysicalStockProducts(AccountId, InstanceId);
        }

        public Task<bool> updatePhysicalStockHistory(List<PhysicalStockHistory> list)
        {
            return _productStockDataAccess.updatePhysicalStockHistory(list);
        }

        public async Task<InventorySettings> UpdatePhysicalStockPopupSettings(InventorySettings data)
        {
            return await _productStockDataAccess.UpdatePhysicalStockPopupSettings(data);
        }
        /// <summary>
        /// Closing Stock scheduler added by Poongodi on 20/10/17 
        /// </summary>
        /// <param name="sAccountId"></param>
        /// <param name="sInstanceId"></param>
        /// <returns></returns>
        public async Task<bool> ClosingStockScheduler(string sAccountId, string sInstanceId,string sUserId)
        {
            return await _productStockDataAccess.ClosingStockScheduler(  sAccountId, sInstanceId, sUserId);
        }

        public Task<bool> updateBarcodeProfile(BarcodeProfile data)
        {
            return _productStockDataAccess.updateBarcodeProfile(data);
        }

        public async Task<List<BarcodeProfile>> getBarcodeProfileList(string name)
        {
            return await _productStockDataAccess.getBarcodeProfileList(name);
        }

        public Task<bool> updateBarcodePrnDesign(BarcodePrnDesign data)
        {
            return _productStockDataAccess.updateBarcodePrnDesign(data);
        }

        public async Task<List<BarcodePrnDesign>> getBarcodePrnDesignList(string name)
        {
            return await _productStockDataAccess.getBarcodePrnDesignList(name);
        }

        public async Task<List<ProductStock>> generateBarcode(List<ProductStock> productStock, string BarcodeProfileId)
        {
            return await _productStockDataAccess.generateBarcode(productStock, BarcodeProfileId);
        }

        public async Task<string> GetProductStockInstanceId()
        {
            return await _productStockDataAccess.GetProductStockInstanceId();
        }
		// Added by Gavaskar Kind Of Product Master Settings Start 14-12-2017
        public async Task<IEnumerable<KindProductMaster>> GetKindOfProductList(string accountId, string type = null)
        {
            return await _productStockDataAccess.GetKindOfProductList(accountId, type);
        }
        // Added by Gavaskar Kind Of Product Master Settings End 14-12-2017
    }
}
