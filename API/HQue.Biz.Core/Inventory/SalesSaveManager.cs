﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HQue.Biz.Core.Master;
using HQue.Biz.Core.Reminder;
using HQue.Biz.Core.Setup;
using HQue.Communication.Email;
using HQue.Communication.Sms;
using HQue.Contract.Infrastructure.Inventory;
using HQue.DataAccess.DbModel;
using HQue.DataAccess.Inventory;
using HQue.DataAccess.Master;
using Utilities.Helpers;
using HQue.Contract.Infrastructure.Communication;
using System;
using HQue.Biz.Core.Extension;
using HQue.Contract.Infrastructure.Master;
using HQue.Biz.Core.Leads;
using System.Net.NetworkInformation;
using HQue.Contract.Infrastructure;
using HQue.Contract.Infrastructure.Settings;

namespace HQue.Biz.Core.Inventory
{
    public class SalesSaveManager : BizManager
    {
        private readonly ProductDataAccess _productDataAccess;
        private readonly InstanceSetupManager _instanceSetupManager;
        private readonly DoctorManager _doctorManager;
        private readonly UserReminderManager _userReminderManager;
        private readonly SmsSender _smsSender;
        private readonly EmailSender _emailSender;
        private readonly ConfigHelper _configHelper;
        private readonly ProductStockManager _productStockManager;
        private readonly PatientManager _patientManager;
        private readonly LeadsManager _LeadsManager;
        //List<SalesItem> _soldItems;
        private new SalesDataAccess DataAccess => base.DataAccess as SalesDataAccess;

        public SalesSaveManager(SalesDataAccess dataAccess, ProductDataAccess productDataAccess, InstanceSetupManager instanceSetupManager,
            DoctorManager doctorManager, UserReminderManager userReminderManager, SmsSender smsSender,
            EmailSender emailSender, ConfigHelper configHelper, ProductStockManager productStockManager, PatientManager patientManager, LeadsManager leadsManager) : base(dataAccess)
        {
            _productDataAccess = productDataAccess;
            _instanceSetupManager = instanceSetupManager;
            _doctorManager = doctorManager;
            _userReminderManager = userReminderManager;
            _smsSender = smsSender;
            _emailSender = emailSender;
            _configHelper = configHelper;
            _productStockManager = productStockManager;
            _patientManager = patientManager;
            _LeadsManager = leadsManager;
        }

        public async Task<Sales> SaveBulkData(Sales data)
        {
            await DataAccess.SaveBulkData(data);
            if (data.SaveFailed && data.ErrorLog.ErrorMessage.ToLower() != "stock validation failed" && NetworkInterface.GetIsNetworkAvailable())
            {
                var txtEmail = $@"<p>Dear Team,<br/><br/>Customer is facing below issue, please look into this.<br/>
                                <br/><B>Issue details:</B><br/>
                                <br/><B>Mode:</B> { (_configHelper.AppConfig.OfflineMode ? "Offline" : "Online") }
                                <br/><B>Branch Id:</B> { data.InstanceId }
                                <br/><B>Branch Name:</B> { data.Instance.Name }                                
                                <br/><B>Transaction Type:</B> Sales
                                <br/><br/><B>Error Message:</B> { data.ErrorLog.ErrorMessage }
                                <br/><B>Error Stack Trace:</B> { data.ErrorLog.ErrorStackTrace }
                                <br/><br/><br/>Thank You!</p><p>Powered By hCue</p>";

                _emailSender.Send("pharmacytech@myhcue.com", 19, txtEmail);
            }
            return data;
        }
        public async Task<Sales> CreateSale(Sales data, DateTime invoiceDate, bool fromNewScreen)
        {

            var Id = await PostSalesSave(data); //Changed by Violet for PatientId added in sale table

            if (!string.IsNullOrEmpty(data.PatientId))
            {
                if (data.PatientId.Length < 32)
                {
                    data.PatientId = "";
                }
            }
            if (string.IsNullOrEmpty(data.PatientId))
            {
                data.PatientId = Id;
            }
            if (Id == "false")
            {
                data.isEmpID = true;
                return data;
            }
            await PreSalesSave(data, invoiceDate); //Changed by Poongodi on 10/08/2017
            var sales = await SaveSales(data, fromNewScreen);

            return sales;
        }
        public async Task SendSms(CustomerSms customerSms, Sales data)
        {
            //Added by Sarubala on 20-11-17
            bool sendSms = Convert.ToBoolean(DataAccess.GetCustomerBulkSmsSetting(data.InstanceId).Result);

            //Commented by Annadurai
            //data.Instance = await _instanceSetupManager.GetById(data.InstanceId);
            var customers = customerSms.Customers.GroupBy(x => x.Mobile).Select(x => x.FirstOrDefault());

            if (sendSms == true) //Modified by Sarubala on 20-11-17
            {
                foreach (var customer in customers)
                {
                    //Commented by Annadurai
                    //await _smsSender.Send(customer.Mobile, 0, _smsSender.CustomerBodyBuilder_Setting(customer.Name, customerSms.SmsContent, data));
                    SmsLog log1 = new SmsLog();
                    log1.SmsDate = DateTime.Today;
                    log1.LogType = "Customer Bulk SMS";
                    log1.ReceiverName = customer.Name;
                    log1.ReferenceId = customer.Id;
                    log1.InvoiceDate = DateTime.Now;

                    await _smsSender.SendSms(customer.Mobile, 0, log1, _smsSender.CustomerBodyBuilder_Setting(string.IsNullOrEmpty(customer.Name) ? "Customer" : customer.Name, customerSms.SmsContent, data));
                }
            }
            else
            {
                throw new Exception("Message Not sent");
            }

        }
        private async Task<Sales> SaveSales(Sales data, bool fromNewScreen)
        {
            //Added by Sarubala on 28-10-17

            string previousPaymentType = "";
            if (!string.IsNullOrEmpty(data.Id))
            {
                previousPaymentType = await DataAccess.GetSalesPaymentType(data);
            }

            if (string.IsNullOrEmpty(data.Id))
            {
                data.BankDeposited = false;
                data.AmountCredited = false;
            }

            if (data.DoctorName != null)
            {
                var docid = await _doctorManager.SaveDoctor(data.GetDoctorObject());
                if (docid != "")
                {
                    data.DoctorId = docid;
                }
            }
            var getfinyear = await getFinYear(data); /*Added by Poongodi on 26/03/2017*/
            data.FinyearId = getfinyear;

            var sales = data;

            // Added by Gavaskar Loyalty Points 12-12-2017 Start
            decimal CurrentLoyaltyProductPts = 0.00M;
            var LoyaltyCustomerCheck = await DataAccess.getLoyaltyPatient(data.AccountId, data.PatientId);
            if (!sales.SalesEditMode)
            {
                var getLoyaltyPts = await DataAccess.getLoyaltyPointSettings(data.AccountId, data.InstanceId);

                if (getLoyaltyPts != null && LoyaltyCustomerCheck != null)
                {
                    if (Convert.ToDecimal(getLoyaltyPts.MinimumSalesAmt) <= data.NetAmount)
                    {
                        if (getLoyaltyPts.StartDate <= CustomDateTime.IST.Date && (getLoyaltyPts.EndDate >= CustomDateTime.IST.Date || getLoyaltyPts.EndDate == null || getLoyaltyPts.EndDate == DateTime.MinValue))
                        {
                            foreach (var salesItem in data.SalesItem)
                            {
                                var itemSelect = getLoyaltyPts.LoyaltyProductPoints.Where(x => x.KindName == salesItem.ProductStock.Product.KindName).ToList();

                                decimal itemAmt = Convert.ToDecimal(salesItem.TotalAmount) - Convert.ToDecimal(salesItem.RedeemAmtPerItem);
                                if (itemSelect != null && itemSelect.Count > 0 && itemSelect.First().KindOfProductPts > 0)
                                {
                                    salesItem.LoyaltyProductPts = (getLoyaltyPts.LoyaltyPoint * itemAmt * Convert.ToDecimal(itemSelect.First().KindOfProductPts)) / getLoyaltyPts.LoyaltySaleValue;
                                    CurrentLoyaltyProductPts = Convert.ToDecimal(CurrentLoyaltyProductPts) + Convert.ToDecimal(salesItem.LoyaltyProductPts);
                                }
                                else
                                {
                                    salesItem.LoyaltyProductPts = (getLoyaltyPts.LoyaltyPoint * itemAmt) / getLoyaltyPts.LoyaltySaleValue;
                                    CurrentLoyaltyProductPts = Convert.ToDecimal(CurrentLoyaltyProductPts) + Convert.ToDecimal(salesItem.LoyaltyProductPts);
                                }
                            }
                        }
                    }
                }

                data.LoyaltyPts = CurrentLoyaltyProductPts;

                if (LoyaltyCustomerCheck != null)
                {
                    await DataAccess.LoyaltyPatientUpdate(data, data.AccountId, data.InstanceId, data.Name, data.Mobile);
                }


            }
            else
            {
                var getLoyaltyPts = await DataAccess.getSalesLoyaltyPoint(data.AccountId, data.LoyaltyId, data.Id);

                if (getLoyaltyPts != null && LoyaltyCustomerCheck != null)
                {
                    if (Convert.ToDecimal(getLoyaltyPts.MinimumSalesAmt) <= data.NetAmount)
                    {
                        foreach (var salesItem in data.SalesItem)
                        {
                            var itemSelect = getLoyaltyPts.LoyaltyProductPoints.Where(x => x.KindName == salesItem.ProductStock.Product.KindName).ToList();

                            decimal itemAmt = Convert.ToDecimal(salesItem.TotalAmount) - Convert.ToDecimal(salesItem.RedeemAmtPerItem);
                            if (itemSelect != null && itemSelect.Count > 0 && itemSelect.First().KindOfProductPts > 0)
                            {
                                salesItem.LoyaltyProductPts = (getLoyaltyPts.LoyaltyPoint * itemAmt * Convert.ToDecimal(itemSelect.First().KindOfProductPts)) / getLoyaltyPts.LoyaltySaleValue;
                                CurrentLoyaltyProductPts = Convert.ToDecimal(CurrentLoyaltyProductPts) + Convert.ToDecimal(salesItem.LoyaltyProductPts);
                            }
                            else
                            {
                                salesItem.LoyaltyProductPts = (getLoyaltyPts.LoyaltyPoint * itemAmt) / getLoyaltyPts.LoyaltySaleValue;
                                CurrentLoyaltyProductPts = Convert.ToDecimal(CurrentLoyaltyProductPts) + Convert.ToDecimal(salesItem.LoyaltyProductPts);
                            }
                        }
                    }
                }

                data.LoyaltyPts = CurrentLoyaltyProductPts;
                if (LoyaltyCustomerCheck != null)
                {
                    await DataAccess.ModifyLoyaltyPatientUpdate(data, data.AccountId, data.InstanceId, data.Name, data.Mobile);
                }

            }
                        
            // Added by Gavaskar Loyalty Points 12-12-2017 End

            if (data.WriteExecutionQuery)
            {
                sales = await DataAccess.Save(data);
            }
            else
            {
                sales = await DataAccess.SaveSale(data);
            }
            sales.IsPartial = false;
            sales.IsCreated = true;

            await DataAccess.Save(GetEnumerable(sales));

            if (!data.WriteExecutionQuery)
            {
                var notCreatedItems = sales.SalesItem.Where(x => x.IsCreated == false).Count();
                if (notCreatedItems > 0)
                {
                    //var saleItems = sales.SalesItem;
                    var saleItems = await DataAccess.GetQtyBySalesId(sales.Id);
                    sales.SalesItem.ForEach(x =>
                    {
                        var iItem = saleItems.Where(y => y.Id == x.Id).FirstOrDefault();
                        if (iItem != null)
                            x.Quantity = iItem.Quantity;
                    });
                    sales.IsPartial = true;
                    if (sales.SalesItem.Count() != notCreatedItems)
                    {
                        await DataAccess.UpdateSalesByItems(sales);
                    }
                    else
                    {
                        sales.IsCreated = false;
                        await DataAccess.DeleteSale(sales);
                        return sales;
                    }
                }
            }


            //await ValidateAndReverseSale(sales);

            await DataAccess.SaveSalesItemAudit(GetEnumerable(sales));

            //if (fromNewScreen)  //Altered by Sarubala on 20-09-17
            //{
            //Sales Payments starts
            IEnumerable<SalesPayment> existingSalesPayments = null;

            if (!string.IsNullOrEmpty(data.Id))
            {
                existingSalesPayments = await DataAccess.GetSalesPayments(data.AccountId, data.InstanceId, data.Id);
                if (existingSalesPayments.Count() > 0)
                {
                    existingSalesPayments.ToList().ForEach(x => { x.SetExecutionQuery(sales.GetExecutionQuery(), sales.WriteExecutionQuery); });
                }
            }

            //var sales = await DataAccess.Save(data);
            var salesPayments = data.SalesPayments;

            salesPayments.ForEach(x => { x.SalesId = sales.Id; x.AccountId = sales.AccountId; x.InstanceId = sales.InstanceId; x.isActive = true; x.CreatedBy = sales.CreatedBy; x.UpdatedBy = sales.UpdatedBy; x.SetExecutionQuery(sales.GetExecutionQuery(), sales.WriteExecutionQuery); }); //Altered by Sarubala on 20-09-17

            var salesPayment = await DataAccess.SaveSalesPayment(salesPayments, existingSalesPayments);

            //Added by Sarubala on 28-10-17
            if (previousPaymentType != data.CashType && previousPaymentType == "MultiplePayment" && existingSalesPayments.Count() > 0)
            {
                await DataAccess.InactiveSalesPayments(data);
            }

            //Sales Payment ends
            //}  //Altered by Sarubala on 20-09-17
            await SaveSalesItem(sales);
            return sales;
        }
        //The following method added by Poongodi to get Finyear data on 26/03/2017
        /// <summary>
        /// Get finyear
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public async Task<string> getFinYear(Sales data)
        {
            return await DataAccess.GetFinYear(data);
        }
        private async Task PreSalesSave(Sales data, DateTime invoiceDate)
        {
            if (data.IsNew)
            {
                data.InvoiceDate = invoiceDate;
                //await PrepareNewSale(data, invoiceDate);
            }
            else
            {
                await HandleSoldItems(data);
                await CancelUserReminder(data);
            }
        }

        private async Task CancelUserReminder(Sales data)
        {
            await _userReminderManager.CancelSalesReminder(data);
        }

        private async Task HandleSoldItems(Sales data)
        {
            await GetSoldItems(data);
            if (!data.WriteExecutionQuery)
            {
                await ReverseSale(data);
            }
            await DeleteSoldItem(data);
        }

        private async Task DeleteSoldItem(Sales data)
        {
            foreach (var salesItem in data._soldItems)
            {
                if (data.SalesItem.All(x => x.Id != salesItem.Id))
                {
                    if (salesItem.WriteExecutionQuery)
                    {
                        DataAccess.WriteDeleteExecutionQuery(salesItem, SalesItemTable.Table);
                    }
                    else
                    {
                        await DataAccess.Delete(salesItem, SalesItemTable.Table);
                    }

                    salesItem.Action = "D";
                    salesItem.Id = null;
                    salesItem.CreatedBy = data.CreatedBy;
                    salesItem.UpdatedBy = data.UpdatedBy;
                    if (salesItem.WriteExecutionQuery)
                    {
                        DataAccess.SetInsertMetaData(salesItem, SalesItemAuditTable.Table);
                        DataAccess.WriteInsertExecutionQuery(salesItem, SalesItemAuditTable.Table);
                    }
                    else
                    {
                        await DataAccess.Insert(salesItem, SalesItemAuditTable.Table);
                    }
                }

            }
        }

        private async Task ReverseSale(Sales data)
        {
            foreach (var salesItem in data._soldItems)
            {
                //await _productStockManager.UpdateProductStock(salesItem.ProductStockId, salesItem.Quantity); // by San 10-04-2017
                await _productStockManager.SalesEditProductStock(salesItem.ProductStockId, salesItem.Quantity);
            }
        }

        private async Task GetSoldItems(Sales data)
        {
            data._soldItems = await DataAccess.SalesItemList(data);
            foreach (var item in data._soldItems)
            {
                item.SetExecutionQuery(data.GetExecutionQuery(), data.WriteExecutionQuery);
            }
        }

        private async Task PrepareNewSale(Sales data, DateTime invoiceDate)
        {
            data.InvoiceDate = invoiceDate;
            if (data.InvoiceSeries != "MAN")
            {
                /*Prefix Added by Poongodi on 10/10/2017*/
                data.InvoiceNo = await GetCustomInvoiceNo(data.InstanceId, data.InvoiceSeries, invoiceDate, Convert.ToString(data.Prefix));
            }


        }
        // Removed Autosave by Violet
        private async Task<string> PostSalesSave(Sales sales)
        {
            // if (sales.DoctorName != null)
            // {
            //     await _doctorManager.SaveDoctor(sales.GetDoctorObject());
            // }

            Patient patient = new Patient();
            patient.AccountId = sales.AccountId;
            patient.InstanceId = sales.InstanceId;
            patient.Name = sales.Name;
            patient.Mobile = sales.Mobile;
            patient.Id = sales.PatientId;
            patient.EmpID = sales.EmpID;
            patient.CreatedBy = sales.CreatedBy;
            patient.UpdatedBy = sales.UpdatedBy;
            patient.LocationType = 1;
            patient.Status = 1;
            patient.SetExecutionQuery(sales.GetExecutionQuery(), sales.WriteExecutionQuery);

            var Id = "";
            if (patient.Id == null || patient.Id == "")
                Id = await _patientManager.autoSavepatient(patient);
            if (Id != "false")
            {
                if (sales.leadsProductSelected == true)
                {
                    HQue.Contract.Infrastructure.Leads.Leads lead = new Contract.Infrastructure.Leads.Leads();

                    lead.Id = sales.leadId;
                    lead.LeadStatus = "ACPH";
                    lead.LocalLeadStatus = "CMPT";
                    lead.InstanceId = sales.InstanceId;
                    lead.AccountId = sales.AccountId;
                    lead.CreatedBy = sales.CreatedBy;
                    lead.UpdatedBy = sales.UpdatedBy;
                    lead.SetExecutionQuery(sales.GetExecutionQuery(), sales.WriteExecutionQuery);

                    await _LeadsManager.UpdateStatus(lead);


                }
                sales.Instance = await _instanceSetupManager.GetById(sales.InstanceId);
                await GetProduct(sales);
                await _userReminderManager.SetSalesReminder(sales);

                //if (sales.OfflineStatus != true)
                //{
                //    await SendSaleCommunication(sales);
                //}

                //bool internet = NetworkInterface.GetIsNetworkAvailable();

                //if (internet == true)
                //{
                //    await SendSaleCommunication(sales);
                //}
            }
            return Id;

        }

        private async Task SaveSalesItem(Sales sales)
        {
            //await DataAccess.Save(GetEnumerable(sales)); // Moved to a different place

            if (sales.PaymentType == "Credit" && sales.Credit != null && sales.Credit != 0)
            {
                sales.PaymentType = "Cash";
                await DataAccess.SavetoCustomerPayment(sales);
            }
            else if (sales.PaymentType == "Multiple" && sales.SalesPayments.Count > 0)
            {
                var res = sales.SalesPayments.Where(x => x.PaymentInd == 4).ToList();
                if (res.Count > 0 && res.First().Amount > 0)
                {
                    sales.Credit = res.First().Amount;
                    await DataAccess.SavetoCustomerPayment(sales);
                }
                else //Added by Sarubala on 09-11-17
                {
                    await DataAccess.DeleteCustomerPayment(sales);
                }
            }

            // Added Gavaskar 01/02/2017
            if (sales.CashType == "Full")
            {
                await DataAccess.DeleteCustomerPayment(sales);
            }
            //await UpdateProductStock(sales.SalesItem); //Moved to a different place
        }

        private IEnumerable<SalesItem> GetEnumerable(Sales sales)
        {
            foreach (var item in sales.SalesItem)
            {
                SetMetaData(sales, item);
                item.SalesId = sales.Id;
                if (string.IsNullOrEmpty(item.SaleProductName) && !string.IsNullOrEmpty(item.ProductName))
                {
                    item.SaleProductName = item.ProductName;
                }
                item.SalesPatientId = sales.PatientId; // Added by Gavaskar 02-10-2017 Patient Id Check
                yield return item;
            }
        }

        private async Task UpdateProductStock(IEnumerable<SalesItem> salesItems)
        {
            foreach (var salesItem in salesItems)
            {
                await _productStockManager.UpdateProductStock(salesItem.ProductStockId, (-1) * salesItem.Quantity);
            }
        }

        public async Task<IEnumerable<Patient>> getPatientDetails(Patient patient)
        {
            return await _patientManager.PatientOfflineList(patient, patient.AccountId, patient.InstanceId, 0);
        }

        private async Task<List<SalesItem>> GetProduct(Sales sales)
        {
            var list = new List<SalesItem>();

            foreach (var item in sales.SalesItem)
            {
                SetMetaData(sales, item);
                item.SalesId = sales.Id;
                item.ProductStock.Product = await _productDataAccess.GetProductByProductStockId(item.ProductStockId);
                list.Add(item);
            }

            return list;
        }

        public async Task<List<SalesReturnItem>> GetProductForReturn(Sales sales)
        {
            var list = new List<SalesReturnItem>();

            foreach (var item in sales.SalesReturn.SalesReturnItem)
            {
                SetMetaData(sales, item);
                item.ProductStock.Product = await _productDataAccess.GetProductByProductStockId(item.ProductStockId);
                list.Add(item);
            }

            return list;
        }

        private async Task<string> GetCustomInvoiceNo(string instanceId, string invoiceSeries, DateTime invoiceDate, string sPrefix)
        {
            /*Prefix Added by Poongodi on 10/10/2017*/
            return await DataAccess.GetCustomInvoiceNo(instanceId, invoiceSeries, invoiceDate, sPrefix);
        }

        //Added by Sarubala on 03-10-17
        public async Task SendSmsOnEdit(Sales sale, string mode)
        {
            var temp = DataAccess.GetAccountDetailToSendSms(sale.AccountId).Result;
            string MobileNo = temp.Phone;

            //Modified by Sarubala on 21-11-17
            SmsLog log1 = new SmsLog();
            log1.SmsDate = DateTime.Today;
            log1.LogType = mode == "Cancel" ? "Mediplus Sales Cancel" : "Mediplus Sales Edit";
            log1.ReceiverName = temp.Name;
            log1.InvoiceNo = sale.Prefix + sale.InvoiceSeries + sale.InvoiceNo;
            log1.InvoiceDate = sale.InvoiceDate;
            log1.ReferenceId = mode == "Edit" ? sale.Id : sale.SalesReturn.Id;

            await _smsSender.SendSms(MobileNo, 0, log1, _smsSender.SalesEditBuilder_Setting(sale, mode));
        }
        public async Task SendSaleCommunication(Sales sales)
        {
            if (sales.Patient.PatientType != 2)
            {
                InventorySmsSettings inventorySmsSettings = await DataAccess.GetSmsSetting(sales.AccountId, sales.InstanceId);

                var url = $"{_configHelper.UrlBuilder.InvoiceUrl}{sales.Id}";
                if (sales.Mobile != null)
                {
                    if (sales.SendSms ?? false)
                    { //Modified by Sarubala on 16-11-17

                        SmsLog log1 = new SmsLog();
                        log1.SmsDate = DateTime.Today;
                        log1.LogType = sales.SalesEditMode == false ? "Sales Create" : "Sales Edit";
                        log1.ReceiverName = sales.Name;
                        log1.InvoiceNo = sales.Prefix + sales.InvoiceSeries + sales.InvoiceNo;
                        log1.InvoiceDate = sales.InvoiceDate;
                        log1.ReferenceId = sales.Id;

                        await _smsSender.SendSms(sales.Mobile, 0, log1, _smsSender.SalesBodyBuilder_SettingNew(sales, inventorySmsSettings));
                    }

                }

                if (sales.Email != null)
                {
                    if (sales.Email.Length > 0)
                    {
                        if (sales.isValidEmail)
                        {
                            if (sales.SendEmail ?? false)
                                _emailSender.Send(sales.Email, 1, _emailSender.SellsBodyBuilder(sales, url));
                        }
                        else
                        {
                            if (sales.SendEmail ?? false)
                                _emailSender.Send(sales.Email, 1, _emailSender.SellsBodyBuilder(sales, url));
                        }
                    }
                }
            }

        }

        // Added for Sales Return by San - 15-06-2017
        public async Task SendSaleReturnCommunication(Sales sale, SalesReturn salesReturn)
        {
            if (salesReturn.Patient.PatientType != 2)
            {
                InventorySmsSettings inventorySmsSettings = await DataAccess.GetSmsSetting(salesReturn.AccountId, salesReturn.InstanceId);

                var sales = await DataAccess.GetByReturnId(salesReturn.Id);
                sales.Mobile = sale.Mobile;
                sales.Email = sale.Email;
                sales.SendSms = sale.SendSms;
                sales.SendEmail = sale.SendEmail;
                sales.isValidEmail = sale.isValidEmail;
                sales.SalesReturn.NetAmount = sale.SalesReturn.NetAmount;
                sales.SalesReturn.RoundOffNetAmount = sale.SalesReturn.RoundOffNetAmount;
                sales.SalesReturn.ReturnItemAmount = sale.SalesReturn.ReturnItemAmount;
                sales.SalesReturn.TotalDiscountValue = sale.SalesReturn.TotalDiscountValue;

                var url = $"{_configHelper.UrlBuilder.ReturnInvoiceUrl}{salesReturn.Id}";
                if (sales.Mobile != null)
                {
                    if (sales.SendSms ?? false)
                    {
                        //Modified by Sarubala on 16-11-17
                        SmsLog log1 = new SmsLog();
                        log1.SmsDate = DateTime.Today;
                        log1.LogType = "Sales Return";
                        log1.ReceiverName = sales.Name;
                        log1.InvoiceNo = salesReturn.Prefix + salesReturn.InvoiceSeries + salesReturn.ReturnNo;
                        log1.InvoiceDate = salesReturn.ReturnDate;
                        log1.ReferenceId = salesReturn.Id;

                        await _smsSender.SendSms(sales.Mobile, 0, log1, _smsSender.SalesReturnBodyBuilder_Setting(sales, inventorySmsSettings));
                    }




                }

                if (sales.Email != null)
                {
                    if (sales.Email.Length > 0)
                    {
                        if (sales.isValidEmail)
                        {
                            if (sales.SendEmail ?? false)
                                _emailSender.Send(sales.Email, 1, _emailSender.SalesReturnBodyBuilder(sales, url));
                        }
                        else
                        {
                            if (sales.SendEmail ?? false)
                                _emailSender.Send(sales.Email, 1, _emailSender.SalesReturnBodyBuilder(sales, url));
                        }
                    }
                }
            }
        }

        #region Sales Order Template
        // Added by Gavaskar 03-10-2017 Save Sales Order Start
        public async Task<SalesOrderEstimate> CreateSalesOrder(SalesOrderEstimate data)
        {

            data.OrderEstimateDate = CustomDateTime.IST;
            data.OrderEstimateId = await GetSalesOrderNo(data.InstanceId, Convert.ToInt32(data.SalesOrderEstimateType), data.Prefix);

            var getfinyear = await getSalesOrderEstimateFinYear(data); /*Added by Gavaskar on 17/10/2017*/
            data.FinyearId = getfinyear;

            var SalesOrder = await DataAccess.SaveSalesOrder(data);
            if (SalesOrder.SalesOrderEstimateItem.Count > 0)
            {
                SalesOrder.SalesOrderEstimateItem = (await GetProductEnumerable(SalesOrder)).ToList();
                await SaveSalesOrderItem(SalesOrder);
            }
            if (SalesOrder.SalesEstimateItem.Count > 0)
            {
                await SaveSalesEstimateItem(SalesOrder);
            }
            // Sales Order and Estimate Communication Start
            bool internet = NetworkInterface.GetIsNetworkAvailable();
            if (internet == true)
            {
                await ResendedSalesOrderEstimateSms(SalesOrder, data.AccountId, data.InstanceId);
                await ResendedSalesOrderEstimateEmail(SalesOrder, data.AccountId, data.InstanceId);
            }
            // Sales Order and Estimate Communication Start
            return SalesOrder;
        }

        public async Task<string> getSalesOrderEstimateFinYear(SalesOrderEstimate data)
        {
            return await DataAccess.getSalesOrderEstimateFinYear(data);
        }

        public async Task<string> GetSalesOrderNo(string InstanceId, int SalesOrderEstimateType, string Prefix)
        {
            return await DataAccess.GetSalesOrderNo(InstanceId, SalesOrderEstimateType, Prefix);
        }



        private async Task SaveSalesOrderItem(SalesOrderEstimate SalesOrderEstimate)
        {
            foreach (var item in SalesOrderEstimate.SalesOrderEstimateItem)
            {
                if (item.IsDeleted == true)
                {
                }
                else
                {
                    item.AccountId = SalesOrderEstimate.AccountId;
                    item.InstanceId = SalesOrderEstimate.InstanceId;
                    item.SalesOrderEstimateId = SalesOrderEstimate.Id;
                    item.ProductId = item.ProductId;
                    item.CreatedBy = SalesOrderEstimate.CreatedBy;
                    item.UpdatedBy = SalesOrderEstimate.UpdatedBy;
                    await DataAccess.SaveSalesOrderItem(item);
                }

            }
        }

        private async Task SaveSalesEstimateItem(SalesOrderEstimate SalesOrderEstimate)
        {
            foreach (var item in SalesOrderEstimate.SalesEstimateItem)
            {
                if (item.IsDeleted == true)
                {
                }
                else
                {
                    item.AccountId = SalesOrderEstimate.AccountId;
                    item.InstanceId = SalesOrderEstimate.InstanceId;
                    item.SalesOrderEstimateId = SalesOrderEstimate.Id;
                    item.ProductStockId = item.ProductStockId;
                    //item.ProductId = item.Id;
                    item.CreatedBy = SalesOrderEstimate.CreatedBy;
                    item.UpdatedBy = SalesOrderEstimate.UpdatedBy;
                    await DataAccess.SaveSalesOrderItem(item);
                }

            }
        }

        public async Task<SalesTemplate> CreateSalesNewTemplate(SalesTemplate SalesTemplate)
        {
            await ValidateTemplate(SalesTemplate);
            SalesTemplate.TemplateDate = CustomDateTime.IST;
            SalesTemplate.TemplateNo = await GetTemplateNo(SalesTemplate.InstanceId, SalesTemplate.Prefix);
            var getfinyear = await getSalesTemplateFinYear(SalesTemplate); /*Added by Gavaskar on 17/10/2017*/
            SalesTemplate.FinyearId = getfinyear;

            SalesTemplate.IsActive = 0;
            var salesTemplate = await DataAccess.SaveSalesTemplate(SalesTemplate);
            salesTemplate.SalesTemplateItem = (await GetProductEnumerableSalesTemplate(salesTemplate)).ToList();
            await SaveSalesTemplateItem(salesTemplate);
            return salesTemplate;
        }
        public async Task<string> ValidateTemplate(SalesTemplate data)
        {
            string newTemplate = await DataAccess.CheckUniqueTemplate(data);
            if (!string.IsNullOrEmpty(newTemplate))
            {
                string errorMessage = "Template Name Already Exist" + " for this Template No. " + newTemplate;
                throw new Exception(errorMessage);
            }
            return "ok";
        }
        public async Task<string> getSalesTemplateFinYear(SalesTemplate data)
        {
            return await DataAccess.getSalesTemplateFinYear(data);
        }
        public async Task<string> GetTemplateNo(string InstanceId, string Prefix)
        {
            return await DataAccess.GetTemplateNo(InstanceId, Prefix);
        }

        public async Task SaveSalesTemplateItem(SalesTemplate SalesTemplate)
        {
            foreach (var item in SalesTemplate.SalesTemplateItem)
            {
                if (item.IsDeleted == true)
                {
                }
                else
                {
                    item.AccountId = SalesTemplate.AccountId;
                    item.InstanceId = SalesTemplate.InstanceId;
                    item.TemplateId = SalesTemplate.Id;
                    if (item.ProductId == null)
                    {
                        item.ProductId = item.Id;
                    }
                    else
                    {
                        item.ProductId = item.ProductId;
                    }

                    item.IsDeleted = false;
                    item.CreatedBy = SalesTemplate.CreatedBy;
                    item.UpdatedBy = SalesTemplate.UpdatedBy;
                    await DataAccess.SaveSalesTemplateItemItem(item);
                }

            }
        }
        public async Task<List<SalesTemplate>> GetTemplateNameList(string AccountId, string InstanceId)
        {
            return await DataAccess.GetTemplateNameList(AccountId, InstanceId);
        }
        public async Task<List<SalesTemplateItem>> getSelectedTemplateNameList(string TemplateId, string AccountId, string InstanceId)
        {
            return await DataAccess.getSelectedTemplateNameList(TemplateId, AccountId, InstanceId);
        }

        public async Task<SalesTemplate> updateTemplate(SalesTemplate SalesTemplate)
        {
            foreach (var item in SalesTemplate.SalesTemplateItem)
            {
                if (item.Id != null)
                {
                    item.IsDeleted = item.IsDeleted as bool? ?? false;
                    item.UpdatedBy = SalesTemplate.UpdatedBy;
                    await DataAccess.UpdateSalesTemplateItem(item);
                }
                else
                {
                    item.AccountId = SalesTemplate.AccountId;
                    item.InstanceId = SalesTemplate.InstanceId;
                    item.TemplateId = SalesTemplate.Id;
                    item.IsDeleted = false;
                    item.CreatedBy = SalesTemplate.CreatedBy;
                    item.UpdatedBy = SalesTemplate.UpdatedBy;

                    if (!string.IsNullOrEmpty(item.ProductMasterID))
                    {
                        var product = new Product
                        {
                            Name = item.ProductName,
                            ProductMasterID = item.ProductMasterID,
                            Manufacturer = item.Manufacturer,
                            Schedule = item.Schedule,
                            GenericName = item.GenericName,
                            Packing = item.Packing,
                            Category = item.Category,
                            AccountId = SalesTemplate.AccountId,
                            InstanceId = SalesTemplate.InstanceId,
                            CreatedBy = SalesTemplate.CreatedBy,
                            UpdatedBy = SalesTemplate.UpdatedBy,
                            CreatedAt = CustomDateTime.IST,
                            UpdatedAt = CustomDateTime.IST
                        };

                        int NoOfRows = await _productDataAccess.TotalProductCount(product);

                        var getChar = product.Name.Substring(0, 2);
                        product.Code = (getChar.ToUpper() + (NoOfRows + 1)).ToString();
                        product = await _productDataAccess.Save(product);
                        item.ProductId = product.Id;
                        item.Product.Name = product.Name;
                    }
                    else
                    {
                        var product = await _productDataAccess.GetProductById(item.ProductId);
                        item.Product.Name = product.Name;
                    }

                    await DataAccess.SaveSalesTemplateItemItem(item);
                }

            }
            return SalesTemplate;
        }
        public async Task<PagerContract<SalesOrderEstimate>> SalesOrderList(SalesOrderEstimate data, string instanceid)
        {
            var pager = new PagerContract<SalesOrderEstimate>
            {
                NoOfRows = await DataAccess.SalesOrderCount(data),
                List = await DataAccess.SalesOrderEstimateList(data, instanceid)
            };
            return pager;
        }
        public async Task<PagerContract<SalesTemplate>> SalesTemplateList(SalesTemplate data, string instanceid)
        {
            var pager = new PagerContract<SalesTemplate>
            {
                NoOfRows = await DataAccess.SalesTemplatedCount(data),
                List = await DataAccess.SalesTemplateList(data, instanceid)
            };
            return pager;
        }

        public async Task<SalesTemplate> UpdateSalesTemplateStatus(SalesTemplate SalesTemplate)
        {
            return await DataAccess.UpdateSalesTemplateStatus(SalesTemplate);
        }

        public async Task<SalesOrderEstimate> GetSalesOrderById(string salesOrderEstimateId, string AccountId, string instanceid)
        {
            return await DataAccess.GetSalesOrderById(salesOrderEstimateId, AccountId, instanceid);
        }
        public async Task<List<SalesOrderEstimateItem>> updateSalesOrderEstimateItem(List<SalesOrderEstimateItem> data, SalesOrderEstimate model)
        {
            foreach (var item in data)

            {
                if (item.SalesOrderEstimateId != null)
                {
                    await DataAccess.updateSalesOrderEstimateItem(item);
                }
                else
                {
                    if (item.IsDeleted == null || item.IsDeleted == false)
                    {
                        item.AccountId = model.AccountId;
                        item.InstanceId = model.InstanceId;
                        item.SalesOrderEstimateId = model.Id;
                        item.CreatedBy = model.CreatedBy;
                        item.UpdatedBy = model.UpdatedBy;

                        if (!string.IsNullOrEmpty(item.ProductMasterID))
                        {
                            var product = new Product
                            {
                                Name = item.ProductName,
                                ProductMasterID = item.ProductMasterID,
                                Manufacturer = item.Manufacturer,
                                Schedule = item.Schedule,
                                GenericName = item.GenericName,
                                Packing = item.Packing,
                                Category = item.Category,
                                AccountId = model.AccountId,
                                InstanceId = model.InstanceId,
                                CreatedBy = model.CreatedBy,
                                UpdatedBy = model.UpdatedBy,
                                CreatedAt = CustomDateTime.IST,
                                UpdatedAt = CustomDateTime.IST
                            };

                            int NoOfRows = await _productDataAccess.TotalProductCount(product);

                            var getChar = product.Name.Substring(0, 2);
                            product.Code = (getChar.ToUpper() + (NoOfRows + 1)).ToString();
                            product = await _productDataAccess.Save(product);
                            item.ProductId = product.Id;
                            item.Product.Name = product.Name;
                        }
                        else
                        {
                            var product = await _productDataAccess.GetProductById(item.ProductId);
                            item.Product.Name = product.Name;
                        }

                        await DataAccess.SaveSalesOrderItem(item);
                    }

                }

            }


            int salesCount = await DataAccess.SalesOrderEstimatePendingCount(model.Id, model.AccountId, model.InstanceId);
            if (salesCount == 0)
            {
                await DataAccess.updateSalesOrderEstimateConvertedPending(model);
            }
            return data;
        }

        public async Task<List<ProductStock>> GetSelectedProductStockList(string TemplateId, string AccountId, string InstanceId, string Types)
        {
            return await DataAccess.GetSelectedProductStockList(TemplateId, AccountId, InstanceId, Types);
        }
        public async Task<bool> ResendSalesOrderEstimateSms(SalesOrderEstimate salesOrderEstimate, string AccountId, string InstanceId)
        {
            return await ResendedSalesOrderEstimateSms(salesOrderEstimate, AccountId, InstanceId);
        }
        private async Task<bool> ResendedSalesOrderEstimateSms(SalesOrderEstimate salesOrderEstimate, string AccountId, string InstanceId)
        {
            //Added by Sarubala on 21-11-17
            SmsSettings settings = (DataAccess.getSmsSettings(InstanceId, AccountId).Result);
            bool sendSms1 = false;
            if (salesOrderEstimate.SalesOrderEstimateType == 1)
            {
                sendSms1 = Convert.ToBoolean(settings.IsSalesOrderSms);
            }
            else if (salesOrderEstimate.SalesOrderEstimateType == 2)
            {
                sendSms1 = Convert.ToBoolean(settings.IsSalesEstimateSms);
            }

            salesOrderEstimate.Instance = await _instanceSetupManager.GetById(InstanceId);
            salesOrderEstimate.OrderEstimateId = Convert.ToString(salesOrderEstimate.Prefix) + salesOrderEstimate.OrderEstimateId;
            if (!string.IsNullOrEmpty(salesOrderEstimate.Patient.Mobile) && sendSms1 == true)
            {
                SmsLog log1 = new SmsLog();
                log1.SmsDate = DateTime.Today;
                log1.ReceiverName = salesOrderEstimate.Patient.Name;
                log1.LogType = salesOrderEstimate.SalesOrderEstimateType == 1 ? "Sales Order" : "Sales Estimate";
                log1.InvoiceNo = salesOrderEstimate.OrderEstimateId;
                log1.InvoiceDate = salesOrderEstimate.OrderEstimateDate;
                log1.ReferenceId = salesOrderEstimate.Id;

                await _smsSender.SendSms(salesOrderEstimate.Patient.Mobile, 0, log1, _smsSender.SalesOrderEstimateBodyBuilder(salesOrderEstimate));
            }
            return true;
        }
        public async Task<bool> ResendSalesOrderEstimateEmail(SalesOrderEstimate salesOrderEstimate, string AccountId, string InstanceId)
        {
            return await ResendedSalesOrderEstimateEmail(salesOrderEstimate, AccountId, InstanceId);
        }
        private async Task<bool> ResendedSalesOrderEstimateEmail(SalesOrderEstimate salesOrderEstimate, string AccountId, string InstanceId)
        {
            salesOrderEstimate.Instance = await _instanceSetupManager.GetById(InstanceId);
            salesOrderEstimate.OrderEstimateId = Convert.ToString(salesOrderEstimate.Prefix) + salesOrderEstimate.OrderEstimateId.Replace("O", "");
            if (!string.IsNullOrEmpty(salesOrderEstimate.Patient.Email))
            {
                if (salesOrderEstimate.SalesOrderEstimateType == 1)
                {
                    _emailSender.Send(salesOrderEstimate.Patient.Email, 16, _emailSender.SalesOrderEstimateBodyBuilder(salesOrderEstimate));
                }
                else if (salesOrderEstimate.SalesOrderEstimateType == 2)
                {
                    _emailSender.Send(salesOrderEstimate.Patient.Email, 17, _emailSender.SalesOrderEstimateBodyBuilder(salesOrderEstimate));
                }

            }
            return true;
        }
        public async Task<List<SalesOrderEstimate>> GetSelectedCustomerOrderEstimateList(string patientId, string AccountId, string InstanceId)
        {
            return await DataAccess.GetSelectedCustomerOrderEstimateList(patientId, AccountId, InstanceId);
        }

        private async Task<IEnumerable<SalesOrderEstimateItem>> GetProductEnumerable(SalesOrderEstimate salesOrder)
        {
            var list = new List<SalesOrderEstimateItem>();

            if (salesOrder.SalesOrderEstimateType == 1)
            {
                foreach (var item in salesOrder.SalesOrderEstimateItem)
                {
                    if (!string.IsNullOrEmpty(item.ProductMasterID))
                    {
                        var product = new Product
                        {
                            Name = item.ProductName,
                            ProductMasterID = item.ProductMasterID,
                            Manufacturer = item.Manufacturer,
                            Schedule = item.Schedule,
                            GenericName = item.GenericName,
                            Packing = item.Packing,
                            Category = item.Category,
                            AccountId = salesOrder.AccountId,
                            InstanceId = salesOrder.InstanceId,
                            CreatedBy = salesOrder.CreatedBy,
                            UpdatedBy = salesOrder.UpdatedBy,
                            CreatedAt = CustomDateTime.IST,
                            UpdatedAt = CustomDateTime.IST
                        };

                        int NoOfRows = await _productDataAccess.TotalProductCount(product);

                        var getChar = product.Name.Substring(0, 2);
                        product.Code = (getChar.ToUpper() + (NoOfRows + 1)).ToString();
                        product = await _productDataAccess.Save(product);
                        item.ProductId = product.Id;
                        item.Product.Name = product.Name;
                    }
                    else
                    {
                        var product = await _productDataAccess.GetProductById(item.ProductId);
                        item.Product.Name = product.Name;
                    }
                    list.Add(item);
                }
            }
            return list;
        }
        private async Task<IEnumerable<SalesTemplateItem>> GetProductEnumerableSalesTemplate(SalesTemplate salesTemplate)
        {
            var list = new List<SalesTemplateItem>();


            foreach (var item in salesTemplate.SalesTemplateItem)
            {
                if (!string.IsNullOrEmpty(item.ProductMasterID))
                {
                    var product = new Product
                    {
                        Name = item.ProductName,
                        ProductMasterID = item.ProductMasterID,
                        Manufacturer = item.Manufacturer,
                        Schedule = item.Schedule,
                        GenericName = item.GenericName,
                        Packing = item.Packing,
                        Category = item.Category,
                        AccountId = salesTemplate.AccountId,
                        InstanceId = salesTemplate.InstanceId,
                        CreatedBy = salesTemplate.CreatedBy,
                        UpdatedBy = salesTemplate.UpdatedBy,
                        CreatedAt = CustomDateTime.IST,
                        UpdatedAt = CustomDateTime.IST
                    };

                    int NoOfRows = await _productDataAccess.TotalProductCount(product);

                    var getChar = product.Name.Substring(0, 2);
                    product.Code = (getChar.ToUpper() + (NoOfRows + 1)).ToString();
                    product = await _productDataAccess.Save(product);
                    item.ProductId = product.Id;
                    item.Product.Name = product.Name;
                }
                else
                {
                    var product = await _productDataAccess.GetProductById(item.ProductId);
                    item.Product.Name = product.Name;
                }
                list.Add(item);
            }

            return list;
        }
        // Added by Gavaskar 03-10-2017 Save Sales Order End
        #endregion

    }
}
