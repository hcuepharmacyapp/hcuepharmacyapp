﻿using System;
using System.Threading.Tasks;

namespace HQue.Biz.Core.Replication
{
    public class SyncTrigger: ISyncTrigger
    {
        private readonly ReplicationManager _replicationManager;

        public SyncTrigger(ReplicationManager replicationManger)
        {
            _replicationManager = replicationManger;
        }

        public async Task StartSync()
        {
            try
            {
                if (await _replicationManager.CheckIfClientHasEverSynced())
                    await _replicationManager.StartSync();
            }
            catch (Exception)
            {
                
            }
        }
    }
}
