﻿using HQue.Contract.Infrastructure.Replication;
using HQue.DataAccess.Replication;
using System.Collections.Generic;
using System.Linq;
using HQue.DataAccess.DbModel;
using System.Threading.Tasks;
using HQue.Communication;
using Utilities.Helpers;
using DataAccess.QueryBuilder;

namespace HQue.Biz.Core.Replication
{
    public class ReplicationManager : BizManager
    {
        private readonly ISyncDataAccessFactory _syncDataAccessFactory;
        private readonly ReplicationDataAccess _replicationDataAccess;
        private readonly InstanceClientDataAccess _instanceClientDataAccess;
        private readonly OnlineReplicationDataAccess _onlineReplicationDataAccess;
        private readonly HelperCommunication _helperCommunication;

        private ProcessRequestHelper _processRequestHelper;

        private ProcessRequestHelper ProcessRequestHelper => _processRequestHelper ??
                                                             (_processRequestHelper =
                                                                 new ProcessRequestHelper(_replicationDataAccess,
                                                                     _syncDataAccessFactory, _helperCommunication));

        private static IEnumerable<InstanceClient> _instanceClient;

        private async Task<IEnumerable<InstanceClient>> GetInstanceClient()
        {
            return _instanceClient ?? (_instanceClient = await _instanceClientDataAccess.GetInstanceClients());
        }

        public ReplicationManager(ReplicationDataAccess replicationDataAccess,
            InstanceClientDataAccess instanceClientDataAccess, OnlineReplicationDataAccess onlineReplicationDataAccess,
            ISyncDataAccessFactory factory, HelperCommunication helperCommunication) : base(replicationDataAccess)
        {
            _instanceClientDataAccess = instanceClientDataAccess;
            _replicationDataAccess = replicationDataAccess;
            _onlineReplicationDataAccess = onlineReplicationDataAccess;
            _syncDataAccessFactory = factory;
            _helperCommunication = helperCommunication;
        }

        public async Task StartSync()
        {
            //Fetch offline replication data
            var instanceClient = (await GetInstanceClient()).First();
            var replicationData = await _replicationDataAccess.GetReplicationData();

            var onlineReplicationData = GetOnlineReplicationData(instanceClient);

            //Resolve conflicts
            var pushList = new List<SyncRequestEntity>();
            var pullList = new List<SyncRequestEntity>();
            var replicationEntityDataToBeSyncedOnlineFromOffline = new List<ReplicationEntity>();

            //Resolve conflicts.
            ResolveConflicts(replicationData, onlineReplicationData, replicationEntityDataToBeSyncedOnlineFromOffline,
                pushList, pullList);

            //Populate push list to sync to cloud
            //PopulatePushList(pushList);
            await ProcessRequestHelper.PopulatePushListWithData(pushList, _syncDataAccessFactory);

            //Populate pull list to sync to cloud
            await PopulatePullList(pullList, instanceClient);

            await SyncOfflineDB(pullList);

            await SyncOnlineDB(pushList, onlineReplicationData, replicationEntityDataToBeSyncedOnlineFromOffline,
                instanceClient);

            await CleanReplicationTable(replicationData);

            await UpdateLastSyncedAt();
        }

        private List<ReplicationEntity> GetOnlineReplicationData(InstanceClient instanceClient)
        {
            var syncRequest = new SyncRequest {InstanceClientData = instanceClient};
            var requestEntity = new SyncRequestEntity()
            {
                TableName = ReplicationDataTable.Table.TableName,
                Action = (int) OperationType.Select
            };
            var syncRequestEntityList = new List<SyncRequestEntity> {requestEntity};
            syncRequest.Request = syncRequestEntityList.ToArray();
            //Fetch online replication data
            var d = _onlineReplicationDataAccess.ProcessRequest(syncRequest).Result;
            var result = d.Response.First().ReplicationEntityData.ToList();
            return result;
        }

        private async Task CleanReplicationTable(List<ReplicationEntity> replicationData)
        {
            //Delete replication table records in offline
            foreach (var item in replicationData.Where(item => item.MarkedForDeletion))
            {
                await
                    _replicationDataAccess.DeleteRecords(item, ReplicationDataTable.Table,
                        ReplicationDataTable.TransactionIDColumn, item.TransactionID);
            }
        }

        private async Task SyncOfflineDB(List<SyncRequestEntity> pullList)
        {
            foreach (var item in pullList)
            {
                var da = _syncDataAccessFactory.GetSyncDataAccess(item.TableName, item.InstanceClientData.InstanceId,
                    item.InstanceClientData.AccountId);
                await ProcessRequestHelper.SaveData(item, da);
            }
        }

        private async Task SyncOnlineDB(List<SyncRequestEntity> pushList,
            IEnumerable<ReplicationEntity> onlineReplicationData,
            List<ReplicationEntity> replicationEntityDataToBeSyncedOnlineFromOffline, InstanceClient instanceClient)
        {
            //Update replication table records in online database
            onlineReplicationData = onlineReplicationData.Where(o => o.ModifiedByClient).ToList();
            var replicationEntityList = new List<SyncRequestEntity>();
            foreach (var item in onlineReplicationData)
            {
                item.ClientId = instanceClient.ClientId;
                item.UpdatedBy = instanceClient.ClientId;
                item.UpdatedAt = CustomDateTime.IST;
                item.ActionTime = CustomDateTime.IST;
                replicationEntityList.Add(new SyncRequestEntity
                {
                    TableName = ReplicationDataTable.TableName,
                    TransactionId = item.TransactionID,
                    EntityId = item.Id,
                    Action = (int) OperationType.Update,
                    ReplicationEntityData = item
                });
            }

            //Update replication table records in online database
            replicationEntityList.AddRange(
                replicationEntityDataToBeSyncedOnlineFromOffline.Select(
                    item =>
                        new SyncRequestEntity()
                        {
                            TableName = ReplicationDataTable.TableName,
                            TransactionId = item.TransactionID,
                            EntityId = item.Id,
                            Action = (int) OperationType.Insert,
                            ReplicationEntityData = item
                        }));

            if (replicationEntityList.Count > 0)
                pushList.AddRange(replicationEntityList);

            if (pushList.Count > 0)
                await
                    _onlineReplicationDataAccess.ProcessRequest(new SyncRequest
                    {
                        Request = pushList.ToArray(),
                        InstanceClientData = instanceClient
                    });
        }

        private async Task PopulatePullList(List<SyncRequestEntity> pullList, InstanceClient instanceClientData)
        {
            var newRequest = new SyncRequest
            {
                InstanceClientData = instanceClientData,
                Request =
                    pullList.Select(
                        e =>
                            new SyncRequestEntity
                            {
                                Action = (int) OperationType.Select,
                                EntityId = e.EntityId,
                                UseId = true,
                                TableName = e.TableName,
                                ActionTime = e.ActionTime
                            }).ToArray()
            };

            if (newRequest.Request?.Length <= 0) return;

            //Fetch data from cloud to sync to offline
            var result = await _onlineReplicationDataAccess.ProcessRequest(newRequest);

            ProcessRequestHelper.PopulatePullListWithResponseData(pullList, result);
        }

        public async Task<SyncResponse> ProcessRequest(SyncRequest request)
        {
            return await ProcessRequestHelper.ProcessRequest(request);
        }

        public void ResolveConflicts(List<ReplicationEntity> offlineData, List<ReplicationEntity> onlineData,
            List<ReplicationEntity> replicationEntityDataToBeSyncedOnlineFromOffline, List<SyncRequestEntity> pushList,
            List<SyncRequestEntity> pullList)
        {
            foreach (var entity in offlineData)
            {
                var itemFoundOnline =
                    onlineData.FirstOrDefault(o => o.TableName == entity.TableName && o.Id == entity.Id);
                if (itemFoundOnline != null)
                {
                    if (entity.ActionTime < itemFoundOnline.ActionTime)
                    {
                        //Online has latest. Get latest object and update offline
                        //Mark both replication items for deletion
                        pullList.Add(new SyncRequestEntity
                        {
                            TableName = itemFoundOnline.TableName,
                            EntityId = itemFoundOnline.Id,
                            Action = itemFoundOnline.Action.Value
                        });
                        entity.MarkedForDeletion = true;
                    }
                    else
                    {
                        //Offline has latest. Get latest object and update online
                        //Mark both replication items for deletion.
                        pushList.Add(new SyncRequestEntity
                        {
                            TableName = entity.TableName,
                            EntityId = entity.Id,
                            TransactionId = entity.TransactionID,
                            Action = entity.Action.Value
                        });
                        itemFoundOnline.ModifiedByClient = true;
                        entity.MarkedForDeletion = true;
                    }
                }
                else
                {
                    //Update to online
                    pushList.Add(new SyncRequestEntity
                    {
                        TableName = entity.TableName,
                        EntityId = entity.Id,
                        TransactionId = entity.TransactionID,
                        Action = (int) OperationType.Insert
                    });
                    //Mark offline for deletion
                    entity.MarkedForDeletion = true;
                    replicationEntityDataToBeSyncedOnlineFromOffline.Add(entity);
                }
            }
            if (onlineData == null) return;
            pullList.AddRange(from t in onlineData
                where !t.MarkedForDeletion && t.Action.ToString() != OperationType.Delete.ToString()
                select
                    new SyncRequestEntity()
                    {
                        TableName = t.TableName,
                        EntityId = t.Id,
                        Action = (int) OperationType.Insert
                    });
        }

        public async Task<bool> CheckIfClientHasEverSynced()
        {
            return (await _instanceClientDataAccess.GetInstanceClients()).FirstOrDefault() != null;
        }

        private async Task UpdateLastSyncedAt()
        {
            var instanceClient = (await _instanceClientDataAccess.GetInstanceClients()).First();
            instanceClient.LastSyncedAt = CustomDateTime.IST;
            await _instanceClientDataAccess.Save(instanceClient);
        }
    }
}
