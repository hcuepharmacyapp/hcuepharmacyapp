﻿using System.Threading.Tasks;

namespace HQue.Biz.Core.Replication
{
    public interface ISyncTrigger
    {
        Task StartSync();
    }
}