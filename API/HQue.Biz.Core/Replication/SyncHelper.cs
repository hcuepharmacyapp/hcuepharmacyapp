﻿using DataAccess.QueryBuilder;
using HQue.Biz.Core.Setup;
using HQue.Biz.Core.UserAccount;
using HQue.Contract.Infrastructure.Replication;
using HQue.Contract.Infrastructure.Setup;
using HQue.Contract.Infrastructure.UserAccount;
using HQue.DataAccess.DbModel;
using HQue.DataAccess.Replication;
using System;
using System.Linq;
using System.Threading.Tasks;
using Utilities.Helpers;

namespace HQue.Biz.Core.Replication
{
    public class SyncHelper
    {
        private readonly ConfigHelper _configHelper;
        private readonly InstanceClientDataAccess _instanceClientDataAccess;
        private readonly OnlineReplicationDataAccess _onlineReplicationDataAccess;
        private readonly ProcessRequestHelper _processRequestHelper;
        private readonly ReplicationManager _replicationManager;
        private readonly UserManager _userManager;
        private readonly InstanceSetupManager _instanceSetupManager;

        public SyncHelper(ConfigHelper configHelper, UserManager userManager,
            InstanceClientDataAccess instanceClientDataAccess, OnlineReplicationDataAccess onlineReplicationDataAccess,
            ProcessRequestHelper processRequestHelper, InstanceSetupManager instanceSetupManager, ReplicationManager replicationManager)
        {
            _configHelper = configHelper;
            _instanceClientDataAccess = instanceClientDataAccess;
            _onlineReplicationDataAccess = onlineReplicationDataAccess;
            _processRequestHelper = processRequestHelper;
            _replicationManager = replicationManager;
            _instanceSetupManager = instanceSetupManager;
            _userManager = userManager;
        }

        public async Task<Tuple<bool, HQueUser>> HandleSync(HQueUser user)
        {
            if (!_configHelper.AppConfig.OfflineMode)
                return await _userManager.ValidateUser(user);

            //return await ValidateAndSyncOffline(user);

            //validate user locally now
            var validatedUser  = await _userManager.ValidateUser(user);

            if (!validatedUser.Item1)
            {
                int userCount=await _userManager.getOfflineUserCount();
                //validate user online now                
                if(userCount<=0)
                    validatedUser = await ValidateOfflineUser(user);
            }

            return validatedUser;
        }

        private async Task<Tuple<bool, HQueUser>> ValidateAndSyncOffline(HQueUser user)
        {
            if (await _replicationManager.CheckIfClientHasEverSynced())
            {
                var result = await _userManager.ValidateUser(user);
                if (!result.Item1)
                    return result;

                var instanceClient = await _instanceClientDataAccess.GetInstanceClients();
                Constant.ClientId = instanceClient.First().ClientId;
                return result;
            }
            else
            {
                //Call api to validate the user
                var result = await _userManager.ValidateOnlineUser(user);
                return new Tuple<bool, HQueUser>(result.IsSuccess, result.Data);
            }
        }

        private async Task<Tuple<bool, HQueUser>> ValidateOfflineUser(HQueUser user)
        {
            //Call api to validate the user
            var result = await _userManager.ValidateOnlineUser(user);
            var validatedUser = new Tuple<bool, HQueUser>(result.IsSuccess, result.Data);
            if (validatedUser.Item1)
            {
                //insert instance data
                validatedUser.Item2.Instance.CreatedBy = "localadmin";
                validatedUser.Item2.Instance.UpdatedBy = "localadmin";
                //Account accountInsert = new Account();
                //accountInsert.Id = result.Data.AccountId;

                await _instanceSetupManager.SaveForOffline(validatedUser.Item2.Instance);
                             
                //accountInsert.Name = result.Data.Instance.AccountName;
                //accountInsert.InstanceCount = result.Data.Instance.InstanceCount;
                //accountInsert.CreatedBy = validatedUser.Item2.Instance.CreatedBy;
                //accountInsert.UpdatedBy = validatedUser.Item2.Instance.UpdatedBy;
                //await _instanceSetupManager.SaveAccountsForOffline(accountInsert);

                //Setup account, instance and access for offline
                var data = await _userManager.SaveForOffline(validatedUser.Item2);
                data.Password = user.Password;
                await _userManager.ChangePassword(data);
                return validatedUser;
            }
            return validatedUser;
        }

        public async Task SyncOnlineData(HQueUser result)
        {
            if (_configHelper.AppConfig.IsSqlServer)
                return;

            if (!await _replicationManager.CheckIfClientHasEverSynced())
            {
                //Sync online data to offline db for the first time use
                await SyncOnlineDatabaseToClient(result.InstanceId, result.AccountId);

                //Set instance client id
                var instanceClient = CreateInstanceClientObject(result);
                await _instanceClientDataAccess.Insert(instanceClient, InstanceClientTable.Table);

                Constant.ClientId = instanceClient.ClientId;
            }
            else
            {
                await _replicationManager.StartSync();
            }
        }

        private InstanceClient CreateInstanceClientObject(HQueUser user)
        {
            return new InstanceClient
            {
                Id = Guid.NewGuid().ToString(),
                AccountId = user.AccountId,
                InstanceId = user.InstanceId,
                LastSyncedAt = CustomDateTime.IST,
                ClientId = Guid.NewGuid().ToString()
            };
        }


        private async Task SyncOnlineDatabaseToClient(string instanceId, string accountId)
        {
            var requestForFirstTimeSync = CreateRequestForFirstTimeSync(instanceId, accountId);
            var response = await _onlineReplicationDataAccess.ProcessRequest(requestForFirstTimeSync);
            await _processRequestHelper.SyncOnlineDataToTheClient(response.Response.First(), instanceId, accountId);
        }

        private SyncRequest CreateRequestForFirstTimeSync(string instanceId, string accountId)
        {
            return new SyncRequest
            {
                InstanceClientData = new InstanceClient {AccountId = accountId, InstanceId = instanceId},
                Request = new[]
                {
                    new SyncRequestEntity
                    {
                        Action = (int) OperationType.SyncOnlineDatabaseToClient,
                    }
                }
            };
        }
    }
}
