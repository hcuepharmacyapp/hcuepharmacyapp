﻿using HQue.Contract.Base;
using HQue.Contract.Infrastructure.Inventory;
using HQue.Contract.Infrastructure.Replication;
using HQue.DataAccess.DbModel;
using HQue.DataAccess.Replication;
using System.Collections.Generic;
using System.Linq;
using HQue.Communication;
using System.Threading.Tasks;
using DataAccess.QueryBuilder;

namespace HQue.Biz.Core.Replication
{
    public class ProcessRequestHelper
    {
        private readonly ISyncDataAccessFactory _dataAccessFactory;
        private readonly ReplicationDataAccess _replicationDataAccess;
        private BaseInstanceClient _instanceClientData;
        private readonly HelperCommunication _helperCommunication;
        private readonly Dictionary<string, bool> _transactionIds;

        public ProcessRequestHelper(ReplicationDataAccess replicationDataAccess, ISyncDataAccessFactory factory,
            HelperCommunication helperCommunication)
        {
            _replicationDataAccess = replicationDataAccess;
            _dataAccessFactory = factory;
            _helperCommunication = helperCommunication;
            _transactionIds = new Dictionary<string, bool>();
        }

        public async Task<SyncResponse> ProcessRequest(SyncRequest request)
        {
            _instanceClientData = request.InstanceClientData;
            var syncResponse = new SyncResponse
            {
                Response = new List<SyncResponseEntity>()
            };
            var responsesList = new List<SyncResponseEntity>();
            if (request.Request.Length == 0)
                return syncResponse;

            var requestEntity = request.Request[0];

            if ((OperationType) requestEntity.Action == OperationType.SyncOnlineDatabaseToClient)
            {
                var response = new SyncResponseEntity();
                await SyncOnlineDatabaseToClient(response, requestEntity);
                syncResponse.Response = new List<SyncResponseEntity> {response};
                return syncResponse;
            }

            //Check if the first request is "Select". i.e all requests are select and proceed with the earlier logic
            if (request.Request.First().Action == (int) OperationType.Select)
            {
                responsesList.AddRange(await ProcessRequests(request.Request));
                syncResponse.Response = responsesList;
                return syncResponse;
            }

            var queryBuilderList = new List<QueryBuilderBase>();
            var productStockList =
                request.Request.Where(
                    r =>
                        r.TableName == ProductStockTable.TableName
                        || r.TableName == VendorPurchaseItemTable.TableName
                        || r.TableName == VendorPurchaseTable.TableName
                        || r.TableName == VendorReturnTable.TableName
                        || r.TableName == VendorReturnItemTable.TableName
                        || r.TableName == SalesItemTable.TableName
                        || r.TableName == SalesTable.TableName
                        || r.TableName == SalesReturnTable.TableName
                        || r.TableName == SalesReturnItemTable.TableName).ToList();

            //Filter records which needs to run in transaction
            var recordsExceptProductStock = request.Request.Except(productStockList);

            //Build query builder base for non product stock records
            queryBuilderList.AddRange(GetQueryBuilderForSave(recordsExceptProductStock));

            //1. For product stock, build the query builder base along with vendor purchase, return and sales and sales return  
            if (productStockList.Any())
            {
                var result = await GetQueryBuilderForProductStock(productStockList);
                if (result != null) queryBuilderList.AddRange(result);
            }

            //Call DA to execute query builder objects
            //await _replicationDataAccess.ExecuteQueryBuilderForSave(queryBuilderList);

            //Make use of transaction id
            //Wrap transaction in base data access

            syncResponse.Response = responsesList;

            await HandleSmsAndEmail(request);
            return syncResponse;
        }

        private List<QueryBuilderBase> GetQueryBuilderForSave(IEnumerable<SyncRequestEntity> requests)
        {
            var qbList = new List<QueryBuilderBase>();
            foreach (var request in requests)
            {
                if (!_transactionIds.ContainsKey(request.TransactionId))
                    _transactionIds.Add(request.TransactionId,
                        _replicationDataAccess.CheckIfTransactionIdExisting(request.TransactionId).Result);
                if (!_transactionIds[request.TransactionId])
                    qbList.Add(ReturnQueryBuilder(request));
            }
            return qbList;
        }

        private async Task<List<QueryBuilderBase>> GetQueryBuilderForProductStock(
            IEnumerable<SyncRequestEntity> requestsList)
        {
            var productStockDictionary = new Dictionary<string, int>();
            var productStockQueryBuilderList = new List<QueryBuilderBase>();

            var productStocksList = requestsList.Where(r => r.TableName == ProductStockTable.TableName);

            if (productStocksList.Any())
            {
                var result2 = GetQueryBuilderForSave(productStocksList);
                if (result2?.Count > 0) productStockQueryBuilderList.AddRange(result2);
            }

            var vendorPurchasesList = requestsList.Where(r => (r.TableName == VendorPurchaseTable.TableName));
            var result = GetQueryBuilderForSave(vendorPurchasesList);
            if (result?.Count > 0) productStockQueryBuilderList.AddRange(result);

            var vendorPurchasesItemList = requestsList.Where(r => r.TableName == VendorPurchaseItemTable.TableName);
            foreach (
                var vendorPurchasesItem in
                    vendorPurchasesItemList.Where(
                        vendorPurchasesItem =>
                            vendorPurchasesItem.Action != (int) OperationType.Select &&
                            vendorPurchasesItem.Action != (int) OperationType.Delete))
            {
                //When updating vendor purchase item, add the purchased quantity to the existing product stock
                if (!productStockDictionary.ContainsKey(vendorPurchasesItem.VendorPurchaseItemData.ProductStockId))
                    productStockDictionary.Add(vendorPurchasesItem.VendorPurchaseItemData.ProductStockId, 0);

                productStockDictionary[vendorPurchasesItem.VendorPurchaseItemData.ProductStockId] +=
                    (int) vendorPurchasesItem.VendorPurchaseItemData.Quantity;

                productStockQueryBuilderList.AddRange(
                    GetQueryBuilderForSave(new[] {vendorPurchasesItem}));
            }

            var vendorReturnList = requestsList.Where(r => r.TableName == VendorReturnTable.TableName);
            result = GetQueryBuilderForSave(vendorReturnList);
            if (result?.Count > 0) productStockQueryBuilderList.AddRange(result);

            var vendorReturnItemList = requestsList.Where(r => r.TableName == VendorReturnItemTable.TableName);
            foreach (var vendorReturnItem in vendorReturnItemList)
            {
                //If the action is just select, do not go forward to update the product stock
                if (vendorReturnItem.Action == (int) OperationType.Select ||
                    vendorReturnItem.Action == (int) OperationType.Delete) continue;

                //When updating vendor return item, decrease the product stock with the returned quantity
                if (!productStockDictionary.ContainsKey(vendorReturnItem.VendorReturnItemData.ProductStockId))
                    productStockDictionary.Add(vendorReturnItem.VendorReturnItemData.ProductStockId, 0);

                productStockDictionary[vendorReturnItem.VendorReturnItemData.ProductStockId] -=
                    (int) vendorReturnItem.VendorReturnItemData.Quantity;

                productStockQueryBuilderList.AddRange(GetQueryBuilderForSave(new[] {vendorReturnItem}));
            }

            var SalesList = requestsList.Where(r => (r.TableName == SalesTable.Table.TableName));
            result = GetQueryBuilderForSave(SalesList);
            if (result?.Count > 0) productStockQueryBuilderList.AddRange(result);

            var salesItemList = requestsList.Where(r => r.TableName == SalesItemTable.TableName);
            foreach (
                var salesItem in
                    salesItemList.Where(
                        salesItem =>
                            salesItem.Action != (int) OperationType.Select &&
                            salesItem.Action != (int) OperationType.Delete))
            {
                //When updating sales item, decrease the stock with the sales quantity
                if (!productStockDictionary.ContainsKey(salesItem.SalesItemData.ProductStockId))
                    productStockDictionary.Add(salesItem.SalesItemData.ProductStockId, 0);

                productStockDictionary[salesItem.SalesItemData.ProductStockId] -= (int) salesItem.SalesItemData.Quantity;
                productStockQueryBuilderList.AddRange(GetQueryBuilderForSave(new[] {salesItem}));
            }

            var salesReturnList = requestsList.Where(r => (r.TableName == SalesReturnTable.Table.TableName));
            result = GetQueryBuilderForSave(salesReturnList);
            if (result?.Count > 0) productStockQueryBuilderList.AddRange(result);

            var salesReturnItemList = requestsList.Where(r => r.TableName == SalesReturnItemTable.TableName);
            foreach (
                var salesReturnItem in
                    salesReturnItemList.Where(
                        salesReturnItem =>
                            salesReturnItem.Action != (int) OperationType.Select &&
                            salesReturnItem.Action != (int) OperationType.Delete))
            {
                //When updating sales return item, update the stock i.e add the return quantity to the existing stock
                if (!productStockDictionary.ContainsKey(salesReturnItem.SalesReturnItemData.ProductStockId))
                    productStockDictionary.Add(salesReturnItem.SalesReturnItemData.ProductStockId, 0);

                productStockDictionary[salesReturnItem.SalesReturnItemData.ProductStockId] +=
                    (int) salesReturnItem.SalesReturnItemData.Quantity;
                productStockQueryBuilderList.AddRange(GetQueryBuilderForSave(new[] {salesReturnItem}));
            }

            productStockQueryBuilderList.AddRange(CreateProductStockSyncRequestQueryBuilder(productStockDictionary));
            await Task.Delay(0);
            return productStockQueryBuilderList;
        }

        private IEnumerable<QueryBuilderBase> CreateProductStockSyncRequestQueryBuilder(
            Dictionary<string, int> productStockDictionary)
        {
            return
                productStockDictionary.Select(item => CreateQueryBuilderForProductStock(item.Key, item.Value)).ToList();
        }

        private QueryBuilderBase CreateQueryBuilderForProductStock(string productStockId, int quantity)
        {
            var da = _dataAccessFactory.GetSyncDataAccess(ProductStockTable.Table.TableName,
                _instanceClientData.InstanceId, _instanceClientData.AccountId);
            var query = da.GetQueryBuilderToUpdateProductStock<ProductStock>(productStockId, quantity);
            return query;
        }

        public QueryBuilderBase ReturnQueryBuilder(SyncRequestEntity entity)
        {
            _instanceClientData = _instanceClientData ?? entity.InstanceClientData;
            var da = _dataAccessFactory.GetSyncDataAccess(entity.TableName, _instanceClientData.InstanceId, _instanceClientData.AccountId);
            switch (entity.TableName)
            {
                case AccountTable.TableName:
                    return da.GetQueryBuilderForSave(entity.AccountData);
                case DoctorTable.TableName:
                    return da.GetQueryBuilderForSave(entity.DoctorData);
                case HQueUserTable.TableName:
                    return da.GetQueryBuilderForSave(entity.HQueUserData);
                case InstanceTable.TableName:
                    return da.GetQueryBuilderForSave(entity.InstanceData);
                case InstanceClientTable.TableName:
                    return da.GetQueryBuilderForSave(entity.InstanceClientData);

                case LeadsTable.TableName:
                    return da.GetQueryBuilderForSave(entity.LeadsData);
                case LeadsProductTable.TableName:
                    return da.GetQueryBuilderForSave(entity.LeadsProductData);
                case PatientTable.TableName:
                    return da.GetQueryBuilderForSave(entity.PatientData);
                //var p = new PatientApi();
                //var x = p.PatientSaveExternal(entity.PatientData).Result;

                case PaymentTable.TableName:
                    return da.GetQueryBuilderForSave(entity.PaymentData);
                case ProductTable.TableName:
                    return da.GetQueryBuilderForSave(entity.ProductData);
                case ProductStockTable.TableName:
                    entity.ProductStockData.Stock = 0;
                    return da.GetQueryBuilderForSave(entity.ProductStockData);
                case ReplicationDataTable.TableName:
                    return da.GetQueryBuilderForSave(entity.ReplicationEntityData);
                case SalesTable.TableName:
                    return da.GetQueryBuilderForSave(entity.SalesData);
                case SalesItemTable.TableName:
                    return da.GetQueryBuilderForSave(entity.SalesItemData);
                case SalesReturnTable.TableName:
                    return da.GetQueryBuilderForSave(entity.SalesReturnItemData);
                case SalesReturnItemTable.TableName:
                    return da.GetQueryBuilderForSave(entity.SalesReturnItemData);
                case UserAccessTable.TableName:
                    return da.GetQueryBuilderForSave(entity.UserAccessData);
                case VendorTable.TableName:
                    return da.GetQueryBuilderForSave(entity.VendorData);
                case VendorOrderTable.TableName:
                    return da.GetQueryBuilderForSave(entity.VendorOrderData);
                case VendorOrderItemTable.TableName:
                    return da.GetQueryBuilderForSave(entity.VendorOrderItemData);
                case VendorPurchaseTable.TableName:
                    return da.GetQueryBuilderForSave(entity.VendorPurchaseData);
                case VendorPurchaseItemTable.TableName:
                    return da.GetQueryBuilderForSave(entity.VendorPurchaseItemData);
                case VendorReturnTable.TableName:
                    return da.GetQueryBuilderForSave(entity.VendorReturnData);
                case VendorReturnItemTable.TableName:
                    return da.GetQueryBuilderForSave(entity.VendorReturnItemData);
                default:
                    return null;
            }
        }

        private async Task HandleSmsAndEmail(SyncRequest request)
        {
            if (request.Request == null || !request.Request.Any())
            {
                return;
            }

            var salesData = request.Request.Where(r => r.TableName == SalesTable.TableName).Select(r => r.SalesData);
            foreach (var sales in salesData)
            {
                await _helperCommunication.SendSales(sales.Id);
            }

            var vendorOrderData =
                request.Request.Where(r => r.TableName == VendorOrderTable.TableName).Select(r => r.VendorOrderData);
            foreach (var vendorOrder in vendorOrderData)
            {
                await _helperCommunication.SendVenderOrder(vendorOrder.Id);
            }
        }

        private async Task<List<SyncResponseEntity>> ProcessRequests(IEnumerable<SyncRequestEntity> requests)
        {
            var list = new List<SyncResponseEntity>();
            foreach (var entity in requests)
            {
                SyncResponseEntity response = await ProcessRequest(entity);
                list.Add(response);
            }
            return list;
        }

        private async Task<SyncResponseEntity> ProcessRequest(SyncRequestEntity entity)
        {
            var response = new SyncResponseEntity();
            var da = _dataAccessFactory.GetSyncDataAccess(entity.TableName, _instanceClientData.InstanceId, _instanceClientData.AccountId);
            if (entity.TableName == ReplicationDataTable.Table.TableName && entity.Action == (int) OperationType.Select)
                response.ReplicationEntityData =
                    (await _replicationDataAccess.GetReplicationData(entity, _instanceClientData)).ToArray();
            else
                switch (entity.Action)
                {
                    case (int) OperationType.Select:
                        await FetchData(entity, response);
                        break;
                    case (int) OperationType.Delete:
                        await da.DeleteData(entity.EntityId);
                        break;
                    default:
                        await SaveData(entity, da);
                        break;
                }
            return response;
        }

        private async Task SyncOnlineDatabaseToClient(SyncResponseEntity response, SyncRequestEntity request)
        {
            var syncRequest = BuildSyncRequestsForFetchingFirstTimeDataFromOnline(request);
            foreach (var item in syncRequest.Request)
            {
                await FetchData(item, response);
            }
        }

        private SyncRequest BuildSyncRequestsForFetchingFirstTimeDataFromOnline(SyncRequestEntity request)
        {
            var syncRequest = new SyncRequest();
            syncRequest.InstanceClientData = _instanceClientData;
            var requestEntityList = new List<SyncRequestEntity>();

            var accountRequestEntity = new SyncRequestEntity() {TableName = AccountTable.Table.TableName};
            requestEntityList.Add(accountRequestEntity);

            var doctorRequestEntity = new SyncRequestEntity() {TableName = DoctorTable.Table.TableName};
            requestEntityList.Add(doctorRequestEntity);

            var userRequestEntity = new SyncRequestEntity() {TableName = HQueUserTable.Table.TableName};
            requestEntityList.Add(userRequestEntity);

            var instanceRequestEntity = new SyncRequestEntity() {TableName = InstanceTable.Table.TableName};
            requestEntityList.Add(instanceRequestEntity);

            var leadsRequestEntity = new SyncRequestEntity() {TableName = LeadsTable.Table.TableName};
            requestEntityList.Add(leadsRequestEntity);

            //var patientRequestEntity = new SyncRequestEntity() { TableName = PatientTable.Table.TableName };
            //requestEntityList.Add(patientRequestEntity);

            var paymentRequestEntity = new SyncRequestEntity() {TableName = PaymentTable.Table.TableName};
            requestEntityList.Add(paymentRequestEntity);

            var productRequestEntity = new SyncRequestEntity() {TableName = ProductTable.Table.TableName};
            requestEntityList.Add(productRequestEntity);

            var productStockRequestEntity = new SyncRequestEntity() {TableName = ProductStockTable.Table.TableName};
            requestEntityList.Add(productStockRequestEntity);

            var salesRequestEntity = new SyncRequestEntity() {TableName = SalesTable.Table.TableName};
            requestEntityList.Add(salesRequestEntity);

            var salesItemRequestEntity = new SyncRequestEntity() {TableName = SalesItemTable.Table.TableName};
            requestEntityList.Add(salesItemRequestEntity);

            var salesReturnRequestEntity = new SyncRequestEntity() {TableName = SalesReturnTable.Table.TableName};
            requestEntityList.Add(salesReturnRequestEntity);

            var salesReturnItemRequestEntity = new SyncRequestEntity()
            {
                TableName = SalesReturnItemTable.Table.TableName
            };
            requestEntityList.Add(salesReturnItemRequestEntity);

            var userAccessRequestEntity = new SyncRequestEntity() {TableName = UserAccessTable.Table.TableName};
            requestEntityList.Add(userAccessRequestEntity);

            var vendorRequestEntity = new SyncRequestEntity() {TableName = VendorTable.Table.TableName};
            requestEntityList.Add(vendorRequestEntity);

            var vendorOrderRequestEntity = new SyncRequestEntity() {TableName = VendorOrderTable.Table.TableName};
            requestEntityList.Add(vendorOrderRequestEntity);

            var vendorOrderItemRequestEntity = new SyncRequestEntity()
            {
                TableName = VendorOrderItemTable.Table.TableName
            };
            requestEntityList.Add(vendorOrderItemRequestEntity);

            var vendorPurchaseRequestEntity = new SyncRequestEntity() {TableName = VendorPurchaseTable.Table.TableName};
            requestEntityList.Add(vendorPurchaseRequestEntity);

            var vendorPurchaseItemRequestEntity = new SyncRequestEntity()
            {
                TableName = VendorPurchaseItemTable.Table.TableName
            };
            requestEntityList.Add(vendorPurchaseItemRequestEntity);

            var vendorReturnRequestEntity = new SyncRequestEntity() {TableName = VendorReturnTable.Table.TableName};
            requestEntityList.Add(vendorReturnRequestEntity);

            var vendorReturnItemRequestEntity = new SyncRequestEntity()
            {
                TableName = VendorReturnItemTable.Table.TableName
            };
            requestEntityList.Add(vendorReturnItemRequestEntity);

            syncRequest.Request = requestEntityList.ToArray();
            return syncRequest;
        }

        private async Task FetchData(SyncRequestEntity entity, SyncResponseEntity response)
        {
            var da = _dataAccessFactory.GetSyncDataAccess(entity.TableName, _instanceClientData.InstanceId, _instanceClientData.AccountId);
            switch (entity.TableName)
            {
                case AccountTable.TableName:
                    response.AccountData = entity.UseId
                        ? new[] {await da.GetData<BaseAccount>(entity.EntityId)}
                        : (await da.GetList<BaseAccount>()).ToArray();
                    break;
                case DoctorTable.TableName:
                    response.DoctorData = entity.UseId
                        ? new[] {await da.GetData<BaseDoctor>(entity.EntityId)}
                        : (await da.GetList<BaseDoctor>()).ToArray();
                    break;
                case HQueUserTable.TableName:
                    response.HQueUserData = entity.UseId
                        ? new[] {await da.GetData<BaseHQueUser>(entity.EntityId)}
                        : (await da.GetList<BaseHQueUser>()).ToArray();
                    break;
                case InstanceTable.TableName:
                    response.InstanceData = entity.UseId
                        ? new[] {await da.GetData<BaseInstance>(entity.EntityId)}
                        : (await da.GetList<BaseInstance>()).ToArray();
                    break;
                case InstanceClientTable.TableName:
                    response.InstanceClientData = entity.UseId
                        ? new BaseInstanceClient[] {await da.GetData<InstanceClient>(entity.EntityId)}
                        : (await da.GetList<BaseInstanceClient>()).ToArray();
                    break;
                case LeadsTable.TableName:
                    response.LeadsData = entity.UseId
                        ? new[] {await da.GetData<BaseLeads>(entity.EntityId)}
                        : (await da.GetList<BaseLeads>()).ToArray();
                    break;
                case PatientTable.TableName:
                    response.PatientData = entity.UseId
                        ? new[] {await da.GetData<BasePatient>(entity.EntityId)}
                        : (await da.GetList<BasePatient>()).ToArray();
                    break;
                case PaymentTable.TableName:
                    response.PaymentData = entity.UseId
                        ? new[] {await da.GetData<BasePayment>(entity.EntityId)}
                        : (await da.GetList<BasePayment>()).ToArray();
                    break;
                case ProductTable.TableName:
                    response.ProductData = entity.UseId
                        ? new[] {await da.GetData<BaseProduct>(entity.EntityId)}
                        : (await da.GetList<BaseProduct>()).ToArray();
                    break;
                case ProductStockTable.TableName:
                    response.ProductStockData = entity.UseId
                        ? new[] {await da.GetData<BaseProductStock>(entity.EntityId)}
                        : (await da.GetList<BaseProductStock>()).ToArray();
                    break;
                case ReplicationDataTable.TableName:
                    response.ReplicationEntityData = entity.UseId
                        ? new[] {await da.GetData<ReplicationEntity>(entity.EntityId)}
                        : (await da.GetList<ReplicationEntity>()).ToArray();
                    break;
                case SalesTable.TableName:
                    response.SalesData = entity.UseId
                        ? new[] {await da.GetData<BaseSales>(entity.EntityId)}
                        : (await da.GetList<BaseSales>()).ToArray();
                    break;
                case SalesItemTable.TableName:
                    response.SalesItemData = entity.UseId
                        ? new[] {await da.GetData<BaseSalesItem>(entity.EntityId)}
                        : (await da.GetList<BaseSalesItem>()).ToArray();
                    break;
                case SalesReturnTable.TableName:
                    response.SalesReturnData = entity.UseId
                        ? new[] {await da.GetData<BaseSalesReturn>(entity.EntityId)}
                        : (await da.GetList<BaseSalesReturn>()).ToArray();
                    break;
                case SalesReturnItemTable.TableName:
                    response.SalesReturnItemData = entity.UseId
                        ? new[] {await da.GetData<BaseSalesReturnItem>(entity.EntityId)}
                        : (await da.GetList<BaseSalesReturnItem>()).ToArray();
                    break;
                case UserAccessTable.TableName:
                    response.UserAccessData = entity.UseId
                        ? new[] {await da.GetData<BaseUserAccess>(entity.EntityId)}
                        : (await da.GetList<BaseUserAccess>()).ToArray();
                    break;
                case VendorTable.TableName:
                    response.VendorData = entity.UseId
                        ? new[] {await da.GetData<BaseVendor>(entity.EntityId)}
                        : (await da.GetList<BaseVendor>()).ToArray();
                    break;
                case VendorOrderTable.TableName:
                    response.VendorOrderData = entity.UseId
                        ? new[] {await da.GetData<BaseVendorOrder>(entity.EntityId)}
                        : (await da.GetList<BaseVendorOrder>()).ToArray();
                    break;
                case VendorOrderItemTable.TableName:
                    response.VendorOrderItemData = entity.UseId
                        ? new[] {await da.GetData<BaseVendorOrderItem>(entity.EntityId)}
                        : (await da.GetList<BaseVendorOrderItem>()).ToArray();
                    break;
                case VendorPurchaseTable.TableName:
                    response.VendorPurchaseData = entity.UseId
                        ? new[] {await da.GetData<BaseVendorPurchase>(entity.EntityId)}
                        : (await da.GetList<BaseVendorPurchase>()).ToArray();
                    break;
                case VendorPurchaseItemTable.TableName:
                    response.VendorPurchaseItemData = entity.UseId
                        ? new[] {await da.GetData<BaseVendorPurchaseItem>(entity.EntityId)}
                        : (await da.GetList<BaseVendorPurchaseItem>()).ToArray();
                    break;
                case VendorReturnTable.TableName:
                    response.VendorReturnData = entity.UseId
                        ? new[] {await da.GetData<BaseVendorReturn>(entity.EntityId)}
                        : (await da.GetList<BaseVendorReturn>()).ToArray();
                    break;
                case VendorReturnItemTable.TableName:
                    response.VendorReturnItemData = entity.UseId
                        ? new[] {await da.GetData<BaseVendorReturnItem>(entity.EntityId)}
                        : (await da.GetList<BaseVendorReturnItem>()).ToArray();
                    break;
            }
        }

        public async Task SaveData(SyncRequestEntity entity, ISyncDataAccess<BaseContract> da)
        {
            switch (entity.TableName)
            {
                case AccountTable.TableName:
                    await da.SaveData(entity.AccountData);
                    break;
                case DoctorTable.TableName:
                    await da.SaveData(entity.DoctorData);
                    break;
                case HQueUserTable.TableName:
                    await da.SaveData(entity.HQueUserData);
                    break;
                case InstanceTable.TableName:
                    await da.SaveData(entity.InstanceData);
                    break;
                case InstanceClientTable.TableName:
                    await da.SaveData(entity.InstanceClientData);
                    break;
                case LeadsTable.TableName:
                    await da.SaveData(entity.LeadsData);
                    break;
                case LeadsProductTable.TableName:
                    await da.SaveData(entity.LeadsProductData);
                    break;
                case PatientTable.TableName:
                    await da.SaveData(entity.PatientData);
                    //var p = new PatientApi();
                    //var x = p.PatientSaveExternal(entity.PatientData).Result;
                    break;
                case PaymentTable.TableName:
                    await da.SaveData(entity.PaymentData);
                    break;
                case ProductTable.TableName:
                    await da.SaveData(entity.ProductData);
                    break;
                case ProductStockTable.TableName:
                    await da.SaveData(entity.ProductStockData);
                    break;
                case ReplicationDataTable.TableName:
                    await da.SaveData(entity.ReplicationEntityData);
                    break;
                case SalesTable.TableName:
                    await da.SaveData(entity.SalesData);
                    break;
                case SalesItemTable.TableName:
                    await da.SaveData(entity.SalesItemData);
                    break;
                case SalesReturnTable.TableName:
                    await da.SaveData(entity.SalesReturnItemData);
                    break;
                case SalesReturnItemTable.TableName:
                    await da.SaveData(entity.SalesReturnItemData);
                    break;
                case UserAccessTable.TableName:
                    await da.SaveData(entity.UserAccessData);
                    break;
                case VendorTable.TableName:
                    await da.SaveData(entity.VendorData);
                    break;
                case VendorOrderTable.TableName:
                    await da.SaveData(entity.VendorOrderData);
                    break;
                case VendorOrderItemTable.TableName:
                    await da.SaveData(entity.VendorOrderItemData);
                    break;
                case VendorPurchaseTable.TableName:
                    await da.SaveData(entity.VendorPurchaseData);
                    break;
                case VendorPurchaseItemTable.TableName:
                    await da.SaveData(entity.VendorPurchaseItemData);
                    break;
                case VendorReturnTable.TableName:
                    await da.SaveData(entity.VendorReturnData);
                    break;
                case VendorReturnItemTable.TableName:
                    await da.SaveData(entity.VendorReturnItemData);
                    break;
            }
        }

        public async Task PopulatePushListWithData(List<SyncRequestEntity> pushList,
            ISyncDataAccessFactory syncDataAccessFactory)
        {
            //This check is needed as this method is getting called from replication manager directly and instance client object is not set
            if (pushList.Count > 0)
                _instanceClientData = _instanceClientData ?? pushList[0].InstanceClientData;
            foreach (
                var entity in
                    pushList.Where(
                        entity =>
                            entity.Action == (int) OperationType.Insert || entity.Action == (int) OperationType.Update))
            {
                switch (entity.TableName)
                {
                    case AccountTable.TableName:
                        entity.AccountData =
                            await
                                syncDataAccessFactory.GetSyncDataAccess(entity.TableName, _instanceClientData.InstanceId, _instanceClientData.AccountId)
                                    .GetData<BaseAccount>(entity.EntityId);
                        break;
                    case DoctorTable.TableName:
                        entity.DoctorData =
                            await
                                syncDataAccessFactory.GetSyncDataAccess(entity.TableName, _instanceClientData.InstanceId, _instanceClientData.AccountId)
                                    .GetData<BaseDoctor>(entity.EntityId);
                        break;
                    case HQueUserTable.TableName:
                        entity.HQueUserData =
                            await
                                syncDataAccessFactory.GetSyncDataAccess(entity.TableName, _instanceClientData.InstanceId, _instanceClientData.AccountId)
                                    .GetData<BaseHQueUser>(entity.EntityId);
                        break;
                    case InstanceTable.TableName:
                        entity.InstanceData =
                            await
                                syncDataAccessFactory.GetSyncDataAccess(entity.TableName, _instanceClientData.InstanceId, _instanceClientData.AccountId)
                                    .GetData<BaseInstance>(entity.EntityId);
                        break;
                    case InstanceClientTable.TableName:
                        entity.InstanceClientData =
                            await
                                syncDataAccessFactory.GetSyncDataAccess(entity.TableName, _instanceClientData.InstanceId, _instanceClientData.AccountId)
                                    .GetData<BaseInstanceClient>(entity.EntityId);
                        break;
                    case LeadsTable.TableName:
                        entity.LeadsData =
                            await
                                syncDataAccessFactory.GetSyncDataAccess(entity.TableName, _instanceClientData.InstanceId, _instanceClientData.AccountId)
                                    .GetData<BaseLeads>(entity.EntityId);
                        break;
                    case LeadsProductTable.TableName:
                        entity.LeadsProductData =
                            await
                                syncDataAccessFactory.GetSyncDataAccess(entity.TableName, _instanceClientData.InstanceId, _instanceClientData.AccountId)
                                    .GetData<BaseLeadsProduct>(entity.EntityId);
                        break;
                    case PatientTable.TableName:
                        entity.PatientData =
                            await
                                syncDataAccessFactory.GetSyncDataAccess(entity.TableName, _instanceClientData.InstanceId, _instanceClientData.AccountId)
                                    .GetData<BasePatient>(entity.EntityId);
                        break;
                    case PaymentTable.TableName:
                        entity.PaymentData =
                            await
                                syncDataAccessFactory.GetSyncDataAccess(entity.TableName, _instanceClientData.InstanceId, _instanceClientData.AccountId)
                                    .GetData<BasePayment>(entity.EntityId);
                        break;
                    case ProductTable.TableName:
                        entity.ProductData =
                            await
                                syncDataAccessFactory.GetSyncDataAccess(entity.TableName, _instanceClientData.InstanceId, _instanceClientData.AccountId)
                                    .GetData<BaseProduct>(entity.EntityId);
                        break;
                    case ProductStockTable.TableName:
                        entity.ProductStockData =
                            await
                                syncDataAccessFactory.GetSyncDataAccess(entity.TableName, _instanceClientData.InstanceId, _instanceClientData.AccountId)
                                    .GetData<BaseProductStock>(entity.EntityId);
                        break;
                    case ReplicationDataTable.TableName:
                        entity.ReplicationEntityData =
                            await
                                syncDataAccessFactory.GetSyncDataAccess(entity.TableName, _instanceClientData.InstanceId, _instanceClientData.AccountId)
                                    .GetData<ReplicationEntity>(entity.EntityId);
                        break;
                    case SalesTable.TableName:
                        entity.SalesData =
                            await
                                syncDataAccessFactory.GetSyncDataAccess(entity.TableName, _instanceClientData.InstanceId, _instanceClientData.AccountId)
                                    .GetData<Sales>(entity.EntityId);
                        break;
                    case SalesItemTable.TableName:
                        entity.SalesItemData =
                            await
                                syncDataAccessFactory.GetSyncDataAccess(entity.TableName, _instanceClientData.InstanceId, _instanceClientData.AccountId)
                                    .GetData<SalesItem>(entity.EntityId);
                        break;
                    case SalesReturnTable.TableName:
                        entity.SalesReturnData =
                            await
                                syncDataAccessFactory.GetSyncDataAccess(entity.TableName, _instanceClientData.InstanceId, _instanceClientData.AccountId)
                                    .GetData<BaseSalesReturn>(entity.EntityId);
                        break;
                    case SalesReturnItemTable.TableName:
                        entity.SalesReturnItemData =
                            await
                                syncDataAccessFactory.GetSyncDataAccess(entity.TableName, _instanceClientData.InstanceId, _instanceClientData.AccountId)
                                    .GetData<BaseSalesReturnItem>(entity.EntityId);
                        break;
                    case UserAccessTable.TableName:
                        entity.UserAccessData =
                            await
                                syncDataAccessFactory.GetSyncDataAccess(entity.TableName, _instanceClientData.InstanceId, _instanceClientData.AccountId)
                                    .GetData<BaseUserAccess>(entity.EntityId);
                        break;
                    case VendorTable.TableName:
                        entity.VendorData =
                            await
                                syncDataAccessFactory.GetSyncDataAccess(entity.TableName, _instanceClientData.InstanceId, _instanceClientData.AccountId)
                                    .GetData<BaseVendor>(entity.EntityId);
                        break;
                    case VendorOrderTable.TableName:
                        entity.VendorOrderData =
                            await
                                syncDataAccessFactory.GetSyncDataAccess(entity.TableName, _instanceClientData.InstanceId, _instanceClientData.AccountId)
                                    .GetData<BaseVendorOrder>(entity.EntityId);
                        break;
                    case VendorOrderItemTable.TableName:
                        entity.VendorOrderItemData =
                            await
                                syncDataAccessFactory.GetSyncDataAccess(entity.TableName, _instanceClientData.InstanceId, _instanceClientData.AccountId)
                                    .GetData<BaseVendorOrderItem>(entity.EntityId);
                        break;
                    case VendorPurchaseTable.TableName:
                        entity.VendorPurchaseData =
                            await
                                syncDataAccessFactory.GetSyncDataAccess(entity.TableName, _instanceClientData.InstanceId, _instanceClientData.AccountId)
                                    .GetData<BaseVendorPurchase>(entity.EntityId);
                        break;
                    case VendorPurchaseItemTable.TableName:
                        entity.VendorPurchaseItemData =
                            await
                                syncDataAccessFactory.GetSyncDataAccess(entity.TableName, _instanceClientData.InstanceId, _instanceClientData.AccountId)
                                    .GetData<BaseVendorPurchaseItem>(entity.EntityId);
                        break;
                    case VendorReturnTable.TableName:
                        entity.VendorReturnData =
                            await
                                syncDataAccessFactory.GetSyncDataAccess(entity.TableName, _instanceClientData.InstanceId, _instanceClientData.AccountId)
                                    .GetData<BaseVendorReturn>(entity.EntityId);
                        break;
                    case VendorReturnItemTable.TableName:
                        entity.VendorReturnItemData =
                            await
                                syncDataAccessFactory.GetSyncDataAccess(entity.TableName, _instanceClientData.InstanceId, _instanceClientData.AccountId)
                                    .GetData<BaseVendorReturnItem>(entity.EntityId);
                        break;
                    default:
                        break;
                }
            }
        }

        public void PopulatePullListWithResponseData(List<SyncRequestEntity> pullList, SyncResponse result)
        {
            var i = 0;
            foreach (var entity in pullList)
            {
                switch (entity.TableName)
                {
                    case AccountTable.TableName:
                        entity.AccountData = (result.Response[i].AccountData != null)
                            ? result.Response[i].AccountData[0]
                            : null;
                        break;
                    case DoctorTable.TableName:
                        entity.DoctorData = (result.Response[i].DoctorData != null)
                            ? result.Response[i].DoctorData[0]
                            : null;
                        break;
                    case HQueUserTable.TableName:
                        entity.HQueUserData = (result.Response[i].HQueUserData != null)
                            ? result.Response[i].HQueUserData[0]
                            : null;
                        break;
                    case InstanceTable.TableName:
                        entity.InstanceData = (result.Response[i].InstanceData != null)
                            ? result.Response[i].InstanceData[0]
                            : null;
                        break;
                    case InstanceClientTable.TableName:
                        entity.InstanceClientData = (result.Response[i].InstanceClientData != null)
                            ? result.Response[i].InstanceClientData[0]
                            : null;
                        break;
                    case LeadsTable.TableName:
                        entity.LeadsData = (result.Response[i].LeadsData != null)
                            ? result.Response[i].LeadsData[0]
                            : null;
                        break;
                    case LeadsProductTable.TableName:
                        entity.LeadsProductData = (result.Response[i].LeadsProductData != null)
                            ? result.Response[i].LeadsProductData[0]
                            : null;
                        break;
                    case PatientTable.TableName:
                        entity.PatientData = (result.Response[i].PatientData != null)
                            ? result.Response[i].PatientData[0]
                            : null;
                        break;
                    case PaymentTable.TableName:
                        entity.PaymentData = (result.Response[i].PaymentData != null)
                            ? result.Response[i].PaymentData[0]
                            : null;
                        break;
                    case ProductTable.TableName:
                        entity.ProductData = (result.Response[i].ProductData != null)
                            ? result.Response[i].ProductData[0]
                            : null;
                        break;
                    case ProductStockTable.TableName:
                        entity.ProductStockData = (result.Response[i].ProductStockData != null)
                            ? result.Response[i].ProductStockData[0]
                            : null;
                        break;
                    case ReplicationDataTable.TableName:
                        entity.ReplicationEntityData = (result.Response[i].ReplicationEntityData != null)
                            ? result.Response[i].ReplicationEntityData[0]
                            : null;
                        break;
                    case SalesTable.TableName:
                        entity.SalesData = (result.Response[i].SalesData != null)
                            ? result.Response[i].SalesData[0]
                            : null;
                        break;
                    case SalesItemTable.TableName:
                        entity.SalesItemData = (result.Response[i].SalesItemData != null)
                            ? result.Response[i].SalesItemData[0]
                            : null;
                        break;
                    case SalesReturnTable.TableName:
                        entity.SalesReturnData = (result.Response[i].SalesReturnData != null)
                            ? result.Response[i].SalesReturnData[0]
                            : null;
                        break;
                    case SalesReturnItemTable.TableName:
                        entity.SalesReturnItemData = (result.Response[i].SalesReturnItemData != null)
                            ? result.Response[i].SalesReturnItemData[0]
                            : null;
                        break;
                    case UserAccessTable.TableName:
                        entity.UserAccessData = (result.Response[i].UserAccessData != null)
                            ? result.Response[i].UserAccessData[0]
                            : null;
                        break;
                    case VendorTable.TableName:
                        entity.VendorData = (result.Response[i].VendorData != null)
                            ? result.Response[i].VendorData[0]
                            : null;
                        break;
                    case VendorOrderTable.TableName:
                        entity.VendorOrderData = (result.Response[i].VendorOrderData != null)
                            ? result.Response[i].VendorOrderData[0]
                            : null;
                        break;
                    case VendorOrderItemTable.TableName:
                        entity.VendorOrderItemData = (result.Response[i].VendorOrderItemData != null)
                            ? result.Response[i].VendorOrderItemData[0]
                            : null;
                        break;
                    case VendorPurchaseTable.TableName:
                        entity.VendorPurchaseData = (result.Response[i].VendorPurchaseData != null)
                            ? result.Response[i].VendorPurchaseData[0]
                            : null;
                        break;
                    case VendorPurchaseItemTable.TableName:
                        entity.VendorPurchaseItemData = (result.Response[i].VendorPurchaseItemData != null)
                            ? result.Response[i].VendorPurchaseItemData[0]
                            : null;
                        break;
                    case VendorReturnTable.TableName:
                        entity.VendorReturnData = (result.Response[i].VendorReturnData != null)
                            ? result.Response[i].VendorReturnData[0]
                            : null;
                        break;
                    case VendorReturnItemTable.TableName:
                        entity.VendorReturnItemData = (result.Response[i].VendorReturnItemData != null)
                            ? result.Response[i].VendorReturnItemData[0]
                            : null;
                        break;
                }
                i++;
            }
        }

        public async Task SyncOnlineDataToTheClient(SyncResponseEntity syncResponse, string instanceId,string accountId)
        {
            if (syncResponse.AccountData != null && syncResponse.AccountData.Length > 0)
            {
                var da = _dataAccessFactory.GetSyncDataAccess(AccountTable.TableName, instanceId, accountId);
                foreach (var item in syncResponse.AccountData)
                    await da.SaveData(item);
            }
            if (syncResponse.DoctorData != null && syncResponse.DoctorData.Length > 0)
            {
                var da = _dataAccessFactory.GetSyncDataAccess(DoctorTable.TableName, instanceId, accountId);
                foreach (var item in syncResponse.DoctorData)
                    await da.SaveData(item);
            }
            if (syncResponse.HQueUserData != null && syncResponse.HQueUserData.Length > 0)
            {
                var da = _dataAccessFactory.GetSyncDataAccess(HQueUserTable.TableName, instanceId, accountId);
                foreach (var item in syncResponse.HQueUserData)
                    await da.SaveData(item);
            }
            if (syncResponse.InstanceData != null && syncResponse.InstanceData.Length > 0)
            {
                var da = _dataAccessFactory.GetSyncDataAccess(InstanceTable.TableName, instanceId, accountId);
                foreach (var item in syncResponse.InstanceData)
                    await da.SaveData(item);
            }
            if (syncResponse.InstanceClientData != null && syncResponse.InstanceClientData.Length > 0)
            {
                var da = _dataAccessFactory.GetSyncDataAccess(InstanceClientTable.TableName, instanceId, accountId);
                foreach (var item in syncResponse.InstanceClientData)
                    await da.SaveData(item);
            }
            if (syncResponse.LeadsData != null && syncResponse.LeadsData.Length > 0)
            {
                var da = _dataAccessFactory.GetSyncDataAccess(LeadsTable.TableName, instanceId, accountId);
                foreach (var item in syncResponse.LeadsData)
                    await da.SaveData(item);
            }
            if (syncResponse.LeadsProductData != null && syncResponse.LeadsProductData.Length > 0)
            {
                var da = _dataAccessFactory.GetSyncDataAccess(LeadsProductTable.TableName, instanceId, accountId);
                foreach (var item in syncResponse.LeadsProductData)
                    await da.SaveData(item);
            }
            if (syncResponse.PatientData != null && syncResponse.PatientData.Length > 0)
            {
                var da = _dataAccessFactory.GetSyncDataAccess(PatientTable.TableName, instanceId, accountId);
                foreach (var item in syncResponse.PatientData)
                    await da.SaveData(item);
            }
            if (syncResponse.PaymentData != null && syncResponse.PaymentData.Length > 0)
            {
                var da = _dataAccessFactory.GetSyncDataAccess(PaymentTable.TableName, instanceId, accountId);
                foreach (var item in syncResponse.PaymentData)
                    await da.SaveData(item);
            }

            if (syncResponse.ProductData != null && syncResponse.ProductData.Length > 0)
            {
                var da = _dataAccessFactory.GetSyncDataAccess(ProductTable.TableName, instanceId, accountId);
                foreach (var item in syncResponse.ProductData)
                    await da.SaveData(item);
            }
            if (syncResponse.ProductStockData != null && syncResponse.ProductStockData.Length > 0)
            {
                var da = _dataAccessFactory.GetSyncDataAccess(ProductStockTable.TableName, instanceId, accountId);
                foreach (var item in syncResponse.ProductStockData)
                    await da.SaveData(item);
            }
            if (syncResponse.ReplicationEntityData != null && syncResponse.ReplicationEntityData.Length > 0)
            {
                var da = _dataAccessFactory.GetSyncDataAccess(ReplicationDataTable.TableName, instanceId, accountId);
                foreach (var item in syncResponse.ReplicationEntityData)
                    await da.SaveData(item);
            }
            if (syncResponse.SalesData != null && syncResponse.SalesData.Length > 0)
            {
                var da = _dataAccessFactory.GetSyncDataAccess(SalesTable.TableName, instanceId, accountId);
                foreach (var item in syncResponse.SalesData)
                    await da.SaveData(item);
            }
            if (syncResponse.SalesItemData != null && syncResponse.SalesItemData.Length > 0)
            {
                var da = _dataAccessFactory.GetSyncDataAccess(SalesItemTable.TableName, instanceId, accountId);
                foreach (var item in syncResponse.SalesItemData)
                    await da.SaveData(item);
            }
            if (syncResponse.SalesReturnData != null && syncResponse.SalesReturnData.Length > 0)
            {
                var da = _dataAccessFactory.GetSyncDataAccess(SalesReturnTable.TableName, instanceId, accountId);
                foreach (var item in syncResponse.SalesReturnData)
                    await da.SaveData(item);
            }
            if (syncResponse.SalesReturnItemData != null && syncResponse.SalesReturnItemData.Length > 0)
            {
                var da = _dataAccessFactory.GetSyncDataAccess(SalesReturnItemTable.TableName, instanceId, accountId);
                foreach (var item in syncResponse.SalesReturnItemData)
                    await da.SaveData(item);
            }
            if (syncResponse.UserAccessData != null && syncResponse.UserAccessData.Length > 0)
            {
                var da = _dataAccessFactory.GetSyncDataAccess(UserAccessTable.TableName, instanceId, accountId);
                foreach (var item in syncResponse.UserAccessData)
                    await da.SaveData(item);
            }
            if (syncResponse.VendorData != null && syncResponse.VendorData.Length > 0)
            {
                var da = _dataAccessFactory.GetSyncDataAccess(VendorTable.TableName, instanceId, accountId);
                foreach (var item in syncResponse.VendorData)
                    await da.SaveData(item);
            }
            if (syncResponse.VendorOrderData != null && syncResponse.VendorOrderData.Length > 0)
            {
                var da = _dataAccessFactory.GetSyncDataAccess(VendorOrderTable.TableName, instanceId, accountId);
                foreach (var item in syncResponse.VendorOrderData)
                    await da.SaveData(item);
            }
            if (syncResponse.VendorOrderItemData != null && syncResponse.VendorOrderItemData.Length > 0)
            {
                var da = _dataAccessFactory.GetSyncDataAccess(VendorOrderItemTable.TableName, instanceId, accountId);
                foreach (var item in syncResponse.VendorOrderItemData)
                    await da.SaveData(item);
            }
            if (syncResponse.VendorPurchaseData != null && syncResponse.VendorPurchaseData.Length > 0)
            {
                var da = _dataAccessFactory.GetSyncDataAccess(VendorPurchaseTable.TableName, instanceId, accountId);
                foreach (var item in syncResponse.VendorPurchaseData)
                    await da.SaveData(item);
            }
            if (syncResponse.VendorPurchaseItemData != null && syncResponse.VendorPurchaseItemData.Length > 0)
            {
                var da = _dataAccessFactory.GetSyncDataAccess(VendorPurchaseItemTable.TableName, instanceId, accountId);
                foreach (var item in syncResponse.VendorPurchaseItemData)
                    await da.SaveData(item);
            }
            if (syncResponse.VendorReturnData != null && syncResponse.VendorReturnData.Length > 0)
            {
                var da = _dataAccessFactory.GetSyncDataAccess(VendorReturnTable.TableName, instanceId, accountId);
                foreach (var item in syncResponse.VendorReturnData)
                    await da.SaveData(item);
            }
            if (syncResponse.VendorReturnItemData != null && syncResponse.VendorReturnItemData.Length > 0)
            {
                var da = _dataAccessFactory.GetSyncDataAccess(VendorReturnItemTable.TableName, instanceId, accountId);
                foreach (var item in syncResponse.VendorReturnItemData)
                    await da.SaveData(item);
            }
        }
    }
}