﻿namespace Utilities.Repository.Interface
{
    public interface IMessageRepository
    {
        string GetMessage(int result, params string[] data);
    }
}