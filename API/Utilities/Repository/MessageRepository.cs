﻿using System.Collections.Generic;
using System.Linq;
using Utilities.Enum;
using Utilities.Logger;
using Utilities.Repository.Interface;

namespace Utilities.Repository
{
    public class MessageRepository : IMessageRepository
    {
        private readonly IDictionary<int, string> _messageList;

        public MessageRepository()
        {

            _messageList = new Dictionary<int, string>
                           {
                               {MessageId.Misc, "{0}"},
                               {MessageId.NoResult, "There is something wrong"},
                               {MessageId.DataSaved, "{0} saved successfully"},
                               {MessageId.DataUpdated, "{0} updated successfully"},
                               {MessageId.InvalidUser, "Invalid UserId or Password"},
                               {MessageId.InvalidOldPassword, "Invalid old Password"},
                               {MessageId.UserExists, "The user with the same name exists"},
                               {MessageId.DataDeleted, "{0} deleted successfully"},
                               {MessageId.ComplaintCreated, "The complaint is registred. The complaint no. is {0}"},
                               {
                                   MessageId.UserCreatedProfileComplet,
                                   "User created successfully, Please complete your profile"
                               }
                           };
        }

        public string GetMessage(int messageId, params string[] messageParameter)
        {

            LogAndException.WriteToLogger(messageId.ToString(), ErrorLevel.Debug);
            string message;
            _messageList.TryGetValue(messageId, out message);

            switch (messageId)
            {
                case MessageId.DataSaved:
                case MessageId.DataUpdated:
                case MessageId.DataDeleted:
                    message = string.Format(message, (messageParameter.Any() ? messageParameter : new object[] {"Data"}));
                    break;
                default:
                    message = string.Format(message, messageParameter);
                    break;
            }
            return message;
        }
    }
}