﻿using System.Collections.Generic;
using System.Linq;

namespace Utilities.Repository
{
    public class FieldOptionRepository
    {
        private readonly List<dynamic> _fieldOption = new List<dynamic>();

        public FieldOptionRepository()
        {
            _fieldOption.Add(new {FieldOption = OptionType.MaritalStatus, Value = "Married", Text = "Married"});
            _fieldOption.Add(new {FieldOption = OptionType.MaritalStatus, Value = "Unmarried", Text = "Unmarried"});
            _fieldOption.Add(new {FieldOption = OptionType.MaritalStatus, Value = "Child", Text = "Child"});

            _fieldOption.Add(new {FieldOption = OptionType.Gender, Value = "Male", Text = "Male"});
            _fieldOption.Add(new {FieldOption = OptionType.Gender, Value = "Female", Text = "Female"});

            /*
             _fieldOption.Add(new {FieldOption = OptionType.Location, Value = "Malleshwaram", Text = "Malleshwaram"});
            _fieldOption.Add(new {FieldOption = OptionType.Location, Value = "Marathahalli", Text = "Marathahalli"});
            _fieldOption.Add(new {FieldOption = OptionType.Location, Value = "CRHRF", Text = "CRHRF"});
            */

            _fieldOption.Add(new {FieldOption = OptionType.Location, Value = "Allahabad", Text = "Allahabad"});

            _fieldOption.Add(new {FieldOption = OptionType.Status, Value = "Active", Text = "Active"});
            _fieldOption.Add(new {FieldOption = OptionType.Status, Value = "In-Active", Text = "In-Active"});

            _fieldOption.Add(new {FieldOption = OptionType.UserType, Value = "Main Doctor", Text = "Main Doctor"});
            _fieldOption.Add(new {FieldOption = OptionType.UserType, Value = "Jr. Doctor", Text = "Jr. Doctor"});
            _fieldOption.Add(new {FieldOption = OptionType.UserType, Value = "Dispatch", Text = "Dispatch"});
            _fieldOption.Add(new {FieldOption = OptionType.UserType, Value = "Admin", Text = "Admin"});
        }


        public List<dynamic> GetFieldOption(string optionType)
        {
            IEnumerable<dynamic> query = from x in _fieldOption
                where x.FieldOption == optionType
                select new {x.Value, x.Text} as dynamic;

            List<dynamic> list = query.ToList();

            return list;
        }
    }

    public static class OptionType
    {
        public const string MaritalStatus = "MaritalStatus";
        public const string Gender = "Gender";
        public const string Location = "Location";
        public const string Status = "Status";
        public const string UserType = "UserType";
    }
}