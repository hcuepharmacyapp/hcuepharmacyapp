﻿using System;
using System.Text;

namespace Utilities.Report
{
    public enum Alignment
    {
        Center = 0,
        Left = 1,
        Right = 2
    }

    public enum TextAdjustment
    {
        Wrap = 0,
        NewLine = 1
    }

    public class PlainTextReport
    {
        public int Columns { get; set; }
        public int Rows { get; set; }
        public StringBuilder Content { get; set; }

        StringBuilder lines = new StringBuilder();
        int colPrevOffset = 0;
        string lineExtra = "";
        TextAdjustment wrapMode;

        public PlainTextReport()
        {
            Content = new StringBuilder();
        }

        public void NewLine()
        {
            if (!string.IsNullOrEmpty(lines.ToString()))
                this.Content.AppendLine(lines.ToString());
            //this.Content.AppendLine();

            lines = new StringBuilder();

            if (lineExtra.Length > 0)
            {
                switch (wrapMode)
                {
                    case TextAdjustment.Wrap:
                        lines.Append(lineExtra);
                        break;
                    case TextAdjustment.NewLine:
                        this.Content.AppendLine(lineExtra);
                        lines.Append("".PadRight(Columns, ' '));
                        break;
                }
                lineExtra = "";
                return;
            }

            
            lines.Append("".PadRight(Columns, ' '));
            colPrevOffset = 0;
        }

        public void Write()
        {
            
            this.Content.AppendLine(lines.ToString());
            lines = new StringBuilder();

            if (lineExtra.Length > 0)
            {
                switch (wrapMode)
                {
                    case TextAdjustment.Wrap:
                        lines.Append(lineExtra);
                        break;
                    case TextAdjustment.NewLine:
                        this.Content.AppendLine(lineExtra);
                        break;
                }
                lineExtra = "";
                return;
            }
        }

        public void WriteLine()
        {
            this.Content.Append(Environment.NewLine);
        }

        public void Write(string data, int columnWidth, Alignment align)
        {
            if (string.IsNullOrEmpty(data))
                return;

            switch (align)
            {
                case Alignment.Center:
                    int padlen = (columnWidth - data.Length) / 2;
                    data = data.PadLeft(padlen + data.Length, ' ');
                    data = data.PadRight(padlen + data.Length + (data.Length % 2), ' ');
                    break;
            }


            lines.Remove(0, columnWidth);
            if (data.Length > columnWidth)
                lines.Insert(0, data.Substring(0, columnWidth));
            else
                lines.Insert(0, data);

        }

        public void Write(string data, int columnWidth, Alignment align, TextAdjustment adjust)
        {
            if (string.IsNullOrEmpty(data))
                return;

            var tmp = "";
            if (data.Length > columnWidth)
            {
                tmp = data;
                var str = data.Substring(0, columnWidth);
                int idx = str.LastIndexOf(" ");
                data = data.Substring(0, idx);
                tmp = tmp.Substring(idx);
            }

            Write(data, columnWidth, 0, align);

            lineExtra = "";
            if (tmp.Length > 0)
            {
                lineExtra = "".PadRight(Columns, ' ');
                if (tmp.Length > columnWidth)
                    tmp = Textalign(tmp.Substring(0, columnWidth), columnWidth, align);
                else
                    tmp = Textalign(tmp, columnWidth, align);

                lineExtra = lineExtra.Remove(0, columnWidth);
                lineExtra = lineExtra.Insert(0, tmp.PadRight(columnWidth, ' '));
                wrapMode = adjust;
            }

        }

        public void Write(string data, int columnWidth, int offset, Alignment align, int lineNo)
        {
            if (string.IsNullOrEmpty(data) || offset > Columns)
                return;

            switch (align)
            {
                case Alignment.Center:
                    int padlen = (columnWidth - data.Length) / 2;
                    data = data.PadLeft(padlen + data.Length, ' ');
                    data = data.PadRight(padlen + data.Length + (data.Length % 2), ' ');
                    break;
                case Alignment.Right:
                    data = data.PadLeft(columnWidth, ' ');
                    break;
                case Alignment.Left:
                    data = data.PadRight(columnWidth, ' ');
                    break;
            }

            GetLine(lineNo);

            if (string.IsNullOrEmpty(lines.ToString()))
                NewLine();

            if (data.Length + offset > Columns)
                lines.Remove(offset, Columns - offset);
            else
                lines.Remove(offset, columnWidth);

            if (data.Length + offset > Columns)
                lines.Insert(offset, data.Substring(0, Columns - offset));
            else if (data.Length > columnWidth)
                lines.Insert(offset, data.Substring(0, columnWidth));
            else
                lines.Insert(offset, data);

            setLine(lineNo, lines.ToString());
        }

        public void Write(string data, int columnWidth, int offset, Alignment align)
        {
            if (string.IsNullOrEmpty(data) || offset > Columns)
                return;

            switch (align)
            {
                case Alignment.Center:
                    int padlen = (columnWidth - data.Length) / 2;
                    data = data.PadLeft(padlen + data.Length, ' ');
                    data = data.PadRight(padlen + data.Length + (data.Length % 2), ' ');
                    break;
                case Alignment.Right:
                    data = data.PadLeft(columnWidth, ' ');
                    break;
                case Alignment.Left:
                    data = data.PadRight(columnWidth, ' ');
                    break;
            }

            if (string.IsNullOrEmpty(lines.ToString()))
                NewLine();

            if(lines.ToString().Length < Columns)
                lines.Append("".PadRight(Columns - lines.ToString().Length, ' '));

            if (data.Length + offset >= Columns)
                lines.Remove(offset, Columns - offset);
            else
                lines.Remove(offset, columnWidth);

            if (data.Length + offset >= Columns)
                lines.Insert(offset, data.Substring(0, Columns - offset));
            else if (data.Length > columnWidth)
                lines.Insert(offset, data.Substring(0, columnWidth));
            else
                lines.Insert(offset, data);
        }
        public void Write(string data, int columnWidth, int offset, Alignment align, bool isBold)
        {
            if (string.IsNullOrEmpty(data) || offset > Columns)
                return;

            switch (align)
            {
                case Alignment.Center:
                    int padlen = (columnWidth - data.Length) / 2;
                    //if(isBold==true)
                    //{
                    //    data = data.PadLeft((padlen-4) + data.Length, ' ');
                    //}
                    //else
                    //{
                    //    data = data.PadLeft(padlen + data.Length, ' ');
                    //}
                    data = data.PadLeft(padlen + data.Length, ' ');
                    data = data.PadRight(padlen + data.Length + (data.Length % 2), ' ');
                    break;
                case Alignment.Right:
                    data = data.PadLeft(columnWidth, ' ');
                    break;
                case Alignment.Left:
                    data = data.PadRight(columnWidth, ' ');
                    break;
            }

            if (string.IsNullOrEmpty(lines.ToString()))
                NewLine();


            //if (isBold == true)
            //{
            //    data = "#BB#" + data + "#BE#";
            //}

            if (lines.ToString().Length < Columns)
                lines.Append("".PadRight(Columns - lines.ToString().Length, ' '));

            if (data.Length + offset >= Columns)
                lines.Remove(offset, Columns - offset);
            else
                lines.Remove(offset, columnWidth);

            if (data.Length + offset >= Columns)
                lines.Insert(offset, !isBold? data.Substring(0, Columns - offset) : string.Format("#BB#{0}#BE#",data.Substring(0, Columns - offset)));
            else if (data.Length > columnWidth)
                lines.Insert(offset, !isBold ? data.Substring(0, columnWidth) : string.Format("#BB#{0}#BE#", data.Substring(0, columnWidth)));
            else
                lines.Insert(offset, !isBold ? data : string.Format("#BB#{0}#BE#", data));
        }

        public void Write(string data, int columnWidth, int offset, Alignment align, bool isBold, bool isDblBold)
        {
            if (string.IsNullOrEmpty(data) || offset > Columns)
                return;

            switch (align)
            {
                case Alignment.Center:
                    int padlen = (columnWidth - data.Length) / 2;
                    
                    data = data.PadLeft(padlen + data.Length, ' ');
                    data = data.PadRight(padlen + data.Length + (data.Length % 2), ' ');
                    break;
                case Alignment.Right:
                    data = data.PadLeft(columnWidth, ' ');
                    break;
                case Alignment.Left:
                    data = data.PadRight(columnWidth, ' ');
                    break;
            }

            if (string.IsNullOrEmpty(lines.ToString()))
                NewLine();            

            if (lines.ToString().Length < Columns)
                lines.Append("".PadRight(Columns - lines.ToString().Length, ' '));

            if (data.Length + offset >= Columns)
                lines.Remove(offset, Columns - offset);
            else
                lines.Remove(offset, columnWidth);

            if (data.Length + offset >= Columns)
                lines.Insert(offset, !isBold ? data.Substring(0, Columns - offset) : string.Format("#FB#{0}#FE#", data.Substring(0, Columns - offset)));
            else if (data.Length > columnWidth)
                lines.Insert(offset, !isBold ? data.Substring(0, columnWidth) : string.Format("#FB#{0}#FE#", data.Substring(0, columnWidth)));
            else
                lines.Insert(offset, !isBold ? data : string.Format("#FB#{0}#FE#", data));
        }

        public void Write(string data, int columnWidth, int offset, Alignment align, TextAdjustment adjust)
        {
            if (string.IsNullOrEmpty(data) || offset > Columns)
                return;

            var tmp = "";
            if (data.Length > columnWidth)
            {
                tmp = data;
                var str = data.Substring(0, columnWidth);
                int idx = str.LastIndexOf(" ");
                data = data.Substring(0, idx);
                tmp = tmp.Substring(idx);
            }

            Write(data, columnWidth, offset, align);

            lineExtra = "";
            if (tmp.Length > 0)
            {
                lineExtra = "".PadRight(Columns, ' ');
                if (tmp.Length > columnWidth)
                    tmp = Textalign(tmp.Substring(0, columnWidth), columnWidth, align);
                else
                    tmp = Textalign(tmp, columnWidth, align);

                lineExtra = lineExtra.Remove(offset, columnWidth);
                lineExtra = lineExtra.Insert(offset, tmp.PadRight(columnWidth, ' '));
                wrapMode = adjust;
            }

        }

        public void Write(string data, int columnWidth, int offset, Alignment align, TextAdjustment adjust,bool isBold)
        {
            if (string.IsNullOrEmpty(data) || offset > Columns)
                return;

            var tmp = "";
            if (data.Length > columnWidth)
            {
                tmp = data;
                var str = data.Substring(0, columnWidth);
                int idx = str.LastIndexOf(" ");
                data = data.Substring(0, idx);
                tmp = tmp.Substring(idx);
            }

            Write(data, columnWidth, offset, align, isBold);

            lineExtra = "";
            if (tmp.Length > 0)
            {
                lineExtra = "".PadRight(Columns, ' ');
                if (tmp.Length > columnWidth)
                    tmp = Textalign(tmp.Substring(0, columnWidth), columnWidth, align);
                else
                    tmp = Textalign(tmp, columnWidth, align);
                
                lineExtra = lineExtra.Remove(offset, columnWidth);
                lineExtra = lineExtra.Insert(offset, !isBold? tmp.PadRight(columnWidth, ' '): string.Format("#BB#{0}#BE#", tmp.PadRight(columnWidth, ' ')));
                wrapMode = adjust;
            }

        }

        public void Write(string data, int columnWidth, bool autoMargin, Alignment align)
        {
            if (string.IsNullOrEmpty(data))
                return;

            data = Textalign(data, columnWidth, align);

            lines.Remove(colPrevOffset + 1, columnWidth);

            if (data.Length > columnWidth)
                lines.Insert(colPrevOffset + 1, data.Substring(0, columnWidth));
            else
                lines.Insert(colPrevOffset + 1, data);

            colPrevOffset += columnWidth + 1;
        }

        public void Write(string data, int columnWidth)
        {
            if (string.IsNullOrEmpty(data))
                return;

           

            data = data.PadRight(columnWidth, ' ');
            lines.Remove(0, columnWidth);

            if (data.Length > columnWidth)
                lines.Insert(0, data.Substring(0, columnWidth));
            else
                lines.Insert(0, data);

            colPrevOffset = columnWidth;
        }

        public void Write(string data, int columnWidth, int offset)
        {
            if (string.IsNullOrEmpty(data) || offset > Columns)
                return;

            
            data = data.PadRight(columnWidth, ' ');
            lines.Remove(offset, columnWidth);

            if (data.Length > columnWidth)
                lines.Insert(offset, data.Substring(0, columnWidth));
            else
                lines.Insert(offset, data);

            colPrevOffset = columnWidth + offset;
        }

        public void Write(string data, int columnWidth, int offset, TextAdjustment adjust)
        {
            if (string.IsNullOrEmpty(data) || offset > Columns)
                return;

            var tmp = "";
            if (data.Length > columnWidth)
            {
                tmp = data;
                var str = data.Substring(0, columnWidth);
                int idx = str.LastIndexOf(" ");
                data = data.Substring(0, idx);
                tmp = tmp.Substring(idx);
            }

            Write(data, columnWidth, offset);

            lineExtra = "";
            if (tmp.Length > 0)
            {
                lineExtra = "".PadRight(Columns, ' ');
                lineExtra = lineExtra.Remove(0, columnWidth);
                lineExtra = lineExtra.Insert(0, tmp.PadRight(columnWidth, ' '));
                wrapMode = adjust;
            }

        }

        public void Write(string data, int columnWidth, bool autoMargin)
        {
            if (string.IsNullOrEmpty(data))
                return;

            data = data.PadRight(columnWidth, ' ');
            lines.Remove(colPrevOffset + 1, columnWidth);

            if (data.Length > columnWidth)
                lines.Insert(colPrevOffset + 1, data.Substring(0, columnWidth));
            else
                lines.Insert(colPrevOffset + 1, data);

            colPrevOffset += columnWidth + 1;
        }

        public void Drawline()
        {
            //this.Content.AppendLine();
            this.Content.AppendLine("".PadRight(Columns - 1, '-'));
        }

        public void Drawline(int margin)
        {
            //this.Content.AppendLine();
            var str = "".PadLeft(margin, ' ');
            this.Content.AppendLine(str.PadRight(Columns - (margin * 2), '-'));
        }

        //Remove Right Margin
        public void Drawline(int margin,int rightMargin)
        {
            //this.Content.AppendLine();
            var str = "".PadLeft(margin, ' ');
            this.Content.AppendLine(str.PadRight(Columns - (rightMargin * 1), '-'));
        }

        //some more helper
        private string Textalign(string data, int columnWidth, Alignment align)
        {
            
            switch (align)
            {
                case Alignment.Center:
                    //For center
                    int padlen = (columnWidth - data.Length) / 2;
                    data = data.PadLeft(padlen + data.Length, ' ');
                    data = data.PadRight(padlen + data.Length + (data.Length % 2), ' ');
                    break;

                case Alignment.Right:
                    data = data.PadLeft(columnWidth, ' ');
                    break;

                case Alignment.Left:
                    data = data.PadRight(columnWidth, ' ');
                    break;
            }
            return data;
        }

        private void GetLine(int lineNo)
        {
            String[] retLine = this.Content.ToString().Split('\n');
            lines = new StringBuilder();

            if (lineNo > retLine.Length)
            {
                for (int i = 0; i < lineNo - retLine.Length; i++)
                    NewLine();
            }

            lines.Append(retLine[lineNo]);
        }

        private void setLine(int lineNo, string value)
        {
            String[] retLine = this.Content.ToString().Split('\n');
            retLine[lineNo] = value;

            this.Content = new StringBuilder();
            for (int i = 0; i < retLine.Length; i++)
                this.Content.AppendLine(retLine[i]);
        }
    }
}
