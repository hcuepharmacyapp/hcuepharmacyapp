﻿//using log4net;
//using log4net.Appender;
//using log4net.Core;
//using log4net.Layout;
//using log4net.Repository.Hierarchy;
using System.Runtime.CompilerServices;

namespace Utilities.Logger
{
    public static class LogAndException
    {
        #region Logger

        //THIS FUNCTION IS USED TO WRITE TO LOG FILE
        //private static readonly ILog Logger = LogManager.GetLogger("Crm");

        //static LogAndException()
        //{
        //    XmlConfigurator.Configure();
        //}

        public static void WriteToLogger(string logDescription, ErrorLevel level, [CallerMemberName] string callerMemberName = "", [CallerFilePath] string callerFilePath = "", [CallerLineNumber] int callerLineNumber = -1)
        {
            //var message = $"[{logDescription}] [{callerMemberName}] [{callerFilePath}] [{callerLineNumber}]";

            //switch (level)
            //{
            //    case ErrorLevel.Info:
            //        Logger.Info(message);
            //        break;
            //    case ErrorLevel.Debug:
            //        Logger.Debug(message);
            //        break;
            //    case ErrorLevel.Error:
            //        Logger.Error(message);
            //        break;
            //    case ErrorLevel.Fatal:
            //        Logger.Fatal(message);
            //        break;
            //    case ErrorLevel.Warn:
            //        Logger.Warn(message);
            //        break;
            //    default:
            //        throw new ArgumentOutOfRangeException(nameof(level));
            //}
        }

        #endregion
    }
    public class Logger
    {
        public static void Setup()
        {
            //Hierarchy hierarchy = (Hierarchy)LogManager.GetRepository();

            //PatternLayout patternLayout = new PatternLayout();
            //patternLayout.ConversionPattern = "%date [%thread] %-5level %logger - %message%newline";
            //patternLayout.ActivateOptions();

            //RollingFileAppender roller = new RollingFileAppender();
            //roller.AppendToFile = false;
            //roller.File = @"Logs\EventLog.txt";
            //roller.Layout = patternLayout;
            //roller.MaxSizeRollBackups = 5;
            //roller.MaximumFileSize = "1GB";
            //roller.RollingStyle = RollingFileAppender.RollingMode.Size;
            //roller.StaticLogFileName = true;
            //roller.ActivateOptions();
            //hierarchy.Root.AddAppender(roller);

            //MemoryAppender memory = new MemoryAppender();
            //memory.ActivateOptions();
            //hierarchy.Root.AddAppender(memory);

            //hierarchy.Root.Level = Level.All;
            //hierarchy.Configured = true;
        }
    }
    public enum ErrorLevel
    {
        Info,
        Debug,
        Error,
        Fatal,
        Warn
    }
}