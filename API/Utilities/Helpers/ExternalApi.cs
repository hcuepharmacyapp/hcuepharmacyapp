using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;

namespace Utilities.Helpers
{
    public class ExternalApi : BaseConfig
    {
        public ExternalApi(IConfigurationRoot configurationRoot, IHostingEnvironment env) : base(configurationRoot, env)
        {
        }

        public virtual string UrlAddUpdatePharma => $"{InternalUrlExternalPlatform}platformPartners/addUpdatePharma";
        public virtual string UrlAddPatient => $"{InternalUrlExternalPatient}patients/addPatient";
        public virtual string UrlUpdatePatient => $"{InternalUrlExternalPatient}patients/updatePatient";
        public virtual string UrlGetPatient => $"{InternalUrlExternalPatient}patients/getPatients";
        public virtual string UrlUpdatePatientCasePrescriptionStatus => $"{InternalUrlExternalPatient}patientCase/updatePatientCasePrescriptionStatus";
        public virtual string UrlGetPharmaLeads => $"{InternalUrlExternalPlatform}platformPartners/getPharmaLeads";
        public virtual string UrlGetPatientCookie => $"{InternalUrlExternalPatient}getPatientCookie";
        public virtual string UrlGetPlatformCookie => $"{InternalUrlExternalPlatform}getPlatformCookie";
        public virtual string UrlUpdatePharmaLeadsStatus => $"{InternalUrlExternalPlatform}platformPartners/updatePharmaLeads";
    }
}