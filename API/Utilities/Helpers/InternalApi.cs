using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;

namespace Utilities.Helpers
{
    public class InternalApi : BaseConfig
    {
        public InternalApi(IConfigurationRoot configurationRoot, IHostingEnvironment env) : base(configurationRoot, env)
        {
        }

        public string AuthUrl => $"{InternalUrlApi}user/ValidateUser2";
        public string SyncUrl => $"{InternalUrlSyncApi}Sync/ExportOnlineData";
        public string Instance => $"{InternalUrlApi}instance/list";
        public string ProductUrl => $"{InternalUrlApi}Products/GetBranchWiseStock";
        public string SyncPatientUrl => $"{InternalUrlSyncApi}Sync/ExportOnlinePatient";
        public string SyncProductUrl => $"{InternalUrlApi}Sync/SyncProductToOnline";
        public string SyncMissedItem => $"{InternalUrlSyncApi}Sync/GetMissedItem";
        public string GetProductCodesUrl => $"{InternalUrlApi}Products/GetProductCodes";
        public string GetDataCorrectionProductStocksUrl => $"{InternalUrlApi}Products/GetDataCorrectionProductStocks";
        public string InsertDataCorrectionOpeningStocksUrl => $"{InternalUrlApi}Products/InsertDataCorrectionOpeningStocks";
        public string InsertDataCorrectionStockAdjustmentsUrl => $"{InternalUrlApi}Products/InsertDataCorrectionStockAdjustments";
        public string InsertDataCorrectionTempStocksUrl => $"{InternalUrlApi}Products/InsertDataCorrectionTempStocks";
        public string UpdateProductStockToOnlineUrl => $"{InternalUrlApi}Products/UpdateProductStockToOnline";
        public string GetReverseSyncOfflineToOnlineCompareUrl => $"{InternalUrlApi}Products/GetReverseSyncOfflineToOnlineCompare";

        public string StockTransferUrl => $"{InternalUrlApi}StockTransfer/GetStockTransfer";
    }
}