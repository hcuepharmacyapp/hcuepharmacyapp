using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using System;

namespace Utilities.Helpers
{
    public class BaseConfig
    {
        private readonly IConfigurationRoot _configurationRoot;
        protected IHostingEnvironment Env;

        public BaseConfig(IConfigurationRoot configurationRoot, IHostingEnvironment env)
        {
            _configurationRoot = configurationRoot;
            Env = env;
        }

        protected virtual string InternalPatientAuthToken => _configurationRoot["Data:AuthToken:Patient"];
        protected virtual string InternalPlatformAuthToken => _configurationRoot["Data:AuthToken:Platform"];
        protected virtual string InternalInstallType => _configurationRoot["Data:InstallType"];
        protected virtual bool InternalOfflineMode => Convert.ToBoolean(_configurationRoot["Data:OfflineMode"]);
        protected virtual string InternalClientId => _configurationRoot["Data:ClientId"];
        protected virtual string InternalDeploy => _configurationRoot["Data:Deploy"];
        protected virtual string InternalVersion => _configurationRoot["Data:Version"];
        protected virtual bool InternalApiAuth => Convert.ToBoolean(_configurationRoot["Data:ApiAuth"]);
        protected virtual bool InternalSendSms => Convert.ToBoolean(_configurationRoot["Data:Communication:SendSms"]);
        protected virtual bool InternalSendEmail => Convert.ToBoolean(_configurationRoot["Data:Communication:SendEmail"]);
        protected virtual string InternalUrlApi => _configurationRoot["Data:Api:ApiUri"];
        protected virtual string InternalUrlSyncApi => _configurationRoot["Data:Api:SyncApiUri"];
        protected virtual string InternalUrlExternalPatient => _configurationRoot["Data:Api:External:Patient"];
        protected virtual string InternalUrlExternalPlatform => _configurationRoot["Data:Api:External:Platform"];
        protected virtual string InternalSqlConnection => _configurationRoot["Data:DefaultConnection:ConnectionString"];
        protected virtual string InternalSqLiteConnection => _configurationRoot["Data:DefaultConnection:ConnectionString"];
        protected virtual string InternalLogConnection => _configurationRoot["Data:DefaultConnection:LogConnectionString"];
        protected virtual string InternalContactUrl => _configurationRoot["Data:Communication:ContactUrl"];
        protected virtual string StorageConnectionStr => _configurationRoot["Data:StorageConnectionStr"];
        protected virtual bool IneternalIsGstEnabled => Convert.ToBoolean(_configurationRoot["Data:GSTEnabled"]);
        protected virtual string InternalEnv => _configurationRoot["Data:Env"]; //Added by Sumathi on 08-11-18

        protected virtual string InternalSyncDataUploadUri => _configurationRoot["Data:Api:SyncDataUploadUri"]; //Added by Sumathi on 21-12-18
    }
}