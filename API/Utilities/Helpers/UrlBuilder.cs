using Microsoft.AspNetCore.Http;

namespace Utilities.Helpers
{
    public class UrlBuilder
    {
        private readonly string _url;

        public UrlBuilder(IHttpContextAccessor httpContextAccessor)
        {
            _url =
                $"{httpContextAccessor.HttpContext.Request.Scheme}://{httpContextAccessor.HttpContext.Request.Host.ToUriComponent()}/";
        }

        public string InvoiceUrl => $"{_url}Invoice/SalesInvoice?id=";
        public string ReturnInvoiceUrl => $"{_url}Invoice/ReturnSalesInvoiceView?id=";
        public string ResetUrl => $"{_url}resetpassword?Key=";
        public string ExternalResetUrl => $"http://hcuepharma.com/resetpassword?Key=";
        //public string ExternalResetUrl => $"http://hcue-web-dev.azurewebsites.net/resetpassword?Key=";
    }
}