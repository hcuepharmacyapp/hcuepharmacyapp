﻿namespace Utilities.Helpers
{
    public static class Constant
    {
        public const string InstallTypeSqlite = "sqlite";
        public const string InstallTypeSqlServer = "sqlserver";
        public const string Development = "dev";
        public const bool OfflineModeOFF = false;
        public const bool OfflineModeON = true;

        public static string ClientId;
        public const string UserType = "PHRCONNECT";
        public const string PharmaUserType = "PHARMA";
    }
}