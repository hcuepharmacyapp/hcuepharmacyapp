﻿using System;

namespace Utilities.Helpers
{
    public static class NumberToText
    {
        public static string ConvertNumbertoWords(this long number)
        {
            if (number == 0) return "Zero";
            if (number < 0) return "minus " + ConvertNumbertoWords(Math.Abs(number));
            var words = "";
            if ((number / 100000) > 0)
            {
                if((number / 100000)==1)
                    words += ConvertNumbertoWords(number / 100000) + " Lakh ";
                else
                    words += ConvertNumbertoWords(number / 100000) + " Lakhs ";
                number %= 100000;
            }
            if ((number / 1000) > 0)
            {
                words += ConvertNumbertoWords(number / 1000) + " Thousand ";
                number %= 1000;
            }
            if (number / 100 > 0)
            {
                words += ConvertNumbertoWords(number / 100) + " Hundred ";
                number %= 100;
            }

            if (number <= 0)
                return words;

            if (words != "") words += "And ";
            var unitsMap = new[]
                {"Zero", "One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine", "Ten", "Eleven", "Twelve", "Thirteen", "Fourteen", "Fifteen", "Sixteen", "Seventeen", "Eighteen", "Nineteen"};
            var tensMap = new[]
                {"Zero", "Ten", "Twenty", "Thirty", "Forty", "Fifty", "Sixty", "Seventy", "Eighty", "Ninety"};
            if (number < 20) words += unitsMap[number];
            else
            {
                words += tensMap[number / 10];
                if (number % 10 > 0) words += " " + unitsMap[number % 10];
            }
            return words;
        }

        public static string ConvertNumbertoWords(this decimal decimalNumber)
        {
            decimal value = Math.Round(decimalNumber, 2, MidpointRounding.AwayFromZero);
            string[] numbers = value.ToString().Split('.');
            long number = long.Parse(numbers[0]);
            long number2 = long.Parse(numbers[1]);
            var words = ConvertNumbertoWords(number);
            if (number2 > 0)
            {
                words = words.Replace("And ", "");
                words = words + " And ";
                words = words + ConvertNumbertoWords(number2) + " Paisa";
            }
            return words;
        }
    }
}
