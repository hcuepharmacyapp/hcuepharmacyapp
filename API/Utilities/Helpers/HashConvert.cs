﻿using System;

namespace Utilities.Helpers
{
    public class HashFactory
    {
        private const string HashName = "SHA";

        public static string GetHashedValue(string key)
        {
            if (string.IsNullOrEmpty(key))
                return string.Empty;

            byte[] hash = new byte[0];
            ;
#if dnx451
            var x = SHA1.Create(HashName);
            hash = x.ComputeHash(Encoding.UTF8.GetBytes(key));
#endif

#if dnxcore50
            System.Security.Cryptography.
#endif


            return BitConverter.ToString(hash).Replace("-", "");
        }

        //public static byte[] HmacSha1Sign(byte[] keyBytes, string message)
        //{
        //    var messageBytes = Encoding.UTF8.GetBytes(message);
        //    MacAlgorithmProvider objMacProv = MacAlgorithmProvider.OpenAlgorithm("HMAC_SHA1");
        //    CryptographicKey hmacKey = objMacProv.CreateKey(keyBytes.AsBuffer());
        //    IBuffer buffHMAC = CryptographicEngine.Sign(hmacKey, messageBytes.AsBuffer());
        //    return buffHMAC.ToArray();

        //}
    }
}