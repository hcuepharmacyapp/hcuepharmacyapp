﻿namespace Utilities.Helpers.Interface
{
    public interface ICacheManager
    {
        bool Set(string key, object value);
        object Get(string key);
        void Remove(string key);
        T Get<T>(string key);
        bool Set(string key, object value, int cacheDurationSeconds);
    }
}