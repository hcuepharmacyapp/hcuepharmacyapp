﻿using Microsoft.AspNetCore.Http;
namespace Utilities.Helpers
{
    public class ConfigHelper
    {
        
        public ConfigHelper(AppConfig appConfig,InternalApi internalApi,ExternalApi externalApi,UrlBuilder urlBuilder, IHttpContextAccessor httpContextAccessor)
        {
            AppConfig = appConfig;
            InternalApi = internalApi;
            ExternalApi = externalApi;
            UrlBuilder = urlBuilder;
            HttpContextAccessor = httpContextAccessor;
        }
        public AppConfig AppConfig { get; }

        public InternalApi InternalApi { get; }

        public ExternalApi ExternalApi { get; }

        public UrlBuilder UrlBuilder { get; }
        public IHttpContextAccessor HttpContextAccessor { get; }
    }
}

