﻿using System;
using Microsoft.Extensions.Caching.Memory;
using Utilities.Helpers.Interface;

namespace Utilities.Helpers
{
    public class CacheManager : ICacheManager
    {
        private readonly IMemoryCache _memoryCache;

        public CacheManager(IMemoryCache memoryCache)
        {
            _memoryCache = memoryCache;
        }

        public bool Set(string key, object value, int cacheDurationSeconds)
        {
            _memoryCache.Set(key, value, GetCacheEntryOptions(cacheDurationSeconds));
            return true;
        }

        public bool Set(string key, object value)
        {
            return Set(key, value, 60);
        }

        public object Get(string key)
        {
            object data;
            _memoryCache.TryGetValue(key, out data);
            return data;
        }

        public T Get<T>(string key)
        {
            return _memoryCache.Get<T>(key);
        }

        public void Remove(string key)
        {
            _memoryCache.Remove(key);
        }

        private static MemoryCacheEntryOptions GetCacheEntryOptions(int cacheDurationSeconds)
        {
            return new MemoryCacheEntryOptions()
                .SetSlidingExpiration(TimeSpan.FromSeconds(cacheDurationSeconds));
        }
    }
}
