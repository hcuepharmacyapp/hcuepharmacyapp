﻿using System;
using HQue.Contract.Base;
using Utilities.Enum;
using Utilities.UserSession;
using System.Collections.Generic;
using System.Net;
using System.Collections;
using System.Reflection;
using System.Security.Claims;
using System.Security.Principal;

namespace Utilities.Helpers
{
    public static class Extension
    {
        public static string ToFormat(this DateTime? value)
        {
            return value.HasValue ? value.Value.ToFormat() : string.Empty;
        }

        public static string ToFormat(this DateTime value)
        {
            return value.ToString("dd-MMM-yyyy");
        }

        public static string ToFormatExp(this DateTime? value)
        {
            return value.HasValue ? value.Value.ToFormatExp() : string.Empty;
        }

        public static string ToFormatExp(this DateTime value)
        {
            return value.ToString("MM/yy");
        }
        public static string ToExternalFormat(this DateTime value)
        {
            return value.ToString("yyyy-MM-dd");
        }

        public static bool IsSuccessfull(this int value)
        {
            return value == MessageId.DataSaved || value == MessageId.DataUpdated;
        }

        public static string SubstringWithCheck(this string value, int index, int length)
        {
            if (!string.IsNullOrEmpty(value) && value.Length >= (index + length))
                return value.Substring(index, length);
            return string.Empty;
        }

        public static IContract SetLoggedUserDetails(this IContract contract, ClaimsPrincipal claimsPrincipal)
        {
            var user = claimsPrincipal.Identity;
            contract.OfflineStatus = Convert.ToBoolean(claimsPrincipal.OfflineStatus());
            return LoggedUserDetails(contract, user);
        }

        private static IContract LoggedUserDetails(this IContract contract, IIdentity user)
        {
            contract.ClientId = Constant.ClientId;
            contract.InstanceId = user.InstanceId();
            contract.AccountId = user.AccountId();
            contract.CreatedBy = user.Id();
            contract.UpdatedBy = user.Id();            
            return contract;
        }

        public static string Id(this IIdentity claimsIdentity)
        {
            var ci = claimsIdentity as ClaimsIdentity;
            return ci.FindFirst(ClaimTypes.Sid)?.Value;
        }

        public static string AccountId(this IIdentity claimsIdentity)
        {
            var ci = claimsIdentity as ClaimsIdentity;

            return ci.FindFirst(CustomIdentity.AccountId)?.Value;
        }

        public static string InstanceId(this IIdentity claimsIdentity)
        {
            var ci = claimsIdentity as ClaimsIdentity;
            var claim = ci.FindFirst(CustomIdentity.InstanceId);
            return claim?.Value;
        }

        public static string InstanceId(this ClaimsPrincipal claimsIdentity)
        {
            var ci = claimsIdentity as ClaimsPrincipal;
            var claim = ci.FindFirst(CustomIdentity.InstanceId);
            return claim?.Value;
        }

        public static string InstanceName(this ClaimsPrincipal claimsIdentity)
        {
            var ci = claimsIdentity;
            var claim = ci.FindFirst(CustomIdentity.InstanceName);
            return claim?.Value;
        }

        //New method for GST Null Available by Manivannan on 10-Oct-2017
        public static string GSTNullAvailable(this ClaimsPrincipal claimsIdentity)
        {
            var ci = claimsIdentity;
            var claim = ci.FindFirst(CustomIdentity.GSTNullAvailable);
            return claim?.Value;
        }

        public static string InstancePhone(this ClaimsPrincipal claimsIdentity)
        {
            var ci = claimsIdentity;
            var claim = ci.FindFirst(CustomIdentity.InstancePhone);
            return claim?.Value;
        }

        public static string InstanceAddress(this ClaimsPrincipal claimsIdentity)
        {
            var ci = claimsIdentity;
            var claim = ci.FindFirst(CustomIdentity.InstanceAddress);
            return claim?.Value;
        }

        public static string Name(this IIdentity claimsIdentity)
        {
            var ci = claimsIdentity as ClaimsIdentity;

            return ci.FindFirst(ClaimTypes.Name)?.Value;
        }

        public static string UserId(this IIdentity claimsIdentity)
        {
            var ci = claimsIdentity as ClaimsIdentity;

            return ci.FindFirst(ClaimTypes.Email)?.Value;
        }

        public static string Mobile(this IIdentity claimsIdentity)
        {
            var ci = claimsIdentity as ClaimsIdentity;

            return ci.FindFirst(ClaimTypes.MobilePhone)?.Value;
        }

        public static bool IsAdmin(this ClaimsPrincipal claimsIdentity)
        {
            var ci = claimsIdentity.Identity as ClaimsIdentity;

            return ci.FindFirst(ClaimTypes.Role).Value == UserType.HQueAdmin.ToString();
        }

        public static bool IsDistributor(this ClaimsPrincipal claimsIdentity)
        {
            var ci = claimsIdentity.Identity as ClaimsIdentity;

            return ci.FindFirst(ClaimTypes.Role).Value == UserType.DistributorUser.ToString();
        }
        public static bool IsHQueToolUser(this ClaimsPrincipal claimsIdentity)
        {
            var ci = claimsIdentity.Identity as ClaimsIdentity;

            return ci.FindFirst(ClaimTypes.Role).Value == UserType.HQueToolUser.ToString();
        }
        public static string OfflineStatus(this ClaimsPrincipal claimsIdentity)
        {
            var ci = claimsIdentity;
            var claim = ci.FindFirst(CustomIdentity.OfflineMode);
            return claim?.Value;
        }

        public static string OfflineInstalled(this ClaimsPrincipal claimsIdentity)
        {
            var ci = claimsIdentity;
            var claim = ci.FindFirst(CustomIdentity.OfflineInstalled);
            return claim?.Value;
        }
        /*GSTEnabled Method added by Poongodi on 29/06/2017*/
        public static string GSTEnabled(this ClaimsPrincipal claimsIdentity)
        {
            var ci = claimsIdentity;
            var  claim = ci.FindFirst(CustomIdentity.GSTEnabled);
            return claim?.Value;
        }
        public static string Name(this ClaimsPrincipal claimsIdentity)
        {
            var ci = claimsIdentity.Identity as ClaimsIdentity;

            return ci.FindFirst(ClaimTypes.Name)?.Value;
        }

        public static string UserId(this ClaimsPrincipal claimsIdentity)
        {
            var ci = claimsIdentity.Identity as ClaimsIdentity;

            return ci.FindFirst(ClaimTypes.Email).Value;
        }

        public static string AccountId(this ClaimsPrincipal claimsIdentity)
        {
            var ci = claimsIdentity.Identity as ClaimsIdentity;

            return ci.FindFirst(CustomIdentity.AccountId)?.Value;
        }

        public static bool HasMultiInstanceAccess(this ClaimsPrincipal claimsIdentity)
        {
            var ci = claimsIdentity.Identity as ClaimsIdentity;
            var value = ci.FindFirst(CustomIdentity.HasMultipleInstanceAccess)?.Value;
            if (string.IsNullOrEmpty(value))
                return false;
            if (value == "true")
                return true;
            return false;
        }
        public static string LoggedinDate(this ClaimsPrincipal claimsIdentity)
        {
            var ci = claimsIdentity.Identity as ClaimsIdentity;

            return ci.FindFirst(CustomIdentity.LoggedinDate)?.Value;
        }
      

        public static bool HasInstanceId(this ClaimsPrincipal claimsIdentity)
        {
            var ci = claimsIdentity.Identity as ClaimsIdentity;
            var instanceId = ci.FindFirst(CustomIdentity.InstanceId);
            return instanceId != null;
        }

        public static T GetService<T>(this IServiceProvider serviceProvider, T type)
        {
            return (T) serviceProvider.GetService(typeof (T));
        }
        public static bool HasPermission(this ClaimsPrincipal claimsIdentity, int id)
        {
            var ci = claimsIdentity.Identity as ClaimsIdentity;
            var userPermission = ci.FindFirst(CustomIdentity.UserAccess).ToString();
            return userPermission.Contains($"{{{id}}}");
        }
        public static string ShortCutKeysType(this ClaimsPrincipal claimsIdentity)
        {
            var ci = claimsIdentity;
            var claim = ci.FindFirst(CustomIdentity.ShortCutKeysType);
            return claim?.Value;
        }
        public static string NewWindowType(this ClaimsPrincipal claimsIdentity)
        {
            var ci = claimsIdentity;
            var claim = ci.FindFirst(CustomIdentity.NewWindowType);
            return claim?.Value;
        }
        public static IEnumerable<Cookie> GetAllCookies(this CookieContainer c)
        {
            Hashtable k = (Hashtable)c.GetType().GetField("m_domainTable", BindingFlags.Instance | BindingFlags.NonPublic).GetValue(c);
            foreach (DictionaryEntry element in k)
            {
                SortedList l = (SortedList)element.Value.GetType().GetField("m_list", BindingFlags.Instance | BindingFlags.NonPublic).GetValue(element.Value);
                foreach (var e in l)
                {
                    var cl = (CookieCollection)((DictionaryEntry)e).Value;
                    foreach (Cookie fc in cl)
                    {
                        yield return fc;
                    }
                }
            }
        }

        public static bool HasMultiInstance(this ClaimsPrincipal claimsIdentity)
        {
            var ci = claimsIdentity.Identity as ClaimsIdentity;
            var value = ci.FindFirst(CustomIdentity.HasMultipleInstance)?.Value;
            if (string.IsNullOrEmpty(value))
                return false;
            if (value == "true")
                return true;
            return false;
        }
        public static string InstanceMobile(this ClaimsPrincipal claimsIdentity)
        {
            var ci = claimsIdentity;
            var claim = ci.FindFirst(CustomIdentity.InstanceMobile);
            return claim?.Value;
        }
        public static string IsComposite(this ClaimsPrincipal claimsIdentity)
        {
            var ci = claimsIdentity;
            var claim = ci.FindFirst(CustomIdentity.IsComposite);
            return claim?.Value;
        }
        ////Added by Sumathi on 10-Nov-18
        //public static bool IsSyncEnabled(this ClaimsPrincipal claimsIdentity)
        //{
        //    var ci = claimsIdentity;
        //    var claim = ci.FindFirst(CustomIdentity.IsSyncEnabled);
        //    return (claim?.Value == "true" ? true : false);
        //}

        //Added by Sarubala on 18-12-18
        public static int InstanceTypeId(this ClaimsPrincipal claimsIdentity)
        {
            var ci = claimsIdentity;
            var claim = ci.FindFirst(CustomIdentity.InstanceTypeId.ToString());
            if (string.IsNullOrEmpty(claim?.Value))
                return 0;
            return Convert.ToInt32(claim?.Value);
        }

        //Added by Sarubala on 30-05-2020 for SMS Integration
        public static string SMSUserName(this ClaimsPrincipal claimsIdentity)
        {
            var ci = claimsIdentity;
            var claim = ci.FindFirst(CustomIdentity.SMSUserName);
            return claim?.Value;
        }

        public static string SMSPassword(this ClaimsPrincipal claimsIdentity)
        {
            var ci = claimsIdentity;
            var claim = ci.FindFirst(CustomIdentity.SMSPassword);
            return claim?.Value;
        }

        public static string SMSUrl(this ClaimsPrincipal claimsIdentity)
        {
            var ci = claimsIdentity;
            var claim = ci.FindFirst(CustomIdentity.SMSUrl);
            return claim?.Value;
        }

    }
}