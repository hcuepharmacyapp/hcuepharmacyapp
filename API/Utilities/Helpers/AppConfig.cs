﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;

namespace Utilities.Helpers
{
    public class AppConfig : BaseConfig
    {
        public AppConfig(IConfigurationRoot configurationRoot, IHostingEnvironment env) : base(configurationRoot, env)
        {
        }

        public virtual bool IsSqlServer => InstallType == Constant.InstallTypeSqlServer;
        public virtual bool IsSqLite => InstallType == Constant.InstallTypeSqlite;

        public virtual string ConnectionString
            =>
                IsSqlServer
                    ? InternalSqlConnection
                    : $"Data Source = {Env.WebRootPath}\\{InternalSqLiteConnection}";
        public virtual string LogConnectionString
          =>
              IsSqlServer
                  ? InternalLogConnection
                  : $"Data Source = {Env.WebRootPath}\\{InternalSqlConnection}";
       
        public virtual bool ApiAuth => InternalApiAuth;
        public virtual string StorageConnection => StorageConnectionStr;
        public virtual string PatientAuthToken=>InternalPatientAuthToken;
        public virtual string PlatformAuthToken=>InternalPlatformAuthToken;
        public virtual string InstallType=>InternalInstallType;
        public virtual bool OfflineMode => InternalOfflineMode;
        public virtual string ClientId=>InternalClientId;
        public virtual string Deploy=>InternalDeploy;
        public virtual string Version => InternalVersion;
        public virtual bool SendSms=>InternalSendSms;
        public virtual bool SendEmail=>InternalSendEmail;
        public virtual string ContactUrl => InternalContactUrl;
        public virtual bool IsDev => Deploy == Constant.Development;
        public virtual bool IsGstEnabled => IneternalIsGstEnabled;
        public virtual string AppEnvironment => InternalEnv; //Added by Sumathi on 08-11-18
        public virtual string SyncDataUploadUri => InternalSyncDataUploadUri; //Added by Sumathi on 21-12-18
    }
}