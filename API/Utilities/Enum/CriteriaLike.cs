﻿namespace Utilities.Enum
{
    public enum CriteriaLike
    {
        Begin = 0,
        End = 1,
        Both = 2
    }
}