﻿namespace Utilities.Enum
{
    public enum CriteriaEquation
    {
        Equal = 0,
        NotEqual = 1,
        Like = 2,
        NotLike = 3,
        Between = 4,
        Greater = 5,
        Lesser = 6,
        GreaterEqual = 7,
        LesserEqual = 8 //Added by Annadurai
    }
}