﻿namespace Utilities.Enum
{
    public enum CriteriaCondition
    {
        And = 0,
        Or = 1,
        IsNull = 2,
        IsNotNull = 3
    }
}