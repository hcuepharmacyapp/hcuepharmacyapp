﻿namespace Utilities.Enum
{
    public class UserType
    {
        public const int HQueAdmin = 1;
        public const int InstanceUser = 2;
        public const int DistributorUser = 3;
        public const int HQueAdminUser = 4;
        public const int HQueToolUser = 5;
    }
}