﻿namespace Utilities.Enum
{
    public class MessageId
    {
        public const int NoResult = 0;
        public const int DataSaved = 1;
        public const int DataUpdated = 2;
        public const int ValidUser = 3;
        public const int InvalidUser = 4;
        public const int InvalidEmail = 5;
        public const int DataDeleted = 6;
        public const int UserExists = 7;
        public const int InvalidOldPassword = 8;
        public const int ComplaintCreated = 9;
        public const int UserCreatedProfileComplet = 10;
        public const int Misc = 1000;
    }
}
