﻿using System.Collections.Generic;
using HQue.Contract.Infrastructure.UserAccount;
using Utilities.Enum;
using System.Security.Claims;
using System;

namespace Utilities.UserSession
{
    public sealed class CustomIdentity : ClaimsIdentity
    {
        public const string AccountId = "AccountId";
        public const string InstanceId = "InstanceId";
        public const string InstanceName = "InstanceName";
        public const string OfflineMode = "OfflineMode";
        public const string OfflineInstalled = "OfflineInstalled";
        public const string HasMultipleInstanceAccess = "HasMultipleIstanceAccess";
        public const string InstancePhone = "InstancePhone";
        public const string InstanceAddress = "InstanceAddress";
        public const string UserAccess = "UserAccess";
        public const string NewWindowType = "NewWindowType";
        public const string ShortCutKeysType = "ShortCutKeysType";
        public const string GSTEnabled = "GSTEnabled";  //GSTEnabled Added by Poongodi on 29/06/2017
        public const string HasMultipleInstance = "HasMultipleIstance";
        public const string InstanceMobile = "InstanceMobile";
        public const string GSTNullAvailable = "GSTNullAvailable";
        public const string LoggedinDate = "LoggedinDate";
        public const string IsComposite = "IsComposite";
        // public const string IsSyncEnabled = "IsSyncEnabled"; //Added by Sumathi on 10-11-18
        public const string InstanceTypeId = "InstanceTypeId"; //Added by Sarubala on 18-12-18
        public const string SMSUserName = "SMSUserName"; //Added by Sarubala on 20-05-20
        public const string SMSPassword = "SMSPassword"; //Added by Sarubala on 20-05-20
        public const string SMSUrl = "SMSUrl"; //Added by Sarubala on 20-05-20
        public CustomIdentity(HQueUser user)
            : base("hque")
        {
            AddClaim(new Claim(ClaimTypes.Sid, user.Id));
            AddClaim(new Claim(ClaimTypes.Name, user.Name));
            AddClaim(new Claim(ClaimTypes.Role, user.UserType.ToString()));
            AddClaim(new Claim(ClaimTypes.Email, user.UserId));
            AddClaim(new Claim(ClaimTypes.MobilePhone, string.IsNullOrEmpty(user.Mobile)?"": user.Mobile));
            AddClaim(new Claim(GSTEnabled, Convert.ToString(user.IsGstEnabled)));
            AddClaim(new Claim(UserAccess, user.UserPermission));
            AddClaim(new Claim(OfflineMode, user.OfflineStatus.ToString()));
            AddClaim(new Claim(LoggedinDate, DateTime.Now.ToString("dd/MM/yyyy")));

            if (user.UserType == UserType.HQueAdmin)
                return;

            AddClaim(new Claim(AccountId, user.AccountId));

            if (string.IsNullOrEmpty(user.InstanceId))
                return;
            AddClaim(new Claim(InstanceId, user.InstanceId));
            AddClaim(new Claim(InstanceName, user.Instance.Name));
            AddClaim(new Claim(InstancePhone, string.IsNullOrEmpty(user.Instance.Phone) ? "" : user.Instance.Phone));
            AddClaim(new Claim(InstanceMobile, string.IsNullOrEmpty(user.Instance.Mobile) ? "" : user.Instance.Mobile));            
            AddClaim(new Claim(OfflineInstalled, user.Instance.IsOfflineInstalled.ToString()));
            AddClaim(new Claim(IsComposite, user.Instance.Gstselect.ToString()));
            AddClaim(new Claim(InstanceTypeId, user.Instance.InstanceTypeId.ToString()));
            AddClaim(new Claim(SMSUserName, string.IsNullOrEmpty(user.Instance.SmsUserName) ? "" : user.Instance.SmsUserName));
            AddClaim(new Claim(SMSPassword, string.IsNullOrEmpty(user.Instance.SmsPassword) ? "" : user.Instance.SmsPassword));
            AddClaim(new Claim(SMSUrl, string.IsNullOrEmpty(user.Instance.SmsUrl) ? "" : user.Instance.SmsUrl));


            if (user.NewWindowType == null || user.NewWindowType == "False")
            {
                AddClaim(new Claim(CustomIdentity.NewWindowType, "False"));
            }
            else
            {
                AddClaim(new Claim(CustomIdentity.NewWindowType, user.NewWindowType));
            }

            if (user.ShortCutKeysType == null)
            {
                AddClaim(new Claim(CustomIdentity.ShortCutKeysType, "True"));
            }
            else if (user.ShortCutKeysType == "True")
            {
                AddClaim(new Claim(CustomIdentity.ShortCutKeysType, user.ShortCutKeysType));
            }
            else if (user.ShortCutKeysType == "False")
            {
                AddClaim(new Claim(CustomIdentity.ShortCutKeysType, user.ShortCutKeysType));
            }

            //AddClaim(new Claim(NewWindowType, user.NewWindowType));
            //AddClaim(new Claim(ShortCutKeysType, user.ShortCutKeysType));

            if (!string.IsNullOrEmpty(user.Instance.Address))
            {
                AddClaim(new Claim(InstanceAddress, user.Instance.Address));
            }
        }


        public CustomIdentity(IEnumerable<Claim> claims)
            : base("hque")
        {
            AddClaims(claims);
        }
    }
}
