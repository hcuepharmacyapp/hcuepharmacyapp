﻿using System.Threading.Tasks;
using HQue.Biz.Core.Inventory;
using Microsoft.AspNetCore.Mvc;
namespace HQue.ApiCore.Controllers
{
    [Route("[controller]")]
    public class HomeController : Controller
    {
        private readonly SalesManager _replicationManager;

        public HomeController(SalesManager replicationManager)
        {
            _replicationManager = replicationManager;
        }

        [Route("/")]
        [Route("[action]")]
        public async Task<ViewResult> Index()
        {
            ViewBag.Connected = (await _replicationManager.ConnectionTest()) ? "connected" : "with error connecting";
            return View();
        }
    }
}