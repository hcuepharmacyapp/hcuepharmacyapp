﻿using HQue.Biz.Core.ETL;
using HQue.Biz.Core.Inventory;
using HQue.Biz.Core.Sync;
using HQue.Contract.External;
using HQue.Contract.External.Sync;
using HQue.Contract.Infrastructure.Inventory;
using HQue.Contract.Infrastructure.Master;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using HQue.DataAccess.Setup;
using HQue.Contract.Infrastructure.Settings;
using Utilities.Helpers;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using System.Net.Http;
using System.Collections;

namespace HQue.ApiCore.Controllers
{
    [Route("[controller]")]
    public class SyncController : Controller
    {
        private readonly SyncManager _syncManager;
        private readonly SalesManager _salesManager;
        private readonly Import _import;
        private readonly ConfigHelper _configHelper;

        public SyncController(SyncManager syncManager, Import import, ConfigHelper configHelper)
        {
            _syncManager = syncManager;
            _import = import;
            _configHelper = configHelper;
        }

        //[Route("[action]")]
        //[HttpPost]
        //public Task<bool> SyncDataToServer([FromBody]SyncObject data)
        //{
        //    //return Task.FromResult(false);

        //    if (data == null)
        //        return Task.FromResult(false);
        //    try
        //    {
        //        //WriteLog(JsonConvert.SerializeObject(data));
        //        //SyncObject syncObj = JsonConvert.DeserializeObject<SyncObject>(data);
        //        return _syncManager.SyncDataToServer(data);
        //    }
        //    catch (SqlException e)
        //    {
        //        return Task.FromResult(false);

        //    }

        //}





        [Route("[action]")]
        [HttpPost]
        public Task<bool> SyncDataToServer([FromBody]dynamic data, bool IsForceSync = false)
        {

            try
            {
               // If the data is null return true  to continue reading other files  - Added by Sumathi On 20/03/19          
                if (data == null)
                {
                    return Task.FromResult(true);  
                }

                var list = new List<SyncObject>();
                var type = data.GetType();
                bool IsSyncEnabled = false;
                DateTime syncDate = DateTime.MinValue;

                if (type.ToString().LastIndexOf("JObject") > 0)
                {
                    list.Add(JsonConvert.DeserializeObject<SyncObject>(JsonConvert.SerializeObject(data)));
                }
                else
                {
                    list.AddRange(JsonConvert.DeserializeObject<List<SyncObject>>(JsonConvert.SerializeObject(data)));
                }

                if (IsForceSync)
                {
                    return DoSync(list);
                }
                else
                {
                    string InstanceId = string.Empty, Id = string.Empty;
                    bool bSyncXml = false;
                    try
                    {
                        if (list.Count > 0)
                        {
                            Id = list[0].Id;
                            foreach (KeyValuePair<string, object> dict in list[0].Parameters)
                            {
                                if (dict.Key.ToString().ToLower() == "createdat"
                                    || dict.Key.ToString().ToLower() == "updatedat")
                                {
                                    if (!DateTime.TryParse(dict.Value.ToString(), out syncDate))
                                    {
                                        syncDate = DateTime.MinValue;
                                    }
                                }
                                else if (dict.Key.ToString().ToLower() == "instanceid")
                                {
                                    if (dict.Value != null)
                                        InstanceId = dict.Value.ToString();
                                }
                            }

                            //List<CustomSettingsDetail> LstCustomSettings = _syncManager.GetCustomSettings(list[0].AccountId, InstanceId, 5).Result;

                            //if (LstCustomSettings.Count > 0)
                            //{
                            //    CustomSettingsDetail item = (CustomSettingsDetail)LstCustomSettings[0];
                            //    IsSyncEnabled = (bool)item.IsActive;
                            //}

                            InstanceId = !string.IsNullOrEmpty(list[0].InstanceId) ? list[0].InstanceId : InstanceId;

                            IsSyncEnabled = (bool)_syncManager.IsSyncEnabled(list[0].AccountId, InstanceId).Result;
                            if (list[0].FileType == "xml")
                                bSyncXml = true;

                        }
                        if (IsSyncEnabled)
                        {
                            return DoSync(list);
                        }
                        else
                        {
                            Id = Id != string.Empty ? Id : Guid.NewGuid().ToString();

                            //if(bSyncXml)
                            //    return UploadFileToAzure(data, "syncdata-" + _configHelper.AppConfig.AppEnvironment,"closingtock", Id + "_" + syncDate.ToString("yyyyMMddHHmmss"));
                            //else
                            //     return UploadFileToAzure(data, "syncdata-" + _configHelper.AppConfig.AppEnvironment, syncDate.ToString("yyyyMMdd"), Id + "_" + syncDate.ToString("yyyyMMddHHmmss"));


                            string syncdirname = "";
                            if (syncDate == DateTime.MinValue)
                                syncdirname = "00010101";   // "filewodate";
                            else
                                syncdirname = syncDate.ToString("yyyyMMdd");

                            if (bSyncXml)
                                syncdirname = "closingstock";


                            // return UploadFileToAzure(data, "syncdata-" + _configHelper.AppConfig.AppEnvironment, syncdirname, Id + "_" + syncDate.ToString("yyyyMMddHHmmss"));
                            //   return UploadFileToAzure(data, "syncdata-" + _configHelper.AppConfig.AppEnvironment, syncdirname, InstanceId + "_" + syncDate.ToString("yyyyMMddHHmmssfff"));

                          //  return UploadFileToAzure(data, "syncdata-" + _configHelper.AppConfig.AppEnvironment, syncdirname, InstanceId + "_" + DateTime.Now.ToString("yyyyMMddHHmmssfff")); //File created with current datetime

                         
                             return UploadFile(data, "syncdata-" + _configHelper.AppConfig.AppEnvironment, syncdirname, InstanceId + "_" + DateTime.Now.ToString("yyyyMMddHHmmssfff"), true);

                        }
                    }

                    catch (Exception e)
                    {
                        return Task.FromResult(false);
                    }
                }
            }
            catch (Exception e)
            {
                return Task.FromResult(false);
            }

        }

        private Task<bool> DoSync(List<SyncObject> list)
        {
            try
            {
                var status = _syncManager.ExecuteSyncPendingQuery(list[0].AccountId, list[0].InstanceId).Result;
                if (status == false)
                {
                    return Task.FromResult(false);
                }

                for (var i = 0; i < list.Count; i++)
                {
                    var syncStatus = true;
                    syncStatus = _syncManager.SyncDataToServer(list[i]).Result;
                    if (syncStatus == false)
                    {
                        if (string.IsNullOrEmpty(list[i].AccountId) || string.IsNullOrEmpty(list[i].InstanceId))
                        {
                            return Task.FromResult(false);
                        }
                        else
                        {
                            var syncObjectList = new List<SyncObject>();
                            for (var j = i; j < list.Count; j++)
                            {
                                syncObjectList.Add(list[j]);
                            }
                            return _syncManager.SaveSyncPendingQuery(syncObjectList);
                        }
                    }
                }
                return Task.FromResult(true);
            }
            catch (Exception e)
            {
                return Task.FromResult(false);
            }
        }

        private void WriteLog(string message)
        {
            try
            {
                string logfile = string.Format("SyncAPI{0}_{1}.txt", DateTime.Now.Date.ToString("yyyyMMdd"), DateTime.Now.Hour.ToString());
                using (FileStream logFile = new FileStream(logfile, FileMode.Append, FileAccess.Write))
                using (StreamWriter sw = new StreamWriter(logFile))
                {
                    sw.WriteLine(string.Format("{0}: {1}", DateTime.Now.ToString(), message));
                }

            }
            catch (Exception ex)
            {
                //do nothing
            }

        }

        [Route("[action]")]
        [HttpPost]
        public async Task<ActionResult> ExportOnlineData([FromBody]ExportDataRequest req)
        {
            var status = _syncManager.ExecuteSyncPendingQuery(req.AccountID, req.InstanceID).Result;
            if (status == false)
            {
                throw new Exception("Offline data is not synced properly to Online. Please try again.");
            }
            var dataFile = await _syncManager.ExportOnlineData(req.InstanceID);
            FileInfo info = new FileInfo(dataFile);
            HttpContext.Response.ContentType = "application/notepad";
            return File(info.OpenRead(), "application/notepad", info.Name);
        }

        [Route("[action]")]
        public async Task<bool> GetAddonFeatureFromApi(string AccountId, string InstanceId, int GroupId)
        {
            if (string.IsNullOrEmpty(AccountId) || string.IsNullOrEmpty(InstanceId) || GroupId == 0)
            {
                return false;
            }
            return await _syncManager.GetFeatureAccess(GroupId, AccountId, InstanceId);
        }

        [Route("[action]")]
        [HttpPost]
        public async Task<string> GetSeedData([FromBody]ExportDataRequest req)
        {
            //return _syncManager.ExportOnlineData(req.InstanceID);
            var result = await _syncManager.ExportSeedData(req.AccountID, req.InstanceID, req.SeedIndex);

            return await Task.FromResult(result);
        }

        [Route("[action]")]
        [HttpPost]
        public async Task<ActionResult> ExportOnlinePatient([FromBody]ExportDataRequest req)
        {
            var dataFile = await _salesManager.ExportOnlinePatient(req.AccountID, req.InstanceID);
            FileInfo info = new FileInfo(dataFile);
            HttpContext.Response.ContentType = "application/notepad";
            return File(info.OpenRead(), "application/notepad", info.Name);
        }
        //Method  Added by Poongodi on 21/08/2017
        [HttpPost]
        [Route("[action]")]
        public async Task SyncProductToOnline([FromBody]SyncProduct data)
        {
            await _import.SyncProductToOnline(data.fname, data.AccountId, data.Userid);


        }
        //Method  Added by Poongodi on 21/09/2017
        [HttpPost]
        [Route("[action]")]
        public async Task<MissedDetail> GetMissedItem([FromBody]MissedItems data)
        {
            return await _import.GetMissedItem(data.AccountId, data.InstanceId, data.Type);
        }

        [Route("[action]")]
        [HttpGet]
        public string TestSync()
        {
            return "./temp";
        }


        //Created by Sumathi on 29-oct-18  to move data from SyncData table to TempSyncData
        //[Route("[action]")]
        //[HttpPost]
        //public async Task<bool> RemoveSyncData([FromBody]ExportDataRequest req)
        //{
        //    var result = await _syncManager.RemoveSyncData(req.AccountID, req.InstanceID, req.SeedIndex);
        //    return await Task.FromResult(result);
        //}

        private async Task<bool> UploadFileToAzure(dynamic data, string StorageContainer, string path, string fileName)
        {
            try
            {
                CloudStorageAccount storageAccount = CloudStorageAccount.Parse(_configHelper.AppConfig.StorageConnection);

                // Create the blob client.
                CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();
                CloudBlobContainer container = blobClient.GetContainerReference(StorageContainer); //syncdata_dev

                CloudBlockBlob blockBlob = container.GetBlockBlobReference(path + "/" + fileName);
                BlobRequestOptions blobOptions = new BlobRequestOptions
                {
                    MaximumExecutionTime = TimeSpan.FromSeconds(10),
                    ParallelOperationThreadCount = 48
                };
                await blockBlob.UploadTextAsync(JsonConvert.SerializeObject(data), null, null, blobOptions, null);
                return true;

            }
            catch (Exception ex)
            {
                return false;
            }
        }


        [Route("[action]")]
        [HttpPost]
        public async Task<bool> TestFileUpload([FromBody]dynamic data)
        {
            await UploadFile(data, "syncdata-dev", "201234", "textfile",false);
            return false;
        }

        private Task<bool> UploadFile(dynamic data, string path, string folderName, string fileName, bool IsUpload)
        {
            try
            {
                if (!IsUpload)
                {
                    return FileSave(path, folderName, fileName, data);
                }
                else
                {
                    using (var client = new HttpClient())
                    {
                        client.BaseAddress = new Uri(_configHelper.AppConfig.SyncDataUploadUri);

                        //   ArrayList paramList = new ArrayList();
                        Dictionary<string, object> paramList = new Dictionary<string, object>();
                        paramList.Add("path", path);
                        paramList.Add("folderName", folderName);
                        paramList.Add("fileName", fileName);
                        paramList.Add("data", JsonConvert.SerializeObject(data));

                        string contents = JsonConvert.SerializeObject(paramList);

                        HttpResponseMessage response = client.PostAsync("Upload/SyncData", new StringContent(contents, Encoding.UTF8, "application/json")).Result;

                        if (response.IsSuccessStatusCode)
                            return Task.FromResult(true);
                        else
                            return Task.FromResult(false);

                    }
                }
            }
            catch (Exception ex)
            {
                return Task.FromResult(false);
            }

        }

        private async Task<bool> FileSave(string path, string folderName, string fileName, dynamic data)
        {
            string SyncFilePath = "../../storage/" + path + "/" + folderName;

            if (!Directory.Exists(SyncFilePath))
                Directory.CreateDirectory(SyncFilePath);


            string pathString = System.IO.Path.Combine(SyncFilePath, fileName);
            var fileCreate = System.IO.File.Create(pathString);

            using (StreamWriter sw = new StreamWriter(fileCreate))
            {
                await sw.WriteLineAsync(data.ToString());
                return true;
            }

        }


    }
}