﻿using System;
using System.Threading.Tasks;
using HQue.Biz.Core.Leads;
using Microsoft.AspNetCore.Mvc;
using HQue.Contract.Infrastructure.Leads;
using System.Collections.Generic;
using HQue.Contract.External.Leads;
using System.IO;
using System.Text;

namespace HQue.ApiCore.Controllers
{
    [Route("[controller]")]
    public class LeadsController : Controller
    {
        private readonly LeadsManager _leadsManager;

        public LeadsController(LeadsManager leadsManager)
        {
            _leadsManager = leadsManager;
        }

        [Route("[action]")]
        [HttpGet]
        public async Task<string> NewLeadAlert(Int64 instanceId, Int64 leadId, string leadStatus)
        {
            try
            {
                string name = string.Format("leadsAPI.txt");

                FileInfo info = new FileInfo(name);
             /*As suggested by Martin Log writting commented by Poongodion 16/09/2017*/
            /*using (StreamWriter writer = info.AppendText())
                {
                    var msg = string.Format("{0} : InstanceID : {1} | LeadID : {2} | LeadStatus : {3} \r\n",DateTime.Now.ToString(), instanceId, leadId, leadStatus);
                    writer.Write(msg, false, Encoding.UTF8);

                }*/
                

                await _leadsManager.GetLeadsExternal(instanceId, leadId, leadStatus);

                return "Succuess";
            }catch(Exception e)
            {
                return e.Message;
            }
        }

        //Added by Sarubala on 26-02-20 for Philippines
        [Route("[action]")]
        [HttpPost]
        public async Task<string> NewLeadEntry([FromBody]Leads leadData)
        {
            try
            {
                await _leadsManager.SaveLeads(leadData);

                return "Success";
            }
            catch(Exception ex)
            {
                return ex.Message;
            }

        }

        [Route("[action]")]
        [HttpPost]
        public async Task<string> UpdateLeadStatus(string instanceId, string leadId, string leadStatus)
        {
            //await _leadsManager.GetLeadsExternal(instanceId, leadId, leadStatus);
            await _leadsManager.UpdateLeadStatus(instanceId, leadId, leadStatus);
          
            return "Succuess";
        }


        [Route("[action]")]
        [HttpPost]
        public async Task<string> UpdateLeadInfo([FromBody]LeadInfo info)
        {

            if (string.IsNullOrEmpty(info.InstanceId) || string.IsNullOrEmpty(info.LeadId))
                return "Insufficient Data to process request";

            //await _leadsManager.GetLeadsExternal(instanceId, leadId, leadStatus);
            await _leadsManager.UpdateLeadInfo(info);

            return "Succuess";
        }


        [Route("[action]")]
        [HttpGet]
        public async Task<List<Contract.Infrastructure.Leads.Leads>> GetAllLeads(string instanceId, string leadStatus)
        {
            return await _leadsManager.GetAllLeadsByStatus(instanceId, leadStatus); ;
        }

        [Route("[action]")]
        [HttpGet]
        public async Task<List<Contract.Infrastructure.Leads.Leads>> GetLeadData(string instanceId, string leadId)
        {
            return await _leadsManager.GetAllLeadsById(instanceId, leadId); 
        }

    }
}
