﻿using HQue.Biz.Core.Dashboard;
using HQue.Biz.Core.Inventory;
using HQue.Biz.Core.Master;
using HQue.Contract.External;
using HQue.Contract.Infrastructure.Common;
using HQue.Contract.Infrastructure.Dashboard;
using HQue.Contract.Infrastructure.Inventory;
using HQue.Contract.Infrastructure.Master;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HQue.ApiCore.Controllers
{
    [Route("[controller]")]
    public class StockTransferController : Controller
    {
        private readonly StockTransferManager _stockTransferManager;
      

        public StockTransferController(StockTransferManager stockTransferManager)
        {
            _stockTransferManager = stockTransferManager;
           
        }

        [HttpPost]
        [Route("[action]")]
        public async Task<List<StockTransferItem>> GetStockTransferItem([FromBody]ProductSearch data)
        {           
            return await _stockTransferManager.GetStockTransferItem(data.Id, string.Empty);
        }
    }
}
