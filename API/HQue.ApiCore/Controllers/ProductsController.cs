﻿using HQue.Biz.Core.Dashboard;
using HQue.Biz.Core.Inventory;
using HQue.Biz.Core.Master;
using HQue.Contract.External;
using HQue.Contract.Infrastructure.Common;
using HQue.Contract.Infrastructure.Dashboard;
using HQue.Contract.Infrastructure.Inventory;
using HQue.Contract.Infrastructure.Master;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace HQue.ApiCore.Controllers
{
    [Route("[controller]")]
    public class ProductsController : Controller
    {
        private readonly ProductStockManager _productStockManager;
        private readonly DashboardManager _dashboardManager;
        private readonly ProductManager _productManager;

        public ProductsController(ProductManager productManager, ProductStockManager productStockManager, DashboardManager dashboardManager)
        {
            _productStockManager = productStockManager;
            _dashboardManager = dashboardManager;
            _productManager = productManager;
        }

        [Route("[action]")]
        [HttpGet]
        public async Task<List<ProductStockResult>> Search(string externalId, string searchText)
        {
            return await _productStockManager.GetProductStockByExternalId(externalId, searchText);
        }

        //Newly added on 16-Aug-2017 by Manivannan
        [HttpPost]
        [Route("[action]")]
        public async Task<List<ProductStockResult>> SearchProducts([FromBody]ProductSearchRequest data)
        {
            return (await _productStockManager.GetProductStocksByRequest(data)).ToList();
        }

        [Route("[action]")]
        [HttpGet]
        public async Task DashboardDailyMail()
        {
            await _dashboardManager.DashboardDailyMail("18204879-99ff-4efd-b076-f85b4a0da0a3");
        }

        [Route("[action]")]
        [HttpGet]
        public async Task  DailyMailForShopOwners()
        {
            await _dashboardManager.DailyMailForShopOwner();
        }

        [Route("[action]")]
        [HttpGet]
        public async Task DailyMailerForStock()
        {
            await _dashboardManager.DailyMailerForStock("013513b1-ea8c-4ea8-9fed-054b260ee197"); //Choolaimadu id added by poongodi on 05/04/2017
        }


        [Route("[action]")]
        [HttpGet]
        public async Task DashboardManagementMail()
        {
            await _dashboardManager.DashboardManagementMail();
        }
        //Method  Added by Poongodi on 06/06/2017
        [HttpPost]
        [Route("[action]")]
        public async Task<BranchWiseStock> GetBranchWiseStock([FromBody]ProductSearch data)
        {
            return await _productStockManager.GetBranchWiseStockItem(data);

         
        }

        [Route("[action]")]
        [HttpGet]
        public async Task PlusPharmacyMail()
        {
            await _dashboardManager.PlusPharmacyMail(); // Modified by Gavaskar 05-10-2017
        }

        [Route("[action]")]
        public async Task<List<Product>> GetProductCodes([FromBody]List<Product> data)
        {
            return await _productManager.GetProductCodes(data);
        }
        [Route("[action]")]
        public async Task<Either<ProductStock>> GetDataCorrectionProductStocks([FromBody]ProductStock data)
        {
            // return null;
            var user =  await _productManager.GetDataCorrectionProductStocks(data);
            return new Either<ProductStock>
            {
                IsSuccess = user.Item1,
                Data = user.Item2
            };
        }
        [Route("[action]")]
        public async Task<Either<ProductStock>> InsertDataCorrectionOpeningStocks([FromBody]ProductStock data)
        {
            var user = await _productManager.InsertDataCorrectionOpeningStocks(data);
            return new Either<ProductStock>
            {
                IsSuccess = user.Item1,
                Data = user.Item2
            };
        }
        [Route("[action]")]
        public async Task<Either<StockAdjustment>> InsertDataCorrectionStockAdjustments([FromBody]StockAdjustment data)
        {
            var user = await _productManager.InsertDataCorrectionStockAdjustments(data);
            return new Either<StockAdjustment>
            {
                IsSuccess = user.Item1,
                Data = user.Item2
            };
        }
        [Route("[action]")]
        public async Task<Either<TempVendorPurchaseItem>> InsertDataCorrectionTempStocks([FromBody]TempVendorPurchaseItem data)
        {
            var user = await _productManager.InsertDataCorrectionTempStocks(data);
            return new Either<TempVendorPurchaseItem>
            {
                IsSuccess = user.Item1,
                Data = user.Item2
            };
        }
        [HttpPost]
        [Route("[action]")]
        public async Task<Either<ProductStock>> UpdateProductStockToOnline([FromBody]ProductStock data)
        {
            var user = await _productManager.UpdateProductStockToOnline(data);
            return new Either<ProductStock>
            {
                IsSuccess = user.Item1,
                Data = user.Item2
            };
        }
        [HttpPost]
        [Route("[action]")]
        public async Task<string> GetReverseSyncOfflineToOnlineCompare([FromBody]string SeedIndex)
        {
            var SyncDataId = await _productManager.GetReverseSyncOfflineToOnlineCompare(SeedIndex);
            return SyncDataId;

        }

    }
}
