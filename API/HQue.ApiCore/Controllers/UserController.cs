﻿using System.Threading.Tasks;
using HQue.Biz.Core.Setup;
using HQue.Biz.Core.UserAccount;
using HQue.Contract.Infrastructure.Common;
using HQue.Contract.Infrastructure.UserAccount;
using System.Linq;
using Microsoft.AspNetCore.Mvc;

namespace HQue.ApiCore.Controllers
{
    [Route("[controller]")]
    public class UserController : Controller
    {
        private readonly UserManager _userManager;
        private readonly InstanceSetupManager _instanceSetupManager;

        public UserController(UserManager userManager, InstanceSetupManager instanceSetupManager)
        {
            _userManager = userManager;
            _instanceSetupManager = instanceSetupManager;
        }

        [HttpPost]
        [Route("[action]")]
        public async Task<Either<HQueUser>> ValidateUser(HQueUser data)
        {
            var user = await _userManager.ValidateUser(data);

            if (!user.Item2.HasInstanceId)
            {
                var instanceList = await _instanceSetupManager.List(user.Item2.AccountId);
                if (instanceList.Count > 0)
                {
                    user.Item2.InstanceId = instanceList.First().Id;
                    user.Item2.Instance = instanceList.First();
                }
            }

            return new Either<HQueUser>
            {
                IsSuccess = user.Item1,
                Data = user.Item2
            };
        }

        [HttpPost]
        [Route("[action]")]
        public async Task<Either<HQueUser>> ValidateUser2([FromBody]HQueUser data)
        {
            var user = await _userManager.ValidateUser(data);

            if (!user.Item2.HasInstanceId)
            {
                var instanceList = await _instanceSetupManager.List(user.Item2.AccountId);
                if (instanceList.Count > 0)
                {
                    user.Item2.InstanceId = instanceList.First().Id;
                    user.Item2.Instance = instanceList.First();
                }
            }
            //var getAccount=await _instanceSetupManager.AccountList(user.Item2.AccountId);
            //user.Item2.Instance.AccountName = getAccount[0].Name;
            //user.Item2.Instance.InstanceCount = getAccount[0].InstanceCount;
            //user.Item2.Instance.IsBillNumberReset = getAccount[0].IsBillNumberReset;
            await _instanceSetupManager.UpdateOfflineStatus(user.Item2.InstanceId);

            return new Either<HQueUser>
            {
                IsSuccess = user.Item1,
                Data = user.Item2
            };
        }


        [HttpPost]
        [Route("[action]")]
        public async Task<Either<string>> ResetPassword(string userId)
        {
            //var result = await _userManager.ValidateEmail(user);
            var result = await _userManager.ExternalResetPassword(userId);
            var successMsg = "";
            if(result.Item1==true)
            {
                successMsg = userId +" password reset successful";
            }
            else
            {
                successMsg= userId + " does not exist";
            }

            return new Either<string>
            {
                IsSuccess = result.Item1,
                Data = successMsg,
            };
        }
    }
}
