﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using System.Net.Http;
using Microsoft.AspNetCore;
using System.IO;
using System.Collections;
using System.Net;
using Newtonsoft.Json;
//Test

namespace HQue.ApiCore.Controllers
{
    [Route("[controller]")]
    public class UploadController
    {
        //[Route("[action]")]
        //[HttpPost]
        //public async Task<bool> SyncData(string Content, string FolderName, string FileName)
        //{

        //    try
        //    {
        //        //   string SyncFilePath = Directory.GetCurrentDirectory() + "\\SyncData\\" + CreatedAt.ToString("yyyyMMdd") + "\\";
        //        string SyncFilePath = "H:\\" + "\\SyncData\\" + FolderName + "\\";

        //        if (!Directory.Exists(SyncFilePath))
        //            Directory.CreateDirectory(SyncFilePath);


        //        string pathString = System.IO.Path.Combine(SyncFilePath, FileName);
        //        var fileCreate = System.IO.File.Create(pathString);

        //        using (StreamWriter sw = new StreamWriter(fileCreate))
        //        {
        //           await sw.WriteLineAsync(Content);
        //        }
        //        return true;
        //    }
        //    catch (Exception ex)
        //    {
        //        return false;
        //    }
        //}

        [Route("[action]")]
        [HttpPost]
        public async Task<HttpResponseMessage> SyncData([FromBody]Dictionary<string, object> paramList)
        {

            try
            {
               // Dictionary<string, object> paramList = JsonConvert.DeserializeObject<Dictionary<string, object>>(param);
                if (paramList.Count >= 4)
                {
                     string SyncPath = paramList["path"].ToString();
                    string FolderName = paramList["folderName"].ToString();
                    string FileName = paramList["fileName"].ToString();

                    string SyncFilePath = "../../storage/"+ SyncPath+"/"+ FolderName; // "H:\\" + "\\SyncData\\" + FolderName + "\\";

                    if (!Directory.Exists(SyncFilePath))
                        Directory.CreateDirectory(SyncFilePath);


                    string pathString = System.IO.Path.Combine(SyncFilePath, FileName);
                    var fileCreate = System.IO.File.Create(pathString);

                    using (StreamWriter sw = new StreamWriter(fileCreate))
                    {
                        await sw.WriteLineAsync(paramList["data"].ToString());
                    }
                    HttpResponseMessage response = new HttpResponseMessage { StatusCode = HttpStatusCode.Created };
                    return response;
                }
                else
                {
                    HttpResponseMessage response = new HttpResponseMessage { StatusCode = HttpStatusCode.InternalServerError };
                    return response;
                }
            }
            catch (Exception ex)
            {
                return new HttpResponseMessage { StatusCode = HttpStatusCode.InternalServerError }; ;
            }
        }
    }
}
