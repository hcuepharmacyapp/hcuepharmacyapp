﻿using HQue.Communication.Sms;
using HQue.Contract.Infrastructure.Setup;
using HQue.DataAccess.Inventory;
using HQue.DataAccess.Setup;
using System;
using System.Diagnostics;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace HQue.ApiCore.Controllers
{
    public class SmsReportController
    {

        private readonly SmsSender _smsSender;
        private readonly SalesDataAccess _salesDataAccess;
        private readonly InstanceSetupDataAccess _instanceSetupDataAccess;

        public SmsReportController(SmsSender smsSender, SalesDataAccess  salesDataAccess, InstanceSetupDataAccess instanceSetupDataAccess)
        {

            _smsSender = smsSender;
            _salesDataAccess = salesDataAccess;
            _instanceSetupDataAccess = instanceSetupDataAccess;
        }

        [Route("[action]")]
        [HttpGet]
        public async Task<bool> SmsDaySummary(string externalId, string searchText)
        {
            var result = false;

            var list = await _instanceSetupDataAccess.GetPharmacyList();
            foreach (var i in list)
            {
                var msg = BuildSmsBody(i.Name, i.Address);
                var strSummary = await _salesDataAccess.GetTotalTransactionSummary(i.InstanceId);
                msg.AppendLine();
                msg.Append(strSummary);
                msg.AppendLine();
                msg.AppendLine("Powered by HCue.in");

                Debug.WriteLine("****************"+ i.Phone +"*****************************");
                Debug.Write(msg.ToString());
                result = await _smsSender.Send(i.Phone, msg.ToString());
            }


            return result;
        }


        private StringBuilder BuildSmsBody(string pharmaName, string location)
        {
            var smsContent = new StringBuilder();

            smsContent.AppendLine(string.Format("{0} Transaction summary", DateTime.Today.ToString("MMM-dd-yyyy")));
            smsContent.AppendLine(string.Format("Branch : {0}", pharmaName));

            return smsContent;
        }
    }
}
