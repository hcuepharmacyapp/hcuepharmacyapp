﻿using System.Collections.Generic;
using System.Threading.Tasks;
using HQue.Biz.Core.Setup;
using HQue.Contract.Infrastructure.Setup;
using Microsoft.AspNetCore.Mvc;
namespace HQue.ApiCore.Controllers
{
    [Route("[controller]")]
    public class InstanceController : Controller
    {
        private readonly InstanceSetupManager _instanceSetupManager;

        public InstanceController(InstanceSetupManager instanceSetupManager)
        {
            _instanceSetupManager = instanceSetupManager;
        }

        [Route("[action]")]
        [HttpGet]
        public async Task<List<Instance>> List(string accountId)
        {
            return await _instanceSetupManager.List(accountId);
        }
    }
}
