﻿using HQue.Biz.Core.ETL;
using HQue.Biz.Core.Inventory;
using HQue.Biz.Core.Sync;
using HQue.Contract.External;
using HQue.Contract.External.Sync;
using HQue.Contract.Infrastructure.Inventory;
using HQue.Contract.Infrastructure.Master;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using HQue.DataAccess.Setup;
using HQue.Contract.Infrastructure.Settings;
using Utilities.Helpers;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using System.Linq;

namespace HQue.ApiCore.Controllers
{
    [Route("[controller]")]
    public class SyncSchedulerController : Controller
    {
        private SyncController _syncController;

        private readonly SyncManager _syncManager;
        private readonly Import _import;
        private readonly ConfigHelper _configHelper;

        private int FileNum = 0;
        public static int FileCount = 0;
        public static string FolderName = "";


        public SyncSchedulerController(SyncManager syncManager, Import import, ConfigHelper configHelper)
        {
            _syncManager = syncManager;
            _import = import;
            _configHelper = configHelper;
        }

        [Route("[action]")]
        [HttpPost]
        public async Task<bool> SyncData()
        {
            try
            {
                CloudStorageAccount storageAccount = CloudStorageAccount.Parse(_configHelper.AppConfig.StorageConnection);
                // Create the blob client.
                CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();
                //Getting container reference
                CloudBlobContainer container = blobClient.GetContainerReference("syncdata-" + _configHelper.AppConfig.AppEnvironment);

                var blobList = await container.ListBlobsSegmentedAsync(string.Empty, false, BlobListingDetails.None, int.MaxValue, null, null, null);

                IEnumerable<IListBlobItem> DirectoryList = blobList.Results;

                foreach (IListBlobItem item in DirectoryList)
                {
                    int fileCount = 0;
                    bool IsRepeat = true;
                    if (item.GetType() == typeof(CloudBlobDirectory))
                    {
                        CloudBlobDirectory directory = (CloudBlobDirectory)item;
                        if (directory.Prefix == "00010101/" || directory.Prefix == "closingstock/")
                        {
                            continue;
                        }
                        //if (!directory.Prefix.StartsWith("201811"))
                        //{
                        //    continue;
                        //}


                        FileNum = 0;
                        while (IsRepeat)
                        {
                            try
                            {
                                var files = directory.ListBlobsSegmentedAsync(false, BlobListingDetails.Metadata, 100, null, null, null).Result;
                                IEnumerable<IListBlobItem> FileList = files.Results;


                                fileCount = 0;
                                foreach (IListBlobItem file in FileList)
                                {
                                    ++fileCount;
                                    ++FileNum;
                                    try
                                    {
                                        if (file.GetType() == typeof(CloudBlockBlob))
                                        {
                                            await SaveBlobContent(container, file, false);
                                        }
                                    }
                                    catch (Exception e)
                                    {
                                        return false;
                                    }
                                }
                                if (fileCount == 0)
                                {
                                    //Remove Directory 
                                    IsRepeat = false;
                                    //  await container.GetBlockBlobReference(((CloudBlockBlob)item).Name).DeleteIfExistsAsync();
                                }
                            }

                            catch (Exception e)
                            {
                                IsRepeat = false;
                            }
                        }

                    }

                }
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        private async Task<bool> SaveBlobContent(CloudBlobContainer container, IListBlobItem file, bool bsyncxml)
        {
            CloudBlockBlob blob = (CloudBlockBlob)file;
            string content = await blob.DownloadTextAsync();
            bool Success = true;
            if (bsyncxml == false && content.Contains("FileType") && content.Contains("xml"))
            {
                Success = true;
            }
            else
            {
                Success = await SyncDataToServer(content);
                if (Success)
                {
                    return await container.GetBlockBlobReference((blob).Name).DeleteIfExistsAsync();
                }
            }
            return Success;
        }
        private async Task<bool> SaveBlobContent(CloudBlobContainer container, IListBlobItem file)
        {
            CloudBlockBlob blob = (CloudBlockBlob)file;
            string content = await blob.DownloadTextAsync();


            bool Success = await SyncDataToServer(content);
            if (Success)
            {
                return await container.GetBlockBlobReference((blob).Name).DeleteIfExistsAsync();
            }

            return Success;
        }
        private async Task<bool> SyncDataToServer(string content)
        {
            try
            {
                _syncController = new SyncController(_syncManager, _import, _configHelper);

                var settings = new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore,
                    MissingMemberHandling = MissingMemberHandling.Ignore
                };
                var obj = JsonConvert.DeserializeObject<object>(content, settings);

                bool success = await _syncController.SyncDataToServer(obj, true);
                return success;
            }
            catch (Exception ex)
            {
                return false;
            }
        }


        [Route("[action]")]
        [HttpGet]
        public async Task<bool> SyncDataDirectory(string dir)
        {
            try
            {
                string dirName = dir;   // "filewodate";
                CloudStorageAccount storageAccount = CloudStorageAccount.Parse(_configHelper.AppConfig.StorageConnection);
                // Create the blob client.
                CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();
                //Getting container reference

                // string folderPath = "syncdata-" + _configHelper.AppConfig.AppEnvironment;// + "/" + dirName;// + "/";
                string folderPath = "syncdata-" + _configHelper.AppConfig.AppEnvironment;// + "/" + dirName;// + "/";

                //   CloudBlobContainer container = blobClient.GetContainerReference(folderPath);


                CloudBlobContainer container = blobClient.GetContainerReference(folderPath);
                CloudBlobDirectory blobDirectory = container.GetDirectoryReference(dirName);

                int MaxCount = 500;

                int fileCount = 0;
                bool IsRepeat = true;

                DateTime startTime = DateTime.Now;

                while (IsRepeat)
                {
                    try
                    {
                        //  var files = await container.ListBlobsSegmentedAsync(dirName + "/", false, BlobListingDetails.None, 100, null, null, null);
                        var files = await blobDirectory.ListBlobsSegmentedAsync(true, BlobListingDetails.None, 100, null, null, null);
                        IEnumerable<IListBlobItem> FileList = files.Results;

                        bool HasFile = false;

                        foreach (IListBlobItem file in FileList)
                        {
                            ++fileCount;
                            HasFile = true;
                            try
                            {
                                if (file.GetType() == typeof(CloudBlockBlob))
                                {                                   
                                    await SaveBlobContent(container, file);
                                }
                            }
                            catch (Exception e)
                            {
                                return false;
                            }

                        }
                        if (fileCount >= MaxCount || HasFile == false)
                        {
                            IsRepeat = false;
                            break;
                        }
                    }
                    catch (Exception e)
                    {
                        IsRepeat = false;
                    }

                }

              //  DateTime EndTime = DateTime.Now;
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

      
      
        [Route("[action]")]
        [HttpGet]
        public async Task<bool> StartSync(string dir="")
        {        
           return await SyncFromLocalDrive("../../storage/" + "syncdata-" + _configHelper.AppConfig.AppEnvironment + "/", dir);
        }


        private async Task<bool> SyncFromLocalDrive(string FilePath, string dir = "")
        {
            string NewId = Guid.NewGuid().ToString();
            try
            {
                // WriteLog(FilePath);
                await _syncManager.LogScheduler(NewId, 1, "START", DateTime.Now);

                List<string> DirectoryList = new List<string>();

                if (dir != "")
                    DirectoryList.Add(FilePath + "\\" + dir);
                else
                    DirectoryList = Directory.GetDirectories(FilePath).ToList();

                DirectoryList.Sort();

                DateTime StartTime = DateTime.Now;
                FileCount = 0;

                foreach (string dirName in DirectoryList)
                {
                    DirectoryInfo info = new DirectoryInfo(dirName);
                    FolderName = info.Name;

                    int TotalFileCount = info.GetFiles().Count();

                    do
                    {
                        FileInfo[] files = info.GetFiles().OrderBy(p => p.CreationTime).Take(100).ToArray();
                        //   FileInfo[] files = info.GetFiles().OrderBy(p => p.CreationTime).ToArray();
                        bool Success = false;

                        TotalFileCount = TotalFileCount - files.Length;
                        
                        if (files.Length == 0)
                        {
          
                            try
                            {
                                TotalFileCount = -1;
                                info.Delete();
                            }
                            catch (Exception e)
                            {
                            }
                            
                        }
                        foreach (FileInfo file in files)
                        {
                            ++FileCount;
                            Success = false;
                            using (FileStream fs = new FileStream(file.FullName, FileMode.Open, FileAccess.Read))
                            {
                                using (var sr = new StreamReader(fs, Encoding.UTF8))
                                {
                                    var content = await sr.ReadToEndAsync();

                                    Success = await SyncDataToServer(content);
                                }
                            }

                            if (Success)
                            {
                                file.Delete();
                            }
                        }
                    } while (TotalFileCount >= 0);
                }

                await _syncManager.LogSchedulerUpdate(NewId, 1, "END", DateTime.Now);

                return true;
            }
            catch (Exception e)
            {
                await _syncManager.LogSchedulerUpdate(NewId, 1, e.Message, DateTime.Now);
                return false;
            }
        }
    }
}