﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using System.IO;

// For more information on enabling Web API for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace HQue.ApiCore.Controllers
{
    [Route("api/[controller]")]
    public class LogController : Controller
    {
        // GET: api/values
        [HttpGet]
        public string log(string id)
        {
            if (id == "1" || string.IsNullOrEmpty(id))
            {
                string name = string.Format("leadsAPI.txt");

                //FileInfo logFile = new FileInfo(name);
                HttpContext.Response.ContentType = "text/plain";

                return System.IO.File.ReadAllText(name);
            }

            if(id == "2")
            {
                string name = string.Format("patientAPI.txt");

                //FileInfo logFile = new FileInfo(name);
                HttpContext.Response.ContentType = "text/plain";

                return System.IO.File.ReadAllText(name);
            }
            if (id == "3")
            {
                //string name = string.Format("SyncAPI2.txt");
                string name = string.Format("SyncAPI{0}_{1}.txt", DateTime.Now.Date.ToString("yyyyMMdd"), DateTime.Now.Hour.ToString());
                //FileInfo logFile = new FileInfo(name);
                HttpContext.Response.ContentType = "text/plain";

                return System.IO.File.ReadAllText(name);
            }

            return "";
        }


      
    }
}
