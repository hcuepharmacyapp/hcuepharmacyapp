﻿using System.Collections.Generic;
using System.Threading.Tasks;
using HQue.Biz.Core.Master;
using HQue.Contract.Infrastructure.Master;
using Microsoft.AspNetCore.Mvc;

namespace HQue.ApiCore.Controllers
{
    [Route("[controller]")]
    public class PatientController : Controller
    {
        private readonly PatientManager _patientManager;

        public PatientController(PatientManager patientManager)
        {
            _patientManager = patientManager;
        }

        [HttpPost]
        [Route("[action]")]
        public async Task<IEnumerable<Patient>> Search([FromBody]Patient data)
        {
            return await _patientManager.PatientList(data,data.AccountId,data.InstanceId);
        }
    }
}
