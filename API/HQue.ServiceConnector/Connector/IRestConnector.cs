using System.Threading.Tasks;

namespace HQue.ServiceConnector.Connector
{
    public interface IRestConnector
    {
        Task<string> PostAsyc(string uri, object data);
        Task<string> GetAsyc(string uri);
        Task<T> PostAsyc<T>(string uri, object data);
        Task<T> GetAsyc<T>(string uri);
        void AddHeaders(string name, string value);
    }
}