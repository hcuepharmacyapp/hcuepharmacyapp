﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Diagnostics;

namespace HQue.ServiceConnector.Connector
{
    public class RestConnector : IRestConnector
    {

        private IDictionary<string, string> _header;

        public CookieContainer Cookies { get; set; }

        public RestConnector() : this(new CookieContainer())
        {

        }

        public RestConnector(CookieContainer cookieContainer)
        {
            Cookies = cookieContainer;
            _header = new Dictionary<string, string>();
        }

        public async Task<string> PostAsyc(string uri, object data)
        {
            var jsonData = JsonConvert.SerializeObject(data);
            HttpContent d = new StringContent(jsonData);
            d.Headers.ContentType = new MediaTypeHeaderValue("text/json");

            using (var client = GetClient())
            using (var response = await client.PostAsync(uri, d))
            using (var content = response.Content)
            {
                var result = await content.ReadAsStringAsync();
                return result;
            }
        }

        public async Task<string> GetAsyc(string uri)
        {
            var url = new Uri(uri);
            using (var client = GetClient())
            using (var response = await client.GetAsync(url))
            using (var content = response.Content)
            {
                var result = await content.ReadAsStringAsync();
                return result;
            }
        }

        public async Task<HttpResponseHeaders> GetHeaderAsyc(string uri)
        {
            var url = new Uri(uri);
            using (var client = GetClient())
            using (var response = await client.GetAsync(url))
                return response.Headers;
        }

        public async Task<T> PostAsyc<T>(string uri, object data)
        {
            Debug.Write(JsonConvert.SerializeObject(data));
            var result = await PostAsyc(uri, data);
            return JsonConvert.DeserializeObject<T>(result);
        }

        public async Task<string> PostAsycMobileVerify<T>(string uri, object data)
        {
            Debug.Write(JsonConvert.SerializeObject(data));
            var result = await PostAsycMobileVerify(uri, data);
            return result;
        }

        public async Task<string> PostAsycMobileVerify(string uri, object data)
        {
            var jsonData = JsonConvert.SerializeObject(data);
            HttpContent d = new StringContent(jsonData);
            d.Headers.ContentType = new MediaTypeHeaderValue("application/json");

            using (var client = GetClient())
            using (var response = await client.PostAsync(uri, d))
            using (var content = response.Content)
            {
                var result = await content.ReadAsStringAsync();
                return result;
            }
        }

        public async Task<string> Post2Asyc<T>(string uri, object data)
        {
            Debug.Write(JsonConvert.SerializeObject(data));
            var result = await PostAsyc(uri, data);
            return result;
        }

        public async Task<T> GetAsyc<T>(string uri)
        {
            var result = await GetAsyc(uri);
            return JsonConvert.DeserializeObject<T>(result);
        }

        public void AddHeaders(string name, string value)
        {
            _header.Add(name,value);
        }

        private HttpClient GetClient()
        {
            //Cookies.SetCookies
            var client = new HttpClient(new HttpClientHandler { AutomaticDecompression = DecompressionMethods.GZip, CookieContainer = Cookies });
            client.DefaultRequestHeaders.AcceptEncoding.Add(new StringWithQualityHeaderValue("gzip"));
            foreach (var item in _header)
            {
                client.DefaultRequestHeaders.TryAddWithoutValidation(item.Key, item.Value);
            }
            return client;
        }
    }
}
