﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;
using System.Data.Common;
using Dapper;
using DataAccess.QueryBuilder;

namespace DataAccess
{
    public class SqlDatabaseHelper
    {
        string _connection = string.Empty;
        IDbConnection dbconn;
        public SqlDatabaseHelper(string connection)
        {
            _connection = connection;

        } 
     
        public async Task<IEnumerable<T>> ExecuteQueryAsync<T>(string sql, object parameters = null)
        {
            using (dbconn = new SqlConnection(_connection))
            {
                dbconn.Open(); 
                CommandDefinition comm = new CommandDefinition(sql, parameters, commandType: CommandType.Text);
                return await dbconn.QueryAsync<T>(comm);
            }
        }

        public async Task<object> ExecuteScalarAsync(string sql, object parameters)
        {
            using (dbconn = new SqlConnection(_connection))
            {
                dbconn.Open();
                CommandDefinition comm = new CommandDefinition(sql, parameters, commandType: CommandType.Text);

                return await dbconn.ExecuteScalarAsync(comm);

            }
        }
         
        public async Task<bool> ExecuteAsync(string sql, object items)
        {

            using (dbconn = new SqlConnection(_connection))
            {
                dbconn.Open();
                int result = await dbconn.ExecuteAsync(sql, items);
                return result == -1 ? false : true;
            }
        }
         
        public async Task<IEnumerable<T>> ExecuteProcedureAsync<T>(string procName, Dictionary<string,object> parameters)
        {
            using (dbconn = new SqlConnection(_connection))
            {
                CommandDefinition comm = default(CommandDefinition);
                dbconn.Open();
                var parms = GetParameters(parameters);
                if (procName == "usp_Get_DrugWiseReportList")
                    comm = new CommandDefinition(procName, parms, commandTimeout: 0, commandType: CommandType.StoredProcedure);
                else
                    comm = new CommandDefinition(procName, parms, commandType: CommandType.StoredProcedure);

                return await dbconn.QueryAsync<T>(comm);

            }
        }
        public async Task<IEnumerable<T>> ExecuteProcedureAsync<T>(string procName, Dictionary<string, object> parameters, SqlConnection con, SqlTransaction transaction)
        {
            var parms = GetParameters(parameters);
            CommandDefinition comm = new CommandDefinition(procName, parms, transaction, commandType: CommandType.StoredProcedure);
            return await con.QueryAsync<T>(comm);
        }
        public async Task<IEnumerable<T>> ExecuteProcedureAsync<T>(QueryBuilderBase query, SqlConnection con, SqlTransaction transaction)
        {
            var parms = GetParameters(query);
            CommandDefinition comm = new CommandDefinition(query.GetQuery(), parms, transaction, commandType: CommandType.StoredProcedure);
            return await con.QueryAsync<T>(comm);
        }
        private DynamicParameters GetParameters(Dictionary<string,object> parms)
        {
            DynamicParameters parmeters = new DynamicParameters();
            foreach (var parm in parms)
            {
                parmeters.Add( parm.Key, parm.Value);
            }
            return parmeters;
        }
        private DynamicParameters GetParameters(QueryBuilderBase query)
        {
            DynamicParameters parmeters = new DynamicParameters();
            foreach (var parm in query.Parameters)
            {
                parmeters.Add(parm.Key, parm.Value);
            }
            return parmeters;
        }

    }
}
