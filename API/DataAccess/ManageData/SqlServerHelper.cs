﻿using DataAccess.ManageData.Interface;
using System;
using System.Threading.Tasks;
using DataAccess.QueryBuilder;
using Utilities.Logger;
using Utilities.Helpers;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.SqlClient;
using System.Data;
using System.Diagnostics;

namespace DataAccess.ManageData
{
    public class SqlServerHelper : BaseHelper, ISqlHelper
    {
        public bool IsItFromReplication { get; set; }

        public DbOperationsEventHandler OnSyncQueueOperation { get; set; }
        public SqlServerHelper(string connectionString) : base(connectionString)
        {
        }

        public SqlServerHelper(ConfigHelper configHelper) : base(configHelper)
        {
        }

        private bool NonQuery(QueryBuilderBase query)
        {
            LogAndException.WriteToLogger("ExecuteReaderByParamer", ErrorLevel.Debug);
            var command = new SqlCommand
            {
                CommandType = CommandType.Text,
                CommandText = query.GetQuery(),
                Connection = new SqlConnection(Connection)
            };

            try
            {
                foreach (var parameter in query.Parameters)
                    command.Parameters.AddWithValue(parameter.Key, parameter.Value ?? DBNull.Value);
                command.Connection.Open();
                command.ExecuteNonQuery();

        
                if(OnSyncQueueOperation != null)
                    OnSyncQueueOperation(query);

                return true;
            }
            catch (SqlException ex)
            {
                LogAndException.WriteToLogger(ex.Message, ErrorLevel.Error);
                throw;
            }
            catch (Exception ex)
            {
                LogAndException.WriteToLogger(ex.Message, ErrorLevel.Error);
                throw;
            }
            finally
            {
                command.Connection.Close();
                command.Dispose();
            }
        }

        private DbDataReader Query(QueryBuilderBase query)
        {
            LogAndException.WriteToLogger("ExecuteReaderByParamer", ErrorLevel.Debug);
            var command = new SqlCommand
            {
                CommandType = CommandType.Text,
                CommandText = query.GetQuery(),
                Connection = new SqlConnection(Connection)
            };

            try
            {
                foreach (var parameter in query.Parameters)
                    command.Parameters.AddWithValue(parameter.Key, parameter.Value ?? DBNull.Value);
                command.Connection.Open();
                return command.ExecuteReader(CommandBehavior.CloseConnection);
            }
            catch (SqlException ex)
            {
                Debug.WriteLine(command.CommandText);
                LogAndException.WriteToLogger(ex.Message, ErrorLevel.Error);
                 throw;
            }
            catch (Exception ex)
            {
                LogAndException.WriteToLogger(ex.Message, ErrorLevel.Error);
                throw;
            }
        }


        private DbDataReader ExecuteProc(QueryBuilderBase query)
        {
            LogAndException.WriteToLogger("ExecuteReaderByParamer", ErrorLevel.Debug);
            var command = new SqlCommand
            {
                CommandType = CommandType.StoredProcedure,
                CommandText = query.GetQuery(),
                Connection = new SqlConnection(Connection)
            };

            try
            {
                foreach (var parameter in query.Parameters)
                    command.Parameters.AddWithValue(parameter.Key, parameter.Value ?? DBNull.Value);
                command.Connection.Open();
                return command.ExecuteReader(CommandBehavior.CloseConnection);
            }
            catch (SqlException ex)
            {
                LogAndException.WriteToLogger(ex.Message, ErrorLevel.Error);
                throw;
            }
            catch (Exception ex)
            {
                LogAndException.WriteToLogger(ex.Message, ErrorLevel.Error);
                throw;
            }
        }

        public override DbCommand SetDbCommand(QueryBuilderBase query)
        {
            var queryString = "INSERT INTO REPLICATIONDATA (Id,AccountId,InstanceId,ClientId,TableName,Action,ActionTime,CreatedAt,UpdatedAt,CreatedBy,UpdatedBy,TransactionId)"
                + "VALUES(@Id,@AccountId,@InstanceId,@ClientId,@TableName,@Action,@ActionTime,@CreatedAt,@UpdatedAt,@CreatedBy,@UpdatedBy,@TransactionId)";

            var command = new SqlCommand
            {
                CommandType = CommandType.Text,
                CommandText = queryString,
                Connection = new SqlConnection(Connection)
            };

            PopulateCommandParameters(command, query);
            return command;
        }

        private void PopulateCommandParameters(SqlCommand command, QueryBuilderBase query)
        {
            command.Parameters.AddWithValue("Id", query.Parameters["Id"]);
            command.Parameters.AddWithValue("AccountId", query.Parameters["AccountId"]);
            if (query.Parameters.Keys.Contains("UpdatedBy"))
                command.Parameters.AddWithValue("InstanceId", query.Parameters["InstanceId"]);
            else
                command.Parameters.AddWithValue("InstanceId", DBNull.Value);

            command.Parameters.AddWithValue("ClientId", Constant.ClientId);
            command.Parameters.AddWithValue("TableName", query.Table.TableName);
            command.Parameters.AddWithValue("Action", 1);
            command.Parameters.AddWithValue("ActionTime", CustomDateTime.IST);
            command.Parameters.AddWithValue("CreatedAt", query.Parameters["CreatedAt"]);
            command.Parameters.AddWithValue("CreatedBy", query.Parameters["CreatedBy"]);

            if (query.Parameters.Keys.Contains("UpdatedBy"))
                command.Parameters.AddWithValue("UpdatedBy", query.Parameters["UpdatedBy"]);
            else
                command.Parameters.AddWithValue("UpdatedBy", "");
            if (query.Parameters.Keys.Contains("UpdatedAt"))
                command.Parameters.AddWithValue("UpdatedAt", query.Parameters["UpdatedAt"]);
            else
                command.Parameters.AddWithValue("UpdatedAt", CustomDateTime.IST);

            command.Parameters.AddWithValue("TransactionId", DBNull.Value);
        }

        private object SingleValue(QueryBuilderBase query)
        {
            LogAndException.WriteToLogger("ExecuteReaderByParamer", ErrorLevel.Debug);
            var command = new SqlCommand
            {
                CommandType = CommandType.Text,
                CommandText = query.GetQuery(),
                Connection = new SqlConnection(Connection)
            };

            try
            {
                foreach (var parameter in query.Parameters)
                    command.Parameters.AddWithValue(parameter.Key, parameter.Value ?? DBNull.Value);
                command.Connection.Open();
                return command.ExecuteScalar();
            }
            catch (SqlException ex)
            {
                LogAndException.WriteToLogger(ex.Message, ErrorLevel.Error);
                throw;
            }
            catch (Exception ex)
            {
                LogAndException.WriteToLogger(ex.Message, ErrorLevel.Error);
                throw;
            }
            finally
            {
                command.Connection.Close();
                command.Dispose();
            }
        }

        public Task<bool> NonQueryAsyc(QueryBuilderBase query)
        {
            return Task.Run(() => NonQuery(query));
        }

        public Task<DbDataReader> QueryAsyc(QueryBuilderBase query)
        {
            return Task.Run(() => Query(query));
        }

        public Task<DbDataReader> ExecuteProcAsyc(QueryBuilderBase query)
        {
            return Task.Run(() => ExecuteProc(query));
        }

        public Task<object> SingleValueAsyc(QueryBuilderBase query)
        {
            return Task.Run(() => SingleValue(query));
        }

        public Task<bool> ExecuteNonQueryAsyc(IEnumerable<QueryBuilderBase> queries)
        {
            //Commented by Poongodi to fix deadlock issue as suggested by Martin on 11/08/2017
            LogAndException.WriteToLogger("ExecuteNonQueryAsyc", ErrorLevel.Debug);
            /*  var connection = new SqlConnection(Connection);
              connection.Open();
              var transaction = connection.BeginTransaction();
              try
              {
                  foreach (var query in queries)
                  {
                      if (query == null) continue;

                      var command = new SqlCommand
                      {
                          Connection = connection,
                          Transaction = transaction,
                          CommandType = CommandType.Text,
                          CommandText = query.GetQuery()
                      };

                      foreach (var parameter in query.Parameters)
                          command.Parameters.AddWithValue(parameter.Key, parameter.Value ?? DBNull.Value);

                      command.ExecuteNonQuery();

                      if (OnSyncQueueOperation != null)
                          OnSyncQueueOperation(query);

                  }

                  transaction.Commit();
                  return Task.FromResult(true);
              }
              catch (SqlException ex)
              {
                  LogAndException.WriteToLogger(ex.Message, ErrorLevel.Error);
                  transaction.Rollback();
                  throw;
              }
              catch (Exception ex)
              {
                  LogAndException.WriteToLogger(ex.Message, ErrorLevel.Error);
                  transaction.Rollback();
                  throw;
              }
              finally
              {
                  connection.Close();
                  connection.Dispose();
                  transaction.Dispose();
              }*/
            return null;
        }


        public Task<bool> NonQueryAsyc2(QueryBuilderBase query)
        {
            LogAndException.WriteToLogger("ExecuteReaderByParamer", ErrorLevel.Debug);
            var command = new SqlCommand
            {
                CommandType = CommandType.Text,
                CommandText = query.GetQuery(),
                Connection = new SqlConnection(Connection)
            };

            try
            {
                foreach (var parameter in query.Parameters)
                    command.Parameters.AddWithValue(parameter.Key, parameter.Value ?? DBNull.Value);
                command.Connection.Open();
                command.ExecuteNonQuery();

                return Task.FromResult(true);
            }
            catch (SqlException ex)
            {
                LogAndException.WriteToLogger(ex.Message, ErrorLevel.Error);
                throw;
            }
            catch (Exception ex)
            {
                LogAndException.WriteToLogger(ex.Message, ErrorLevel.Error);
                throw;
            }
            finally
            {
                command.Connection.Close();
                command.Dispose();
            }
        }

        /// <summary>
        /// This method pointed to LOGDB --
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        public Task<bool> NonQueryAsyc3(QueryBuilderBase query)
        {
            LogAndException.WriteToLogger("ExecuteReaderByParamer", ErrorLevel.Debug);
            var command = new SqlCommand
            {
                CommandType = CommandType.Text,
                CommandText = query.GetQuery(),
                Connection = new SqlConnection(LogConnection)
            };

            try
            {
                foreach (var parameter in query.Parameters)
                    command.Parameters.AddWithValue(parameter.Key, parameter.Value ?? DBNull.Value);
                command.Connection.Open();
                command.ExecuteNonQuery();

                return Task.FromResult(true);
            }
            catch (SqlException ex)
            {
                LogAndException.WriteToLogger(ex.Message, ErrorLevel.Error);
                throw;
            }
            catch (Exception ex)
            {
                LogAndException.WriteToLogger(ex.Message, ErrorLevel.Error);
                throw;
            }
            finally
            {
                command.Connection.Close();
                command.Dispose();
            }
        }

        public SqlConnection OpenConnection()
        {
            SqlConnection con = new SqlConnection(Connection);
            con.Open();
            return con;
        }

        public bool CloseConnection(SqlConnection con)
        {
            con.Close();
            con.Dispose();
            return true;
        }

        private bool NonQuery(QueryBuilderBase query, SqlConnection con, SqlTransaction transaction)
        {
            var command = con.CreateCommand();
            command.Connection = con;
            command.Transaction = transaction;
            command.CommandText = query.GetQuery();

            foreach (var parameter in query.Parameters)
                command.Parameters.AddWithValue(parameter.Key, parameter.Value ?? DBNull.Value);
            command.ExecuteNonQuery();

            return true;
        }

        public Task<bool> NonQueryAsyc(QueryBuilderBase query, SqlConnection con, SqlTransaction transaction)
        {
            return Task.Run(() => NonQuery(query, con, transaction));
        }

        private object SingleValue(QueryBuilderBase query, SqlConnection con, SqlTransaction transaction)
        {
            var command = new SqlCommand
            {
                CommandType = CommandType.Text,
                CommandText = query.GetQuery(),
                Connection = con,
                Transaction = transaction
            };

            try
            {
                foreach (var parameter in query.Parameters)
                    command.Parameters.AddWithValue(parameter.Key, parameter.Value ?? DBNull.Value);
                return command.ExecuteScalar();
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                command.Dispose();
            }
        }

        public Task<object> SingleValueAsyc(QueryBuilderBase query, SqlConnection con, SqlTransaction transaction)
        {
            return Task.Run(() => SingleValue(query, con, transaction));
        }

        private DbDataReader Query(QueryBuilderBase query, SqlConnection con, SqlTransaction transaction)
        {
            var command = new SqlCommand
            {
                CommandType = CommandType.Text,
                CommandText = query.GetQuery(),
                Connection = con,
                Transaction = transaction
            };

            try
            {
                foreach (var parameter in query.Parameters)
                    command.Parameters.AddWithValue(parameter.Key, parameter.Value ?? DBNull.Value);
                return command.ExecuteReader();
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                command.Dispose();
            }
        }

        public Task<DbDataReader> QueryAsyc(QueryBuilderBase query, SqlConnection con, SqlTransaction transaction)
        {
            return Task.Run(() => Query(query, con, transaction));
        }

    }
}