﻿using System;
using DataAccess.ManageData.Interface;
using DataAccess.QueryBuilder;
using Utilities.Logger;
using Utilities.Helpers;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Data.Common;
using Microsoft.Data.Sqlite;
using System.Data;
using System.Data.SqlClient;

namespace DataAccess.ManageData
{
    public class SqLiteHelper : BaseHelper, ISqlHelper
    {
        public bool IsItFromReplication { get; set; }

        bool ISqlHelper.IsItFromReplication
        {
            get
            {
                throw new NotImplementedException();
            }

            set
            {
                throw new NotImplementedException();
            }
        }

        public DbOperationsEventHandler OnSyncQueueOperation
        {
            get
            {
                throw new NotImplementedException();
            }

            set
            {
                throw new NotImplementedException();
            }
        }

        public SqLiteHelper(string connectionString) : base(connectionString)
        {
        }

        public SqLiteHelper(ConfigHelper configHelper) : base(configHelper)
        {
            
        }

        public bool NonQuery(QueryBuilderBase query)
        {
            LogAndException.WriteToLogger("ExecuteReaderByParamer", ErrorLevel.Debug);

            var connection = new SqliteConnection(Connection);
            var command = connection.CreateCommand();
            command.CommandText = query.GetQuery();

            try
            {
                foreach (var parameter in query.Parameters)
                    command.Parameters.AddWithValue($"@{parameter.Key}", parameter.Value ?? DBNull.Value);

                command.Connection.Open();
                command.ExecuteNonQuery();

                if (!IsItFromReplication && CheckIfReplicationTableEntryIsRequired(query))
                    ExecuteNonQuery(SetDbCommand(query));
                return true;
            }
            catch (SqliteException ex)
            {
                LogAndException.WriteToLogger(ex.Message, ErrorLevel.Error);
                throw;
            }
            catch (Exception ex)
            {
                LogAndException.WriteToLogger(ex.Message, ErrorLevel.Error);
                throw;
            }
            finally
            {
                command.Connection.Close();
                command.Dispose();
            }
        }

        public DbDataReader Query(QueryBuilderBase query)
        {
            LogAndException.WriteToLogger("ExecuteReaderByParamer", ErrorLevel.Debug);
            var connection = new SqliteConnection(Connection);
            var command = connection.CreateCommand();
            command.CommandText = query.GetQuery();

            try
            {
                foreach (var parameter in query.Parameters)
                    command.Parameters.AddWithValue($"@{parameter.Key}", parameter.Value ?? DBNull.Value);
                command.Connection.Open();
                return command.ExecuteReader(CommandBehavior.Default);

            }
            catch (SqliteException ex)
            {
                LogAndException.WriteToLogger(ex.Message, ErrorLevel.Error);
                throw;
            }
            catch (Exception ex)
            {
                LogAndException.WriteToLogger(ex.Message, ErrorLevel.Error);
                throw;
            }
        }

        public object SingleValue(QueryBuilderBase query)
        {
            LogAndException.WriteToLogger("ExecuteReaderByParamer", ErrorLevel.Debug);
            var connection = new SqliteConnection(Connection);
            var command = connection.CreateCommand();
            command.CommandText = query.GetQuery();

            try
            {
                foreach (var parameter in query.Parameters)
                    command.Parameters.AddWithValue($"@{parameter.Key}", parameter.Value ?? DBNull.Value);
                command.Connection.Open();
                return command.ExecuteScalar();
            }
            catch (SqliteException ex)
            {
                LogAndException.WriteToLogger(ex.Message, ErrorLevel.Error);
                throw;
            }
            catch (Exception ex)
            {
                LogAndException.WriteToLogger(ex.Message, ErrorLevel.Error);
                throw;
            }
            finally
            {
                command.Connection.Close();
                command.Dispose();
            }
        }

        private void PopulateCommandParameters(SqliteCommand command, QueryBuilderBase query)
        {
            command.Parameters.AddWithValue("@Id", query.Parameters["Id"]);
            command.Parameters.AddWithValue("@AccountId", query.Parameters["AccountId"]);
            command.Parameters.AddWithValue("@InstanceId", query.Parameters["InstanceId"]);
            command.Parameters.AddWithValue("@ClientId", Constant.ClientId);
            command.Parameters.AddWithValue("@TableName", query.Table.TableName);
            command.Parameters.AddWithValue("@Action", 1);
            command.Parameters.AddWithValue("@ActionTime", CustomDateTime.IST);
            command.Parameters.AddWithValue("@CreatedAt", query.Parameters["CreatedAt"]);
            command.Parameters.AddWithValue("@UpdatedAt", query.Parameters["UpdatedAt"]);
            command.Parameters.AddWithValue("@CreatedBy", query.Parameters["CreatedBy"]);
            command.Parameters.AddWithValue("@UpdatedBy", query.Parameters["UpdatedBy"]);
            command.Parameters.AddWithValue("@TransactionID", DBNull.Value);
        }

        public override DbCommand SetDbCommand(QueryBuilderBase query)
        {
            var queryString = "INSERT INTO REPLICATIONDATA (Id,AccountId,InstanceId,ClientId,TableName,Action,ActionTime,CreatedAt,UpdatedAt,CreatedBy,UpdatedBy,TransactionID)"
                                + "VALUES(@Id,@AccountId,@InstanceId,@ClientId,@TableName,@Action,@ActionTime,@CreatedAt,@UpdatedAt,@CreatedBy,@UpdatedBy,@TransactionID)";

            var connection = new SqliteConnection(Connection);
            var command = connection.CreateCommand();
            command.CommandText = queryString;

            PopulateCommandParameters(command, query);
            return command;
        }

        public Task<bool> NonQueryAsyc(QueryBuilderBase query)
        {
            return Task.Run(() => NonQuery(query));
        }

        public Task<DbDataReader> QueryAsyc(QueryBuilderBase query)
        {
            return Task.Run(() => Query(query));
        }

        public Task<object> SingleValueAsyc(QueryBuilderBase query)
        {
            return Task.Run(() => SingleValue(query));
        }

        public Task<bool> ExecuteNonQueryAsyc(IEnumerable<QueryBuilderBase> queries)
        {
            LogAndException.WriteToLogger("ExecuteNonQueryAsync", ErrorLevel.Debug);
            var connection = new SqliteConnection(Connection);
            connection.Open();
            var transaction = connection.BeginTransaction();
            try
            {
                foreach (var query in queries)
                {
                    if (query == null) continue;

                    var command = new SqliteCommand();
                    command.Connection = connection;
                    command.Transaction = transaction;
                    command.CommandType = CommandType.Text;
                    command.CommandText = query.GetQuery();

                    foreach (var parameter in query.Parameters)
                        command.Parameters.AddWithValue($"@{parameter.Key}", parameter.Value ?? DBNull.Value);

                    command.ExecuteNonQuery();
                    if (!IsItFromReplication && CheckIfReplicationTableEntryIsRequired(query))
                        ExecuteNonQuery(SetDbCommand(query));
                }
                transaction.Commit();

                return Task.FromResult(true);
            }
            catch (SqliteException ex)
            {
                LogAndException.WriteToLogger(ex.Message, ErrorLevel.Error);
                transaction.Rollback();
                throw;
            }
            catch (Exception ex)
            {
                LogAndException.WriteToLogger(ex.Message, ErrorLevel.Error);
                transaction.Rollback();
                throw;
            }
            finally
            {
                connection.Close();
                connection.Dispose();
                transaction.Dispose();
            }
        }

        Task<DbDataReader> ISqlHelper.QueryAsyc(QueryBuilderBase query)
        {
            throw new NotImplementedException();
        }

        Task<bool> ISqlHelper.ExecuteNonQueryAsyc(IEnumerable<QueryBuilderBase> queries)
        {
            throw new NotImplementedException();
        }

        Task<bool> ISqlHelper.NonQueryAsyc(QueryBuilderBase query)
        {
            throw new NotImplementedException();
        }

        Task<DbDataReader> ISqlHelper.ExecuteProcAsyc(QueryBuilderBase query)
        {
            throw new NotImplementedException();
        }

        Task<object> ISqlHelper.SingleValueAsyc(QueryBuilderBase query)
        {
            throw new NotImplementedException();
        }

        public Task<bool> NonQueryAsyc2(QueryBuilderBase query)
        {
            throw new NotImplementedException();
        }

        public Task<bool> NonQueryAsyc(QueryBuilderBase query, SqlConnection con, SqlTransaction transaction)
        {
            throw new NotImplementedException();
        }
        public Task<bool> NonQueryAsyc3(QueryBuilderBase query)
        {
            throw new NotImplementedException();
        }

        public SqlConnection OpenConnection()
        {
            throw new NotImplementedException();
        }

        public bool CloseConnection(SqlConnection con)
        {
            throw new NotImplementedException();
        }

        public Task<object> SingleValueAsyc(QueryBuilderBase query, SqlConnection con, SqlTransaction transaction)
        {
            throw new NotImplementedException();
        }

        public Task<DbDataReader> QueryAsyc(QueryBuilderBase query, SqlConnection con, SqlTransaction transaction)
        {
            throw new NotImplementedException();
        }

        //public Task<bool> NonQueryAsyc(QueryBuilderBase query, SqlConnection con, SqlTransaction transaction)
        //{
        //    throw new NotImplementedException();
        //}

        //SqlConnection ISqlHelper.OpenConnection()
        //{
        //    throw new NotImplementedException();
        //}

        //public bool CloseConnection(SqlConnection con)
        //{
        //    throw new NotImplementedException();
        //}
    }
}
