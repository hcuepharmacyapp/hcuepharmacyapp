﻿using DataAccess.QueryBuilder;
using System;
using System.Data.Common;
using System.Data.SqlClient;
using Utilities.Helpers;
using Utilities.Logger;

namespace DataAccess.ManageData
{
    public abstract class BaseHelper
    {
        protected readonly string Connection;
        protected readonly string LogConnection;
        protected BaseHelper(string connection)
        {
            Connection = connection;
        }

        protected BaseHelper(ConfigHelper configHelper)
        {
            Connection = configHelper.AppConfig.ConnectionString;
            LogConnection = configHelper.AppConfig.LogConnectionString;

        }

        public abstract DbCommand SetDbCommand(QueryBuilderBase query);

        protected bool ExecuteNonQuery(DbCommand dbCommand)
        {
            LogAndException.WriteToLogger("ExecuteReaderByParamer", ErrorLevel.Debug);
            try
            {
                dbCommand.Connection.Open();
                dbCommand.ExecuteNonQuery();
                return true;
            }
            catch (SqlException ex)
            {
                LogAndException.WriteToLogger(ex.Message, ErrorLevel.Error);
                throw;
            }
            catch (Exception ex)
            {
                LogAndException.WriteToLogger(ex.Message, ErrorLevel.Error);
                throw;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        protected bool CheckIfReplicationTableEntryIsRequired(QueryBuilderBase query)
        {
            return (query.OperationType == OperationType.Insert && IsReplicationEntryNeededForThisTable(query));
        }

        private bool IsReplicationEntryNeededForThisTable(QueryBuilderBase query)
        {
            var tableName = query.Table.TableName;

            if (tableName == "HQueUser")
            {
                if (query.Parameters["InstanceId"] == null)
                    return false;
            }


            return (tableName != "Account" && tableName != "Doctor"
                && tableName != "Instance" && tableName != "InstanceClient"
                && tableName != "Patient" && tableName != "ReplicationData");
        }
    }
}
