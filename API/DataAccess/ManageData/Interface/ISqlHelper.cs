﻿using System.Threading.Tasks;
using DataAccess.QueryBuilder;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.SqlClient;

namespace DataAccess.ManageData.Interface
{
    public delegate void DbOperationsEventHandler(QueryBuilderBase status);

    public interface ISqlHelper
    {
        DbOperationsEventHandler OnSyncQueueOperation { get; set; }
        //bool NonQuery(QueryBuilderBase query);
        //DbDataReader Query(QueryBuilderBase query);
        //object SingleValue(QueryBuilderBase query);
        Task<bool> ExecuteNonQueryAsyc(IEnumerable<QueryBuilderBase> queries); 
        Task<bool> NonQueryAsyc(QueryBuilderBase query);
        Task<bool> NonQueryAsyc(QueryBuilderBase query, SqlConnection con, SqlTransaction transaction);
        Task<DbDataReader> QueryAsyc(QueryBuilderBase query);
        Task<DbDataReader> QueryAsyc(QueryBuilderBase query, SqlConnection con, SqlTransaction transaction);
        Task<DbDataReader> ExecuteProcAsyc(QueryBuilderBase query);
        Task<object> SingleValueAsyc(QueryBuilderBase query);
        Task<object> SingleValueAsyc(QueryBuilderBase query, SqlConnection con, SqlTransaction transaction);
        bool IsItFromReplication { get; set; }

        Task<bool> NonQueryAsyc2(QueryBuilderBase query);
        SqlConnection OpenConnection();
        bool CloseConnection(SqlConnection con);

        Task<bool> NonQueryAsyc3(QueryBuilderBase query);
      
    }
}