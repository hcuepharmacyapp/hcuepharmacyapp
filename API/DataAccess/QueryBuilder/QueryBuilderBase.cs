﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataAccess.Criteria.Interface;
using Utilities.Helpers;
using System.Data;

namespace DataAccess.QueryBuilder
{
    public abstract class QueryBuilderBase
    {
        private string _query;
        private IDbColumn[] _dbColumnName;

        public OperationType OperationType { get; private set; }
        public CommandType CommandType { get; private set; }
        public string ProcedureName { get; private set; }
        public bool ExecuteCommand { get; private set; } = true;

        public IDbTable Table { get; private set; }

        public static Func<IDbColumn, bool> UpdateSkipColumn = column =>
        column.ColumnName != "Id" &&
        column.ColumnName != "CreatedBy" &&
                             column.ColumnName != "CreatedAt" &&
                             column.ColumnName != "AccountId" &&
                             column.ColumnName != "InstanceId";


        protected QueryBuilderBase(SelectBuilder selectBuilder, ConfigHelper configHelper)
        {
            SelectBuilder = selectBuilder;
            Parameters = new Dictionary<string, object>();
            JoinBuilder = new JoinBuilder();
            ConditionBuilder = new ConditionBuilder(configHelper, Parameters);
        }

        public void SetDbTable(IDbTable table, OperationType type)
        {
            Table = table;
            OperationType = type;
        }

        public void SetQuery(string query)
        {
            _query = query;
            OperationType = OperationType.Misc;
        }

        public void SetCommandType(CommandType commandType)
        {
            CommandType = commandType;
        }

        public void SetExecuteCommand(bool executeCommand)
        {
            ExecuteCommand = executeCommand;
        }
        public bool GetExecuteCommand(bool executeCommand)
        {
            return ExecuteCommand;
        }

        public void SetProcName(string procedureName)
        {
            ProcedureName = procedureName;
        }

        public IDictionary<string, object> Parameters { get; }

        public ConditionBuilder ConditionBuilder { get; }

        public JoinBuilder JoinBuilder { get; }

        public SelectBuilder SelectBuilder { get; }

        public QueryBuilderBase AddColumn(params IDbColumn[] dbColumnName)
        {
            _dbColumnName = dbColumnName;
            return this;
        }

        public string GetQuery()
        {
            switch (OperationType)
            {
                case OperationType.Delete:
                    return Delete();
                case OperationType.Insert:
                    return Insert();
                case OperationType.Update:
                    return Update();
                case OperationType.Select:
                    return Select();
                case OperationType.Count:
                    return Count();
                case OperationType.Merge:
                    return Merge();
                default:
                    return _query;
            }
        }

        private string Merge()
        {
            string query = $@"MERGE INTO {Table.TableName} USING (SELECT {PrepareMergeColumnNames(Table.ColumnList)} ) AS SourceSet ON {Table.TableName}.Id = SourceSet.Id WHEN NOT MATCHED THEN INSERT({GetInsertColumnNames()}) VALUES({PrepareMergeInsertColumnNames(Table.ColumnList)}) WHEN MATCHED THEN UPDATE SET {PrepareMergeUpdateColumnNames(Table.ColumnList)}";
            return query;
        }

        private string Count()
        {
            var select = $"SELECT COUNT({GetDistinct()}{GetCountColumnNames()}) FROM {Table.TableName} (nolock) ";
            if (!string.IsNullOrEmpty(JoinBuilder.GetJoin()))
                select = $"{select} {JoinBuilder.GetJoin()}";
            if (!string.IsNullOrEmpty(ConditionBuilder.GetWhere()))
                select = $"{select} WHERE {ConditionBuilder.GetWhere()}";
            if (!string.IsNullOrEmpty(SelectBuilder.GetGroupBy()))
                select = $"{select} GROUP BY {SelectBuilder.GetGroupBy()}";
            if (!string.IsNullOrEmpty(ConditionBuilder.GetHaving()))
                select = $"{select} HAVING {ConditionBuilder.GetHaving()}";
            return $"{select}{SelectBuilder.GetQuery()}";
        }        

        private string Select()
        {
            var select = $"SELECT {GetDistinct()}{SelectBuilder.GetTop()}{GetColumnNames()} FROM {Table.TableName}  (nolock) ";
            if (!string.IsNullOrEmpty(JoinBuilder.GetJoin()))
                select = $"{select} {JoinBuilder.GetJoin()}";
            if (!string.IsNullOrEmpty(ConditionBuilder.GetWhere()))
                select = $"{select} WHERE {ConditionBuilder.GetWhere()}";
            if (!string.IsNullOrEmpty(SelectBuilder.GetGroupBy()))
                select = $"{select} GROUP BY {SelectBuilder.GetGroupBy()}";
            if (!string.IsNullOrEmpty(ConditionBuilder.GetHaving()))
                select = $"{select} HAVING {ConditionBuilder.GetHaving()}";
            return $"{select}{SelectBuilder.GetQuery()}";
        }

        private string GetDistinct()
        {
            return SelectBuilder.MakeDistinct ? " DISTINCT " : string.Empty;
        }

        private string Delete()
        {
            var delete = $"DELETE FROM {Table.TableName}";
            if (ConditionBuilder.GetWhere() != null)
                delete = $"{delete} WHERE {ConditionBuilder.GetWhere()}";
            return delete;
        }

        private string Insert()
        {
            return $"INSERT INTO {Table.TableName} ({GetInsertColumnNames()})" +
                   $"VALUES({GetInsertColumnParameterNames()})";
        }

        private string Update()
        {
            var update = $"UPDATE {Table.TableName} SET {GetUpdateColumn()}";
            if (ConditionBuilder.GetWhere() != null)
                update = $"{update} WHERE {ConditionBuilder.GetWhere()}";
            return update;
        }

        private string GetUpdateColumn()
        {
            if (_dbColumnName == null || _dbColumnName.Length == 0)
                return PrepareUpdateColumnNames(Table.ColumnList);

            return PrepareUpdateColumnNames(_dbColumnName);
        }

        private string PrepareUpdateColumnNames(IEnumerable<IDbColumn> columnNames)
        {
            var columnName = new StringBuilder();

            foreach (var column in columnNames.Where(UpdateSkipColumn))
            {
                columnName.Append($",{column.ColumnName} = {column.ColumnVariable}");
            }

            return columnName.ToString().Substring(1);
        }

        private string PrepareMergeUpdateColumnNames(IEnumerable<IDbColumn> columnNames)
        {
            var columnName = new StringBuilder();

            foreach (var column in columnNames.Where(UpdateSkipColumn))
            {
                columnName.Append($",{column.ColumnName} = SourceSet.{column.ColumnName}");
            }

            return columnName.ToString().Substring(1);
        }

        private string PrepareMergeInsertColumnNames(IEnumerable<IDbColumn> columnNames)
        {
            var columnName = new StringBuilder();

            foreach (var column in columnNames)
            {
                columnName.Append($",SourceSet.{column.ColumnName}");
            }

            return columnName.ToString().Substring(1);
        }

        private string PrepareMergeColumnNames(IEnumerable<IDbColumn> columnNames)
        {
            var columnName = new StringBuilder();

            foreach (var column in columnNames)
            {
                columnName.Append($",@{column.ColumnName} AS {column.ColumnName}");
            }

            return columnName.ToString().Substring(1);
        }

        private string GetInsertColumnNames()
        {
            if (_dbColumnName == null || _dbColumnName.Length == 0)
                return GetColumnNames(Table.ColumnList);

            return GetColumnNames(_dbColumnName);
        }

        private string GetInsertColumnParameterNames()
        {
            if (_dbColumnName == null || _dbColumnName.Length == 0)
                return GetColumnParameterNames(Table.ColumnList);

            return GetColumnParameterNames(_dbColumnName);
        }

        private string GetColumnNames(IEnumerable<IDbColumn> columnNames)
        {
            var columnName = new StringBuilder();

            foreach (var column in columnNames)
            {
                columnName.Append($",{column.ColumnName}");
            }

            return columnName.ToString().Substring(1);
        }

        private string GetColumnParameterNames(IEnumerable<IDbColumn> columnNames)
        {
            var columnName = new StringBuilder();

            foreach (var column in columnNames)
            {
                columnName.Append($",{column.ColumnVariable}");
            }

            return columnName.ToString().Substring(1);
        }

        private string GetColumnNames()
        {
            if ((_dbColumnName == null || _dbColumnName.Length == 0) && string.IsNullOrEmpty(JoinBuilder.GetJoinColumn()))
                return $"{GetColumnFullNames(Table.ColumnList)}";

            if (string.IsNullOrEmpty(GetColumnFullNames(_dbColumnName)))
                return JoinBuilder.GetJoinColumn();

            if (string.IsNullOrEmpty(JoinBuilder.GetJoinColumn()))
                return GetColumnFullNames(_dbColumnName);

            return $"{GetColumnFullNames(_dbColumnName)},{JoinBuilder.GetJoinColumn()}";
        }

        private string GetCountColumnNames()
        {
            if ((_dbColumnName == null || _dbColumnName.Length == 0))
            {
                return $"*";
            }
            else
            {
                return $"{_dbColumnName.First()}";
            }
        }

        private string GetColumnFullNames(IEnumerable<IDbColumn> columnNames)
        {
            if (columnNames == null)
                return string.Empty;

            if (!columnNames.Any())
                return string.Empty;

            var columnName = new StringBuilder();

            foreach (var column in columnNames)
            {
                columnName.Append($",{column.FullColumnName}");
            }

            return columnName.ToString().Substring(1);
        }
    }

    public enum OperationType
    {
        Insert = 1,
        Update = 2,
        Delete = 3,
        Select = 4,
        Count = 5,
        Misc = 6,
        SyncOnlineDatabaseToClient = 7,
        Merge = 8
    }
}
