﻿using System.Collections.Generic;
using DataAccess.Criteria;
using DataAccess.Criteria.Interface;
using Utilities.Enum;
using Utilities.Helpers;

namespace DataAccess.QueryBuilder
{
    public class ConditionBuilder
    {

        private readonly InternalConditionBuilder _where;
        private readonly InternalConditionBuilder _having;

        public ConditionBuilder(ConfigHelper configHelper, IDictionary<string, object> parameters = null)
        {
            _where = new InternalConditionBuilder(configHelper, parameters);
            _having = new InternalConditionBuilder(configHelper, parameters);
        }

        #region where

        public ConditionBuilder And(ICustomCriteria customCriteria)
        {
            _where.And(customCriteria);
            return this;
        }

        public ConditionBuilder And(IDbColumn columnName, object parameter,
            CriteriaEquation equation = CriteriaEquation.Equal,
            CriteriaLike cLike = CriteriaLike.Both)
        {
            _where.And(columnName, parameter, equation, cLike);
            return this;
        }

        public ConditionBuilder And(IDbColumn columnName,
            CriteriaEquation equation = CriteriaEquation.Equal,
            CriteriaLike cLike = CriteriaLike.Both)
        {
            _where.And(columnName, equation, cLike);
            return this;
        }

        public ConditionBuilder Or(ICustomCriteria customCriteria)
        {
            _where.Or(customCriteria);

            return this;
        }

        public ConditionBuilder Or(IDbColumn columnName, object parameter,
            CriteriaEquation equation = CriteriaEquation.Equal,
            CriteriaLike cLike = CriteriaLike.Both)
        {
            _where.Or(columnName, parameter, equation, cLike);

            return this;
        }

        public ConditionBuilder Or(IDbColumn columnName,
            CriteriaEquation equation = CriteriaEquation.Equal,
            CriteriaLike cLike = CriteriaLike.Both)
        {
            _where.Or(columnName, equation, cLike);
            return this;
        }

        public string GetWhere()
        {
            return _where.GetQuery();
        }

        #endregion where

        #region having

        public string GetHaving()
        {
            return _having.GetQuery();
        }

        public ConditionBuilder HavingAnd(ICustomCriteria customCriteria)
        {
            _having.And(customCriteria);
            return this;
        }

        public ConditionBuilder HavingAnd(IDbColumn columnName, object parameter,
            CriteriaEquation equation = CriteriaEquation.Equal,
            CriteriaLike cLike = CriteriaLike.Both)
        {
            _having.And(columnName, parameter, equation, cLike);
            return this;
        }

        public ConditionBuilder HavingAnd(IDbColumn columnName,
            CriteriaEquation equation = CriteriaEquation.Equal,
            CriteriaLike cLike = CriteriaLike.Both)
        {
            _having.And(columnName, equation, cLike);
            return this;
        }

        public ConditionBuilder HavingOr(ICustomCriteria customCriteria)
        {
            _having.Or(customCriteria);
            return this;
        }

        public ConditionBuilder HavingOr(IDbColumn columnName, object parameter,
            CriteriaEquation equation = CriteriaEquation.Equal,
            CriteriaLike cLike = CriteriaLike.Both)
        {
            _having.Or(columnName, parameter, equation, cLike);
            return this;
        }

        public ConditionBuilder HavingOr(IDbColumn columnName,
            CriteriaEquation equation = CriteriaEquation.Equal,
            CriteriaLike cLike = CriteriaLike.Both)
        {
             _having.Or(columnName, equation, cLike);
            return this;
        }

        #endregion having

        private class InternalConditionBuilder
        {
            private readonly IDictionary<string, object> _parameters;

            public InternalConditionBuilder(ConfigHelper configHelper,IDictionary<string, object> parameters = null)
            {
                _configHelper = configHelper;
                _parameters = parameters;
            }

            private string _customCriteria;
            private ConfigHelper _configHelper;

            public void And(ICustomCriteria customCriteria)
            {
                _customCriteria = string.IsNullOrEmpty(_customCriteria)
                    ? customCriteria.GetCustomCriteria()
                    : $"{_customCriteria} AND {customCriteria.GetCustomCriteria()}";
            }

            public void And(IDbColumn columnName, object parameter,
                CriteriaEquation equation = CriteriaEquation.Equal,
                CriteriaLike cLike = CriteriaLike.Both)
            {
                var cc = new CriteriaColumn(columnName, _configHelper , equation, cLike);

                _customCriteria = string.IsNullOrEmpty(_customCriteria)
                    ? cc.GetQuery()
                    : $"{_customCriteria} AND {cc.GetQuery()}";

                _parameters.Add(columnName.ColumnName, parameter);
            }

            public void And(IDbColumn columnName,
                CriteriaEquation equation = CriteriaEquation.Equal,
                CriteriaLike cLike = CriteriaLike.Both)
            {
                var cc = new CriteriaColumn(columnName, _configHelper , equation, cLike);

                _customCriteria = string.IsNullOrEmpty(_customCriteria)
                    ? cc.GetQuery()
                    : $"{_customCriteria} AND {cc.GetQuery()}";
            }

            public void Or(ICustomCriteria customCriteria)
            {
                _customCriteria = string.IsNullOrEmpty(_customCriteria)
                    ? customCriteria.GetCustomCriteria()
                    : $"{_customCriteria} OR {customCriteria.GetCustomCriteria()}";
            }

            public void Or(IDbColumn columnName, object parameter,
                CriteriaEquation equation = CriteriaEquation.Equal,
                CriteriaLike cLike = CriteriaLike.Both)
            {
                var cc = new CriteriaColumn(columnName,_configHelper , equation, cLike);

                _customCriteria = string.IsNullOrEmpty(_customCriteria)
                    ? cc.GetQuery()
                    : $"{_customCriteria} OR {cc.GetQuery()}";

                if (parameter != null)
                    _parameters.Add(columnName.ColumnName, parameter);

            }

            public void Or(IDbColumn columnName,
                CriteriaEquation equation = CriteriaEquation.Equal,
                CriteriaLike cLike = CriteriaLike.Both)
            {
                Or(columnName, null, equation, cLike);
            }

            public string GetQuery()
            {
                return _customCriteria;
            }
        }
    }
}
