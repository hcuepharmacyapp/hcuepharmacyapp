﻿using System;
using DataAccess.Criteria.Interface;
using Microsoft.AspNetCore.Http;

namespace DataAccess.QueryBuilder
{
    public class QueryBuilderFactory
    {
        private readonly IServiceProvider _serviceProvider;
        private readonly IHttpContextAccessor _httpContextAccessor;

        public QueryBuilderFactory(IServiceProvider serviceProvider, IHttpContextAccessor httpContextAccessor)
        {
            _serviceProvider = serviceProvider;
            _httpContextAccessor = httpContextAccessor;
        }

        public QueryBuilderBase GetQueryBuilder(IDbTable table, OperationType type)
        {
            var qb = (QueryBuilderBase)_serviceProvider.GetService(typeof(QueryBuilderBase));
            qb.SetDbTable(table,type);
            return qb;
        }


        public QueryBuilderBase GetQueryBuilder(string query)
        {
            var qb = (QueryBuilderBase)_serviceProvider.GetService(typeof(QueryBuilderBase));
            qb.SetQuery(query);
            return qb;
        }

        public IHttpContextAccessor GetHttpContext()
        {
            return _httpContextAccessor;
        }


    }
}
