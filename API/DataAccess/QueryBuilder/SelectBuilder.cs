using DataAccess.Criteria.Interface;
using System.Text;

namespace DataAccess.QueryBuilder
{
    public abstract class SelectBuilder
    {
        protected int PageNo;
        protected int PageSize;
        private IDbColumn[] _sortColumnNames;
        private IDbColumn[] _groupByColumnNames;
        private bool _isAsc;
        protected int? Top;
        protected SelectBuilder()
        {
            PageNo = 0;
            PageSize = 0;
            MakeDistinct = false;
            _isAsc = false;
        }

        protected int GetOffset()
        {
            return (PageSize * PageNo) - PageSize;
        }

        public bool MakeDistinct { get; set; }

        public void SetPage(int pageNo, int pageSize)
        {
            PageNo = pageNo;
            PageSize = pageSize;
        }

        public void SortBy(params IDbColumn[] sortColumnNames )
        {
            _isAsc = true;
            _sortColumnNames = sortColumnNames;
        }

        public void SortByDesc(params IDbColumn[] sortColumnNames)
        {
            _isAsc = false;
            _sortColumnNames = sortColumnNames;
        }

        public SelectBuilder GroupBy(params IDbColumn[] groupByColumnNames)
        {
            _groupByColumnNames = groupByColumnNames;
            return this;
        }

        protected string GetSortBy()
        {
            if (_sortColumnNames == null) return "1";
            var columnName = new StringBuilder();

            foreach (var column in _sortColumnNames)
            {
                columnName.Append($",{column.FullColumnName}");
            }
            var orderBy = _isAsc ? "asc" : "desc";
            return $"{columnName.ToString().Substring(1)} {orderBy}";
        }

        public SelectBuilder SetTop(int top)
        {
            Top = top;
            return this;
        }

        public abstract string GetTop();

        public abstract string GetQuery();

        public string GetGroupBy()
        {
            if (_groupByColumnNames == null) return "";

            var columnName = new StringBuilder();

            foreach (var column in _groupByColumnNames)
            {
                columnName.Append($",{column.FullColumnName}");
            }
            return columnName.ToString().Substring(1);
        }
    }
}