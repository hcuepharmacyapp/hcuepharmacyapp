﻿using Utilities.Helpers;

namespace DataAccess.QueryBuilder.SqlServer
{
    public class SqlQueryBuilder : QueryBuilderBase
    {
        public SqlQueryBuilder(SelectBuilder selectBuilder, ConfigHelper configHelper) : base(selectBuilder, configHelper)
        {
        }
    }
}
