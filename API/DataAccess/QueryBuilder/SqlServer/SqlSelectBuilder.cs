namespace DataAccess.QueryBuilder.SqlServer
{
    public class SqlSelectBuilder : SelectBuilder
    {
        public override string GetQuery()
        {
            return (PageNo == 0 || PageSize == 0)
                ? $" ORDER BY {GetSortBy()} "
                : $" ORDER BY {GetSortBy()} OFFSET {GetOffset()} ROWS FETCH NEXT {PageSize} ROWS ONLY ";
        }

        public override string GetTop()
        {
            return Top.HasValue ? $" TOP {Top.Value} " : "";
        }
    }
}