﻿using System.Collections.Generic;
using System.Text;
using DataAccess.Criteria.Interface;

namespace DataAccess.QueryBuilder
{
    public class JoinBuilder
    {
        private string _join = string.Empty;
        private readonly IList<JoinColumn> _joinColumns;


        public JoinBuilder()
        {
            _joinColumns = new List<JoinColumn>();
        }

        public JoinBuilder Join(IDbTable tableName,IDbColumn column1,IDbColumn column2 )
        {
            _join += GetJoin(tableName, column1, column2, "INNER");

            return this;
        }

        private static string GetJoin(IDbTable tableName, IDbColumn column1, IDbColumn column2, string join)
        {
            return $"{join} JOIN {tableName.TableName} (nolock) ON {column1.FullColumnName} = {column2.FullColumnName} ";
        }


        public JoinBuilder ORJoin(IDbTable tableName, IDbColumn column1, IDbColumn column2, IDbColumn column3, IDbColumn column4)
        {
            _join += GetOrJoin(tableName, column1, column2, column3, column4, "INNER");

            return this;
        }

        private static string GetOrJoin(IDbTable tableName, IDbColumn column1, IDbColumn column2, IDbColumn column3, IDbColumn column4, string join)
        {
            return $"{join} JOIN {tableName.TableName} (nolock)  ON ({column1.FullColumnName} = {column2.FullColumnName} OR {column3.FullColumnName} = {column4.FullColumnName}) ";
        }

        public JoinBuilder LeftJoin(IDbTable tableName, IDbColumn column1, IDbColumn column2)
        {
            _join += GetJoin(tableName, column1, column2, "LEFT");

            return this;
        }

        private static string GetJoins(IDbTable tableName, IDbColumn column1, IDbColumn column2, IDbColumn column3, IDbColumn column4, string join)
        {
            return $"{join} JOIN {tableName.TableName} (nolock)  ON {column1.FullColumnName} = {column2.FullColumnName} And {column3.FullColumnName} = {column4.FullColumnName} ";
        }

        public JoinBuilder LeftJoins(IDbTable tableName, IDbColumn column1, IDbColumn column2, IDbColumn column3, IDbColumn column4)
        {
            _join += GetJoins(tableName, column1, column2, column3, column4, "LEFT");

            return this;
        }



        private static string GetJoins2(IDbTable tableName, IDbColumn column1, IDbColumn column2, IDbColumn column3, string column4Value, string join)
        {
            return $"{join} JOIN {tableName.TableName} (nolock)  ON {column1.FullColumnName} = {column2.FullColumnName} And {column3.FullColumnName} = '{column4Value} '";
        }

        public JoinBuilder LeftJoins2(IDbTable tableName, IDbColumn column1, IDbColumn column2, IDbColumn column3, string column4Value)
        {
            _join += GetJoins2(tableName, column1, column2, column3, column4Value, "LEFT");

            return this;
        }

        public string GetJoin()
        {
            return _join;
        }

        public void AddJoinColumn(IDbTable tableName,params IDbColumn[] dbColumnName)
        {
            _joinColumns.Add(new JoinColumn { DbTableName = tableName , DbColumnName = dbColumnName});
        }

        public string GetJoinColumn()
        {
            if (_joinColumns.Count == 0)
                return string.Empty;

            var columnName = new StringBuilder();

            foreach (var joinColumn in _joinColumns)
            {
                foreach (var column in joinColumn.DbColumnName)
                {
                    columnName.Append(
                        $",{column.FullColumnName} AS [{joinColumn.DbTableName.TableName}.{column.ColumnName}]");
                }
            }

            return columnName.ToString().Substring(1);
        }

        private class JoinColumn
        {
            public IDbTable DbTableName { get; set; }

            public IDbColumn[] DbColumnName { get; set; }
        }
    }
}
