﻿namespace DataAccess.QueryBuilder.SqLite
{
    public class SqliteSelectBuilder : SelectBuilder
    {
        public override string GetQuery()
        {
            if (Top.HasValue)
                return $" LIMIT {Top.Value} ";

            return (PageNo == 0 || PageSize == 0)
                ? $" ORDER BY {GetSortBy()} "
                : $" ORDER BY {GetSortBy()} LIMIT {PageSize} OFFSET {GetOffset()} ";
        }

        public override string GetTop()
        {
            return string.Empty;
        }
    }
}
