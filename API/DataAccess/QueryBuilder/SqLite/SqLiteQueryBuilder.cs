﻿using Utilities.Helpers;

namespace DataAccess.QueryBuilder.SqLite
{
    public class SqLiteQueryBuilder : QueryBuilderBase
    {
        public SqLiteQueryBuilder(SelectBuilder selectBuilder,ConfigHelper configHelper) : base(selectBuilder, configHelper)
        {
        }
    }
}
