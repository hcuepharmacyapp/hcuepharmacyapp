﻿using DataAccess.Criteria.Interface;

namespace DataAccess.Criteria
{
    public class DbColumn : ICriteriaQuery, IDbColumn
    {
        private readonly string _tableName;
        private readonly string _columnAlias;
        private readonly string _columnName;

        public DbColumn(string tableName, string columnName, string columnAlias = "")
        {
            _tableName = tableName;
            _columnName = columnName;
            _columnAlias = columnAlias;
        }

        public virtual string GetQuery()
        {
            return ColumnName;
        }

     //   public string ColumnVariable => $"@{ColumnName}";

        public string ColumnVariable
        {
            get
            {
                return $"@{ColumnName}";
            }
            set
            {


            }
        }

        public string TableName => $"{_tableName}";

        public virtual string FullColumnName
            =>
                string.IsNullOrEmpty(_columnAlias)
                    ? $"{_tableName}.{ColumnName}"
                    : $"{_tableName}.{ColumnName} AS {_columnAlias}";

        public string ColumnName
            => string.IsNullOrEmpty(_columnAlias) ? $"{_columnName}" : $"{_columnName} AS {_columnAlias}";

        public override string ToString()
        {
            return ColumnName;
        }

        public static IDbColumn CountColumn(string alias = "", IDbColumn dbColumn = null)
        {
            var obj = dbColumn == null ? new CountDbColumn() : new CountDbColumn(dbColumn);
            obj.SetAlias(alias);
            return obj;
        }

        public static IDbColumn SumColumn(string alias = "",IDbColumn dbColumn = null)
        {
            var obj = new SumDbColumn(dbColumn);
            obj.SetAlias(alias);
            return obj;
        }

        public static IDbColumn MinColumn(string alias = "", IDbColumn dbColumn = null)
        {
            var obj = new MinDbColumn(dbColumn);
            obj.SetAlias(alias);
            return obj;
        }

        public static IDbColumn MaxColumn(string alias = "", IDbColumn dbColumn = null)
        {
            var obj = new MaxDbColumn(dbColumn);
            obj.SetAlias(alias);
            return obj;
        }

        public static IDbColumn CustomColumn(string column)
        {
            var obj = new CustomColumn(column);
            return obj;
        }
    }

    public abstract class AggregationDbColumn : DbColumn
    {
        protected IDbColumn DbColumn;
        private string _alias;

        protected AggregationDbColumn(string tableName, string columnName, string columnAlias = "")
            : base(tableName, columnName, columnAlias)
        {
        }

        public void SetAlias(string alias) => _alias = alias;
        public abstract override string GetQuery();

        protected string GetColumnWitAlias(string column)
        {
            return string.IsNullOrEmpty(_alias) ? column : $"{column} AS {_alias}";
        }
    }

    public class CountDbColumn : AggregationDbColumn
    {
        public CountDbColumn() : base(null, null)
        {

        }

        public CountDbColumn(IDbColumn dbColumn) : base(dbColumn.TableName, dbColumn.ColumnName)
        {
            DbColumn = dbColumn;
        }

        public override string FullColumnName => GetQuery();

        public override string GetQuery()
        {
            return DbColumn == null
                ? $" {GetColumnWitAlias("COUNT(*)")} "
                : $" {GetColumnWitAlias($"COUNT({DbColumn.FullColumnName})")} ";
        }
    }

    public class SumDbColumn : AggregationDbColumn
    {
        public SumDbColumn() : this(null)
        {

        }

        public SumDbColumn(IDbColumn dbColumn) : base(dbColumn.TableName, dbColumn.ColumnName)
        {
            DbColumn = dbColumn;
        }

        public override string FullColumnName => GetQuery();

        public override string GetQuery()
        {
            return DbColumn == null
                ? $" {GetColumnWitAlias("SUM(*)")} "
                : $" {GetColumnWitAlias($"SUM({DbColumn.FullColumnName})")} ";
        }
    }

    public class MinDbColumn : AggregationDbColumn
    {
        public MinDbColumn() : this(null)
        {

        }

        public MinDbColumn(IDbColumn dbColumn) : base(dbColumn.TableName, dbColumn.ColumnName)
        {
            DbColumn = dbColumn;
        }

        public override string FullColumnName => GetQuery();

        public override string GetQuery()
        {
            return DbColumn == null
                ? $" {GetColumnWitAlias("MIN(*)")} "
                : $" {GetColumnWitAlias($"MIN({DbColumn.FullColumnName})")} ";
        }
    }

    public class MaxDbColumn : AggregationDbColumn
    {
        public MaxDbColumn() : this(null)
        {

        }

        public MaxDbColumn(IDbColumn dbColumn) : base(dbColumn.TableName, dbColumn.ColumnName)
        {
            DbColumn = dbColumn;
        }

        public override string FullColumnName => GetQuery();

        public override string GetQuery()
        {
            return DbColumn == null
                ? $" {GetColumnWitAlias("MAX(*)")} "
                : $" {GetColumnWitAlias($"MAX({DbColumn.FullColumnName})")} ";
        }
    }

    public class CustomColumn : IDbColumn , ICriteriaQuery
    {
        private readonly string _column;

        public CustomColumn(string column)
        {
            _column = column;
        }

        public string TableName { get; }
        public string FullColumnName => _column;
        public string ColumnName { get; }
        public string ColumnVariable { get; set; }
        public string GetQuery()
        {
            return _column;
        }
    }

    public static class DbColumnHelper
    {
        public static IDbColumn For(this IDbColumn dbColumn, string aliasName)
        {
            return new DbColumn(aliasName, dbColumn.ColumnName);
        }

        public static IDbColumn Convert(this IDbColumn dbColumn, string dataType)
        {
            var column = $"CONVERT({dataType}, {dbColumn.FullColumnName})";

            return new CustomColumn(column);
        }

        public static IDbColumn IsNull(this IDbColumn dbColumn, string defaultValue)
        {
            var column = $"ISNULL({dbColumn.FullColumnName},{defaultValue})";

            return new CustomColumn(column);
        }
    }
}