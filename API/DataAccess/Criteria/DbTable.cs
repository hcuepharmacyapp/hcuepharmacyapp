﻿using System;
using System.Collections.Generic;
using DataAccess.Criteria.Interface;

namespace DataAccess.Criteria
{
    public class DbTable : IDbTable
    {
        private readonly Func<IEnumerable<IDbColumn>> _columnName;

        public DbTable(string tableName, Func<IEnumerable<IDbColumn>> columnName)
        {
            TableName = tableName;
            _columnName = columnName;
        }

        public string TableName { get; }

        public virtual IEnumerable<IDbColumn> ColumnList => _columnName.Invoke();

        public override string ToString()
        {
            return TableName;
        }
    }

    public class AliasDbClass : DbTable
    {
        private string _aliasName;
        private IDbTable _table;

        public AliasDbClass(IDbTable table, string aliasName) : base($"{table.TableName} AS {aliasName}", null)
        {
            _table = table;
            _aliasName = aliasName;
        }

        public override IEnumerable<IDbColumn> ColumnList
        {
            get
            {
                foreach (var column in _table.ColumnList)
                {
                    yield return column.For(_aliasName);
                }
            }
        }
    }

    public class JoinAliasDbClass : DbTable
    {
        private string _aliasName;
        private IDbTable _table;

        public JoinAliasDbClass(IDbTable table, string aliasName) : base(aliasName, null)
        {
            _table = table;
            _aliasName = aliasName;
        }

        public override IEnumerable<IDbColumn> ColumnList
        {
            get
            {
                foreach (var column in _table.ColumnList)
                {
                    yield return column.For(_aliasName);
                }
            }
        }

    }

    public static class IDbTableHelper
    {
        public static IDbTable As(this IDbTable table,string aliasName)
        {
            var aliasTable = new AliasDbClass(table, aliasName);
            return aliasTable;
        }

        public static IDbTable JoinAs(this IDbTable table, string aliasName)
        {
            var aliasTable = new JoinAliasDbClass(table, aliasName);
            return aliasTable;
        }
    }
     
}
