﻿using DataAccess.Criteria.Interface;
using Utilities.Enum;

namespace DataAccess.Criteria
{
    public class CustomCriteria : ICustomCriteria, ICriteriaQuery
    {
        private readonly string _query = string.Empty;

        public CustomCriteria()
        {
        }

        /// <summary>
        ///     To be used with caution, as no column names / variables are validated. This could lead to a SQL injection attack.
        /// </summary>
        /// <param name="query">The query will be passed in as a string</param>
        public CustomCriteria(string query)
        {
            _query = query;
        }

        public CustomCriteria(ICriteriaColumn column1)
        {
            _query = $"({column1.GetCriteriaColumn()})";
        }

        public CustomCriteria(IDbColumn column1, CriteriaCondition condition)
        {
            switch (condition)
            {
                case CriteriaCondition.IsNull:
                    _query = $"({column1.FullColumnName} IS NULL)";
                    break;
                case CriteriaCondition.IsNotNull:
                    _query = $"({column1.FullColumnName} IS NOT NULL)";
                    break;
            }
        }

        public CustomCriteria(IDbColumn column1, IDbColumn column2, CriteriaCondition condition)
            : this(new CriteriaColumn(column1), new CriteriaColumn(column2), condition)
        {

        }

        public CustomCriteria(IDbColumn column1, IDbColumn column2,string variable, CriteriaCondition condition)
            : this(new CriteriaColumn(column1, variable), new CriteriaColumn(column2, variable), condition)
        {

        }

        public CustomCriteria(ICriteriaColumn column1, ICriteriaColumn column2, CriteriaCondition condition)
        {
            _query = $"({column1.GetCriteriaColumn()} {condition} {column2.GetCriteriaColumn()})";
        }

        public CustomCriteria(ICriteriaColumn column1, ICustomCriteria cust1, CriteriaCondition condition)
        {
            _query = $"({column1.GetCriteriaColumn()} {condition} {cust1.GetCustomCriteria()})";
        }

        public CustomCriteria(ICustomCriteria cust1, ICustomCriteria cust2, CriteriaCondition condition)
        {
            _query = $"({cust1.GetCustomCriteria()} {condition} {cust2.GetCustomCriteria()})";
        }

        public string GetQuery()
        {
            return GetCustomCriteria();
        }

        public string GetCustomCriteria()
        {
            return _query;
        }
    }
}