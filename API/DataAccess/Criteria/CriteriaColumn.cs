﻿using System;
using DataAccess.Criteria.Interface;
using Utilities.Enum;
using Utilities.Helpers;

namespace DataAccess.Criteria
{
    public class CriteriaColumn : ICriteriaColumn, ICriteriaQuery
    {
        private readonly string _query;
        private ConfigHelper _configHelper;

        public CriteriaColumn(IDbColumn column, ConfigHelper configHelper, CriteriaEquation equation = CriteriaEquation.Equal,
            CriteriaLike cLike = CriteriaLike.Both) : this(column, null, configHelper, equation, cLike)
        {

        }

        public CriteriaColumn(IDbColumn column, CriteriaEquation equation = CriteriaEquation.Equal,
            CriteriaLike cLike = CriteriaLike.Both) : this(column, null, null,equation, cLike)
        {

        }

        public CriteriaColumn(IDbColumn column, string variable, CriteriaEquation equation = CriteriaEquation.Equal,
            CriteriaLike cLike = CriteriaLike.Both) : this(column,variable,null,equation,cLike)
        {
            
        }

        public CriteriaColumn(IDbColumn column, string variable, ConfigHelper configHelper, CriteriaEquation equation = CriteriaEquation.Equal,
            CriteriaLike cLike = CriteriaLike.Both)
        {
            _configHelper = configHelper;

            _query = equation == CriteriaEquation.Between
                ? $"{column.FullColumnName} {GetCondition(equation)} {"@FilterFromDate"} AND {"@FilterToDate"}"
                : $"{column.FullColumnName} {GetVariable(variable ?? column.ColumnVariable, equation, cLike)}";
        }

        public string GetCriteriaColumn()
        {
            return _query;
        }

        public string GetQuery()
        {
            return GetCriteriaColumn();
        }

        private string GetVariable(string variable, CriteriaEquation equation, CriteriaLike cLike)
        {
            var dbVar = GetCondition(equation);

            if (equation == CriteriaEquation.Like)
            {
                if (_configHelper != null && _configHelper.AppConfig.IsSqlServer)
                {
                    switch (cLike)
                    {
                        case CriteriaLike.End:
                            dbVar = $"{dbVar} '%'+{variable}";
                            break;
                        case CriteriaLike.Begin:
                            dbVar = $"{dbVar} {variable}+'%'";
                            break;
                        case CriteriaLike.Both:
                            dbVar = $"{dbVar} '%'+{variable}+'%'";
                            break;
                    }
                }
                else
                {
                    dbVar = $"{dbVar} {variable}";
                }
            }
            else
            {
                dbVar = $"{dbVar} {variable}";
            }

            return dbVar;
        }

        private string GetCondition(CriteriaEquation equation)
        {
            string condition;
            switch (equation)
            {
                case CriteriaEquation.Equal:
                    condition = " = ";
                    break;
                case CriteriaEquation.NotEqual:
                    condition = " <> ";
                    break;
                case CriteriaEquation.Like:
                    condition = " Like ";
                    break;
                case CriteriaEquation.NotLike:
                    condition = " Not Like ";
                    break;
                case CriteriaEquation.Between:
                    condition = " Between ";
                    break;
                case CriteriaEquation.Greater:
                    condition = " > ";
                    break;
                case CriteriaEquation.GreaterEqual:
                    condition = " >= ";
                    break;
                case CriteriaEquation.Lesser:
                    condition = " < ";
                    break;
                //Added for PurchaseReturnStatusReport by Anna
                case CriteriaEquation.LesserEqual:
                    condition = " <= ";
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(equation), equation, null);
            }
            return condition;
        }
    }
}