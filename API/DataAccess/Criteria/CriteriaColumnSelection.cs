﻿using System.Text;
using DataAccess.Criteria.Interface;

namespace DataAccess.Criteria
{
    public class CriteriaColumnSelection : ICriteriaColumnSelection, ICriteriaQuery
    {
        private const string Distinct = " DISTINCT ";
        private readonly StringBuilder _column = new StringBuilder(string.Empty);
        private string _comma = string.Empty;

        public CriteriaColumnSelection(IDbColumn columnName, bool isDistinct = false)
        {
            AddColumn(columnName);
            IsDistinct = isDistinct;
        }

        public bool IsDistinct { get; set; }

        public void AddColumn(IDbColumn columnName)
        {
            _column.Append($"{_comma} {columnName.ColumnName}");
            _comma = ",";
        }

        public string ColumnSelection => IsDistinct ? $"{Distinct} {_column}" : _column.ToString();

        public string GetQuery()
        {
            return ColumnSelection;
        }

        public void MakeDistinct()
        {
            IsDistinct = true;
        }
    }
}