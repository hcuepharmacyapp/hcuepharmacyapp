﻿namespace DataAccess.Criteria.Interface
{
    public interface ICriteriaQuery
    {
        string GetQuery();
    }
}