﻿using System.Collections.Generic;

namespace DataAccess.Criteria.Interface
{
    public interface IDbTable
    {
        string TableName { get; }
        IEnumerable<IDbColumn> ColumnList { get; }
    }
}
