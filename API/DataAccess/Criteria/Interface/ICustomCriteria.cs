﻿namespace DataAccess.Criteria.Interface
{
    public interface ICustomCriteria
    {
        string GetCustomCriteria();
    }
}