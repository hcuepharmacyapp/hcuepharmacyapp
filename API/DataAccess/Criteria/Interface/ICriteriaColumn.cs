﻿namespace DataAccess.Criteria.Interface
{
    public interface ICriteriaColumn
    {
        string GetCriteriaColumn();
    }
}