﻿namespace DataAccess.Criteria.Interface
{
    public interface IDbColumn
    {
        string TableName { get; }
        string FullColumnName { get; }
        string ColumnName { get; }
        string ColumnVariable { get; set; }
    }
}