﻿namespace DataAccess.Criteria.Interface
{
    public interface ICriteriaColumnSelection
    {
        void AddColumn(IDbColumn columnName);
        string ColumnSelection { get; }
    }
}