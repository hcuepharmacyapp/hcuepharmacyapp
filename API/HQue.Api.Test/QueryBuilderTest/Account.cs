﻿using System.Collections.Generic;

namespace HQue.Api.Test.QueryBuilderTest
{
    public class Account
    {
        public static readonly IDbTable Table = new DbTable("Account", GetColumn);

        public static readonly IDbColumn IdColumn = new DbColumn("Account","Id");
        public static readonly IDbColumn CodeColumn = new DbColumn("Account","Code");
        public static readonly IDbColumn NameColumn = new DbColumn("Account","Name");
        public static readonly IDbColumn AreaColumn = new DbColumn("Account","Area");
        //public static readonly IDbColumnName CreatedAtColumn = new DbColumnName("Account","CreatedAt");
        //public static readonly IDbColumnName UpdatedAtColumn = new DbColumnName("Account","UpdatedAt");
        //public static readonly IDbColumnName CreatedByColumn = new DbColumnName("Account","CreatedBy");
        //public static readonly IDbColumnName UpdatedByColumn = new DbColumnName("Account","UpdatedBy");

        private static IEnumerable<IDbColumn> GetColumn()
        {
            yield return IdColumn;
            yield return CodeColumn;
            yield return NameColumn;
            yield return AreaColumn;
        }

    }
}
