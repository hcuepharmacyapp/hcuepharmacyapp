﻿using Utilities.Enum;

namespace HQue.Api.Test.QueryBuilderTest
{
    public class CriteriaTest
    {
        [Fact]
        public void Single_Equal()
        {
            var expected = "(Account.Id  =  @Id)";

            var cri = new CustomCriteria(new CriteriaColumn(Account.IdColumn));
            var query = cri.GetQuery();

            Assert.Equal(expected, query);
        }

        [Fact]
        public void Single_Not_Equal()
        {
            var expected = "(Account.Id  <>  @Id)";

            var cri = new CustomCriteria(new CriteriaColumn(Account.IdColumn, CriteriaEquation.NotEqual));
            var query = cri.GetQuery();

            Assert.Equal(expected, query);
        }

        [Fact]
        public void Single_Like_Both()
        {
            var expected = "(Account.Id  Like  '%'+@Id+'%')";

            var cri = new CustomCriteria(new CriteriaColumn(Account.IdColumn, CriteriaEquation.Like));
            var query = cri.GetQuery();

            Assert.Equal(expected, query);
        }
    }
}
