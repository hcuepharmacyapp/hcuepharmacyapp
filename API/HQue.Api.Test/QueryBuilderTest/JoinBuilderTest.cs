﻿namespace HQue.Api.Test.QueryBuilderTest
{
    public class JoinBuilderTest
    {
        [Fact]
        public void Join_Fluidic()
        {
            var expected = "INNER JOIN Account ON Account.Id = Account.Name ";

            var cri = new JoinBuilder();
            cri.Join(Account.Table, Account.IdColumn , Account.NameColumn);
            var query = cri.GetJoin();

            Assert.Equal(expected, query);
        }

        [Fact]
        public void Join_Column_Fluidic()
        {
            var expected = "Account.Id AS [Account.Id]";

            var cri = new JoinBuilder();
            cri.AddJoinColumn(Account.Table, Account.IdColumn);
            var query = cri.GetJoinColumn();

            Assert.Equal(expected, query);
        }
    }
}
