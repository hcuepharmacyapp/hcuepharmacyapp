﻿//using DataAccess.QueryBuilder;
//using DataAccess.QueryBuilder.SqlServer;
//using Xunit;

//namespace HQue.Api.Test.QueryBuilderTest
//{
//    public class QueryBuilderTest
//    {
//        [Fact]
//        public void And_Where_First_Column()
//        {
//            var expected = "SELECT Account.Id FROM Account WHERE Account.Id  =  @Id";

//            var cri = new SqlQueryBuilder(Account.Table,OperationType.Select);
//            cri.AddColumn(Account.IdColumn); 
//            cri.ConditionBuilder.And(Account.IdColumn);
//            var query = cri.GetQuery();

//            Assert.Equal(expected, query);
//        }


//        [Fact]
//        public void And_Where_Two_Column()
//        {
//            var expected = "SELECT Account.Id FROM Account WHERE Account.Id  =  @Id AND Account.Name  =  @Name";
            

//            var cri = new SqlQueryBuilder(Account.Table, OperationType.Select);
//            cri.AddColumn(Account.IdColumn);
//            cri.ConditionBuilder.And(Account.IdColumn);
//            cri.ConditionBuilder.And(Account.NameColumn);
//            var query = cri.GetQuery();

//            Assert.Equal(expected, query);
//        }

//        [Fact]
//        public void And_OR_Where_Two_Column()
//        {
//            var expected = "SELECT Account.Id FROM Account WHERE Account.Id  =  @Id OR Account.Name  =  @Name";

//            var cri = new SqlQueryBuilder(Account.Table, OperationType.Select);
//            cri.AddColumn(Account.IdColumn);
//            cri.ConditionBuilder.And(Account.IdColumn);
//            cri.ConditionBuilder.Or(Account.NameColumn);
//            var query = cri.GetQuery();

//            Assert.Equal(expected, query);
//        }

//        [Fact]
//        public void Or_Where_First_Column()
//        {
//            var expected = "SELECT Account.Id FROM Account WHERE Account.Id  =  @Id";

//            var cri = new SqlQueryBuilder(Account.Table, OperationType.Select);
//            cri.AddColumn(Account.IdColumn);
//            cri.ConditionBuilder.Or(Account.IdColumn);
//            var query = cri.GetQuery();

//            Assert.Equal(expected, query);
//        }
//    }
//}
