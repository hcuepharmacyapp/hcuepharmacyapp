﻿using System.Threading.Tasks;
using HQue.ApiDataAccess.External;
using HQue.Contract.Infrastructure.Setup;

namespace HQue.Api.Test.ExternalApiTest
{
    public class PharmaTest
    {
        [Fact]
        public async Task AddPharma()
        {
            var pharma = new Instance
            {
                Name = "Chantilly",
                Address = "NO 21 (114/1), SOUTH WEST CORNER PORTION, PERIYAR PATHAI, GROUND FLOOR, CHOOLAIMEDU, CHENNAI - 600094",
                Phone = "2026743309",
                TinNo = "",
                ContactName = ""
            };

            var pa = new PharmaApi(new TempConfig());
            var res = await pa.SavePharmaExternal(pharma);
        }
    }
}
