﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HQue.Contract.Infrastructure.Master;
using HQue.ApiDataAccess.External;
using Utilities.Helpers;

namespace HQue.Api.Test.ExternalApiTest
{
    public class PatientTest
    {
        [Fact]
        public async void Search()
        {
            //var pm = new PatientManager(new PatientDataAccess(null), new PatientApi());
            //var data = new Patient { Mobile = "9884201159" };
            //var result = await pm.PatientListExternal(data);

            var pa = new PatientApi(new TempConfig());

            var list = new List<Patient>
            {
                new Patient {Mobile = "22568824"},
                new Patient {Mobile = "22665522"},
                new Patient {Mobile = "25456256"},
                new Patient {Mobile = "4423611031"},
                new Patient {Mobile = "7200491456"},
                new Patient {Mobile = "7299513193"},
                new Patient {Mobile = "7358309314"},
                new Patient {Mobile = "7358522460"},
                new Patient {Mobile = "7401234237"},
                new Patient {Mobile = "7639330492"},
                new Patient {Mobile = "7708733787"},
                new Patient {Mobile = "7708733787"},
                new Patient {Mobile = "7871083093"},
                new Patient {Mobile = "8056011163"},
                new Patient {Mobile = "8056070747"},
                new Patient {Mobile = "8056078754"},
                new Patient {Mobile = "8056098934"},
                new Patient {Mobile = "8098830754"},
                new Patient {Mobile = "8344274382"},
                new Patient {Mobile = "8675636365"},
                new Patient {Mobile = "8675777862"},
                new Patient {Mobile = "8754502449"},
                new Patient {Mobile = "8760515740"},
                new Patient {Mobile = "8807576290"},
                new Patient {Mobile = "8870008433"},
                new Patient {Mobile = "8883666419"},
                new Patient {Mobile = "8883666419"},
                new Patient {Mobile = "8883666419"},
                new Patient {Mobile = "8939112802"},
                new Patient {Mobile = "9003106486"},
                new Patient {Mobile = "9003204079"},
                new Patient {Mobile = "9003980645"},
                new Patient {Mobile = "9025125195"},
                new Patient {Mobile = "9087002296"},
                new Patient {Mobile = "9094984554"},
                new Patient {Mobile = "9095498142"},
                new Patient {Mobile = "9176443121"},
                new Patient {Mobile = "9176809596"},
                new Patient {Mobile = "9344959892"},
                new Patient {Mobile = "9380094403"},
                new Patient {Mobile = "9382204466"},
                new Patient {Mobile = "9442485294"},
                new Patient {Mobile = "9442540779"},
                new Patient {Mobile = "9444311973"},
                new Patient {Mobile = "9445390047"},
                new Patient {Mobile = "9486847965"},
                new Patient {Mobile = "9500133456"},
                new Patient {Mobile = "9551066040"},
                new Patient {Mobile = "9566199713"},
                new Patient {Mobile = "9566945926"},
                new Patient {Mobile = "9658585855"},
                new Patient {Mobile = "9677189313"},
                new Patient {Mobile = "9710061204"},
                new Patient {Mobile = "9710245847"},
                new Patient {Mobile = "9710577592"},
                new Patient {Mobile = "9710679839"},
                new Patient {Mobile = "9715304865"},
                new Patient {Mobile = "9786234758"},
                new Patient {Mobile = "9786257971"},
                new Patient {Mobile = "9789036890"},
                new Patient {Mobile = "9789036891"},
                new Patient {Mobile = "9789036891"},
                new Patient {Mobile = "9790260939"},
                new Patient {Mobile = "9790878817"},
                new Patient {Mobile = "9790967553"},
                new Patient {Mobile = "9790981475"},
                new Patient {Mobile = "9791166287"},
                new Patient {Mobile = "9791840627"},
                new Patient {Mobile = "9798917355"},
                new Patient {Mobile = "9840058102"},
                new Patient {Mobile = "9840156718"},
                new Patient {Mobile = "9840403626"},
                new Patient {Mobile = "9840437948"},
                new Patient {Mobile = "9840468916"},
                new Patient {Mobile = "9840718365"},
                new Patient {Mobile = "9840766277"},
                new Patient {Mobile = "9840906923"},
                new Patient {Mobile = "9840968868"},
                new Patient {Mobile = "9840996290"},
                new Patient {Mobile = "9840996290"},
                new Patient {Mobile = "9841056051"},
                new Patient {Mobile = "9841113199"},
                new Patient {Mobile = "9841267198"},
                new Patient {Mobile = "9841489352"},
                new Patient {Mobile = "9841489352"},
                new Patient {Mobile = "9841706754"},
                new Patient {Mobile = "9841767293"},
                new Patient {Mobile = "9841972169"},
                new Patient {Mobile = "9865205750"},
                new Patient {Mobile = "9865205750"},
                new Patient {Mobile = "9884600762"},
                new Patient {Mobile = "9884662332"},
                new Patient {Mobile = "9884740515"},
                new Patient {Mobile = "9894173169"},
                new Patient {Mobile = "9894675815"},
                new Patient {Mobile = "9940066020"},
                new Patient {Mobile = "9940684143"},
                new Patient {Mobile = "9941812581"},
                new Patient {Mobile = "9942060491"},
                new Patient {Mobile = "9962917768"},
                new Patient {Mobile = "9994405116"},

            };

            foreach (var patient in list)
            {
                var result = await pa.PatientListExternal(patient);
                Assert.True(result.Any());
            }
        }

        //[Fact]
        //public async void Add()
        //{
        //    var pm = new PatientManager(new PatientDataAccess(null));
        //    var data = new Patient
        //    {
        //        Name = "Yash Raithatha",
        //        Gender = "M",
        //        Age = 33,
        //        Mobile = "9884201159",
        //        Email = "",
        //        DOB = DateTime.Parse("05-06-70"),
        //        Address = ""
        //    };
        //    var result = await pm.SavePatientExternal(data);
        //}

        [Fact]
        public async Task AddPatient()
        {
            var list = new List<Patient>
            {
            //new Patient{Name = "GUPANDRAN",Gender = "M",Age =46,Mobile = "22568824",Email = "N/A",DOB = DateTime.Parse("05-Jun-1970"),Address ="PERIYARPATHAI"},
//new Patient{Name = "AMSAVALLI",Gender = "F",Age =41,Mobile = "22665522",Email = "N/A",DOB = DateTime.Parse("20-May-1975"),Address ="PERIYARPATHAI"},
//new Patient{Name = "MRS.JOTHI",Gender = "F",Age =41,Mobile = "25456256",Email = "N/A",DOB = DateTime.Parse("30-Nov-1975"),Address ="CHOOLAIMEDU"},
//new Patient{Name = "RATHINA BAI,MRS",Gender = "F",Age =71,Mobile = "4423611031",Email = "",DOB = DateTime.Parse("20-Apr-1945"),Address =""},
//new Patient{Name = "DHAYA ANAND.MR",Gender = "M",Age =31,Mobile = "7200491456",Email = "",DOB = DateTime.Parse("13-Aug-1985"),Address =""},
//new Patient{Name = "VIJAYALOKESH",Gender = "M",Age =36,Mobile = "7299513193",Email = "N/A",DOB = DateTime.Parse("20-Oct-1980"),Address ="CH"},
//new Patient{Name = "JAYALAKSHMI.MRS",Gender = "F",Age =24,Mobile = "7358309314",Email = "",DOB = DateTime.Parse("20-Feb-1992"),Address =""},
//new Patient{Name = "ANANDARAJ.MR",Gender = "M",Age =42,Mobile = "7358522460",Email = "",DOB = DateTime.Parse("02-Feb-1974"),Address =""},
//new Patient{Name = "SARAN.MR S/O.NAGARAJ",Gender = "M",Age =14,Mobile = "7401234237",Email = "",DOB = DateTime.Parse("11-Dec-2002"),Address =""},
//new Patient{Name = "SATHYA",Gender = "M",Age =21,Mobile = "7639330492",Email = "N/A",DOB = DateTime.Parse("11-Dec-1995"),Address ="10/ANDAL ST .PERIYARPATHAI"},
//new Patient{Name = "ALAGUSUNDARAM.MR",Gender = "M",Age =81,Mobile = "7708733787",Email = "",DOB = DateTime.Parse("01-May-1935"),Address =""},
//new Patient{Name = "TAMILARASI",Gender = "F",Age =46,Mobile = "7708733787",Email = "N/A",DOB = DateTime.Parse("20-Aug-1970"),Address ="PERIYARPATHAI"},
//new Patient{Name = "PRANEETH MAST",Gender = "M",Age =6,Mobile = "7871083093",Email = "",DOB = DateTime.Parse("07-Jan-2010"),Address =""},
//new Patient{Name = "SUGANYA.MRS",Gender = "F",Age =21,Mobile = "8056011163",Email = "N/A",DOB = DateTime.Parse("22-Dec-1995"),Address ="25 BARATHAMATHA ST PERAMBUR CH 11"},
//new Patient{Name = "RAJENDRAN",Gender = "M",Age =31,Mobile = "8056070747",Email = "N/A",DOB = DateTime.Parse("10-Oct-1985"),Address ="CH"},
//----1
//new Patient{Name = "PRASANTH MR",Gender = "M",Age =26,Mobile = "8056078754",Email = "",DOB = DateTime.Parse("05-May-1990"),Address =""},
//new Patient{Name = "UMAVATHY",Gender = "F",Age =38,Mobile = "8056098934",Email = "",DOB = DateTime.Parse("12-Aug-1978"),Address =""},
//new Patient{Name = "ALEX.MR",Gender = "M",Age =17,Mobile = "8098830754",Email = "",DOB = DateTime.Parse("02-Feb-1999"),Address =""},
//new Patient{Name = "DEVARAJ.MR",Gender = "M",Age =36,Mobile = "8344274382",Email = "",DOB = DateTime.Parse("12-Oct-1980"),Address =""},
//new Patient{Name = "ELAVARASAN.R.MR",Gender = "M",Age =133,Mobile = "8675636365",Email = "ramela1983@gmail.com",DOB = DateTime.Parse("18-Aug-1983"),Address =""},
//new Patient{Name = "YUVARAJ.MR",Gender = "M",Age =15,Mobile = "8675777862",Email = "",DOB = DateTime.Parse("14-Dec-2001"),Address =""},
//new Patient{Name = "THIRUNAVUKKARFASU.MR",Gender = "M",Age =48,Mobile = "8754502449",Email = "",DOB = DateTime.Parse("11-Dec-1968"),Address =""},
//new Patient{Name = "SUBRAMANIYAN.MR",Gender = "M",Age =16,Mobile = "8760515740",Email = "",DOB = DateTime.Parse("12-May-2000"),Address =""},
//new Patient{Name = "BALACHANDAR MR",Gender = "M",Age =31,Mobile = "8807576290",Email = "",DOB = DateTime.Parse("05-Aug-1985"),Address =""},
//new Patient{Name = "VISHNUBHARATH MR",Gender = "M",Age =38,Mobile = "8870008433",Email = "",DOB = DateTime.Parse("06-Jan-1978"),Address =""},
//new Patient{Name = "JAYAVEERAN",Gender = "M",Age =30,Mobile = "8883666419",Email = "N/A",DOB = DateTime.Parse("20-Jun-1986"),Address ="VRIDDHACHALAM"},
//new Patient{Name = "JAYAVEERAN",Gender = "M",Age =30,Mobile = "8883666419",Email = "N/A",DOB = DateTime.Parse("20-Jun-1986"),Address ="VRIDDHACHALAM"},
//new Patient{Name = "VEERA",Gender = "M",Age =30,Mobile = "8883666419",Email = "N/A",DOB = DateTime.Parse("20-Jun-1986"),Address ="PERIYARPATHAI"},
//new Patient{Name = "JAYAM MRS",Gender = "F",Age =51,Mobile = "8939112802",Email = "",DOB = DateTime.Parse("21-Jan-1965"),Address =""},
//new Patient{Name = "RAJESHWARI.MRS",Gender = "F",Age =56,Mobile = "9003106486",Email = "",DOB = DateTime.Parse("12-Aug-1960"),Address =""},
//new Patient{Name = "RAMA.MRS",Gender = "F",Age =17,Mobile = "9003204079",Email = "",DOB = DateTime.Parse("02-Mar-1999"),Address =""},
//----2
//new Patient{Name = "SUJATHA,MRS",Gender = "F",Age =18,Mobile = "9003980645",Email = "",DOB = DateTime.Parse("12-Apr-1998"),Address =""},
//new Patient{Name = "RADHA.MRS",Gender = "F",Age =43,Mobile = "9025125195",Email = "",DOB = DateTime.Parse("23-Nov-1973"),Address =""},
//new Patient{Name = "JAYANTHI MRS",Gender = "F",Age =31,Mobile = "9087002296",Email = "",DOB = DateTime.Parse("03-May-1985"),Address =""},
//new Patient{Name = "GANESAN",Gender = "M",Age =46,Mobile = "9094984554",Email = "N/A",DOB = DateTime.Parse("05-Jun-1970"),Address ="PERIYARPATHAI"},
//new Patient{Name = "GOURISHANKAR.MR",Gender = "M",Age =15,Mobile = "9095498142",Email = "",DOB = DateTime.Parse("21-Dec-2001"),Address =""},
//new Patient{Name = "KALAVATHI.MRS",Gender = "F",Age =61,Mobile = "9176443121",Email = "",DOB = DateTime.Parse("21-Aug-1955"),Address =""},
//new Patient{Name = "PADMAPRIYA.MRS",Gender = "F",Age =32,Mobile = "9176809596",Email = "",DOB = DateTime.Parse("11-Oct-1984"),Address =""},
//new Patient{Name = "udaya kumar",Gender = "M",Age =29,Mobile = "9344959892",Email = "udayakumar@hcue.co",DOB = DateTime.Parse("01-Jan-1987"),Address =""},
//new Patient{Name = "CHITRA.MS",Gender = "F",Age =31,Mobile = "9380094403",Email = "N/A",DOB = DateTime.Parse("12-Oct-1985"),Address ="N/A"},
//new Patient{Name = "SRIVATHSAN.B.DR",Gender = "M",Age =52,Mobile = "9382204466",Email = "",DOB = DateTime.Parse("22-Dec-1964"),Address =""},
//new Patient{Name = "GUNASEKARAN MR",Gender = "M",Age =49,Mobile = "9442485294",Email = "",DOB = DateTime.Parse("05-Aug-1967"),Address =""},
//new Patient{Name = "VIJAYALAKSHMI",Gender = "M",Age =46,Mobile = "9442540779",Email = "N/A",DOB = DateTime.Parse("20-May-1970"),Address ="PERIYARPATHAI"},
//new Patient{Name = "RAJAMANI MR",Gender = "M",Age =26,Mobile = "9444311973",Email = "",DOB = DateTime.Parse("08-Jul-1990"),Address =""},
//new Patient{Name = "SUSEELA.MRS",Gender = "F",Age =51,Mobile = "9445390047",Email = "",DOB = DateTime.Parse("05-Dec-1965"),Address =""},
//new Patient{Name = "BIBRIN",Gender = "M",Age =25,Mobile = "9486847965",Email = "N/A",DOB = DateTime.Parse("10-May-1991"),Address ="PERIYARPATHAI"},
//new Patient{Name = "SENTHAMARAI.MRS",Gender = "F",Age =52,Mobile = "9500133456",Email = "",DOB = DateTime.Parse("12-Dec-1964"),Address =""},
//new Patient{Name = "SHANTHI.MRS",Gender = "F",Age =51,Mobile = "9551066040",Email = "",DOB = DateTime.Parse("05-May-1965"),Address =""},
//new Patient{Name = "JOHN.MR",Gender = "M",Age =20,Mobile = "9566199713",Email = "",DOB = DateTime.Parse("15-May-1996"),Address =""},
//new Patient{Name = "LOGANATHAN.MR",Gender = "M",Age =19,Mobile = "9566945926",Email = "",DOB = DateTime.Parse("15-Dec-1997"),Address =""},
//----3
//new Patient{Name = "JAIBALAJI.MR",Gender = "M",Age =32,Mobile = "9658585855",Email = "ajaibalalaji@gmail.com",DOB = DateTime.Parse("10-Jul-1984"),Address ="f4 nr flats alagiri nagar 6 th street chennai"},
//new Patient{Name = "SENGANI.MR",Gender = "M",Age =46,Mobile = "9677189313",Email = "",DOB = DateTime.Parse("01-May-1970"),Address =""},
//new Patient{Name = "ANITHA.MRS",Gender = "F",Age =21,Mobile = "9710061204",Email = "",DOB = DateTime.Parse("05-Jun-1995"),Address =""},
//new Patient{Name = "ANDAL.MRS",Gender = "F",Age =61,Mobile = "9710245847",Email = "",DOB = DateTime.Parse("05-Jul-1955"),Address =""},
//new Patient{Name = "FAREEDHA BAI",Gender = "F",Age =19,Mobile = "9710577592",Email = "",DOB = DateTime.Parse("20-Feb-1997"),Address =""},
//new Patient{Name = "ILANGOVAN.MR",Gender = "M",Age =46,Mobile = "9710679839",Email = "",DOB = DateTime.Parse("12-Oct-1970"),Address =""},
//new Patient{Name = "REVATHI.MRS",Gender = "F",Age =28,Mobile = "9715304865",Email = "revathiela1988@gmail.com",DOB = DateTime.Parse("23-May-1988"),Address =""},
//new Patient{Name = "NALINIKUMARI MRS",Gender = "F",Age =31,Mobile = "9786234758",Email = "",DOB = DateTime.Parse("12-Sep-1985"),Address =""},
//new Patient{Name = "MUTHULAKSHMI.MRS",Gender = "F",Age =20,Mobile = "9786257971",Email = "",DOB = DateTime.Parse("12-Aug-1996"),Address =""},
//new Patient{Name = "SELVIMAHADEVAN.MRS",Gender = "F",Age =36,Mobile = "9789036890",Email = "",DOB = DateTime.Parse("15-May-1980"),Address =""},
//new Patient{Name = "MAHADEVAN",Gender = "M",Age =44,Mobile = "9789036891",Email = "gomahadevan@gmail.com",DOB = DateTime.Parse("27-Nov-1972"),Address =""},
//new Patient{Name = "V. MAHADEVAN",Gender = "M",Age =44,Mobile = "9789036891",Email = "gomahadevan@gmail.com",DOB = DateTime.Parse("27-Nov-1972"),Address ="PLT NO 33 ,DOOR NO 5 A,1 ST CROSS ST ,IYAPPA  NAGAR ,RAJAKILPAKKAM ,CH-73"},
//new Patient{Name = "SURESHBABU.H.MR",Gender = "M",Age =32,Mobile = "9790260939",Email = "",DOB = DateTime.Parse("04-May-1984"),Address =""},
//new Patient{Name = "MEENA.MS",Gender = "F",Age =19,Mobile = "9790878817",Email = "",DOB = DateTime.Parse("12-Feb-1997"),Address =""},
//new Patient{Name = "RAMATHULASI.A",Gender = "F",Age =29,Mobile = "9790967553",Email = "N/A",DOB = DateTime.Parse("01-Oct-1987"),Address ="NO/31/10CHETTY ST AYNAVARAM.CHENNAI"},
//new Patient{Name = "KALA.MRS",Gender = "F",Age =35,Mobile = "9790981475",Email = "",DOB = DateTime.Parse("23-Feb-1981"),Address =""},
//new Patient{Name = "ADHILAKSHI.MRS",Gender = "F",Age =46,Mobile = "9791166287",Email = "",DOB = DateTime.Parse("02-Jun-1970"),Address =""},
//new Patient{Name = "DILIPIN",Gender = "M",Age =31,Mobile = "9791840627",Email = "",DOB = DateTime.Parse("31-Jul-1985"),Address =""},
//new Patient{Name = "SHANTHI MRS",Gender = "F",Age =36,Mobile = "9798917355",Email = "",DOB = DateTime.Parse("20-Jan-1980"),Address =""},
//new Patient{Name = "MATHU.MR",Gender = "M",Age =29,Mobile = "9840058102",Email = "",DOB = DateTime.Parse("24-Dec-1987"),Address =""},
//new Patient{Name = "KRISHNA VENI.MRS",Gender = "F",Age =51,Mobile = "9840156718",Email = "",DOB = DateTime.Parse("12-Dec-1965"),Address =""},
//new Patient{Name = "KARTHIK BALAJI MAST",Gender = "M",Age =8,Mobile = "9840403626",Email = "",DOB = DateTime.Parse("05-Feb-2008"),Address =""},
//new Patient{Name = "KOWSALYA MRS",Gender = "F",Age =28,Mobile = "9840437948",Email = "",DOB = DateTime.Parse("31-Dec-1988"),Address =""},
//new Patient{Name = "SAMBATH.MR",Gender = "M",Age =49,Mobile = "9840468916",Email = "",DOB = DateTime.Parse("21-Aug-1967"),Address =""},
//new Patient{Name = "BALAJI",Gender = "M",Age =36,Mobile = "9840718365",Email = "N/A",DOB = DateTime.Parse("12-Dec-1980"),Address ="PERIYAR PATHAI"},
//new Patient{Name = "USHA RAMANATHAN MRS",Gender = "F",Age =51,Mobile = "9840766277",Email = "",DOB = DateTime.Parse("25-May-1965"),Address =""},
//new Patient{Name = "HEMALATHA .MS",Gender = "F",Age =29,Mobile = "9840906923",Email = "N/A",DOB = DateTime.Parse("30-Dec-1987"),Address ="PERIYAR PATHI"},
//new Patient{Name = "SHANTHI MRS",Gender = "F",Age =46,Mobile = "9840968868",Email = "",DOB = DateTime.Parse("21-Aug-1970"),Address =""},
//new Patient{Name = "THANGAMANI.MRS",Gender = "F",Age =74,Mobile = "9840996290",Email = "",DOB = DateTime.Parse("25-Dec-1942"),Address =""},
//----4
//new Patient{Name = "LALITHA MRS",Gender = "F",Age =36,Mobile = "9840996290",Email = "",DOB = DateTime.Parse("21-Dec-1980"),Address =""},
//new Patient{Name = "THANGARAJ.MR",Gender = "M",Age =66,Mobile = "9841056051",Email = "",DOB = DateTime.Parse("11-Dec-1950"),Address =""},
//new Patient{Name = "SUGANTHI .MRS",Gender = "F",Age =45,Mobile = "9841113199",Email = "",DOB = DateTime.Parse("21-Dec-1971"),Address =""},
//new Patient{Name = "ANAND MR",Gender = "M",Age =41,Mobile = "9841267198",Email = "",DOB = DateTime.Parse("03-May-1975"),Address =""},
//new Patient{Name = "SUGANYA.MRS",Gender = "F",Age =24,Mobile = "9841489352",Email = "N/A",DOB = DateTime.Parse("09-May-1992"),Address ="CH"},
//new Patient{Name = "SUGANYA.MRS",Gender = "F",Age =24,Mobile = "9841489352",Email = "",DOB = DateTime.Parse("09-May-1992"),Address =""},
//new Patient{Name = "ANURADHA.MRS",Gender = "F",Age =22,Mobile = "9841706754",Email = "",DOB = DateTime.Parse("11-Dec-1994"),Address =""},
//new Patient{Name = "JAYAKUMAR.MR",Gender = "M",Age =40,Mobile = "9841767293",Email = "",DOB = DateTime.Parse("12-Aug-1976"),Address =""},
//new Patient{Name = "SARALA MRS",Gender = "F",Age =41,Mobile = "9841972169",Email = "",DOB = DateTime.Parse("21-Jan-1975"),Address =""},
//new Patient{Name = "JAYAVEERAN",Gender = "M",Age =30,Mobile = "9865205750",Email = "N/A",DOB = DateTime.Parse("20-Jun-1986"),Address ="VRIDDHCHALAM"},
//new Patient{Name = "JAYAVEERAN",Gender = "M",Age =30,Mobile = "9865205750",Email = "",DOB = DateTime.Parse("20-Jun-1986"),Address =""},
//new Patient{Name = "RENUKA MRS",Gender = "F",Age =51,Mobile = "9884600762",Email = "",DOB = DateTime.Parse("05-Jul-1965"),Address =""},
//new Patient{Name = "RATHNA MRS",Gender = "F",Age =15,Mobile = "9884662332",Email = "",DOB = DateTime.Parse("11-Dec-2001"),Address =""},
//new Patient{Name = "MOHAN.MR",Gender = "M",Age =31,Mobile = "9884740515",Email = "",DOB = DateTime.Parse("01-Feb-1985"),Address =""},
//new Patient{Name = "ARUN ROBERT .MR",Gender = "M",Age =51,Mobile = "9894173169",Email = "",DOB = DateTime.Parse("25-May-1965"),Address =""},
//new Patient{Name = "SUBHASHREE",Gender = "F",Age =30,Mobile = "9894675815",Email = "N/A",DOB = DateTime.Parse("30-Jun-1986"),Address ="N/A"},
//new Patient{Name = "KARTHIK.MR",Gender = "M",Age =27,Mobile = "9940066020",Email = "",DOB = DateTime.Parse("12-Aug-1989"),Address =""},
//new Patient{Name = "SAHANA SRI.BABY",Gender = "F",Age =1,Mobile = "9940684143",Email = "",DOB = DateTime.Parse("10-Oct-2015"),Address =""},
//new Patient{Name = "TIRUPATHI.MR",Gender = "M",Age =29,Mobile = "9941812581",Email = "",DOB = DateTime.Parse("27-Nov-1987"),Address =""},
//new Patient{Name = "SURESH",Gender = "M",Age =36,Mobile = "9942060491",Email = "N/A",DOB = DateTime.Parse("20-Aug-1980"),Address ="PERIYARPATHAI"},
//new Patient{Name = "SUSAN.BABY",Gender = "F",Age =2,Mobile = "9962917768",Email = "",DOB = DateTime.Parse("02-Jan-2014"),Address =""},
//new Patient{Name = "ADILINGAM.MR",Gender = "M",Age =31,Mobile = "9994405116",Email = "N/A",DOB = DateTime.Parse("27-Nov-1985"),Address ="NO27 SARASWATHY MAIN ROAD ALWARTHIRUNAGAR CHENNAI"},

};
            //var pa = new PatientApi(new TempConfig());

            //    foreach (var patient in list)
            //    {
            //        try
            //        {
            //            await pa.PatientSaveExternal(patient);
            //        }
            //        catch (Exception)
            //        {

            //        }
            //    }




        }
    }


    public class TempConfig : ConfigHelper
    {
        public TempConfig() : base(null, null, null,null)
        {

        }

        //public override string PatientAuthToken { get { return "QVdTaEN1ZVBhdGllbnRBcHBXZWJBQ0xTVFIxNzAyMjAxNg=="; } }

        //public override string PlatformAuthToken { get { return "QVdTaEN1ZVBQYXJ0bmVyQXBwV2ViQUNMU1RSMTcwMjIwMTY="; } }

        //public override bool ApiAuth {
        //    get { return true; }
        //}

        //public override string UrlExternalPatient {
        //    get { return "https://d1aunqc1cts68e.cloudfront.net/"; }
        //}

        //public override string UrlExternalPlatform
        //{
        //    get { return "https://d2uy4gg09d0916.cloudfront.net/"; }
        //}
    }

}


