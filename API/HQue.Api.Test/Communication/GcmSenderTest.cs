﻿using HQue.ServiceConnector.Connector;

namespace HQue.Api.Test.Communication
{
    public class GcmSenderTest
    {
        [Fact]
        public async void SendStartReplication()
        {
            var connector = new RestConnector();

            var gsmMessage = new GcmMessage
            {
                to = "eC74R_vEShA:APA91bEVFiBMhJ3YbCfYilmcfNwND_MP0GQvnKMPn_kU9KxmerNsXKi7wHIVENHPoq4fEyswXQNpmBPRlS9cG_sU3Gf9UW1WZW0qBZvnnyPJSMERXTjcoUJaS1sZMrhOZYHiYHlunKWY",
                data = { message = "replicate", messageId = "" }
            };

            connector.AddHeaders("Authorization", "key=AIzaSyAOeKtGq6VeN3yjBVZtjDnoTRpu5e6ZWLw");
            var result = await connector.PostAsyc("https://gcm-http.googleapis.com/gcm/send", gsmMessage);
        }

    }

    public class GcmMessage
    {
        public GcmMessage()
        {
            data = new Data();
        }

        public Data data { get; set; }

        public string to { get; set; }
    }

    public class Data
    {
        public string messageId { get; set; }
        public string message { get; set; }
    }
}
