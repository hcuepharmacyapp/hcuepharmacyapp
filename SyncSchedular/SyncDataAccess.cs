﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace SyncSchedular
{
    public partial class SyncDataAccess
    {
      
        public async Task<bool> ExecuteProcedureAsync(string procName, Dictionary<string, object> parameters)
        {
            // SqlConnection conn = new SqlConnection("Database=student;Server=.;user=sa;password=aaaaaaa");
            try
            {

                using (SqlConnection conn = new SqlConnection(ConnectionString))
                {
                    conn.Open();
                    //int result = false;
                    using (SqlCommand cmd = new SqlCommand(procName, conn))
                    {
                        //  add the table - valued - parameter.
                        try
                        {
                            cmd.CommandType = CommandType.StoredProcedure;
                            foreach (KeyValuePair<string, object> item in parameters)
                            {
                                cmd.Parameters.AddWithValue(item.Key, GetParameterValue(item.Value));
                            }

                             await cmd.ExecuteNonQueryAsync();
                            
                            return true;
                        }
                         
                        catch (SqlException ex)
                        {
                            return false;
                        }
                       
                    }

                }
               
            }
            catch(Exception ex)
            {
                return false;
            }
           
        }




        public async Task<bool> NonQueryAsyc(string query, Dictionary<string, object> parameters)
        {
           
            using (SqlConnection conn = new SqlConnection(ConnectionString))
            {
                conn.Open();
                //int result = false;
                using (SqlCommand cmd = new SqlCommand(query, conn))

                {
                    //  add the table - valued - parameter.
                    try
                    {
                        cmd.CommandType = CommandType.Text; ;
                        foreach (KeyValuePair<string, object> item in parameters)
                        {
                            cmd.Parameters.AddWithValue(item.Key, GetParameterValue(item.Value));
                        }
                       
                       dynamic result =  await cmd.ExecuteNonQueryAsync();
                        return true;
                    }
                    catch (SqlException ex)
                    {
                        throw;
                    }

                }

            }
        }

        

        public void WriteOfflineErrorLog(SyncObject data, string errorMessage)
        {
            if (data == null)
                return;

            try
            {

                using (SqlConnection conn = new SqlConnection(LogConnectionString))
                {
                    conn.Open();
                    string query = $"INSERT INTO syncdata_Offline_error (AccountID,InstanceId,Action,Data, ErrorMessage) VALUES (@AccountId, @InstanceId, @Action, @Data, @ErrorMessage)";
                    using (SqlCommand cmd = new SqlCommand(query, conn))
                    {
                        cmd.Parameters.AddWithValue("AccountID", data.Parameters.Keys.Contains("AccountId") ? data.Parameters["AccountId"] : DBNull.Value);

                        cmd.Parameters.AddWithValue("InstanceId", data.Parameters.Keys.Contains("InstanceId") ? data.Parameters["InstanceId"] : DBNull.Value);
                        cmd.Parameters.AddWithValue("Action", data.EventLevel);
                        cmd.Parameters.AddWithValue("Data", JsonConvert.SerializeObject(data));
                        cmd.Parameters.AddWithValue("ErrorMessage", errorMessage);
                        cmd.ExecuteNonQuery();
                    }
                }               

            }
            catch (Exception ex)
            {

                //do nothing
            }

        }

        public async Task<bool> WriteXmlToDatabase(SyncObject data)
        {
            try
            {
                Dictionary<string, object> param = new Dictionary<string, object>();
                //XmlDocument xmldoc = new XmlDocument();
                //xmldoc.LoadXml(data.QueryText);

                using (SqlConnection conn = new SqlConnection(ConnectionString))
                {
                    conn.Open();
                    //int result = false;
                    using (SqlCommand cmd = new SqlCommand("Usp_ClosingStockToOnline", conn))
                    {
                        
                        cmd.CommandType = CommandType.StoredProcedure;
                        foreach (KeyValuePair<string, object> item in data.Parameters)
                        {
                            cmd.Parameters.AddWithValue(item.Key, GetParameterValue(item.Value));
                            //GetParameter(cmd, item.Key, item.Value);                            
                        }

                        cmd.Parameters.AddWithValue("xmlData", data.QueryText);
                        await cmd.ExecuteNonQueryAsync();
                        return true;
                    }
                }
                
            }
            catch (Exception e)
            {
                WriteOfflineErrorLog(data, "Usp_ClosingStockToOnline" + e.Message);
                // do nothing for PRIMARY KEY VIOLATION
                if (((System.Data.SqlClient.SqlException)e).Number == 2627)
                    return true;

                return false;
            }
            
        }
        public async void WriteToOnlineSyncTargetQueue(SyncObject data)
        {
            // from ONLINE to OFFLINE SYNC
            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionString))
                {
                    conn.Open();
                    string query = $"INSERT INTO SyncData (AccountID,InstanceId,Action,Data) VALUES (@AccountId, @InstanceId, @Action, @Data)";
                    using (SqlCommand cmd = new SqlCommand(query, conn))
                    {
                      
                        cmd.Parameters.AddWithValue("AccountID", data.Parameters.Keys.Contains("AccountId") ? data.Parameters["AccountId"] : DBNull.Value);

                      //  cmd.Parameters.AddWithValue("InstanceId", data.Parameters.Keys.Contains("InstanceId") ? data.Parameters["InstanceId"] : DBNull.Value);
                        cmd.Parameters.AddWithValue("InstanceId", data.Parameters.Keys.Contains("ToInstanceId") ? data.Parameters["ToInstanceId"] : data.Parameters["InstanceId"]);

                        cmd.Parameters.AddWithValue("Action", data.EventLevel);
                        cmd.Parameters.AddWithValue("Data", JsonConvert.SerializeObject(data));
                        await cmd.ExecuteNonQueryAsync();
                    }
                }
                      
            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
            }
        }

        public async void WriteOfflineDBLog(SyncObject data)
        {
            //LogConnectionString

            if (data == null)
                return;

            try
            {

                using (SqlConnection conn = new SqlConnection(LogConnectionString))
                {
                    conn.Open();
                    string query = $"INSERT INTO syncdata_Offline (AccountID,InstanceId,Action,Data) VALUES (@AccountId, @InstanceId, @Action, @Data)";
                    using (SqlCommand cmd = new SqlCommand(query, conn))
                    {
                        string pAccountIdKey = string.Empty;
                        if (data.Parameters.Keys.Contains("Accountid"))
                            pAccountIdKey = "Accountid";
                        else if (data.Parameters.Keys.Contains("AccountId"))
                            pAccountIdKey = "AccountId";

                        cmd.Parameters.AddWithValue("AccountID", data.Parameters.Keys.Contains(pAccountIdKey) ? data.Parameters[pAccountIdKey] : DBNull.Value);

                        cmd.Parameters.AddWithValue("InstanceId", data.Parameters.Keys.Contains("InstanceId") ? data.Parameters["InstanceId"] : DBNull.Value);

                        cmd.Parameters.AddWithValue("Action", data.EventLevel);
                        cmd.Parameters.AddWithValue("Data", JsonConvert.SerializeObject(data));

                        await cmd.ExecuteScalarAsync();
                    }
                }

                
            }
            catch (Exception ex)
            {

                //do nothing
            }

        }


        public Task<bool> SaveSyncPendingQuery(List<SyncObject> data)
        {          
            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionString))
                {
                    conn.Open();
                    string query = $"INSERT INTO SyncPendingQuery (Id, AccountID, InstanceId, SyncQuery) VALUES (@Id, @AccountId, @InstanceId, @SyncQuery)";
                    using (SqlCommand cmd = new SqlCommand(query, conn))
                    {

                        cmd.Parameters.AddWithValue("Id", Guid.NewGuid().ToString());
                        cmd.Parameters.AddWithValue("AccountId", data[0].AccountId);
                        cmd.Parameters.AddWithValue("InstanceId", data[0].InstanceId);
                        cmd.Parameters.AddWithValue("SyncQuery", JsonConvert.SerializeObject(data));
                        cmd.ExecuteNonQuery();
                        return Task.FromResult(true);
                    }
                }

            }
            catch (Exception ex)
            {
                return Task.FromResult(true);
            }
        }

        public async Task<bool> ExecuteSyncPendingQuery(string AccountId, string InstanceId)
        {
            if (string.IsNullOrEmpty(AccountId) || string.IsNullOrEmpty(InstanceId))
            {
                if (!string.IsNullOrEmpty(InstanceId))
                {
                    string query1 = $"SELECT AccountId FROM Instance WHERE Id = @InstanceId";
                    using (SqlConnection conn = new SqlConnection(ConnectionString))
                    {
                        conn.Open();
                        using (SqlCommand cmd = new SqlCommand(query1, conn))
                        {
                            cmd.Parameters.AddWithValue("InstanceId", InstanceId);
                            AccountId = cmd.ExecuteScalar().ToString();
                        }
                    }
                   
                }
                else
                {
                    return true;
                }
            }


            using (SqlConnection conn = new SqlConnection(ConnectionString))
            {
                conn.Open();
                string query = $"SELECT Id, SyncQuery FROM SyncPendingQuery WHERE AccountId = @AccountId AND InstanceId = @InstanceId ORDER BY CreatedAt ASC";
                using (SqlCommand cmd = new SqlCommand(query, conn))
                {
                    cmd.Parameters.AddWithValue("AccountId", AccountId);
                    cmd.Parameters.AddWithValue("InstanceId", InstanceId);

                    using (var reader = cmd.ExecuteReaderAsync().Result)
                    {
                        while (reader.Read())
                        {
                            var syncObject = new SyncPendingQuery
                            {
                                Id = reader["Id"].ToString(),
                                SyncQuery = reader["SyncQuery"].ToString(),
                            };
                            var list = JsonConvert.DeserializeObject<List<SyncObject>>(syncObject.SyncQuery);
                            for (var i = 0; i < list.Count; i++)
                            {
                                var syncStatus = true;
                                syncStatus = await WriteToDatabase(list[i]);//.Result;
                                if (syncStatus == false)
                                {
                                    var syncObjectList = new List<SyncObject>();
                                    for (var j = i; j < list.Count; j++)
                                    {
                                        syncObjectList.Add(list[j]);
                                    }

                                    UpdateSyncPendingQuery(syncObjectList, AccountId, InstanceId, syncObject.Id);
                                    return false;
                                }
                            }
                            DeleteSyncPendingQuery(AccountId, InstanceId, syncObject.Id);
                        }
                    }

                    return true;

                }
            }
            
      
        }

        public Task<bool> UpdateSyncPendingQuery(List<SyncObject> data, string AccountId, string InstanceId, string Id)
        {
            try
            {
                string query = $"UPDATE SyncPendingQuery SET SyncQuery = @SyncQuery WHERE AccountId = @AccountId AND InstanceId = @InstanceId AND Id = @Id";

                using (SqlConnection conn = new SqlConnection(ConnectionString))
                {
                    conn.Open();

                    using (SqlCommand cmd = new SqlCommand(query, conn))
                    {
                        try
                        {
                            cmd.Parameters.AddWithValue("Id", Id);
                            cmd.Parameters.AddWithValue("AccountId", AccountId);
                            cmd.Parameters.AddWithValue("InstanceId", InstanceId);
                            cmd.Parameters.AddWithValue("SyncQuery", JsonConvert.SerializeObject(data));
                            cmd.ExecuteNonQuery();
                            return Task.FromResult(true);
                        }
                        catch (SqlException e)
                        {
                            return Task.FromResult(false);
                        }
                    }
                }
                
            }

            catch(Exception e)
            {
                return Task.FromResult(false);
            }
                    
           
        }

        public Task<bool> DeleteSyncPendingQuery(string AccountId, string InstanceId, string Id)
        {
            string query = $"DELETE FROM SyncPendingQuery WHERE AccountId = @AccountId AND InstanceId = @InstanceId AND Id = @Id";
            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionString))
                {
                    conn.Open();

                    using (SqlCommand cmd = new SqlCommand(query, conn))
                    {
                        try
                        {
                            cmd.Parameters.AddWithValue("Id", Id);
                            cmd.Parameters.AddWithValue("AccountId", AccountId);
                            cmd.Parameters.AddWithValue("InstanceId", InstanceId);
                            cmd.ExecuteNonQuery();
                           return Task.FromResult(true);
                        }
                        catch (SqlException e)
                        {
                            return Task.FromResult(false);
                        }
                    }
                }
            }

            catch (Exception e)
            {
                return Task.FromResult(false);
            }

        }

        public Task<bool> LogScheduler(string Id,int EventType,string Data,DateTime StartTime)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(LogConnectionString))
                {
                    conn.Open();
                    string query = $"INSERT INTO syncdata_scheduler (id,eventtype,starttime,data) VALUES (@Id, @EventType,@StartTime,@Data)";
                    using (SqlCommand cmd = new SqlCommand(query, conn))
                    {

                        cmd.Parameters.AddWithValue("Id", Id);
                        cmd.Parameters.AddWithValue("EventType", EventType);
                        cmd.Parameters.AddWithValue("StartTime", StartTime);
                        cmd.Parameters.AddWithValue("Data", Data);
                        cmd.ExecuteNonQuery();
                        return Task.FromResult(true);
                    }
                }

            }
            catch (Exception ex)
            {
                return Task.FromResult(true);
            }
        }


        public Task<bool> LogSchedulerUpdate(string Id, int EventType,string Data, DateTime EndTime)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(LogConnectionString))
                {
                    conn.Open();
                    string query = $"UPDATE syncdata_scheduler SET endtime=@EndTime,data=data+ ' - '+ @Data WHERE id=@Id AND eventtype=@EventType";
                    using (SqlCommand cmd = new SqlCommand(query, conn))
                    {

                        cmd.Parameters.AddWithValue("Id", Id);
                        cmd.Parameters.AddWithValue("EventType", EventType);
                        cmd.Parameters.AddWithValue("EndTime", EndTime);
                        cmd.Parameters.AddWithValue("Data", Data);
                        cmd.ExecuteNonQuery();
                        return Task.FromResult(true);
                    }
                }

            }
            catch (Exception ex)
            {
                return Task.FromResult(true);
            }
        }

        private object GetParameterValue(object value)
        {
          return value != null ? value : DBNull.Value;
            
        }

        private string GetAdminUser(string AccountId)
        {
            string UserId = "";
            try
            {
                using (SqlConnection conn = new SqlConnection(LogConnectionString))
                {
                    conn.Open();
                    string query = $"SELECT Id FROM HQueUser WHERE AccountId = @AccountId AND InstanceId IS NULL";
                    using (SqlCommand cmd = new SqlCommand(query, conn))
                    {

                        cmd.Parameters.AddWithValue("AccountId", AccountId);
                      
                        UserId =  cmd.ExecuteScalar().ToString();
                        
                    }
                    return UserId;
                }

            }
            catch (Exception ex)
            {
                return "";
            }
        }


    }
}
