﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//test
namespace SyncSchedular
{
    public partial class SyncDataAccess
    {
        SyncDataAccess sqldb;

        //public SyncController()
        //{
        //    sqldb = new SyncDataAccess();
        //}

        //  SqlConnection conn;
        string ConnectionString = "";
        string LogConnectionString = "";

        public SyncDataAccess()
        {
            ConnectionString = MyConfigurationSettings.DefaultConnectionString; 
            LogConnectionString = MyConfigurationSettings.LogConnectionString; 
        }

        public Task<bool> SyncDataToServer(dynamic data)
        {
            try
            {
                // If the data is null return true  to continue reading other files  - Added by Sumathi On 20/03/19      
                if (data == null)
                {
                    return Task.FromResult(true);
                }

                var list = new List<SyncObject>();
                var type = data.GetType();
              

                if (type.ToString().LastIndexOf("JObject") > 0)
                {
                    list.Add(JsonConvert.DeserializeObject<SyncObject>(JsonConvert.SerializeObject(data)));
                }
                else
                {
                    list.AddRange(JsonConvert.DeserializeObject<List<SyncObject>>(JsonConvert.SerializeObject(data)));
                }


                var status = ExecuteSyncPendingQuery(list[0].AccountId, list[0].InstanceId).Result;
                if (status == false)
                {
                    return Task.FromResult(false);
                }

                for (var i = 0; i < list.Count; i++)
                {
                    var syncStatus = true;
                    syncStatus = WriteToDatabase(list[i]).Result;
                    if (syncStatus == false)
                    {
                        if (string.IsNullOrEmpty(list[i].AccountId) || string.IsNullOrEmpty(list[i].InstanceId))
                        {
                            return Task.FromResult(false);
                        }
                        else
                        {
                            var syncObjectList = new List<SyncObject>();
                            for (var j = i; j < list.Count; j++)
                            {
                                syncObjectList.Add(list[j]);
                            }
                            return sqldb.SaveSyncPendingQuery(syncObjectList);
                        }
                    }
                }
                return Task.FromResult(true);
            }
            catch (Exception e)
            {
                return Task.FromResult(false);
            }

        }




        public async Task<bool> WriteToDatabase(SyncObject data)
        {
            bool isExecSp = false;
            bool isSPReturn = true;
            string spname = "";
            try
            {
                WriteOfflineDBLog(data);
                if (data.FileType == "xml")
                {
                    return await WriteXmlToDatabase(data);
                }
                //if (data.QueryText.ToLower().Contains("productstock") && data.EventLevel == "A")
                //    return true;

                /* if (data.QueryText.StartsWith("Exec"))
                 {
                     string spname = data.QueryText.Replace("Exec", "").Trim();
                     Dictionary<string, object> param = new Dictionary<string, object>();
                     foreach (var param1 in data.Parameters)
                     {
                         param.Add(param1.Key, param1.Value);
                     }
                     var result = await sqldb.ExecuteProcedureAsync<dynamic>(spname, param);
                     return true;
                 }*/



               // var qb = QueryBuilderFactory.GetQueryBuilder(data.QueryText);

                Dictionary<string, object> qb = new Dictionary<string, object>();
              

                if (data.QueryText.Contains("UPDATE ProductStock SET Stock = @Stock,VendorId = @VendorId,UpdatedAt = @UpdatedAt,ProductId = @ProductId,BatchNo = @BatchNo,ExpireDate = @ExpireDate,VAT = @VAT,TaxType = @TaxType,SellingPrice = @SellingPrice,MRP = @MRP,PurchaseBarcode = @PurchaseBarcode,PackageSize = @PackageSize,PurchasePrice = @PurchasePrice,OfflineStatus = @OfflineStatus,UpdatedAt = @UpdatedAt,UpdatedBy = @UpdatedBy,CST = @CST,HsnCode = @HsnCode,Igst = @Igst,Cgst = @Cgst,Sgst = @Sgst,GstTotal = @GstTotal,Eancode = @Eancode WHERE ProductStock.Id  =  @Id AND ProductStock.AccountId  =  @AccountId AND ProductStock.InstanceId  =  @InstanceId")
                    || data.QueryText.Contains("UPDATE ProductStock SET Stock = @Stock,VendorId = @VendorId,UpdatedAt = @UpdatedAt,BatchNo = @BatchNo,ExpireDate = @ExpireDate,VAT = @VAT,TaxType = @TaxType,SellingPrice = @SellingPrice,MRP = @MRP,PurchaseBarcode = @PurchaseBarcode,PackageSize = @PackageSize,PurchasePrice = @PurchasePrice,OfflineStatus = @OfflineStatus,UpdatedAt = @UpdatedAt,UpdatedBy = @UpdatedBy,CST = @CST,HsnCode = @HsnCode,Igst = @Igst,Cgst = @Cgst,Sgst = @Sgst,GstTotal = @GstTotal,Eancode = @Eancode")
                    || data.QueryText.Contains("UPDATE ProductStock SET Stock = @Stock,VendorId = @VendorId,ProductId = @ProductId,BatchNo = @BatchNo,ExpireDate = @ExpireDate,VAT = @VAT,TaxType = @TaxType,SellingPrice = @SellingPrice,MRP = @MRP,PurchaseBarcode = @PurchaseBarcode,PackageSize = @PackageSize,PurchasePrice = @PurchasePrice,OfflineStatus = @OfflineStatus,UpdatedAt = @UpdatedAt,UpdatedBy = @UpdatedBy,CST = @CST,HsnCode = @HsnCode,Igst = @Igst,Cgst = @Cgst,Sgst = @Sgst,GstTotal = @GstTotal,Eancode = @Eancode WHERE ProductStock.Id  =  @Id AND ProductStock.AccountId  =  @AccountId AND ProductStock.InstanceId  =  @InstanceId")
                    || data.QueryText.Contains("UPDATE ProductStock SET Stock = @Stock,VendorId = @VendorId,UpdatedAt = @UpdatedAt,ProductId = @ProductId,BatchNo = @BatchNo,ExpireDate = @ExpireDate,VAT = @VAT,TaxType = @TaxType,SellingPrice = @SellingPrice,MRP = @MRP,PurchaseBarcode = @PurchaseBarcode,PackageSize = @PackageSize,PurchasePrice = @PurchasePrice,OfflineStatus = @OfflineStatus,UpdatedBy = @UpdatedBy,CST = @CST,HsnCode = @HsnCode,Igst = @Igst,Cgst = @Cgst,Sgst = @Sgst,GstTotal = @GstTotal,Eancode = @Eancode"))
                {
                    // data.QueryText = "UPDATE ProductStock SET Stock = @Stock,VendorId = @VendorId,ProductId = @ProductId,BatchNo = @BatchNo,ExpireDate = @ExpireDate,VAT = @VAT,TaxType = @TaxType,SellingPrice = @SellingPrice,MRP = @MRP,PurchaseBarcode = @PurchaseBarcode,PackageSize = @PackageSize,PurchasePrice = @PurchasePrice,OfflineStatus = @OfflineStatus,UpdatedAt = @UpdatedAt,UpdatedBy = @UpdatedBy,CST = @CST,HsnCode = @HsnCode,Igst = @Igst,Cgst = @Cgst,Sgst = @Sgst,GstTotal = @GstTotal,Eancode = @Eancode WHERE ProductStock.Id  =  @Id AND ProductStock.AccountId  =  @AccountId AND ProductStock.InstanceId  =  @InstanceId";
                    isExecSp = true;
                    spname = "Usp_purchase_productstock_update";

                }
                else if (data.QueryText.Contains("UPDATE ProductStock SET Stock = @Stock,VendorId = @VendorId,UpdatedAt = @UpdatedAt,IsMovingStock = @IsMovingStock,SellingPrice = @SellingPrice,Eancode = @Eancode,OfflineStatus = @OfflineStatus,MRP = @MRP WHERE ProductStock.Id  =  @Id AND ProductStock.AccountId  =  @AccountId AND ProductStock.InstanceId  =  @InstanceId"))
                {

                    isExecSp = true;
                    spname = "usp_Productstock_Update";
                }
                else if (data.QueryText.Contains("Update ProductStock Set GstTotal =  @GstTotal, Igst = @Igst, Cgst = @Cgst, Sgst = @Sgst, UpdatedAt = @UpdatedAt, UpdatedBy = @UpdatedBy Where ProductId = @ProductId And GstTotal is NULL And AccountId = @AccountId"))
                {
                    isExecSp = true;
                    isSPReturn = false;
                    spname = "usp_ProductStock_UpdateByProductId";
                }
                else if (data.QueryText.Contains("UPDATE Product SET Manufacturer = @Manufacturer,Schedule = @Schedule,GenericName = @GenericName,Category = @Category,Packing = @Packing,CommodityCode = @CommodityCode,Type = @Type,RackNo = @RackNo WHERE Product.Id  =  @Id"))
                {
                    isExecSp = true;
                    spname = "USP_Product_Update";
                }
                else if (data.QueryText.Contains("UPDATE Product SET UpdatedAt = @UpdatedAt,UpdatedBy = @UpdatedBy,HsnCode = @HsnCode,Igst = @Igst,Cgst = @Cgst,Sgst = @Sgst,GstTotal = @GstTotal WHERE Product.Id  =  @Id") ||
                    data.QueryText.Contains("UPDATE Product SET UpdatedAt = @UpdatedAt,UpdatedBy = @UpdatedBy,HsnCode = @HsnCode WHERE Product.Id  =  @Id"))
                {
                    isExecSp = true;
                    spname = "usp_product_HSN_update";
                }
                else if (data.QueryText.Contains("INSERT INTO ProductStock (Id,AccountId,InstanceId,ProductId,VendorId,BatchNo,ExpireDate,VAT,TaxType,SellingPrice,MRP,PurchaseBarcode,Stock,PackageSize,PackagePurchasePrice,PurchasePrice,OfflineStatus,CreatedAt,UpdatedAt,CreatedBy,UpdatedBy,CST,IsMovingStock,ReOrderQty,Status,IsMovingStockExpire,NewOpenedStock,NewStockInvoiceNo,NewStockQty,StockImport,ImportQty,Eancode,HsnCode,Igst,Cgst,Sgst,GstTotal,ImportDate,Ext_RefId,TaxRefNo)"))
                {
                    isExecSp = true;
                    spname = "usp_productStock_Insert";
                }

                else if (data.QueryText.Contains(", UpdatedAt = @UpdatedAt, UpdatedBy = @UpdatedBy, TransactionId = @TransactionId where AccountId = @AccountId And InstanceId = @InstanceId And  id = @Id") && data.QueryText.Contains("Update ProductStock Set Stock = Stock"))
                {
                    isExecSp = true;
                    spname = "usp_Sales_Productstock_Update";
                }
                else if (data.QueryText.Contains("UpdatedAt = @UpdatedAt, UpdatedBy = @UpdatedBy where AccountId = @AccountId And InstanceId = @InstanceId And  id = @Id") && data.QueryText.Contains("Update ProductStock Set Stock = Stock -"))
                {
                    isExecSp = true;
                    spname = "usp_Sales_Productstock_Update";
                }
                else if (data.QueryText.Contains("Update ProductStock Set Stock = @Stock, UpdatedAt = @UpdatedAt, UpdatedBy = @UpdatedBy, TransactionId = @TransactionId where AccountId = @AccountId And InstanceId = @InstanceId And  id = @Id"))
                {
                    isExecSp = true;
                    spname = "usp_Sales_Productstock_Updates";
                }
                else if (data.QueryText.Contains("DELETE FROM CustomerPayment WHERE CustomerPayment.AccountId  =  @AccountId AND CustomerPayment.InstanceId  =  @InstanceId AND CustomerPayment.SalesId  =  @SalesId"))
                {
                    isExecSp = true;
                    spname = "usp_CustomerPayment_Updates";
                }
                //else if(data.QueryText.Contains("SYNC DATA DELETE SCHEDULER"))  //Added by Sumathi on 29-11-18 -   Removed as per Poongodi's Suggesstion
                //{
                //    isExecSp = true;
                //    spname = "usp_Reverse_SyncData_Seed_Insert";
                //}

                else if (data.QueryText.Contains("INSERT INTO Patient"))
                {

                    if (data.QueryText.Contains("@CreatedBy") && (data.Parameters["CreatedBy"] == null))
                    {
                        data.Parameters["CreatedBy"] = GetAdminUser(data.Parameters["AccountId"].ToString());
                    }

                    if (data.QueryText.Contains("@UpdatedBy") && (!data.Parameters.ContainsKey("UpdatedBy")))
                    {
                        data.Parameters.Add("UpdatedBy", data.Parameters["CreatedBy"]);
                    }
                    if (data.QueryText.Contains("@EmpID") && (!data.Parameters.ContainsKey("EmpID")))
                    {
                        data.Parameters.Add("EmpID", null);
                    }
                    if (data.QueryText.Contains("@Discount") && (!data.Parameters.ContainsKey("Discount")))
                    {
                        data.Parameters.Add("Discount", null);
                    }
                    if (data.QueryText.Contains("@IsSync") && (!data.Parameters.ContainsKey("IsSync")))
                    {
                        data.Parameters.Add("IsSync", null);
                    }
                    if (data.QueryText.Contains("@CustomerPaymentType") && (!data.Parameters.ContainsKey("CustomerPaymentType")))
                    {
                        data.Parameters.Add("CustomerPaymentType", null);
                    }
                    if (data.QueryText.Contains("@PatientType") && (!data.Parameters.ContainsKey("PatientType")))
                    {
                        data.Parameters.Add("PatientType", null);
                    }
                    if (data.QueryText.Contains("@IsAutoSave") && (!data.Parameters.ContainsKey("IsAutoSave")))
                    {
                        data.Parameters.Add("IsAutoSave", null);
                    }

                    if (data.QueryText.Contains("@Pan") && (!data.Parameters.ContainsKey("Pan")))
                    {
                        data.Parameters.Add("Pan", null);
                    }
                    if (data.QueryText.Contains("@GsTin") && (!data.Parameters.ContainsKey("GsTin")))
                    {
                        data.Parameters.Add("GsTin", null);
                    }
                    if (data.QueryText.Contains("@DrugLicenseNo") && (!data.Parameters.ContainsKey("DrugLicenseNo")))
                    {
                        data.Parameters.Add("DrugLicenseNo", null);
                    }
                    if (data.QueryText.Contains("@LocationType") && (!data.Parameters.ContainsKey("LocationType")))
                    {
                        data.Parameters.Add("LocationType", null);
                    }
                    if (data.QueryText.Contains("@BalanceAmount") && (!data.Parameters.ContainsKey("BalanceAmount")))
                    {
                        data.Parameters.Add("BalanceAmount", null);
                    }
                    if (data.QueryText.Contains("@Status") && (!data.Parameters.ContainsKey("Status")))
                    {
                        data.Parameters.Add("Status", 1);
                    }
                }

                if (isExecSp)
                {
                    Dictionary<string, object> param = new Dictionary<string, object>();
                    foreach (var param1 in data.Parameters)
                    {
                        if (param1.Key.ToString().ToLower() != "transquantity")
                            param.Add(param1.Key, param1.Value);
                    }
                    var result = await ExecuteProcedureAsync(spname, param);
                    if (isSPReturn)
                    {
                        return true;
                    }
                }

                foreach (var param in data.Parameters)
                {
                    if (param.Key.ToString().ToLower() == "updatedby" && param.Value != null && param.Value.ToString().Contains("@"))
                    {
                        qb.Add(param.Key, "");//  qb.Parameters.Add(param.Key, "");
                    }
                    else
                    {
                        qb.Add(param.Key, param.Value);//qb.Parameters.Add(param.Key, param.Value);
                    }
                }
                if (isSPReturn)
                {
                    await NonQueryAsyc(data.QueryText, qb); // await QueryExecuter.NonQueryAsyc(qb);
                }
                //else
                //{
                //    if (data.Parameters.Where(x => x.Key == "InstanceId").Count() == 0)
                //        data.Parameters.Add("InstanceId", "");
                //}
                if (qb.Keys.Contains("ToInstanceId") || data.EventLevel == "A" || data.EventLevel == "G")
                {
                    //WriteToOnlineSyncTargetQueue(new SyncObject() { Id = data.Id, QueryText = qb.GetQuery(), Parameters = qb.Parameters });
                    WriteToOnlineSyncTargetQueue(data); //Added by Martin on 23/05/2017
                }
            }
            catch (Exception e)
            {
                WriteOfflineErrorLog(data, spname + e.Message);

                // do nothing if PasswordResetKey has empty object
                if (data.QueryText.Contains("PasswordResetKey"))
                    return true;

                // do nothing for PRIMARY KEY VIOLATION
                if (((System.Data.SqlClient.SqlException)e).Number == 2627)
                    return true;

                // do nothing for deadlocked just return false to retry
                if (e.Message.Contains("deadlocked"))
                    return false;

                return false;
            }

            return true;
        }
        

        //private void WriteOfflineErrorLog(SyncObject data, string errorMessage)
        //{
        //    if (data == null)
        //        return;

        //    sqldb.WriteOfflineErrorLog(data, errorMessage);

        //}

    }
}
