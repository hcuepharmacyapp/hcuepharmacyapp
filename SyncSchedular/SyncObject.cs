﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SyncSchedular
{
    public class SyncObject
    {
        public SyncObject()
        {
            this.EventLevel = "I";
            this.FileType = "";
        }
        public string Id { get; set; }
        public string QueryText { get; set; }
        public IDictionary<string, object> Parameters { get; set; }
        public string EventLevel { get; set; }
        public string FileType { get; set; }
        public string AccountId { get; set; }
        public string InstanceId { get; set; }
    }



    public class SyncPendingQuery 
    {
        public string Id { get; set; }
        public string SyncQuery { get; set; }
    }
}
