﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace SyncSchedular
{
    internal static class NativeMethods
    {
        [DllImport("kernel32.dll")]
        internal static extern Boolean AllocConsole();
    }

    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>


        [STAThread]
        static void Main(string[] arg)
        {

            //NativeMethods.AllocConsole();
            //MyConfigurationSettings.AppEnvironment = "Prod";
            //// Console.WriteLine("Sync in progress... Please wait...");
            ////  ClsBackgroundWorker.Initialize();
            //ClsBackgroundWorker cls = new ClsBackgroundWorker();
            //bool success = cls.DoSync2();


      //      return;

            if (arg == null || arg.Count() != 2)
            {
                return;
            }


          //  MyConfigurationSettings.AppEnvironment = arg[0];

            if (arg[1] == "FormsApp")
            {
                MyConfigurationSettings.AppEnvironment = arg[0];
                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);
                Application.Run(new FrmSync());
            }
            else if (arg[1] == "console")
            {
                NativeMethods.AllocConsole();
                MyConfigurationSettings.AppEnvironment = arg[0];
                Console.WriteLine("Sync in progress... Please wait...");
                ClsBackgroundWorker.Initialize();
                // Console.ReadLine();
            }


            //NativeMethods.AllocConsole();
            //MyConfigurationSettings.AppEnvironment = "dev";           
            //Console.WriteLine("Starting Application...");
            //ClsBackgroundWorker.Initialize();
            //Console.ReadLine();
        }


    }
}
