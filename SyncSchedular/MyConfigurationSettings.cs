﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;

namespace SyncSchedular
{
    class MyConfigurationSettings
    {
        public static string AppEnvironment = string.Empty;
        //{
        //    get { }
        //    set { }
        //}

        public static string DefaultConnectionString
        {
            get { return ConfigurationManager.ConnectionStrings["DefaultConnectionString_"+ AppEnvironment].ToString();  }
        }
        public static string LogConnectionString
        {
            get { return ConfigurationManager.ConnectionStrings["LogConnectionString_"+ AppEnvironment].ToString(); }
        }
        public static string SyncFolderPath
        {
            get { return ConfigurationManager.AppSettings["SyncFolderPath"].ToString(); }
        }

        //public static string AppEnvironment
        //{
        //    get { return ConfigurationManager.AppSettings["Env"].ToString(); }
        //}
        public static string StorageConnection
        {
            get { return ConfigurationManager.AppSettings["StorageConnectionStr"].ToString(); }
        }
    }
}
