﻿namespace SyncSchedular
{
    partial class FrmSync
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBoxSyncFolder = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.LblResult = new System.Windows.Forms.Label();
            this.backgroundWorker2 = new System.ComponentModel.BackgroundWorker();
            this.BtnStart = new System.Windows.Forms.Button();
            this.BtnStop = new System.Windows.Forms.Button();
            this.BtnSync_Azure = new System.Windows.Forms.Button();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.SuspendLayout();
            // 
            // textBoxSyncFolder
            // 
            this.textBoxSyncFolder.Location = new System.Drawing.Point(145, 65);
            this.textBoxSyncFolder.Name = "textBoxSyncFolder";
            this.textBoxSyncFolder.Size = new System.Drawing.Size(255, 20);
            this.textBoxSyncFolder.TabIndex = 2;
            this.textBoxSyncFolder.Text = "00010101";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(34, 68);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(67, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Folder Name";
            // 
            // LblResult
            // 
            this.LblResult.AutoSize = true;
            this.LblResult.Location = new System.Drawing.Point(142, 174);
            this.LblResult.Name = "LblResult";
            this.LblResult.Size = new System.Drawing.Size(61, 13);
            this.LblResult.TabIndex = 5;
            this.LblResult.Text = "                  ";
            // 
            // BtnStart
            // 
            this.BtnStart.Location = new System.Drawing.Point(145, 130);
            this.BtnStart.Name = "BtnStart";
            this.BtnStart.Size = new System.Drawing.Size(141, 23);
            this.BtnStart.TabIndex = 6;
            this.BtnStart.Text = "Sync Local Folder";
            this.BtnStart.UseVisualStyleBackColor = true;
            this.BtnStart.Click += new System.EventHandler(this.BtnStart_Click);
            // 
            // BtnStop
            // 
            this.BtnStop.Location = new System.Drawing.Point(372, 194);
            this.BtnStop.Name = "BtnStop";
            this.BtnStop.Size = new System.Drawing.Size(75, 23);
            this.BtnStop.TabIndex = 7;
            this.BtnStop.Text = "Stop";
            this.BtnStop.UseVisualStyleBackColor = true;
            this.BtnStop.Visible = false;
            this.BtnStop.Click += new System.EventHandler(this.BtnStop_Click);
            // 
            // BtnSync_Azure
            // 
            this.BtnSync_Azure.Location = new System.Drawing.Point(301, 130);
            this.BtnSync_Azure.Name = "BtnSync_Azure";
            this.BtnSync_Azure.Size = new System.Drawing.Size(146, 23);
            this.BtnSync_Azure.TabIndex = 8;
            this.BtnSync_Azure.Text = "Sync From Azue";
            this.BtnSync_Azure.UseVisualStyleBackColor = true;
            this.BtnSync_Azure.Click += new System.EventHandler(this.BtnSync_Azure_Click);
            // 
            // FrmSync
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(474, 229);
            this.Controls.Add(this.BtnSync_Azure);
            this.Controls.Add(this.BtnStop);
            this.Controls.Add(this.BtnStart);
            this.Controls.Add(this.LblResult);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.textBoxSyncFolder);
            this.MaximizeBox = false;
            this.Name = "FrmSync";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Sync Form";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.TextBox textBoxSyncFolder;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label LblResult;
        private System.ComponentModel.BackgroundWorker backgroundWorker2;
        private System.Windows.Forms.Button BtnStart;
        private System.Windows.Forms.Button BtnStop;
        private System.Windows.Forms.Button BtnSync_Azure;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
    }
}

