﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Threading;

namespace SyncSchedular
{
   
    class ClsBackgroundWorker
    {
    

     //   private static BackgroundWorker backgroundWorker2 = new BackgroundWorker();

        public static void Initialize()
        {
            Run();
        }

        private static void Run()
        {
            try
            {
                bool success = Task.Run(async () => await DoSync()).GetAwaiter().GetResult();
            }
            catch (Exception)
            {

            }
            finally
            {
                System.Environment.Exit(1);
            }
        }
        

        private static async Task<bool> DoSync()
        {
            SyncSchedulerController SyncScheduler = new SyncSchedulerController();
            return await SyncScheduler.SyncFromLocalDrive(MyConfigurationSettings.SyncFolderPath + "syncdata-" + MyConfigurationSettings.AppEnvironment + "\\", "");
        }


        //public void Test()
        //{
        //    DoSync2();
        //}

        //public bool DoSync2()
        //{
        //    SyncSchedulerController SyncScheduler = new SyncSchedulerController();
        //    return SyncScheduler.SyncFromLocalDriveTest(MyConfigurationSettings.SyncFolderPath + "syncdata-" + MyConfigurationSettings.AppEnvironment + "\\", "");
        //}
    }
}
