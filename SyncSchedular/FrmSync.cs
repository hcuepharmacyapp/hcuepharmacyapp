﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SyncSchedular
{
    public partial class FrmSync : Form
    {
       // private static Thread MonitorThread;

        public FrmSync()
        {
            InitializeComponent();
            InitializeBackgroundWorker();           
        }


        private void button1_Click(object sender, EventArgs e)
        {
            
        }

        private async Task<bool> DoSync()
        {

            SyncSchedulerController SyncScheduler = new SyncSchedulerController();
            
            return await SyncScheduler.SyncFromLocalDrive(MyConfigurationSettings.SyncFolderPath + "syncdata-"+MyConfigurationSettings.AppEnvironment + "\\", textBoxSyncFolder.Text);
        }

        private void setLblResultTextSafe(string txt)
        {
            if (LblResult.InvokeRequired)
            { LblResult.Invoke(new Action(() => LblResult.Text = txt)); return; }
            LblResult.Text = txt;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            //this.backgroundWorker2 = new System.ComponentModel.BackgroundWorker();
            //this.backgroundWorker2.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorker2_DoWork);

            //this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            //this.backgroundWorker1.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorker1_DoWork);

        }

        private void InitializeBackgroundWorker()
        {
            this.backgroundWorker2 = new System.ComponentModel.BackgroundWorker();
            this.backgroundWorker2.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorker2_DoWork);

            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.backgroundWorker1.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorker1_DoWork);
        }

        private void backgroundWorker2_DoWork(object sender, DoWorkEventArgs e)
        {
            //  setLblResultTextSafe("Processing Folder " + textBoxSyncFolder.Text + "   File - " + SyncSchedulerController.FileCount);
            setLblResultTextSafe("Processing...please wait... ");
            bool success = Task.Run(async () => await DoSync()).GetAwaiter().GetResult();
          //  MonitorThreadStop();
            if (success)
            {
                if (BtnStart.InvokeRequired)
                { BtnStart.Invoke(new Action(() => EnableDisableButtons(true))); return; }

                EnableDisableButtons(true);
            }

        }

        private void BtnStart_Click(object sender, EventArgs e)
        {
            //setLblResultTextSafe("Processing Folder " + textBoxSyncFolder.Text + "   File - " + SyncSchedulerController.FileCount);
            //bool success = Task.Run(async () => await DoSync()).GetAwaiter().GetResult();
            

            backgroundWorker2.RunWorkerAsync(2000);
       //     BtnStart.Enabled = false;
            BtnSync_Azure.Enabled = false;
           // MonitorThreadStart();
        }

        private void BtnStop_Click(object sender, EventArgs e)
        {
            if (!backgroundWorker2.IsBusy)
                backgroundWorker2.CancelAsync();
        }

        private void BtnSync_Azure_Click(object sender, EventArgs e)
        {
            backgroundWorker1.RunWorkerAsync(2000);
            BtnStart.Enabled = false;
            BtnSync_Azure.Enabled = false;
        }

        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
         //   setLblResultTextSafe("Processing Folder " + textBoxSyncFolder.Text + "   File - " + SyncSchedulerController.FileCount);
            bool success = Task.Run(async () => await DoSyncFromAzure()).GetAwaiter().GetResult();

            if (success)
            {
                if (BtnStart.InvokeRequired)
                { BtnStart.Invoke(new Action(() => EnableDisableButtons(true))); return; }

                EnableDisableButtons(true);
            }
        }

        private async Task<bool> DoSyncFromAzure()
        {
            SyncSchedulerController SyncScheduler = new SyncSchedulerController();

            // return await SyncScheduler.SyncData("syncdata-" + MyConfigurationSettings.AppEnvironment + textBoxSyncPath.Text + "\\", textBoxSyncFolder.Text);
            if(textBoxSyncFolder.Text.Trim() != "")
                return await SyncScheduler.SyncDataDirectory("syncdata-" + MyConfigurationSettings.AppEnvironment, textBoxSyncFolder.Text);           
            else
                return await SyncScheduler.SyncDataAzure("syncdata-" + MyConfigurationSettings.AppEnvironment);
        }

        private void EnableDisableButtons(bool flag)
        {
            BtnStart.Enabled = flag;
            BtnSync_Azure.Enabled = flag;
        }

        //private void MonitorThreadStart()
        //{
        //    MonitorThread = new Thread(UpdateProgress);
        //    MonitorThread.IsBackground = false;
        //    MonitorThread.Start();
        //}

        //private void MonitorThreadStop()
        //{
        //    if (MonitorThread.IsAlive == true)
        //    {
        //        MonitorThread.Abort();
        //    }
        //}

        //private void UpdateProgress()
        //{

        //    while (true)
        //    {                
        //        Thread.Sleep(20000);
        //        setLblResultTextSafe("Processing -  " + SyncSchedulerController.FolderName);
        //    }
        //}

    }
}
