﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;


namespace SyncSchedular
{
    public class SyncSchedulerController
    {
        private SyncDataAccess _syncDataAccess;

        public static int FileCount = 0;
        // private int FileNum = 0;
        public static string FolderName = "";
        public static int FolderCount = 0;

        public SyncSchedulerController()
        {
            _syncDataAccess = new SyncDataAccess();
        }

        public async Task<bool> SyncDataAzure(string FilePath)
        {
            
            try
            {
                string NewId = Guid.NewGuid().ToString();
                await _syncDataAccess.LogScheduler(NewId, 1, "START", DateTime.Now);

                CloudStorageAccount storageAccount = CloudStorageAccount.Parse(MyConfigurationSettings.StorageConnection);
                // Create the blob client.
                CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();
                //Getting container reference
               // FilePath = FilePath + dir;
                CloudBlobContainer container = blobClient.GetContainerReference(FilePath);

                var blobList = await container.ListBlobsSegmentedAsync(string.Empty, false, BlobListingDetails.None, int.MaxValue, null, null, null);
                
                IEnumerable<IListBlobItem> DirectoryList = blobList.Results;
                 FolderCount = DirectoryList.Count();

                foreach (IListBlobItem item in DirectoryList)
                {
                 //   int fileCount = 0;
                   // bool IsRepeat = true;

                    if (item.GetType() == typeof(CloudBlobDirectory))
                    {
                        CloudBlobDirectory directory = (CloudBlobDirectory)item;
                        //if (directory.Prefix == "00010101/" || directory.Prefix == "closingstock/")
                        //{
                        //    continue;
                        //}
                        FolderName = directory.Prefix;
                        await SyncDir(blobClient, container, directory);
                    }

                }
                await _syncDataAccess.LogSchedulerUpdate(NewId, 1, "END", DateTime.Now);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        private async Task<bool> SaveBlobContent(CloudBlobContainer container, IListBlobItem file, bool bsyncxml)
        {
            CloudBlockBlob blob = (CloudBlockBlob)file;
            string content = await blob.DownloadTextAsync();
            bool Success = true;
            if (bsyncxml == false && content.Contains("FileType") && content.Contains("xml"))
            {
                Success = true;
            }
            else
            {
                Success = await SyncDataToServer(content);
                if (Success)
                {
                    return await container.GetBlockBlobReference((blob).Name).DeleteIfExistsAsync();
                }
            }
            return Success;
        }

        public async Task<bool> SyncDataDirectory(string FolderPath, string dir = "")
        {
            string dirName = dir;   // "filewodate";
            CloudStorageAccount storageAccount = CloudStorageAccount.Parse(MyConfigurationSettings.StorageConnection);

            CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();

            CloudBlobContainer container = blobClient.GetContainerReference(FolderPath);
            CloudBlobDirectory blobDirectory = container.GetDirectoryReference(dirName);

            return await SyncDir(blobClient, container, blobDirectory);            
        }

        private async Task<bool> SyncDir(CloudBlobClient blobClient, CloudBlobContainer container, CloudBlobDirectory blobDirectory)
        {
            try
            {
                //string dirName = dir;   // "filewodate";
                //CloudStorageAccount storageAccount = CloudStorageAccount.Parse(MyConfigurationSettings.StorageConnection);
               
                //CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();
               
                //CloudBlobContainer container = blobClient.GetContainerReference(FolderPath);
                //CloudBlobDirectory blobDirectory = container.GetDirectoryReference(dirName);

              //  int MaxCount = 500;

                int fileCount = 0;
                bool IsRepeat = true;

               // DateTime startTime = DateTime.Now;

                while (IsRepeat)
                {
                    try
                    {
                  
                        var files = await blobDirectory.ListBlobsSegmentedAsync(true, BlobListingDetails.None, 100, null, null, null);
                        IEnumerable<IListBlobItem> FileList = files.Results;

                        bool HasFile = false;

                        foreach (IListBlobItem file in FileList)
                        {
                            ++fileCount;
                            HasFile = true;
                            try
                            {
                                if (file.GetType() == typeof(CloudBlockBlob))
                                {
                                    await SaveBlobContent(container, file);
                                }
                            }
                            catch (Exception e)
                            {
                                return false;
                            }

                        }
                        if (HasFile == false)
                        {
                            IsRepeat = false;
                            break;
                        }
                    }
                    catch (Exception e)
                    {
                        IsRepeat = false;
                    }

                }

                  DateTime EndTime = DateTime.Now;
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        private async Task<bool> SaveBlobContent(CloudBlobContainer container, IListBlobItem file)
        {
            CloudBlockBlob blob = (CloudBlockBlob)file;
            string content = await blob.DownloadTextAsync();


            bool Success = await SyncDataToServer(content);
            if (Success)
            {
                return await container.GetBlockBlobReference((blob).Name).DeleteIfExistsAsync();
            }

            return Success;
        }

        //public async Task<bool> SyncFromLocalDrive(string FilePath, string dir = "")
        //{
        //    // string FilePath = @"H:\Blob\syncdata_prod\";
        //    string NewId = Guid.NewGuid().ToString();
        //    try
        //    {
        //       // WriteLog(FilePath);
        //        await _syncDataAccess.LogScheduler(NewId, 1,"START", DateTime.Now);

        //        List <string> DirectoryList = new List<string>();

        //        if (dir != "")
        //            DirectoryList.Add(FilePath + "\\" + dir);
        //        else
        //            DirectoryList = Directory.GetDirectories(FilePath).ToList();

        //        DateTime StartTime = DateTime.Now;
        //        // int fileCount = 0;
        //        FileCount = 0;

        //        foreach (string dirName in DirectoryList)
        //        {
        //            DirectoryInfo info = new DirectoryInfo(dirName);
        //            bool IsRepeat = true;
        //            FolderName = info.Name;
        //            do
        //            {
        //                FileInfo[] files = info.GetFiles().OrderBy(p => p.CreationTime).Take(100).ToArray();
        //                bool Success = false;

        //              //  WriteLog("File count" + files.Length);

        //                if (files.Length == 0)
        //                {
        //                    IsRepeat = false;
        //                    try
        //                    {
        //                        info.Delete();
        //                    }
        //                    catch(Exception e)
        //                    { }
        //                    //break;
        //                }
        //                foreach (FileInfo file in files)
        //                {
        //                    ++FileCount;
        //                    Success = false;
        //                    using (FileStream fs = new FileStream(file.FullName, FileMode.Open, FileAccess.Read))
        //                    {
        //                        using (var sr = new StreamReader(fs, Encoding.UTF8))
        //                        {
        //                            var content = await sr.ReadToEndAsync();

        //                              Success = await SyncDataToServer(content);

        //                            // sr.Close();
        //                        }
        //                        // fs.Close();
        //                    }

        //                    if (Success)
        //                    {
        //                        file.Delete();
        //                    }
        //                }
        //            } while (IsRepeat == true);
        //        }

        //        await _syncDataAccess.LogSchedulerUpdate(NewId, 1,"END", DateTime.Now);

        //        return true;
        //    }
        //    catch(Exception e)
        //    {
        //     //   WriteLog("Message" + e.Message);
        //        await _syncDataAccess.LogSchedulerUpdate(NewId, 1, e.Message,DateTime.Now);
        //        return false;
        //    }
        //}

        public async Task<bool> SyncFromLocalDrive(string FilePath, string dir = "")
        {
            // string FilePath = @"H:\Blob\syncdata_prod\";
            string NewId = Guid.NewGuid().ToString();
            try
            {
                // WriteLog(FilePath);
                await _syncDataAccess.LogScheduler(NewId, 1, "START", DateTime.Now);

                List<string> DirectoryList = new List<string>();

                if (dir != "")
                    DirectoryList.Add(FilePath + "\\" + dir);
                else
                    DirectoryList = Directory.GetDirectories(FilePath).ToList();

                DirectoryList.Sort();

                DateTime StartTime = DateTime.Now;
                // int fileCount = 0;
                FileCount = 0;

                foreach (string dirName in DirectoryList)
                {
                    DirectoryInfo info = new DirectoryInfo(dirName);
                //    bool IsRepeat = true;
                    FolderName = info.Name;

                    int TotalFileCount = info.GetFiles().Count();

                    do
                    {
                        FileInfo[] files = info.GetFiles().OrderBy(p => p.CreationTime).Take(100).ToArray();
                        //   FileInfo[] files = info.GetFiles().OrderBy(p => p.CreationTime).ToArray();
                        bool Success = false;

                        TotalFileCount = TotalFileCount - files.Length;

                        //  WriteLog("File count" + files.Length);

                        if (files.Length == 0)
                        {
                          //  IsRepeat = false;
                            try
                            {
                                TotalFileCount = -1;
                                info.Delete();
                            }
                            catch (Exception e)
                            { }
                            //break;
                        }
                        foreach (FileInfo file in files)
                        {
                            ++FileCount;
                            Success = false;
                            using (FileStream fs = new FileStream(file.FullName, FileMode.Open, FileAccess.Read))
                            {
                                using (var sr = new StreamReader(fs, Encoding.UTF8))
                                {
                                    var content = await sr.ReadToEndAsync();

                                    Success = await SyncDataToServer(content);

                                }
                            }

                            if (Success)
                            {
                                file.Delete();
                            }
                        }
                    } while (TotalFileCount >= 0);
                }

                await _syncDataAccess.LogSchedulerUpdate(NewId, 1, "END", DateTime.Now);

                return true;
            }
            catch (Exception e)
            {
                //   WriteLog("Message" + e.Message);
                await _syncDataAccess.LogSchedulerUpdate(NewId, 1, e.Message, DateTime.Now);
                return false;
            }
        }


        public bool SyncFromLocalDriveTest(string FilePath, string dir = "")
        {
            // string FilePath = @"H:\Blob\syncdata_prod\";
            string NewId = Guid.NewGuid().ToString();
            try
            {
                // WriteLog(FilePath);
             //   await _syncDataAccess.LogScheduler(NewId, 1, "START", DateTime.Now);

                List<string> DirectoryList = new List<string>();

                if (dir != "")
                    DirectoryList.Add(FilePath + "\\" + dir);
                else
                    DirectoryList = Directory.GetDirectories(FilePath).ToList();

                DirectoryList.Sort();

                DateTime StartTime = DateTime.Now;
                // int fileCount = 0;
                FileCount = 0;

                foreach (string dirName in DirectoryList)
                {
                    DirectoryInfo info = new DirectoryInfo(dirName);
                    bool IsRepeat = true;
                    FolderName = info.Name;
                    int TotalFileCount = info.GetFiles().Count();

                   // int FileIndex = 0;

                    while (TotalFileCount > 0)
                    {                        
                        FileInfo[] files = info.GetFiles().OrderBy(p => p.CreationTime).Take(100).ToArray();
                        bool Success = false;


                        TotalFileCount = TotalFileCount - files.Length;

                        //  WriteLog("File count" + files.Length);

                        if (files.Length == 0)
                        {
                            IsRepeat = false;
                            try
                            {
                                info.Delete();
                            }
                            catch (Exception e)
                            { }
                            //break;
                        }
                        foreach (FileInfo file in files)
                        {
                            ++FileCount;
                            Success = false;
                            using (FileStream fs = new FileStream(file.FullName, FileMode.Open, FileAccess.Read))
                            {
                                using (var sr = new StreamReader(fs, Encoding.UTF8))
                                {
                                    var content =  sr.ReadToEnd();

                                    Success =  SyncDataToServer(content).Result;
                                }
                            }

                            if (Success)
                            {
                                file.Delete();
                            }
                        }
                    } 
                }

                 _syncDataAccess.LogSchedulerUpdate(NewId, 1, "END", DateTime.Now);

                return true;
            }
            catch (Exception e)
            {
                //   WriteLog("Message" + e.Message);
                // _syncDataAccess.LogSchedulerUpdate(NewId, 1, e.Message, DateTime.Now);
                return false;
            }
        }

        private async Task<bool> SyncDataToServer(string content)
        {
            try
            {
               // _syncController = new SyncController(_syncManager, _import, _configHelper);

                var settings = new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore,
                    MissingMemberHandling = MissingMemberHandling.Ignore
                };
                var obj = JsonConvert.DeserializeObject<object>(content, settings);

                bool success = await _syncDataAccess.SyncDataToServer(obj);
                return success;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        private static void WriteLog(string logText)
        {
            string logFileName = "log.txt";
            using (FileStream logFile = new FileStream(logFileName, FileMode.Append, FileAccess.Write))
            using (StreamWriter sw = new StreamWriter(logFile))
            {
                sw.WriteLine(string.Format("{0}: {1}", DateTime.Now.ToString(), logText));
            }

        }
    }
}
