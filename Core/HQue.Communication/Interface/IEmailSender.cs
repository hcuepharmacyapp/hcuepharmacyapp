﻿namespace HQue.Communication.Interface
{
    interface IEmailSender
    {
        bool OverrideConfig { get; set; }
        bool Send(string toAddress, int templateId, params object[] templateParams);
    }
}
