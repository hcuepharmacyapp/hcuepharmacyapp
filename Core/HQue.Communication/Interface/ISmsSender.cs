﻿using System.Threading.Tasks;

namespace HQue.Communication.Interface
{
    interface ISmsSender
    {
        Task<bool> Send(string mobile, int templateId, params object[] templateParams);
    }
}
