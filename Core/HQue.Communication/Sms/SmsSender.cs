﻿using System;
using System.Text;
using System.Threading.Tasks;
using HQue.Communication.Interface;
using HQue.Contract.Infrastructure.Communication;
using HQue.Contract.Infrastructure.Inventory;
using HQue.DataAccess.Communication;
using HQue.ServiceConnector.Connector;
using Utilities.Helpers;
using Utilities.Logger;
using Microsoft.AspNetCore.Http;
using System.Security.Claims;
using System.Security.Principal;
using System.Linq;
using HQue.Contract.Infrastructure.Setup;

namespace HQue.Communication.Sms
{
    public class SmsSender : ISmsSender
    {
        private readonly ConfigHelper _configHelper;
        private readonly CommunicationDataAccess _communicationDataAccess;
        private readonly IIdentity _user;
        private ClaimsPrincipal _pri;

        private const string Url =
            "http://trans.kapsystem.com/api/web2sms.php?workingkey=A2e7a2783e11a0de51aee1f6d386bfb4c&sender={1}&to={0}&message={2}";        

        private const string SenderId = "HCUEMS";

        public const int Register = 4;
        public const int AllowViewStock = 5;
        public const int WelcomeCustomer = 6;

        public SmsSender(ConfigHelper configHelper, IHttpContextAccessor httpContextAccessor,
            CommunicationDataAccess communicationDataAccess)
        {
            _configHelper = configHelper;
            _communicationDataAccess = communicationDataAccess;
            _user = httpContextAccessor.HttpContext.User.Identity;
            _pri = httpContextAccessor.HttpContext.User;
        }

        //Added by Sarubala on 16-11-17 - start
        public async Task<bool> SendSms(string mobile, int templateId, SmsLog data, params object[] templateParams)
        {
            data.SetLoggedUserDetails(_pri);
            Instance temp = _communicationDataAccess.GetInstanceSmsCount(data.AccountId, data.InstanceId).Result;
            temp.TotalSmsCount = temp.TotalSmsCount > 0 ? temp.TotalSmsCount : 0;
            temp.SentSmsCount = temp.SentSmsCount > 0 ? temp.SentSmsCount : 0;

            temp.SetLoggedUserDetails(_pri);

            if (string.IsNullOrEmpty(mobile))
            {
                LogAndException.WriteToLogger("Invalid mobile number", ErrorLevel.Debug);
                return false;
            }

            if (data.LogType != "Mediplus Sales Cancel" && data.LogType != "Mediplus Sales Edit" && (string.IsNullOrEmpty(temp.SmsUrl) || string.IsNullOrWhiteSpace(temp.SmsUrl)))
            {
                if (temp.TotalSmsCount <= temp.SentSmsCount)
                {
                    LogAndException.WriteToLogger("Message Count Exceeded", ErrorLevel.Debug);
                    return false;
                }
            }

            var message = templateId == 0 ? templateParams[0].ToString() : PrepareMessage(templateId, templateParams);
            if (_configHelper.AppConfig.SendSms)
            {
                var smsCharsCount = 180;
                if (!string.IsNullOrEmpty(temp.SmsUrl) && temp.SmsUrl.Contains("sms.teleosms.com"))
                {
                    smsCharsCount = 160;
                }
                data.NoOfSms = (Int32)Math.Ceiling((double)message.Length / smsCharsCount);
                data.OpeningCount = temp.TotalSmsCount - temp.SentSmsCount;
                data.ClosingCount = data.OpeningCount - data.NoOfSms;

                temp.SentSmsCount = temp.SentSmsCount + data.NoOfSms;

                if (!string.IsNullOrWhiteSpace(temp.SmsUrl) && !string.IsNullOrEmpty(temp.SmsUrl))
                {
                    if (temp.SmsUserName.ToUpper() != "HCUE" || temp.SmsUserName.ToUpper() == "HCUE" && temp.TotalSmsCount >= temp.SentSmsCount)
                    {
                        //Updated by Sarubala on 07-12-17
                        _communicationDataAccess.SaveSmsLog(data);

                        if (!mobile.StartsWith("91"))
                            mobile = $"91{mobile}";

                        message = message.Replace("&", "%26"); //URL Encode "&"                    

                        var sendUrl = string.Format(temp.SmsUrl, temp.SmsUserName, temp.SmsPassword, mobile, message);

                        SendMessage(sendUrl);
                        await LogCommunication1(mobile, message, data.Id);
                        await _communicationDataAccess.UpdateSentSmsCount(temp, Convert.ToInt32(data.NoOfSms));

                        return true;
                    }
                    else
                    {
                        LogAndException.WriteToLogger("Message Count Exceeded", ErrorLevel.Debug);
                        return false;
                    }
                }
                else if (temp.TotalSmsCount >= temp.SentSmsCount || data.LogType == "Mediplus Sales Cancel" || data.LogType == "Mediplus Sales Edit")
                {
                    //await _communicationDataAccess.UpdateSentSmsCount(temp, Convert.ToInt32(data.NoOfSms)); //Updated by Sarubala on 07-12-17
                    _communicationDataAccess.SaveSmsLog(data);

                    if (!mobile.StartsWith("91"))
                        mobile = $"91{mobile}";

                    message = message.Replace("&", "%26"); //URL Encode "&"

                    var sendUrl = string.Format(Url, mobile, SenderId, message);

                    SendMessage(sendUrl);
                    await LogCommunication1(mobile, message, data.Id);
                    await _communicationDataAccess.UpdateSentSmsCount(temp, Convert.ToInt32(data.NoOfSms));

                    return true;

                }

            }

            LogAndException.WriteToLogger($"NotSent:{message}", ErrorLevel.Debug);
            return false;
        }
        //Added by Sarubala on 16-11-17 - end

        public async Task<bool> Send(string mobile, int templateId, params object[] templateParams)
        {
            if (string.IsNullOrEmpty(mobile))
            {
                LogAndException.WriteToLogger("Invalid mobile number", ErrorLevel.Debug);
                return false;
            }

            Instance temp = new Instance();
            temp.SetLoggedUserDetails(_pri);

            var message = templateId == 0 ? templateParams[0].ToString() : PrepareMessage(templateId, templateParams);
            if (_configHelper.AppConfig.SendSms)
            {
                if (!mobile.StartsWith("91"))
                    mobile = $"91{mobile}";

                //message = Uri.EscapeUriString(message);
                message = message.Replace("&", "%26"); //URL Encode "&"

                if (!string.IsNullOrWhiteSpace(temp.SmsUrl) && !string.IsNullOrEmpty(temp.SmsUrl))
                {
                    var sendUrl = string.Format(temp.SmsUrl, temp.SmsUserName, temp.SmsPassword, mobile, message);
                    //if(HelperCommunication.CheckNet())
                    //{
                    SendMessage(sendUrl);
                    await LogCommunication(mobile, message);
                    //}

                    return true;
                }
                else
                {
                    var sendUrl = string.Format(Url, mobile, SenderId, message);
                    //if(HelperCommunication.CheckNet())
                    //{
                    SendMessage(sendUrl);
                    await LogCommunication(mobile, message);
                    //}

                    return true;
                }

                
            }

            LogAndException.WriteToLogger($"NotSent:{message}", ErrorLevel.Debug);
            return false;
        }

        public Task<bool> Send(string phone, string v)
        {
            throw new NotImplementedException();
        }

        private async Task LogCommunication1(string mobile, string message, string smslogId) //Added by Sarubala on 16-11-2017
        {
            var cl = new CommunicationLog
            {
                UserId = _user.Id(),
                Message = message,
                Mobile = mobile,
                SmsLogId = smslogId,
            };

            cl.SetLoggedUserDetails(_pri);
            if (!String.IsNullOrEmpty(cl.AccountId) && !String.IsNullOrEmpty(cl.InstanceId))
                await _communicationDataAccess.Save(cl);
        }

        private async Task LogCommunication(string mobile, string message)
        {
            var cl = new CommunicationLog
            {
                UserId = _user.Id(),
                Message = message,
                Mobile = mobile,
            };

            cl.SetLoggedUserDetails(_pri);
            if (!String.IsNullOrEmpty(cl.AccountId) && !String.IsNullOrEmpty(cl.InstanceId))
                await _communicationDataAccess.Save(cl);
        }

        private static async void SendMessage(string sendUrl)
        {
            var connector = new RestConnector();
            if (sendUrl.Length > 8000)
            {
                sendUrl.Substring(0, 8000);
            }
            var result = await connector.GetAsyc(sendUrl);
        }

        private string PrepareMessage(int templateId, params object[] templateParams)
        {
            var message = new StringBuilder();

            switch (templateId)
            {
                case Register:
                    message.AppendLine("Dear Member,");
                    message.AppendLine("To activate your account  please check your registered email id.");
                    message.AppendLine("");
                    message.AppendLine("Powered By hCue");
                    break;
                case AllowViewStock:
                    message.AppendLine("Dear Member,");
                    message.AppendLine("To activate your account  please check your registered email id.");
                    message.AppendLine("");
                    message.AppendLine("Powered By hCue");
                    break;
                case WelcomeCustomer:
                    message.AppendLine("Dear {0},");
                    message.AppendLine("Welcome to hCue.");
                    message.AppendLine("");
                    message.AppendLine("Powered By hCue");
                    break;
            }

            return string.Format(message.ToString(), templateParams);
        }

        public StringBuilder SellsBodyBuilder(Sales sales)
        {
            var newbody = new StringBuilder();

            #region Sell

            newbody.AppendFormat("Thank you for shopping at {0}. ", sales.Instance.Name).AppendLine();
            newbody.AppendFormat("Please find the invoice of Rs.{0} for the following purchase. ", Math.Round(Convert.ToDecimal(sales.NetAmount), 2, MidpointRounding.AwayFromZero))
                .AppendLine();
            var i = 1;
            foreach (var item in sales.SalesItem)
            {
                newbody.AppendFormat("{0}. {1} - ({2}) - {3}", i++, item.ProductStock.Product.Name, item.Quantity, item.Total).AppendLine();
            }
            newbody.AppendFormat("Total {0} Rupees", Math.Round(Convert.ToDecimal(sales.NetAmount), 2, MidpointRounding.AwayFromZero)).AppendLine();
            newbody.AppendLine("Powered By hCue");

            #endregion

            return newbody;
        }

        public StringBuilder VendorOrderBodyBuilder(VendorOrder vendorOrder)
        {
            var newbody = new StringBuilder();

            #region Vendor Order

            newbody.AppendFormat("Dear {0},", vendorOrder.Vendor.Name).AppendLine();
            newbody.AppendFormat("{0} is pleased to place an order for the following details(Order Id : {1}). ",
                vendorOrder.Instance.Name, vendorOrder.OrderId).AppendLine();
            var i = 1;
            foreach (var item in vendorOrder.VendorOrderItem)
            {
                newbody.AppendFormat("{0}. {1} - {2}", i++, item.Product.Name, item.Quantity).AppendLine();
            }
            newbody.AppendFormat("Pharmacy Instance Name: {0}", vendorOrder.Instance.Name).AppendLine();
            newbody.AppendFormat("Address: {0}", vendorOrder.Instance.FullAddress).AppendLine();
            newbody.AppendFormat("Phone Number: {0}.", vendorOrder.Instance.Phone).AppendLine();
            newbody.AppendLine("Powered By hCue");

            #endregion

            return newbody;
        }

        //Added by Sarubala on 03-10-17 to send sms to Mediplus on Edit and Cancel
        public StringBuilder SalesEditBuilder_Setting(Sales sales, string salesMode)
        {
            var newbody = new StringBuilder();
            if (salesMode == "Edit")
            {
                newbody.AppendFormat("The following sales invoice is Edited by {0}. ", sales.HQueUser.Name).AppendLine();
            }
            else if (salesMode == "Cancel")
            {
                newbody.AppendFormat("The following sales invoice is Cancelled by {0}. ", sales.HQueUser.Name).AppendLine();
            }

            //newbody.AppendFormat(" Bill No / {0}. {1} - ({2})", (sales.Prefix + sales.InvoiceSeries + sales.InvoiceNo) , sales.InvoiceDate, sales.Instance.Name.ToUpper()).AppendLine();
            newbody.AppendFormat(" Bill No - {0}", (sales.Prefix + sales.InvoiceSeries + sales.InvoiceNo)).AppendLine();
            newbody.AppendFormat(" Bill Date - {0}", Convert.ToDateTime(sales.InvoiceDate).ToString("dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture)).AppendLine();
            if (salesMode == "Cancel") //Added by Sarubala on 13-11-17
            {
                newbody.AppendFormat(" Amount - {0}", sales.FinalValue).AppendLine();
            }
            newbody.AppendFormat(" Branch - {0}", sales.Instance.Name.ToUpper()).AppendLine();

            newbody.AppendLine("Powered By hCue");

            return newbody;
        }

        // Newly Added Gavaskar 08-11-2016 Start
        public StringBuilder SalesBodyBuilder_Setting(Sales sales, InventorySmsSettings inventorySmsSettings)
        {
            var newbody = new StringBuilder();
            //decimal ReturnDiscount = 0;
            //decimal ReturnTotal = 0;

            newbody.AppendFormat("Thank you for shopping at {0}. ", sales.Instance.Name).AppendLine();
            newbody.AppendFormat("Ph : {0}. ", sales.Instance.Phone).AppendLine();
            //newbody.AppendFormat("Please find the invoice of Rs.{0} for the following purchase. ", string.Format("{0:0.00}", sales.RoundOff))
            //    .AppendLine();
            newbody.AppendLine("Please find the invoice details");
            var i = 1;
            //foreach (var item in sales.SalesItem)
            foreach (var item in sales.SalesItem.Where(x => x.IsCreated))
            {
                //newbody.AppendFormat("{0}. {1} - ({2}) - {3}", i++, item.ProductStock.Product.Name.ToUpper(), item.Quantity, item.Total).AppendLine();
                newbody.AppendFormat("{0}. {1} - ({2}) - {3}", i++, item.ProductStock.Product.Name.ToUpper(), item.Quantity, string.Format("{0:0.00}", item.ItemAmount)).AppendLine();
            }
            //foreach (var item in sales.SalesReturn.SalesReturnItem)
            //{
            //    ReturnTotal += item.Total as decimal? ?? 0;
            //    ReturnDiscount += ((item.Discount / 100) * item.Total) as decimal? ?? 0;
            //}

            //newbody.AppendFormat("Sale amount : {0}", string.Format("{0:0.00}", sales.PurchasedTotal - sales.DiscountInValue)).AppendLine();
            newbody.AppendFormat("Sale amount : {0}", string.Format("{0:0.00}", sales.SaleAmount - sales.RoundoffSaleAmount)).AppendLine();

            //if (ReturnTotal > 0)
            if (sales.SalesReturn.NetAmount > 0)
            {
                //newbody.AppendFormat("Return amount : {0}", string.Format("{0:0.00}", ReturnTotal - ReturnDiscount)).AppendLine();
                newbody.AppendFormat("Return amount : {0}", string.Format("{0:0.00}", sales.SalesReturn.NetAmount - sales.SalesReturn.RoundOffNetAmount)).AppendLine();
            }
            //newbody.AppendFormat("Disc. : {0}", string.Format("{0:0.00}", sales.DiscountInValue - ReturnDiscount)).AppendLine();
            newbody.AppendFormat("R.Off : {0}", string.Format("{0:0.00}", sales.RoundoffNetAmount)).AppendLine();
            newbody.AppendFormat("Payable amount : {0}", string.Format("{0:0.00}", sales.NetAmount)).AppendLine();
            //newbody.AppendFormat("Total {0} Rupees.", Math.Round(Convert.ToDecimal(sales.RoundOff), 2, MidpointRounding.AwayFromZero)).AppendLine();
            if (inventorySmsSettings != null && inventorySmsSettings.SmsOption == "Yes")
            {
                newbody.AppendFormat("{0}. ", inventorySmsSettings.SmsContent).AppendLine();
            }
            newbody.AppendLine("Powered By hCue");

            return newbody;
        }

        public StringBuilder SalesBodyBuilder_SettingNew(Sales sales, InventorySmsSettings inventorySmsSettings)
        {
            var newbody = new StringBuilder();
            //decimal ReturnDiscount = 0;
            //decimal ReturnTotal = 0;

            newbody.AppendFormat("Thank you for shopping at {0}. ", sales.Instance.Name);
            newbody.AppendFormat("Ph : {0}. ", sales.Instance.Phone);
            //newbody.AppendFormat("Please find the invoice of Rs.{0} for the following purchase. ", string.Format("{0:0.00}", sales.RoundOff))
            //    .AppendLine();
            newbody.Append("Please find the invoice details ");
            var i = 1;
            //foreach (var item in sales.SalesItem)
            foreach (var item in sales.SalesItem.Where(x => x.IsCreated))
            {
                //newbody.AppendFormat("{0}. {1} - ({2}) - {3}", i++, item.ProductStock.Product.Name.ToUpper(), item.Quantity, item.Total).AppendLine();
                newbody.AppendFormat("{0}. {1} - ({2}) - {3} ", i++, item.ProductStock.Product.Name.ToUpper(), item.Quantity, string.Format("{0:0.00}", item.ItemAmount));
            }
            //foreach (var item in sales.SalesReturn.SalesReturnItem)
            //{
            //    ReturnTotal += item.Total as decimal? ?? 0;
            //    ReturnDiscount += ((item.Discount / 100) * item.Total) as decimal? ?? 0;
            //}

            //newbody.AppendFormat("Sale amount : {0}", string.Format("{0:0.00}", sales.PurchasedTotal - sales.DiscountInValue)).AppendLine();
            newbody.AppendFormat("Sale amount : {0} ", string.Format("{0:0.00}", sales.SaleAmount - sales.RoundoffSaleAmount));
            newbody.AppendFormat("Discount amount : {0} ", string.Format("{0:0.00}", sales.DiscountValue > 0 ? sales.DiscountValue : sales.DiscountInValue));

            //if (ReturnTotal > 0)
            if (sales.SalesReturn.NetAmount > 0)
            {
                //newbody.AppendFormat("Return amount : {0}", string.Format("{0:0.00}", ReturnTotal - ReturnDiscount)).AppendLine();
                //newbody.AppendFormat("Return amount : {0} ", string.Format("{0:0.00}", sales.SalesReturn.NetAmount - sales.SalesReturn.RoundOffNetAmount));
            }
            //newbody.AppendFormat("Disc. : {0}", string.Format("{0:0.00}", sales.DiscountInValue - ReturnDiscount)).AppendLine();
            newbody.AppendFormat("R.Off : {0} ", string.Format("{0:0.00}", sales.RoundoffNetAmount));
            newbody.AppendFormat("Payable amount : {0} ", string.Format("{0:0.00}", sales.NetAmount));
            //newbody.AppendFormat("Total {0} Rupees.", Math.Round(Convert.ToDecimal(sales.RoundOff), 2, MidpointRounding.AwayFromZero)).AppendLine();
            if (inventorySmsSettings != null && inventorySmsSettings.SmsOption == "Yes")
            {
                newbody.AppendFormat("{0}. ", inventorySmsSettings.SmsContent);
            }

            return newbody;
        }

        public StringBuilder CustomerBodyBuilder_Setting(string customerName, string smsContent, Sales data)
        {
            var newbody = new StringBuilder();
            #region Customer

            var instanceContact = "";
            if (!string.IsNullOrEmpty(data.Instance.Phone) || !string.IsNullOrEmpty(data.Instance.Mobile))
            {
                string contact = !string.IsNullOrEmpty(data.Instance.Phone) ? data.Instance.Phone : data.Instance.Mobile;
                instanceContact = " Contact: " + contact;
            }

            newbody.AppendFormat("Dear {0},", customerName).AppendLine();
            newbody.AppendFormat("{0}.", smsContent).AppendLine();
            newbody.AppendFormat("{0}.", data.Instance.Name + "." + instanceContact).AppendLine();
            newbody.AppendLine("Powered By hCue");

            #endregion
            return newbody;
        }

        // Newly Added Gavaskar 08-11-2016 End

        // Added for Sales Return by San - 15-06-2017
        public StringBuilder SalesReturnBodyBuilder_Setting(Sales sales, InventorySmsSettings inventorySmsSettings)
        {
            var newbody = new StringBuilder();
            //decimal ReturnDiscount = 0;
            //decimal ReturnTotal = 0;

            newbody.AppendFormat("Thank you for visiting {0}. ", sales.Instance.Name).AppendLine();
            newbody.AppendFormat("Ph : {0}. ", sales.Instance.Phone).AppendLine();

            newbody.AppendLine("Please find the invoice details");
            var i = 1;
            foreach (var item in sales.SalesReturnItem)
            {
                item.MrpSellingPrice = item.SellingPrice;
                //newbody.AppendFormat("{0}. {1} - ({2}) - {3}", i++, item.ProductStock.Product.Name.ToUpper(), string.Format("{0:0}", item.Quantity), string.Format("{0:0.00}", item.Total) ).AppendLine();            
                newbody.AppendFormat("{0}. {1} - ({2}) - {3}", i++, item.ProductStock.Product.Name.ToUpper(), string.Format("{0:0}", item.Quantity), string.Format("{0:0.00}", item.Quantity * item.MrpSellingPrice)).AppendLine();
                //ReturnTotal += item.Total as decimal? ?? 0;
                //ReturnDiscount += ((item.Discount / 100) * item.Total) as decimal? ?? 0;
            }

            //newbody.AppendFormat("R.Off : {0}", string.Format("{0:0.00}", sales.RoundoffNetAmount)).AppendLine();
            newbody.AppendFormat("R.Off : {0}", string.Format("{0:0.00}", sales.SalesReturn.RoundOffNetAmount)).AppendLine();
            //newbody.AppendFormat("Return amount : {0}", string.Format("{0:0.00}", Math.Round((decimal)(ReturnTotal - ReturnDiscount), MidpointRounding.AwayFromZero))).AppendLine();
            newbody.AppendFormat("Return amount : {0}", string.Format("{0:0.00}", sales.SalesReturn.NetAmount)).AppendLine();
            if (inventorySmsSettings != null && inventorySmsSettings.SmsOption == "Yes")
            {
                newbody.AppendFormat("{0}. ", inventorySmsSettings.SmsContent).AppendLine();
            }
            newbody.AppendLine("Powered By hCue");

            return newbody;
        }

        // Added by Gavaskar 28-10-2017  Sales Order ,Estimate SMS Start
        public StringBuilder SalesOrderEstimateBodyBuilder(SalesOrderEstimate salesOrderEstimate)
        {
            var newbody = new StringBuilder();

            if (salesOrderEstimate.SalesOrderEstimateType == 1)
            {
                #region Sales Order

                newbody.AppendFormat("Dear {0},", salesOrderEstimate.Patient.Name).AppendLine();
                newbody.AppendFormat("{0} is pleased to receive the sales order for the following details(Order No : {1}). ",
                 salesOrderEstimate.Instance.Name, salesOrderEstimate.OrderEstimateId).AppendLine();
                var i = 1;
                foreach (var item in salesOrderEstimate.SalesOrderEstimateItem)
                {
                    if (item.IsOrder == null || item.IsOrder == 0)
                    {
                        if (item.Product.Name != null)
                        {
                            item.ProductName = item.Product.Name;
                        }

                        newbody.AppendFormat("{0}. {1} - {2}", i++, item.ProductName, item.Quantity).AppendLine();
                    }

                }
                newbody.AppendFormat("Pharmacy Instance Name: {0}", salesOrderEstimate.Instance.Name).AppendLine();
                newbody.AppendFormat("Address: {0}", salesOrderEstimate.Instance.FullAddress).AppendLine();
                newbody.AppendFormat("Phone Number: {0}.", salesOrderEstimate.Instance.Phone).AppendLine();
                newbody.AppendLine("Powered By hCue");

                #endregion
            }
            else if (salesOrderEstimate.SalesOrderEstimateType == 2)
            {
                #region Sales Estimate

                newbody.AppendFormat("Dear {0},", salesOrderEstimate.Patient.Name).AppendLine();
                newbody.AppendFormat("{0} is pleased to give an sales estimate for the following details(Estimate No : {1}). ",
                      salesOrderEstimate.Instance.Name, salesOrderEstimate.OrderEstimateId).AppendLine();

                var i = 1;
                foreach (var item in salesOrderEstimate.SalesEstimateItem)
                {
                    if (item.IsOrder == null || item.IsOrder == 0)
                    {
                        if (item.Product.Name != null)
                        {
                            item.ProductName = item.Product.Name;
                        }
                        newbody.AppendFormat("{0}. {1} - {2}", i++, item.ProductName, item.Quantity).AppendLine();
                    }

                }
                newbody.AppendFormat("Pharmacy Instance Name: {0}", salesOrderEstimate.Instance.Name).AppendLine();
                newbody.AppendFormat("Address: {0}", salesOrderEstimate.Instance.FullAddress).AppendLine();
                newbody.AppendFormat("Phone Number: {0}.", salesOrderEstimate.Instance.Phone).AppendLine();
                newbody.AppendLine("Powered By hCue");

                #endregion
            }


            return newbody;
        }
        // Added by Gavaskar 28-10-2017  Sales Order ,Estimate SMS End

    }
}
