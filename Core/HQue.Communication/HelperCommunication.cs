﻿using System.Threading.Tasks;
using HQue.Communication.Email;
using HQue.Communication.Sms;
using HQue.DataAccess.Inventory;
using Utilities.Helpers;
using System;

namespace HQue.Communication
{
    public class HelperCommunication
    {
        private readonly SalesDataAccess _salesDataAccess;
        private readonly VendorOrderDataAccess _vendorOrderDataAccess;
        private readonly SmsSender _smsSender;
        private readonly EmailSender _emailSender;
        private readonly ConfigHelper _configHelper;

        public HelperCommunication(SalesDataAccess salesDataAccess, VendorOrderDataAccess vendorOrderDataAccess,
            SmsSender smsSender, EmailSender emailSender, ConfigHelper configHelper)
        {
            _salesDataAccess = salesDataAccess;
            _vendorOrderDataAccess = vendorOrderDataAccess;
            _smsSender = smsSender;
            _emailSender = emailSender;
            _configHelper = configHelper;
        }

        public async Task<bool> SendSales(string id)
        {
            var sales = await _salesDataAccess.GetById(id);

            var url = $"{_configHelper.UrlBuilder.InvoiceUrl}{sales.Id}";

            if (sales.SendSms ?? false)
                _smsSender.Send(sales.Mobile, 0, _smsSender.SellsBodyBuilder(sales));
            if (sales.SendEmail ?? false)
                _emailSender.Send(sales.Email, 1, _emailSender.SellsBodyBuilder(sales, url));

            return true;
        }

        public async Task<bool> SendVenderOrder(string id)
        {
            var vendorOrder = await _vendorOrderDataAccess.GetById(id);
            vendorOrder.OrderId = Convert.ToString(vendorOrder.Prefix) + vendorOrder.OrderId; //Prefix added by Poongodi on 21/06/2017
            _smsSender.Send(vendorOrder.Vendor.Mobile, 0, _smsSender.VendorOrderBodyBuilder(vendorOrder));
            _emailSender.Send(vendorOrder.Vendor.Email, 2, _emailSender.VendorOrderBodyBuilder(vendorOrder));

            return true;
        }
    }
}
