﻿using System.Collections.Generic;
using System.Text;
using System.Threading;
using HQue.Communication.Interface;
using HQue.Contract.Infrastructure.Inventory;
using Utilities.Helpers;
using Utilities.Logger;
using Amazon;
using Amazon.SimpleEmail;
using Amazon.SimpleEmail.Model;
using MimeKit;
using System.IO;
using MailKit.Net.Smtp;
using HQue.Contract.Infrastructure.Dashboard;
using Microsoft.AspNetCore.Hosting;
using System;
using System.Linq;
using System.Globalization;
using HQue.Contract.Infrastructure.UserAccount;

namespace HQue.Communication.Email
{
    public class EmailSender : IEmailSender
    {
        private readonly ConfigHelper _configHelper;
        private readonly IHostingEnvironment _environment;

        private const string FromAddress = "no-reply@hcue.co";
        //private const string AccessKeyId = "AKIAJFTECBP67XWKU47A";
        //private const string SecretAccessKey = "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";
        private const string AccessKeyId = "AKIAILO2W5O6D3JZY6AA";
        private const string SecretAccessKey = "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";
        private static readonly RegionEndpoint Region = RegionEndpoint.USWest2;

        public const int ThankyouCustomer = 1;
        public const int VendorOrder = 2;
        public const int ForgotPassword = 3;
        public const int Register = 4;
        public const int AllowViewStock = 5;
        public const int DashboardDailyMail = 10;
        public const int DashboardDailyManagementMail = 11;
        public const int ExternalPasswordReset = 12;
        public const int DashboardDailyStock = 13;
        public const int OnlineBlock = 14;
        public const int DashboardPlusPharmacyDailyMail = 15;
        // Added by Gavaskar 28-10-2017  Sales Order ,Estimate Email Start
        public const int SalesOrder = 16;
        public const int SalesEstimate = 17;
        // Added by Gavaskar 28-10-2017  Sales Order ,Estimate Email End
        public const int LoginMail = 18;
        public const int ErrorLogMail = 19;

        public EmailSender(ConfigHelper configHelper, IHostingEnvironment environment)
        {
            _configHelper = configHelper;
            _environment = environment;
            OverrideConfig = false;
        }

        public bool OverrideConfig { get; set; }
        public object Integer { get; private set; }

        public bool Send(string toAddress, int templateId, params object[] templateParams)
        {
            if (string.IsNullOrEmpty(toAddress))
            {
                LogAndException.WriteToLogger("Invalid Email ID", ErrorLevel.Debug);
                return false;
            }

            var message = PrepareMessage(templateId, templateParams);

            if (_configHelper.AppConfig.SendEmail || OverrideConfig)
            {
                //using (var client = new AmazonSimpleEmailServiceClient(AccessKeyId, SecretAccessKey, Region))
                //{
                //    var sendRequest = new SendEmailRequest
                //    {
                //        Source = FromAddress,
                //        Destination = new Destination { ToAddresses = new List<string> { toAddress } },
                //        Message = new Message
                //        {
                //            Subject = new Content(message[0]),
                //            Body = new Body { Html = new Content(message[1]) }
                //        }
                //    };

                //    SendMail(client,sendRequest);

                //    return true;
                //}

                var sendRequest = new SendEmailRequest
                {
                    Source = FromAddress,
                    Destination = new Destination { ToAddresses = new List<string> { toAddress } },
                    Message = new Message
                    {
                        Subject = new Content(message[0]),
                        Body = new Body { Html = new Content(message[1]) }
                    }
                };

                var emailMessage = new MimeMessage();

                emailMessage.From.Add(new MailboxAddress("hCue", sendRequest.Source));
                emailMessage.To.Add(new MailboxAddress("", sendRequest.Destination.ToAddresses[0]));
                emailMessage.Subject = sendRequest.Message.Subject.Data;
                emailMessage.Body = new TextPart("html") { Text = message[1] };

                using (var client = new SmtpClient())
                {
                    //if (HelperCommunication.CheckForInternetConnection())
                    //{
                        client.Connect("mail.smtp2go.com", 2525, false);

                        // Note: only needed if the SMTP server requires authentication
                        client.Authenticate("hcue.health1@gmail.com", "mzPN3N3WGarh");

                        client.Send(emailMessage);
                        client.Disconnect(true);

                        //client.ConnectAsync("mail.smtp2go.com", 2525, SecureSocketOptions.None).ConfigureAwait(false);
                        //client.SendAsync(emailMessage).ConfigureAwait(false);
                        //client.DisconnectAsync(true).ConfigureAwait(false);
                    //}
                }
                return true;
            }

            LogAndException.WriteToLogger($"NotSent:{message}", ErrorLevel.Debug);
            return false;
        }

        private static async void SendMail(AmazonSimpleEmailServiceClient client, SendEmailRequest sendRequest)
        {
            // Define the cancellation token.
            var source = new CancellationTokenSource();
            var token = source.Token;
            var response = await client.SendEmailAsync(sendRequest, token);
        }

        private string[] PrepareMessage(int templateId, params object[] templateParams)
        {
            var message = new string[2];

            switch (templateId)
            {
                case ThankyouCustomer:
                    message[0] = "hCue Invoice";
                    message[1] = templateParams[0].ToString();
                    break;
                case VendorOrder:
                    message[0] = "hCue Order Details";
                    message[1] = templateParams[0].ToString();
                    break;
                case ForgotPassword:
                    message[0] = "hCue Forgot Password";
                    message[1] = string.Format("<p>Dear Member,<br/><br/>As per your request, we have sent you the password reset url. Click the url to reset your password:<br/><br/><a href='{0}'>{0}</a><br/><br/>Thank you</p><br/><br/><p>Powered By hCue</p>", templateParams);
                    break;
                case Register:
                    message[0] = "hCue";
                    message[1] = string.Format("<p>Dear Member,<br/><br/>Click the url to activate hCue account and set your new password:<br/><br/><a href='{0}'>{0}</a><br/><br/>Thank you</p><br/><br/><p>Powered By hCue</p>", templateParams);
                    break;
                case AllowViewStock:
                    message[0] = "hCue";
                    message[1] = string.Format("<p>Dear Member,<br/><br/>Click the url to activate hCue account and set your new password:<br/><br/><a href='{0}'>{0}</a><br/><br/>Thank you</p><br/><br/><p>Powered By hCue</p>", templateParams);
                    break;
                case DashboardDailyMail:
                    message[0] = "Dashboard Daily Report";
                    message[1] = templateParams[0].ToString();
                    break;
                case DashboardDailyManagementMail:
                    message[0] = "hCue Pharmacy Management Dashboard";
                    message[1] = templateParams[0].ToString();
                    break;

                case ExternalPasswordReset:
                    message[0] = "Greetings from hCue";
                    message[1] = templateParams[0].ToString();
                    break;
                case DashboardDailyStock:
                    message[0] = "Dashboard Daily Stock Report";
                    message[1] = templateParams[0].ToString();
                    break;

                case OnlineBlock:
                    message[0] = "Online Block/Unblock";
                    message[1] = templateParams[0].ToString();
                    break;
                case DashboardPlusPharmacyDailyMail:
                    message[0] = "hCue Dashboard Daily Report"; // Modified by Gavaskar 05-10-2017
                    message[1] = templateParams[0].ToString();
                    break;
                // Added by Gavaskar 28-10-2017  Sales Order ,Estimate Email Start
                case SalesOrder:
                    message[0] = "hCue Sales Order Details";
                    message[1] = templateParams[0].ToString();
                    break;
                case SalesEstimate:
                    message[0] = "hCue Sales Estimate Details";
                    message[1] = templateParams[0].ToString();
                    break;
                    // Added by Gavaskar 28-10-2017  Sales Order ,Estimate Email End
                    
                case LoginMail:
                    message[0] = "hCue Login Daily Report"; // Modified by nandhini 16.11.17
                    message[1] = templateParams[0].ToString();
                    break;
                case ErrorLogMail:
                    message[0] = (_configHelper.AppConfig.OfflineMode ? "hCue Pharmacy Offline Issue Details" : "hCue Pharmacy Online Issue Details");
                    message[1] = templateParams[0].ToString();
                    break;
            }

            return message;
        }

        public StringBuilder SellsBodyBuilder(Sales sales, params object[] templateParams)
        {
            var newbody = new StringBuilder();

            //#region Sell
            //newbody.AppendFormat("<table>");
            //newbody.AppendFormat("<tr><td>");
            //newbody.AppendFormat("<p>Dear {0},</p>", sales.Patient.Name);
            //newbody.AppendFormat("<p>Thank you for shopping at {0}</p>", sales.Instance.Name);
            //newbody.AppendFormat("<p>Please find the invoice of Rs.{0:0.00} for the following purchase.</p>", sales.RoundOff);
            //newbody.AppendFormat("<br/>");

            ////Arrange Css as per requirement
            //newbody.Append("<table width=600px border=1 cellspacing=2 cellpadding=2 align=center bgcolor=White dir=ltr rules=all style=border-width: thin; border-style: solid; line-height: normal; vertical-align: baseline; text-align: center; font-family: Calibri; font-size: medium; font-weight: normal; font-style: normal; font-variant: normal; color: #000000; list-style-type: none;>");
            //newbody.Append("<tr>");
            //newbody.Append("<td>");
            //newbody.Append("<table width=600px border=1 cellspacing=2 cellpadding=2 align=center bgcolor=White dir=ltr rules=all style=border-width: thin; border-style: solid; line-height: normal; vertical-align: baseline; text-align: center; font-family: Calibri; font-size: medium; font-weight: normal; font-style: normal; font-variant: normal; color: #000000; list-style-type: none;>");
            //#region Html Table Creation
            ////Heading
            //newbody.Append("<tr style=background-color:#ECECEC;font-weight:bold>");
            //newbody.Append("<td>");
            //newbody.Append("Sl No");
            //newbody.Append("</td>");
            //newbody.Append("<td>");
            //newbody.Append("Product Name");
            //newbody.Append("</td>");
            //newbody.Append("<td>");
            //newbody.Append("Quantity");
            //newbody.Append("</td>");
            //newbody.Append("<td>");
            //newbody.Append("Price");
            //newbody.Append("</td>");
            //newbody.Append("</tr>");
            //var i = 0;
            //foreach (var item in sales.SalesItem)
            //{
            //    //Rows
            //    newbody.Append("<tr>");
            //    newbody.Append("<td>");
            //    newbody.Append(++i);
            //    newbody.Append("</td>");
            //    newbody.Append("<td>");
            //    newbody.Append(item.ProductStock.Product.Name);
            //    newbody.Append("</td>");
            //    newbody.Append("<td>");
            //    newbody.Append(string.Format("{0:0}", item.Quantity));
            //    newbody.Append("</td>");
            //    newbody.Append("<td>");
            //    newbody.Append(string.Format("{0:0.00}", item.Total));
            //    newbody.Append("</td>");
            //    newbody.Append("</tr>");
            //}
            //#endregion
            //newbody.Append("</table>");
            //newbody.Append("</td>");
            //newbody.Append("</tr>");
            //newbody.Append("</table>");

            //newbody.AppendFormat("<br/>");
            //newbody.AppendFormat("<p>Total {0:0.00} Rupees</p>", sales.RoundOff);
            //newbody.AppendFormat("<br/>");
            //newbody.AppendFormat("<p>For detail invoice, Please <a href='{0}'>click here</a></p>", templateParams[0]);
            //newbody.AppendFormat("<br/>");
            //newbody.AppendFormat("<br/>");
            //newbody.AppendFormat("<p>Powered By hCue</p>");
            //newbody.AppendFormat("</td></tr></table>");
            //#endregion

            string file = "";
            // if (sales.SalesReturnItem.Count() > 0)
            if (sales.SalesReturn.SalesReturnItem.Count() > 0)
            {
                file = @"Views/Sales/SalesReturnMail.html";
            }
            else
            {
                file = @"Views/Sales/SalesMail.html";
            }

            string str = Path.GetFullPath(file);
            var fileContents = System.IO.File.ReadAllText(str);
            /*Prefix added by Poongodi on 15/06/2017*/
            CultureInfo ci = new CultureInfo("en-IN");
            DateTime dt = DateTime.Now;

            fileContents = fileContents.Replace("{{P_NAME}}", sales.Name);
            fileContents = fileContents.Replace("{{INSTANCE_NAME}}", sales.Instance.Name);
            fileContents = fileContents.Replace("{{DOCTOR_NAME}}", sales.DoctorName);
            fileContents = fileContents.Replace("{{BILL_NO}}", Convert.ToString(sales.Prefix) + sales.InvoiceSeries + " " + sales.InvoiceNo);
            fileContents = fileContents.Replace("{{PATIENT_NAME}}", sales.Name);
            fileContents = fileContents.Replace("{{B_DATE}}", Convert.ToDateTime(sales.InvoiceDate).ToString("dd/MM/yyyy"));
            fileContents = fileContents.Replace("{{ADDRESS}}", sales.Address);

            //Added by Sarubala on 28-10-17
            if(sales.PaymentType == "Multiple" && sales.CashType == "MultiplePayment" && sales.SalesPayments.Count == 1 && sales.SalesPayments.FirstOrDefault().PaymentInd == 5)
            {
                fileContents = fileContents.Replace("{{B_TYPE}}", "eWallet");
            }
            else
            {
                fileContents = fileContents.Replace("{{B_TYPE}}", sales.CashType.ToLower() == "credit" ? sales.CashType : sales.PaymentType); //Cashtype added by Poongodi on 27/07/2017
            }
            
            fileContents = fileContents.Replace("{{PH_NO}}", sales.Mobile);
            fileContents = fileContents.Replace("{{ID_NO}}", sales.EmpID);
            
            var SalesItemList = "";
            var SalesReturnItemList = "";
            //decimal ReturnTotal = 0;
            //decimal ReturnDiscount = 0;            
            decimal? cgstTotal = 0;
            decimal? itemGSTTotalValue = 0;

            //foreach (var item in sales.SalesItem)
            foreach (var item in sales.SalesItem.Where(x => x.IsCreated))
            {

                SalesItemList += "<tr style=''>";

                SalesItemList += string.Format("<td style=text-align:left;padding:5px;line-height:20px;font-size:13px;font-family:Trebuchet MS,Arial,Helvetica,sans-serif;color:#000000;border-bottom:1px solid #f2f2f2 !important;>{0}</td>", item.ProductStock.Product.Name.ToUpper());
                if (item.ProductStock.Product.Manufacturer == null || item.ProductStock.Product.Manufacturer == "")
                {
                    SalesItemList += string.Format("<td style=text-align:left;padding:5px;line-height:20px;font-size:13px;font-family:Trebuchet MS,Arial,Helvetica,sans-serif;color: #000000;border-bottom:1px solid #f2f2f2 !important;>{0}</td>", "-");
                }
                else
                {
                    string[] arr = item.ProductStock.Product.Manufacturer.Split(' ');
                    SalesItemList += string.Format("<td style=text-align:left;padding:5px;line-height:20px;font-size:13px;font-family:Trebuchet MS,Arial,Helvetica,sans-serif;color: #000000;border-bottom:1px solid #f2f2f2 !important;>{0}</td>", arr[0]);
                }

                SalesItemList += string.Format("<td style=text-align:right;padding:5px;line-height:20px;font-size:13px;font-family:Trebuchet MS,Arial,Helvetica,sans-serif;color: #000000;border-bottom:1px solid #f2f2f2 !important;>{0}</td>", item.BatchNo);

                SalesItemList += string.Format("<td style=text-align:center;padding:5px;line-height:20px;font-size:13px;font-family:Trebuchet MS,Arial,Helvetica,sans-serif;color: #000000;border-bottom:1px solid #f2f2f2 !important;>{0}</td>", item.ExpireDate.ToFormatExp());

                SalesItemList += string.Format("<td style=text-align:right;padding:5px;line-height:20px;font-size:13px;font-family:Trebuchet MS,Arial,Helvetica,sans-serif;color: #000000;border-bottom:1px solid #f2f2f2 !important;>{0}</td>", string.Format("{0:0}", item.Quantity));
                //Added by Sarubala to include GST 06-07-2017 - start
                if(sales.TaxRefNo == 1)
                {
                    if (sales.Instance.Gstselect != true)
                    {
                        SalesItemList += string.Format("<td style=text-align:right;padding:5px;line-height:20px;font-size:13px;font-family:Trebuchet MS,Arial,Helvetica,sans-serif;color: #000000;border-bottom:1px solid #f2f2f2 !important;>{0}</td>", string.Format("{0:0.00}", ((item.SellingPrice * 100) / (item.GstTotal + 100))));
                        fileContents = fileContents.Replace("{{TAX_TYPE}}", "GST");
                        fileContents = fileContents.Replace("{{isGSTDisplay}}", "block");
                        fileContents = fileContents.Replace("{{isGSTDisplay_Sales_Header}}", "none");
                        fileContents = fileContents.Replace("{{isGSTDisplay_Sales_Footer}}", "none");
                    }
                    else
                    {
                        SalesItemList += string.Format("<td style=text-align:right;padding:5px;line-height:20px;font-size:13px;font-family:Trebuchet MS,Arial,Helvetica,sans-serif;color: #000000;border-bottom:1px solid #f2f2f2 !important;>{0}</td>", string.Format("{0:0.00}", item.SellingPrice));
                        fileContents = fileContents.Replace("{{TAX_TYPE}}", "");
                        fileContents = fileContents.Replace("{{isGSTDisplay}}", "none");
                        fileContents = fileContents.Replace("{{isGSTDisplay_Sales_Header}}", "block");
                        fileContents = fileContents.Replace("{{isGSTDisplay_Sales_Footer}}", "block");
                    }
                }
                else
                {
                    SalesItemList += string.Format("<td style=text-align:right;padding:5px;line-height:20px;font-size:13px;font-family:Trebuchet MS,Arial,Helvetica,sans-serif;color: #000000;border-bottom:1px solid #f2f2f2 !important;>{0}</td>", string.Format("{0:0.00}", ((item.SellingPrice * 100) / (item.VAT + 100))));

                    fileContents = fileContents.Replace("{{TAX_TYPE}}", "VAT");
                }
                
                if(sales.TaxRefNo == 1)
                {
                    if (sales.Instance.Gstselect != true)
                    {
                        SalesItemList += string.Format("<td style=text-align:right;padding:5px;line-height:20px;font-size:13px;font-family:Trebuchet MS,Arial,Helvetica,sans-serif;color: #000000;border-bottom:1px solid #f2f2f2 !important;>{0}</td>", string.Format("{0:0.00}", item.SellingPrice - ((item.SellingPrice * 100) / (item.GstTotal + 100))));

                        cgstTotal = cgstTotal + ((item.SellingPrice - ((item.SellingPrice * 100) / (item.GstTotal + 100))) * item.Quantity);
                        decimal? getGstPercent = item.GstTotal + 100;
                        decimal? SalesItemDiscount = 0;
                        if (item.Discount== null)
                        {
                            item.Discount = 0;
                        }
                        if (item.Discount > 0)
                            SalesItemDiscount = ((item.Discount / 100) * item.Total);
                        else
                            SalesItemDiscount = ((sales.Discount / 100) * item.Total);

                        var ActualValue = (item.Total - SalesItemDiscount);
                        itemGSTTotalValue += ActualValue - ((ActualValue * 100) / getGstPercent);
                    }
                        
                }
                else
                {
                    SalesItemList += string.Format("<td style=text-align:right;padding:5px;line-height:20px;font-size:13px;font-family:Trebuchet MS,Arial,Helvetica,sans-serif;color: #000000;border-bottom:1px solid #f2f2f2 !important;>{0}</td>", string.Format("{0:0.00}", item.SellingPrice - ((item.SellingPrice * 100) / (item.VAT + 100))));
                }
                //Added by Sarubala to include GST 06-07-2017 - end

                SalesItemList += string.Format("<td style=text-align:right;padding:5px;line-height:20px;font-size:13px;font-family:Trebuchet MS,Arial,Helvetica,sans-serif;color: #000000;border-bottom:1px solid #f2f2f2 !important;>{0}</td>", string.Format("{0:0.00}", item.Total));

                SalesItemList += "</tr>";
            }

            fileContents = fileContents.Replace("{{INVOICE_DETAILS}}", SalesItemList);

            var SalesNet = sales.PurchasedTotal - sales.DiscountInValue;

            if (sales.Instance.Gstselect != true)
            {
                fileContents = fileContents.Replace("{{isCGST_SGST_Display}}", "block");
                fileContents = fileContents.Replace("{{CGST}}", string.Format("{0:0.00}", itemGSTTotalValue / 2)); //cgstTotal / 2
                fileContents = fileContents.Replace("{{SGST}}", string.Format("{0:0.00}", itemGSTTotalValue / 2));  //cgstTotal / 2
            }
            else
            {
                fileContents = fileContents.Replace("{{isCGST_SGST_Display}}", "none");
                fileContents = fileContents.Replace("{{CGST}}", "");  
                fileContents = fileContents.Replace("{{SGST}}", "");   
            }


            //fileContents = fileContents.Replace("{{TOT}}", string.Format("{0:0.00}", sales.PurchasedTotal));
            //fileContents = fileContents.Replace("{{DISC}}", string.Format("{0:0.00}", sales.DiscountInValue));
            //fileContents = fileContents.Replace("{{NET_AMT}}", string.Format("{0:0.00}", SalesNet));
            fileContents = fileContents.Replace("{{TOT}}", string.Format("{0:0.00}", sales.SalesItemAmount));
            fileContents = fileContents.Replace("{{DISC}}", string.Format("{0:0.00}", sales.TotalDiscountValue));
            fileContents = fileContents.Replace("{{NET_AMT}}", string.Format("{0:0.00}", sales.SaleAmount - sales.RoundoffSaleAmount));

            //if (sales.SalesReturnItem.Count() > 0)
            if (sales.SalesReturn.SalesReturnItem.Count() > 0)
            {
                decimal? returnCgstTotal = 0;
                decimal? rtnGSTTotalValue = 0;
                foreach (var item in sales.SalesReturn.SalesReturnItem)
                {

                    SalesReturnItemList += "<tr style=''>";

                    SalesReturnItemList += string.Format("<td style=text-align:left;padding:5px;line-height:20px;font-size:13px;font-family:Trebuchet MS,Arial,Helvetica,sans-serif;color:#000000;border-bottom:1px solid #f2f2f2 !important;>{0}</td>", item.ProductStock.Product.Name.ToUpper());
                    if (item.ProductStock.Product.Manufacturer == null || item.ProductStock.Product.Manufacturer == "")
                    {
                        SalesReturnItemList += string.Format("<td style=text-align:left;padding:5px;line-height:20px;font-size:13px;font-family:Trebuchet MS,Arial,Helvetica,sans-serif;color: #000000;border-bottom:1px solid #f2f2f2 !important;>{0}</td>", "-");
                    }
                    else
                    {
                        string[] arr = item.ProductStock.Product.Manufacturer.Split(' ');
                        SalesReturnItemList += string.Format("<td style=text-align:left;padding:5px;line-height:20px;font-size:13px;font-family:Trebuchet MS,Arial,Helvetica,sans-serif;color: #000000;border-bottom:1px solid #f2f2f2 !important;>{0}</td>", arr[0]);
                    }

                    SalesReturnItemList += string.Format("<td style=text-align:right;padding:5px;line-height:20px;font-size:13px;font-family:Trebuchet MS,Arial,Helvetica,sans-serif;color: #000000;border-bottom:1px solid #f2f2f2 !important;>{0}</td>", item.BatchNo);

                    SalesReturnItemList += string.Format("<td style=text-align:center;padding:5px;line-height:20px;font-size:13px;font-family:Trebuchet MS,Arial,Helvetica,sans-serif;color: #000000;border-bottom:1px solid #f2f2f2 !important;>{0}</td>", item.ExpireDate.ToFormatExp());

                    SalesReturnItemList += string.Format("<td style=text-align:right;padding:5px;line-height:20px;font-size:13px;font-family:Trebuchet MS,Arial,Helvetica,sans-serif;color: #000000;border-bottom:1px solid #f2f2f2 !important;>{0}</td>", string.Format("{0:0}", item.Quantity));

                    if(sales.TaxRefNo == 1)
                    {
                        if (sales.Instance.Gstselect != true)
                        {
                            SalesReturnItemList += string.Format("<td style=text-align:right;padding:5px;line-height:20px;font-size:13px;font-family:Trebuchet MS,Arial,Helvetica,sans-serif;color: #000000;border-bottom:1px solid #f2f2f2 !important;>{0}</td>", string.Format("{0:0.00}", ((item.SellingPrice * 100) / (item.GstTotal + 100))));
                            fileContents = fileContents.Replace("{{isGSTReturnDisplay}}", "block");
                            fileContents = fileContents.Replace("{{isGSTDisplay_Header}}", "none");
                            fileContents = fileContents.Replace("{{isGSTDisplay_Footer}}", "none");
                        }
                        else
                        {
                            SalesReturnItemList += string.Format("<td style=text-align:right;padding:5px;line-height:20px;font-size:13px;font-family:Trebuchet MS,Arial,Helvetica,sans-serif;color: #000000;border-bottom:1px solid #f2f2f2 !important;>{0}</td>", string.Format("{0:0.00}", item.SellingPrice));
                            fileContents = fileContents.Replace("{{isGSTReturnDisplay}}", "none");
                            fileContents = fileContents.Replace("{{isGSTDisplay_Header}}", "block");
                            fileContents = fileContents.Replace("{{isGSTDisplay_Footer}}", "block");
                        }
                                               
                    }
                    else
                    {
                        SalesReturnItemList += string.Format("<td style=text-align:right;padding:5px;line-height:20px;font-size:13px;font-family:Trebuchet MS,Arial,Helvetica,sans-serif;color: #000000;border-bottom:1px solid #f2f2f2 !important;>{0}</td>", string.Format("{0:0.00}", ((item.SellingPrice * 100) / (item.VAT + 100))));
                    }
                    
                    if(sales.TaxRefNo == 1)
                    {
                        if (sales.Instance.Gstselect != true)
                        {
                            SalesReturnItemList += string.Format("<td style=text-align:right;padding:5px;line-height:20px;font-size:13px;font-family:Trebuchet MS,Arial,Helvetica,sans-serif;color: #000000;border-bottom:1px solid #f2f2f2 !important;>{0}</td>", string.Format("{0:0.00}", item.SellingPrice - ((item.SellingPrice * 100) / (item.GstTotal + 100))));

                            returnCgstTotal = returnCgstTotal + ((item.SellingPrice - ((item.SellingPrice * 100) / (item.GstTotal + 100))) * item.Quantity);
                            decimal? getGstPercent = item.GstTotal + 100;
                            decimal? SalesItemDiscount = ((item.Discount / 100) * item.Total);

                            var ActualValue = (item.Total - SalesItemDiscount);
                            rtnGSTTotalValue += ActualValue - ((ActualValue * 100) / getGstPercent);
                        }
                            
                    }
                    else
                    {
                        SalesReturnItemList += string.Format("<td style=text-align:right;padding:5px;line-height:20px;font-size:13px;font-family:Trebuchet MS,Arial,Helvetica,sans-serif;color: #000000;border-bottom:1px solid #f2f2f2 !important;>{0}</td>", string.Format("{0:0.00}", item.SellingPrice - ((item.SellingPrice * 100) / (item.VAT + 100))));
                    }
                   
                    SalesReturnItemList += string.Format("<td style=text-align:right;padding:5px;line-height:20px;font-size:13px;font-family:Trebuchet MS,Arial,Helvetica,sans-serif;color: #000000;border-bottom:1px solid #f2f2f2 !important;>{0}</td>", string.Format("{0:0.00}", item.Total));

                    //ReturnTotal += item.Total as decimal? ?? 0;
                    //ReturnDiscount += ((item.Discount / 100) * item.Total) as decimal? ?? 0;

                    SalesReturnItemList += "</tr>";
                }

                fileContents = fileContents.Replace("{{INVOICE_RETURN_DETAILS}}", SalesReturnItemList);

                //var ReturnNet = ReturnTotal - ReturnDiscount;
                if (sales.Instance.Gstselect != true)
                {
                    fileContents = fileContents.Replace("{{RETURN_CGST}}", string.Format("{0:0.00}", (itemGSTTotalValue / 2) - (rtnGSTTotalValue / 2)));  //returnCgstTotal / 2
                    fileContents = fileContents.Replace("{{RETURN_SGST}}", string.Format("{0:0.00}", (itemGSTTotalValue / 2) - (rtnGSTTotalValue / 2)));   //returnCgstTotal / 2
                    fileContents = fileContents.Replace("{{isCGST_SGST_Return_Display}}", "block");
                }
                else
                {
                    fileContents = fileContents.Replace("{{RETURN_CGST}}", "");  
                    fileContents = fileContents.Replace("{{RETURN_SGST}}", "");
                    fileContents = fileContents.Replace("{{isCGST_SGST_Return_Display}}", "none");
                }
                    

                //fileContents = fileContents.Replace("{{RETURN_TOT}}", string.Format("{0:0.00}", ReturnTotal));
                //fileContents = fileContents.Replace("{{RETURN_DISC}}", string.Format("{0:0.00}", ReturnDiscount));
                //fileContents = fileContents.Replace("{{RETURN_NET_AMT}}", string.Format("{0:0.00}", ReturnNet));
                fileContents = fileContents.Replace("{{RETURN_TOT}}", string.Format("{0:0.00}", sales.SalesReturn.ReturnItemAmount));
                fileContents = fileContents.Replace("{{RETURN_DISC}}", string.Format("{0:0.00}", sales.SalesReturn.TotalDiscountValue));
                fileContents = fileContents.Replace("{{RETURN_NET_AMT}}", string.Format("{0:0.00}", sales.SalesReturn.NetAmount- sales.SalesReturn.RoundOffNetAmount));
            }

            fileContents = fileContents.Replace("{{ROFF}}", string.Format("{0:0.00}", sales.RoundoffNetAmount));
            fileContents = fileContents.Replace("{{PAY_NET_AMT}}", string.Format("{0:0.00}", sales.NetAmount));
            newbody.Append(fileContents);


            newbody.AppendFormat("<p>For detail invoice, Please <a href='{0}'>click here</a></p>", templateParams[0]);

            return newbody;
        }
        public StringBuilder VendorOrderBodyBuilder(VendorOrder vendorOrder)
        {
            var newbody = new StringBuilder();

            #region Vendor Order
            newbody.AppendFormat("<table>");
            newbody.AppendFormat("<tr><td>");
            newbody.AppendFormat("<p>Dear {0},</p>", vendorOrder.Vendor.Name);
            newbody.AppendFormat("<p>We {0} are pleased to place an order for the following details(Order Id : {1}).</p>", vendorOrder.Instance.Name, vendorOrder.OrderId);
            newbody.AppendFormat("<br/>");

            //Arrange Css as per requirement
            newbody.Append("<table width=600px border=1 cellspacing=2 cellpadding=2 align=center bgcolor=White dir=ltr rules=all style=border-width: thin; border-style: solid; line-height: normal; vertical-align: baseline; text-align: center; font-family: Calibri; font-size: medium; font-weight: normal; font-style: normal; font-variant: normal; color: #000000; list-style-type: none;>");
            newbody.Append("<tr>");
            newbody.Append("<td>");
            newbody.Append("<table width=600px border=1 cellspacing=2 cellpadding=2 align=center bgcolor=White dir=ltr rules=all style=border-width: thin; border-style: solid; line-height: normal; vertical-align: baseline; text-align: center; font-family: Calibri; font-size: medium; font-weight: normal; font-style: normal; font-variant: normal; color: #000000; list-style-type: none;>");
            #region Html Table Creation
            //Heading
            newbody.Append("<tr style=background-color:#ECECEC;font-weight:bold>");
            newbody.Append("<td>");
            newbody.Append("Sl No");
            newbody.Append("</td>");
            newbody.Append("<td>");
            newbody.Append("Product Name");
            newbody.Append("</td>");
            newbody.Append("<td>");
            newbody.Append("Quantity");
            newbody.Append("</td>");
            newbody.Append("<td>");
            newbody.Append("Free Quantity");
            newbody.Append("</td>");
            newbody.Append("<td>");
            newbody.Append("Mrp/Strip");
            newbody.Append("</td>");
            newbody.Append("<td>");
            newbody.Append("GST %");
            newbody.Append("</td>");
            newbody.Append("<td>");
            newbody.Append("Strip Price");
            newbody.Append("</td>");
            newbody.Append("</tr>");
            var i = 0;
            foreach (var item in vendorOrder.VendorOrderItem)
            {
                //Rows
                newbody.Append("<tr>");
                newbody.Append("<td>");
                newbody.Append(++i);
                newbody.Append("</td>");
                newbody.Append("<td>");
                newbody.Append(item.Product.Name);
                newbody.Append("</td>");
                newbody.Append("<td>");
                newbody.Append(string.Format("{0:0}", item.Quantity));
                newbody.Append("</td>");
                newbody.Append("<td>");
                newbody.Append(string.Format("{0:0}", item.FreeQty));
                newbody.Append("</td>");
                newbody.Append("<td>");
                newbody.Append(string.Format("{0:0}", item.SellingPrice));
                newbody.Append("</td>");
                newbody.Append("<td>");
                newbody.Append(string.Format("{0:0}", item.GstTotal));
                newbody.Append("</td>");
                newbody.Append("<td>");
                newbody.Append(string.Format("{0:0}", item.PurchasePrice));
                newbody.Append("</td>");
                newbody.Append("</tr>");
            }
            #endregion
            newbody.Append("</table>");
            newbody.Append("</td>");
            newbody.Append("</tr>");
            newbody.Append("</table>");

            newbody.AppendFormat("<br/>");
            newbody.AppendFormat("<p>Pharmacy Instance Name: {0}</p>", vendorOrder.Instance.Name);
            newbody.AppendFormat("<p>Address: {0}</p>", vendorOrder.Instance.Address);
            newbody.AppendFormat("<p>Phone Number: {0}</p>", vendorOrder.Instance.Phone);
            newbody.AppendFormat("<br/>");
            newbody.AppendFormat("<br/>");
            newbody.AppendFormat("<p>Powered By hCue</p>");
            newbody.AppendFormat("</td></tr></table>");
            #endregion
            return newbody;
        }

        public StringBuilder DashboardBodyBuilder(IEnumerable<NameValue> result, IEnumerable<NameValue> Marginresult, IEnumerable<NameValue> PatientVisit, IEnumerable<NameValue> BouncedItems, params object[] templateParams)
        {
            var newbody = new StringBuilder();
            string file = @"Views/DashboardDailyMail.html";
            string str = Path.GetFullPath(file);
            var fileContents = System.IO.File.ReadAllText(str);
            // var fileContents = System.IO.File.ReadAllText(@"C:\DashboardDailyMail.html");
            //var fileContents = System.IO.File.ReadAllText("~/Views/Dashboard/DashboardDailyMail.html");

            var PatientVisitHeader = "";
            var PatientVisitValue = "";
            decimal TotalSales = 0;
            decimal TotalPurchase = 0;
            decimal TotalGrossMargin = 0;
            decimal TotalCash = 0;
            decimal? TotalCard = 0;
            decimal TotalCredit = 0;
            decimal TotalNumber = 0;
            decimal TotalValue = 0;
            var CombineBranch = "";
            var CombineHeader = "";
            CultureInfo ci = new CultureInfo("en-IN");

            //fileContents = fileContents.Replace("{{PHARMACY_DATE}}",DateTime.Now.ToString());
            DateTime dt = DateTime.Now;
            // fileContents = fileContents.Replace("{{PHARMACY_DATE}}", dt.ToString("dddd, dd MMMM yyyy hh:mm tt"));
            fileContents = fileContents.Replace("{{PHARMACY_DATE}}", dt.ToString("dddd, dd MMMM yyyy"));
            PatientVisitHeader = "<tr style=text-align:center;color:#ffffff;background:#fbad80;>";

            PatientVisitHeader += string.Format("<td style=text-align:center;padding:5px;line-height:15px;font-size:14px;font-family:Trebuchet MS;color:#000000;border-bottom:1px solid #f2f2f2 !important;>{0}</td>", "Branch");

            var designations = PatientVisit.Select(p => p.PatientTypeName).Distinct();


            foreach (var PatientTypeName in designations)
            {
                if (PatientTypeName != CombineHeader)
                {
                    if (PatientTypeName != "-")
                    {
                        PatientVisitHeader += string.Format("<td style=text-transform:capitalize;text-align:center;padding:5px;line-height:15px;font-size:14px;font-family:Trebuchet MS;color:#000000;border-bottom:1px solid #f2f2f2 !important;>{0}</td>", PatientTypeName);
                        CombineHeader = PatientTypeName;
                    }
                }
            }
            PatientVisitHeader += "</tr>";
            fileContents = fileContents.Replace("{{PATIENTVISITHEADER}}", PatientVisitHeader);


            foreach (var items in PatientVisit)
            {
                PatientVisitValue += "<tr style=''>";
                if (items.Branch != CombineBranch)
                {
                    PatientVisitValue += string.Format("<td style=text-align:left;padding:5px;line-height:20px;font-size:13px;font-family:Trebuchet MS,Arial,Helvetica,sans-serif;color:#000000;border-bottom:1px solid #f2f2f2 !important;>{0}</td>", items.Branch);
                    CombineBranch = items.Branch;
                    if (items.Branch == CombineBranch)
                    {
                        foreach (var PatientTypeName in designations)
                        {
                            var PatientGetValue = PatientVisit.Where(p => p.PatientTypeName == PatientTypeName && p.Branch == items.Branch).Select(p => p.PatientVisitCount);

                            if (PatientTypeName != "-")
                            {
                                foreach (var PatientVisitCount in PatientGetValue)
                                {
                                    PatientVisitValue += string.Format("<td style=text-align:right;padding:5px;line-height:20px;font-size:13px;font-family:Trebuchet MS,Arial,Helvetica,sans-serif;color:#000000;border-bottom:1px solid #f2f2f2 !important;>{0}</td>", PatientVisitCount);
                                }
                            }
                        }
                        ////foreach (var itemsValue in PatientVisit)
                        ////{
                        ////    if (itemsValue.PatientTypeName != "-" && itemsValue.Branch== items.Branch)
                        ////    {

                        ////            PatientVisitValue += string.Format("<td style=text-align:center;padding:5px;line-height:20px;font-size:13px;font-family:Trebuchet MS,Arial,Helvetica,sans-serif;color:#000000;border-bottom:1px solid #f2f2f2 !important;>{0}</td>", itemsValue.PatientVisitCount);
                        ////    }

                        ////    //else if (itemsValue.PatientTypeName == "-" && itemsValue.Branch != items.Branch)
                        ////    //{
                        ////    //   PatientVisitValue += string.Format("<td style=text-align:center;padding:5px;line-height:20px;font-size:13px;font-family:Trebuchet MS,Arial,Helvetica,sans-serif;color:#000000;border-bottom:1px solid #f2f2f2 !important;>{0}</td>", 0);
                        ////    //}
                        ////}

                    }
                }
                PatientVisitValue += "</tr>";
            }

            fileContents = fileContents.Replace("{{PATIENTVISITVALUE}}", PatientVisitValue);

            //PatientVisitValue = "<tr style='text-align: center;'>";
            //foreach (var items in PatientVisit)
            //{
            //    if (items.PatientTypeName != "-")
            //    {
            //        PatientVisitValue += string.Format("<td style='padding: 5px;line-height: 20px;font-size: 13px;font-family: 'Trebuchet MS' , Arial, Helvetica, sans-serif;color: #000000;border-bottom: 1px solid #f2f2f2!important;'>{0}</td>", items.PatientVisitCount);
            //    }
            //}
            //PatientVisitValue += "</tr>";
            // fileContents = fileContents.Replace("{{PATIENTVISITVALUE}}", PatientVisitValue);

            fileContents = fileContents.Replace("{{BREAKUPOTC}}", "0.00");
            fileContents = fileContents.Replace("{{BREAKUPPHARMACY}}", "0.00");
            fileContents = fileContents.Replace("{{BREAKUPSURGICAL}}", "0.00");

            var marginContent = "";
            foreach (var items in Marginresult)
            {
                marginContent += "<tr style=''>";

                marginContent += string.Format("<td style=text-align:left;padding:5px;line-height:20px;font-size:13px;font-family:Trebuchet MS,Arial,Helvetica,sans-serif;color:#000000;border-bottom:1px;border-style: solid;>{0}</td>", items.Branch);

                marginContent += string.Format("<td style=text-align:right;padding:5px;line-height:20px;font-size:13px;font-family:Trebuchet MS,Arial,Helvetica,sans-serif;color: #000000;border-bottom:1px solid #f2f2f2 !important;>{0}</td>", string.Format("{0:N}", items.SalesNetMargin));

                TotalSales += items.SalesNetMargin;

                marginContent += string.Format("<td style=text-align:right;padding:5px;line-height:20px;font-size:13px;font-family:Trebuchet MS,Arial,Helvetica,sans-serif;color: #000000;border-bottom:1px solid #f2f2f2 !important;>{0}</td>", string.Format("{0:N}", items.PurchaseNetMargin));

                TotalPurchase += items.PurchaseNetMargin;

                marginContent += string.Format("<td style=text-align:right;padding:5px;line-height:20px;font-size:13px;font-family:Trebuchet MS,Arial,Helvetica,sans-serif;color: #000000;border-bottom:1px solid #f2f2f2 !important;>{0}</td>", string.Format("{0:N}", items.Profit));

                TotalGrossMargin += items.Profit;

                marginContent += "</tr>";
            }

            fileContents = fileContents.Replace("{{MARGIN}}", marginContent);

            fileContents = fileContents.Replace("{{TOTAL_SALES}}", string.Format("{0:N}", TotalSales));
            fileContents = fileContents.Replace("{{TOTAL_PURCHASE}}", string.Format("{0:N}", TotalPurchase));
            fileContents = fileContents.Replace("{{TOTAL_MARGIN}}", string.Format("{0:N}", TotalGrossMargin));

            var TotalSaleContent = "";
            var TotalBouncedContent = "";
            foreach (var items in result)
            {
                fileContents = fileContents.Replace("{{PHARMACY_NAME}}", items.PharmacyName);
                fileContents = fileContents.Replace("{{PHARMACY_OWNER_NAME}}", "Rajesh"/*items.OwnerName*/);

                TotalSaleContent += "<tr style=''>";

                TotalSaleContent += string.Format("<td style=text-align:left;padding:5px;line-height:20px;font-size:13px;font-family:Trebuchet MS,Arial,Helvetica,sans-serif;color:#000000;border-bottom:1px solid #f2f2f2 !important;>{0}</td>", items.Branch);

                TotalSaleContent += string.Format("<td style=text-align:right;padding:5px;line-height:20px;font-size:13px;font-family:Trebuchet MS,Arial,Helvetica,sans-serif;color: #000000;border-bottom:1px solid #f2f2f2 !important;>{0}</td>", string.Format("{0:N}", items.TotalCash));

                TotalCash += items.TotalCash;

                TotalSaleContent += string.Format("<td style=text-align:right;padding:5px;line-height:20px;font-size:13px;font-family:Trebuchet MS,Arial,Helvetica,sans-serif;color: #000000;border-bottom:1px solid #f2f2f2!important;>{0}</td>", string.Format("{0:N}", items.TotalCard));

                TotalCard += items.TotalCard;

                TotalSaleContent += string.Format("<td style=text-align:right;padding:5px;line-height:20px;font-size:13px;font-family:Trebuchet MS,Arial,Helvetica,sans-serif;color: #000000;border-bottom:1px solid #f2f2f2 !important;>{0}</td>", string.Format("{0:N}", items.TotalCredit));

                TotalCredit += items.TotalCredit;
                TotalSaleContent += "</tr>";
            }

            fileContents = fileContents.Replace("{{TOTAL_CARDDETAILS}}", TotalSaleContent);

            foreach (var Bounced in BouncedItems)
            {
                TotalBouncedContent += "<tr style=''>";

                TotalBouncedContent += string.Format("<td style=text-align:left;padding:5px;line-height:20px;font-size:13px;font-family:Trebuchet MS,Arial,Helvetica,sans-serif;color:#000000;border-bottom:1px solid #f2f2f2 !important;>{0}</td>", Bounced.Branch);

                TotalBouncedContent += string.Format("<td style=text-align:right;padding:5px;line-height:20px;font-size:13px;font-family:Trebuchet MS,Arial,Helvetica,sans-serif;color: #000000;border-bottom:1px solid #f2f2f2 !important;>{0}</td>", string.Format("{0:N}", Bounced.TotalSalesCount));

                TotalNumber += Bounced.TotalSalesCount;

                TotalBouncedContent += "</tr>";
            }
            fileContents = fileContents.Replace("{{TOTAL_BOUNCEDITEMS}}", TotalBouncedContent);

            fileContents = fileContents.Replace("{{TOTAL_CASH}}", string.Format("{0:N}", TotalCash));
            fileContents = fileContents.Replace("{{TOTAL_CARD}}", string.Format("{0:N}", TotalCard));
            fileContents = fileContents.Replace("{{TOTAL_CREDIT}}", string.Format("{0:N}", TotalCredit));

            fileContents = fileContents.Replace("{{TOTAL_NUMBER}}", string.Format("{0:N}", TotalNumber));
            fileContents = fileContents.Replace("{{TOTAL_VALUE}}", string.Format("{0:0.00}", TotalValue));


            newbody.Append(fileContents);
            return newbody;
            //string.Format("{0:n0}", items.PurchaseNetMargin)
        }


        public StringBuilder DailyStockBodyBuilder(IEnumerable<NameValue> result, IEnumerable<StockNameValue> ReorderResult, IEnumerable<StockNameValue> ZeroQuantityResult, IEnumerable<StockNameValue> NonMovingResult, IEnumerable<StockNameValue> TrendingSalesResult, IEnumerable<StockNameValue> AbouttoExpireResult, IEnumerable<StockNameValue> ExpiredResult, params object[] templateParams)
        {
            var newbody = new StringBuilder();
            string file = @"Views/DashboardDailyMailStock.html";
            string str = Path.GetFullPath(file);
            var fileContents = System.IO.File.ReadAllText(str);
            CultureInfo ci = new CultureInfo("en-IN");
            DateTime dt = DateTime.Now;
            dt.ToString("dddd, dd MMMM yyyy hh:mm tt");
            foreach (var items in result)
            {
                fileContents = fileContents.Replace("{{PHARMACY_NAME}}", items.Branch);
                fileContents = fileContents.Replace("{{PHARMACY_OWNER_NAME}}", items.OwnerName);
            }

            var Reordercontent = "";
            var Reordercount = 0;
            foreach (var items in ReorderResult)
            {
                Reordercount++;
                Reordercontent += "<tr style=''>";
                Reordercontent += string.Format("<td style=text-align:left;padding:5px;line-height:20px;font-size:13px;font-family:Trebuchet MS,Arial,Helvetica,sans-serif;color:#000000;border-bottom:1px;border-style: solid;>{0}</td>", Reordercount);
                Reordercontent += string.Format("<td style=text-align:left;padding:5px;line-height:20px;font-size:13px;font-family:Trebuchet MS,Arial,Helvetica,sans-serif;color:#000000;border-bottom:1px;border-style: solid; text-transform: uppercase;>{0}</td>", items.ProductName);

                Reordercontent += string.Format("<td style=text-align:right;padding:5px;line-height:20px;font-size:13px;font-family:Trebuchet MS,Arial,Helvetica,sans-serif;color: #000000;border-bottom:1px solid #f2f2f2 !important;>{0}</td>", string.Format("{0:N}", Math.Round(items.AvailableStock, 0).ToString()));

                Reordercontent += string.Format("<td style=text-align:right;padding:5px;line-height:20px;font-size:13px;font-family:Trebuchet MS,Arial,Helvetica,sans-serif;color: #000000;border-bottom:1px solid #f2f2f2 !important;>{0}</td>", string.Format("{0:N}", Math.Round(items.ReOrderQuantity, 0).ToString()));

                Reordercontent += string.Format("<td style=text-align:right;padding:5px;line-height:20px;font-size:13px;font-family:Trebuchet MS,Arial,Helvetica,sans-serif;color: #000000;border-bottom:1px solid #f2f2f2 !important;>{0}</td>", string.Format("{0:N}",  Math.Round(items.Totalsold, 0).ToString()));
                Reordercontent += string.Format("<td style=text-align:right;padding:5px;line-height:20px;font-size:13px;font-family:Trebuchet MS,Arial,Helvetica,sans-serif;color: #000000;border-bottom:1px solid #f2f2f2 !important;>{0}</td>", string.Format("{0:N}", Math.Round(items.AverageSold, 0).ToString()));

                Reordercontent += "</tr>";
            }

            fileContents = fileContents.Replace("{{REORDER}}", Reordercontent);



            var ZeroQuantityContent = "";
            var ZeroQuantityCount = 0;
            foreach (var items in ZeroQuantityResult)
            {


                ZeroQuantityCount++;
                ZeroQuantityContent += "<tr style=''>";
                ZeroQuantityContent += string.Format("<td style=text-align:left;padding:5px;line-height:20px;font-size:13px;font-family:Trebuchet MS,Arial,Helvetica,sans-serif;color:#000000;border-bottom:1px;border-style: solid;>{0}</td>", ZeroQuantityCount);

                ZeroQuantityContent += string.Format("<td style=text-align:left;padding:5px;line-height:20px;font-size:13px;font-family:Trebuchet MS,Arial,Helvetica,sans-serif;color:#000000;border-bottom:1px;border-style: solid;text-transform: uppercase;>{0}</td>", items.ProductName);

                ZeroQuantityContent += string.Format("<td style=text-align:right;padding:5px;line-height:20px;font-size:13px;font-family:Trebuchet MS,Arial,Helvetica,sans-serif;color: #000000;border-bottom:1px solid #f2f2f2 !important;>{0}</td>", string.Format("{0:N}", Math.Round(items.AverageSold, 0).ToString()));

                ZeroQuantityContent += string.Format("<td style=text-align:right;padding:5px;line-height:20px;font-size:13px;font-family:Trebuchet MS,Arial,Helvetica,sans-serif;color: #000000;border-bottom:1px solid #f2f2f2 !important;>{0}</td>", string.Format("{0:N}", Math.Round(items.ReOrderQuantity, 0).ToString()));

                ZeroQuantityContent += "</tr>";
            }

            fileContents = fileContents.Replace("{{ZEROQTY}}", ZeroQuantityContent);


            var TrendingSalesContent = "";
            var TrendingSalesCount = 0;
            foreach (var items in TrendingSalesResult)
            {
                TrendingSalesCount++;
                TrendingSalesContent += "<tr style=''>";
                TrendingSalesContent += string.Format("<td style=text-align:left;padding:5px;line-height:20px;font-size:13px;font-family:Trebuchet MS,Arial,Helvetica,sans-serif;color:#000000;border-bottom:1px;border-style: solid;>{0}</td>", TrendingSalesCount);
                TrendingSalesContent += string.Format("<td style=text-align:left;padding:5px;line-height:20px;font-size:13px;font-family:Trebuchet MS,Arial,Helvetica,sans-serif;color:#000000;border-bottom:1px;border-style: solid;text-transform: uppercase;>{0}</td>", items.ProductName);
                TrendingSalesContent += string.Format("<td style=text-align:right;padding:5px;line-height:20px;font-size:13px;font-family:Trebuchet MS,Arial,Helvetica,sans-serif;color: #000000;border-bottom:1px solid #f2f2f2 !important;>{0}</td>", string.Format("{0:N}", Math.Round(items.AvailableStock, 0).ToString()));
                TrendingSalesContent += string.Format("<td style=text-align:right;padding:5px;line-height:20px;font-size:13px;font-family:Trebuchet MS,Arial,Helvetica,sans-serif;color: #000000;border-bottom:1px solid #f2f2f2 !important;>{0}</td>", string.Format("{0:N}", Math.Round(items.Totalsold, 0).ToString()));
                TrendingSalesContent += string.Format("<td style=text-align:right;padding:5px;line-height:20px;font-size:13px;font-family:Trebuchet MS,Arial,Helvetica,sans-serif;color: #000000;border-bottom:1px solid #f2f2f2 !important;>{0}</td>", string.Format("{0:N}", Math.Round(items.AverageSold, 0).ToString()));

                TrendingSalesContent += "</tr>";
            }

            fileContents = fileContents.Replace("{{TRENDING}}", TrendingSalesContent);



            var NonMovingStockContent = "";
            var NonMovingStockCount = 0;
            foreach (var items in NonMovingResult)
            {
                NonMovingStockCount++;
                NonMovingStockContent += "<tr style=''>";
                NonMovingStockContent += string.Format("<td style=text-align:left;padding:5px;line-height:20px;font-size:13px;font-family:Trebuchet MS,Arial,Helvetica,sans-serif;color:#000000;border-bottom:1px;border-style: solid;>{0}</td>", NonMovingStockCount);

                NonMovingStockContent += string.Format("<td style=text-align:left;padding:5px;line-height:20px;font-size:13px;font-family:Trebuchet MS,Arial,Helvetica,sans-serif;color:#000000;border-bottom:1px;border-style: solid;text-transform: uppercase;>{0}</td>", items.ProductName);
                NonMovingStockContent += string.Format("<td style=text-align:right;padding:5px;line-height:20px;font-size:13px;font-family:Trebuchet MS,Arial,Helvetica,sans-serif;color: #000000;border-bottom:1px solid #f2f2f2 !important;>{0}</td>", string.Format("{0:N}", Math.Round(items.AvailableStock, 0).ToString()));
                NonMovingStockContent += string.Format("<td style=text-align:right;padding:5px;line-height:20px;font-size:13px;font-family:Trebuchet MS,Arial,Helvetica,sans-serif;color: #000000;border-bottom:1px solid #f2f2f2 !important;>{0}</td>", string.Format("{0:N}", items.LastSaleDate));
                NonMovingStockContent += string.Format("<td style=text-align:right;padding:5px;line-height:20px;font-size:13px;font-family:Trebuchet MS,Arial,Helvetica,sans-serif;color: #000000;border-bottom:1px solid #f2f2f2 !important;>{0}</td>", string.Format("{0:N}", items.InvoiceNo));
                NonMovingStockContent += string.Format("<td style=text-align:right;padding:5px;line-height:20px;font-size:13px;font-family:Trebuchet MS,Arial,Helvetica,sans-serif;color: #000000;border-bottom:1px solid #f2f2f2 !important;>{0}</td>", string.Format("{0:N}", Math.Round(items.Age, 0).ToString()));

                NonMovingStockContent += "</tr>";
            }

            fileContents = fileContents.Replace("{{NONMOVING}}", NonMovingStockContent);

            var AboutToExpireContent = "";
            var AboutToExpireCount = 0;
            foreach (var items in AbouttoExpireResult)
            {
                AboutToExpireCount++;
                AboutToExpireContent += "<tr style=''>";
                AboutToExpireContent += string.Format("<td style=text-align:left;padding:5px;line-height:20px;font-size:13px;font-family:Trebuchet MS,Arial,Helvetica,sans-serif;color:#000000;border-bottom:1px;border-style: solid;>{0}</td>", AboutToExpireCount);
                AboutToExpireContent += string.Format("<td style=text-align:left;padding:5px;line-height:20px;font-size:13px;font-family:Trebuchet MS,Arial,Helvetica,sans-serif;color:#000000;border-bottom:1px;border-style: solid;text-transform: uppercase;>{0}</td>", items.ProductName);
                AboutToExpireContent += string.Format("<td style=text-align:right;padding:5px;line-height:20px;font-size:13px;font-family:Trebuchet MS,Arial,Helvetica,sans-serif;color: #000000;border-bottom:1px solid #f2f2f2 !important;>{0}</td>", string.Format("{0:N}", items.ExpireDate));
                AboutToExpireContent += string.Format("<td style=text-align:right;padding:5px;line-height:20px;font-size:13px;font-family:Trebuchet MS,Arial,Helvetica,sans-serif;color: #000000;border-bottom:1px solid #f2f2f2 !important;>{0}</td>", string.Format("{0:N}", Math.Round(items.AvailableStock, 0).ToString()));
                AboutToExpireContent += string.Format("<td style=text-align:right;padding:5px;line-height:20px;font-size:13px;font-family:Trebuchet MS,Arial,Helvetica,sans-serif;color: #000000;border-bottom:1px solid #f2f2f2 !important;>{0}</td>", string.Format("{0:N}", items.GoodsRcvNo));
                AboutToExpireContent += string.Format("<td style=text-align:right;padding:5px;line-height:20px;font-size:13px;font-family:Trebuchet MS,Arial,Helvetica,sans-serif;color: #000000;border-bottom:1px solid #f2f2f2 !important;>{0}</td>", string.Format("{0:N}", items.InvoiceDate));
                AboutToExpireContent += string.Format("<td style=text-align:right;padding:5px;line-height:20px;font-size:13px;font-family:Trebuchet MS,Arial,Helvetica,sans-serif;color: #000000;border-bottom:1px solid #f2f2f2 !important;>{0}</td>", string.Format("{0:N}", items.VendorName));

                AboutToExpireContent += "</tr>";
            }

            fileContents = fileContents.Replace("{{ABOUTTOEXPIRE}}", AboutToExpireContent);


            var ExpiredContent = "";
            var ExpiredCount = 0;
            foreach (var items in ExpiredResult)
            {
                ExpiredCount++;
                ExpiredContent += "<tr style=''>";
                ExpiredContent += string.Format("<td style=text-align:left;padding:5px;line-height:20px;font-size:13px;font-family:Trebuchet MS,Arial,Helvetica,sans-serif;color:#000000;border-bottom:1px;border-style: solid;>{0}</td>", ExpiredCount);

                ExpiredContent += string.Format("<td style=text-align:left;padding:5px;line-height:20px;font-size:13px;font-family:Trebuchet MS,Arial,Helvetica,sans-serif;color:#000000;border-bottom:1px;border-style: solid;text-transform: uppercase;>{0}</td>", items.ProductName);
                ExpiredContent += string.Format("<td style=text-align:right;padding:5px;line-height:20px;font-size:13px;font-family:Trebuchet MS,Arial,Helvetica,sans-serif;color: #000000;border-bottom:1px solid #f2f2f2 !important;>{0}</td>", string.Format("{0:N}", items.ExpireDate));
                ExpiredContent += string.Format("<td style=text-align:right;padding:5px;line-height:20px;font-size:13px;font-family:Trebuchet MS,Arial,Helvetica,sans-serif;color: #000000;border-bottom:1px solid #f2f2f2 !important;>{0}</td>", string.Format("{0:N}", Math.Round(items.AvailableStock, 0).ToString()));
                ExpiredContent += string.Format("<td style=text-align:right;padding:5px;line-height:20px;font-size:13px;font-family:Trebuchet MS,Arial,Helvetica,sans-serif;color: #000000;border-bottom:1px solid #f2f2f2 !important;>{0}</td>", string.Format("{0:N}", items.GoodsRcvNo));
                ExpiredContent += string.Format("<td style=text-align:right;padding:5px;line-height:20px;font-size:13px;font-family:Trebuchet MS,Arial,Helvetica,sans-serif;color: #000000;border-bottom:1px solid #f2f2f2 !important;>{0}</td>", string.Format("{0:N}", items.InvoiceDate));
                ExpiredContent += string.Format("<td style=text-align:right;padding:5px;line-height:20px;font-size:13px;font-family:Trebuchet MS,Arial,Helvetica,sans-serif;color: #000000;border-bottom:1px solid #f2f2f2 !important;>{0}</td>", string.Format("{0:N}", items.VendorName));

                ExpiredContent += "</tr>";
            }

            fileContents = fileContents.Replace("{{EXPIRED}}", ExpiredContent);

            fileContents = fileContents.Replace("{{PHARMACY_DATE}}", dt.ToString("dddd, dd MMMM yyyy hh:mm tt"));
            newbody.Append(fileContents);
            return newbody;

        }


        public StringBuilder PasswordResetBodyBuilder(Tuple<bool, HQueUser> result, string url)
        {
            var newbody = new StringBuilder();
            string file = @"Views/PasswordReset.html";
            string str = Path.GetFullPath(file);
            var fileContents = System.IO.File.ReadAllText(str);

            DateTime dt = DateTime.Now;

            fileContents = fileContents.Replace("{{currentDate}}", dt.ToString("dddd, dd MMMM yyyy hh:mm tt"));

            fileContents = fileContents.Replace("{{UserName}}", result.Item2.Name);
            fileContents = fileContents.Replace("{{UserId}}", result.Item2.UserId);
            fileContents = fileContents.Replace("{{ResetLink}}", url);

            newbody.Append(fileContents);
            return newbody;
        }

        public StringBuilder DashboardManagementBodyBuilder(IEnumerable<NameValue> result, IEnumerable<NameValue> PharmaLiveList, IEnumerable<NameValue> PharmaTrialList, IEnumerable<NameValue> PharmaEdgeList, params object[] templateParams)
        {
            var newbody = new StringBuilder();
            string file = @"Views/DashboardManagement.html";
            string str = Path.GetFullPath(file);
            var fileContents = System.IO.File.ReadAllText(str);

            decimal TotalSales = 0;
            decimal TotalPurchase = 0;
            int TotalSalesCount = 0;
            int TotalPurchaseCount = 0;
            decimal TotalTrialSales = 0;
            decimal TotalTrialPurchase = 0;
            int TotalTrialSalesCount = 0;
            int TotalTrialPurchaseCount = 0;
            decimal TotalEdgeSales = 0;
            decimal TotalEdgePurchase = 0;
            int TotalEdgeSalesCount = 0;
            int TotalEdgePurchaseCount = 0;
            DateTime dt = DateTime.Now;
            int webUser = 0;
            int Offline = 0;
            int TotalUser = 0;
            DateTime Yesterday = DateTime.Now.AddDays(-1);
            fileContents = fileContents.Replace("{{PHARMACY_DATE}}", dt.ToString("dddd, dd MMMM yyyy")); // dt.ToString("dddd, dd MMMM yyyy hh:mm tt"));
            fileContents = fileContents.Replace("{{YESTERDAY_DATE}}", Yesterday.ToString("dddd, dd MMMM yyyy"));

            foreach (var items in result)
            {
                fileContents = fileContents.Replace("{{Edge}}", items.Edge);
                fileContents = fileContents.Replace("{{Paid}}", items.Paid);
                fileContents = fileContents.Replace("{{Trial}}", items.Trial);
                fileContents = fileContents.Replace("{{TotalUser}}", items.Paid);
                webUser = Convert.ToInt32(items.Paid);
                Offline = Convert.ToInt32(items.Offline);
                TotalUser = webUser - Offline;
                fileContents = fileContents.Replace("{{Web}}", string.Format("{0:0}", TotalUser));
                fileContents = fileContents.Replace("{{Offline}}", items.Offline);
                fileContents = fileContents.Replace("{{PharmaLite}}", "0");
                fileContents = fileContents.Replace("{{PatientOver}}", items.PatientOverAll);
                fileContents = fileContents.Replace("{{PatientYes}}", items.PatientYesterday);
                fileContents = fileContents.Replace("{{PatientAcc}}", items.PatientAccepted);
                fileContents = fileContents.Replace("{{PatientMiss}}", items.PatientMissed);
                fileContents = fileContents.Replace("{{DoctorOver}}", items.DoctorOverAll);
                fileContents = fileContents.Replace("{{DoctorYes}}", items.DoctorYesterday);
                fileContents = fileContents.Replace("{{DoctorAcc}}", items.DoctorAccepted);
                fileContents = fileContents.Replace("{{DoctorMiss}}", items.DoctorMissed);
            }

            var PharmaLiveContent = "";
            var PharmaTrialContent = "";
            var PharmaEdgeContent = "";

            //Manivannan on 10-Feb-2017
            var iSno = 0;
            var iSno1 = 0;
            var iSno2 = 0;
            var TotalUsed = 0;
            var TotalTrialUsed = 0;
            var TotalEdgeUsed = 0;

            var City = "";
            var Area = "";

            var TitleCaseBranch = "";
            var TitleCaseCity = "";
            var TitleCaseArea = "";

            // LIVE DETAILS
            foreach (var items in PharmaLiveList)
            {
                iSno++; //Manivannan on 10-Feb-2017

                PharmaLiveContent += "<tr align='center'  style='text-align:center;height:30px;'>";

                PharmaLiveContent += string.Format("<td align='center'  'style=text-transform:capitalize;text-align:right;padding:5px;line-height:20px;font-size:13px;font-family: Arial, Helvetica, sans-serif;color:#000000;border-bottom:1px solid #f2f2f2;'>{0}</td>", iSno); //Manivannan on 10-Feb-2017

                TitleCaseBranch = new string(CharsToTitleCase(items.Branch).ToArray());

                PharmaLiveContent += string.Format("<td align='left' style=text-transform:capitalize;text-align:left;padding:5px;line-height:20px;font-size:13px;font-family: Arial, Helvetica, sans-serif;color:#000000;border-bottom:1px solid #f2f2f2;>{0}</td>", TitleCaseBranch);

                TitleCaseCity = new string(CharsToTitleCase(items.City).ToArray());

                if (TitleCaseCity == null || TitleCaseCity == "" || TitleCaseCity == "." || TitleCaseCity == "-")
                {
                    PharmaLiveContent += string.Format("<td align='center' style=text-transform:capitalize;text-align:center;padding:5px;line-height:20px;font-size:13px;font-family: Arial, Helvetica, sans-serif;color:#000000;border-bottom:1px solid #f2f2f2;>{0}</td>", "-");
                }
                else
                {
                    PharmaLiveContent += string.Format("<td align='center' style=text-transform:capitalize;text-align:left;padding:5px;line-height:20px;font-size:13px;font-family: Arial, Helvetica, sans-serif;color:#000000;border-bottom:1px solid #f2f2f2;>{0}</td>", TitleCaseCity);
                }


                TitleCaseArea = new string(CharsToTitleCase(items.Area).ToArray());

                if (TitleCaseArea == null || TitleCaseArea == "" || TitleCaseArea == "." || TitleCaseArea == "-")
                {
                    PharmaLiveContent += string.Format("<td align='center' style=text-transform:capitalize;text-align:center;padding:5px;line-height:20px;font-size:13px;font-family: Arial, Helvetica, sans-serif;color:#000000;border-bottom:1px solid #f2f2f2;>{0}</td>", "-");
                }
                else
                {
                    PharmaLiveContent += string.Format("<td align='center' style=text-transform:capitalize;text-align:left;padding:5px;line-height:20px;font-size:13px;font-family: Arial, Helvetica, sans-serif;color:#000000;border-bottom:1px solid #f2f2f2;>{0}</td>", TitleCaseArea);
                }

                PharmaLiveContent += string.Format("<td align='right' style=text-align:right;padding:5px;line-height:20px;font-size:13px;font-family: Arial, Helvetica, sans-serif;color: #000000;border-bottom:1px solid #f2f2f2 !important;>{0}</td>", items.TotalSaleCount);

                TotalSalesCount += items.TotalSaleCount;

                PharmaLiveContent += string.Format("<td align='right' style=text-align:right;padding:5px;line-height:20px;font-size:13px;font-family: Arial, Helvetica, sans-serif;color: #000000;border-bottom:1px solid #f2f2f2 !important;>{0}</td>", string.Format("{0:n0}", items.SalesNetMargin));

                TotalSales += items.SalesNetMargin;

                PharmaLiveContent += string.Format("<td align='right' style=text-align:right;padding:5px;line-height:20px;font-size:13px;color: #000000;border-bottom:1px solid #f2f2f2 !important;>{0}</td>", items.TotalPurchaseCount);

                TotalPurchaseCount += items.TotalPurchaseCount;

                //Manivannan on 10-Feb-2017
                if (items.TotalSaleCount > 0 || items.TotalPurchaseCount > 0)
                {
                    TotalUsed++;
                }

                PharmaLiveContent += string.Format("<td  align='right' style=text-align:right;padding:5px;line-height:20px;font-size:13px;font-family: Arial, Helvetica, sans-serif;color: #000000;border-bottom:1px solid #f2f2f2 !important;>{0}</td>", string.Format("{0:n0}", items.PurchaseNetMargin));

                TotalPurchase += items.PurchaseNetMargin;

                PharmaLiveContent += "</tr>";
            }

            fileContents = fileContents.Replace("{{PharmaLiveList}}", PharmaLiveContent);

           // iSno = 0;

            // TRIAL DETAILS
            if (PharmaTrialList.Count() == 0)
            {
                iSno1++; //Manivannan on 10-Feb-2017

                PharmaTrialContent += "<tr style=''>";

                PharmaTrialContent += string.Format("<td width='25px' style=text-transform:capitalize;text-align:right;padding:5px;line-height:20px;font-size:13px;font-family:Trebuchet;color:#000000;border-bottom:1px solid #f2f2f2;>{0}</td>", "-"); //Manivannan on 10-Feb-2017

                PharmaTrialContent += string.Format("<td style=text-transform:capitalize;text-align:center;padding:5px;line-height:20px;font-size:13px;font-family:Trebuchet;color:#000000;border-bottom:1px solid #f2f2f2;>{0}</td>", "-");

                PharmaTrialContent += string.Format("<td style=text-align:center;padding:5px;line-height:20px;font-size:13px;font-family:Trebuchet MS,Arial,Helvetica,sans-serif;color: #000000;border-bottom:1px solid #f2f2f2 !important;>{0}</td>", "-");

                PharmaTrialContent += string.Format("<td style=text-align:center;padding:5px;line-height:20px;font-size:13px;font-family:Trebuchet MS,Arial,Helvetica,sans-serif;color: #000000;border-bottom:1px solid #f2f2f2 !important;>{0}</td>", string.Format("{0:n0}", "-"));

                PharmaTrialContent += string.Format("<td style=text-align:center;padding:5px;line-height:20px;font-size:13px;font-family:Trebuchet MS,Arial,Helvetica,sans-serif;color: #000000;border-bottom:1px solid #f2f2f2 !important;>{0}</td>", "-");

                PharmaTrialContent += string.Format("<td style=text-align:center;padding:5px;line-height:20px;font-size:13px;font-family:Trebuchet MS,Arial,Helvetica,sans-serif;color: #000000;border-bottom:1px solid #f2f2f2 !important;>{0}</td>", string.Format("{0:n0}", "-"));

                PharmaTrialContent += string.Format("<td style=text-align:center;padding:5px;line-height:20px;font-size:13px;font-family:Trebuchet MS,Arial,Helvetica,sans-serif;color: #000000;border-bottom:1px solid #f2f2f2 !important;>{0}</td>", string.Format("{0:n0}", "-"));

                PharmaTrialContent += string.Format("<td style=text-align:center;padding:5px;line-height:20px;font-size:13px;font-family:Trebuchet MS,Arial,Helvetica,sans-serif;color: #000000;border-bottom:1px solid #f2f2f2 !important;>{0}</td>", string.Format("{0:n0}", "-"));

                PharmaTrialContent += "</tr>";
            }
            else
            {
                foreach (var items in PharmaTrialList)
                {
                    iSno1++; //Manivannan on 10-Feb-2017

                   
                    PharmaTrialContent += "<tr align='center'  style='text-align:center;height:30px;'>";

                    PharmaTrialContent += string.Format("<td align='center'  'style=text-transform:capitalize;text-align:right;padding:5px;line-height:20px;font-size:13px;font-family: Arial, Helvetica, sans-serif;color:#000000;border-bottom:1px solid #f2f2f2;'>{0}</td>", iSno1); //Manivannan on 10-Feb-2017

                    TitleCaseBranch = new string(CharsToTitleCase(items.Branch).ToArray());

                    PharmaTrialContent += string.Format("<td align='left' style=text-transform:capitalize;text-align:left;padding:5px;line-height:20px;font-size:13px;font-family: Arial, Helvetica, sans-serif;color:#000000;border-bottom:1px solid #f2f2f2;>{0}</td>", TitleCaseBranch);

                    TitleCaseCity = new string(CharsToTitleCase(items.City).ToArray());

                    if (TitleCaseCity == null || TitleCaseCity == "" || TitleCaseCity == "." || TitleCaseCity == "-")
                    {
                        PharmaTrialContent += string.Format("<td align='center' style=text-transform:capitalize;text-align:center;padding:5px;line-height:20px;font-size:13px;font-family: Arial, Helvetica, sans-serif;color:#000000;border-bottom:1px solid #f2f2f2;>{0}</td>", "-");
                    }
                    else
                    {
                        PharmaTrialContent += string.Format("<td align='center' style=text-transform:capitalize;text-align:left;padding:5px;line-height:20px;font-size:13px;font-family: Arial, Helvetica, sans-serif;color:#000000;border-bottom:1px solid #f2f2f2;>{0}</td>", TitleCaseCity);
                    }
                   

                    TitleCaseArea = new string(CharsToTitleCase(items.Area).ToArray());

                    if (TitleCaseArea == null || TitleCaseArea == "" || TitleCaseArea == "." || TitleCaseArea == "-")
                    {
                        PharmaTrialContent += string.Format("<td align='center' style=text-transform:capitalize;text-align:center;padding:5px;line-height:20px;font-size:13px;font-family: Arial, Helvetica, sans-serif;color:#000000;border-bottom:1px solid #f2f2f2;>{0}</td>", "-");
                    }
                    else
                    {
                        PharmaTrialContent += string.Format("<td align='center' style=text-transform:capitalize;text-align:left;padding:5px;line-height:20px;font-size:13px;font-family: Arial, Helvetica, sans-serif;color:#000000;border-bottom:1px solid #f2f2f2;>{0}</td>", TitleCaseArea);
                    }
                   

                    PharmaTrialContent += string.Format("<td align='right' style=text-align:right;padding:5px;line-height:20px;font-size:13px;font-family: Arial, Helvetica, sans-serif;color: #000000;border-bottom:1px solid #f2f2f2 !important;>{0}</td>", items.TotalSaleCount);

                    TotalTrialSalesCount += items.TotalSaleCount;

                    PharmaTrialContent += string.Format("<td align='right' style=text-align:right;padding:5px;line-height:20px;font-size:13px;font-family: Arial, Helvetica, sans-serif;color: #000000;border-bottom:1px solid #f2f2f2 !important;>{0}</td>", string.Format("{0:n0}", items.SalesNetMargin));

                    TotalTrialSales += items.SalesNetMargin;

                    PharmaTrialContent += string.Format("<td align='right' style=text-align:right;padding:5px;line-height:20px;font-size:13px;color: #000000;border-bottom:1px solid #f2f2f2 !important;>{0}</td>", items.TotalPurchaseCount);


                    TotalTrialPurchaseCount += items.TotalPurchaseCount;

                    //Manivannan on 10-Feb-2017
                    if (items.TotalSaleCount > 0 || items.TotalPurchaseCount > 0)
                    {
                        TotalTrialUsed++;
                    }

                    PharmaTrialContent += string.Format("<td  align='right' style=text-align:right;padding:5px;line-height:20px;font-size:13px;font-family: Arial, Helvetica, sans-serif;color: #000000;border-bottom:1px solid #f2f2f2 !important;>{0}</td>", string.Format("{0:n0}", items.PurchaseNetMargin));

                    TotalTrialPurchase += items.PurchaseNetMargin;

                    PharmaTrialContent += "</tr>";
                }
            }

            fileContents = fileContents.Replace("{{PharmaTrialList}}", PharmaTrialContent);

           // iSno = 0;

            // EDGE DETAILS
            if (PharmaEdgeList.Count() == 0)
            {
                iSno2++; //Manivannan on 10-Feb-2017

                PharmaEdgeContent += "<tr style=''>";

                PharmaEdgeContent += string.Format("<td width='25px' style=text-transform:capitalize;text-align:right;padding:5px;line-height:20px;font-size:13px;font-family:Trebuchet;color:#000000;border-bottom:1px solid #f2f2f2;>{0}</td>", "-"); //Manivannan on 10-Feb-2017

                PharmaEdgeContent += string.Format("<td style=text-transform:capitalize;text-align:center;padding:5px;line-height:20px;font-size:13px;font-family:Trebuchet;color:#000000;border-bottom:1px solid #f2f2f2;>{0}</td>", "-");

                PharmaEdgeContent += string.Format("<td style=text-align:center;padding:5px;line-height:20px;font-size:13px;font-family:Trebuchet MS,Arial,Helvetica,sans-serif;color: #000000;border-bottom:1px solid #f2f2f2 !important;>{0}</td>", "-");

                PharmaEdgeContent += string.Format("<td style=text-align:center;padding:5px;line-height:20px;font-size:13px;font-family:Trebuchet MS,Arial,Helvetica,sans-serif;color: #000000;border-bottom:1px solid #f2f2f2 !important;>{0}</td>", string.Format("{0:n0}", "-"));

                PharmaEdgeContent += string.Format("<td style=text-align:center;padding:5px;line-height:20px;font-size:13px;font-family:Trebuchet MS,Arial,Helvetica,sans-serif;color: #000000;border-bottom:1px solid #f2f2f2 !important;>{0}</td>", "-");

                PharmaEdgeContent += string.Format("<td style=text-align:center;padding:5px;line-height:20px;font-size:13px;font-family:Trebuchet MS,Arial,Helvetica,sans-serif;color: #000000;border-bottom:1px solid #f2f2f2 !important;>{0}</td>", string.Format("{0:n0}", "-"));

                PharmaEdgeContent += string.Format("<td style=text-align:center;padding:5px;line-height:20px;font-size:13px;font-family:Trebuchet MS,Arial,Helvetica,sans-serif;color: #000000;border-bottom:1px solid #f2f2f2 !important;>{0}</td>", "-");

                PharmaEdgeContent += string.Format("<td style=text-align:center;padding:5px;line-height:20px;font-size:13px;font-family:Trebuchet MS,Arial,Helvetica,sans-serif;color: #000000;border-bottom:1px solid #f2f2f2 !important;>{0}</td>", string.Format("{0:n0}", "-"));

                PharmaEdgeContent += "</tr>";
            }
            else
            {
                foreach (var items in PharmaEdgeList)
                {

                    iSno2++; //Manivannan on 10-Feb-2017

                    PharmaEdgeContent += "<tr align='center'  style='text-align:center;height:30px;'>";

                    PharmaEdgeContent += string.Format("<td align='center'  'style=text-transform:capitalize;text-align:right;padding:5px;line-height:20px;font-size:13px;font-family: Arial, Helvetica, sans-serif;color:#000000;border-bottom:1px solid #f2f2f2;'>{0}</td>", iSno2); //Manivannan on 10-Feb-2017

                    TitleCaseBranch = new string(CharsToTitleCase(items.Branch).ToArray());

                    PharmaEdgeContent += string.Format("<td align='left' style=text-transform:capitalize;text-align:left;padding:5px;line-height:20px;font-size:13px;font-family: Arial, Helvetica, sans-serif;color:#000000;border-bottom:1px solid #f2f2f2;>{0}</td>", TitleCaseBranch);

                    TitleCaseCity = new string(CharsToTitleCase(items.City).ToArray());

                    if (TitleCaseCity == null || TitleCaseCity == "" || TitleCaseCity == "." || TitleCaseCity == "-")
                    {
                        PharmaEdgeContent += string.Format("<td align='center' style=text-transform:capitalize;text-align:center;padding:5px;line-height:20px;font-size:13px;font-family: Arial, Helvetica, sans-serif;color:#000000;border-bottom:1px solid #f2f2f2;>{0}</td>", "-");
                    }
                    else
                    {
                        PharmaEdgeContent += string.Format("<td align='center' style=text-transform:capitalize;text-align:left;padding:5px;line-height:20px;font-size:13px;font-family: Arial, Helvetica, sans-serif;color:#000000;border-bottom:1px solid #f2f2f2;>{0}</td>", TitleCaseCity);
                    }


                    TitleCaseArea = new string(CharsToTitleCase(items.Area).ToArray());

                    if (TitleCaseArea == null || TitleCaseArea == "" || TitleCaseArea == "." || TitleCaseArea == "-")
                    {
                        PharmaEdgeContent += string.Format("<td align='center' style=text-transform:capitalize;text-align:center;padding:5px;line-height:20px;font-size:13px;font-family: Arial, Helvetica, sans-serif;color:#000000;border-bottom:1px solid #f2f2f2;>{0}</td>", "-");
                    }
                    else
                    {
                        PharmaEdgeContent += string.Format("<td align='center' style=text-transform:capitalize;text-align:left;padding:5px;line-height:20px;font-size:13px;font-family: Arial, Helvetica, sans-serif;color:#000000;border-bottom:1px solid #f2f2f2;>{0}</td>", TitleCaseArea);
                    }

                    PharmaEdgeContent += string.Format("<td align='left' style=text-transform:capitalize;text-align:left;padding:5px;line-height:20px;font-size:13px;font-family: Arial, Helvetica, sans-serif;color:#000000;border-bottom:1px solid #f2f2f2;>{0}</td>", TitleCaseBranch);

                    PharmaEdgeContent += string.Format("<td align='right' style=text-align:right;padding:5px;line-height:20px;font-size:13px;font-family: Arial, Helvetica, sans-serif;color: #000000;border-bottom:1px solid #f2f2f2 !important;>{0}</td>", items.TotalSaleCount);

                    TotalEdgeSalesCount += items.TotalSaleCount;

                    PharmaEdgeContent += string.Format("<td align='right' style=text-align:right;padding:5px;line-height:20px;font-size:13px;font-family: Arial, Helvetica, sans-serif;color: #000000;border-bottom:1px solid #f2f2f2 !important;>{0}</td>", string.Format("{0:n0}", items.SalesNetMargin));

                    TotalEdgeSales += items.SalesNetMargin;

                    PharmaEdgeContent += string.Format("<td align='right' style=text-align:right;padding:5px;line-height:20px;font-size:13px;color: #000000;border-bottom:1px solid #f2f2f2 !important;>{0}</td>", items.TotalPurchaseCount);


                    TotalEdgePurchaseCount += items.TotalPurchaseCount;

                    //Manivannan on 10-Feb-2017
                    if (items.TotalSaleCount > 0 || items.TotalPurchaseCount > 0)
                    {
                        TotalEdgeUsed++;
                    }

                    PharmaEdgeContent += string.Format("<td  align='right' style=text-align:right;padding:5px;line-height:20px;font-size:13px;font-family: Arial, Helvetica, sans-serif;color: #000000;border-bottom:1px solid #f2f2f2 !important;>{0}</td>", string.Format("{0:n0}", items.PurchaseNetMargin));

                    TotalEdgePurchase += items.PurchaseNetMargin;

                    PharmaEdgeContent += "</tr>";
                }
            }
            fileContents = fileContents.Replace("{{PharmaEdgeList}}", PharmaEdgeContent);

            fileContents = fileContents.Replace("{{TOTAL_USED_CNT}}", string.Format("{0:0}", TotalUsed)); //Manivannan on 10-Feb-2017

            fileContents = fileContents.Replace("{{TOTAL_SALES_CNT}}", string.Format("{0:0}", TotalSalesCount));
            fileContents = fileContents.Replace("{{TOTAL_SALES}}", string.Format("{0:n0}", TotalSales)); // string.Format("{0:N}", TotalSales));
            fileContents = fileContents.Replace("{{TOTAL_PURCHASE_CNT}}", string.Format("{0:0}", TotalPurchaseCount));
            fileContents = fileContents.Replace("{{TOTAL_PURCHASE}}", string.Format("{0:n0}", TotalPurchase));

            fileContents = fileContents.Replace("{{TOTAL_TUSED_CNT}}", string.Format("{0:0}", TotalTrialUsed)); 
            fileContents = fileContents.Replace("{{TOTAL_TSALES_CNT}}", string.Format("{0:0}", TotalTrialSalesCount));
            fileContents = fileContents.Replace("{{TOTAL_TSALES}}", string.Format("{0:n0}", TotalTrialSales)); 
            fileContents = fileContents.Replace("{{TOTAL_TPURCHASE_CNT}}", string.Format("{0:0}", TotalTrialPurchaseCount));
            fileContents = fileContents.Replace("{{TOTAL_TPURCHASE}}", string.Format("{0:n0}", TotalTrialPurchase));

            fileContents = fileContents.Replace("{{TOTAL_EUSED_CNT}}", string.Format("{0:0}", TotalEdgeUsed));
            fileContents = fileContents.Replace("{{TOTAL_ESALES_CNT}}", string.Format("{0:0}", TotalEdgeSalesCount));
            fileContents = fileContents.Replace("{{TOTAL_ESALES}}", string.Format("{0:n0}", TotalEdgeSales));
            fileContents = fileContents.Replace("{{TOTAL_EPURCHASE_CNT}}", string.Format("{0:0}", TotalEdgePurchaseCount));
            fileContents = fileContents.Replace("{{TOTAL_EPURCHASE}}", string.Format("{0:n0}", TotalEdgePurchase));

            newbody.Append(fileContents);
            return newbody;
        }


        // Added for Sales Retrun by San - 15-06-2017

        public StringBuilder SalesReturnBodyBuilder(Sales sales, params object[] templateParams)
        {
            var newbody = new StringBuilder();


            string file = "";
            
           file = @"Views/Sales/SalesOnlyReturnMail.html";
           
            string str = Path.GetFullPath(file);
            var fileContents = System.IO.File.ReadAllText(str);

            CultureInfo ci = new CultureInfo("en-IN");
            DateTime dt = DateTime.Now;

            fileContents = fileContents.Replace("{{P_NAME}}", sales.Name);
            fileContents = fileContents.Replace("{{INSTANCE_NAME}}", sales.Instance.Name);
            fileContents = fileContents.Replace("{{DOCTOR_NAME}}", sales.DoctorName);
            fileContents = fileContents.Replace("{{BILL_NO}}", sales.InvoiceSeries + " " + sales.InvoiceNo);
            fileContents = fileContents.Replace("{{PATIENT_NAME}}", sales.Name);
            fileContents = fileContents.Replace("{{B_DATE}}", Convert.ToDateTime(sales.InvoiceDate).ToString("dd/MM/yyyy"));
            fileContents = fileContents.Replace("{{ADDRESS}}", sales.Address);

            //Added by Sarubala on 28-10-17
            if (sales.PaymentType == "Multiple" && sales.CashType == "MultiplePayment" && sales.SalesPayments.Count == 1 && sales.SalesPayments.FirstOrDefault().PaymentInd == 5)
            {
                fileContents = fileContents.Replace("{{B_TYPE}}", "eWallet");
            }
            else
            {
                fileContents = fileContents.Replace("{{B_TYPE}}", sales.PaymentType);
            }
            
            fileContents = fileContents.Replace("{{PH_NO}}", sales.Mobile);
            fileContents = fileContents.Replace("{{ID_NO}}", sales.EmpID);
            
            var SalesReturnItemList = "";
            //decimal ReturnTotal = 0;
            //decimal ReturnDiscount = 0;
            decimal? cgst = 0;
            decimal? OnlyRtnGSTTotalValue = 0;

             foreach (var item in sales.SalesReturnItem)
                {
                item.MrpSellingPrice = item.SellingPrice;
                //Added by Sarubala to include GST 06-07-2017 - start
                decimal? taxValue = 0;
                if(sales.TaxRefNo == 1)
                {
                    fileContents = fileContents.Replace("{{TAX_TYPE}}", "GST");

                    taxValue = (item.ProductStock.GstTotal != null && item.ProductStock.GstTotal > 0) ? item.ProductStock.GstTotal : ((item.GstTotal != null && item.GstTotal > 0) ? item.GstTotal : 0);
                }
                else
                {
                    fileContents = fileContents.Replace("{{TAX_TYPE}}", "VAT");

                    taxValue = (item.ProductStock.VAT != null && item.ProductStock.VAT > 0) ? item.ProductStock.VAT : ((item.VAT != null && item.VAT > 0) ? item.VAT : 0);
                }
                //Added by Sarubala to include GST 06-07-2017 - end   
                SalesReturnItemList += "<tr style=''>";

                    SalesReturnItemList += string.Format("<td style=text-align:left;padding:5px;line-height:20px;font-size:13px;font-family:Trebuchet MS,Arial,Helvetica,sans-serif;color:#000000;border-bottom:1px solid #f2f2f2 !important;>{0}</td>", item.ProductStock.Product.Name.ToUpper());
                    if (item.ProductStock.Product.Manufacturer == null || item.ProductStock.Product.Manufacturer == "")
                    {
                        SalesReturnItemList += string.Format("<td style=text-align:left;padding:5px;line-height:20px;font-size:13px;font-family:Trebuchet MS,Arial,Helvetica,sans-serif;color: #000000;border-bottom:1px solid #f2f2f2 !important;>{0}</td>", "-");
                    }
                    else
                    {
                        string[] arr = item.ProductStock.Product.Manufacturer.Split(' ');
                        SalesReturnItemList += string.Format("<td style=text-align:left;padding:5px;line-height:20px;font-size:13px;font-family:Trebuchet MS,Arial,Helvetica,sans-serif;color: #000000;border-bottom:1px solid #f2f2f2 !important;>{0}</td>", arr[0]);
                    }

                    SalesReturnItemList += string.Format("<td style=text-align:right;padding:5px;line-height:20px;font-size:13px;font-family:Trebuchet MS,Arial,Helvetica,sans-serif;color: #000000;border-bottom:1px solid #f2f2f2 !important;>{0}</td>", item.ProductStock.BatchNo);

                    SalesReturnItemList += string.Format("<td style=text-align:center;padding:5px;line-height:20px;font-size:13px;font-family:Trebuchet MS,Arial,Helvetica,sans-serif;color: #000000;border-bottom:1px solid #f2f2f2 !important;>{0}</td>", item.ProductStock.ExpireDate.ToFormatExp());

                    SalesReturnItemList += string.Format("<td style=text-align:right;padding:5px;line-height:20px;font-size:13px;font-family:Trebuchet MS,Arial,Helvetica,sans-serif;color: #000000;border-bottom:1px solid #f2f2f2 !important;>{0}</td>", string.Format("{0:0}", item.Quantity));
                
                //Added by Sarubala to include GST 06-07-2017 - start
                
                SalesReturnItemList += string.Format("<td style=text-align:right;padding:5px;line-height:20px;font-size:13px;font-family:Trebuchet MS,Arial,Helvetica,sans-serif;color: #000000;border-bottom:1px solid #f2f2f2 !important;>{0}</td>", string.Format("{0:0.00}", ((item.SellingPrice * 100) / (taxValue + 100))));                              
                
                SalesReturnItemList += string.Format("<td style=text-align:right;padding:5px;line-height:20px;font-size:13px;font-family:Trebuchet MS,Arial,Helvetica,sans-serif;color: #000000;border-bottom:1px solid #f2f2f2 !important;>{0}</td>", string.Format("{0:0.00}", item.SellingPrice - ((item.SellingPrice * 100) / (taxValue + 100))));

                if(sales.TaxRefNo == 1)
                {
                    cgst = cgst + (item.SellingPrice - ((item.SellingPrice * 100) / (taxValue + 100)));
                    decimal? getGstPercent = item.GstTotal + 100;
                    decimal? SalesItemDiscount = ((item.Discount / 100) * item.Total);
                    var ActualValue = (item.Total - SalesItemDiscount);
                    OnlyRtnGSTTotalValue += ActualValue - ((ActualValue * 100) / getGstPercent);
                }

                //Added by Sarubala to include GST 06-07-2017 - end   

                SalesReturnItemList += string.Format("<td style=text-align:right;padding:5px;line-height:20px;font-size:13px;font-family:Trebuchet MS,Arial,Helvetica,sans-serif;color: #000000;border-bottom:1px solid #f2f2f2 !important;>{0}</td>", string.Format("{0:0.00}", item.Total));

                    //ReturnTotal += item.Total as decimal? ?? 0;
                    //ReturnDiscount += ((item.Discount / 100) * item.Total) as decimal? ?? 0;

                    SalesReturnItemList += "</tr>";
                }

                fileContents = fileContents.Replace("{{INVOICE_RETURN_DETAILS}}", SalesReturnItemList);

                //var ReturnNet = ReturnTotal - ReturnDiscount;

            fileContents = fileContents.Replace("{{RETURN_CGST}}", string.Format("{0:0.00}", OnlyRtnGSTTotalValue / 2));  //cgst / 2
            fileContents = fileContents.Replace("{{RETURN_SGST}}", string.Format("{0:0.00}", OnlyRtnGSTTotalValue / 2));   //cgst / 2

            //fileContents = fileContents.Replace("{{RETURN_TOT}}", string.Format("{0:0.00}", ReturnTotal));
            //    fileContents = fileContents.Replace("{{RETURN_DISC}}", string.Format("{0:0.00}", ReturnDiscount));
            fileContents = fileContents.Replace("{{RETURN_TOT}}", string.Format("{0:0.00}", sales.SalesReturn.ReturnItemAmount));
            fileContents = fileContents.Replace("{{RETURN_DISC}}", string.Format("{0:0.00}", sales.SalesReturn.TotalDiscountValue));
            //fileContents = fileContents.Replace("{{RETURN_NET_AMT}}", string.Format("{0:0.00}", ReturnNet));

            //fileContents = fileContents.Replace("{{ROFF}}", string.Format("{0:0.00}", sales.RoundoffNetAmount));
            fileContents = fileContents.Replace("{{ROFF}}", string.Format("{0:0.00}", sales.SalesReturn.RoundOffNetAmount));
            //fileContents = fileContents.Replace("{{RETURN_NET_AMT}}", string.Format("{0:0.00}", ReturnNet));
            fileContents = fileContents.Replace("{{RETURN_NET_AMT}}", string.Format("{0:0.00}", sales.SalesReturn.NetAmount - sales.SalesReturn.RoundOffNetAmount));
            //fileContents = fileContents.Replace("{{PAY_NET_AMT}}", string.Format("{0:0.00}", Math.Round((decimal)ReturnNet, MidpointRounding.AwayFromZero)));
            fileContents = fileContents.Replace("{{PAY_NET_AMT}}", string.Format("{0:0.00}", sales.SalesReturn.NetAmount));
            newbody.Append(fileContents);


             newbody.AppendFormat("<p>For detail invoice, Please <a href='{0}'>click here</a></p>", templateParams[0]);

            return newbody;
        }

        public StringBuilder PlusPharmacyBodyBuilder(IEnumerable<NameValue> result, IEnumerable<NameValue> Marginresult,  params object[] templateParams)
        {
            var newbody = new StringBuilder();
            string file = @"Views/DashboardPlusPharmacyDailyMail.html";
            string str = Path.GetFullPath(file);
            var fileContents = System.IO.File.ReadAllText(str);

            var PatientVisitHeader = "";
            var PatientVisitValue = "";
            decimal TotalSales = 0;
            decimal TotalPurchase = 0;
            decimal TotalGrossMargin = 0;
            decimal TotalCash = 0;
            decimal? TotalCard = 0;
            decimal TotalCredit = 0;
            decimal TotalNumber = 0;
            decimal TotalValue = 0;
            var CombineBranch = "";
            var CombineHeader = "";
            CultureInfo ci = new CultureInfo("en-IN");

            DateTime dt = DateTime.Now;
            fileContents = fileContents.Replace("{{PHARMACY_DATE}}", dt.ToString("dddd, dd MMMM yyyy"));
           

            var marginContent = "";
            var TitleCaseBranch = "";
            var TitleCaseCity = "";
            var TitleCaseArea = "";


            foreach (var items in Marginresult)
            {
                marginContent += "<tr style=''>";

                TitleCaseBranch = new string(CharsToTitleCase(items.Branch).ToArray());

                marginContent += string.Format("<td style=text-align:left;padding:5px;line-height:20px;font-size:13px;font-family: Arial, Helvetica, sans-serif;color:#000000;border-bottom:1px;border-style: solid;>{0}</td>", TitleCaseBranch);

                TitleCaseCity = new string(CharsToTitleCase(items.City).ToArray());

                if (TitleCaseCity==null || TitleCaseCity == "" || TitleCaseCity=="." || TitleCaseCity=="-")
                {
                    marginContent += string.Format("<td style=text-align:center;padding:5px;line-height:20px;font-size:13px;font-family:Trebuchet MS,Arial,Helvetica,sans-serif;color:#000000;border-bottom:1px;border-style: solid;>{0}</td>", "-");
                }
                else
                {
                    marginContent += string.Format("<td style=text-align:center;padding:5px;line-height:20px;font-size:13px;font-family:Trebuchet MS,Arial,Helvetica,sans-serif;color:#000000;border-bottom:1px;border-style: solid;>{0}</td>", TitleCaseCity);
                }

                

                TitleCaseArea = new string(CharsToTitleCase(items.Area).ToArray());

                if (TitleCaseArea == null || TitleCaseArea == "" || TitleCaseArea == "." || TitleCaseArea == "-")
                {
                    marginContent += string.Format("<td style=text-align:center;padding:5px;line-height:20px;font-size:13px;font-family:Trebuchet MS,Arial,Helvetica,sans-serif;color:#000000;border-bottom:1px;border-style: solid;>{0}</td>", "-");
                }
                else
                {
                    marginContent += string.Format("<td style=text-align:center;padding:5px;line-height:20px;font-size:13px;font-family:Trebuchet MS,Arial,Helvetica,sans-serif;color:#000000;border-bottom:1px;border-style: solid;>{0}</td>", TitleCaseArea);
                }

                marginContent += string.Format("<td style=text-align:right;padding:5px;line-height:20px;font-size:13px;font-family:Trebuchet MS,Arial,Helvetica,sans-serif;color: #000000;border-bottom:1px solid #f2f2f2 !important;>{0}</td>", string.Format("{0:N}", items.SalesNetMargin));

                TotalSales += items.SalesNetMargin;

                marginContent += string.Format("<td style=text-align:right;padding:5px;line-height:20px;font-size:13px;font-family:Trebuchet MS,Arial,Helvetica,sans-serif;color: #000000;border-bottom:1px solid #f2f2f2 !important;>{0}</td>", string.Format("{0:N}", items.PurchaseNetMargin));

                TotalPurchase += items.PurchaseNetMargin;

                marginContent += string.Format("<td style=text-align:right;padding:5px;line-height:20px;font-size:13px;font-family:Trebuchet MS,Arial,Helvetica,sans-serif;color: #000000;border-bottom:1px solid #f2f2f2 !important;>{0}</td>", string.Format("{0:N}", items.Profit));

                TotalGrossMargin += items.Profit;

                marginContent += "</tr>";
            }

            fileContents = fileContents.Replace("{{MARGIN}}", marginContent);

            fileContents = fileContents.Replace("{{TOTAL_SALES}}", string.Format("{0:N}", TotalSales));
            fileContents = fileContents.Replace("{{TOTAL_PURCHASE}}", string.Format("{0:N}", TotalPurchase));
            fileContents = fileContents.Replace("{{TOTAL_MARGIN}}", string.Format("{0:N}", TotalGrossMargin));

            var TotalSaleContent = "";
            foreach (var items in result)
            {
                fileContents = fileContents.Replace("{{PHARMACY_NAME}}", items.PharmacyName);
                fileContents = fileContents.Replace("{{PHARMACY_OWNER_NAME}}", items.OwnerName);

                TotalSaleContent += "<tr style=''>";

                TitleCaseBranch = new string(CharsToTitleCase(items.Branch).ToArray());

                TotalSaleContent += string.Format("<td style=text-align:left;padding:5px;line-height:20px;font-size:13px;font-family:Trebuchet MS,Arial,Helvetica,sans-serif;color:#000000;border-bottom:1px solid #f2f2f2 !important;>{0}</td>", TitleCaseBranch);

                TitleCaseCity = new string(CharsToTitleCase(items.City).ToArray());

                if (TitleCaseCity == null || TitleCaseCity == "" || TitleCaseCity == "." || TitleCaseCity == "-")
                {
                    TotalSaleContent += string.Format("<td style=text-align:center;padding:5px;line-height:20px;font-size:13px;font-family:Trebuchet MS,Arial,Helvetica,sans-serif;color:#000000;border-bottom:1px;border-style: solid;>{0}</td>", "-");
                }
                else
                {
                    TotalSaleContent += string.Format("<td style=text-align:center;padding:5px;line-height:20px;font-size:13px;font-family:Trebuchet MS,Arial,Helvetica,sans-serif;color:#000000;border-bottom:1px;border-style: solid;>{0}</td>", TitleCaseCity);
                }



                TitleCaseArea = new string(CharsToTitleCase(items.Area).ToArray());

                if (TitleCaseArea == null || TitleCaseArea == "" || TitleCaseArea == "." || TitleCaseArea == "-")
                {
                    TotalSaleContent += string.Format("<td style=text-align:center;padding:5px;line-height:20px;font-size:13px;font-family:Trebuchet MS,Arial,Helvetica,sans-serif;color:#000000;border-bottom:1px;border-style: solid;>{0}</td>", "-");
                }
                else
                {
                    TotalSaleContent += string.Format("<td style=text-align:center;padding:5px;line-height:20px;font-size:13px;font-family:Trebuchet MS,Arial,Helvetica,sans-serif;color:#000000;border-bottom:1px;border-style: solid;>{0}</td>", TitleCaseArea);
                }

                TotalSaleContent += string.Format("<td style=text-align:right;padding:5px;line-height:20px;font-size:13px;font-family:Trebuchet MS,Arial,Helvetica,sans-serif;color: #000000;border-bottom:1px solid #f2f2f2 !important;>{0}</td>", string.Format("{0:N}", items.TotalCash));

                TotalCash += items.TotalCash;

                TotalSaleContent += string.Format("<td style=text-align:right;padding:5px;line-height:20px;font-size:13px;font-family:Trebuchet MS,Arial,Helvetica,sans-serif;color: #000000;border-bottom:1px solid #f2f2f2!important;>{0}</td>", string.Format("{0:N}", items.TotalCard));

                TotalCard += items.TotalCard;

                TotalSaleContent += string.Format("<td style=text-align:right;padding:5px;line-height:20px;font-size:13px;font-family:Trebuchet MS,Arial,Helvetica,sans-serif;color: #000000;border-bottom:1px solid #f2f2f2 !important;>{0}</td>", string.Format("{0:N}", items.TotalCredit));

                TotalCredit += items.TotalCredit;
                TotalSaleContent += "</tr>";
            }

            fileContents = fileContents.Replace("{{TOTAL_CARDDETAILS}}", TotalSaleContent);

            fileContents = fileContents.Replace("{{TOTAL_CASH}}", string.Format("{0:N}", TotalCash));
            fileContents = fileContents.Replace("{{TOTAL_CARD}}", string.Format("{0:N}", TotalCard));
            fileContents = fileContents.Replace("{{TOTAL_CREDIT}}", string.Format("{0:N}", TotalCredit));

            fileContents = fileContents.Replace("{{TOTAL_NUMBER}}", string.Format("{0:N}", TotalNumber));
            fileContents = fileContents.Replace("{{TOTAL_VALUE}}", string.Format("{0:0.00}", TotalValue));


            newbody.Append(fileContents);
            return newbody;
        }

        IEnumerable<char> CharsToTitleCase(string s)
        {
            bool newWord = true;
            foreach (char c in s)
            {
                if (newWord) { yield return Char.ToUpper(c); newWord = false; }
                else yield return Char.ToLower(c);
                if (c == ' ') newWord = true;
            }
        }

        public bool SendGSTMail(string toAddress, List<string> msg)//Lawrence start
        {
            var message = PrepareGSTMessage(msg);

            if (_configHelper.AppConfig.SendEmail || OverrideConfig)
            {
                var sendRequest = new SendEmailRequest
                {
                    Source = FromAddress,
                    Destination = new Destination { ToAddresses = new List<string> { toAddress } },
                    Message = new Message
                    {
                        Subject = new Content(message[0].ToString()),
                        Body = new Body { Html = new Content(message[1].ToString()) }
                    }
                };

                var emailMessage = new MimeMessage();

                emailMessage.From.Add(new MailboxAddress("hCue", sendRequest.Source));
                emailMessage.To.Add(new MailboxAddress("", sendRequest.Destination.ToAddresses[0]));
                emailMessage.Subject = sendRequest.Message.Subject.Data;
                emailMessage.Body = new TextPart("html") { Text = message[1].ToString() }; //Text = message[1]

                using (var client = new SmtpClient())
                {
                    client.Connect("mail.smtp2go.com", 2525, false);
                    client.Authenticate("hcue.health1@gmail.com", "mzPN3N3WGarh");

                    client.Send(emailMessage);
                    client.Disconnect(true);
                }
                return true;
            }

            LogAndException.WriteToLogger($"NotSent:{message}", ErrorLevel.Debug);
            return false;
        }

        private string[] PrepareGSTMessage(List<string> msg)
        {
            var message = new string[2];            
            string file = "";

            if (msg.Count > 0)
            {
                file = @"Views/Registration/BranchMail.html";
            }

            string str = Path.GetFullPath(file);
            var fileContents = System.IO.File.ReadAllText(str);

            fileContents = fileContents.Replace("{{V_NAME}}", msg[3]);
            fileContents = fileContents.Replace("{{BODY_1}}", "Please make note of GSTIN details for the below mentioned Pharmarcy");
            fileContents = fileContents.Replace("{{GSTIN_NO}}", msg[2]);
            fileContents = fileContents.Replace("{{PHARMARCY_NAME}}", "<br/>" + msg[0] + "");
            fileContents = fileContents.Replace("{{ADDRESS}}", "<br/>" + msg[1] + "");
            fileContents = fileContents.Replace("{{Contact_Name}}", "<br/>" + msg[4] + "");
            fileContents = fileContents.Replace("{{Mobile_No}}", "<br/>" + msg[5] + "");
            message[0] = msg[0]+"'s" + " GSTIN Number";
            message[1] = fileContents;
            
            return message;
        
        }

        public StringBuilder SalesOrderEstimateBodyBuilder(SalesOrderEstimate salesOrderEstimate)
        {
            var newbody = new StringBuilder();
            if (salesOrderEstimate.SalesOrderEstimateType == 1)
            {
                #region Sales Order
                newbody.AppendFormat("<table>");
                newbody.AppendFormat("<tr><td>");
                newbody.AppendFormat("<p>Dear {0},</p>", salesOrderEstimate.Patient.Name);
                //newbody.AppendFormat("<p>We {0} are pleased to receive the sales order for the following details(Order No : {1}).</p>", salesOrderEstimate.Instance.Name, salesOrderEstimate.OrderEstimateId);
                newbody.AppendFormat("<p>Please find the below sales ​order​ details.</p>");
                newbody.AppendFormat("<p>Shop Name - {0}</p>", salesOrderEstimate.Instance.Name);
                newbody.AppendFormat("<p>Address    - {0}</p>", salesOrderEstimate.Instance.FullAddress);
                //newbody.AppendFormat("<p>Order No  - {0}                  Order Date and time  - {1}</p>", salesOrderEstimate.OrderEstimateId, Convert.ToDateTime(salesOrderEstimate.OrderEstimateDate).ToString("dd MMMM yyyy") + " " + Convert.ToDateTime(salesOrderEstimate.CreatedAt).ToString("hh:mm tt"));
                newbody.AppendFormat("<p>Order No   - {0}</p>", salesOrderEstimate.OrderEstimateId);
                newbody.AppendFormat("<p>Order Date and time  - {0}</p>", Convert.ToDateTime(salesOrderEstimate.OrderEstimateDate).ToString("dd MMMM yyyy") + " " + Convert.ToDateTime(salesOrderEstimate.CreatedAt).ToString("hh:mm tt"));
                //newbody.AppendFormat("<br/>");

                //Arrange Css as per requirement
                newbody.Append("<table width=600px border=0 cellspacing=2 cellpadding=2 align=center bgcolor=White dir=ltr rules=all style=border-width: thin; border-style: solid; line-height: normal; vertical-align: baseline; text-align: center; font-family: Calibri; font-size: medium; font-weight: normal; font-style: normal; font-variant: normal; color: #000000; list-style-type: none;>");
                newbody.Append("<tr>");
                newbody.Append("<td>");
                newbody.Append("<table width=600px border=1 cellspacing=2 cellpadding=2 align=center bgcolor=White dir=ltr rules=all style=border-width: thin; border-style: solid; line-height: normal; vertical-align: baseline; text-align: center; font-family: Calibri; font-size: medium; font-weight: normal; font-style: normal; font-variant: normal; color: #000000; list-style-type: none;>");
                #region Html Table Creation
                //Heading
                newbody.Append("<tr style=background-color:#ECECEC;font-weight:bold>");
                newbody.Append("<td>");
                newbody.Append("S No");
                newbody.Append("</td>");
                newbody.Append("<td>");
                newbody.Append("Product Name");
                newbody.Append("</td>");
                newbody.Append("<td>");
                newbody.Append("Quantity");
                newbody.Append("</td>");
                newbody.Append("<td>");
                newbody.Append("Disc %");
                newbody.Append("</td>");
                newbody.Append("<td>");
                newbody.Append("MRP");
                newbody.Append("</td>");
                newbody.Append("<td>");
                newbody.Append("Total Price");
                newbody.Append("</td>");
                newbody.Append("</tr>");
                var i = 0;
                decimal TotalOrder = 0;
                foreach (var item in salesOrderEstimate.SalesOrderEstimateItem)
                {
                    //Rows
                    if (item.IsOrder == null || item.IsOrder == 0)
                    {
                        var TotalMRP = item.Quantity * item.SellingPrice;
                        if (item.Product.Name != null)
                        {
                            item.ProductName = item.Product.Name;
                        }
                        newbody.Append("<tr>");
                        newbody.Append("<td align='right'>");
                        newbody.Append(++i);
                        newbody.Append("</td>");
                        newbody.Append("<td>");
                        newbody.Append(item.ProductName);
                        newbody.Append("</td>");
                        newbody.Append("<td align='right'>");
                        newbody.Append(string.Format("{0:0}", item.Quantity));
                        newbody.Append("</td>");
                        newbody.Append("<td align='right'>");
                        newbody.Append(string.Format("{0:0}", item.Discount));
                        newbody.Append("</td>");
                        newbody.Append("<td align='right'>");
                        newbody.Append(string.Format("{0:0.00}", item.SellingPrice));
                        newbody.Append("</td>");
                        newbody.Append("<td align='right'>");
                        newbody.Append(string.Format("{0:0.00}", TotalMRP));
                        newbody.Append("</td>");
                        TotalOrder = TotalOrder + Convert.ToDecimal(TotalMRP);
                        newbody.Append("</tr>");

                    }
                        
                }
                #endregion
                newbody.Append("</table>");
                newbody.Append("</td>");
              
                newbody.Append("</tr>");
                newbody.Append("</table>");

                newbody.AppendFormat("<p alt='alt text' border='0' align='right' style='padding-right:4px'>Total Amount : {0}</p>", string.Format("{0:0.00}", TotalOrder));
                //newbody.AppendFormat("<p>Phone Number: {0}</p>", salesOrderEstimate.Instance.Phone);
                newbody.AppendFormat("<br/>");
                newbody.AppendFormat("<p>Powered By</p>");
                newbody.AppendFormat("<img src='https://s3.amazonaws.com/hcuetemplates/testing/Images/headerlogo.png' alt='alt text' border='0' align='left'></img>");
                newbody.AppendFormat("</td></tr></table>");
                #endregion
            }
            else if (salesOrderEstimate.SalesOrderEstimateType == 2)
            {
                #region Sales Estimate
                newbody.AppendFormat("<table>");
                newbody.AppendFormat("<tr><td>");
                newbody.AppendFormat("<p>Dear {0},</p>", salesOrderEstimate.Patient.Name);
                //newbody.AppendFormat("<p>We {0} are pleased to give an sales estimate for the following details(Estimate No : {1}).</p>", salesOrderEstimate.Instance.Name, salesOrderEstimate.OrderEstimateId);
                newbody.AppendFormat("<p>Please find the below sales estimate details.</p>");
                newbody.AppendFormat("<p>Shop Name - {0}</p>", salesOrderEstimate.Instance.Name);
                newbody.AppendFormat("<p>Address    - {0}</p>", salesOrderEstimate.Instance.FullAddress);
                //newbody.AppendFormat("<p>Order No  - {0}                  Order Date and time  - {1}</p>", salesOrderEstimate.OrderEstimateId, Convert.ToDateTime(salesOrderEstimate.OrderEstimateDate).ToString("dd MMMM yyyy") + " " + Convert.ToDateTime(salesOrderEstimate.CreatedAt).ToString("hh:mm tt"));
                newbody.AppendFormat("<p>Estimate No   - {0}</p>", salesOrderEstimate.OrderEstimateId);
                newbody.AppendFormat("<p>Estimate Date and time  - {0}</p>", Convert.ToDateTime(salesOrderEstimate.OrderEstimateDate).ToString("dd MMMM yyyy") + " " + Convert.ToDateTime(salesOrderEstimate.CreatedAt).ToString("hh:mm tt"));
                //newbody.AppendFormat("<br/>");

                //Arrange Css as per requirement
                newbody.Append("<table width=600px border=0 cellspacing=2 cellpadding=2 align=center bgcolor=White dir=ltr rules=all style=border-width: thin; border-style: solid; line-height: normal; vertical-align: baseline; text-align: center; font-family: Calibri; font-size: medium; font-weight: normal; font-style: normal; font-variant: normal; color: #000000; list-style-type: none;>");
                newbody.Append("<tr>");
                newbody.Append("<td>");
                newbody.Append("<table width=600px border=1 cellspacing=2 cellpadding=2 align=center bgcolor=White dir=ltr rules=all style=border-width: thin; border-style: solid; line-height: normal; vertical-align: baseline; text-align: center; font-family: Calibri; font-size: medium; font-weight: normal; font-style: normal; font-variant: normal; color: #000000; list-style-type: none;>");
                #region Html Table Creation
                //Heading
                newbody.Append("<tr style=background-color:#ECECEC;font-weight:bold>");
                newbody.Append("<td>");
                newbody.Append("S No");
                newbody.Append("</td>");
                newbody.Append("<td>");
                newbody.Append("Product Name");
                newbody.Append("</td>");
                newbody.Append("<td>");
                newbody.Append("Quantity");
                newbody.Append("</td>");
                newbody.Append("<td>");
                newbody.Append("Batch No");
                newbody.Append("</td>");
                newbody.Append("<td>");
                newbody.Append("Expiry");
                newbody.Append("</td>");
                newbody.Append("<td>");
                newbody.Append("Disc %");
                newbody.Append("</td>");
                newbody.Append("<td>");
                newbody.Append("MRP");
                newbody.Append("</td>");
                newbody.Append("<td>");
                newbody.Append("Total Price");
                newbody.Append("</td>");
                newbody.Append("</tr>");
                var i = 0;
                decimal TotalEstimate = 0;
                foreach (var item in salesOrderEstimate.SalesEstimateItem)
                {
                    if (item.IsOrder == null || item.IsOrder == 0)
                    {
                        //Rows
                        var TotalMRP = item.Quantity * item.SellingPrice;
                        if (item.Product.Name != null)
                        {
                            item.ProductName = item.Product.Name;
                        }
                        newbody.Append("<tr>");
                        newbody.Append("<td align='right'>");
                        newbody.Append(++i);
                        newbody.Append("</td>");
                        newbody.Append("<td>");
                        newbody.Append(item.ProductName);
                        newbody.Append("</td>");
                        newbody.Append("<td align='right'>");
                        newbody.Append(string.Format("{0:0}", item.Quantity));
                        newbody.Append("</td>");
                        newbody.Append("<td>");
                        newbody.Append(item.BatchNo);
                        newbody.Append("</td>");
                        newbody.Append("<td>");
                        newbody.Append(item.ExpireDate.ToFormatExp());
                        newbody.Append("</td>");
                        newbody.Append("<td align='right'>");
                        newbody.Append(string.Format("{0:0}", item.Discount));
                        newbody.Append("</td>");
                        newbody.Append("<td align='right'>");
                        newbody.Append(string.Format("{0:0.00}", item.SellingPrice));
                        newbody.Append("</td>");
                        newbody.Append("<td align='right'>");
                        newbody.Append(string.Format("{0:0.00}", TotalMRP));
                        newbody.Append("</td>");

                        newbody.Append("</tr>");
                        TotalEstimate = TotalEstimate + Convert.ToDecimal(TotalMRP);
                    }
                       
                }
                #endregion
                newbody.Append("</table>");
                newbody.Append("</td>");
                newbody.Append("</tr>");
                newbody.Append("</table>");
                newbody.AppendFormat("<p alt='alt text' border='0' align='right' style='padding-right:4px'> Total Amount : {0}</p>", string.Format("{0:0.00}", TotalEstimate));
                //newbody.AppendFormat("<p>Pharmacy Instance Name: {0}</p>", salesOrderEstimate.Instance.Name);
                //newbody.AppendFormat("<p>Address: {0}</p>", salesOrderEstimate.Instance.FullAddress);
                //newbody.AppendFormat("<p>Phone Number: {0}</p>", salesOrderEstimate.Instance.Phone);
                newbody.AppendFormat("<br/>");
                newbody.AppendFormat("<p>Powered By</p>");
                newbody.AppendFormat("<img src='https://s3.amazonaws.com/hcuetemplates/testing/Images/headerlogo.png' alt='alt text' border='0' align='left'></img>");
                newbody.AppendFormat("</td></tr></table>");
                #endregion
            }

            return newbody;
        }
        //added by nandhini for daily login report
        public StringBuilder DailyLoginMailBodyBuilder(IEnumerable<LoginHistory> result)
        {
            var newbody = new StringBuilder();
            string file = "";
            file = @"Views/Account/DailyLoginMail.html";
            string str = Path.GetFullPath(file);
            var fileContents = System.IO.File.ReadAllText(str);
           
            fileContents = fileContents.Replace("{{Branch_Name}}", result.FirstOrDefault().BranchName.ToString());
            fileContents = fileContents.Replace("{{User_Name}}", result.FirstOrDefault().LogInUserId.ToString());
            fileContents = fileContents.Replace("{{Opening_Date}}", Convert.ToDateTime(result.FirstOrDefault().LoggedInDate).ToString("dd MMMM yyyy"));
            fileContents = fileContents.Replace("{{Opening_time}}", Convert.ToDateTime(result.FirstOrDefault().LoggedInDate).ToString("hh:mm tt"));
            if (result.FirstOrDefault().LoggedOutDate == null)
            {
                fileContents = fileContents.Replace("{{Closing_Date}}", "-");
                fileContents = fileContents.Replace("{{Closing_time}}", "-");
            }
            else
            {
                fileContents = fileContents.Replace("{{Closing_Date}}", Convert.ToDateTime(result.FirstOrDefault().LoggedOutDate).ToString("dd MMMM yyyy"));
                fileContents = fileContents.Replace("{{Closing_time}}", Convert.ToDateTime(result.FirstOrDefault().LoggedOutDate).ToString("hh:mm tt"));
            }
           

            newbody.Append(fileContents);
            return newbody;
        }
    }
}
