﻿using HQue.ApiDataAccess.External;
using HQue.ApiDataAccess.Internal;
using HQue.Biz.Core.Inventory;
using HQue.Biz.Core.Leads;
using HQue.Biz.Core.Master;
using HQue.Biz.Core.Replication;
using HQue.Biz.Core.Setup;
using HQue.Biz.Core.UserAccount;
using HQue.DataAccess.Inventory;
using HQue.DataAccess.Master;
using HQue.DataAccess.Replication;
using HQue.DataAccess.Setup;
using HQue.DataAccess.UserAccount;
using Utilities.Helpers;
using Utilities.Helpers.Interface;
using HQue.Communication.Email;
using HQue.Communication.Sms;
using HQue.Communication;
using HQue.Biz.Core.Dashboard;
using HQue.Biz.Core.Reminder;
using HQue.DataAccess.Dashboard;
using HQue.Biz.Core.Report;
using HQue.DataAccess.Reminder;
using HQue.DataAccess.Report;
using HQue.DataAccess.Accounts;
using HQue.Biz.Core.Accounts;
using HQue.Biz.Core.ETL;
using HQue.Biz.Core.Master.Offline;
using HQue.DataAccess.Communication;
using HQue.DataAccess.Audits;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Configuration;
using Microsoft.AspNetCore.Mvc.TagHelpers;
using DataAccess.QueryBuilder;
using DataAccess.ManageData.Interface;
using DataAccess.ManageData;
using DataAccess.QueryBuilder.SqlServer;
using DataAccess.QueryBuilder.SqLite;
using HQue.Biz.Core.Inventory.Estimates;
using HQue.Biz.Core.Inventory.InvoicePrint;
using HQue.DataAccess.Sync;
using HQue.Biz.Core.Sync;
using HQue.Contract.Infrastructure;
using HQue.DataAccess.Inventory.Estimates;
using HQue.Biz.Core.Settings;
using HQue.DataAccess.Settings;
using HQue.DataAccess.Helpers;
using HQue.DataAccess.ETL;

namespace HQue.Common
{
    public static class DependencyConfig
    {
        public static void ConfigureDependency(this IServiceCollection services, IConfigurationRoot configuration)
        {
            services
                .AddTransient<Response>()
                .AddTransient<SalesManager>()
                .AddTransient<SalesSaveManager>()
                .AddTransient<VendorManager>()
                .AddTransient<UserManager>()
                .AddTransient<VendorPurchaseManager>()
                .AddTransient<ProductStockManager>()
                .AddTransient<AccountSetupManager>()
                .AddTransient<InstanceSetupManager>()
                .AddTransient<ProductManager>()
                .AddTransient<VendorOrderManager>()
                .AddTransient<SalesReturnManager>()
                .AddTransient<DoctorManager>()
                .AddTransient<ReplicationManager>()
                .AddTransient<LeadsManager>()
                .AddTransient<VendorPurchaseReturnManager>()
                .AddTransient<DashboardManager>()
                .AddTransient<ReportManager>()
                .AddTransient<UserReminderManager>()
                .AddTransient<PettyCashManager>()
                 .AddTransient<PettyCashHdrManager>()
                  .AddTransient<PettyCashDtlManager>()
                .AddTransient<PaymentManager>()
                 .AddTransient<CustomerReceiptManager>()
                .AddTransient<CustomReportManager>()
                .AddTransient<StockTransferManager>()
                .AddTransient<EstimateManager>()
                .AddTransient<EstimateSaveManager>()
                .AddTransient<PrintManager>()
                .AddTransient<RegistrationManager>()
                .AddTransient<ToolManager>()
                .AddTransient<CustomerReportManager>()
                .AddTransient<SupplierReportManager>()

                .AddTransient<SalesDataAccess>()
                .AddTransient<PatientDataAccess>()
                .AddTransient<VendorDataAccess>()
                .AddTransient<UserDataAccess>()
                .AddTransient<VendorPurchaseDataAccess>()
                .AddTransient<ProductStockDataAccess>()
                .AddTransient<AccountSetupDataAccess>()
                .AddTransient<InstanceSetupDataAccess>()
                .AddTransient<UserDataAccess>()
                .AddTransient<ProductDataAccess>()
                .AddTransient<VendorOrderDataAccess>()
                .AddTransient<SalesReturnDataAccess>()
                .AddTransient<VendorPurchaseReturnDataAccess>()
                .AddTransient<DoctorDataAccess>()
                .AddTransient<ReplicationDataAccess>()
                .AddTransient<InstanceClientDataAccess>()
                .AddTransient<OnlineReplicationDataAccess>()
                .AddTransient<LeadsDataAccess>()
                .AddTransient<DashboardDataAccess>()
                .AddTransient<ReportDataAccess>()
                .AddTransient<UserAccessManager>()
                .AddTransient<UserManagementDataAccess>()
                .AddTransient<UserReminderDataAccess>()
                .AddTransient<PettyCashDataAccess>()
                 .AddTransient<PettyCashHdrDataAccess>()
                  .AddTransient<PettyCashDtlDataAccess>()
                .AddTransient<PaymentDataAccess>()
                .AddTransient<CustomerReceiptDataAccess>()
                .AddTransient<CommunicationDataAccess>()
                .AddTransient<AuditDataAccess>()
                .AddTransient<CustomReportDataAccess>()
                .AddTransient<StockTransferDataAccess>()
                .AddTransient<EstimateDataAccess>()
                .AddTransient<RegistrationDataAccess>()
                .AddTransient<ToolDataAccess>()
                .AddTransient<CustomerReportDataAccess>()
                .AddTransient<SupplierReportDataAccess>()
                 .AddTransient<HelperDataAccess>()
                .AddTransient<SyncManager>()
                .AddTransient<SyncOnlineDataAccess>()
                .AddTransient<SyncDataAccess>()
                .AddTransient<SyncDataManager>()
                .AddTransient<SyncHelper>()
                .AddTransient<ETLDataAccess>()

                .AddTransient<ProcessRequestHelper>()
                .AddTransient<Import>()
                .AddTransient<ImportCustomer>()

                .AddTransient<ISyncTrigger, SyncTrigger>()
                .AddTransient<ISyncDataAccessFactory, SyncDataAccessFactory>()

                .AddTransient<PatientApi>()
                .AddTransient<PharmaApi>()
                .AddTransient<LeadsApi>()

                .AddTransient<CacheTagHelper>()

                .AddTransient<SmsSender>()
                .AddTransient<EmailSender>()
                .AddTransient<InstanceApi>()
                .AddTransient<HelperCommunication>()
                .AddTransient<QueryBuilderFactory>()

                .AddSingleton(provider => configuration)
                .AddSingleton<AppConfig>()
                .AddSingleton<ExternalApi>()
                .AddSingleton<InternalApi>()
                .AddSingleton<ConfigHelper>()
                .AddSingleton<UrlBuilder>();


            var installType = configuration["Data:InstallType"];

            var appMode = configuration["Data:OfflineMode"];

            if (installType == Constant.InstallTypeSqlServer)
            {
                services.AddTransient<ISqlHelper, SqlServerHelper>()
                    .AddTransient<SelectBuilder, SqlSelectBuilder>()
                    .AddTransient<QueryBuilderBase, SqlQueryBuilder>()
                    .AddTransient<PatientManager>();
            }

            if (installType == Constant.InstallTypeSqlite)
            {
                services.AddTransient<ISqlHelper, SqLiteHelper>()
                    .AddTransient<SelectBuilder, SqliteSelectBuilder>()
                    .AddTransient<QueryBuilderBase, SqLiteQueryBuilder>()
                    .AddTransient<PatientManager, OfflinePatientManager>();
            }

            services.AddSingleton<ICacheManager, CacheManager>();

        }
    }
}
